`�a<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:3;s:11:"post_author";s:2:"32";s:9:"post_date";s:19:"2018-08-28 16:17:28";s:13:"post_date_gmt";s:19:"2018-08-28 14:17:28";s:12:"post_content";s:18611:"<p><strong>Informativa Privacy ai sensi del Regolamento UE 2016/679</strong></p>
<p>D.P.S.</p>

<!-- wp:paragraph -->
<p><strong>Informativa
sulla protezione dei dati personali</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La presente informativa
è resa - ai sensi degli artt. 13 e 14 del Regolamento UE n. 679/2016 (d’ora in
poi GDPR, General Data Protection Regulation) - agli utenti che accedono al
portale <strong>www.acercampania.it</strong>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La validità
dell'informativa non si estende a siti web terzi accessibili attraverso
collegamento ipertestuale.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dati di contatto del Titolare del trattamento dei dati
personali e del DPO</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il <strong>Titolare</strong> del trattamento dei dati personali è l’Agenzia Campana per
i Servizi Residenziali (A.C.E.R.), con sede centrale in Napoli, alla Via
Domenico Morelli 75, C.F.: 08496131213, PEC: acercampania@legalmail.it.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gli utenti, utilizzando
il sito, accettano questa informativa e sono, pertanto, invitati a prenderne
visione prima di fornire informazioni personali di qualsiasi genere.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In conformità agli
artt. 37-39 del GDPR, l’A.C.E.R. ha designato il prof. avv. Salvatore Sica
quale&nbsp; <strong>Responsabile della Protezione dei dati personali </strong>(DPO),
contattabile all’indirizzo e-mail: dpo@acercampania.it.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Finalità e base giuridica del trattamento</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I dati personali forniti attraverso la navigazione del sito sono trattati
per le finalità strettamente connesse e necessarie all'accesso e alla
consultazione del sito dell’A.C.E.R.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I dati personali forniti volontariamente attraverso il modulo di contatto
messo a disposizione da A.C.E.R. saranno trattati esclusivamente per finalità
che rientrano nei compiti istituzionali e statutari dell’Ente e di interesse
pubblico di cui alla Legge regionale 18 gennaio 2016, n. 1 e alle successive
Delibere di Giunta Regionale, per gli adempimenti previsti da norme di legge o
di regolamento, o per rispondere alle richieste degli utenti. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Per la privacy policy completa dell’Ente, rivolta anche agli utenti di
A.C.E.R. e dei suoi Dipartimenti territoriali, si rimanda all’apposita sezione
presente sul sito web www.acercampania.it.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ai sensi dell’art. 6, par. 1, lett. a), b), c) ed e) del GDPR, le basi
giuridiche del trattamento svolto dall’A.C.E.R. possono consistere:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>a) nell’esecuzione di un compito di interesse pubblico o connesso
all’esercizio di pubblici poteri. Rientrano in questo ambito i trattamenti
compiuti per lo svolgimento di funzioni amministrative inerenti gli inquilini,
i fornitori, le imprese, la gestione dei servizi interni, la gestione del
personale, la gestione della contabilità, la gestione del servizio legale e
contenzioso; l’esercizio di ulteriori funzioni amministrative per servizi di
competenza regionale affidate all’Ente. Adempimento di un obbligo legale al
quale è soggetto l’Ente. Esecuzione di un contratto con i soggetti interessati.
Altre specifiche e diverse finalità. La finalità del trattamento è stabilita
dalla fonte normativa che lo disciplina.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>b) nell’adempimento di un obbligo legale al quale è soggetto l’Ente. La
finalità del trattamento è stabilita dalla fonte normativa che lo disciplina;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>c) nell’esecuzione di un contratto con soggetti interessati;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>d) in finalità diverse da quelle di cui ai precedenti punti, purché
l’interessato esprima il consenso al trattamento.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Tipologie di dati trattati</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’A.C.E.R. tratta in via generale i dati personali dei soggetti con cui
stabilisce rapporti precontrattuali e/o contrattuali in conformità delle
finalità istituzionali perseguite, consistenti in via esemplificativa nei dati
e documenti anagrafici e di tutte le altre informazioni, anche di carattere
economico-reddituale, che risultano strettamente necessarie per lo svolgimento
delle predette finalità istituzionali.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dati
di navigazione</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I sistemi informatici e
le procedure software preposte al funzionamento del sito web acquisiscono
alcuni dati personali la cui trasmissione è implicita nell'uso dei protocolli
di comunicazione di Internet (ad esempio, l'indirizzo IP o i nomi a dominio dei
dispositivi attraverso cui gli utenti si connettono al sito, gli indirizzi di
notazione URI (Uniform Resources Identifier) delle risorse richieste, i log di
sistema, l'ID o identificatore univoco del dispositivo, il tipo di
dispositivo).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Si tratta di dati che
non sono raccolti per essere combinati ai fini dell’identificazione di una
determinata persona fisica, ma sono utilizzati al solo fine di ricavare
informazioni statistiche anonime sull'uso del sito e per controllarne il
corretto funzionamento. Essi vengono cancellati immediatamente dopo
l'elaborazione o, comunque, dopo trenta giorni. I dati possono anche essere
utilizzati per l'accertamento di responsabilità in caso di ipotetici reati
informatici ai danni del sito, in tal caso i termini per la cancellazione sono
sospesi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Accanto a questi dati,
vengono trattati anche i dati personali relativi ai log degli utenti iscritti e
abilitati all'Accesso Riservato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dati
forniti volontariamente dall'utente</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Attraverso il sito è
possibile inviare richieste e comunicazioni agli indirizzi di contatto ivi
indicati. I dati conferiti verranno utilizzati per rispondere alle richieste
inviate e per inviare le comunicazioni inerenti gli aggiornamenti del sito.Gli
eventuali servizi informativi e di aggiornamento offerti dall’A.C.E.R. agli utenti
che hanno prestato il proprio consenso, possono essere cancellati in ogni
momento inviando una richiesta all’indirizzo e-mail: privacy@acercampania.it.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Cookie</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I cookies sono piccoli
file di testo che i siti web usano per memorizzare e raccogliere informazioni
attraverso il browser. Sono utilizzati principalmente per misurare e migliorare
la qualità del sito attraverso l'analisi del comportamento dei visitatori, per
personalizzare le pagine e ricordare le preferenze degli utenti</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La Direttiva 2009/136/CE)
ha modificato l'art. 122 del d.lgs. 196/2003, richiedendo che sia ottenuto il
consenso dai visitatori per memorizzare o raccogliere ogni informazione sul
computer o su ogni altro dispositivo connesso al web. Il Garante per la
Protezione dei Dati Personali ha individuato le modalità semplificate per
l'informativa e l'acquisizione del consenso per l'uso dei cookie nel
provvedimento datato 8 maggio 2014 - doc. web n. 3118884 a cui queste note
legali si conformano. Questa informativa si adegua a quanto previsto dal GDPR e
tiene conto del progetto di Regolamento ePrivacy in corso di approvazione.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I cookies si
definiscono di “prima parte” quando vengono installati dal sito che l’utente
sta visitando, mentre sono di “terze parti” quando vengono impostati da un sito
web diverso da quello che l’utente sta visitando.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>La durata dei cookie
installati può essere limitata alla sessione di navigazione o estendersi per un
tempo maggiore, anche dopo che l’utente ha abbandonato il sito visitato. Per
disattivare, rimuovere o bloccare i cookies è comunque possibile ricorrere alle
impostazioni del browser o all'opzione DoNotTrack, dove disponibile. In caso di
disattivazione non si garantisce la completa fruibilità del sito web.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tipologie di cookie e
loro funzionalità</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>1. <em>Cookie tecnici</em>. Questi cookie sono fondamentali per il corretto
funzionamento del sito web e per l’utilizzo di alcune funzionalità: ad esempio,
essi consentono di identificare una sessione, accedere ad aree riservate,
ricordare gli elementi che compongono una richiesta formulata in precedenza,
perfezionare un ordine di acquisto o la memorizzazione di un preventivo. Senza
i cookies tecnici, i servizi normalmente offerti dal sito potrebbero risultare
parzialmente o totalmente inaccessibili. Trattandosi di cookies indispensabili
per la navigazione e la fruizione dei servizi richiesti, non è necessario
richiedere e fornire il consenso dell’utente. E' possibile bloccare o rimuovere
i cookie tecnici modificando la configurazione delle opzioni del proprio browser:
tuttavia, eseguendo una di queste operazioni, è possibile che non si riesca ad
accedere a determinate aree del sito web o ad utilizzare alcuni dei servizi
offerti. Questa tipologia di cookie non raccoglie informazioni utilizzabili a
fini commerciali e la durata è limitata a quella della sessione stessa.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I cookies tecnici
verranno conservati fino alla conclusione della tua visita e verranno
cancellati alcune ore dopo la chiusura del browser.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>2. <em>Cookie analitici</em>. I cookie analitici raccolgono in forma anonima
informazioni sulla navigazione degli utenti e vengono utilizzati per calcolare
e migliorare le prestazioni del sito, nonché per personalizzare l’esperienza
online dell’utente attraverso l’individuazione di contenuti specifici. Le
informazioni raccolte attraverso i cookie analitici (ad es. le informazioni
sulle modalità di utilizzo del sito web da parte degli utenti e la ricezione di
messaggi di errore) vengono utilizzate esclusivamente dal Consiglio Nazionale
Forense o da parte dei fornitori di servizi che operano per conto della
medesima, i quali non memorizzano, comunicano e diffondono i dati a soggetti
terzi. Per i cookies analitici non è necessario raccogliere o fornire il
consenso dell’utente, il quale può bloccarli o rimuoverli modificando la configurazione
delle opzioni del proprio browser: tuttavia, eseguendo una di queste
operazioni, è possibile che non si riesca ad accedere a determinate aree del
sito web o ad utilizzare alcuni dei servizi offerti.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Le informazioni
generate sull’utilizzo del sito web da parte dell’utente (compreso il suo
indirizzo IP) vengono trasmesse e depositate presso server di terze parti.
Questa tipologia di cookie non raccoglie informazioni di carattere personale
e/o utilizzabili a fini commerciali e la durata di conservazione dei dati è
limitata a 26 mesi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esistono diversi modi
per gestire i cookie e altre tecnologie di tracciabilità. Modificando le
impostazioni del browser, puoi accettare o rifiutare i cookie o decidere di
ricevere un messaggio di avviso prima di accettare un cookie dai siti Web
visitati. Ti ricordiamo che disabilitando completamente i cookie nel browser
potresti non essere in grado di utilizzare tutte le nostre funzionalità
interattive. Se utilizzi più computer in postazioni diverse, assicurati che
ogni browser sia impostato in modo da soddisfare le tue preferenze. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>È possibile eliminare
tutti i cookie installati nella cartella dei cookie del tuo browser. Ciascun
browser presenta procedure diverse per la gestione delle impostazioni. Fai clic
su uno dei collegamenti sottostanti per ottenere istruzioni specifiche.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>3. <em>Attivazione e disattivazione dei cookie tramite servizi di terzi</em>.
Oltre a poter utilizzare gli strumenti previsti dal browser per attivare o
disattivare i singoli cookie, informiamo che il sito www.youronlinechoices.com
riporta l'elenco (in costante aggiornamento) dei principali provider che
lavorano con i gestori dei siti web per raccogliere e utilizzare informazioni
utili alla fruizione della pubblicità comportamentale. Questo strumento
permette all’utente di disattivare o attivare i cookies per tutte le società o,
in alternativa, regolare le sue preferenze per ogni singola società.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Modalità, durata del trattamento e conservazione</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’A.C.E.R. informa che
i dati acquisiti nell'ambito dell’attività di erogazione dei propri servizi,
raccolti presso le proprie sedi o tramite il proprio sito internet:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; saranno trattati nel rispetto delle
normative vigenti con/e senza l'ausilio di mezzi informatici e telematici o
comunque automatizzati, sotto la responsabilità dell’A.C.E.R., che è titolare
del trattamento al solo fine di fornire il servizio richiesto;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; saranno conservati esclusivamente per il
periodo in cui lo stesso sito internet o servizio di newsletter, di
informazione saranno attivi;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; saranno trattati esclusivamente ai
predetti fini istituzionali; </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; non saranno trattati e/o comunicati e/o
condivisi a soggetti diversi da Fondazioni ed Università collegate e/o altri
enti pubblici o privati che ne facciano richiesta se non per fini e scopi
previsti dalla Legge.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tutti i dati verranno
trattati in formato prevalentemente elettronico. I dati personali nonché ogni
altra eventuale informazione associabile, direttamente od indirettamente, ad un
utente determinato, sono raccolti e trattati applicando le misure di sicurezza
tecniche e organizzative tali da garantire un livello di sicurezza adeguato al
rischio, tenendo conto dello stato dell'arte e dei costi di attuazione, o, ove
previste, misure di sicurezza prescritte da apposita normativa quale a titolo
di esempio non esaustivo: misure previste da applicabili provvedimenti emessi
dal Garante per la protezione dati personali e saranno accessibili solo al
personale specificamente autorizzato.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I dati personali
trattati verranno conservati in una forma che consenta l'identificazione degli
interessati per un arco di tempo non superiore al conseguimento delle finalità
per le quali sono trattati, fatta salva la necessità di conservarli per un
periodo maggiore a seguito di richieste da parte delle Autorità competenti in
materia di prevenzione e prosecuzione di reati o, comunque per far valere o
difendere un diritto in sede giudiziaria. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Categorie di destinatari dei dati personali</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I dati saranno trattati
esclusivamente dal personale e dai collaboratori autorizzati dell’A.C.E.R. o
delle imprese espressamente nominate come Responsabili o Sub-Responsabili del
trattamento (ad es. per esigenze di manutenzione tecnologica del sito o per la
gestione operativa ed informatica di alcuni servizi). </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Il Titolare del
Trattamento non trasferisce i dati personali raccolti a Paesi terzi o
organizzazioni internazionali, salvo i casi in cui - a norma dell’art. 46 del
GDPR - fornisca garanzie adeguate, e a condizione che l’interessato disponga di
diritti azionabili e di mezzi di ricorso effettivi.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Diritti dell’interessato</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ciascun soggetto
interessato ha diritto di ottenere dall’A.C.E.R., nei casi previsti, l'accesso
ai dati personali e la rettifica, l’integrazione o la cancellazione degli
stessi o la limitazione del trattamento che lo riguarda, la portabilità, la
trasformazione in forma anonima o il blocco dei dati trattati compresi quelli
di cui non è necessaria la conservazione in relazione agli scopi per i quali i
dati sono stati raccolti o successivamente trattati, o di opporsi al
trattamento (artt. 15 e ss. del GDPR) eccettuato il caso in cui tale
adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente
sproporzionato rispetto al diritto tutelato. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L'apposita istanza va
indirizzata all’A.C.E.R. - Agenzia Campana per i Servizi Residenziali Via
Domenico Morelli 75, 80121 Napoli, email: privacy@acercampania.it, anche per il
tramite del Responsabile della protezione dei dati personali. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>L’interessato ha
altresì diritto, nel caso in cui ritenga che il trattamento dei dati personali
effettuato attraverso i servizi offerti dall’A.C.E.R. avvenga in violazione di
quanto previsto dal GDPR, di proporre reclamo al Garante della protezione dei
dati personali (ex art. 77 del GDPR), o di adire le opportune sedi giudiziarie
(ex art. 79 del GDPR).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>La
presente informativa è valida dal 25 maggio 2018 ed è soggetta ad
aggiornamenti.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Gli
utenti, pertanto, sono invitati a verificarne periodicamente il contenuto</strong>.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:14:"Privacy Policy";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"privacy-policy";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2021-10-13 13:25:51";s:17:"post_modified_gmt";s:19:"2021-10-13 11:25:51";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:37:"http://www.acercampania.it/?page_id=3";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}
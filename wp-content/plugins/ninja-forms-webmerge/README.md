# Ninja Forms WebMerge

Create Documents Faster with the Ninja Forms integration for WebMerge.

## Settings

Requires API Key and API Secret from WebMerge.

- API Key
- API Secret

## Actions

### WebMerge

Merge the form submission with a document using the WebMerge service.

- Document: The document that you want to merge.

* Merge Fields are dynamically added as options to the action based on the chosen document.

## Reference

### WebMerge API Documentation

The WebMerge API Documentation can be found at [https://www.webmerge.me/developers](https://www.webmerge.me/developers).

### PHP Wrapper for WebMerge API

This extension uses the PHP Wrapper for the WebMerge API, which can be found on [Github](https://github.com/) at [https://github.com/webmerge/api-php](https://github.com/webmerge/api-php).

## Issues

Any issues or feature requests can be reported to the WP Ninjas Support Team at [NinjaForms.com](https://ninjaforms.com/contact/).

<?php if ( ! defined( 'ABSPATH' ) ) exit;

final class NF_WebMerge_Menu extends NF_Base_Menu
{
    public $menu_slug = 'ninja-forms-webmerge';

    public function __construct()
    {
        $this->name  = 'WebMerge';
        $this->title = __( 'WebMerge', NF_WebMerge::TEXTDOMAIN );

        $this->settings = array(
            'api_key'    => __( 'API Key', NF_WebMerge::TEXTDOMAIN ),
            'api_secret' => __( 'API Secret', NF_WebMerge::TEXTDOMAIN ),
        );

        parent::__construct();
    }

    /**
     * Display
     *
     * The default display method.
     */
    public function display()
    {
        $this->tab = ( isset( $_GET['tab'] ) ) ? $_GET['tab'] : '';
        include NF_WebMerge::$dir . 'includes/templates/admin-menu.html.php';
    }

    /**
     * Enqueue Styles
     */
    public function enqueue_styles()
    {
        wp_enqueue_style(
        /* Handle       */ 'ninja-forms-webmerge-admin-css',
            /* Source       */ NF_WebMerge::$url . '/assets/css/dev/ninja-forms-webmerge-admin.css',
            /* Dependencies */ FALSE,
            /* Version      */ NF_WebMerge::VERSION,
            /* In Footer    */ FALSE
        );
    }

    /**
     * Enqueue Scripts
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script(
        /* Handle       */ 'ninja-forms-webmerge-admin-js',
            /* Source       */ NF_WebMerge::$url . '/assets/js/min/ninja-forms-webmerge-admin.min.js',
            /* Dependencies */ array( 'jquery' ),
            /* Version      */ NF_WebMerge::VERSION,
            /* In Footer    */ TRUE
        );
    }


} // End NF_WebMerge_Menu Class

new NF_WebMerge_Menu();

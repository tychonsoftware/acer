<h2 class="nav-tab-wrapper">

    <a href="<?php echo admin_url( 'admin.php?page=' . $this->menu_slug ); ?>" class="nav-tab <?php if( '' == $this->tab OR 'settings' == $this->tab ) echo 'nav-tab-active';?>">
        <?php _e( 'Settings', NF_WebMerge::TEXTDOMAIN ); ?>
    </a>

    <a href="https://www.webmerge.me/register?plan=free" target="_blank" class="button button-secondary">
        <span class="dashicons dashicons-external" style="padding: 2px 2px 0 0;"></span>
        <?php _e( 'WebMerge.me', NF_WebMerge::TEXTDOMAIN ); ?>
    </a>

    <a href="https://www.webmerge.me/manage/account?status=new_api_key&page=api" target="_blank" class="button button-secondary">
        <span class="dashicons dashicons-external" style="padding: 2px 2px 0 0;"></span>
        <?php _e( 'Get API Keys', NF_WebMerge::TEXTDOMAIN ); ?>
    </a>

</h2>
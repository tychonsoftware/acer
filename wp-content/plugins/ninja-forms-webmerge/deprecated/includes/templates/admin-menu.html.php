<div class="wrap">

    <h2><?php _e( 'Ninja Forms', 'ninja-forms' ); ?> <?php _e( 'WebMerge', NF_WebMerge::TEXTDOMAIN ); ?></h2>

    <p>Create Documents Faster with the Ninja Forms integration for WebMerge.</p>

    <?php include NF_WebMerge::$dir . 'includes/templates/admin-menu-toolbar.html.php'; ?>

    <?php if( '' == $this->tab )
        include NF_WebMerge::$dir . 'includes/templates/admin-menu-settings.html.php';
    ?>

</div>
<!-- SETTING: DOCUMENTS -->
<?php if( ! $this->webmerge ): ?>
<tr>
    <th>
        <!-- This <th/> intentionally left blank. -->
    </th>
    <td>
        <?php printf( __( 'Please add your API Keys to %sconfigure WebMerge%s', NF_WebMerge::TEXTDOMAIN ), '<a href="' . admin_url( 'admin.php?page=ninja-forms-webmerge' ) . '">', '</a>' ); ?>
    </td>
</tr>
<?php else: ?>
<tr>
    <th scope="row">
        <label for="settings[document']"><?php _e( 'Documents', NF_WebMerge::TEXTDOMAIN ); ?></label>
    </th>
    <td>
        <?php if( is_array( $this->documents ) AND ! empty( $this->documents ) ): ?>
            <select name="settings[document]" id="settings-document">

                <?php foreach( $this->documents as $document ): ?>
                    <option value="<?php echo $document['id']; ?>"<?php if( $document['id'] == $settings['document'] ) echo " selected"; ?>>
                        <?php echo $document['name']; ?>
                    </option>
                <?php endforeach; ?>

            </select>
            <img id="mergeFieldsLoading" src="<?php echo includes_url( 'images/spinner.gif' ); ?>" alt="Loading..." style="display: none; margin-bottom: -5px;" />
        <?php else: ?>
            <span class=""><?php _e( 'No documents were found. Please confirm your', NF_WebMerge::TEXTDOMAIN ); ?> <a href="<?php echo admin_url( 'admin.php?page=ninja-forms-webmerge' ); ?>"><?php _e( 'WebMerge', NF_WebMerge::TEXTDOMAIN ); ?> <?php _e( 'Settings', NF_WebMerge::TEXTDOMAIN ); ?></a>.</span>
        <?php endif; ?>
        <span class="howto"><?php _e( 'The Document that you want to merge.', NF_WebMerge::TEXTDOMAIN ); ?></span>
        <?php if( isset( $_GET['debug'] ) ) { echo "<pre>"; var_dump($settings['document']); echo "</pre>"; } ?>
        <?php if( isset( $_GET['debug'] ) ) { echo "<pre>"; var_dump($this->documents); echo "</pre>"; } ?>
    </td>
</tr>
<tbody id="mergeFields">

    <?php foreach( $this->fields as $field ): ?>
    <tr>
        <th><?php echo $field['name']; ?></th>
        <td>
            <select name="settings[<?php echo $settings['document']; ?>][<?php echo $field['key']; ?>]">
                <option></option>
                <?php foreach( $form_fields as $form_field ): ?>
                <option value="<?php echo $form_field['id']; ?>"<?php if( $form_field['id'] == $settings['fields'][ $settings['document'] ][ $field['key'] ] ) echo " selected"; ?>>
                    <?php echo $form_field['data']['label']; ?>
                </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <?php endforeach; ?>

</tbody>

<script type="text/javascript">
    /* ToDo: Move variables to an object */
    var formFields = <?php echo json_encode( $form_fields ); ?>;

    var actionSettings = <?php echo json_encode( $settings ); ?>;

    var selectFields = jQuery( '<select></select>');

    jQuery.each( formFields, function( key, field ) {

        if ('_submit' != field.type) {

            var option = jQuery( '<option></option>' ).val( field.id ).text( field.data.label );

            jQuery( selectFields ).append( option );
        }

    });
</script>

<script type="text/javascript" >
    jQuery(document).ready(function($) {

        $( "#settings-document" ).change(function() {

            $('#mergeFields').empty();
            $('#response').hide();
            $('#mergeFieldsLoading').show();

            var data = {
                'action': 'nf_webmerge_document_fields',
                'document_id': $(this).val()
            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            $.post(ajaxurl, data, function(response) {

                console.log( JSON.parse( response ) );

                $.each( JSON.parse( response ), function( key, field ){

                    var select = $( selectFields).clone().attr( 'name', 'settings[' + data.document_id + '][' + field.key + ']' );

                    console.log( field.key );

                    // Pre-populate stored values
                    if( actionSettings['fields'][ data.document_id ] ){
                        $( select).val( actionSettings['fields'][ data.document_id ][ field.key ] );
                    }

                    var th = $('<th>' + field.name + '</th>');

                    var td = $('<td></td>').append( select );

                    var tr = $('<tr></tr>').append( th ).append( td );

                    $('#mergeFields').append( tr );

                });

                $('#response').show();
                $('#mergeFieldsLoading').hide();
            });

        });

    });
</script>
<?php endif; ?>
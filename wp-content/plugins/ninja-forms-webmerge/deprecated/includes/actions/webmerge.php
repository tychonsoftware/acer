<?php if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'NF_Notification_Base_Type' ) ) return;

final class NF_WebMerge_Action extends NF_Notification_Base_Type
{
    /**
     * @var name
     */
    public $name = '';

    /**
     * @var options
     */
    public $options = array();

    /**
     * @var WebMerge
     */
    public $webmerge;

    /**
     * @var documents
     */
    public $documents = array();

    /**
     * @var fields
     */
    public $fields = array();



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->name = __( 'WebMerge', NF_WebMerge::TEXTDOMAIN );

        add_filter( 'nf_notification_types', array( $this, 'register_action_type' ) );

        add_action( 'wp_ajax_nf_webmerge_document_fields', array( $this, 'ajax_document_fields' ) );

        $this->options = get_option( "ninja_forms_webmerge" );

        if( $this->options['api_key'] && $this->options['api_secret'] ) {
            $this->webmerge = new WebMerge(
                $this->options['api_key'],
                $this->options['api_secret']
            );
        }
    }


    /**
     * Register Action Type
     *
     * @param $types
     * @return array
     */
    public function register_action_type( $types )
    {
        $types[ $this->name ] = $this;
        return (array) $types;
    }



    /**
     * Edit Screen
     *
     * @return void
     */
    public function edit_screen( $id = '' )
    {
        if( $this->webmerge ) {
            $this->documents = $this->webmerge->getDocuments();

            $settings['document'] = Ninja_Forms()->notification($id)->get_setting('document');

            foreach ($this->documents as $document) {
                $settings['fields'][$document['id']] = maybe_unserialize(Ninja_Forms()->notification($id)->get_setting($document['id']));
            }

            $form_fields = Ninja_Forms()->form($_GET['form_id'])->fields;

            if ($settings['document']) {
                $this->fields = $this->webmerge->getDocumentFields($settings['document']);
            }
        }

        include NF_WebMerge::$dir . 'includes/templates/action-webmerge.html.php';
    }



    /**
     * Process
     *
     * @param string $id
     * @return void
     */
    public function process( $id = '' )
    {
        global $ninja_forms_processing;

        $settings['document'] = Ninja_Forms()->notification( $id )->get_setting( 'document' );

        $document = $this->webmerge->getDocument( $settings['document'] );

        $settings['fields'] = maybe_unserialize( Ninja_Forms()->notification( $id )->get_setting( $document['id'] ) );

        $data = array();

        foreach( $settings['fields'] as $key => $value ){
            $data[ $key ] = $ninja_forms_processing->data['fields'][ $value ];
        }

        try{
        $merge = $this->webmerge->doMerge( $document['id'], $document['key'], $data  );
        } catch ( Exception $e ) {
            // This section intentionall left blank.
        }
    }




    /**
     * AJAX Document Fields
     */
    public function ajax_document_fields()
    {
        $document_id = intval( $_POST['document_id'] );

        $fields = $this->webmerge->getDocumentFields( $document_id );

        echo json_encode( $fields );

        wp_die();
    }
}

new NF_WebMerge_Action;

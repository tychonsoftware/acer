<?php if ( ! defined( 'ABSPATH' ) ) exit;

/*
 * Plugin Name: Ninja Forms - WebMerge
 * Plugin URI: https://ninjaforms.com/extensions/webmerge/
 * Description: Create Documents Faster with the Ninja Forms integration for WebMerge.
 * Version: 3.0.3
 * Author: The WP Ninjas
 * Author URI: http://wpninjas.com/
 * Text Domain: ninja-forms-webmerge
 *
 * Copyright 2016 The WP Ninjas.
 */

if( version_compare( get_option( 'ninja_forms_version', '0.0.0' ), '3', '<' ) || get_option( 'ninja_forms_load_deprecated', FALSE ) ) {

    include 'deprecated/ninja-forms-webmerge.php';

} else {

    include_once 'includes/Libraries/webmerge.class.php';

    /**
     * Class NF_WebMerge
     */
    final class NF_WebMerge
    {
        const VERSION = '3.0.3';
        const SLUG    = 'webmerge';
        const NAME    = 'WebMerge';
        const AUTHOR  = 'The WP Ninjas';
        const PREFIX  = 'NF_WebMerge';

        /**
         * @var NF_WebMerge
         * @since 3.0
         */
        private static $instance;

        /**
         * Plugin Directory
         *
         * @since 3.0
         * @var string $dir
         */
        public static $dir = '';

        /**
         * Plugin URL
         *
         * @since 3.0
         * @var string $url
         */
        public static $url = '';

        /**
         * API Connector
         *
         * @var
         */
        private $_api;

        /**
         * Main Plugin Instance
         *
         * Insures that only one instance of a plugin class exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @since 3.0
         * @static
         * @static var array $instance
         * @return NF_WebMerge Highlander Instance
         */
        public static function instance()
        {
            if (!isset(self::$instance) && !(self::$instance instanceof NF_WebMerge)) {
                self::$instance = new NF_WebMerge();

                self::$dir = plugin_dir_path(__FILE__);

                self::$url = plugin_dir_url(__FILE__);

                /*
                 * Register our autoloader
                 */
                spl_autoload_register(array(self::$instance, 'autoloader'));

                new NF_WebMerge_Admin_Settings();
            }
            return self::$instance;
        }

        public function __construct()
        {
            /*
             * Required for all Extensions.
             */
            add_action( 'admin_init', array( $this, 'setup_license') );
            
            /*
             * Optional. If your extension processes or alters form submission data on a per form basis...
             */
            add_filter( 'ninja_forms_register_actions', array($this, 'register_actions'));
        }

        /**
         * Optional. If your extension processes or alters form submission data on a per form basis...
         */
        public function register_actions($actions)
        {
            $actions[ 'webmerge' ] = new NF_WebMerge_Actions_WebMerge(); // includes/Actions/WebMergeExample.php

            return $actions;
        }

        /*
         * Optional methods for convenience.
         */

        public function autoloader($class_name)
        {
            if (class_exists($class_name)) return;

            if ( false === strpos( $class_name, self::PREFIX ) ) return;

            $class_name = str_replace( self::PREFIX, '', $class_name );
            $classes_dir = realpath(plugin_dir_path(__FILE__)) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
            $class_file = str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';

            if (file_exists($classes_dir . $class_file)) {
                require_once $classes_dir . $class_file;
            }
        }
        
        /**
         * Template
         *
         * @param string $file_name
         * @param array $data
         */
        public static function template( $file_name = '', array $data = array() )
        {
            if( ! $file_name ) return;

            extract( $data );

            include self::$dir . 'includes/Templates/' . $file_name;
        }

        /**
         * @return array
         */
        public function get_lists()
        {
            if( ! $this->api() ) return array();

            $response = $this->api()->getDocuments();

            $lists = array();

            if( ! is_array( $response ) ) return $lists;

            foreach( $response as $data ){

                Ninja_Forms()->update_setting( 'webmerge_list_' . $data[ 'id' ], $data );

                $lists[] = array(
                    'value'     => $data[ 'id' ],
                    'label'     => $data[ 'name' ],
                    'merge_key' => $data[ 'key' ],
                    'fields'    => $this->get_document_fields_list( $data['id'] ),
                );
            }
            return $lists;
        }

        /**
         * @param $list_id
         * @return array
         */
        public function get_document_fields_list( $list_id )
        {
            if( ! $this->api() ) return array();

            $response = $this->api()->getDocumentFields( $list_id );
            
            $fields = array(); 
            foreach( $response as $data ){

                $fields[] = array(
                    'value' => $data[ 'key' ] . '_' .  $list_id,
                    'label' => $data[ 'name' ],
                );
            }
            return $fields;
        }
        
        public function merge_documents( $id, $key, $data )
        {
            $merge = $this->api()->doMerge( $id, $key, $data  );

            return $merge;
        }

        /**
         * @return WebMerge
         * Connects to the WebMerge API.
         */
        public function api()
        {
            $api_key    =  trim( Ninja_Forms()->get_setting( 'webmerge_api_key' ) );

            $secret_api =  trim( Ninja_Forms()->get_setting( 'webmerge_api_secret' ) );

            $this->_api = new WebMerge( $api_key, $secret_api );

            return $this->_api;
        }
        
        /**
         * Config
         *
         * @param $file_name
         * @return mixed
         */
        public static function config( $file_name )
        {
            return include self::$dir . 'includes/Config/' . $file_name . '.php';
        }

        /*
         * Required methods for all extension.
         */

        public function setup_license()
        {
            if ( ! class_exists( 'NF_Extension_Updater' ) ) return;

            new NF_Extension_Updater( self::NAME, self::VERSION, self::AUTHOR, __FILE__, self::SLUG );
        }
    }

    /**
     * The main function responsible for returning The Highlander Plugin
     * Instance to functions everywhere.
     *
     * Use this function like you would a global variable, except without needing
     * to declare the global.
     *
     * @since 3.0
     * @return {class} Highlander Instance
     */
    function NF_WebMerge()
    {
        return NF_WebMerge::instance();
    }

    NF_WebMerge();
}

add_filter( 'ninja_forms_upgrade_action_WebMerge', 'NF_WebMerge_Upgrade_Action' );
function NF_WebMerge_Upgrade_Action( $action ){

    $document = $action[ 'document' ];

    $data = array(
        'active'   => $action[ 'active' ],
        'type'     => 'webmerge', // Convert type to lowercase
        'label'    => $action[ 'label' ],
        'newsletter_list' => $document,
    );

    $fields = maybe_unserialize( $action[ $document ] );

    foreach( $fields as $key => $value ){
        $data[ $key . '_' . $document ] = '{field:' . $value . '}';
    }

    return $data;
}

add_filter( 'ninja_forms_upgrade_settings', 'NF_WebMerge_Upgrade' );
function NF_WebMerge_Upgrade( $data ){

    $plugin_settings = get_option( 'ninja_forms_webmerge' );

    Ninja_Forms()->update_settings(
        array(
            'webmerge_api_key'  => $plugin_settings[ 'api_key' ],
            'webmerge_api_secret' => $plugin_settings[ 'api_secret' ],
        )
    );

    return $data;
}



=== Ninja Forms - WebMerge Extension ===
Contributors: kbjohnson90
Donate link: http://ninjaforms.com
Tags: form, forms
Requires at least: 4.6
Tested up to: 4.8
Stable tag: 3.0.3

License: GPLv2 or later

== Description ==
Create Documents Faster with the Ninja Forms integration for WebMerge.

== Screenshots ==

To see up to date screenshots, visit [NinjaForms.com](http://ninjaforms.com).

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the `ninja-forms-webmerge` directory to your `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Visit the 'Forms -> WebMerge' menu item in your admin sidebar
4. Add your API Key and API Secret
5. In the Ninja Forms Emails & Actions tab, you can now add a new action to merge your form submission using the WebMerge service.

== Use ==

For help and video tutorials, please visit our website: [Ninja Forms Documentation](http://docs.ninjaforms.com/)

== Changelog ==

= 3.0.3 (14 December 2017) =

*Bugs:*

* Fixed an issue that was causing submission sequence merge tag to not send any data.

= 3.0.2 (02 August 2017) =

*Bugs:*

* Fixed a bug that could throw PHP errors in version 2.9.x of Ninja Forms.

= 3.0.1 (06 September 2016 ) =

* Updated with Ninja Forms v3.x compatibility

= 3.0 (10 August 2016 ) =

* Updated with Ninja Forms v3.x compatibility
* Deprecated Ninja Forms v2.9.x compatible code

= 1.0.3 (30 July 2015) =

*Bugs:*

* Fixed a bug that caused saved values to not pre-populate.
* Fixed a bug that caused broke automatic updates.

= 1.0.2 (29 July 2015) =

*Bugs:*

* Fixed a bug that could cause a fatal error in Ninja Forms was deactivated.

= 1.0.1 (7 July 2015) =

* Initial Release
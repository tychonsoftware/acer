<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class NF_Webmerge_Admin_Settings
 */
final class NF_WebMerge_Admin_Settings
{
    public function __construct()
    {
        add_filter( 'ninja_forms_plugin_settings', array( $this, 'webmerge_plugin_settings'), 10, 1);
        add_filter( 'ninja_forms_plugin_settings_groups', array( $this, 'webmerge_plugin_settings_groups' ), 10, 1 );
        add_filter('ninja_forms_check_setting_webmerge_api_secret', array( $this, 'validate_ninja_forms_webmerge_api' ), 10 );
    }

    public function webmerge_plugin_settings($settings)
    {
        $settings[ 'webmerge' ] = NF_WebMerge()->config('PluginSettings');
        return $settings;
    }

    public function webmerge_plugin_settings_groups( $groups )
    {
        $groups = array_merge( $groups, NF_WebMerge()->config( 'PluginSettingsGroups' ) );
        return $groups;
    }
    
    public function validate_ninja_forms_webmerge_api( $setting )
    {
        if( ! class_exists( 'WebMerge' ) ) return $setting;

        $api_key    =  trim( Ninja_Forms()->get_setting( 'webmerge_api_key' ) );
        $secret_api =  trim( $setting[ 'value' ] );

        try {
            $validate_merge = new WebMerge( $api_key, $secret_api );
            $validate_merge->getDocuments();
        } catch( Exception $e ) {
            $setting[ 'errors' ][] = __( 'The WebMerge API key you have entered appears to be invalid.', 'ninja-forms-webmerge');
        }
        return $setting;
    }
}


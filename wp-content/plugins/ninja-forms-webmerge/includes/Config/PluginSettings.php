<?php if ( ! defined( 'ABSPATH' ) ) exit;

return apply_filters( 'nf_webmerge_plugin_settings', array(
    /*
    |--------------------------------------------------------------------------
    | API Key
    |--------------------------------------------------------------------------
    */

    'webmerge_api_key' => array(
        'id'    => 'webmerge_api_key',
        'type'  => 'textbox',
        'label' => __( 'API KEY', 'ninja-forms-webmerge' ),
        'desc'  => sprintf(
            __( 'Grab your %sAPI Key%s from your WebMerge Account.', 'ninja-forms-webmerge' ),
            '<a href="https://www.webmerge.me/manage/account?status=new_api_key&page=api" target="_blank">', '</a>'
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | API Secret
    |--------------------------------------------------------------------------
    */

    'webmerge_api_secret' => array(
        'id'    => 'webmerge_api_secret',
        'type'  => 'textbox',
        'label' => __( 'API Secret', 'ninja-forms-webmerge' ),
        'desc'  => sprintf(
            __( 'Grab your %sAPI Key%s from your WebMerge Account.', 'ninja-forms-webmerge' ),
            '<a href="https://www.webmerge.me/manage/account?status=new_api_key&page=api" target="_blank">', '</a>'
        ),
    ),
));

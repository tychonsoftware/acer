<?php if ( ! defined( 'ABSPATH' ) ) exit;

return apply_filters( 'nf_webmerge_plugin_settings_groups', array(

    'webmerge' => array(
        'id' => 'webmerge',
        'label' => __( 'WebMerge', 'ninja-forms-webmerge' ),
    ),

));

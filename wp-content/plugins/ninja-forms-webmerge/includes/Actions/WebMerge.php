<?php if ( ! defined( 'ABSPATH' ) || ! class_exists( 'NF_Abstracts_Action' )) exit;

/**
 * Class NF_Action_WebMerge
 */
final class NF_WebMerge_Actions_WebMerge extends NF_Abstracts_ActionNewsletter
{
    /**
     * @var string
     */
    protected $_name  = 'webmerge';

    /**
     * @var array
     */
    protected $_tags = array();

    /**
     * @var string
     */
    protected $_timing = 'late';

    /**
     * @var int
     */
    protected $_priority = '10';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->_nicename = __( 'WebMerge', 'ninja-forms-webmerge' );

        unset( $this->_settings[ $this->_name . 'newsletter_list_groups' ] );
    }

    /*
    * PUBLIC METHODS
    */

    public function save( $action_settings )
    {

    }

    public function process( $action_settings, $form_id, $data )
    {
        $id = $action_settings[ 'newsletter_list' ];

        $merge_keys = $this->get_merge_key( $action_settings, $id );
        
        $document =  Ninja_Forms()->get_setting( 'webmerge_list_' . $id );

        $merge_data = array();
        foreach( $merge_keys as $key => $value ){
            $merge_data[ $key ] = $value;
        }

        $send_merge = NF_WebMerge()->merge_documents( $document[ 'id' ], $document[ 'key' ], $merge_data );

        $data[ 'actions' ][ 'webmerge' ][ 'list' ] = $id;
        $data[ 'actions' ][ 'webmerge' ][ 'settings' ] = $action_settings;
        $data[ 'actions' ][ 'webmerge' ][ 'response' ] = $send_merge;

        return $data;
    }

    protected function get_merge_key( $action_settings, $list_id )
    {
        $merge_keys = array();
        foreach( $action_settings as $key => $value ){

            if( FALSE === strpos( $key, $list_id ) ) continue;

            $field = str_replace( '_' . $list_id  , '', $key );

            $merge_keys[ $field ] = $value;
        }

        return $merge_keys;
    }

    protected function get_lists()
    {
        return NF_WebMerge()->get_lists();
    }
}

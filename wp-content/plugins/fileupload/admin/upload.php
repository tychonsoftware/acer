<?php
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

if(isset($_REQUEST['action'])){
    
    switch ($_REQUEST['action']){
        
        case "new-upload" :
            Nuovo_Upload();
            break;
        case "add-upload" :
            $message=fu_save_file();
            $messageWithYear = explode("/",$message);
            error_log("message > " . print_r($message,1));
            if(!empty($messageWithYear[0])){
                $location = "?page=importazione&action=new-upload&uploadYear=".$messageWithYear[1] ;
                $location = add_query_arg( 'error', $message, $location );
            }else {
                $location = "?page=importazione&uploadYear=".$messageWithYear[1] ;
                $location = add_query_arg( 'success', 'Success', $location );
            }
            wp_redirect( $location );
            break;
        default:
            Nuovo_Upload();
            break;
    }
}
else {
    Nuovo_Upload();
}
unset($_REQUEST['action']);

function Nuovo_Upload(){
    global $per_page;
    $message = '';
    $success = '';
    if (isset($_REQUEST['error']) ){
        $success = '';
        if($_REQUEST['error'] == 'limiterror'){
            $message= "Il file caricato è superiore al limite massimo ".ini_get('upload_max_filesize');
        }else if(strpos($_REQUEST['error'], 'dberror_') !== false){
            $tokens = explode("_", $_REQUEST['error']);
            if(count($tokens) > 1){
                $etokens = explode("/", $tokens[1]);
                $message= "Connection could not be established to " . UPLOAD_DB_NAME . "_" .$etokens[0];
            }else {
                $message= "Connection could not be established to " . UPLOAD_DB_NAME;
            }
        }else if($_REQUEST['error'] == 'dberror'){
            $message= "Connection could not be established to " . UPLOAD_DB_NAME;
        }else if(!empty($_REQUEST['error'])){
            $message= 'È possibile allegare solo file in formato zip';
           
        }
    }
    if (isset($_REQUEST['success']) ){
        $message = '';
        if(!empty($_REQUEST['success'])){
            $success= 'File importato con successo';
        }
    }
    function remove_url_query($url, $key) {
        $url = preg_replace('/(?:&|(\?))' . $key . '=[^&]*(?(1)&|)?/i', "$1", $url);
        $url = rtrim($url, '?');
        $url = rtrim($url, '&');
        return $url;
    }
    ?>
    <style>
    table {  
        border-collapse: collapse;  
    }  
        .inline{   
            display: inline-block;   
            float: right;   
            margin: 20px 0px;   
        }   

   .pagination {   
        display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 20px;   
    }   
    .pagination a {   
        font-size:13px;   
        color: black;   
        float: left;   
        padding: 8px 16px;   
        text-decoration: none;   
        border:1px solid black;   
    }   
    .pagination a.active {   
            background-color: #018dc4;   
    }   
    .pagination a:hover:not(.active) {   
        background-color: skyblue;   
    }
    div#MetaDati {
        width: 72% !important;
        padding-top: 20px;
        padding-right: 15px;
        float: left;
        margin-top: 20px;
    }
    #MetaDati .inside {
        width: auto;
        display: inline-block;
    }
    #outer {
        width: auto !important;
        /* text-align: right; */
        display: inline-block;
        float: right;
    }
    .tbl-outer {
    width: 100%;
    font-size: 16px;
}
.tbl-outer-left {
    width: 77%;
    display: inline-block;
}
.tbl-outer-right {
    width: 22%;
    display: inline;
    vertical-align: top;
    float: right;
}
.tbl-outer-right-in {
    font-weight: 700;
    border: 1px solid #ddd;
    padding: 12px;
    box-sizing: border-box;
}
.TOR-heading {
    text-align: center;
    text-transform: uppercase;
}
.TOR-in-left {
    display: inline;
}
.TOR-in-right {
    display: inline;
    padding-left: 20px;
}
@media screen and (max-width:1180px){
    .tbl-outer,.tbl-outer-right-in  {
    font-size: 12px;
}
}
</style>
    <form id="addupload" method="post" action="?page=importazione" class="validate" enctype="multipart/form-data">
            <div id="message">
            <?php if(!empty($message)){ ?>
               <div class="error"><?php echo $message;?></div> <?php
            }
            ?>
            <?php 
            if (isset($_GET["pn"])) {    
                $page  = $_GET["pn"];    
                $success = '';
            }    
            else {    
              $page=1;    
            }  
            if(!empty($success)){ 
               // echo "<script>alert('File importato con successo')</script>";
               $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
               $newurl = remove_url_query($current_url, 'success');
               echo "<script>
              
               alert('File importato con successo');
               setTimeout(function(){ window.location.href='.$newurl' }, 100);</script>";

            }
            $per_page_record = $per_page;
              
            $start_from = ($page-1) * $per_page_record;
            $items = fu_get_zip_files($start_from, $per_page_record);
            error_log("Start from : " . $start_from . " and fetch " . $per_page_record);
            $Totalitems = total_fu_get_zip_files();

            $user = wp_get_current_user();
            ?>
               <!-- <div style="color:#4CAF50;"></div> -->
            </div>
		    <input type="hidden" name="action" value="add-upload" />
            <input type="hidden" name="id" value="<?php echo(int)(isset($_REQUEST['id'])?$_REQUEST['id']:0);?>" />
		    <input type="hidden" name="nuovoupload" value="<?php echo wp_create_nonce('nuovoupload')?>" />
            <div id="poststuff">
		        <div id="post-body" class="metabox-holder columns-2">
			        <div id="post-body-content">
                    <div  align="center" style="margin-top:10px">
                            <h2 class="wp-heading-inline">Lotti Fatture Caricati</h2> 
                        </div>
                        <div>
                        <div class="top-label-sel" style="display: table; margin: 0 auto;"> 
                        <?php
                            if(isset($_REQUEST['uploadYear'])){
                                $currently_selected = $_REQUEST['uploadYear'];
                            
                            }else{
                                $currently_selected = date('Y');
                            }
                            $earliest_year = 2019; 
                            $latest_year = date('Y')+70; 
                            
                            $totalFatture = total_fatture($currently_selected,'');
                            $totalUploadZip = total_zip_files($currently_selected,'');
                        ?>
                        <label style="padding-right: 15px;">Anno</label>
                        <select class="form-control" name="upload_year" id="UploadYear" style="margin-bottom: 40px;display: inline; width: auto;margin-top: 30px;">
                        <?php foreach ( range( $latest_year, $earliest_year ) as $i ) {?>
                            <option value="<?php echo $i?>" <?php echo ($i == $currently_selected ? ' selected="selected"' : '')?>><?php  echo $i ?></option>
                        <?php }?>
                        </select>
                        </div>
                        <div class="tbl-outer"><div class="tbl-outer-left">
                        <table class="wp-list-table widefat striped posts" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col" nowrap>Nome File Lotto</th>
                                    <th scope="col" nowrap>Data caricamento</th>
                                    <th scope="col" nowrap>Utente</th>
                                    <th scope="col" nowrap>Dipartimento</th>
                                    <th scope="col" nowrap>Numero fatture</th>
                                    <th scope="col" nowrap>Stato Lavorazione Lotto</th>
                                </tr>
                            </thead>
                            <tbody id="the-list">
                            <?php 
                                if(isset($items)){
                                    if ($items->num_rows > 0) {
                                        while($row = $items->fetch_assoc()) {?>
                                        <tr class="iedit author-self level-0 form-<?php echo $partyId; ?> type-post status-publish format-standard hentry category-annunci">
                                            <td align="center" width="30%">
                                                <?php echo $row["nome_file"]; ?>
                                            </td>
                                            <td align="center" width="5%">
                                                <?php 
                                                    $oDate = new DateTime($row["data"]);
                                                    echo $oDate->format("d/m/Y");
                                                ?> 
                                            </td>
                                            <td align="center" width="25%">
                                                <?php 
                                                    $user = get_user_by( 'id', $row["user_id"]);
                                                    echo $user->display_name;
                                                ?> 
                                            </td>
                                            <td align="center" width="5%">
                                                <?php echo $row["dipartimento"];?> 
                                            </td>
                                            <td align="center" width="2%">
                                                <?php echo $row["numero_fatture"];?> 
                                            </td>
                                            <td align="center" width="30%">
                                                <?php 
                                                    $stato = $row["stato"];
                                                    $stato_arr = fu_GetStatusLabel($stato);
                                                    if ($stato_arr->num_rows > 0) {
                                                        $stat_row = $stato_arr->fetch_assoc();
                                                        echo $stat_row['descrizione']; 
                                                    }
                                                    // if($stato == 0)
                                                    //     echo "File caricato";
                                                    // else if($stato == 10)
                                                    //     echo "Fatture importate";
                                                    // else if($stato == 4)
                                                    //     echo "Fatture importate";
                                                    // else if($stato == 20)
                                                    //     echo "Fatture inviate a CSA";
                                                ?> 
                                            </td>
                                        </tr>

            <?php
                                        }
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                        <div class="pagination">    
                            <?php  
                                $total_records = $Totalitems->num_rows;     
                            echo "</br>";     
                                // Number of pages required.   
                                $total_pages = ceil($total_records / $per_page_record); 
                                $pagLink = "";       
                                $removable_query_args = wp_removable_query_args();
                                $current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
                                $current_url = remove_query_arg( $removable_query_args, $current_url );
                                if($page >= 2){   
                                    echo "<a href='".esc_url( add_query_arg( 'pn', max( 1, $page - 1 ), $current_url ) )."'>  Precedente </a>";
                                }       
                                for ($i=1; $i<=$total_pages; $i++) {   
                                    if ($i == $page) { 
                                        $pagLink .= "<a class = 'active' href='".esc_url( add_query_arg( 'pn', $i, $current_url ) )."'>" . $i . " </a>";
                                    } 
                                    else  {   
                                        $pagLink .= "<a href='".esc_url( add_query_arg( 'pn', $i, $current_url ) )."'>" . $i . " </a>";
                                    }   
                                }; 
                                echo $pagLink;   
                                if($page<$total_pages){
                                    echo "<a href='".esc_url( add_query_arg( 'pn', $page+1, $current_url ) )."'>  Prossima </a>";
                                } 
                            ?>    
                            </div> 
                        </div>
                        <div class="tbl-outer-right">
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Totale</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'');?></div>

                            </div>
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Napoli-benevento</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'NAPOLI');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'NAPOLI');?></div>

                            </div>
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Caserta</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'CASERTA');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'CASERTA');?></div>

                            </div>
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Avellino</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'Avellino');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'Avellino');?></div>

                            </div>
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Salerno</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'Salerno');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'Salerno');?></div>
                            </div>
                            <div class="tbl-outer-right-in">
                                <div class="TOR-heading">Acer</div>
                                <div class="TOR-in-left">Lotti: <?php echo total_zip_files($currently_selected,'Acer');?></div>
                                <div class="TOR-in-right">Fatture: <?php echo total_fatture($currently_selected,'Acer');?></div>
                            </div>

                        </div>

                        <br/>
                        <div class="notewrap postbox center" id="MetaDati">
                            <div class="inside">
                            <?php include_once ( dirname (__FILE__) . '/allegati_edit.php' ); ?>
                            </div>
                            <div id="outer">
                                <div class="inner"> <input type="submit" name="SaveUpload" id="save_upload" value="IMPORTA LOTTO"></div>
                            </div>
                        </div>
                        
			        </div><!-- /post-body-content -->
                </div><!-- /post -->
            </div><!-- /poststuff -->
        </form>
        </div>
   
        
    <div class="wrap">
        <!-- <div class="HeadPage">
             <h2 class="wp-heading-inline">Import fatture</h2> 
             <div class="Obbligatori">
                <span style="color:red;font-weight: bold;">*</span> i campi contrassegnati dall'asterisco sono <strong>obbligatori</strong>
            </div>
            
        </div> -->

        
        
     
        <?php
}

function Lista_Upload(){
    if (isset($_REQUEST['p']))
        $Pag=$_REQUEST['p'];
    else
        $Pag=0;
    $user = get_current_user_id();
    echo' <div class="wrap">
            <div class="HeadPage">
                <h1 class="wp-heading-inline">Importazione fatture</h1>';
    echo'<a href="?page=importazione&amp;action=new-upload" class="add-new-h2">Aggiungi File Zip</a></h2>';
    ?>
    <?php
    }
?>

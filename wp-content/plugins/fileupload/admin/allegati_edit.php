<?php

if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
  die('You are not allowed to call this page directly.');
}
$existingAttachments = "";
if (isset($_REQUEST['id'])) {
    $IdAttachment = $_REQUEST['id'];
    $partiti = dt_get_all_partito($_REQUEST['id']);
    foreach ($partiti as $partito) {
        if(!empty($partito->image_path)){
          $existingAttachments .= '<li class="elemento" id="' . $partito->id .'"><input type="radio" id="r0" name="r0"/><label for="r0" ><span class="closeinput" onclick="deleteAttachment(' . $partito->id . ',-1);">x</span></label><a href="'.FU_URL . 'DowloadDocument.php?action=' . $partito->image_path.'" target="_blank"> <div class="crop"><img src="' . FU_URL . 'DowloadDocument.php?action=' . $partito->image_path .'"></div> </a><br/><p>Nome file ' . basename($partito->image_path) . ', Dimensione ' . dt_Formato_Dimensione_File(filesize($partito->image_path)) . '.</p></li>';
        }
          
          // $existingAttachments .= '<li class="elemento" id="' . $partito->id .'"><input type="radio" id="r0" name="r0"/><label for="r0" ><span class="closeinput" onclick="deleteAttachment(' . $partito->id . ',-1);">x</span></label><a href="'.DT_URL . 'DowloadDocument.php?action=' . $partito->image_path.'" target="_blank"><img src="' . DT_URL . 'img/image_icon.png' . '"></a><br/><p>File name ' . basename($partito->image_path) . ', file size ' . dt_Formato_Dimensione_File(filesize($partito->image_path)) . '.</p></li>';
    }
}
?>
<style>
  #pulSel {
    display: inline-block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    cursor: pointer;
    padding: 10px 20px;
    border: 1px solid #018dc4;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    font: normal normal bold 20px/normal "Times New Roman", Times, serif;
    color: rgba(255, 255, 255, 1);
    -o-text-overflow: clip;
    text-overflow: clip;
    background: #0199d9;
    -webkit-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  }

  #allegati {
    display: inline-block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    cursor: pointer;
    padding: 10px 20px;
    border: 1px solid #018dc4;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    font: normal normal bold 20px/normal "Times New Roman", Times, serif;
    color: #0073aa !important;
    -o-text-overflow: clip;
    text-overflow: clip;
    background: #fff;
    -webkit-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  }

  .elemento {
    display: block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    float: none;
    z-index: auto;
    width: auto;
    height: auto;
    position: static;
    cursor: default;
    opacity: 0.72;
    margin: 2px;
    padding: 4px;
    overflow: visible;
    border: 1px solid;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    font: normal 20px/2 "Times New Roman", Times, serif;
    color: black;
    -o-text-overflow: clip;
    text-overflow: clip;
    background: rgba(252, 252, 2, 0.7);
    -webkit-box-shadow: none;
    box-shadow: none;
    text-shadow: none;
    -webkit-transition: none;
    -moz-transition: none;
    -o-transition: none;
    transition: none;
    -webkit-transform: none;
    transform: none;
    -webkit-transform-origin: 50% 50% 0;
    transform-origin: 50% 50% 0;
  }

  .elemento input {
    width: 100%;
  }

  form .preview.postEdit {
    float: initial;
  }


  ol.list_sbar {    
    overflow: hidden;
    list-style: none;
}
ol.list_sbar li{
    position:relative;
}
ol.list_sbar li > div{
    clear:both;
    border-bottom: solid 1px #ddd;
    min-height:22px;
    line-height: 22px;
    display: block;
    text-decoration: none;
    background: #fff;
    outline: 0;
    font-weight: normal;
    padding: 4px 9px;
}
ol.list_sbar li:first-child {
    border-top: solid 1px #ddd;
}
ol.list_sbar li a {
    text-decoration: none;
    z-index: 33;
    color: #666;
    margin-bottom: 0;
    display:inline-block;
    outline: 0;
    float:left;
}
ol.list_sbar li > div:hover {
    color: #111;
    background: #FFF2BE;
    -moz-box-shadow: inset 0 0 1px #333;
    -webkit-box-shadow: inset 0 0 1px #333;
    box-shadow: inset 0 0 1px #333;
}
ol.list_sbar li input{
    display: none;
}
ol.list_sbar li label:hover{
    color: #f00;
}
ol.list_sbar li label{
    position: absolute; top: 50%;
    line-height: 22px;
    font-size: 18px;
    text-align: right;
    padding-right: 8px;    
    width: 20px;
    height: 100%;
    right: 0;
    color: #999;
    font-family:  calibri, sans-serif; 
    cursor: pointer;    
    transform: translateY(-50%); 
}
/* ol.list_sbar li input:checked + label{
    display: none;
} */
#MetaDati .inside{
  width:370px;
}
#attachmentList{
  margin-top:20px;
}

</style>
<?php 
?>
  <div>
  <label for="files" id="allegati">
    <span class="dashicons dashicons-upload" title="Allegati"></span> Allega file lotto fatture</label>
    <input style="padding: 0; height: 0px; display:none;" type="file" id="files" name="files[]" accept=".zip,.rar,.7zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="visibility: hidden;">
  <input type="hidden" id="removedfiles" name="removedfiles" value="">
</div>
<div class="preview postEdit">
</div>
<script>

jQuery( document ).ready(function($) {
  $('#UploadYear').on('change',function(){
    var url = window.location.href;
    var a = url.indexOf("?");
    var b =  url.substring(a);
    var c = url.replace(b,"");
    var uploadYear = $('#UploadYear').find(":selected").val();
    window.location.href=c+'?uploadYear='+uploadYear; 
  });

  var rids = new Array();
    function deleteAttachment(IdAttachment,uniqid) {
        if(IdAttachment != -1){
            rids.push(IdAttachment);
        var currentVal = document.getElementById("removedfiles").value;
        if(currentVal != ''){
            document.getElementById("removedfiles").value = currentVal + "," + IdAttachment;
        }else 
            document.getElementById("removedfiles").value = IdAttachment;
        removeElement(document.getElementById(IdAttachment));
        }else if(uniqid != -1){
            removeElement(document.getElementById(uniqid));
        }
        $('#files').val('');
    }
    function removeElement(element) {
        element && element.parentNode && element.parentNode.removeChild(element);
    }
    var input = document.querySelector('#files');
    var preview = document.querySelector('.preview');
    input.addEventListener('change', caricaDatiAllegati);
    var existinglist = <?php echo json_encode($existingAttachments); ?>;
    if(existinglist && existinglist.length > 0){
        var attachmentList = document.createElement('ol');
        attachmentList.setAttribute("id", "attachmentList");
        attachmentList.setAttribute("class", "list_sbar");
        attachmentList.innerHTML = existinglist;
        preview.appendChild(attachmentList);
    }
    function caricaDatiAllegati() {
      $("#message").empty();
      $(".error").remove();
      var curFiles = input.files;
    while (preview.firstChild) {
      preview.removeChild(preview.firstChild);
    }
   
    var list =document.createElement('ol');
    list.setAttribute("id", "attachmentList");
    list.setAttribute("class", "list_sbar");
    list.innerHTML = existinglist;
    preview.appendChild(list);

    for (var i = 0; i < curFiles.length; i++) {
      var uniqid = "<?php echo uniqid();?>";
      var icona = "<?php echo FU_URL; ?>" + 'img/zip.png';
      var listItem = document.createElement('li');
      listItem.className = "elemento";
      listItem.setAttribute("id",uniqid );
      var cinput = document.createElement('input');
      cinput.setAttribute("type", "radio");
      cinput.setAttribute("id", "r" + (i+1));
      cinput.setAttribute("name", "r" + (i+1));
      cinput.setAttribute("class", "closeinput");
      
      var clabel = document.createElement('label');
      clabel.setAttribute("for", "r" + (i+1));

      var span = document.createElement('span');
      span.setAttribute("onclick", "deleteAttachment(-1,'" + uniqid + "')");
      span.innerText = 'x';
      clabel.appendChild(span);
      listItem.appendChild(cinput);
      listItem.appendChild(clabel);
      
      var para = document.createElement('p');
      var des = document.createElement('input');
      des.setAttribute("type", "text");
      des.setAttribute("name", "Descrizione[" + i.toString() + "]");
      para.textContent = 'Nome file ' + curFiles[i].name + ', Dimensione ' + returnFileSize(curFiles[i].size) + '.';
      var div = document.createElement('div');
      div.setAttribute("class", "crop");
      var image = document.createElement('img');
      var a = document.createElement('a');
      var file = URL.createObjectURL(curFiles[i]);
      a.setAttribute("href", file);
      a.setAttribute("target", "_blank");
      image.setAttribute("src"," <?php echo FU_URL?>img/zip.png");
      div.appendChild(image);
      a.appendChild(div);
      listItem.appendChild(a);
      listItem.appendChild(document.createElement("br"));
      listItem.appendChild(para);
      listItem.appendChild(des);
      list.appendChild(listItem);
    }
    for (var i = 0; i < rids.length; i++) {
      removeElement(document.getElementById( rids[i]));
    }
}
function returnFileSize(number) {
    if (number < 1024) {
      return number + 'bytes';
    } else if (number > 1024 && number < 1048576) {
      return (number / 1024).toFixed(1) + 'KB';
    } else if (number > 1048576) {
      return (number / 1048576).toFixed(1) + 'MB';
    }
}
   
});
   
</script>

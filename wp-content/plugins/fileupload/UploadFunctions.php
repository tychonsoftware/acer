<?php
/**
 * Functions necessary to the plugin
 * @package    Upload
 */
DEFINE('DS', DIRECTORY_SEPARATOR); 
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }
################################################################################
// Funzioni 
################################################################################
function fu_save_file(){
	$uploadYear=$_POST["upload_year"];
	$target_path = '';
	$numPartiti=count($_FILES["files"]['name']);
	$messages = null;
	$valid_zip_file = true;
	for($i=0;$i<$numPartiti;$i++){
		if (isset($_FILES["file"]) && isset($_FILES["file"]["size"]) && (($_FILES["file"]["size"] / 1024)/1024)<1){
			$DimFile=number_format($_FILES["files"]["size"][$i] / 1024,2);
			$UnitM=" KB";
		}else{
			$DimFile=number_format(($_FILES["files"]["size"][$i] / 1024)/1024,2);	
			$UnitM=" MB";
		}
		$dime= "Dimensione: " . $DimFile . " ".$UnitM;
		if ($_FILES['files']['tmp_name'][$i]==''){
			//$messages[4]= "Fine non selezionato Oppure operazione annullata";
			$messages= "limiterror";
		}else{
			if (($DimFile>(int)ini_get('upload_max_filesize')) and ($UnitM==" MB")){
				$messages= "Il file caricato &egrave; di ".$DimFile." Mb, il limite massimo &egrave; di ".ini_get('upload_max_filesize')." Mb";
			}else{
				if ($_FILES["files"]["error"][$i] > 0){
					$messages= "Errore: " . $_FILES["file"]["error"][$i];
				}else{
					$department_key = DEPARTMENT_KEY;
					$user = wp_get_current_user();
					$departimento = esc_attr(get_user_meta($user->ID,$department_key, true));
					$destination_path = constant(strtoupper($departimento).'_FILES_REPO');
					$result = 0;
					if (!file_exists($destination_path)) {
						mkdir($destination_path, 0777, true);
					}
					$finfo = finfo_open( FILEINFO_MIME_TYPE );
					$mtype = finfo_file( $finfo, $_FILES['files']['tmp_name'][$i] );
					$zip_file_name = basename(sanitize_file_name(remove_accents ( $_FILES['files']['name'][$i])));
					$target_path = fu_UniqueFileName($destination_path . '/' . $zip_file_name);
					error_log("MIME  " . $mtype);
					if (strpos($mtype, 'zip') !== false) {
						if(@move_uploaded_file($_FILES['files']['tmp_name'][$i],$target_path)){
							$serverName = UPLOAD_DB_HOST;
							$isuploaddbconnected = false;
							$isdbconnectissue = false;
							$mysqli = null;
							if(isset($serverName) && ($serverName != null)){
								try {
									$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.'_'.$uploadYear);
									if ($mysqli -> connect_errno) {
										$isuploaddbconnected = false;
										$isdbconnectissue =  true;

									}else {
										$isuploaddbconnected = true;
									}
								} catch (Exception $e) {
									$isuploaddbconnected = false;
									$isdbconnectissue =  true;
								}
							}
							if($isuploaddbconnected === true){
								$zip = new ZipArchive();
								$res = $zip->open($target_path, ZipArchive::CHECKCONS);
								$file_count = $zip->numFiles;
								if ($res !== TRUE) {
									switch($res) {
										case ZipArchive::ER_NOZIP:
											$messages = 'not a zip archive';
											$valid_zip_file = false;
											break;	
										case ZipArchive::ER_INCONS :
											$messages = 'consistency check failed';
											$valid_zip_file = false;
											break;	
										case ZipArchive::ER_CRC :
											$messages = 'checksum failed';
											$valid_zip_file = false;
											break;
									}
								}else {
									
									$zip->extractTo($destination_path);
									$date = date("Y-m-d H:i:s");
									$sql = "INSERT INTO upload_zip (user_id, data,dipartimento,nome_file,numero_fatture,stato)
									VALUES ($user->ID,'$date','$departimento','$zip_file_name',$file_count,0)";
									if ($mysqli->query($sql) === TRUE) {
										$upload_zip_id = $mysqli -> insert_id;

										for ($i = 0; $i < $zip->numFiles; $i++) {
											$filename = $zip->getNameIndex($i);
											$file_path = quotemeta($destination_path. DS . $filename);
											$sql = "INSERT INTO fattura (user_id, date,nome_file_zip,
											dipartimento,path_file_dip,upload_zip_id,stato)

											VALUES ($user->ID,'$date','$filename','$departimento','$file_path',$upload_zip_id,0)";
											if ($mysqli->query($sql) === TRUE) {

											}else {
												error_log($sql . ">>>>> " . $mysqli->error);
												$messages= "dberror";
											}
										}
									} else {
										error_log($sql . ">>>>> " . $mysqli->error);
										$messages= "dberror";
									}

									$zip->close();
									if (file_exists($target_path)) {
										unlink($target_path);
									}
								}
								if(!$valid_zip_file){
									if (file_exists($target_path)) {
										unlink($target_path);
									}
								}
							}else {
								if($isdbconnectissue){
									$messages= "dberror_" . $uploadYear;
								}else {
									$messages= "dberror";
								}
								
							}
						} 
						else{
							$messages= "Il File non caricato: " .str_replace("\\","/",$target_path)."%25%25br%25%25 Errore:".print_r($_FILES['files']['error'],1);
						}
					}else if( $mtype == ( "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ) || 
						$mtype == ( "application/vnd.ms-excel" ) ||
						$mtype == ( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" )) {
							
							if(@move_uploaded_file($_FILES['files']['tmp_name'][$i],$target_path)){
								$serverName = UPLOAD_DB_HOST;
								$isuploaddbconnected = false;
								$isdbconnectissue = false;
								$mysqli = null;
								
								if(isset($serverName) && ($serverName != null)){
									try {
										$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.'_' . $uploadYear);
										if ($mysqli -> connect_errno) {
											$isuploaddbconnected = false;
											$isdbconnectissue =  true;
										}else {
											$isuploaddbconnected = true;
										}
									} catch (Exception $e) {
										$isdbconnectissue =  true;
										$isuploaddbconnected = false;
									}
								}

								if($isuploaddbconnected === true){
									$date = date("Y-m-d H:i:s");
									$file_path = quotemeta($target_path);
									$sql = "INSERT INTO upload_zip (user_id, data,dipartimento,nome_file,path_file,stato)
									VALUES ($user->ID,'$date','$departimento','$zip_file_name','$file_path',0)";
									if ($mysqli->query($sql) === TRUE) {
									
									} else {
										error_log($sql . ">>>>> " . $mysqli->error);
										$messages= "dberror";
									}
								}else {
									if($isdbconnectissue){
										$messages= "dberror_" . $uploadYear;
									}else {
										$messages= "dberror";
									}
								}
								
							}else{
								$messages= "Il File non caricato: " .str_replace("\\","/",$target_path)."%25%25br%25%25 Errore:".print_r($_FILES['files']['error'],1);
							}
					}
				}
			}
		}
	}

	return $messages.'/'.$uploadYear;
}

function fu_UniqueFileName($filename,$inc=0){
	$baseName=$filename;
	while (file_exists($filename)){
		$inc++;
		$filename=substr($baseName,0,strrpos($baseName,".")).$inc.substr($baseName,strrpos($baseName,"."),strlen($baseName)-strrpos($baseName,"."));
	}
	return $filename;	
}

function total_fatture($currently_selected,$dept=''){
	$uploadYear = '_'.$currently_selected;
	$serverName = UPLOAD_DB_HOST;
	$isuploaddbconnected = false;
	$mysqli = null;
	
	if(isset($serverName) && ($serverName != null)){
		try {
			$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.$uploadYear);
			
			if ($mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
	}
	if($isuploaddbconnected === true){
		
		if($dept!=''){
			$sql = "SELECT count(*) AS counts FROM fattura where dipartimento='".$dept."'";
		}else{
			$sql = "SELECT count(*) AS counts FROM fattura";
		}
		
		$res = $mysqli->query($sql);
		$count_result = $res->fetch_assoc();
		return $count_result['counts'];
	}else 
		return 0;
}

function total_zip_files($currently_selected,$dept=''){
	$uploadYear = '_'.$currently_selected;
	$serverName = UPLOAD_DB_HOST;
	$isuploaddbconnected = false;
	$mysqli = null;
	
	if(isset($serverName) && ($serverName != null)){
		try {
			$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.$uploadYear);
			
			if ($mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
	}
	if($isuploaddbconnected === true){
		if($dept!=''){
			$sql = "SELECT count(*) AS counts FROM upload_zip where dipartimento='".$dept."'";
		}else{
			$sql = "SELECT count(*) AS counts FROM upload_zip";
			
		}
		$res = $mysqli->query($sql);
		$count_result = $res->fetch_assoc();
		return $count_result['counts'];
	}else 
		return 0;
}

function total_fu_get_zip_files(){
	global $wpdb;
	$current_year = date('Y');
	if(isset($_REQUEST['uploadYear']) && !empty($_REQUEST['uploadYear'])){
		$uploadYear = '_'.$_REQUEST['uploadYear'];
	}else{
		$uploadYear = '_'.$current_year;
	}
	$user = wp_get_current_user();
	$serverName = UPLOAD_DB_HOST;
	$isuploaddbconnected = false;
	$mysqli = null;
	if(isset($serverName) && ($serverName != null)){
		try {
			$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.$uploadYear);
			if ($mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
	}
	if($isuploaddbconnected === true){
		$department_key = DEPARTMENT_KEY;
		$departimento = esc_attr(get_user_meta($user->ID,$department_key, true));
		//$sql = "SELECT * FROM upload_zip where dipartimento='" . $departimento . "'";
		$sql = "SELECT * FROM upload_zip ";
		return $mysqli->query($sql);
	}else 
		return null;
}

function fu_get_zip_files($start_from, $per_page_record){
	global $wpdb;
	$current_year = date('Y');
	if(isset($_REQUEST['uploadYear'])){
		$uploadYear = '_'.$_REQUEST['uploadYear'];
	}else{
		$uploadYear = '_'.$current_year;
	}
	$user = wp_get_current_user();
	$serverName = UPLOAD_DB_HOST;
	$isuploaddbconnected = false;
	$mysqli = null;
	if(isset($serverName) && ($serverName != null)){
		try {
			$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.$uploadYear);
			
			if ($mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
	}
	if($isuploaddbconnected === true){
		$department_key = DEPARTMENT_KEY;
		$departimento = esc_attr(get_user_meta($user->ID,$department_key, true));
		$sql = "SELECT * FROM upload_zip where 1=1 LIMIT ".$start_from.",".$per_page_record."";
		error_log("fu_get_zip_files query :" . $sql);
		return $mysqli->query($sql);
	}else 
		return null;
}

function fu_GetStatusLabel($statusId){
    $serverName = UPLOAD_DB_HOST;
    $isuploaddbconnected = false;
    $mysqli = null;
    if(isset($serverName) && ($serverName != null)){
		try {
			$mysqli = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME);
			if ($mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
    }
    if($isuploaddbconnected === true){
        global $wpdb;
        $sql = "SELECT descrizione FROM upload_zip_state WHERE stato=" . $statusId;
        return $mysqli->query($sql);
    }
    return null;
}

add_action('wp_head','hide_upload_menu');

function hide_upload_menu() { 
    if ( is_user_logged_in() ) {
    } else {
		$output="<style> .showtologin { display: none; } </style>";
		echo $output;
	}
}

function remove_core_updates(){
	global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}
	add_filter('pre_site_transient_update_core','remove_core_updates');
	add_filter('pre_site_transient_update_plugins','remove_core_updates');
	add_filter('pre_site_transient_update_themes','remove_core_updates');
	add_shortcode('CustomUpload', 'VisualizzaFileUpload');
	function VisualizzaFileUpload($Parametri)
	{
		$ret = "";
		$Parametri = shortcode_atts(array('type' => '1','per_page'=>10), $Parametri, "CustomUpload");
		$GLOBALS['per_page'] = $Parametri['per_page'];
		require_once(dirname(__FILE__) . '/admin/upload.php');
		return $ret;
	}

	
function wpse_remove_edit_post_link( $link ) {
	return '';
}
add_filter('edit_post_link', 'wpse_remove_edit_post_link');	
?>
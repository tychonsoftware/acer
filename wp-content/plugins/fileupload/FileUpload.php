<?php
/**
 * @wordpress-plugin
 * Plugin Name:       File Upload
 * Description:       Upload Zip File
 * Version:           1.00
 * Text Domain:       upload
 */

if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/UploadFunctions.php');	
define("FU_URL", plugin_dir_url(dirname(__FILE__) . '/FileUpload.php'));

if (!class_exists('Importazione')) {
    class Importazione
    {
        var $version;
        var $minium_WP   = '3.1';
        
        function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->load_dependencies();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('FileUpload', 'activate'));
            register_deactivation_hook(__FILE__, array('FileUpload', 'deactivate'));
            register_uninstall_hook(__FILE__, array('FileUpload', 'uninstall'));
            //add_action('admin_enqueue_scripts', array(&$this, 'FileUpload_Enqueue_Scripts'));
            add_action('wp_enqueue_scripts', array(&$this, 'FileUpload_Enqueue_Scripts'));
            //add_action('admin_menu', array(&$this, 'add_menu'));
            
        }

       

        function load_dependencies()
        {
            if (is_admin()) {
                require_once(dirname(__FILE__) . '/admin/admin.php');
            }
        }


        static function add_menu()
	    {
            include_once(ABSPATH . 'wp-includes/pluggable.php');
            add_menu_page('Panoramica', 'Importazione', 'administrator', 'importazione', array('Importazione', 'show_menu'),FU_URL . "img/logo.png");
        }

        static function show_menu()
	    {
            global $FU_OnLine;
            switch ($_REQUEST['page']) {
                case "importazione":
                    include_once(dirname(__FILE__) . '/admin/upload.php');
            }
        }

       

        static function init()
	    {

        }
        static function screen_option()
	    {
            
        }

        static function dizionari_set_option($status, $option, $value)
        {
        }

        static function FileUpload_Enqueue_Scripts($hook_suffix)
		{

            $path = plugins_url('', __FILE__);
            wp_enqueue_script('upload', $path . '/js/upload.js');
            wp_register_style('FileUpload', $path . '/css/style.css');
            wp_enqueue_style('FileUpload');
        }
        
        static function activate()
        {
            global $wpdb;
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);
        }

        
        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
        }
        static function uninstall()
        {
        }
    }
    global $FU_OnLine;
    $FU_OnLine = new Importazione();
}

?>
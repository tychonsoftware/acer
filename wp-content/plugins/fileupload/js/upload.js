jQuery( document ).ready(function($) {

  
    $('#save_upload').click(function () {
        var yearinput = $('#UploadYear').val();
        if( yearinput == '' ){
            $("#message").empty();
            $("#message").html('<div class="error">Seleziona l`anno per la fattura</div>');
            return false;
        }
        var input = document.querySelector('#files');
        if( input.files.length == 0 ){
            $("#message").empty();
            $("#message").html('<div class="error">Allegare il file con le fatture</div>');
            return false;
        }
        $('body').append('<div id="requestOverlay" class="request-overlay"> <div id="text">Importazione del file in corso</div></div>');
        $("#requestOverlay").show();/*Show overlay*/
    });
   
});
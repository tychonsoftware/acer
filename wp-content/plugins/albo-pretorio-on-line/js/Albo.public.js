function format ( d ) {
	return '<table cellpadding="1" cellspacing="0" border="0" style="padding-left:5px;font-size:0.9em;font-weight: bold;">'+
		'<tr>'+
			'<td style="width:5%;text-align: right;">Ente:</td>'+
			'<td>'+d[2]+'</td>'+
		'</tr>'+
		'<tr>'+
			'<td style="text-align: right;">Riferimento:</td>'+
			'<td>'+d[3]+'</td>'+
		'</tr>'+
		'<tr>'+
			'<td style="text-align: right;">Categoria:</td>'+
			'<td>'+d[6]+'</td>'+
		'</tr>'+
	'</table>';
}
jQuery(document).ready(function($){

		$.datepicker.regional['it'] = {
			closeText: 'Chiudi', // set a close button text
			currentText: 'Oggi', // set today text
			monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',   'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'], // set month names
			monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic'], // set short month names
			dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'], // set days names
			dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'], // set short day names
			dayNamesMin: ['Do','Lu','Ma','Me','Gio','Ve','Sa'], // set more short days names
			dateFormat: 'dd/mm/yy' // set format date
		};
		
		$.datepicker.setDefaults($.datepicker.regional['it']);
		$('#SearchDataDa').datepicker({
			dateFormat: 'dd/mm/yy', setDate: $(this).attr('value')
		});
		$('#SearchDataDa').datepicker({
			dateFormat: 'dd/mm/yy', setDate: $(this).attr('value')
		});
		$('#SearchDataA').datepicker({
			dateFormat: 'dd/mm/yy', setDate: $(this).attr('value')
		});
		$('#paginazione').change(function(){
				location.href=$(this).attr('rel')+$('#paginazione option:selected').text();
		});
		$('#Calendario1').datepicker({dateFormat : 'dd/mm/yy'});
		$('#Calendario2').datepicker({dateFormat : 'dd/mm/yy'});
		$('a.addstatdw').click(function() {
				jQuery.ajax({type: 'get',url: $(this).attr('rel')}); //close jQuery.ajax
			return true;		 
			});
	$('#pp-tabs-container').tabs();
	$('#fe-tabs-container').tabs();
	$('#maxminfiltro').on('click',function(){
		if($('#maxminfiltro').attr('class')==='s'){
			$('#fe-tabs-container').hide();
			$('#maxminfiltro').attr('class','h');
			$('#maxminfiltro').html('<span class=\"dashicons dashicons-filter\"></span> Apri Ricerca atti mediante filtri');
			
		}else{
			$('#fe-tabs-container').show();
			$('#maxminfiltro').attr('class','s');
			$('#maxminfiltro').html('<span class=\"dashicons dashicons-filter\"></span> Chiudi Ricerca atti mediante filtri');
		}
	});
	$('a.numero-pagina').click(function(){
		if(location.href.indexOf('fascicolo-personale') > -1){
			location.href=$(this).attr('href');
		}else
			location.href=$(this).attr('href')+'&vf='+$('#maxminfiltro').attr('class')+'#dati';
		return false;
	});
}); 

jQuery(document).on('mouseenter', ".itip", function () {
	var $this = $(this);
	if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
		$this.tooltip({
			title: $this.text(),
			placement: "top"
		});
		$this.tooltip('show');
	}
});
jQuery('.hideText').css('width',$('.hideText').parent().width());
jQuery(document).ready(function ($) {

	var action = getUrlVars()["action"];
	$('#color').wpColorPicker();
	$('#colorp').wpColorPicker();
	$('#colord').wpColorPicker();
	$('#Calendario1').datepicker({
		dateFormat: 'dd/mm/yy'
	});
	$('#Calendario2').datepicker({ dateFormat: 'dd/mm/yy' });

	$('#Calendario4').datepicker({ dateFormat: 'dd/mm/yy' });
	$('#CalendarioIP').datepicker({
		dateFormat: 'dd/mm/yy', setDate: $(this).attr('value'), minDate: 0
	});
	var pubDate = $('#CalendarioIP').datepicker("getDate");
	if(typeof pubDate.getDate !== "undefined"){
		pubDate.setDate(pubDate.getDate() + 1);
		$('#Calendario3').datepicker({ dateFormat: 'dd/mm/yy', minDate: pubDate });
		if(action && action === 'new-atto')
			$('#Calendario3').datepicker('setDate', pubDate);
	}

	$('#CalendarioIP').change(function () {
		var pubDate = $('#CalendarioIP').datepicker("getDate");
		pubDate.setDate(pubDate.getDate() + 1);
		$('#Calendario3').datepicker('setDate', pubDate);
		$("#Calendario3").datepicker("option", "minDate", pubDate);
	});
	
	$('#CalendarioMO').datepicker({ dateFormat: 'dd/mm/yy', setDate: $(this).attr('value'), maxDate: "0D" });
	$('#setta-def-data-o').click(function () {
		$('#Calendario4').datepicker('setDate', $(this).attr('name'));
	});
});

function getUrlVars() {
    var vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
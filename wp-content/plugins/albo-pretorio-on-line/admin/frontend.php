<?php
/**
 * Gestione FrontEnd.
 * @link       http://www.eduva.org
 * @since      4.1.95
 *
 * @package    ALbo On Line
 */

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

include_once(dirname (__FILE__) .'/frontend_filtro.php');
if(isset($_REQUEST['action'])){
	switch ($_REQUEST['action']){
        case 'printatto':
            if (is_numeric($_REQUEST['id'])) {
                if ($_REQUEST['pdf'] == 'c') {
                    StampaAtto($_REQUEST['id'], 'c');
                } elseif ($_REQUEST['pdf'] == 'a') {
                    StampaAtto($_REQUEST['id'], 'a');
                }
            }
            break;
		case 'visatto':
			if(is_numeric($_REQUEST['eid']))
				VisualizzaAtto($_REQUEST['eid']);
			else{
				$ret=Lista_Atti($Parametri);
			}
				
			break;
		case 'addstatall':
			if(is_numeric($_GET['id']) and is_numeric($_GET['idAtto']))
				ap_insert_log(6,5,(int)$_GET['id'],"Download",(int)$_GET['idAtto']);
			break;
		
		// case 'visualizzfascicolo':

		// 	echo '<style type="text/css">';
		// 	//echo '.wp-list-table { width: 1000px !important; }';
		// 	echo '.wp-list-table .NomeDocumento { white-space: nowrap;}';
		// 	echo '.wp-list-table .toggle-row {display: none;}';
		// 	echo 'tr:nth-child(even) {background-color: #f2f2f2}';
		// 	echo '.wp-list-table .DataDocumento { width: 10%; }';
		// 	echo '.wp-list-table .Oggetto {  white-space: nowrap;}';
		// 	echo '.wp-list-table .Document { width: 20%; }';
		// 	echo '.wp-list-table th {text-align: center !important;}';
		// 	echo '.wp-list-table td {text-align: center !important;margin: 10px; padding: 5px;}';
		// 	echo '.wp-list-table tfoot{ display:none; }';
		// 	echo '.header {display: none;}';
		// 	//echo 'table:has(> tbody > tr.no-items) { border-spacing: 0 !important;width: 100% !important;clear: both !important;margin: 0 !important;}';
		// 	echo '</style>';
		// 	echo ' <div class="wrap"> <div class="heading atti-title"><h3>Fascicolo Utente</h3> </div> </div>';
			
		// 	$ret= Lista_Fascicolo();

		// 	break;
		default: 
			if (isset($_REQUEST['filtra'])){			
			 		$ret=Lista_Atti($Parametri,$_REQUEST['categoria'],(int)$_REQUEST['numero'],(int)$_REQUEST['anno'], htmlentities($_REQUEST['oggetto']),htmlentities($_REQUEST['DataInizio']),htmlentities($_REQUEST['DataFine']), htmlentities($_REQUEST['riferimento']),$_REQUEST['ente']);
			}else if(isset($_REQUEST['annullafiltro'])){
					 unset($_REQUEST['categoria']);
					 unset($_REQUEST['numero']);
					 unset($_REQUEST['anno']);
					 unset($_REQUEST['oggetto']);
					 unset($_REQUEST['riferimento']);
					 unset($_REQUEST['DataInizio']);
					 unset($_REQUEST['DataFine']);
					 unset($_REQUEST['ente']);
					 $ret=Lista_Atti($Parametri);
				}else{
					$ret=Lista_Atti($Parametri);
				}
		}	
	}else{
		if (isset($_REQUEST['filtra'])){
	 		$ret=Lista_Atti($Parametri,$_REQUEST['categoria'],(int)$_REQUEST['numero'],(int)$_REQUEST['anno'], htmlentities($_REQUEST['oggetto']),htmlentities($_REQUEST['DataInizio']),htmlentities($_REQUEST['DataFine']), htmlentities($_REQUEST['riferimento']),$_REQUEST['ente']);			
		}else if(isset($_REQUEST['annullafiltro'])){
		 unset($_REQUEST['categoria']);
		 unset($_REQUEST['numero']);
		 unset($_REQUEST['anno']);
		 unset($_REQUEST['oggetto']);
		 unset($_REQUEST['riferimento']);
		 unset($_REQUEST['DataInizio']);
		 unset($_REQUEST['ente']);
		 $ret=Lista_Atti($Parametri);
	}else{
		if(isset($Parametri['stato']) && $Parametri['stato'] == 10){
			unset($_REQUEST['vf']);
			echo '<style type="text/css">';
			//echo '.wp-list-table { width: 1000px !important; }';
			echo '.wp-list-table .NomeDocumento { white-space: nowrap;}';
			echo '.wp-list-table .toggle-row {display: none;}';
			echo 'tr:nth-child(even) {background-color: #f2f2f2}';
			echo '.wp-list-table .DataDocumento { width: 10%; }';
			echo '.wp-list-table .Oggetto {  white-space: nowrap;}';
			echo '.wp-list-table .Document { width: 20%; }';
			echo '.wp-list-table th {text-align: center !important;}';
			echo '.wp-list-table td {text-align: center !important;margin: 10px; padding: 5px;}';
			echo '.wp-list-table tfoot{ display:none; }';
			echo '.header {display: none;}';
			//echo 'table:has(> tbody > tr.no-items) { border-spacing: 0 !important;width: 100% !important;clear: both !important;margin: 0 !important;}';
			echo '</style>';
			echo ' <div class="wrap"> <div class="heading atti-title"><h3>Area Documenti</h3> </div> </div>';
			
			$ret= Lista_Fascicolo($Parametri);
		}else {
			if(!isset($Parametri)){
				$Parametri = array();
			}
			$ret=Lista_Atti($Parametri);
		}
		

	}
}

function VisualizzaAtto($id){
	$risultato=ap_get_atto($id);
	$risultato=$risultato[0];
	$risultatocategoria=ap_get_categoria($risultato->IdCategoria);
	$risultatocategoria=$risultatocategoria[0];
	$allegati=ap_get_all_allegati_atto($id);
	$responsabile=ap_get_responsabile($risultato->RespProc);
	$responsabile=$responsabile[0];
	ap_insert_log(5,5,$id,"Visualizzazione");
	$coloreAnnullati=get_option('opt_AP_ColoreAnnullati');
	if($risultato->DataAnnullamento!='0000-00-00')
		$Annullato='<p style="background-color: '.$coloreAnnullati.';text-align:center;font-size:1.5em;">Atto Annullato dal Responsabile del Procedimento<br /><br />Motivo: <span style="font-size:1;font-style: italic;">'.stripslashes($risultato->MotivoAnnullamento).'</span></p>';
	else
		$Annullato='';
echo '
<div class="Visalbo">
<button class="h" onclick="window.location.href=\''.$_SERVER['HTTP_REFERER'].'\'"><span class="dashicons dashicons-controls-back"></span> Torna alla Lista</button>';
/* <h3>Dati atto </h3> */
echo '<p>'.$Annullato.'</p>
<div class="table-tabVisalbo-wrapper"><table class="tabVisalbo">
	    <tbody id="dati-atto">';
		/*
		<tr>
			<th>Ente titolare dell\'Atto</th>
			<td style="font-style: italic;font-size: 1.5em;vertical-align: middle;">'.stripslashes(ap_get_ente($risultato->Ente)->Nome).'</td>
		</tr>
		<tr>
			<th>Numero Albo</th>
			<td style="vertical-align: middle;">'.$risultato->Numero."/".$risultato->Anno.'</td>
		</tr>
		*/
		get_tipologia_id($risultato->Tipologia);
		
			$Tipo_data=get_option( 'opt_ap_Tipologia' );  
			$Tipo_data=json_decode($Tipo_data);
			foreach($Tipo_data as $tiplology){
			    
						if($tiplology->Codice==$risultato->Tipologia){
						echo "<tr>
			<th>Tipologia</th>
			<td style='vertical-align: middle;'>$tiplology->Funzione</td>
		</tr>";

							
						}
			}
			
echo   '
		<tr>
			<th style="text-transform: none !important">Numero e Data</th>
			<td style="vertical-align: middle;">'.stripslashes($risultato->Riferimento).'</td>
		</tr>
		<tr>
			<th style="text-transform: none !important">Oggetto</th>
			<td style="vertical-align: middle;">'.stripslashes($risultato->Oggetto).'</td>
		</tr>
		<tr>
			<th style="text-transform: none !important">Data inizio pubblicazione</th>
			<td style="vertical-align: middle;">'.ap_VisualizzaData($risultato->DataInizio).'</td>
		</tr>
		<tr>
		<tr>
			<th style="text-transform: none !important">Data fine pubblicazione</th>
			<td style="vertical-align: middle;">'.ap_VisualizzaData($risultato->DataFine).'</td>
		</tr>
		<tr>
			<th style="text-transform: none !important">Data aggiornamento</th>
			<td style="vertical-align: middle;">'.ap_VisualizzaData($risultato->Ultimamodifica).'</td>
		</tr>
		<tr>
			<th style="text-transform: none !important">Descrizione</th>
			<td style="vertical-align: middle;">'.stripslashes($risultato->Informazioni).'</td>
		</tr>';
		 /* <tr>
			<th>Data oblio</th>
			<td style="vertical-align: middle;">'.ap_VisualizzaData($risultato->DataOblio).'</td>
		</tr> 21-12-18*/ 
		
		/*
		<tr>
			<th>Categoria</th>
			<td style="vertical-align: middle;">'.stripslashes($risultatocategoria->Nome).'</td>
		</tr>';
$MetaDati=ap_get_meta_atto($id);
if($MetaDati!==FALSE){
	$Meta="";
	foreach($MetaDati as $Metadato){
		$Meta.="{".$Metadato->Meta."=".$Metadato->Value."} - ";
	}
	$Meta=substr($Meta,0,-3);
		echo'
				<tr>
					<th>Meta Dati</th>
					<td style="vertical-align: middle;">'.$Meta.'</td>
				</tr>';
}
*/
echo' 	    </tbody>
	</table></div>';
$Soggetti=unserialize($risultato->Soggetti);
$Soggetti=ap_get_alcuni_soggetti_ruolo(implode(",",$Soggetti));
$Ruolo=""; ?>

	
	
	<?php
		$Uffici=unserialize($risultato->Uffici);
		$Ana_Soggetti=ap_get_responsabili();
		
					$TabResponsabili=get_option('opt_AP_TabResp');
	if($TabResponsabili){
		$TR=json_decode($TabResponsabili);
	}else{
		$TR=json_decode('[{"ID":"","Funzione":"","Display":"No"}]');
	}
	
	if($Soggetti || is_array($Uffici)){
		echo "		<h3 style=\"text-align:left;\">Riferimenti</h3>";
	}
	
	?>
				
<?php	
		foreach($TR as $uffi){
			if(is_array($Uffici) And in_array($uffi->ID,$Uffici)){
			?>
	<div class="Visallegato">		
		<table class="tabVisResp">
	    		<tbody>
				<tr>
					<th>Settore </th>
					<td style="vertical-align: middle;"><?php echo $uffi->Funzione; ?></td>
				</tr>		
			    </tbody>
		</table>
	</div>
			<?php	}
		}
?>	

<?php
foreach($Soggetti as $Soggetto){
	if(ap_get_Funzione_Responsabile($Soggetto->Funzione,"Display")=="No"){
		continue;
	}
	if($Soggetto->Funzione!=$Ruolo And $Ruolo!=""){
		echo '</div>';
	}
	if($Soggetto->Funzione!=$Ruolo){
	/*	echo '<h4>'.ap_get_Funzione_Responsabile($Soggetto->Funzione,"Descrizione").'</h4>'; */
	echo '<div class="Visallegato">';
	}
	$Ruolo=$Soggetto->Funzione;
	echo'		<table class="tabVisResp">
	    		<tbody>
				<tr>
					<th>Persona</th>
					<td style="vertical-align: middle;">'.$Soggetto->Cognome." ".$Soggetto->Nome.'</td>
				</tr>';
	if ($Soggetto->Email)
	echo'		<tr>
					<th>email</th>
					<td style="vertical-align: middle;"><a href="mailto:'.$Soggetto->Email.'">'.$Soggetto->Email.'</a></td>
				</tr>';
	if ($Soggetto->Telefono)
	echo'			<tr>
					<th>Telefono</th>
					<td style="vertical-align: middle;">'.$Soggetto->Telefono.'</td>
				</tr>';
	if ($Soggetto->Orario)
	echo'		<tr>
					<th>Orario ricevimento</th>
					<td style="vertical-align: middle;">'.$Soggetto->Orario.'</td>
				</tr>';
	if ($Soggetto->Note)
	echo'
				<tr>
					<th>Note</th>
					<td style="vertical-align: middle;">'.$Soggetto->Note.'</td>
				</tr>';
echo'
			    </tbody>
			</table>';
}
if($Ruolo!=""){
	echo '</div>';
}
if (count($allegati)) {
    echo '<h3>Allegati</h3>';
}
//print_r($_SERVER);
$TipidiFiles=ap_get_tipidifiles();
foreach ($allegati as $allegato) {
	$Estensione=ap_ExtensionType($allegato->Allegato);
	echo '<div class="Visallegato">
			<div class="Allegato">';
	if(isset($allegato->TipoFile) and $allegato->TipoFile!="" and ap_isExtensioType($allegato->TipoFile)){
		$Estensione=ap_ExtensionType($allegato->TipoFile);
		echo '<img src="'.$TipidiFiles[$Estensione]['Icona'].'" alt="'.$TipidiFiles[$Estensione]['Descrizione'].'" height="30" width="30"/>';
	}else{
		echo '<img src="'.$TipidiFiles[strtolower($Estensione)]['Icona'].'" alt="'.$TipidiFiles[strtolower($Estensione)]['Descrizione'].'" height="30" width="30"allegato/>';
	}
	echo '</div>
			<div>
				<p>
					'.strip_tags($allegato->TitoloAllegato);
			if (strpos(get_permalink(),"?")>0)
				$sep="&amp;";
			else
				$sep="?";
			if (is_file($allegato->Allegato))
				echo '        <a href="'.ap_DaPath_a_URL($allegato->Allegato).'" class="addstatdw" rel="'.get_permalink().$sep.'action=addstatall&amp;id='.$allegato->IdAllegato.'&amp;idAtto='.$id.'" target="_blank">'. basename( $allegato->Allegato).'</a> ('.ap_Formato_Dimensione_File(filesize($allegato->Allegato)).')<br />'.htmlspecialchars_decode($TipidiFiles[strtolower($Estensione)]['Verifica']).' <a href="'.get_permalink().$sep.'action=dwnalle&amp;id='.$allegato->IdAllegato.'&amp;idAtto='.$id.'" >Scarica allegato</a>';
				
			else
				echo basename( $allegato->Allegato)." File non trovato, il file &egrave; stato cancellato o spostato!";
echo'				</p>
			</div>
			<div style="clear:both;"></div>
		</div>
		';
	}
echo '
</div>
';	
}

function Lista_Atti($Parametri,$Categoria=0,$Numero=0,$Anno=0,$Oggetto='',$Dadata=0,$Adata=0,$Riferimento='',$Ente=-1){
	$TitoloAtti="";
	if(isset($Parametri['stato'])){
		switch ($Parametri['stato']){
				case 0:
					$TitoloAtti="Tutti gli Atti";
					break;
				case 1:
					$TitoloAtti="Atti in corso di Validit&agrave;";
					break;
				case 2:
					$TitoloAtti="Atti Scaduti";
					break;
				case 3:
					$TitoloAtti="Atti da Pubblicare";
					break;
		}
	}
	if (isset($Parametri['per_page'])){
		$N_A_pp=$Parametri['per_page'];	
	}else{
		$N_A_pp=10;
	}
	if (isset($Parametri['evidenza'])){
		$evidenza=$Parametri['evidenza'];	
	}else{
		$evidenza=0;
	}
	$pageTitle = '';
	
	if (isset($Parametri['cat']) and $Parametri['cat']!=0){
		$DesCategorie="";
		$Categoria="";
		$Categorie=explode(",",$Parametri['cat']);
		foreach($Categorie as $Cate){
			$DesCat=ap_get_categoria($Cate);
			$DesCategorie.=$DesCat[0]->Nome.",";
			$Categoria.=$Cate.",";
		}
		$DesCategorie= substr($DesCategorie,0, strlen($DesCategorie)-1);
		$TitoloAtti.=" Categorie ".$DesCategorie;
		$pageTitle = $DesCategorie;
		$Categoria=substr($Categoria,0, strlen($Categoria)-1);
		$cat=1;
	}else{
		$Categorie=$Categoria;
		$cat=0;
	}
	if (!isset($_REQUEST['Pag'])){
		$Da=0;
		$A=$N_A_pp;
	}else{
		$Da=($_REQUEST['Pag']-1)*$N_A_pp;
		$A=$N_A_pp;
	}
	if (!isset($_REQUEST['ente'])){
         $Ente = '-1';
	}else{
        $Ente = $_REQUEST['ente'];
	}
	
	if (isset($Parametri['filtri_personalizzati']) and $Parametri['filtri_personalizzati'] == "si" and 
		isset($_REQUEST['action']) and $_REQUEST['action'] == 'search-atti') {
			
			if(isset($_REQUEST['search_cat']) && $_REQUEST['search_cat'] > 0){
				$Categorie = $_REQUEST['search_cat'];
			}
			if(isset($_REQUEST['search_oggetto'])){
				$Oggetto = $_REQUEST['search_oggetto'];
			}
			if(isset($_REQUEST['search_riferimento'])){
				$Riferimento = $_REQUEST['search_riferimento'];
			}
			if(isset($_REQUEST['search_data_da'])){
				$Dadata = $_REQUEST['search_data_da'];
			}
			if(isset($_REQUEST['search_data_a'])){
				$Adata = $_REQUEST['search_data_a'];
			}
			$TotAtti=ap_get_all_atti($Parametri['stato'],$Numero,$Anno,$Categorie,$Oggetto,$Dadata,$Adata,'',0,0,true,true,$Riferimento,$Ente,1);
			$lista=ap_get_all_atti($Parametri['stato'],$Numero,$Anno,$Categorie,$Oggetto,$Dadata,$Adata,'Anno DESC,Numero DESC',$Da,$A,false,true,$Riferimento,$Ente,1); 
	}else {
		$obj = (isset($Oggetto)?$Oggetto:""); 
		$rif = (isset($Riferimento)?$Riferimento:"");
		$stato = (isset($Parametri['stato'])?$Parametri['stato']:0);
		$TotAtti=ap_get_all_atti($stato,$Numero,$Anno,$Categorie,$obj,$Dadata,$Adata,'',0,0,true,true,$rif,$Ente);
		$lista=ap_get_all_atti($stato,$Numero,$Anno,$Categorie,$obj,$Dadata,$Adata,'Anno DESC,Numero DESC',$Da,$A,false,true,$rif,$Ente); 
	}
	
	$titEnte=get_option('opt_AP_LivelloTitoloEnte');
	if ($titEnte=='')
		$titEnte="h2";
	$titPagina=get_option('opt_AP_LivelloTitoloPagina');
	if ($titPagina=='')
		$titPagina="h3";
	$coloreAnnullati=get_option('opt_AP_ColoreAnnullati');
	$colorePari=get_option('opt_AP_ColorePari');
	$coloreDispari=get_option('opt_AP_ColoreDispari');
	$VisFiltro="";
	if(isset($Parametri['minfiltri']) And $Parametri['minfiltri']=="si"){
		if(isset($_REQUEST['vf']) and  $_REQUEST['vf']=="s"){
			$VisFiltro='<button id="maxminfiltro" class="s"><span class="dashicons dashicons-filter"></span> Chiudi Ricerca atti mediante filtri</button>';
		}else{
//			$VisFiltro='<img src="'.Albo_URL.'img/maximize.png" id="maxminfiltro" class="h" alt="icona massimizza finestra filtri"/>';
			$VisFiltro='<button id="maxminfiltro" class="h"><span class="dashicons dashicons-filter"></span> Apri Ricerca atti mediante filtri</button>';
		}
	}
$Contenuto='';
$Contenuto.=' <div class="Visalbo">
<a name="dati"></a> ';
if (get_option('opt_AP_VisualizzaEnte')=='Si')
		$Contenuto.= '<'.$titEnte.' ><span  class="titoloEnte">'.stripslashes(get_option('opt_AP_Ente')).'</span></'.$titEnte.'>';
        $char = explode(',',$TitoloAtti);
       
    $ht ='<ul class="cat-list0s">';
	foreach($char as $val) {
		$ht .= "<li>".$val ."</li>";
	}
 
	$ht .= "</ul>";
		if($evidenza == 1) {
			//$Contenuto.= '<div class="atti-title" ><h3></h3></div>';
		} else {
			//$Contenuto.= '<div class="atti-title" ><h3>'.$pageTitle.'</h3></div>';
		}
//$Contenuto.='<'.$titPagina.'><span  class="titoloPagina">'. $ht.'</span></'.$titPagina.'>';
if (!isset($Parametri['filtri']) Or $Parametri['filtri']=="si"){
	$stato = isset($Parametri['stato'])?$Parametri['stato']:0;
	$Contenuto.='<h4 class="filtri">'.$VisFiltro.'</h4>'.VisualizzaRicerca($stato,$Categoria,isset($Parametri['minfiltri'])?$Parametri['minfiltri']:"");
}
//$Contenuto.=  $nascondi;

$FEColsOption=get_option('opt_AP_ColonneFE',array(
									"Data"=>0,
									"Ente"=>0,
									"Riferimento"=>0,
									"Oggetto"=>0,
									"Validita"=>0,
									"Categoria"=>0,
									"Note"=>0,
									"RespProc"=>0,
									"DataOblio"=>0));
if(!is_array($FEColsOption)){
	$FEColsOption=shortcode_atts(array(
				"Data"=>0,
				"Ente"=>0,
				"Riferimento"=>0,
				"Oggetto"=>0,
				"Validita"=>0,
				"Categoria"=>0,
				"Note"=>0,
				"RespProc"=>0,
				"DataOblio"=>0), json_decode($FEColsOption,TRUE),"");
}	

if (isset($Parametri['filtri_personalizzati']) and $Parametri['filtri_personalizzati'] == "si") {
	/* Search Section Start*/
	$searched_categorie = 0;
	$searched_oggetto = '';
	$searched_riferimento = '';
	$searched_data_da = '';
	$searched_data_a = '';
	if(isset($_REQUEST['action']) and $_REQUEST['action'] == 'search-atti') {
		if(isset($_REQUEST['search_cat'])){
			$searched_categorie = $_REQUEST['search_cat'];
		}
		if(isset($_REQUEST['search_oggetto'])){
			$searched_oggetto = $_REQUEST['search_oggetto'];
		}
		if(isset($_REQUEST['search_riferimento'])){
			$searched_riferimento = $_REQUEST['search_riferimento'];
		}
		if(isset($_REQUEST['search_data_da'])){
			$searched_data_da = $_REQUEST['search_data_da'];
		}
		if(isset($_REQUEST['search_data_a'])){
			$searched_data_a = $_REQUEST['search_data_a'];
		}
	}
	$Contenuto.= '<form id="search-atti" action="?action=search-atti" method="post"><div class="tabalbo"><div class="atti-list search-panel">';
	//$Contenuto.= '<div class="tabalbo"><div class="atti-list search-panel">';
	$Contenuto.= '<div class="row"><label>Oggetto</label><input class="oggetto-inp" name="search_oggetto" value="' . $searched_oggetto . '"/></div>';
	//$Contenuto.= '<div class="row"><label>Oggetto</label><input class="oggetto-inp" /></div>';
	//$Contenuto.= '<div class="row"><label>Piferimento</label><input/><label class="p-l-2">Categoria</label><select><option value="">Nessuna</option></select></div>';
	$Contenuto.= '<div class="row"><label>Numero e Data Atto</label><input name="search_riferimento" value="' . $searched_riferimento . '"/>';
	$Contenuto.= '<label class="p-l-2">Categoria</label>';
	$Contenuto.=  ap_get_dropdown_categorie('search_cat','search_cat','searchpostform','',$searched_categorie, false); 
	$Contenuto.= '</div>';
	$Contenuto.= '<div class="row"><label class="m-t-10">Da Data Pubblicazione</label><input id="SearchDataDa" name="search_data_da" value="' . $searched_data_da . '"/>
	<label class="p-l-2 m-t-10">A Data Pubblicazione</label><input id="SearchDataA" name="search_data_a" value="' . $searched_data_a . '"/></div>';
	$Contenuto.= '<div class="btn-row"><button class="btn btn-sm btn-primary">Cerca</button></div>';
	$Contenuto.= '</div></div>';
	$Contenuto.= '</form>';
	/* Section End*/
}
$Contenuto.= '	<div class="tabalbo">';                               
/* 		$Contenuto.= '	<table id="elenco-atti-OldStyle" class="tabella-dati-albo" summary="atti validi per riferimento, oggetto e categoria"> 
	    <caption>Atti</caption>
		<thead>
	    	<tr>
	        	<th scope="col">Prog.</th>';
foreach($FEColsOption as $Opzione => $Valore){
		if($Valore==1){
			$Contenuto.= '			<th scope="col">'.$Opzione.'</th>';
		}
}
echo'	</tr>
	    </thead>
	    <tbody>'; */
	    $CeAnnullato=false;
	if ($lista){

		$num_col = 3;
		if (isset($Parametri['num_col'])) {
			$num_columns = $Parametri['num_col'];
			if(!empty($num_columns) && $num_columns > 0 && $num_columns < 4){
				$num_col = $num_columns;
			}
		}
	
	 
	 	$pari=true;
		if (strpos(get_permalink(),"?")>0)
			$sep="&amp;";
		else
			$sep="?";
		foreach($lista as $riga){
			$Link='<a href="'.get_permalink().$sep.'action=visatto&amp;eid='.$riga->IdAtto.'"  style="text-decoration: underline;">';
			$categoria=ap_get_categoria($riga->IdCategoria);
			$cat=$categoria[0]->Nome;
			//$responsabileprocedura=ap_get_responsabile($riga->RespProc);
			//$respproc=$responsabileprocedura[0]->Cognome." ".$responsabileprocedura[0]->Nome;
			$NumeroAtto=ap_get_num_anno($riga->IdAtto);
	//		Bonifica_Url();
			$classe='';
			if ($pari And $coloreDispari) 
				$classe='style="background-color: '.$coloreDispari.';"';
			if (!$pari And $colorePari)
				$classe='style="background-color: '.$colorePari.';"';
			$pari=!$pari;
			if($riga->DataAnnullamento!='0000-00-00'){
				$classe='style="background-color: '.$coloreAnnullati.';"';
				$CeAnnullato=true;
			}
			$show_row = 1;
			$meta_dat = ap_get_elenco_attimeta("Array","","","",$riga->IdAtto);
			$evidenza_value = "";
			$image_value = "";
			foreach($meta_dat as $key=>$value)
			{
				if($value->Meta == '_evidenza')
				{
					$evidenza_value = $value->Value;
				}

				if($value->Meta == '_thumb')
				{
					$image_value = $value->Value;
				}
			}

			if($evidenza == 1) {
				if($evidenza_value == 1) {
					$show_row = 1;
				} else {
					$show_row = 0;
				}
			}
			if($show_row == 1) {
				if(isset($_REQUEST['soggetti']) && !empty($_REQUEST['soggetti'])) {
					$risultato=ap_get_atto($riga->IdAtto);
					$risultato=$risultato[0];
					$Soggetti=unserialize($risultato->Soggetti);
					$Soggetti=ap_get_alcuni_soggetti_ruolo(implode(",",$Soggetti));
					foreach($Soggetti as $Soggetto){
						if($Soggetto->Funzione == $_REQUEST['soggetti']) {
							$show_row = 1;
							break;
						} else {
							$show_row = 0;
						}
					}					
				}
			}
			if($show_row == 1) {
				if($image_value != -1) {
					$thumb_id = $image_value;
					$image_attributes = wp_get_attachment_image_src( $thumb_id );
				} else {
					$thumb_id = '';
					$image_attributes = '';
				}
			$Contenuto.= '<div class="atti-list' . ($num_col != 2 ? " no-mr" : "") . '" style="width: ' . intdiv(95, $num_col) . '%">';
			//$Contenuto.= '<div class="atti-list" >';
			if($image_attributes!="")
				$Contenuto.= '<div class="atti-fur-img"><img src="'.$image_attributes[0].'" /></div>';
			else
				$Contenuto.= '<div class="atti-fur-img"><img src="" /></div>';
			
			$acTitle = stripslashes($riga->Riferimento);
			if (strlen($acTitle) >= 20) {
				$acTitle = substr($acTitle, 0, 20). "...";
			}
			$actInfo = stripslashes($riga->Oggetto);
			if (strlen($actInfo) >= 80) {
				$actInfo = substr($actInfo, 0, 80). "...";
			}
			$actInfo = stripslashes($riga->Oggetto);
			$riferimento = stripslashes($riga->Riferimento);
			$Contenuto.= '<div class="atti-info"><strong>'.$Link;
			$Tipo_data=get_option( 'opt_ap_Tipologia' );  
			$Tipo_data=json_decode($Tipo_data);
			foreach($Tipo_data as $tiplology){
				if($tiplology->Codice==$riga->Tipologia){
					$Contenuto.= $tiplology->Funzione;
				}
			}
			
			$Contenuto.='</a><br/><span class="attdateOne">' .$Link.ap_VisualizzaData($riga->DataInizio) . '</span></a><span class="attdateOne" style="margin-left:25%"> '.$Link.ap_VisualizzaData($riga->DataFine) .'</span></strong><p style="font-size: 13px !important;">'.$Link.$riferimento.'</a></p><p style="font-size: 13px !important;">'.$Link.$actInfo.'</a></p></div></div>';
			
			//$Contenuto.= '<div class="atti-info"><strong>'.$Link.get_tipologia_id($riga->Tipologia).'</a><span class="attdate"> '.$Link.ap_VisualizzaData($riga->Ultimamodifica) .'</span></strong><p>'.$Link.$actInfo.'</a></p></div></div>';
			/*
			$Contenuto.= '<td '.$classe.'>'.$Link.$NumeroAtto.'/'.$riga->Anno .'</a>
					</td>';
			if ($FEColsOption['Data']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->Data) .'</a>
					</td>';
			if ($FEColsOption['Ente']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.$Link.stripslashes(ap_get_ente($riga->Ente)->Nome) .'</a>
					</td>';
			if ($FEColsOption['Riferimento']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Riferimento) .'</a>
					</td>';
			if ($FEColsOption['Oggetto']==1)
				$Contenuto.='			
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Oggetto) .'</a>
					</td>';
			if ($FEColsOption['Validita']==1)
				$Contenuto.='								
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->DataInizio) .'<br />'.ap_VisualizzaData($riga->DataFine) .'</a>  
					</td>';
			if ($FEColsOption['Categoria']==1)
				$Contenuto.='								
					<td '.$classe.'>
						'.$Link.$cat .'</a>  
					</td>';
			if ($FEColsOption['Note']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Informazioni) .'</a>
					</td>';
			if ($FEColsOption['RespProc']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.$respproc .'</a>
					</td>';	
			if ($FEColsOption['DataOblio']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->DataOblio) .'</a>
					</td>';
					*/
/* 		$Contenuto.='</tr>';  */
			}
			}
	} else {
/* 			$Contenuto.= '<tr>
					<td colspan="6">Nessun Atto Codificato</td>
				  </tr>'; */
	}

/* 	$Contenuto.= '
     </tbody>
    </table>'; */
$Contenuto.= '<div class="clearfix"></div>';
if ($TotAtti>$N_A_pp){
	$Para='';
	foreach ($_REQUEST as $k => $v){
		if ($k!="Pag" and $k!="vf")
			if ($Para=='')
				$Para.=$k.'='.$v;
			else
				$Para.='&amp;'.$k.'='.$v;
	}
	if ($Para=='')
		$Para="?Pag=";
	else
		$Para="?".$Para."&amp;Pag=";
	$Npag=(int)($TotAtti/$N_A_pp);
	if ($TotAtti%$N_A_pp>0){
		$Npag++;
	}

	//<p><strong>N. Atti '.$TotAtti.'</strong>&nbsp;&nbsp; Pagine';
	$Contenuto.= ' 
	<div class="tablenav" style="float:right;" id="risultati">
	<div class="tablenav-pages">
	<p>Pagine';
	if (isset($_REQUEST['Pag']) And $_REQUEST['Pag']>1 ){
		$Pagcur=$_REQUEST['Pag'];
		$PagPre=$Pagcur-1;
			$Contenuto.= '&nbsp;<a href="'.$Para.'1" class="page-numbers numero-pagina" title="Vai alla prima pagina">&laquo;</a>
&nbsp;<a href="'.$Para.$PagPre.'" class="page-numbers numero-pagina" title="Vai alla pagina precedente">&lsaquo;</a> ';
	}else{
		$Pagcur=1;
		$Contenuto.= '&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&laquo;</span>
&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&lsaquo;</span> ';
	}
	$Contenuto.= '&nbsp;<span class="page-numbers current">'.$Pagcur.'/'.$Npag.'</span>';
	$PagSuc=$Pagcur+1;
	   if ($PagSuc<=$Npag){
		$Contenuto.= '&nbsp;<a href="'.$Para.$PagSuc.'" class="page-numbers numero-pagina" title="Vai alla pagina successiva">&rsaquo;</a>
&nbsp;<a href="'.$Para.$Npag.'" class="page-numbers numero-pagina" title="Vai all\'ultima pagina">&raquo;</a>';
	}else{
		$Contenuto.= '&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>
&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>';			
	}
$Contenuto.='			</p>
	</div>
</div>';
}

$Contenuto.='</div>';
	/* if ($CeAnnullato) 
		$Contenuto.= '<p>Le righe evidenziate con questo sfondo <span style="background-color: '.$coloreAnnullati.';">&nbsp;&nbsp;&nbsp;</span> indicano Atti Annullati</p>'; */
$Contenuto.= '</div><!-- /wrap -->	';
return $Contenuto;
}

function Lista_Fascicolo($Parametri){
	$per_page=10;
	if (isset($Parametri['per_page'])){
		$per_page=$Parametri['per_page'];	
	}

	if (!isset($_REQUEST['Pag'])){
		$Da=0;
		$A=$per_page;
	}else{
		$Da=($_REQUEST['Pag']-1)*$per_page;
		$A=$per_page;
	}

	$user = get_current_user_id();

	$fascicle_id = uf_get_fascicle_id($user)[0]->Id;
	$total_items = uf_get_all_documents($fascicle_id,true,0,0);
	$lista = uf_get_all_documents($fascicle_id,false,$Da,$A);

	$Contenuto='';
	$Contenuto.=' <div class="Visalbo">';
	$Contenuto.= '<div class="tabalbo">';   
	
	if ($lista){
		foreach($lista as $fascicle){
			$Contenuto.= '<div class="atti-list" >';
			$Contenuto.= '<div class="atti-fur-img"><a class="vfpdf" href="' . UF_URL . 'DowloadDocument.php?action='. $fascicle->Path.'"><img src="'. UF_URL . 'img/Pdf.png" alt="File Pdf" height="30" width="30"allegato/></a></div>';
			$Contenuto.= '<div class="atti-info"><strong><div class="itip hidetext">' . $fascicle->Subject . '</div>';
			$Contenuto.='<span class="attdate">' . date('d/m/Y', strtotime($fascicle->Date)) .'</span>';
			if($fascicle->Protocol_Number){
				$Contenuto.='</strong><p>N. Protocollo: ' . $fascicle->Protocol_Number . '</p>';
			}else {
				$name = $fascicle->Name;
				$name_token = explode(".pdf",$name);
				$tokens = explode("-",$name_token[0]);
				$sub_id = end($tokens);
				$protocol_number = get_post_meta( $sub_id, '_field_105', true );
				if($protocol_number)
					$Contenuto.='</strong><p>N. Protocollo: ' . $protocol_number . '</p>';
			}
			$Contenuto.='</div></div>';
		}
	}
	$Contenuto.= '<div class="clearfix"></div>';
	if ($total_items>$per_page){
		$Para='';
		foreach ($_REQUEST as $k => $v){
			if ($k!="Pag" and $k!="vf")
				if ($Para=='')
					$Para.=$k.'='.$v;
				else
					$Para.='&amp;'.$k.'='.$v;
		}
		if ($Para=='')
			$Para="?Pag=";
		else
			$Para="?".$Para."&amp;Pag=";
		
		$Npag=(int)($total_items/$per_page);
		if ($total_items%$per_page>0){
			$Npag++;
		}
		$Contenuto.= ' 
		<div class="tablenav" style="float:right;" id="risultati">
		<div class="tablenav-pages">
		<p>Pagine';
		if (isset($_REQUEST['Pag']) And $_REQUEST['Pag']>1 ){
			$Pagcur=$_REQUEST['Pag'];
			$PagPre=$Pagcur-1;
			$Contenuto.= '&nbsp;<a href="'.$Para.'1" class="page-numbers numero-pagina" title="Vai alla prima pagina">&laquo;</a>
			&nbsp;<a href="'.$Para.$PagPre.'" class="page-numbers numero-pagina" title="Vai alla pagina precedente">&lsaquo;</a> ';
		}else{
			$Pagcur=1;
			$Contenuto.= '&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&laquo;</span>
				&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&lsaquo;</span> ';
		}
		$Contenuto.= '&nbsp;<span class="page-numbers current">'.$Pagcur.'/'.$Npag.'</span>';
		$PagSuc=$Pagcur+1;
		if ($PagSuc<=$Npag){
			$Contenuto.= '&nbsp;<a href="'.$Para.$PagSuc.'" class="page-numbers numero-pagina" title="Vai alla pagina successiva">&rsaquo;</a>
			&nbsp;<a href="'.$Para.$Npag.'" class="page-numbers numero-pagina" title="Vai all\'ultima pagina">&raquo;</a>';
		}else{
			$Contenuto.= '&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>
			&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>';			
		}
		$Contenuto.='			</p>
		</div>
		</div>';
	}
	$Contenuto.='</div></div>';
	return $Contenuto;
}
function Lista_Atti_Home($Parametri,$Categoria=0,$Numero=0,$Anno=0,$Oggetto='',$Dadata=0,$Adata=0,$Riferimento='',$Ente=-1){
	switch ($Parametri['stato']){
			case 0:
				$TitoloAtti="Tutti gli Atti";
				break;
			case 1:
				$TitoloAtti="Atti in corso di Validit&agrave;";
				break;
			case 2:
				$TitoloAtti="Atti Scaduti";
				break;
			case 3:
				$TitoloAtti="Atti da Pubblicare";
				break;
	}
	if (isset($Parametri['per_page'])){
		$N_A_pp=$Parametri['per_page'];	
	}else{
		$N_A_pp=10;
	}
	if (isset($Parametri['evidenza'])){
		$evidenza=$Parametri['evidenza'];	
	}else{
		$evidenza=0;
	}
	$pageTitle = '';
	if (isset($Parametri['cat']) and $Parametri['cat']!=0){
		$DesCategorie="";
		$Categoria="";
		$Categorie=explode(",",$Parametri['cat']);
		foreach($Categorie as $Cate){
			$DesCat=ap_get_categoria($Cate);
			$DesCategorie.=$DesCat[0]->Nome.",";
			$Categoria.=$Cate.",";
		}
		$DesCategorie= substr($DesCategorie,0, strlen($DesCategorie)-1);
		$TitoloAtti.=" Categorie ".$DesCategorie;
		$pageTitle = $DesCategorie;
		$Categoria=substr($Categoria,0, strlen($Categoria)-1);
		$cat=1;
	}else{
		$Categorie=$Categoria;
		$cat=0;
	}
	if (!isset($_REQUEST['Pag'])){
		$Da=0;
		$A=$N_A_pp;
	}else{
		$Da=($_REQUEST['Pag']-1)*$N_A_pp;
		$A=$N_A_pp;
	}
	if (!isset($_REQUEST['ente'])){
         $Ente = '-1';
	}else{
        $Ente = $_REQUEST['ente'];
	}

	$Proprietario = '';
	if (isset($Parametri['Proprietario'])){
		$Proprietario=$Parametri['Proprietario'];	
	}

	$TotAtti=ap_get_all_atti($Parametri['stato'],$Numero,$Anno,$Categorie,$Oggetto,
	$Dadata,$Adata,'',0,0,true,true,$Riferimento,$Ente,$Proprietario);
	$lista=ap_get_all_atti($Parametri['stato'],$Numero,$Anno,$Categorie,$Oggetto,
	$Dadata,$Adata,'Anno DESC,Numero DESC',$Da,$A,false,true,$Riferimento,$Ente,$Proprietario); 
	$titEnte=get_option('opt_AP_LivelloTitoloEnte');
	if ($titEnte=='')
		$titEnte="h2";
	$titPagina=get_option('opt_AP_LivelloTitoloPagina');
	if ($titPagina=='')
		$titPagina="h3";
	$coloreAnnullati=get_option('opt_AP_ColoreAnnullati');
	$colorePari=get_option('opt_AP_ColorePari');
	$coloreDispari=get_option('opt_AP_ColoreDispari');
	$VisFiltro="";
	if(isset($Parametri['minfiltri']) And $Parametri['minfiltri']=="si"){
		if(isset($_REQUEST['vf']) and  $_REQUEST['vf']=="s"){
			$VisFiltro='<button id="maxminfiltro" class="s"><span class="dashicons dashicons-filter"></span> Chiudi Ricerca atti mediante filtri</button>';
		}else{
//			$VisFiltro='<img src="'.Albo_URL.'img/maximize.png" id="maxminfiltro" class="h" alt="icona massimizza finestra filtri"/>';
			$VisFiltro='<button id="maxminfiltro" class="h"><span class="dashicons dashicons-filter"></span> Apri Ricerca atti mediante filtri</button>';
		}
	}
$Contenuto='';
$Contenuto.=' <div class="Visalbo">
<a name="dati"></a> ';
if (get_option('opt_AP_VisualizzaEnte')=='Si')
		$Contenuto.= '<'.$titEnte.' ><span  class="titoloEnte">'.stripslashes(get_option('opt_AP_Ente')).'</span></'.$titEnte.'>';
        $char = explode(',',$TitoloAtti);
       
    $ht ='<ul class="cat-list0s">';
	foreach($char as $val) {
		$ht .= "<li>".$val ."</li>";
	}
 
	$ht .= "</ul>";
		if($evidenza == 1) {
			$Contenuto.= '<div class="atti-title" ><h3>In Evidenza</h3></div>';
		} else {
			//$Contenuto.= '<div class="atti-title" ><h3>'.$pageTitle.'</h3></div>';
		}
//$Contenuto.='<'.$titPagina.'><span  class="titoloPagina">'. $ht.'</span></'.$titPagina.'>';
if (!isset($Parametri['filtri']) Or $Parametri['filtri']=="si")
	$Contenuto.='<h4 class="filtri">'.$VisFiltro.'</h4>'.VisualizzaRicerca($Parametri['stato'],$Categoria,$Parametri['minfiltri']);
//$Contenuto.=  $nascondi;

$FEColsOption=get_option('opt_AP_ColonneFE',array(
									"Data"=>0,
									"Ente"=>0,
									"Riferimento"=>0,
									"Oggetto"=>0,
									"Validita"=>0,
									"Categoria"=>0,
									"Note"=>0,
									"RespProc"=>0,
									"DataOblio"=>0));
if(!is_array($FEColsOption)){
	$FEColsOption=shortcode_atts(array(
				"Data"=>0,
				"Ente"=>0,
				"Riferimento"=>0,
				"Oggetto"=>0,
				"Validita"=>0,
				"Categoria"=>0,
				"Note"=>0,
				"RespProc"=>0,
				"DataOblio"=>0), json_decode($FEColsOption,TRUE),"");
}	
$Contenuto.= '	<div class="tabalbo">';                               
/* 		$Contenuto.= '	<table id="elenco-atti-OldStyle" class="tabella-dati-albo" summary="atti validi per riferimento, oggetto e categoria"> 
	    <caption>Atti</caption>
		<thead>
	    	<tr>
	        	<th scope="col">Prog.</th>';
foreach($FEColsOption as $Opzione => $Valore){
		if($Valore==1){
			$Contenuto.= '			<th scope="col">'.$Opzione.'</th>';
		}
}
echo'	</tr>
	    </thead>
	    <tbody>'; */
	    $CeAnnullato=false;
	if ($lista){
	 	$pari=true;
		if (strpos(get_permalink(),"?")>0)
			$sep="&amp;";
		else
			$sep="?";
		foreach($lista as $riga){
			$Link='<a href="'.get_permalink().$sep.'page=user-home&amp;action=visatto&amp;eid='.$riga->IdAtto.'"  style="text-decoration: underline;">';
			$categoria=ap_get_categoria($riga->IdCategoria);
			$cat=$categoria[0]->Nome;
			$responsabileprocedura=ap_get_responsabile($riga->RespProc);
			$respproc=$responsabileprocedura[0]->Cognome." ".$responsabileprocedura[0]->Nome;
			$NumeroAtto=ap_get_num_anno($riga->IdAtto);
	//		Bonifica_Url();
			$classe='';
			if ($pari And $coloreDispari) 
				$classe='style="background-color: '.$coloreDispari.';"';
			if (!$pari And $colorePari)
				$classe='style="background-color: '.$colorePari.';"';
			$pari=!$pari;
			if($riga->DataAnnullamento!='0000-00-00'){
				$classe='style="background-color: '.$coloreAnnullati.';"';
				$CeAnnullato=true;
			}
			$show_row = 1;
			$meta_dat = ap_get_elenco_attimeta("Array","","","",$riga->IdAtto);
			$evidenza_value = "";
			$image_value = "";
			foreach($meta_dat as $key=>$value)
			{
				if($value->Meta == '_evidenza')
				{
					$evidenza_value = $value->Value;
				}

				if($value->Meta == '_thumb')
				{
					$image_value = $value->Value;
				}
			}

			if($evidenza == 1) {
				if($evidenza_value == 1) {
					$show_row = 1;
				} else {
					$show_row = 0;
				}
			}
			if($show_row == 1) {
				if(isset($_REQUEST['soggetti']) && !empty($_REQUEST['soggetti'])) {
					$risultato=ap_get_atto($riga->IdAtto);
					$risultato=$risultato[0];
					$Soggetti=unserialize($risultato->Soggetti);
					$Soggetti=ap_get_alcuni_soggetti_ruolo(implode(",",$Soggetti));
					foreach($Soggetti as $Soggetto){
						if($Soggetto->Funzione == $_REQUEST['soggetti']) {
							$show_row = 1;
							break;
						} else {
							$show_row = 0;
						}
					}					
				}
			}
			if($show_row == 1) {
				if($image_value != -1) {
					$thumb_id = $image_value;
					$image_attributes = wp_get_attachment_image_src( $thumb_id );
				} else {
					$thumb_id = '';
					$image_attributes = '';
				}
			$Contenuto.= '<div class="atti-list" >';
			$Contenuto.= '<div class="atti-fur-img"><img src="'.$image_attributes[0].'" /></div>';
			$acTitle = stripslashes($riga->Riferimento);
			if (strlen($acTitle) >= 20) {
				$acTitle = substr($acTitle, 0, 20). "...";
			}
			$actInfo = stripslashes($riga->Oggetto);
			if (strlen($actInfo) >= 30) {
				$actInfo = substr($actInfo, 0, 30). "...";
			}

			$Contenuto.= '<div class="atti-info"><strong>'.$Link;
			$Tipo_data=get_option( 'opt_ap_Tipologia' );  
			$Tipo_data=json_decode($Tipo_data);
			foreach($Tipo_data as $tiplology){
				if($tiplology->Codice==$riga->Tipologia){
					$Contenuto.= $tiplology->Funzione;
				}
			}
			$Contenuto.='</a><span class="attdate"> '.$Link.ap_VisualizzaData($riga->Ultimamodifica) .'</span></strong><p>'.$Link.$actInfo.'</a></p></div></div>';
			
			//$Contenuto.= '<div class="atti-info"><strong>'.$Link.get_tipologia_id($riga->Tipologia).'</a><span class="attdate"> '.$Link.ap_VisualizzaData($riga->Ultimamodifica) .'</span></strong><p>'.$Link.$actInfo.'</a></p></div></div>';
			/*
			$Contenuto.= '<td '.$classe.'>'.$Link.$NumeroAtto.'/'.$riga->Anno .'</a>
					</td>';
			if ($FEColsOption['Data']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->Data) .'</a>
					</td>';
			if ($FEColsOption['Ente']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.$Link.stripslashes(ap_get_ente($riga->Ente)->Nome) .'</a>
					</td>';
			if ($FEColsOption['Riferimento']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Riferimento) .'</a>
					</td>';
			if ($FEColsOption['Oggetto']==1)
				$Contenuto.='			
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Oggetto) .'</a>
					</td>';
			if ($FEColsOption['Validita']==1)
				$Contenuto.='								
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->DataInizio) .'<br />'.ap_VisualizzaData($riga->DataFine) .'</a>  
					</td>';
			if ($FEColsOption['Categoria']==1)
				$Contenuto.='								
					<td '.$classe.'>
						'.$Link.$cat .'</a>  
					</td>';
			if ($FEColsOption['Note']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.stripslashes($riga->Informazioni) .'</a>
					</td>';
			if ($FEColsOption['RespProc']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.$respproc .'</a>
					</td>';	
			if ($FEColsOption['DataOblio']==1)
				$Contenuto.='
					<td '.$classe.'>
						'.$Link.ap_VisualizzaData($riga->DataOblio) .'</a>
					</td>';
					*/
/* 		$Contenuto.='</tr>';  */
			}
			}
	} else {
/* 			$Contenuto.= '<tr>
					<td colspan="6">Nessun Atto Codificato</td>
				  </tr>'; */
	}

/* 	$Contenuto.= '
     </tbody>
    </table>'; */
$Contenuto.= '<div class="clearfix"></div>';
if ($TotAtti>$N_A_pp){
	$Para='';
	foreach ($_REQUEST as $k => $v){
		if ($k!="Pag" and $k!="vf")
			if ($Para=='')
				$Para.=$k.'='.$v;
			else
				$Para.='&amp;'.$k.'='.$v;
	}
	if ($Para=='')
		$Para="?Pag=";
	else
		$Para="?".$Para."&amp;Pag=";
	$Npag=(int)($TotAtti/$N_A_pp);
	if ($TotAtti%$N_A_pp>0){
		$Npag++;
	}

	//<p><strong>N. Atti '.$TotAtti.'</strong>&nbsp;&nbsp; Pagine';
	$Contenuto.= ' 
	<div class="tablenav" style="float:right;" id="risultati">
	<div class="tablenav-pages">
	<p>Pagine';
	if (isset($_REQUEST['Pag']) And $_REQUEST['Pag']>1 ){
		$Pagcur=$_REQUEST['Pag'];
		$PagPre=$Pagcur-1;
			$Contenuto.= '&nbsp;<a href="'.$Para.'1" class="page-numbers numero-pagina" title="Vai alla prima pagina">&laquo;</a>
&nbsp;<a href="'.$Para.$PagPre.'" class="page-numbers numero-pagina" title="Vai alla pagina precedente">&lsaquo;</a> ';
	}else{
		$Pagcur=1;
		$Contenuto.= '&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&laquo;</span>
&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&lsaquo;</span> ';
	}
	$Contenuto.= '&nbsp;<span class="page-numbers current">'.$Pagcur.'/'.$Npag.'</span>';
	$PagSuc=$Pagcur+1;
	   if ($PagSuc<=$Npag){
		$Contenuto.= '&nbsp;<a href="'.$Para.$PagSuc.'" class="page-numbers numero-pagina" title="Vai alla pagina successiva">&rsaquo;</a>
&nbsp;<a href="'.$Para.$Npag.'" class="page-numbers numero-pagina" title="Vai all\'ultima pagina">&raquo;</a>';
	}else{
		$Contenuto.= '&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>
&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>';			
	}
$Contenuto.='			</p>
	</div>
</div>';
}

$Contenuto.='</div>';
	/* if ($CeAnnullato) 
		$Contenuto.= '<p>Le righe evidenziate con questo sfondo <span style="background-color: '.$coloreAnnullati.';">&nbsp;&nbsp;&nbsp;</span> indicano Atti Annullati</p>'; */
$Contenuto.= '</div><!-- /wrap -->	';
return $Contenuto;
}
?>

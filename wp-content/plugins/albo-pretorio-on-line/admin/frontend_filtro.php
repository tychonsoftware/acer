<?php
/**
 * Gestione Filtri FrontEnd.
 * @link       http://www.eduva.org
 * @since      4.1.95
 *
 * @package    ALbo On Line
 */
function VisualizzaRicerca($Stato=1,$cat=0,$StatoFinestra="si"){
	$anni=ap_get_dropdown_anni_atti('anno','anno','postform','',(isset($_REQUEST['anno'])?$_REQUEST['anno']:date("Y")),$Stato); 
	//$categorie=ap_get_dropdown_ricerca_categorie('categoria','categoria','postform','',(isset($_REQUEST['categoria'])?$_REQUEST['categoria']:0),$Stato);
	//$cate = explode(",",$cat);
	$categorie=ap_get_dropdown_ricerca_categorie('categoria','categoria','postform','',$cat,$Stato);
	ap_Bonifica_Url();
	if (strpos($_SERVER['REQUEST_URI'],"?")>0)
		$sep="&amp;";
	else
		$sep="?";
	$titFiltri=get_option('opt_AP_LivelloTitoloFiltri');
	if ($titFiltri=='')
		$titFiltri="h3";
	//$HTML='<div class="ricerca">';
	$HTML='';
	//		<'.$titFiltri.' style="margin-bottom:10px;">Filtri</'.$titFiltri.'>
	$HTML.='		<form id="filtro-atti" action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
	';
			if (strpos(htmlentities($_SERVER['REQUEST_URI']),'page_id')>0){
				$HTML.= '<input type="hidden" name="page_id" value="'.ap_Estrai_PageID_Url().'" />';
			}
//	$HTML.= '<input type="hidden" name="categoria" value="'.$cat.'" />';
				$HTML.= '<table id="tabella-filtro-atti" class="tabella-dati-albo" >';
/*	if($cat!=0){
		$HTML.= '				
					<tr>
						<th scope="row" >Categorie</th>
						<td>'.$categorie.'</td>
					</tr>';
	} */
			$HTML.= '<tr style="display: none;">
						<th scope="row"><label for="ente">Ente</label></th>
						<td>'.ap_get_dropdown_enti("ente","ente","","",(isset($_REQUEST['ente'])?$_REQUEST['ente']:"")).'</td>
					</tr>
					<tr style="display: none;">
						<th scope="row"><label for="numero">Atto N&deg;/Anno</label></th>
						<td><input type="text" size="10" maxlength="10" name="numero" id ="numero" value="'.(isset($_REQUEST['numero'])?$_REQUEST['numero']:"").'"/>/
						'.$anni.'</td>
					</tr>
					<tr>
					<th scope="row"><label for="riferimento">Riferimento</label></th>
						<td><input type="text" size="40" style="width:100%;" name="riferimento" id ="riferimento" value="'.(isset($_REQUEST['riferimento'])?$_REQUEST['riferimento']:"").'"/></td>
					</tr>
					<tr>
						<th scope="row"><label for="oggetto">Oggetto</label></th>
						<td><input type="text" size="40" style="width:100%;" name="oggetto" id ="oggetto" value="'.(isset($_REQUEST['oggetto'])?$_REQUEST['oggetto']:"").'"/></td>
					</tr>
					
					</table>
					<table id="tabella-filtro-atti" class="tabella-dati-albo filtertable">
					<tr>
						<th scope="row"><label for="note">Descrizione</label></th>
						<td colspan="3"><input type="text" size="40" style="width:100%;" name="note" id ="note" value="'.(isset($_REQUEST['note'])?$_REQUEST['note']:"").'"/></td>
					</tr>
					<tr>
						<th scope="row" style="width:121px;"><label for="Calendario1">da Data</label></th>
						<td><input name="DataInizio" id="Calendario1" type="text" value="'.htmlentities((isset($_REQUEST['DataInizio'])?$_REQUEST['DataInizio']:"")).'" size="10" /></td>
						<th scope="row"><label for="Calendario2">a Data</label></th>
						<td><input name="DataFine" id="Calendario2" type="text" value="'.htmlentities((isset($_REQUEST['DataFine'])?$_REQUEST['DataFine']:"")).'" size="10" /></td>
					</tr>
					<tr>
						<th scope="row"><label for="soggetti">Categorie</label></th>
						<td colspan="4">'.$categorie.'</td>
					</tr>
					<tr>
						<th scope="row"><label for="soggetti">Riferimento</label></th>';
						$HTML.= '<td colspan="4">'.ap_get_Funzioni_Responsabili($Output="Select",$ID="soggetti",$Name="soggetti",$Selezionato=(isset($edit) && $edit)?$risultato[0]->Funzione:"").'</td>';
/*						$HTML.= '<td colspan="4"><select name="soggetti" id="soggetti">';
						$HTML.= '<option value="">Select</option>';
						$Ana_Soggetti=ap_get_responsabili();
						if(isset($_REQUEST['soggetti'])) {
							$sogVal = $_REQUEST['soggetti'];
						}
						foreach($Ana_Soggetti as $Soggetto){
							if($sogVal == $Soggetto->IdResponsabile){
								$sel = 'selected';
							} else {
								$sel = '';
							}
							$HTML.= '<option value="'.$Soggetto->IdResponsabile.'" '.$sel.'>'.$Soggetto->Cognome.' '.$Soggetto->Nome.'</option>';
						}
						$HTML.= '</select></td>'; */
					$HTML.= '</tr>
					 		
				</table>
				<div class="actn-btns">
				 <input type="submit" name="filtra" id="filtra" class="bottoneFE inline-block" value="Filtra"  /><input type="submit" name="annullafiltro" id="annullafiltro" class="bottoneFE inline-block" value="Annulla Filtro"  /><button type="button" class="bottoneFE inline-block" id="togle-tbl-btn" onclick="showhidetable()"><i class="arow-icns4e"></i></button>
				</div>
			</form>
	';
	if($StatoFinestra=="si")
		$stile='style="display:none;"';
	else
		$stile="";
	$HTMLC='<div id="fe-tabs-container" '.$stile.'>
					<!--<ul>
						<li><a href="#fe-tab-1">Ricerca</a></li>';
	if($cat==0){
		$HTMLC.='
						<li><a href="#fe-tab-2">Categorie</a></li>';
	}
	$HTMLC.='
					</ul>-->
					<div id="fe-tab-1">';
	$HTMLC.=$HTML;
	$lista=ap_get_categorie_gerarchica();
	$HTMLL='
	          <div class="ricercaCategoria">
	              <ul style="list-style-type: none;">';
	if ($lista){
		foreach($lista as $riga){
		 	$shift=(((int)$riga[2])*15);
	   		$numAtti=ap_num_atti_categoria($riga[0],$Stato);
		 	if (strpos(get_permalink(),"?")>0)
		  		$sep="&amp;";
	   		else
		   		$sep="?";
	   		if ($numAtti>0)
	      		$HTMLL.='               <li style="text-align:left;padding-left:'.$shift.'px;font-weight: bold;"><a href="'.get_permalink().$sep.'filtra=Filtra&amp;categoria='.$riga[0].'"  >'.$riga[1].'</a> '.$numAtti.'</li>'; 
		}
	}else{
		$HTMLL.= '                <li>Nessuna Categoria Codificata</li>';
	}
	$HTMLL.='             </ul>
	          </div>';
	$HTMLC.= '
	      </div>';
	if($cat==0){	
		$HTMLC.= '
				<div id="fe-tab-2">'.$HTMLL.'
					</div>';			
	}
	$HTMLC.= '
				</div>
	<br class="clear" />';
	$HTMLC.= '<script>
   function showhidetable(){
	   $("#togle-tbl-btn").toggleClass("active");
	   $(".filtertable").toggle();
   }
</script>';
	return $HTMLC;
}
?>

<?php
/**
 * Gestione Allegati.
 * @link       http://www.eduva.org
 * @since      4.1.95
 *
 * @package    ALbo On Line
 */
if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
  die('You are not allowed to call this page directly.');
}
$TipiAmmessi = ap_tipiFileAmmessi(TRUE);
//	$TipiAmmessi=implode(",",$TipiAmmessi);
$TEE = "";
$TE = "";
$AI = "";
$IdAtto = 0;

$existingAttachments = "";
foreach ($TipiAmmessi as $Tipo) {
  $TE .= '"' . $Tipo["."] . '", ';
  $TEE .= '.' . $Tipo["."] . ', ';
  $AI .= '"' . $Tipo["."] . '":"' . $Tipo["Icon"] . '", ';
}
$TA = substr($TA, 0, -2);
$TE = substr($TE, 0, -2);
$AI = substr($AI, 0, -2);

if (isset($_REQUEST['id'])) {
  $IdAtto = $_REQUEST['id'];
  
  $allegati = ap_get_all_allegati_atto($_REQUEST['id']);
  $TipidiFiles = ap_get_tipidifiles();
  foreach ($allegati as $allegato) {
    $Estensione = ap_ExtensionType($allegato->Allegato);
    if (isset($allegato->TipoFile) and $allegato->TipoFile != ""  and ap_isExtensioType($allegato->TipoFile)) {
      $Estensione = ap_ExtensionType($allegato->TipoFile);
      $icona = $TipidiFiles[$Estensione]['Icona'];
    } else {
      $icona = $TipidiFiles[strtolower($Estensione)]['Icona'];
    }
    $existingAttachments .= '<li class="elemento" id="' . $allegato->IdAllegato .'"><input type="radio" id="r0" name="r0"/><label for="r0" ><span class="closeinput" onclick="deleteAttachment(' . $allegato->IdAllegato . ',-1);">x</span></label><a href="'.ap_DaPath_a_URL($allegato->Allegato).'" target="_blank"><img src="' . $icona . '"></a><br/><p>File name ' . basename($allegato->Allegato) . ', file size ' . ap_Formato_Dimensione_File(filesize($allegato->Allegato)) . '.</p><input type="text" name="' . strip_tags($allegato->TitoloAllegato) . '"></li>';
  }
}

?>
<style>
  #pulSel {
    display: inline-block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    cursor: pointer;
    padding: 10px 20px;
    border: 1px solid #018dc4;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    font: normal normal bold 20px/normal "Times New Roman", Times, serif;
    color: rgba(255, 255, 255, 1);
    -o-text-overflow: clip;
    text-overflow: clip;
    background: #0199d9;
    -webkit-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  }

  #pulCar {
    display: inline-block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    cursor: pointer;
    padding: 10px 20px;
    border: 1px solid #018dc4;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    font: normal normal bold 20px/normal "Times New Roman", Times, serif;
    color: #0073aa;
    -o-text-overflow: clip;
    text-overflow: clip;
    background: #fff;
    -webkit-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2);
    -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
    transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  }

  .elemento {
    display: block;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    float: none;
    z-index: auto;
    width: auto;
    height: auto;
    position: static;
    cursor: default;
    opacity: 0.72;
    margin: 2px;
    padding: 4px;
    overflow: visible;
    border: 1px solid;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    font: normal 20px/2 "Times New Roman", Times, serif;
    color: black;
    -o-text-overflow: clip;
    text-overflow: clip;
    background: rgba(252, 252, 2, 0.7);
    -webkit-box-shadow: none;
    box-shadow: none;
    text-shadow: none;
    -webkit-transition: none;
    -moz-transition: none;
    -o-transition: none;
    transition: none;
    -webkit-transform: none;
    transform: none;
    -webkit-transform-origin: 50% 50% 0;
    transform-origin: 50% 50% 0;
  }

  .elemento input {
    width: 100%;
  }

  form .preview.postEdit {
    float: initial;
  }


  ol.list_sbar {    
    overflow: hidden;
    list-style: none;
}
ol.list_sbar li{
    position:relative;
}
ol.list_sbar li > div{
    clear:both;
    border-bottom: solid 1px #ddd;
    min-height:22px;
    line-height: 22px;
    display: block;
    text-decoration: none;
    background: #fff;
    outline: 0;
    font-weight: normal;
    padding: 4px 9px;
}
ol.list_sbar li:first-child {
    border-top: solid 1px #ddd;
}
ol.list_sbar li a {
    text-decoration: none;
    z-index: 33;
    color: #666;
    margin-bottom: 0;
    display:inline-block;
    outline: 0;
    float:left;
}
ol.list_sbar li > div:hover {
    color: #111;
    background: #FFF2BE;
    -moz-box-shadow: inset 0 0 1px #333;
    -webkit-box-shadow: inset 0 0 1px #333;
    box-shadow: inset 0 0 1px #333;
}
ol.list_sbar li input{
    display: none;
}
ol.list_sbar li label:hover{
    color: #f00;
}
ol.list_sbar li label{
    position: absolute; top: 50%;
    line-height: 22px;
    font-size: 18px;
    text-align: right;
    padding-right: 8px;    
    width: 20px;
    height: 100%;
    right: 0;
    color: #999;
    font-family:  calibri, sans-serif; 
    cursor: pointer;    
    transform: translateY(-50%); 
}
/* ol.list_sbar li input:checked + label{
    display: none;
} */
</style>
<div>
  <label for="files" id="pulCar"><span class="dashicons dashicons-upload" title="Allegati"></span> Allegati</label>
  <input type="file" id="files" name="files[]" accept="<?php echo $TEE; ?>" multiple>
  <input type="hidden" id="removedfiles" name="removedfiles" value="">
</div>
<div class="preview postEdit">
</div>
<script>
  var rids = new Array();
  function deleteAttachment(IdAllegato,uniqid) {
    if(IdAllegato != -1){
      rids.push(IdAllegato);
      var currentVal = document.getElementById("removedfiles").value;
    if(currentVal != ''){
      document.getElementById("removedfiles").value = currentVal + "," + IdAllegato;
    }else 
      document.getElementById("removedfiles").value = IdAllegato;
    removeElement(document.getElementById(IdAllegato));
    }else if(uniqid != -1){
      removeElement(document.getElementById(uniqid));
    }
    
  }

function removeElement(element) {
    element && element.parentNode && element.parentNode.removeChild(element);
}
  var input = document.querySelector('#files');
  var preview = document.querySelector('.preview');
  input.style.visibility = 'hidden';
  input.addEventListener('change', caricaDatiAllegati);
  
  var existinglist = <?php echo json_encode($existingAttachments); ?>;
  if(existinglist && existinglist.length > 0){
    var attachmentList = document.createElement('ol');
    attachmentList.setAttribute("id", "attachmentList");
    attachmentList.setAttribute("class", "list_sbar");
    attachmentList.innerHTML = existinglist;
    preview.appendChild(attachmentList);
  }

 
  function caricaDatiAllegati() {
    while (preview.firstChild) {
      preview.removeChild(preview.firstChild);
    }
    
   
    var curFiles = input.files;
    var list =document.createElement('ol');
    list.setAttribute("id", "attachmentList");
    list.setAttribute("class", "list_sbar");
    list.innerHTML = existinglist;
    preview.appendChild(list);

    var icone = {
      <?Php echo $AI; ?>
    };
  
    for (var i = 0; i < curFiles.length; i++) {
      var uniqid = "<?php echo uniqid();?>";
      var icona = IconFileType(curFiles[i]);
      var listItem = document.createElement('li');
      listItem.className = "elemento";
      listItem.setAttribute("id",uniqid );
      var cinput = document.createElement('input');
      cinput.setAttribute("type", "radio");
      cinput.setAttribute("id", "r" + (i+1));
      cinput.setAttribute("name", "r" + (i+1));
      cinput.setAttribute("class", "closeinput");
      
      var clabel = document.createElement('label');
      clabel.setAttribute("for", "r" + (i+1));

      var span = document.createElement('span');
      span.setAttribute("onclick", "deleteAttachment(-1,'" + uniqid + "')");
      span.innerText = 'x';
      clabel.appendChild(span);
      listItem.appendChild(cinput);
      listItem.appendChild(clabel);
      
      var para = document.createElement('p');
      var des = document.createElement('input');
      des.setAttribute("type", "text");
      des.setAttribute("name", "Descrizione[" + i.toString() + "]");
      if (validFileType(curFiles[i])) {
        para.textContent = 'File name ' + curFiles[i].name + ', file size ' + returnFileSize(curFiles[i].size) + '.';
        var image = document.createElement('img');
        var a = document.createElement('a');
        var file = URL.createObjectURL(curFiles[i]);
        a.setAttribute("href", file);
        a.setAttribute("target", "_blank");

        image.setAttribute("src", icona);
        a.appendChild(image);
        listItem.appendChild(a);
        listItem.appendChild(document.createElement("br"));
        listItem.appendChild(para);
        listItem.appendChild(des);
      } else {
        listItem.appendChild(document.createElement("br"));
        para.textContent = 'File name ' + curFiles[i].name + ': Tipo di file non permesso. Riprova selezionando un file con estensione diversa.';
        listItem.appendChild(para);
      }
      list.appendChild(listItem);
    }
    for (var i = 0; i < rids.length; i++) {
      removeElement(document.getElementById( rids[i]));
    }
  }

  function getEstensione(filename) {
    var parti = filename.split(".");
    return parti[(parti.length) - 1].toLowerCase();
  }
  var fileTypes = [<?php echo $TE; ?>];

  function validFileType(file) {
    var estensione = getEstensione(file.name);
    for (var i = 0; i < fileTypes.length; i++) {
      if (estensione.toLowerCase() === fileTypes[i].toLowerCase()) {
        return true;
      }
    }
    return false;
  }
  var icone = {
    <?Php echo $AI; ?>
  };

  function IconFileType(file) {
    var estensione = getEstensione(file.name);
    for (var i = 0; i < fileTypes.length; i++) {
      if (estensione === fileTypes[i]) {
        return icone[estensione];
      }
    }
    return false;
  }

  function returnFileSize(number) {
    if (number < 1024) {
      return number + 'bytes';
    } else if (number > 1024 && number < 1048576) {
      return (number / 1024).toFixed(1) + 'KB';
    } else if (number > 1048576) {
      return (number / 1048576).toFixed(1) + 'MB';
    }
  }
</script>
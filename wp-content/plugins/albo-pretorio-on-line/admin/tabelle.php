<?php
/**
 * WGestione Enti.
 * @link       http://www.eduva.org
 * @since      4.1.95
 *
 * @package    ALbo On Line
 */

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }


function load_Data_Funzioni(){
	$TabResponsabili=get_option('opt_AP_TabResp');
	if($TabResponsabili){
		$TR=json_decode($TabResponsabili);
	}else{
		$TR=json_decode('[{"ID":"","Funzione":"","Display":"No"}]');
	}
?>	  
<?php 

$Tipologia_json='[{ "Codice": "1", "Funzione": "Gara" },
            { "Codice": "2", "Funzione": "Avviso" },
            { "Codice": "3", "Funzione": "Aggiomamento" }]';
//add_option( 'opt_ap_Tipologia', $Tipologia_json); 
//update_option( 'opt_ap_Tipologia', $Tipologia_json);



?>

<script id="jsSourceRuoli" type="text/javascript">	  
jQuery(document).ready(function($){
	$('#GridFunzioni').appendGrid('load', [
<?php
	  foreach($TR as $Ruolo){
	  	echo "{ 'ID': '".$Ruolo->ID."', 'funzione': '".$Ruolo->Funzione."', 'visualizza': ".($Ruolo->Display=="Si" ? "true" : "false").", 'staincert': ".($Ruolo->StaCert=="Si" ? "true" : "false")." },";
		}
?>
        ]);	 
        
    $('#GridTipologia').appendGrid({
      	caption: 'Tabella Tipologia',
        columns: [
            { name: 'Codice', display: 'Codice' },
            { name: 'Funzione', display: 'Funzione' },
            {
                name: 'Immagine',
                display: 'Immagine',
                type: "custom",
                customBuilder: function (parent, idPrefix, name, uniqueIndex) {
                    var compiled = _.template($("#GridTipologiaImageColumnTemplate").html());
                    var template = compiled({
                        rowID: idPrefix + "_" + name + "_" + uniqueIndex,
                    });

                    parent.appendChild($(template)[0]);
                },
                customGetter: function (idPrefix, name, uniqueIndex) {
                    var rowID = idPrefix + "_" + name + "_" + uniqueIndex;
                    return parseInt($('#' + rowID + ' input.image-id').val());
                },
                customSetter: function (idPrefix, name, uniqueIndex, value) {
                    var rowID = idPrefix + "_" + name + "_" + uniqueIndex;
                    // document.getElementById(controlId).value = value;
                    $('#' + rowID + ' input.image-id').val(value);
                    wp.media.attachment(value).fetch().then(function (data) {
                        $('#' + rowID + ' .image-name').text(data.filename);
                    });
                }
            },
        ],
        initData:
         <?php $Tipo_data=get_option( 'opt_ap_Tipologia' );  
         print_r($Tipo_data);
         ?>
        ,
         customGridButtons: {
            append: { label: '<span class="dashicons dashicons-plus"></span>' },
            removeLast: { label: '<span class="dashicons dashicons-no"></span>' },
            insert: { label: '<span class="dashicons dashicons-welcome-add-page"></span>' },
            remove: { label: '<span class="dashicons dashicons-editor-removeformatting"></span>' },
            moveUp: { label: '<span class="dashicons dashicons-arrow-up-alt2"></span>' },
            moveDown: { label: '<span class="dashicons dashicons-arrow-down-alt2"></span>' }
		}
    });

    window.onClickGridTipologiaImageColumn = function (rowID) {
        var upload = wp.media({
            title:'Seleziona Immagine', //Title for Media Box
            multiple:false //For limiting multiple image
        })
            .on('select', function(){
                var select = upload.state().get('selection');
                var attach = select.first().toJSON();
                // jQuery('img.imgSrc').attr('src',attach.url).removeClass('elemHide');
                // jQuery('._thumbnail_id').val(attach.id);
                // jQuery('.imgRemove').removeClass('elemHide');
                // jQuery('.imgLoad').addClass('elemHide');
                $('#' + rowID + ' input.image-id').val(attach.id);
                $('#' + rowID + ' .image-name').text(attach.filename);
            })
            .open();
    }
});
</script>
<script type="text/html" id='GridTipologiaImageColumnTemplate'>
    <div id="<%= rowID %>" style="display: flex; flex-direction: column; padding: 5px 10px;">
        <input type="hidden" name="<%= rowID %>" class="image-id" />
        <div class="image-name" style=""></div>
        <a href="#" data-row-id="<%= rowID %>" onclick="onClickGridTipologiaImageColumn('<%= rowID %>')">Seleziona</a>
    </div>
</script>
<?php	  
		
}
$messages[1] = __('Item added.');
$messages[2] = __('Item deleted.');
$messages[3] = __('Item updated.');
$messages[4] = "Elemento non Memorizzato";
$messages[5] = __('Item not updated.');
$messages[6] = __('Item not deleted.');
$messages[7] = __('Impossibile cancellare Enti che sono collegati ad Atti');
$messages[80] = 'ATTENZIONE. Rilevato potenziale pericolo di attacco informatico, l\'operazione &egrave; stata annullata';
load_Data_Funzioni();
?>
<div id="ElaborazioneTabella" style="width: 200px;height: 200px;position: absolute;top: 50%;left: 50%; margin-top: -100px; margin-left: -100px;display:none;" >
	<img src="<?php echo plugin_dir_url( __FILE__ ) . 'css/images/ElaborazioneInCorso.gif'?>" id="ElaborazioneTabella"/>
</div>
<div class="wrap nosubsub">
	<div class="HeadPage">
		<h2 class="wp-heading-inline"><span class="dashicons dashicons-media-spreadsheet" style="font-size: 1.1em;"></span> Tabelle
	</div>

	<div id="config-tabs-container" style="margin-top:20px;">
		<ul>
			<li><a href="#Conf-tab-1">Funzioni</a></li>
		</ul>	 
		<div id="Conf-tab-1">

		  <form action="" method="post" id="FormFunzioni">
		    
		  	<table id="GridFunzioni"></table>
		  	<button type="button" id="MemoFunzioni" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="dashicons dashicons-edit"></span> Memorizza Tabella Funzioni</button>
		  	<button type="button" id="LoadDefaultFunzioni" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="dashicons dashicons-update"></span> Carica i valori di default</button>
		  </form>
		  
		</div>
	</div>
	<div id="config-tabs-container" style="margin-top:20px;">
		<ul>
			<li><a href="#Conf-tab-1">Tipologia</a></li>
		</ul>	 
		<div id="Conf-tab-1">

		  <form action="" method="post" id="FormTipologia">
		      <table id="GridTipologia"></table>
		  	<button type="button" id="MemoTipologia" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="dashicons dashicons-edit"></span> Memorizza Tabella Funzioni
		  	</button>
		  
		  </form>
		  
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $("#MemoTipologia").button().click(function () {
      $.ajax({type: 'POST', url: ajaxurl, 
	        data:{
	            action:'MemoTipologia',
	            security: myajaxsec,
	            valori:$(document.forms['1']).serialize()
	        },
			beforeSend: function() {
            	$("#ElaborazioneTabella").show("slow");
            },
	        success: function(risposta){
	        	$("#ElaborazioneTabella").hide("slow");
	        	alert(risposta);
	        },                   
	        error: function (xhr, ajaxOptions, thrownError) {
	        	$("#ElaborazioneTabella").hide("slow");
        		alert(xhr.status+ " "+thrownError);
	        }
        }); 			
	});
});
</script>

<!-- /wrap -->
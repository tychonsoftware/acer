/*
 *  Table Editor
 *
 *  @description:
 *  @since: 1.0
 *  @created: 17/01/13
 */

jQuery(document).ready(function($) {

    var nfte_utility = {
        escapeQuotes: function(string) {
            return string.replace(/"/g, '\\"');
        },
        unescapeQuotes: function(string) {
            return string.replace(/\\"/g, '"');
        }
    };

    function clean_table_data( table_data, escape ) {
        var clean_table_data = [];
        for (var i = 0; i < table_data.length; i++) {
            var row = []
            for ( var j = 0; j < table_data[ i ].length; j ++ ) {
                var cell = table_data[i][j];
                if ( cell !== null ) {
                    if ( escape ) {
                        cell = nfte_utility.escapeQuotes( cell );
                    } else {
                        cell = nfte_utility.unescapeQuotes( cell );
                    }
                }
                row.push( cell );
            }
            clean_table_data.push( row);
        }

        return clean_table_data
    }

    $("form").bind("reset", function() {
        clear_tables();
    });

    var table_name;
    var table_key;

    function get_table(table) {
        table_name = $( table ).attr('id');
        table_key = table_name.slice(6);
    }

   /**
     * Generate table editors
     */
    function generate_tables() {
        $( ".handsontable_editor" ).each( function() {

            get_table( this );

            var settings = get_settings();
            var cols = settings.number_columns;

            var table_data = get_default_data( settings )

            var saved_data = $( "#table_values_" + table_key ).val();
            if ( saved_data != '' ) {
                saved_data = JSON.parse( saved_data );
                saved_data = clean_table_data( saved_data, false );

                table_data = saved_data;
            }

            var parent_width = $( this ).parents( '.' + settings.parent ).width();

            var config = {
                data       : table_data,
                colHeaders : true,
                contextMenu: [ 'row_above', 'row_below', 'remove_row', 'undo', 'redo' ]
            };

            if ( settings.row_headers == '1' ) {
                config[ 'rowHeaders' ] = true;
                parent_width = parent_width - 50;
            }

            if ( settings.min_rows != '0' ) {
                config[ 'minSpareRows' ] = settings.min_rows;
            }

            if ( settings.edit_columns == 'on' ) {
                config[ 'contextMenu' ] = true;
            }

            if ( settings.drag_down == '0' ) {
                config[ 'fillHandle' ] = false;
            }

            if ( settings.table_width == 'scroll' ) {
                config[ 'stretchH' ] = 'all';
            }

            if ( settings.table_width == 'stretch' ) {
                var col_width = (
                                parent_width / cols
                                ) - 5;
                config[ 'colWidths' ] = Math.floor( col_width );
            }

            var table_headers = $( "#table_headers_" + table_key ).val();
            if ( table_headers != '' && table_headers != 'none' ) {
                config[ 'colHeaders' ] = JSON.parse( table_headers );
            } else if ( table_headers == 'none' ) {
                config[ 'colHeaders' ] = false;
            }

            config[ 'afterRender' ] = function() {
                // refresh wrapper height
	            refresh_height();
            };

            $( "#" + table_name ).handsontable( config );
        } );
    }

    // fire up the tables
    generate_tables();

    $('form.ninja-forms-form').submit(function( event ) {
        save_tables();
    });

	$('form#post').submit(function( event ) {
		if ( $('#post_type').val() == 'nf_sub' ) {
			save_tables();
		}
	});

    $('#ninja_forms_admin').submit(function( event ) {
        save_tables();
    });

    /*
     *  Get settings
     *
     *  @description:
     *  @since 1.0
     *  @created: 29/11/13
     */

    function get_settings() {
        var settings = $("#table_settings_" + table_key).val();
        return JSON.parse(settings);
    }

    /*
     *  Get default data
     *
     *  @description:
     *  @since 1.0
     *  @created: 29/11/13
     */

    function get_default_data( settings ) {
        var cols = settings.number_columns;

        var row_data = [];
        for ( var i = 0; i < cols; i++ ) {
            row_data.push("");
        }

        var table_data = [];
        table_data.push(row_data);
        return table_data;
    }


    /*
     *  Save table editor data
     *
     *  @description:
     *  @since 1.0
     *  @created: 29/11/13
     */

    function save_tables() {
        $( ".handsontable_editor" ).each(function() {
            get_table(this);
            var $container = $('#' + table_name);
            var table_data = $container.handsontable( 'getData' ); //.replace(/\"/g,'&quot;');

            table_data = clean_table_data( table_data, true );
            table_data = JSON.stringify( table_data );

            $("#table_values_" + table_key).val( table_data );
        });
    }

	/**
	 * Refresh Height of table
	 */
	function refresh_height() {
		$( ".handsontable_editor" ).each(function() {
			get_table(this);
			$( '#table_wrapper_' + table_key ).height( $( "#table_" + table_key + ' .wtHider' ).height() );
		});
	}

    /*
     *  Clear table editor data
     *
     *  @description:
     *  @since 1.0
     *  @created: 29/11/13
     */

    function clear_tables() {
        $( ".handsontable_editor" ).each(function() {
            get_table(this);
            var $container = $('#' + table_name);
            $("#table_values_" + table_key).val();
            var settings = get_settings();
            var table_data = get_default_data( settings );
            $container.handsontable( 'loadData', table_data );
        });
    }

    /*
     * Hooks for other add-ons
     */

    // Multi Part
    $( document ).on( 'mp_page_change.example', function() {
        // fire up the tables
        generate_tables();
    } );

    // Conditional Logic
    $( document ).on( 'ninja_forms_conditional_show', function( e ) {
        // save any table data
        save_tables();
        // render tables
        setTimeout( function() {
            generate_tables();
        }, 50 );
    } );

    $('form.ninja-forms-form').on( 'click', '.ninja-forms-mp-nav', function( event ) {
        save_tables();
    });

});
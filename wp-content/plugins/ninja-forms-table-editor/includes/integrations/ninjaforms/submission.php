<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Integrations_NinjaForms_Submission {

	/**
	 * NF_TE_Integrations_NinjaForms_Submission constructor.
	 */
	public function __construct() {
		// Submission table view, column.
		add_filter( 'ninja_forms_custom_columns', array( $this, 'submission_custom_column' ), 10, 3 );
		// Export value
		add_filter( 'ninja_forms_subs_export_pre_value', array( $this, 'submission_export_value' ) );
		// Submission edit table.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
		// Save table editor data via AJAX
		add_action( 'wp_ajax_nf_te_sub_update_value', array( $this, 'ajax_update_value' ) );
		// Make sure we save the same data again
		add_filter( 'nf_edit_sub_user_value', array( $this, 'edit_sub_value' ), 10, 3 );
	}

	/**
	 * Display link to CSV of table in the Submission index table column.
	 *
	 * @param $value
	 * @param $field
	 * @param $sub_id
	 *
	 * @return string|void
	 */
	public function submission_custom_column( $value, $field, $sub_id ) {
		if ( NF_Table_Editor::TYPE !== $field->get_setting( 'type' ) ) {
			return $value;
		}

		if ( ! is_array( $value ) || ! isset( $value['csv_url'] ) || empty( $value['csv_url'] ) ) {
			return __( 'No data', 'ninja-forms-table-editor' );
		}

		return sprintf( '<a href="%s">%s</a>', $value['csv_url'], __( 'Download CSV', 'ninja-forms-table-editor' ) );
	}

	public function submission_export_value( $field_value ) {
		if ( ! is_array( $field_value ) || ! isset( $field_value['csv_url'] ) || empty( $field_value['csv_url'] ) ) {
			return $field_value;
		}

		return $field_value['csv_url'];
	}

	/**
	 * Does the submission contain Table Editor fields.
	 *
	 * @return array
	 */
	protected function submission_has_table_editor_fields( $submission ) {
		$fields    = Ninja_Forms()->form( $submission->get_form_id() )->get_fields();
		$te_fields = array();
		foreach ( $fields as $field ) {
			if ( NF_Table_Editor::TYPE !== $field->get_setting( 'type' ) ) {
				continue;
			}

			$te_fields[ $field->get_id() ] = $field->get_settings();
		}

		return $te_fields;
	}

	/**
	 * Enqueue scripts for submission edit
	 */
	public function enqueue_admin_scripts() {
		if ( ! isset( $_GET['action'] ) || 'edit' !== $_GET['action'] ) {
			return;
		}

		global $post;
		if ( ! isset( $post ) || 'nf_sub' !== $post->post_type ) {
			return;
		}

		$submission = Ninja_Forms()->form()->sub( $post->ID )->get();
		$te_fields  = $this->submission_has_table_editor_fields( $submission );
		if ( empty( $te_fields ) ) {
			return;
		}

		$version = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? time() : NF_Table_Editor()->plugin_version;
		$min     = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$url = plugin_dir_url( NF_Table_Editor()->plugin_file_path );

		$hook = NF_TE_Display_Render::enqueue_handsontable( $url, $version, $min );

		wp_enqueue_style( $hook, $url . 'assets/css/admin/tableEditor.css', array( 'nf-te-handsontable' ), $version );
		wp_enqueue_script( 'nf-te-admin-field-table', $url . 'assets/js/admin/editSubmission.js', array(
			$hook,
		), $version );

		$settings = array();
		foreach ( $te_fields as $field_id => $field_settings ) {
			$value                         = $submission->get_field_value( $field_id );
			$field_settings['raw_data']    = $value['body'];
			$field_settings['table_width'] = NF_TE_Admin_Handsontable::get_table_width( $field_settings );
			$config                        = NF_TE_Admin_Handsontable::get_config( $field_settings );

			$settings[ $field_id ]['config']         = $config;
			$settings[ $field_id ]['table_width']    = $field_settings['table_width'];
			$settings[ $field_id ]['number_columns'] = $field_settings['number_columns'];
			$settings[ $field_id ]['raw_data']       = $field_settings['raw_data'];
		}

		wp_localize_script( 'nf-te-admin-field-table', 'nf_table_editor', array( 'settings' => $settings ) );
	}

	/**
	 * AJAX callback to update the table editor CSV on save of the submission
	 */
	public function ajax_update_value() {
		$sub_id     = filter_input( INPUT_POST, 'sub_id', FILTER_VALIDATE_INT );
		$field_id   = filter_input( INPUT_POST, 'field_id' );
		$table_data = filter_input( INPUT_POST, 'table_data' );

		if ( ! isset( $sub_id ) || ! isset( $field_id ) || ! isset( $table_data ) ) {
			wp_send_json_error( __( 'No table data.', 'ninja-forms-table-editor' ) );
		}

		$submission           = Ninja_Forms()->form()->sub( $sub_id )->get();
		$field                = Ninja_Forms()->form( $submission->get_form_id() )->field( $field_id )->get();
		$field_settings       = $field->get_settings();
		$field_settings['id'] = $field_id;
		$field_settings['raw_data'] = $table_data;

		$value = $submission->get_field_value( $field_id);
		$field_settings = array_merge( $field_settings, $value );

		NF_Table_Editor()->csv->save_csv( $field_settings, $submission, true );

		wp_send_json_success();
	}

	/**
	 * Filter the submission value on save so it doesn't get overwritten by blank
	 *
	 * @param $user_value
	 * @param $field_id
	 * @param $nf_sub_id
	 *
	 * @return mixed
	 */
	public function edit_sub_value( $user_value, $field_id, $nf_sub_id ) {
		$submission = Ninja_Forms()->form()->sub( $nf_sub_id )->get();
		$value      = $submission->get_field_value( $field_id );

		return $value;
	}

}
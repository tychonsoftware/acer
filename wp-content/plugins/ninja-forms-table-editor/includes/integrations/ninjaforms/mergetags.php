<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Integrations_NinjaForms_MergeTags {

	/**
	 * NF_TE_Integrations_NinjaForms_MergeTags constructor.
	 */
	public function __construct() {
		add_filter( 'ninja_forms_merge_tag_value_' . NF_Table_Editor::TYPE, array( $this, 'merge_tag_value' ), 10, 2 );
		add_filter( 'ninja_forms_table_editor_save_csv', array( $this, 'update_merge_tags') );
	}

	/**
	 * Return the CSV URL of the table as the mergetag value.
	 *
	 * @param string $value
	 * @param array $field
	 *
	 * @return string
	 */
	public function merge_tag_value( $value, $field ) {
		if ( is_null( $value ) ) {
			return $value;
		}

		if ( ! is_array( $value ) || ! isset( $value['csv_url'] ) || empty( $value['csv_url'] ) ) {
			return $value;
		}

		return $value['csv_url'];
	}

	public function update_merge_tags( $field ) {
		$all_merge_tags = Ninja_Forms()->merge_tags;

		if ( ! isset( $all_merge_tags['fields'])) {
			return $field;
		}

		if ( ! isset( $field['value']['csv_url'])) {
			return $field;
		}

		// Slim HTML
		$slim = NF_TE_Admin_Handsontable::output_slim( $field );
		$all_merge_tags['fields']->add( 'field_' . $field['key'] . '_slim', $field['key'], "{field:{$field['key']}:slim}", $slim );

		// Full HTML
		$full = NF_TE_Admin_Handsontable::output_full( $field );
		$all_merge_tags['fields']->add( 'field_' . $field['key'] . '_full', $field['key'], "{field:{$field['key']}:full}", $full );

		// Default field value of CSV URL
		$all_merge_tags['fields']->add( 'field_' . $field['key'], $field['key'], "{field:{$field['key']}}", $field['value']['csv_url'] );

		// Save merge tags
		Ninja_Forms()->merge_tags = $all_merge_tags;

		return $field;
	}
}

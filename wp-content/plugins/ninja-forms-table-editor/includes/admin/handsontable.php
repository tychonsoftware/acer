<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Admin_Handsontable {

	public static function get_config( $settings ) {
		$config = array(
			'data'         => self::get_table_data( $settings ),
			'rowHeaders'   => self::get_toggle_value( 'row_headers', $settings ),
			'minSpareRows' => isset( $settings['min_rows'] ) && is_numeric( $settings['min_rows'] ) ? $settings['min_rows'] : 0,
			'contextMenu'  => self::get_toggle_value( 'edit_columns', $settings ),
			'fillHandle'   => self::get_toggle_value( 'drag_down', $settings ),
			'colHeaders'   => self::get_col_headers( $settings ),
		);

		if ( 'scroll' === $settings['table_width'] ) {
			$config['stretchH'] = 'all';
		}

		return $config;
	}

	public static function get_table_width( $settings ) {
		if ( ! isset( $settings['table_width'] ) || 1 == $settings['table_width'] ) {
			return 'stretch';
		}

		return $settings['table_width'];
	}

	public static function get_headers( $field ) {
		$headers      = array();
		$header_names = self::get_col_headers( $field );
		if ( false === $header_names ) {
			return $headers;
		}

		if ( is_array( $header_names ) ) {
			return $header_names;
		}

		for ( $alpha = 65; $alpha < ( 65 + $field['number_columns'] ); $alpha++ ) {
			$headers[] = chr( $alpha );
		}

		return $headers;
	}

	protected static function get_col_headers( $settings ) {
		if ( 'custom' === $settings['custom_headers'] && ! empty( $settings['header_names'] ) ) {
			$headers = preg_split( '/\r\n|\r|\n/', $settings['header_names'] );

			return $headers;
		}

		if ( 'simple' === $settings['custom_headers'] ) {
			return true;
		}

		return false;
	}

	protected static function get_table_data( $settings ) {
		if ( isset( $settings['raw_data'] ) ) {
			return $settings['raw_data'];
		}

		return self::get_default_data( $settings );
	}

	protected static function get_default_data( $settings ) {
		$columns = $settings['number_columns'];

		$row_data = array();
		for ( $i = 0; $i < $columns; $i++ ) {
			$row_data[] = '';
		}

		return array( $row_data );
	}

	/**
	 * Get true or false setting value
	 *
	 * @param string $key
	 * @param array  $settings
	 *
	 * @return bool
	 */
	protected static function get_toggle_value( $key, $settings ) {
		if ( ! isset( $settings[ $key ] ) ) {
			return false;
		}

		if ( 'off' == $settings[ $key ] ) {
			return false;
		}

		return (bool) $settings[ $key ];
	}


	public static function get_data( $field ) {
		$data = $field['raw_data'];
		$data = json_decode( $data );

		if ( is_null( $data ) ) {
			return array();
		}

		if ( $field['min_rows'] <= 0 ) {
			return $data;
		}

		$row_count = 0;
		foreach ( $data as $tr_key => $tr ) {
			$row_count++;
			if ( $row_count > ( count( $data ) - $field['min_rows'] ) ) {
				unset( $data[ $tr_key ] );
			};
		}

		return $data;
	}

	public static function output_slim( $field  ) {
		$headers = isset( $field['value']['headers'] ) ? $field['value']['headers'] : NF_TE_Admin_Handsontable::get_headers( $field );
		$data    = isset( $field['value']['body'] ) ? $field['value']['body'] : NF_TE_Admin_Handsontable::get_data( $field );

		$html = '';
		foreach ( $data as $tr ) {
			$html .= '<div>';
			foreach ( $tr as $td_key => $td ) {
				if ( isset( $headers[ $td_key ] ) ) {
					$html .= '<label style="font-weight: bold;">' . $headers[ $td_key ] . '</label><br>';
				}
				$html .=  stripslashes( $td );
				$html .= '<br>';
			}
			$html .= '</div><br>';
		}

		return $html;
	}

	public static function output_full( $field ) {
		$headers = isset( $field['value']['headers'] ) ? $field['value']['headers'] : NF_TE_Admin_Handsontable::get_headers( $field );
		$data    = isset( $field['value']['body'] ) ? $field['value']['body'] : NF_TE_Admin_Handsontable::get_data( $field );

		$html    = '<table>';
		if ( count( $headers ) > 0 ) {
			$html .= '<thead><tr>';
			foreach ( $headers as $th ) {
				$html .= '<th>' . $th . '</th>';
			}
			$html .= '</tr></thead>';
		}
		if ( $data ) {
			foreach ( $data as $tr ) {
				$html .= '<tr>';
				foreach ( $tr as $td_key => $td ) {
					$html .= '<td>' . stripslashes( $td ) . '</td>';
				}
				$html .= '</tr>';
			}
		}
		$html .= '</table>';

		return $html;
	}
}
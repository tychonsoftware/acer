<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Admin_Upgrade {

	/**
	 * NF_TE_Admin_Upgrade constructor.
	 */
	public function __construct() {
		add_filter( 'ninja_forms_upgrade_settings', array( $this, 'upgrade' ), 99 );
		add_filter( 'ninja_forms_after_upgrade_settings', array( $this, 'after_upgrade' ), 99 );
	}

	/**
	 * Upgrade the settings and field settings when NF core is upgraded to 3.0
	 *
	 * @param array $form
	 *
	 * @return array
	 */
	public function upgrade( $form ) {
		foreach ( $form['actions'] as $action_key => $action ) {
			// Email attachments
			$action = $this->email_attachments( $action, $form );

			$form['actions'][ $action_key ] = $action;
		}

		foreach ( $form['fields'] as $key => $field ) {

			if ( '_table_editor' !== $field['type'] ) {
				// Not an upload field
				continue;
			}

			// Convert field type
			$field['type'] = NF_Table_Editor::TYPE;

			$form['fields'][ $key ] = $field;
		}

		return $form;
	}

	/**
	 * Replace old keys with new Field keys
	 *
	 * @param array $form
	 *
	 * @return array
	 */
	public function after_upgrade( $form ) {
		// Replace field IDs with keys in actions
		foreach ( $form['actions'] as $action_key => $action ) {
			if ( 'email' === $action['type'] ) {
				// Email attachments
				foreach ( $form['fields'] as $field_key => $field ) {
					$attach_key = NF_TE_Integrations_NinjaForms_Attachments::KEY . '-';
					if ( isset( $action[ $attach_key . $field_key ] ) ) {
						$new_key = $attach_key . $field['key'];

						$action[ $new_key ] = $action[ $attach_key . $field_key ];
						unset( $action[ $attach_key . $field_key ] );
					}
				}
			}

			$form['actions'][ $action_key ] = $action;
		}


		return $form;
	}

	/**
	 * Add email attachments to the action
	 *
	 * @param array $action
	 * @param array $form
	 *
	 * @return array
	 */
	protected function email_attachments( $action, $form ) {
		if ( 'email' !== $action['type'] ) {
			return $action;
		}

		foreach ( $form['fields'] as $key => $field ) {
			if ( '_table_editor' !== $field['type'] ) {
				// Not an upload field
				continue;
			}

			if ( ! isset( $action[ 'table_editor_' . $field['id'] ] ) || 1 != $action[ 'table_editor_' . $field['id'] ] ) {
				continue;
			}

			$action[ NF_TE_Integrations_NinjaForms_Attachments::KEY . '-' . $key ] = 1;
		}

		return $action;
	}
}
<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Admin_CSV {

	/**
	 * NF_TE_Admin_CSV constructor.
	 */
	public function __construct() {
		add_action( 'ninja_forms_post_run_action_type_save', array( $this, 'save_csv_on_save_sub' ) );
	}

	public function save_csv_on_save_sub( $data ) {
		if ( ! isset( $data['fields'] ) ) {
			return $data;
		}
		if ( ! isset( $data['form_id'] ) ) {
			return $data;
		}

		$sub_id = $data['actions']['save']['sub_id'];
		$sub    = Ninja_Forms()->form( $data['form_id'] )->sub( $sub_id )->get();

		foreach ( $data['fields'] as $field_id => $field ) {
			if ( NF_Table_Editor::TYPE !== $field['type'] ) {
				continue;
			}

			$updated_field = $this->save_csv( $field, $sub );
			$data['fields'][ $field_id ] = apply_filters( 'ninja_forms_table_editor_save_csv', $updated_field );
		}

		return $data;
	}

	public function save_csv( $field, $sub, $update = false ) {
		$location  = $this->get_save_location( $field );
		$file_name = $this->get_csv_name( $field['label'], $sub->get_id() );
		$file      = $this->get_save_path( $location, $file_name );

		$headers = NF_TE_Admin_Handsontable::get_headers( $field );
		$data    = NF_TE_Admin_Handsontable::get_data( $field );

		$value            = array();
		$value['sub_id']  = $sub->get_id();
		$value['headers'] = $headers;
		$value['body']    = $data;
		$value['csv_url'] = '';

		if ( count( $headers ) > 0 ) {
			$data = array_merge( array( $headers ), $data );
		}

		// Create CSV
		$this->write_csv( $file, $data );

		// TODO save field back
		switch ( $location ) {
			case 'media':
				if ( false === $update ) {
					// Add CSV to media library
					$attachment = array(
						'guid'           => $file,
						'post_mime_type' => 'text/csv',
						'post_title'     => $field['label'],
						'post_content'   => '',
						'post_status'    => 'inherit',
					);
					$attach_id  = wp_insert_attachment( $attachment, $file );
				} else {
					$attach_id = $field['csv_attach_id'];
				}
				$value['csv_url']       = wp_get_attachment_url( $attach_id );
				$value['csv_path']      = $file;
				$value['csv_attach_id'] = $attach_id;
				break;
			case 'server':
				$value['csv_url']  = $this->get_save_url( 'server', $file_name );
				$value['csv_path'] = $file;
				break;
		}

		$sub->update_extra_values( $value );
		$sub->update_field_value( $field['id'], $value );
		$sub->save();

		$field['value'] = $value;

		return $field;
	}

	protected function get_save_location( $field ) {
		$location = 'media';
		if ( isset( $field['save_location'] ) ) {
			$location = $field['save_location'];
		}

		return $location;
	}

	protected function get_csv_name( $name, $sub_id ) {
		$filename = strtolower( sanitize_file_name( $name ) );

		return $filename . '-' . $sub_id . '.csv';
	}

	protected function get_save_path( $location, $filename = '' ) {
		$upload_dir = wp_upload_dir();
		if ( 'media' == $location ) {
			$path = $upload_dir['path'];
		} else {
			$path = $upload_dir['basedir'] . '/ninja-forms';
			wp_mkdir_p($path);
		}

		if ( '' != $filename ) {
			$path = trailingslashit( $path ) . $filename;
		}

		return $path;
	}

	protected function get_save_url( $location, $filename = '' ) {
		$upload_dir = wp_upload_dir();
		if ( 'media' == $location ) {
			$url = $upload_dir['url'];
		} else if ( 'server' == $location ) {
			$url = $upload_dir['baseurl'] . '/ninja-forms';
		}

		if ( '' != $filename ) {
			$url = trailingslashit( $url ) . $filename;
		}

		return $url;
	}

	protected function get_saved_csv_path( $user_value ) {
		if ( isset( $user_value['csv_path'] ) ) {
			$csv = $user_value['csv_path'];
		} else {
			$csv = get_the_guid( $user_value['csv_attach_id'] );
		}

		return $csv;
	}

	protected function get_saved_csv_url( $field_id, $field_row = null ) {
		if ( is_null( $field_row ) ) {
			$field_row = ninja_forms_get_field_by_id( $field_id );
		}
		$file_name = $this->get_csv_name( $field_row['data']['label'] );
		$location  = $this->get_save_location( $field_row );

		return $this->get_save_url( $location, $file_name );
	}

	protected function write_csv( $file, $table_data ) {
		// use NF delimiter and enclosure
		$delimiter = apply_filters( 'nf_table_editor_csv_delimiter', apply_filters( 'nf_sub_csv_delimiter', ',' ) );
		$enclosure = apply_filters( 'nf_table_editor_csv_enclosure', apply_filters( 'nf_sub_csv_enclosure', '"' ) );

		// Create CSV of table and Save to uploads
		$fp = fopen( $file, 'w' );
		foreach ( $table_data as $fields ) {
			fputcsv( $fp, $fields, $delimiter, $enclosure );
		}

		fclose( $fp );
	}
}
<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class NF_TE_Display_Render {

	/**
	 * @var
	 */
	protected static $scripts_loaded = false;

	/**
	 * NF_FU_Display_Render constructor.
	 */
	public function __construct() {
		add_filter( 'ninja_forms_localize_fields', array( $this, 'enqueue_scripts' ) );
		add_filter( 'ninja_forms_localize_fields_preview', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue Handsontable scripts
	 *
	 * @param string $url
	 * @param string $version
	 * @param string $min
	 * @param bool   $frontend
	 *
	 * @return string
	 */
	public static function enqueue_handsontable( $url, $version, $min, $frontend = false ) {
		$hook = 'nf-te-handsontable';

		$dependencies = array(
			'jquery',
		);

		if ( $frontend ) {
			$dependencies[] = 'nf-front-end';
		}

		wp_enqueue_script( $hook, $url . "assets/js/lib/handsontable.full$min.js", $dependencies, $version );
		wp_enqueue_style( $hook, $url . "assets/css/lib/handsontable.full$min.css" );
		wp_enqueue_script( 'nf-te-tableEditor', $url . "assets/js/util/tableEditor.js", array( $hook ), $version );

		return 'nf-te-tableEditor';
	}

	/**
	 * Enqueue scripts for the frontend
	 *
	 * @param array|object $field
	 *
	 * @return array|object $field
	 */
	public function enqueue_scripts( $field ) {
		if ( is_object( $field ) ) {
			$settings = $field->get_settings();
			if ( NF_Table_Editor::TYPE !== $settings['type'] ) {
				return $field;
			}
		}

		if ( is_array( $field ) && ( ! isset( $field['settings']['type'] ) || NF_Table_Editor::TYPE !== $field['settings']['type'] ) ) {
			return $field;
		}

		if ( self::$scripts_loaded ) {
			return $field;
		}

		$version = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? time() : NF_Table_Editor()->plugin_version;
		$min     = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$url = plugin_dir_url( NF_Table_Editor()->plugin_file_path );

		$hook = $this->enqueue_handsontable( $url, $version, $min, true );

		wp_enqueue_script( 'nf-te-field-table', $url . 'assets/js/front-end/controllers/fieldTable.js', array(
			$hook,
		), $version );

		self::$scripts_loaded = true;

		return $field;
	}
}

<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

return apply_filters( 'ninja_forms_table_editor_field_settings', array(
	'save_location'  => array(
		'name'    => 'save_location',
		'type'    => 'select',
		'label'   => __( 'CSV Save Location', 'ninja-forms-table-editor' ),
		'options' => apply_filters( 'nf_table_editor_save_locations', array(
			array(
				'value' => 'media',
				'label' => __( 'Media Library', 'ninja-forms-table-editor' ),
			),
			array(
				'value' => 'server',
				'label' => __( 'Ninja Forms folder in wp-content', 'ninja-forms-table-editor' ),
			),
		) ),
		'value'   => 'media',
		'group'   => 'primary',
		'width'   => 'full',
		'help'    => __( 'Configure where the CSV of table data will be saved.', 'ninja-forms-table-editor' ),
	),
	'number_columns' => array(
		'name'  => 'number_columns',
		'type'  => 'number',
		'label' => __( 'Number of columns', 'ninja-forms-table-editor' ),
		'group' => 'primary',
		'value' => apply_filters( 'nf_table_editor_default_columns', 3 ),
		'width' => 'one-half',
		'help'  => __( 'Specify the number of columns for the table', 'ninja-forms-table-editor' ),
	),
	'custom_headers' => array(
		'name'    => 'custom_headers',
		'type'    => 'select',
		'label'   => __( 'Column Headers', 'ninja-forms-table-editor' ),
		'options' => array(
			array( 'value' => 'simple', 'label' => __( 'Simple', 'ninja-forms-table-editor' ) ),
			array( 'value' => 'custom', 'label' => __( 'Custom', 'ninja-forms-table-editor' ) ),
			array( 'value' => 'none', 'label' => __( 'None', 'ninja-forms-table-editor' ) ),
		),
		'value'   => 'simple',
		'group'   => 'primary',
		'width'   => 'one-half',
		'help'    => __( 'Configure the column headers for the table', 'ninja-forms-table-editor' ),
	),
	'header_names'   => array(
		'name'  => 'header_names',
		'type'  => 'textarea',
		'label' => __( 'Custom Header Names', 'ninja-forms-table-editor' ),
		'group' => 'primary',
		'width' => 'full',
		'help'  => __( 'Enter a column header on a new line', 'ninja-forms-table-editor' ),
		'deps'  => array(
			'custom_headers' => 'custom',
		),
	),
	'table_width'    => array(
		'name'    => 'table_width',
		'type'    => 'select',
		'label'   => __( 'Table width', 'ninja-forms-table-editor' ),
		'options' => array(
			array( 'value' => 'stretch', 'label' => __( 'Stretch', 'ninja-forms-table-editor' ) ),
			array( 'value' => 'scroll', 'label' => __( 'Scroll', 'ninja-forms-table-editor' ) ),
			array( 'value' => 'none', 'label' => __( 'None', 'ninja-forms-table-editor' ) ),
		),
		'group'   => 'primary',
		'width'   => 'one-half',
	),
	'drag_down'      => array(
		'name'  => 'drag_down',
		'type'  => 'toggle',
		'help'  => __( 'Allow users to drag cells to repeat values', 'ninja-forms-table-editor' ),
		'label' => __( 'Drag Cells', 'ninja-forms-table-editor' ),
		'value' => '1',
		'group' => 'primary',
		'width' => 'one-half',
	),
	'min_rows'       => array(
		'name'  => 'min_rows',
		'type'  => 'number',
		'label' => __( 'Minimum Rows', 'ninja-forms-table-editor' ),
		'group' => 'primary',
		'width' => 'one-half',
		'value' => apply_filters( 'nf_table_editor_min_rows', 0 ),
		'help'  => __( 'Specify the number of minimum spare rows for the table', 'ninja-forms-table-editor' ),
	),
	'row_headers'    => array(
		'name'  => 'row_headers',
		'type'  => 'toggle',
		'label' => __( 'Row numbers', 'ninja-forms-table-editor' ),
		'help'  => __( 'Show row numbers on the table', 'ninja-forms-table-editor' ),
		'group' => 'primary',
		'width' => 'one-half',
	),
) );

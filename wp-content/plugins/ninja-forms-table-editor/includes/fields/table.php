<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class NF_TE_Fields_Table
 */
class NF_TE_Fields_Table extends NF_Abstracts_Field {

	protected $_nicename = 'Table Editor';
	protected $_parent_type = 'textbox';
	protected $_section = 'common';
	protected $_icon = 'table';
	protected $_templates = 'table-editor';

	protected $_settings_all_fields = array(
		'key',
		'label',
		'label_pos',
		'required',
		'classes',
		'manual_key',
		'help',
		'description',
	);

	public function __construct() {
		$this->_name = NF_Table_Editor::TYPE;
		$this->_type = NF_Table_Editor::TYPE;

		parent::__construct();

		$this->_nicename = __( 'Table Editor', 'ninja-forms-table-editor' );

		$settings = NF_Table_Editor()->config( 'field-settings' );

		$this->_settings = array_merge( $this->_settings, $settings );
	}

	/**
	 * Format data for the front end JS file
	 *
	 * @param array $settings
	 * @param int   $form_id
	 *
	 * @return array
	 */
	public function localize_settings( $settings, $form_id ) {
		$settings['table_width'] = NF_TE_Admin_Handsontable::get_table_width( $settings );
		$settings['tableConfig'] = NF_TE_Admin_Handsontable::get_config( $settings );

		return $settings;
	}

	/**
	 * Admin Form Element
	 *
	 * Returns the output for editing a table in a submission.
	 *
	 * @param $id
	 * @param $value
	 *
	 * @return string
	 */
	public function admin_form_element( $id, $value ) {
		NF_Table_Editor()->template( 'admin-fields-table-editor', array( 'field_id' => $id, 'class' => '' ) );
	}
}
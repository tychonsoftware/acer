<?php

class ninja_forms_table_editor {

	private $plugin_version;

	function __construct( $plugin_version ) {
		$this->plugin_version = $plugin_version;
		add_action( 'init', array( $this, 'register_table_editor' ) );

		add_action( 'ninja_forms_post_process', array( $this, 'ninja_forms_fields_post_process' ), 11 );

		add_action( 'admin_enqueue_scripts', array( $this, 'ninja_forms_admin_js' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'ninja_forms_subs_js' ) );
		add_action( 'ninja_forms_display_js', array( $this, 'ninja_forms_display_js' ), 10, 2 );
		add_action( 'ninja_forms_display_css', array( $this, 'ninja_forms_display_css' ), 10, 2 );

		add_filter( 'ninja_forms_view_sub_td', array( $this, 'ninja_forms_view_sub_td' ), 999, 3 );
		add_filter( 'ninja_forms_field_shortcode', array( $this, 'ninja_forms_field_shortcode' ), 10, 2 );

		add_filter( 'ninja_forms_export_sub_value', array( $this, 'ninja_forms_export_sub_value' ), 10, 2 );

		add_filter( 'nf_subs_export_pre_value', array( $this, 'ninja_forms_export_sub_pre_value' ), 10, 2 );
		add_filter( 'nf_edit_sub_user_value', array( $this, 'do_edit_sub_value' ), 10, 3 );

		add_filter( 'nf_email_notification_attachment_types', array( $this, 'add_attachment_type' ) );
		add_filter( 'nf_email_notification_attachments', array( $this, 'attach_files' ), 10, 2 );

		// Other addons
		add_filter( 'ninja_forms_pdf_pre_user_value', array( $this, 'ninja_forms_pdf_pre_user_value' ) );
		add_filter( 'ninja_forms_pdf_field_value', array( $this, 'ninja_forms_pdf_field_value'), 10, 3 );
		add_filter( 'ninja_forms_pdf_field_value_wpautop', array( $this, 'ninja_forms_pdf_field_value_wpautop'), 10, 3 );

		// Deprecated
		add_filter( 'ninja_forms_email_all_fields_array', array( $this, 'ninja_forms_email_all_fields_array' ), 10, 2 );
		add_action( 'table_editor_post_process', array( $this, 'table_editor_post_process' ) );
	}

	private function nf_subs() {
		return post_type_exists( 'nf_sub' );
	}

	function register_table_editor() {
		$args = array(
			'name'              => 'Table Editor',
			//Required - This is the name that will appear on the add field button.
			'edit_options'      => array( //Optional - An array of options to show within the field edit <li>. Should be an array of arrays.
				array(
					'type'    => 'select',
					'name'    => 'save_location',
					'label'   => '<strong>' . __( 'CSV Save Location', 'ninja-forms-table-editor' ) . '</strong><br> ' .
					             __( 'Configure where the CSV of table data will be saved.', 'ninja-forms-table-editor' ),
					'options' => $this->get_save_locations()
				),
				array(
					'type'  => 'number',
					'name'  => 'number_columns',
					'label' => '<strong>' . __( 'Number of columns', 'ninja-forms-table-editor' ) . '</strong><br> ' .
					           __( 'Specify the number of columns for the table', 'ninja-forms-table-editor' ),
					'default' => apply_filters( 'nf_table_editor_default_columns', 3 )
				),
				array(
					'type'    => 'select',
					'name'    => 'custom_headers',
					'label'   => '<strong>' . __( 'Column Headers', 'ninja-forms-table-editor' ) . '</strong><br> ' .
					             __( 'Configure the column headers for the table', 'ninja-forms-table-editor' ),
					'options' => array(
						array( 'value' => 'simple', 'name' => __( "Simple", 'ninja-forms-table-editor' ) ),
						array( 'value' => 'custom', 'name' => __( "Custom", 'ninja-forms-table-editor' ) ),
						array( 'value' => 'none', 'name' => __( "None", 'ninja-forms-table-editor' ) )
					)
				),
				array(
					'type'  => 'textarea',
					'name'  => 'header_names',
					'label' => '<strong>' . __( 'Custom Header Names', 'ninja-forms-table-editor' ) . '</strong><br> ' .
					           __( 'Enter a column header on a new line', 'ninja-forms-table-editor' ),
				),
				array(
					'type'  => 'number',
					'name'  => 'min_rows',
					'label' => '<strong>' . __( 'Minimum Rows', 'ninja-forms-table-editor' ) . '</strong><br> ' .
					           __( 'Specify the number of minimum spare rows for the table', 'ninja-forms-table-editor' ),
				),
				array(
					'type'  => 'checkbox',
					'name'  => 'row_headers',
					'label' => __( 'Show row numbers on the table', 'ninja-forms-table-editor' ),

				),
				array(
					'type'    => 'select',
					'name'    => 'table_width',
					'label'   => __( 'Table width', 'ninja-forms-table-editor' ),
					'options' => array(
						array( 'value' => 'stretch', 'name' => __( "Stretch", 'ninja-forms-table-editor' ) ),
						array( 'value' => 'scroll', 'name' => __( "Scroll", 'ninja-forms-table-editor' ) ),
						array( 'value' => 'none', 'name' => __( "None", 'ninja-forms-table-editor' ) )
					)
				),
				array(
					'type'    => 'checkbox',
					'name'    => 'drag_down',
					'label'   => __( 'Allow users to drag cells to repeat values', 'ninja-forms-table-editor' ),
					'default' => 1
				),
				array(
					'type'    => 'checkbox',
					'name'    => 'admin_attach',
					'label'   => __( 'Attach CSV of table data to the email sent to the admin user(s)', 'ninja-forms-table-editor' ),
					'default' => 1
				),

			),
			'edit_function'     => '',
			'display_function'  => array( $this, 'ninja_forms_table_editor_display' ),
			'group'             => 'standard_fields',
			'edit_label'        => true,
			'edit_label_pos'    => true,
			'edit_req'          => false,
			'edit_custom_class' => false,
			'edit_help'         => true,
			'edit_desc'         => true,
			'edit_meta'         => false,
			'sidebar'           => 'template_fields',
			'edit_conditional'  => true,
			'conditional'       => array(
				'value' => array(
					'type' => 'text',
				),
			),
			'process_field'     => true,
			//'post_process' => array( $this, 'ninja_forms_post_process'),
			'edit_sub_process'  => array( $this, 'ninja_forms_table_editor_edit_sub_process' ),
			'sub_table_value'   => array( $this, 'sub_table_value' ),
			'edit_sub_value'    => array( $this, 'edit_sub_value' ),
		);


		if ( class_exists( 'NF_PDF_Submission' ) ) {
			$args['edit_options'][] = array(
				'type'    => 'select',
				'name'    => 'pdf_format',
				'label'   => '<strong>' . __( 'Table Format in PDF', 'ninja-forms-table-editor' ) . '</strong><br> ' .
				             __( 'Configure the output format of the table in a PDF', 'ninja-forms-table-editor' ),
				'options' => array(
					array( 'value' => 'slim', 'name' => __( "Slim", 'ninja-forms-table-editor' ) ),
					array( 'value' => 'html', 'name' => __( "HTML", 'ninja-forms-table-editor' ) ),
				),
				'default' => 'slim'
			);
		}

		if ( function_exists( 'ninja_forms_register_field' ) ) {
			ninja_forms_register_field( '_table_editor', $args );
		}
	}

	function get_save_locations() {
		$default_locations = array(
			array( 'value' => 'media', 'name' => __( "Media Library", 'ninja-forms-table-editor' ) ),
			array( 'value' => 'server', 'name' => __( "Ninja Forms folder in wp-content", 'ninja-forms-table-editor' ) )
		);

		$locations = apply_filters( 'nf_table_editor_save_locations', array() );

		return array_merge( $default_locations, (array) $locations );
	}

	function edit_sub_value( $field_id, $user_value, $field ) {
		$data                  = $field['data'];
		$data['default_value'] = $user_value;
		$this->ninja_forms_table_editor_display( $field_id, $data, false, true );
	}

	function ninja_forms_table_editor_display( $field_id, $data, $form_id = false, $admin = false ) {
		if ( $admin ) {
			$admin = $this->nf_subs();
		}
		$field    = $data;

		if ( 1 == $field['table_width'] ) {
			$field['table_width'] = 'stretch';
		}

		$settings = array(
			'number_columns' => $field['number_columns'],
			'row_headers'    => $field['row_headers'],
			'drag_down'      => $field['drag_down'],
			'min_rows'       => ( ( is_numeric( $field['min_rows'] ) ) ? $field['min_rows'] : '0' ),
			'table_width'    => $field['table_width'],
			'parent'         => ( $admin ) ? 'nf-sub-edit-value' : 'table_editor-wrap'
		);
		$settings = htmlspecialchars( json_encode( $settings ) );

		$table_headers = '';
		if ( $field['custom_headers'] && $field['custom_headers'] == 'custom'
		     && $field['header_names'] && $field['header_names'] != ''
		) {
			$headers       = preg_split( '/\r\n|\r|\n/', $field['header_names'] );
			$table_headers = htmlspecialchars( json_encode( $headers ) );
			$table_headers = str_replace( "\r", "", $table_headers );
		} else {
			if ( $field['custom_headers'] && $field['custom_headers'] == 'none' ) {
				$table_headers = 'none';
			}
		}

		$default_data = '';
		if ( isset( $field['default_value'] ) ) {
			$default_data = ( isset( $field['default_value']['raw_data'] ) && $field['default_value']['raw_data'] != '[' ) ? $field['default_value']['raw_data'] : $field['default_value'];
			if ( $default_data != '' && strpos( $default_data, '"' ) !== false ) {
				$default_data = str_replace( '\&quot;', '&quot;', htmlspecialchars( $default_data ) );
				$default_data = str_replace( "\'", '\u0027', $default_data );
			}
		}
		$field_name = ( $admin ) ? 'fields[' . $field_id . ']' : 'ninja_forms_field_' . $field_id;

		if ( $admin ) {
			echo '</td></tr><tr><td colspan="2"><div class="nf-sub-edit-value type-_table_editor">';
		}

		if ( isset( $field['default_value']['csv_attach_id'] ) && $field['default_value']['csv_attach_id'] != '[' ) {
			$attach_name = ( $admin ) ? 'fields[' . $field_id . '_5]' : 'ninja_forms_field_' . $field_id . '_5';
			?>
			<input type="hidden" name="<?php echo $attach_name; ?>" value="<?php echo $field['default_value']['csv_attach_id']; ?>">
		<?php
		}
		?>
		<div id="table_wrapper_<?php echo $field_id; ?>" class="<?php echo ('scroll' === $field['table_width'] ) ? 'table-scroll' : ''; ?>">
			<div id="table_<?php echo $field_id; ?>" class="handsontable_editor"></div>
		</div>
		<input type="hidden" id="table_settings_<?php echo $field_id; ?>" value="<?php echo $settings; ?>">
		<input type="hidden" name="<?php echo $field_name; ?>" id="table_values_<?php echo $field_id; ?>" value="<?php echo $default_data; ?>">
		<input type="hidden" id="table_headers_<?php echo $field_id; ?>" value="<?php echo $table_headers; ?>">
		<?php
		if ( $admin ) {
			echo '</div>';
		}
	}

	private function render_table( $table_data, $field_id ) {
		$field_row = ninja_forms_get_field_by_id( $field_id );
		$field     = $field_row['data'];

		$headers = array();
		if ( $field['custom_headers'] == 'simple' ) {
			for ( $alpha = 65; $alpha < ( 65 + $field['number_columns'] ); $alpha ++ ) {
				$headers[] = chr( $alpha );
			}
		} else {
			if ( $field['custom_headers'] == 'custom' && $field['header_names'] != '' ) {
				$headers = preg_split( '/\r\n|\r|\n/', $field['header_names'] );
			}
		}

		$table_data = str_replace( '\"', '"', $table_data );
		$table_data = str_replace( "\'", "'", $table_data );
		$table_data = stripslashes( $table_data );
		$table_data = json_decode( $table_data );

		if ( $table_data ) {
			$row_count = 0;
			foreach ( $table_data as $tr_key => $tr ) {
				$row_count ++;
				if ( $field['min_rows'] > 0 && $row_count > ( count( $table_data ) - $field['min_rows'] ) ) {
					unset( $table_data[ $tr_key ] );
				};
			}
		} else {
			$table_data = array();
		}

		return array(
			'header' => $headers,
			'body'   => $table_data
		);
	}

	private function write_csv( $file, $field_id, $user_value) {
		$table_data = $this->render_table( $user_value, $field_id );
		if ( count( $table_data['header'] ) > 0 ) {
			$table_data = array_merge( array( $table_data['header'] ), $table_data['body'] );
		}

		// use NF delimiter and enclosure
		$delimiter = apply_filters( 'nf_table_editor_csv_delimiter', apply_filters( 'nf_sub_csv_delimiter', ',' ) );
		$enclosure = apply_filters( 'nf_table_editor_csv_enclosure', apply_filters( 'nf_sub_csv_enclosure', '"' ) );

		// Create CSV of table and Save to uploads
		$fp   = fopen( $file, 'w' );
		foreach ( $table_data as $fields ) {
			fputcsv( $fp, $fields, $delimiter, $enclosure );
		}
		fclose( $fp );
	}

	function ninja_forms_fields_post_process() {
		global $ninja_forms_fields, $ninja_forms_processing;
		//Loop through the submitted form data and call each field's post_processing function, if one exists.
		$form_id       = $ninja_forms_processing->get_form_ID();
		$field_results = ninja_forms_get_fields_by_form_id( $form_id );
		if ( is_array( $field_results ) AND ! empty( $field_results ) ) {
			foreach ( $field_results as $field ) {
				$field_id   = $field['id'];
				$field_type = $field['type'];
				if ( isset( $ninja_forms_fields[ $field_type ] ) && $field_type == '_table_editor' ) {
					$user_value = $ninja_forms_processing->get_field_value( $field_id );
					$user_value = apply_filters( 'ninja_forms_field_post_process_user_value', $user_value, $field_id );
					$this->ninja_forms_table_editor_post_process( $field_id, $field, $user_value );
				}
			}
		}
	}

	function ninja_forms_table_editor_post_process( $field_id, $field_row, $user_value ) {
		if ( $field_row['type'] == '_table_editor' ) {
			$field = $field_row['data'];
			global $ninja_forms_processing;
			$saved_table_data = $this->save_csv( $field_id, $field_row, $user_value );
			$ninja_forms_processing->update_field_value( $field_id, $saved_table_data );
			ninja_forms_save_sub();

			do_action( 'table_editor_post_process', $field );
		}
	}

	private function save_csv( $field_id, $field_row, $user_value, $sub_id = false ) {
		$location  = $this->get_save_location( $field_row );
		$file_name = $this->get_csv_name( $field_row['data']['label'], $sub_id );
		$file      = $this->get_save_path( $location, $file_name );
		//Create CSV
		$this->write_csv( $file, $field_id, $user_value );

		$saved_table_data = array(
			'raw_data' => $user_value
		);

		switch ( $location ) {

			case 'media':
				// Add CSV to media library
				$attachment = array(
					'guid'           => $file,
					'post_mime_type' => 'text/csv',
					'post_title'     => $field_row['data']['label'],
					'post_content'   => '',
					'post_status'    => 'inherit'
				);
				$attach_id  = wp_insert_attachment( $attachment, $file );
				$saved_table_data['csv_url'] = wp_get_attachment_url( $attach_id );
				$saved_table_data['csv_path'] = $file;
				$saved_table_data['csv_attach_id'] = $attach_id;
				break;
			case 'server':
				$saved_table_data['csv_url'] = $this->get_save_url( 'server', $file_name );
				$saved_table_data['csv_path'] = $file;
				break;
		}


		return $saved_table_data;
	}

	private function save_sub_editor( $field_id, $attach_id, $user_value, $sub_id = false ) {
		//Amend CSV
		$field_row = ninja_forms_get_field_by_id( $field_id );
		$file_name = $this->get_csv_name( $field_row['data']['label'], $sub_id );
		$location = $this->get_save_location( $field_row  );
		$file = $this->get_save_path( $location, $file_name );
		$this->write_csv( $file, $field_id, $user_value );

		$saved_table_data = array(
			'raw_data' => htmlspecialchars( $user_value )
		);

		switch ( $location ) {
			case 'media':
				$saved_table_data['csv_url'] = wp_get_attachment_url( $attach_id );
				$saved_table_data['csv_path'] = $file;
				$saved_table_data['csv_attach_id'] = $attach_id;
				break;
			case 'server':
				$saved_table_data['csv_url'] = $this->get_save_url( 'server', $file_name );
				$saved_table_data['csv_path'] = $file;
				break;
		}

		return $saved_table_data;
	}

	function ninja_forms_table_editor_edit_sub_process( $field_id, $user_value ) {
		global $ninja_forms_processing;
		$attach_id = '';
		if ( $ninja_forms_processing->get_field_value( $field_id . '_5' ) ) {
			$attach_id = $ninja_forms_processing->get_field_value( $field_id . '_5' );
			$ninja_forms_processing->remove_field_value( $field_id . '_5' );
		}
		$saved_table_data = $this->save_sub_editor( $field_id, $attach_id, $user_value );
		$ninja_forms_processing->update_field_value( $field_id, $saved_table_data );
	}

	function do_edit_sub_value( $user_value, $field_id, $sub_id ) {
		$form_id = Ninja_Forms()->sub( $sub_id )->form_id;
		if ( isset ( Ninja_Forms()->form( $form_id )->fields[ $field_id ]['type'] ) && Ninja_Forms()->form( $form_id )->fields[ $field_id ]['type'] == '_table_editor' ) {
			$attach_id = '';
			if ( isset( $_POST['fields'][ $field_id . '_5' ] ) ) {
				$attach_id = $_POST['fields'][ $field_id . '_5' ];
				unset( $_POST['fields'][ $field_id . '_5' ] );
			}
			$user_value = $this->save_sub_editor( $field_id, $attach_id, $user_value, $sub_id );
		}

		return $user_value;
	}

	function ninja_forms_view_sub_td( $user_value, $field_id, $sub_id ) {
		$field = ninja_forms_get_field_by_id( $field_id );
		if ( $field['type'] == '_table_editor' && is_array( $user_value ) ) {
			$user_value = ( isset( $user_value['csv_url'] ) ) ? '<a href="' . $user_value['csv_url'] . '">Download CSV</a>' : 'No data';
		}

		return $user_value;
	}

	function ninja_forms_export_sub_value( $user_value, $field_id ) {
		$field = ninja_forms_get_field_by_id( $field_id );
		if ( $field['type'] == '_table_editor' ) {
			if ( has_filter( 'nf_subs_export_pre_value' ) ) {
				// will be used when ninja forms has the filter 'nf_subs_export_pre_value'
				$user_value = ( strpos( $user_value, 'http://' ) !== false ) ? $user_value : '';
			} else {
				$user_value = '';
			}
		}

		return $user_value;
	}

	function ninja_forms_export_sub_pre_value( $user_value, $field_id ) {
		$field = ninja_forms_get_field_by_id( $field_id );
		if ( $field['type'] == '_table_editor' ) {
			$user_value = isset( $user_value['csv_url'] ) ? $user_value['csv_url'] : $user_value;
		}

		return $user_value;
	}

	function sub_table_value( $field_id, $user_value, $field ) {
		if ( $field['type'] == '_table_editor' ) {
			$user_value = isset( $user_value['csv_url'] ) ? $user_value['csv_url'] : $user_value;
		}
		echo '<a target="_blank" href="' . $user_value . '">' . __( 'View CSV', 'nina-forms-table-editor' ) . '</a>';
	}

	function table_html( $user_value, $field_id ) {
		$table_data = $this->render_table( $user_value, $field_id );
		$html       = '<table>';
		if ( $table_data['header'] && count( $table_data['header'] ) > 0 ) {
			$html .= '<thead><tr>';
			foreach ( $table_data['header'] as $th ) {
				$html .= '<th>' . $th . '</th>';
			}
			$html .= '</tr></thead>';
		}
		if ( $table_data['body'] ) {
			foreach ( $table_data['body'] as $tr ) {
				$html .= '<tr>';
				foreach ( $tr as $td_key => $td ) {
					$html .= '<td>' . stripslashes( $td ) . '</td>';
				}
				$html .= '</tr>';
			}
		}
		$html .= '</table>';

		return $html;
	}

	function table_output_slim( $user_value, $field_id ) {
		$table_data = $this->render_table( $user_value, $field_id );
		$html       = '';
		if ( $table_data['header'] && count( $table_data['header'] ) > 0 ) {
			$header = $table_data['header'];
		}
		if ( $table_data['body'] ) {
			foreach ( $table_data['body'] as $tr ) {
				$html .= '<div>';
				foreach ( $tr as $td_key => $td ) {
					$html .= '<label style="font-weight: bold;">' . $header[$td_key] . '</label>';
					$html .= '<br>' . stripslashes( $td );
					$html .= '<br>';
				}
				$html .= '</div><br>';
			}
		}
		return $html;
	}

	function ninja_forms_field_shortcode( $value, $atts ) {
		$field = ninja_forms_get_field_by_id( $atts['id'] );
		if ( $field['type'] == '_table_editor' ) {

			if ( version_compare( NINJA_FORMS_VERSION, '2.8' ) == -1 ) {
				global $ninja_forms_processing;
				$email_type = $ninja_forms_processing->get_form_setting( 'email_type' );
				if ( $email_type == 'html' ) {
					return $this->table_html( $value, $atts['id'] );
				}
			}
			$value = $this->get_saved_csv_url( $atts['id'], $field );

		}

		return $value;
	}

	function ninja_forms_display_js() {
		$version = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? time() : $this->plugin_version;
		$min     = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_script(
			'handsontable',
			NINJA_FORMS_TABLE_EDITOR_URL . '/assets/js/lib/handsontable.full' . $min . '.js',
			array( 'jquery' ),
			$version
		);

		wp_enqueue_script(
			'table-editor',
			NINJA_FORMS_TABLE_EDITOR_URL . '/assets/js/deprecated/table-editor.js',
			array( 'jquery', 'handsontable' ),
			$version
		);
	}

	function ninja_forms_display_css() {
		$version = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? time() : $this->plugin_version;
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		wp_enqueue_style( 'handsontable', NINJA_FORMS_TABLE_EDITOR_URL . '/assets/css/lib/handsontable.full'. $min .'.css', array(), $version );
		wp_enqueue_style( 'table-editor', NINJA_FORMS_TABLE_EDITOR_URL . '/assets/css/deprecated/table-editor.css', array( 'handsontable' ), $version );
	}

	function ninja_forms_admin_js() {

		if ( isset( $_GET['page'] ) && $_GET['page'] == 'ninja-forms' ) {

			wp_enqueue_script(
				'table-editor',
				NINJA_FORMS_TABLE_EDITOR_URL . '/assets/js/deprecated/edit-field.js',
				array( 'jquery' )
			);
		}
	}

	function ninja_forms_subs_js() {
		global $post;

		if ( isset( $post ) && $post->post_type == 'nf_sub' ) {
			$this->ninja_forms_display_js();
			$this->ninja_forms_display_css();
		}
	}

	// Adds an attachment type to the email notification dropdown
	function add_attachment_type( $types ) {
		// Bail if we don't have a form id set.
		if ( ! isset ( $_REQUEST['form_id'] ) ) {
			return $types;
		}

		foreach ( Ninja_Forms()->form( $_REQUEST['form_id'] )->fields as $field_id => $field ) {
			if ( '_table_editor' == $field['type'] ) {
				$label                               = nf_get_field_admin_label( $field_id );
				$types[ 'table_editor_' . $field_id ] = $label . ' - ID: ' . $field_id;
			}
		}

		return $types;
	}

	// Add our attachment to the email notification if the box was checked.
	function attach_files( $files, $id ) {
		global $ninja_forms_processing;

		foreach ( $ninja_forms_processing->get_all_fields() as $field_id => $user_value ) {
			$type = $ninja_forms_processing->get_field_setting( $field_id, 'type' );
			if ( '_table_editor' == $type && 1 == Ninja_Forms()->notification( $id )->get_setting( 'table_editor_' . $field_id ) ) {
				if ( is_array( $user_value ) ) {
					$files[] = $this->get_saved_csv_path( $user_value );
				}
			}
		}

		return $files;
	}

	function get_save_location( $field ) {
		$location = 'media';
		if ( isset( $field['data']['save_location'] ) ) {
			$location = $field['data']['save_location'];
		}

		return $location;
	}

	function get_csv_name( $name, $sub_id = false ) {
		if ( ! $sub_id ) {
			global $ninja_forms_processing;
			$sub_id = $ninja_forms_processing->get_form_setting( 'sub_id' );
		}
		$filename = strtolower( sanitize_file_name( $name ) );

		return $filename . '-' . $sub_id . '.csv';
	}

	function get_save_path( $location, $filename = '' ) {
		if ( 'media' == $location ) {
			$upload_dir = wp_upload_dir();
			$path       = $upload_dir['path'];
		} else {
			$plugin_settings = get_option( 'ninja_forms_settings' );
			$path            = $plugin_settings['base_upload_dir'];
		}

		if ( '' != $filename ) {
			$path = trailingslashit( $path ) . $filename;
		}

		return $path;
	}

	function get_save_url( $location, $filename = '' ) {
		if ( 'media' == $location ) {
			$upload_dir = wp_upload_dir();
			$url       = $upload_dir['url'];
		} else if ('server' == $location ) {
			$plugin_settings = get_option( 'ninja_forms_settings' );
			$url            = $plugin_settings['base_upload_url'];
		}

		if ( '' != $filename ) {
			$url = trailingslashit( $url ) . $filename;
		}

		return $url;
	}

	function get_saved_csv_path( $user_value ) {
		if ( isset( $user_value['csv_path'] ) ) {
			$csv = $user_value['csv_path'];
		} else {
			$csv = get_the_guid( $user_value['csv_attach_id'] );
		}

		return $csv;
	}

	function get_saved_csv_url( $field_id, $field_row = null ) {
		if ( is_null( $field_row ) ) {
			$field_row = ninja_forms_get_field_by_id( $field_id );
		}
		$file_name = $this->get_csv_name( $field_row['data']['label'] );
		$location = $this->get_save_location( $field_row  );

		return $this->get_save_url( $location, $file_name );
	}

	function ninja_forms_pdf_pre_user_value( $user_value ) {
		if ( is_array( $user_value ) && isset( $user_value['raw_data'] ) && isset( $user_value['csv_url'] ) ) {
			$user_value = $user_value['raw_data'];
		}

		return $user_value;

	}
	function ninja_forms_pdf_field_value( $user_value, $field_id, $field ) {
		if ( '_table_editor' == $field['type'] ) {
			if ( isset( $field['data']['pdf_format'] ) && $field['data']['pdf_format'] == 'html' ) {
				$user_value = $this->table_html( $user_value, $field_id );
			} else {
				$user_value = $this->table_output_slim( $user_value, $field_id );
			}
		}

		return $user_value;
	}

	/**
	 * Override the PDF submission using wpautop() on the submitted value
	 * for the Table Editor.
	 *
	 * @param bool  $wpautop
	 * @param array $user_value
	 * @param array $field
	 *
	 * @return bool
	 */
	function ninja_forms_pdf_field_value_wpautop( $wpautop, $user_value, $field ) {
		if ( '_table_editor' == $field['type'] ) {
			return false;
		}

		return $wpautop;
	}

	/**
	 **************************************************************************
	 * Deprecated
	 **************************************************************************
	 */

	function ninja_forms_email_all_fields_array( $tmp_array, $form_id ) {
		foreach ( $tmp_array as $key => $value ) {
			$field = ninja_forms_get_field_by_id( $key );
			if ( $field['type'] == '_table_editor' ) {
				$tmp_array[ $key ] = $this->get_saved_csv_url( $key, $field );
			}
		}

		return $tmp_array;
	}

	function table_editor_post_process( $field ) {
		if ( version_compare( NINJA_FORMS_VERSION, '2.8' ) == -1 ) {
			global $ninja_forms_processing;
			// CSV attached to admin email
			if ( isset( $field['admin_attach'] ) && $field['admin_attach'] == '1' ) {
				$files = $ninja_forms_processing->get_form_setting( 'admin_attachments' );
				array_push( $files, $file );
				$ninja_forms_processing->update_form_setting( 'admin_attachments', $files );
			}
		}
	}
}
<?php
/*
 * Plugin Name: Ninja Forms - Table Editor
 * Plugin URI: http://www.polevaultweb.com/
 * Description: Adds a new Ninja Forms field that is an Excel like grid table editor.
 * Version: 3.0.4
 * Author: polevaultweb
 * Author URI: http://www.polevaultweb.com/
 * Text Domain: ninja-forms-table-editor
 * Domain Path: /languages/
 *
 * Copyright 2013  polevaultweb  (email : info@polevaultweb.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * The main function responsible for returning the one true instance to functions everywhere.
 */
function NF_Table_Editor() {
	// Load our main plugin class
	require_once dirname( __FILE__ ) . '/includes/table-editor.php';
	$plugin_version = '3.0.4';

	return NF_Table_Editor::instance( 'NF_Table_Editor', __FILE__, $plugin_version );
}

NF_Table_Editor();

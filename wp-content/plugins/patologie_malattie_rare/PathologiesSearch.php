<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Patologie Malattie Rare
 * Description:       Search/Extract Data From Pathologies DataStore
 * Version:           1.00
 * Text Domain:       patologie_malattie_rare
 */

if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/SearchFunctions.php');	
define("PMR_URL", plugin_dir_url(dirname(__FILE__) . '/PathologiesSearch.php'));

if (!class_exists('PatologieMalattieRare')) {
    class PatologieMalattieRare

    {
        var $version;
        var $minium_WP   = '3.1';
        
        function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->define_tables();
            $this->load_dependencies();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('PathologiesSearch', 'activate'));
            register_deactivation_hook(__FILE__, array('PathologiesSearch', 'deactivate'));
            register_uninstall_hook(__FILE__, array('PathologiesSearch', 'uninstall'));
            add_action('wp_enqueue_scripts', array(&$this, 'PathologiesSearch_Enqueue_Scripts'));
        }

        function load_dependencies()
        {
            if (is_admin()) {
                require_once(dirname(__FILE__) . '/admin/admin.php');
            }
        }

        function define_tables()
        {
            global $wpdb, $table_prefix;
            $wpdb->table_name_Patologie_Malattie_Rare = $table_prefix  . "patologie_malattie_rare";
        }

        static function init()
	    {

        }
        static function screen_option()
	    {
            
        }

        static function dizionari_set_option($status, $option, $value)
        {
        }

        static function PathologiesSearch_Enqueue_Scripts($hook_suffix)
		{
            $path = plugins_url('', __FILE__);
            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-tabs', '', array('jquery'));
            wp_enqueue_script('jquery-ui-dialog', '', array('jquery'));
            wp_enqueue_script('jquery-ui-datepicker', '', array('jquery'));
              wp_enqueue_script( 'bootstrap-min', $path . '/js/bootstrap.min.js'
              , array(), true );

            wp_enqueue_script('pathologiessearch', $path . '/js/pathologiessearch.js', array('jquery'));
            wp_enqueue_style( 'bootstrap',  $path . '/css/bootstrap.min.css' );

            wp_enqueue_style('jquery.ui.theme', $path . '/css/jquery-ui-custom.css');
            wp_register_style('pathologiessearch', $path . '/css/style.css');
            wp_enqueue_style('pathologiessearch');
        }
        
        static function activate()
        {
            global $wpdb;
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);
        }

        
        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
        }
        static function uninstall()
        {
        }
    }
    global $PMR_OnLine;
    $PMR_OnLine = new PatologieMalattieRare();
}

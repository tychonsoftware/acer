<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {
    die( 'You are not allowed to call this page directly.' );
}

if ( isset( $_REQUEST['action'] ) ) {
} else {
    if ( isset( $_REQUEST['annullaricerca'] ) ) {
        unset( $_REQUEST['CodiceMalattia'] );
        unset( $_REQUEST['NomeMalattia'] );
        VisualizzaPatologieRicerca();
    }
    VisualizzaPatologieRicerca();
}
function VisualizzaPatologieRicerca( $filter = false ) {
    error_log('Request ' . print_r($_REQUEST,1));
    if ( $_REQUEST ) {
        $_REQUEST = array_map( 'sanitizerequest', $_REQUEST );
    }

    $items = get_pathologies();
    ?>
    <?php include 'search/form.phtml' ?>

    <div style="margin-top:20px;" class="alignfull">
        <?php
        if ( isset( $items ) && count($items)> 0 ) {
            $index = 0;
            foreach ($items as $key => $data) {
                // error_log($key . '............................ ' . print_r($data,1));
                ?>

                <div class="pathcategory toggle-<?php echo $index ?>" style="font-weight: bold"> <?php echo $key ?></div>
                <table class="wp-list-table widefat striped posts toggleable-<?php echo $index ?>" style="width:100%;table-layout: fixed;">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 7%;text-align: center!important;">CODICE MALATTIA</th>
                        <th scope="col" style="width: 15%;text-align: center!important;">MALATTIA E/O GRUPPO</th>
                        <th scope="col" style="width: 10%;text-align: center!important;">SINONIMI</th>
                        <th scope="col" style="width: 8%;text-align: center!important;">MEDICO CERTIFICATORE</th>
                        <th scope="col" style="width: 20%;text-align: center!important;">PROCEDURA ACCESSO ALLA VISITA</th>
                    </tr>
                    </thead>
                    <tbody id="the-list">
                    <?php foreach ($data as $key => $item) {
                        $index++;
                        ?>
                        <tr class="iedit author-self level-0 type-post status-publish format-standard hentry category-annunci">
                            <td align="center" style="width: 10%">
                                <?php echo $item -> codice; ?>
                            </td>
                            <td align="center" style="width: 25%">
                                <?php echo $item -> patologia; ?>
                            </td>
                            <td align="center" style="width: 10%">
                                <?php echo $item -> sinonimi; ?>
                            </td>
                            <td align="center" style="width: 15%">
                                <?php echo $item -> medico_certificatore; ?>
                            </td>
                            <td align="center" style="width: 20%">
                                <button class="procedura_accesso_certificazione"
                                        onclick="openPathPopop( '<?php echo $item -> proc_accesso_cert; ?>')"
                                    style="font-size: 15px;">Procedura accesso certificazione</button>
                            </td>
                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            }
        }
        ?>
        <div class="clearfix"></div>
    </div>
    <?php
}

?>


<!--  <script> -->
<!--                     jQuery(document).ready(function() { -->
<!--                         jQuery('.toggle-<?php echo $index ?>').click(function() { -->
<!--                             jQuery(this).siblings(".toggleable-<?php echo $index ?>").toggle(); -->
<!--                             jQuery('.toggle-<?php echo $index ?> i').toggleClass("down right"); -->
<!--                         }); -->
<!--                     }); -->
<!--                 </script> -->
<div class="modal fade" id="proceduraAccessoCertificazioneModal" tabindex="-1" role="dialog"
     aria-labelledby="proceduraAccessoCertificazioneModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ProceduraAccessoCertificazioneLabel">Procedura accesso certificazione</h5>
                <button type="button" class="close btn-sm" data-dismiss="modal" aria-label="Close"
                        onclick="jQuery('#proceduraAccessoCertificazioneModal').modal('toggle');">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 1rem !important;">
                <div id="proceduraAccessoCertificazioneText"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"
                        onclick="jQuery('#proceduraAccessoCertificazioneModal').modal('toggle');">Chiudi</button>
            </div>
        </div>
    </div>
</div>
</div>

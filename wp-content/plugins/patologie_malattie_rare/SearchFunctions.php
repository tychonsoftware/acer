<?php
/**
 * Functions necessary to the plugin
 * @package    Patologie Malattie Rare
 */
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }
################################################################################
// Funzioni 
################################################################################
add_shortcode('patologie_malattie_rare', 'VisualizzaPathologySearch');
function VisualizzaPathologySearch($Parametri)
{
    $ret = "";
    $Parametri = shortcode_atts(array('per_page' => '1'), $Parametri, "patologie_malattie_rare");
    require_once(dirname(__FILE__) . '/admin/search.php');
    return $ret;
}


function get_pathologies($DaRiga=0,$ARiga=200) {
    global $wpdb;

    if ($DaRiga==0 AND $ARiga==0)
        $Limite="";
    else
        $Limite=" Limit ".$DaRiga.",".$ARiga;
    $where = array_merge([], getWhereConditionFromSearch());

    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    $sql = <<<SQL
SELECT SQL_CALC_FOUND_ROWS p.*
FROM $wpdb->table_name_Patologie_Malattie_Rare p
$Selezione
SQL;
    $results = $wpdb->get_results( $sql);

    //error_log($sql . " > Data :::::::::: " . print_r($results,1));

    $groupedData = array();
    foreach($results as $entity)
    {
        if(!isset($groupedData[$entity->categoria]))
        {
            $groupedData[$entity->categoria] = array();
        }
        $groupedData[$entity->categoria][] = $entity;
    }
    // error_log("Grouped Data :::::::::: " . print_r($groupedData,1));
    return $groupedData;
}


function getWhereConditionFromSearch() {
    $request = getPathologySearchRequest();

    $NomeMalattia = $request['NomeMalattia'] ?? '';
    $CodiceMalattia = $request['CodiceMalattia'] ?? '';
    $where = [];

    if ($CodiceMalattia!= ''){
        $where[] = 'codice like "%'.$CodiceMalattia.'%"';
    }

    if ($NomeMalattia!= ''){
        $where[] = 'patologia like "%'.$NomeMalattia.'%"';
    }
    return $where;
}


/**
 * @return array
 */
function getPathologySearchRequest(): array {

    $request = $_REQUEST;

    if($request != null){
        $request = array_map( 'sanitizerequest', $request );
    }else {
        $request = array();
    }
    return $request;
}


function sanitizerequest($value){
    global $wpdb;
    if (is_array($value)) {
        return array_map('sanitizerequest', $value);
    }
    if (is_string($value)) {
        $value = trim($value);
    }
    return $wpdb->_real_escape( $value );
}

add_action( 'plugins_loaded', function () {
    global $wpdb;
    $prefix = $wpdb->prefix;
    $table_name = $prefix . 'patologie_malattie_rare';

    $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

    if ( ! $wpdb->get_var( $query ) == $table_name ) {
        $charset = $wpdb->get_charset_collate();
        $sql = "
    CREATE TABLE IF NOT EXISTS $table_name(
        id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        categoria VARCHAR(255),
        codice VARCHAR(255),
        patologia VARCHAR(255),
        sinonimi VARCHAR(255),
        azienda_ospedaliera VARCHAR(255),
        referente_aziendale VARCHAR(255),
        contatti_referente_aziendale VARCHAR(255),
        dipartimento VARCHAR(255),
        medico_certificatore varchar(255),
        email VARCHAR(50),
        telefono VARCHAR(50),
        fax VARCHAR(50),
        proc_accesso_cert TEXT,
        PRIMARY KEY (id)
    ) $charset;
    ";
	$wpdb->query( $sql );
    }
} );

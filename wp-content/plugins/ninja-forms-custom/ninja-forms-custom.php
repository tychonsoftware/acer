<?php if (!defined('ABSPATH')) {
    exit;
}

/*
 * Plugin Name: Ninja Forms - Custom
 * Plugin URI:
 * Description:
 * Version: 3.0.0
 * Author:
 * Author URI:
 * Text Domain: ninja-forms-custom
 *
 * Copyright 2020 .
 */

if (version_compare(get_option('ninja_forms_version', '0.0.0'), '3', '<')
    || get_option('ninja_forms_load_deprecated', false)
) {

    //include 'deprecated/ninja-forms-custom.php';

} else {

    /**
     * Class NF_Custom
     */
    final class NF_Custom
    {
        const VERSION = '0.0.1';
        const SLUG = 'custom';
        const NAME = 'Custom';
        const AUTHOR = '';
        const PREFIX = 'NF_Custom';

        /**
         *  Namespace
         *
         * @var string
         */
        protected $namespace = 'submission/v1';

        /**
         * @var NF_Custom
         * @since 3.0
         */
        private static $instance;

        /**
         * Plugin Directory
         *
         * @since 3.0
         * @var string $dir
         */
        public static $dir = '';

        /**
         * Plugin URL
         *
         * @since 3.0
         * @var string $url
         */
        public static $url = '';

        /**
         * Main Plugin Instance
         *
         * Insures that only one instance of a plugin class exists in memory at any one
         * time. Also prevents needing to define globals all over the place.
         *
         * @return NF_Custom Highlander Instance
         * @since  3.0
         * @static
         * @static var array $instance
         */
        public static function instance()
        {
            if (!isset(self::$instance)
                && !(self::$instance instanceof NF_Custom)
            ) {
                self::$instance = new NF_Custom();
                self::$instance->ninja_forms_users_column();
                self::$dir = plugin_dir_path(__FILE__);

                self::$url = plugin_dir_url(__FILE__);

                /*
                 * Register our autoloader
                 */
                spl_autoload_register(array(self::$instance, 'autoloader'));
            }

            return self::$instance;
        }

        public function __construct()
        {
				
            /*
             * Required for all Extensions.
             */
            add_action('admin_init', function () {
                $this->setup_license();
                new NF_Custom_Display_SubmissionDownloadColumn();
                new NF_Custom_Display_SubmissionModifyLink();
                new NF_Custom_Admin_Download_AllSubAttachmentsZip();
                new NF_Custom_Admin_Submission_AdditionalData();
                new NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniSection();
            });
			
			// Call our register merge tags method.	
            add_action('ninja_forms_loaded',
                array($this, 'register_mergetags'));
				
            add_filter('ninja_forms_do_soap_request_filter', function (
                $data,
                $form_data,
                $action_settings,
                $pdf_template_dat
            ) {
                if (!isset($pdf_template_dat)
                    || $pdf_template_dat->web_service_id != 2
                ) {
                    return $data;
                }
                $service = new NF_Custom_Webservices_ComuneTerni($form_data,
                    $action_settings, $pdf_template_dat);


                return $service->makeRequest();
            }, 10, 4);

            add_filter('ninja_forms_payment_soap_filter', function (
                $form_data
            ) {
                $form_id =  $form_data[ 'form_id' ];
                $pdfTemplate = new PDFTemplate;
                $pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($form_id);
                
                $pago_pa_service = new NF_Custom_Webservices_PagoPA($form_data, $pdf_template_dat);
               return $pago_pa_service->makeRequest();
            }, 10, 1);

            add_action('ninja_forms_after_submission', function ($form_data) {
                NF_Custom_Actions_AfterSubmission_AddStatus::process($form_data);
            });

            add_action('parse_request', 'payment_response_handler');
            
            function payment_response_handler() {
                if( isset( $_GET['idSession'] ) && isset( $_GET['esito'] ) ) {
                    global $wpdb;
                    $post_id = $wpdb->get_var( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE `meta_value` = '{$_GET['idSession']}'" );
                    $form_id = get_post_meta($post_id, '_form_id', true);
                    $payment_outcome_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_outcome' AND `parent_id` = {$form_id}" );
                    update_post_meta( $post_id, '_field_' . $payment_outcome_id ,  $_GET['esito'] );
                }
             }
			
			wp_enqueue_script('sweetalert',
                    'https://cdn.jsdelivr.net/npm/sweetalert2@9', array(), '1.0.0', 'all');
                
			wp_enqueue_script('nf_custom_js',
                    plugins_url('/js/nf-custom.js', __FILE__),
                    array('sweetalert', 'jquery'), '1.0.0', 'all');
            
            wp_enqueue_script( 'nf-bootstrap-min', plugins_url('/js/bootstrap.min.js', __FILE__)
              , array(), true );

			wp_localize_script('nf_custom_js', 'nf_custom_js',
                    array('ajax_url' => admin_url('admin-ajax.php')));
			
			wp_enqueue_script('nf_regex_validate',
                    plugins_url('/js/regex_validate.js', __FILE__), array('nf-front-end'));

            wp_enqueue_script('nf_download_js',
                    plugins_url('/js/download.js', __FILE__));

            add_action('admin_enqueue_scripts', function () {
                wp_enqueue_style('admin-styles', plugins_url('/css/admin.css', __FILE__));
            });

            wp_enqueue_style('nf-bootstrap', plugins_url('/css/bootstrap.min.css', __FILE__));

            add_shortcode('my_submission', function ($atts) {
                wp_enqueue_style('my_submission',
                    plugins_url('/includes/Shortcode/MySubmission/my-submission-list.css',
                        __FILE__), array(), '1.0.0', 'all');

                wp_enqueue_script('sweetalert',
                    'https://cdn.jsdelivr.net/npm/sweetalert2@9', array(), '1.0.0', 'all');
                
				wp_enqueue_script('nf_custom_js',
                    plugins_url('/js/nf-custom.js', __FILE__),
                    array('sweetalert', 'jquery'), '1.0.0', 'all');
                
				wp_localize_script('nf_custom_js', 'nf_custom_js',
                    array('ajax_url' => admin_url('admin-ajax.php')));
			
			return (new NF_Custom_Shortcode_MySubmission($atts))->toHtml();
            });

            add_shortcode('form_button', function ($atts) {
                return (new NF_Custom_Shortcode_FormButton($atts))->toHtml();
            });

            add_shortcode('http_button', function ($atts) {
                return (new NF_Custom_Shortcode_HttpButton($atts))->toHtml();
            });

            add_shortcode('custom_list', function ($atts) {
                wp_enqueue_style('custom_list',
                plugins_url('/includes/Shortcode/custom-list.css',
                    __FILE__), array(), '1.0.0', 'all');
                return (new NF_Custom_Shortcode_CustomList($atts))->toHtml();
            });

            add_action('init', function () {
                new NF_Custom_Display_ShowForm();
                new NF_Custom_Permissions_SubmissionReader();
                new NF_Custom_Admin_Translate();
            }, 4);
			
			add_action( 'my_ninja_forms_processing', 'my_ninja_forms_processing_callback' );

            add_action('rest_api_init', function () {
                register_rest_route($this->namespace, '/request_status', array(
                    'methods' => 'POST',
                    'callback' => array($this, 'update_submission_func')
                ));
            });
			
			add_action('wp_ajax_nf_custom_rinuncia', function () {
                $subID = intval($_POST['sub_id']);
                (new NF_Custom_WPAjax_Rinuncia($subID))->process();
            });
            add_action('wp_ajax_nf_allegati_scarica_remove', function () {
                //unlink('Scarica.zip');
               $zip_name = "/wp-content/uploads/ninja-forms/Scarica.zip";
               unlink("..".$zip_name);

            });

            add_action('wp_ajax_nf_remove_documento_finale', function () {

                $res = delete_post_meta($_POST['id'], $_POST['slug'], '');
                return $res;
            });
            
            add_action('wp_ajax_nf_allegati_scarica', function () {
                $files = explode(",",$_POST['files']);
                //$files = array('readme.txt', 'test.html', 'image.gif');
                $localIP = "http://".getHostByName(getHostName());
				$zip = new ZipArchive();
                $zip_name = "Scarica.zip"; // Zip name
                $zip_name = "/wp-content/uploads/ninja-forms/Scarica.zip"; // Zip name
                $zip->open("..".$zip_name,  ZipArchive::CREATE);
                foreach ($files as $file) {
                  $path = $file;
				 // $path = str_replace(home_url(),$localIP,$path);
                  if($path){
					$zip->addFromString(basename($file),  file_get_contents($file));  
                  }
                  else{
                   echo"file does not exist";
                  }
                }
                
                $zip->close();
                
				echo home_url().$zip_name;
                die;
                //(new NF_Custom_WPAjax_Rinuncia())->download($files);
            });

            add_action('wp_ajax_nf_custom_external_app', function () {
                $user = wp_get_current_user();
                $auth_token = '';
                global $wpdb;
                $prefix = $wpdb->prefix;
                $table_name = $prefix . 'login_tokens';
                $sql = "SELECT * FROM $table_name WHERE user_id=$user->ID";
                $results = $wpdb->get_results($sql);
                if(count($results) > 0){
                    $date_now = new DateTime();
                    error_log('Exp date ' . print_r($date_now,1));
                    if($date_now->date <= $results[0]->auth_token_expiration){
                        $auth_token = $results[0]->auth_token;
                    }
                }
                if(empty($auth_token)){
                    $result['logout_url'] = wp_logout_url();
                }
                $result['auth_token'] = $auth_token;
                wp_send_json($result);
            });


            add_action('wp_ajax_nf_aggiungi_allegati', function () {

                $subID = intval($_POST['sub_id']);
                $formId = intval($_POST['form_id']);
                global $wpdb;
                // $field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE type = 'file_upload' AND `parent_id` = {$formId}" );
                // if(!$field_id){
                    $randomNumber = rand(pow(10, 10 - 1) - 1, pow(10, 10) - 1);
                    $wpdb->query( $wpdb->prepare(
                        "
                        INSERT INTO {$wpdb->prefix}nf3_fields ( `label`, `key`, `type`, `parent_id`, `created_at`)
                            VALUES (%s, %s,%s,%d,CURRENT_TIMESTAMP)
                        ", $formId . '_upload_field_',$formId . '_upload_field_'. $randomNumber,'file_upload',$formId
                    ));
                    $field_id = $wpdb->insert_id;
                //}
                    $base_url = wp_upload_dir();
			        $base_upload_dir = $base_url['basedir'] . '/ninja-forms';
                    $file_path = apply_filters( 'ninja_forms_uploads_dir', $base_upload_dir, $field_id ) . '/' . $formId . '/allegati' ;
                    $files = $_FILES['file'];
                    if(!empty($files)){
                        $filename = $files['name'];
                        if(file_exists($file_path . '/'. $filename)){
                            $path_parts = pathinfo($files['name']);
                            $filename = $path_parts['filename'].'_'.time().'.'.$path_parts['extension'];
                        }
                        $file = array(
                            'name'     => $filename,
                            'type'     => $files['type'],
                            'tmp_name' => $files['tmp_name'],
                            'error'    => $files['error'],
                            'size'     => $files['size']
                        );
    
                        if (!file_exists($file_path)) {
                            mkdir($file_path, '0777', true);
                        }
                        move_uploaded_file( $file['tmp_name'], $file_path . '/'. $file['name']);

                        $query = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME ='%s'";
                        $next_id = $wpdb->get_var($wpdb->prepare($query,$wpdb->prefix . "ninja_forms_uploads"));
                      
                        $user = wp_get_current_user();
                        $tmp_array = array(
                            'user_file_name' => $file['name'],
                            'file_name' => $file['name'],
                            'file_path' => $file_path . '/'. $file['name'],
                            'file_url' => $base_url['baseurl']. '/' . 'ninja-forms' .'/' . $formId . '/allegati/'. $file['name'],
                            'complete' => 0,
                             'upload_location' => 'server',
                             "upload_id" => $next_id,
                        );
                        $wpdb->insert( $wpdb->prefix . "ninja_forms_uploads", array('user_id' => $user->ID, 'form_id' => $formId,
                         'field_id' => $field_id, 'data' => serialize($tmp_array)) );
                         add_post_meta($subID, '_field_' . $field_id, $tmp_array, true);
                    }
                    
                    return 0;
                    
            });

            add_action('wp_ajax_nf_get_payment_status', function () {

                $sub_id = intval($_POST['sub_id']);
                $form_id = intval($_POST['form_id']);
                error_log($form_id . "Inside nf_get_payment_status" . $sub_id);
                $pdfTemplate = new PDFTemplate;
                $pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($form_id);
                $pago_pa_service = new NF_Custom_Webservices_PagoPA(Ninja_Forms()->form( $form_id ), $pdf_template_dat);
                $response = $pago_pa_service->checkStatus($sub_id, $form_id);
                wp_send_json($response);
            });

			add_filter( 'ninja_forms_localize_fields', array( $this, 'filter_default_setting' ) );
			
			
			/*add_action( 'nf_get_form_id', function( $form_id ){
				
				  // Check for a specific Form ID.
				if( FORM_ID_PHASE_2 !== $form_id ) return;
			});*/
            /*
             * Optional. If your extension creates a new field interaction or display template...
             */
            // add_filter( 'ninja_forms_register_fields', array($this, 'register_fields'));

            /*
             * Optional. If your extension processes or alters form submission data on a per form basis...
             */
            add_filter( 'ninja_forms_register_actions', array($this, 'register_actions'));

            /*
             * Optional. If your extension collects a payment (ie Strip, PayPal, etc)...
             */
            // add_filter( 'ninja_forms_register_payment_gateways', array($this, 'register_payment_gateways'));
        }
		
		
		function my_ninja_forms_processing_callback( $form_data ){
			$form_id       = $form_data[ 'form_id' ];
			return $actions;
		}

		public function filter_default_setting( $field )
		{
			$settings = is_object( $field ) ? $field->get_settings() : $field['settings'];
			
			if($settings['key']=='comune_di_residenza_1596794837959'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$comune = $this->getValueFromTag("di essere residente nel Comune di");
				if(!empty($comune)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $comune) == 0)
								$data["selected"]=1;
					}
				}
			}

			if($settings['key']=='ad_alimentazione_1628659442578'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("ad_alimentazione_1595517502991");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
					}
				}
			}

            if($settings['key']=='classe_ambientale_1628660498956'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("classe_ambientale_1595517717269");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
					}
				}
			}

            if($settings['key']=='intestazione_veicolo_1628660691070'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("intestazione_veicolo_1595517948385");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
					}
				}
			}

            if($settings['key']=='comune_di_residenza_intestatario_1628661849279'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("comune_di_residenza_intestatario_1595518288707");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
					}
				}
			}

            if($settings['key']=='grado_di_parentela_tra_il_richiedente_il_contributo_e_il_proprietario_del_veicolo_da_rottamare_1628662013117'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("grado_di_parentela_tra_il_richiedente_il_contributo_e_il_proprietario_del_veicolo_da_rottamare_1596087801782");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data)
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
					}
				}
			}
            
            if($settings['key']=='ad_alimentazione_1628662163882'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("ad_alimentazione_1595518957324");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data){
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
                        }
					}
				}
			}

            if($settings['key']=='ad_alimentazione_1630047993724'){
				$options[] = &$field[ 'settings' ][ 'options' ];
				$tag_value = $this->getValueFromMultipleTag("ad_alimentazione_1595518957324");
				if(!empty($tag_value)){
					foreach ($options as &$option) {
						foreach($option as &$data){
							if (strcasecmp($data["value"], $tag_value) == 0)
								$data["selected"]=1;
                        }
					}
				}
			}

            if($settings['key']=='datanascita_1630042831086'){
				$tag_value = $this->getValueFromMultipleTag("data_di_nascita_1594726068041");
                $field['settings']['value'] =  $tag_value;
            }

			return $field;	
		}
		
        public function getValueFromTag($tagLabel) {
			global $wpdb;
			
			$formId = FORM_ID_PHASE_1;
			$phase2AcceptedLabel = NF_Custom_MergeTags_TAGS::PHASE_2_ACCEPTED;
			
			$returnValue = "";
			
			$current_user = wp_get_current_user();
			
			if($current_user){
			
				$query = "SELECT * FROM {$wpdb->prefix}posts WHERE post_author=$current_user->ID AND ID IN (SELECT post_id FROM	{$wpdb->prefix}postmeta WHERE 
										meta_key='state' AND meta_value='$phase2AcceptedLabel')";

				$postArray = $wpdb->get_results($query);
				//error_log("POST_ARRAY:".print_r($postArray,1));
				
				if(!empty($postArray) && count($postArray)>0){
					$postId = $this->getMaxPostId($postArray);
					//error_log("POST ID: $postId");
					$query = "SELECT * FROM {$wpdb->prefix}nf3_fields WHERE label = '$tagLabel' AND parent_id = ".$formId;
					//error_log("QUERY2: $query");
					$getFieldsNumber = $wpdb->get_results($query);
					//error_log("getFieldsNumber: ".print_r($getFieldsNumber,1));
					
					if(!empty($getFieldsNumber) && count($getFieldsNumber)>0){
						$customQuery = $this->customQuery($getFieldsNumber);
							
						$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key IN $customQuery and post_id = $postId";
						//error_log("QUERY3: $query");
						$resultValue = $wpdb->get_results($query);
						//error_log("resultValue: ".print_r($resultValue));
						
						if(!empty($resultValue) && count($resultValue)>0){
							$returnValue = $resultValue[0]->meta_value;
						}
					}
				}
			}
			//error_log("RETURN VALUE: $returnValue");
			return $returnValue;
		} 

		public function getValueFromMultipleTag($tagkey) {
			global $wpdb;
			
			$formId = FORM_ID_PHASE_1;
			$phase2AcceptedLabel = NF_Custom_MergeTags_TAGS::PHASE_2_ACCEPTED;
			
			$returnValue = "";
			
			$current_user = wp_get_current_user();
			
			if($current_user){
			
				$query = "SELECT * FROM {$wpdb->prefix}posts WHERE post_author=$current_user->ID AND ID IN (SELECT post_id FROM	{$wpdb->prefix}postmeta WHERE 
										meta_key='state' AND meta_value='$phase2AcceptedLabel')";

				$postArray = $wpdb->get_results($query);
				//error_log("POST_ARRAY:".print_r($postArray,1));
				
				if(!empty($postArray) && count($postArray)>0){
					$postId = $this->getMaxPostId($postArray);
					//error_log("POST ID: $postId");
					$query = "SELECT * FROM {$wpdb->prefix}nf3_fields WHERE `key` = '$tagkey' AND parent_id = ".$formId;
					//error_log("QUERY2: $query");
					$getFieldsNumber = $wpdb->get_results($query);
					//error_log("getFieldsNumber: ".print_r($getFieldsNumber,1));
					
					if(!empty($getFieldsNumber) && count($getFieldsNumber)>0){
						$customQuery = $this->customQuery($getFieldsNumber);
							
						$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key IN $customQuery and post_id = $postId";
						//error_log("QUERY3: $query");
						$resultValue = $wpdb->get_results($query);
						//error_log("resultValue: ".print_r($resultValue));
						
						if(!empty($resultValue) && count($resultValue)>0){
							$returnValue = $resultValue[0]->meta_value;
						}
					}
				}
			}
			//error_log("RETURN VALUE: $returnValue");
			return $returnValue;
		}
		
		public function getMaxPostId($data)
		{
			//error_log("getMaxPostId");
			$max = -1;
			foreach ($data as $index => $field) {
				if($max<$field->ID){
					$max = $field->ID;
				}
			}
			return $max;
		}

        /**
         * @param WP_REST_Request $request
         *
         * @return stdClass
         */
        public function update_submission_func(WP_REST_Request $request)
        {
            global $wpdb;
            $result = new stdClass();
            $body = $request->get_json_params();

            if (!isset($body['request_id']) || empty($body['request_id'])
                || !isset($body['state'])
                || empty($body['state'])
            ) {
                $result->status = 'ko';
                $result->error
                    = 'Uno dei campi  request_id o state risulta non valorizzato';
                return $result;
            }

            $requestId = $body['request_id'];
            $state = $body['state'];
            $note = isset($body['note']) ? $body['note'] : '';
            $year = substr($requestId, 0, 4);
            $protocolNumber = substr($requestId, 4, strlen($requestId) - 4);

            /* Get all field_ids have label named 'protocolnumber' */
            $getFieldsNumber
                = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}nf3_fields WHERE label = 'protocolnumber'");
            $customQuery = $this->customQuery($getFieldsNumber);
            $getPostMetaNumber
                = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key IN {$customQuery} AND meta_value = {$protocolNumber}");
            $postIdsFromNumber = [];
            foreach ($getPostMetaNumber as $postMeta) {
                $postIdsFromNumber[] = $postMeta->post_id;
            }

            /* Get all field_ids have label named 'protocoldate' */
            $getFieldsDate
                = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}nf3_fields WHERE label = 'protocoldate'");
            $customQuery = $this->customQuery($getFieldsDate);
            $getPostMetaDate
                = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key IN {$customQuery}");
            $postIdsFromDate = [];
            foreach ($getPostMetaDate as $index => $postMeta) {
                if (!$postMeta->meta_value) {
                    continue;
                }
                $post_year = date("Y", strtotime($postMeta->meta_value));
                if ($post_year == $year) {
                    $postIdsFromDate[] = $postMeta->post_id;
                }
            }
            $postIdsFromNumber = array_unique($postIdsFromNumber);
            $postIdsFromDate = array_unique($postIdsFromDate);
            $postIds = array_intersect($postIdsFromNumber, $postIdsFromDate);

            if (!count($postIds)) {
                $result->status = 'ko';
                $result->error = 'Nessuna richiesta trovata per il request _id';
                return $result;
            } else {
                /* Update new value */
                foreach ($postIds as $postId) {
                    update_post_meta($postId, 'state', $state);
                    update_post_meta($postId, 'note', $note);
                }
                $result->status = 'ok';
                $result->error = '';
                return $result;
            }
        }

        /**
         * @param $data
         *
         * @return string
         */
        public function customQuery($data)
        {
            $result = '(';
            foreach ($data as $index => $field) {
                $result .= '\'_field_' . $field->id . '\'';
                if ($index == count($data) - 1) {
                    continue;
                }
                $result .= ',';
            }
            $result .= ')';
            return $result;
        }

        private function ninja_forms_users_column(){
            global $wpdb;
            $table_name = $wpdb->prefix . 'nf_pdf_template';
            $check_column = (array) $wpdb->get_results(  "SELECT count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME = '{$table_name}' AND COLUMN_NAME = 'user_ids'")[0];
            $check_column = (int) array_shift($check_column);
            if($check_column == 0) {
                $wpdb->query("ALTER TABLE $table_name ADD COLUMN `user_ids` VARCHAR(255) NULL");
            }
        }

        public function register_mergetags()
        {
            Ninja_Forms()->merge_tags['merge_tag_nfcustom']
                = new NF_Custom_MergeTags_SPID();

            $tagPhase = new NF_Custom_MergeTags_TAGS();
            Ninja_Forms()->merge_tags['merge_tag_nfcustom_tagphase'] = $tagPhase;

            $tagPhase->addMergeTag("{user:protocolnumber}","protocolnumber", 2);
            $tagPhase->addMergeTag("{user:protocoldate}","protocoldate", 2);
            $tagPhase->addMergeTag("{user:emailricezione}","Email per ricezione della ricevuta", 2);
            $tagPhase->addMergeTag("{user:telefonocomunicazioni}","Telefono", 2);
            $tagPhase->addMergeTag("{user:indirizzoresidenza}","Indirizzo", 2);
           
            $tagPhase->addMergeTag("{user:protocolnumber_fase_2}","protocolnumber", 3);
            $tagPhase->addMergeTag("{user:protocoldate_fase_2}","protocoldate", 3);
            $tagPhase->addMergeTag("{user:emailricezione_fase_2}","Email", 3);
            $tagPhase->addMergeTag("{user:telefonocomunicazioni_fase_2}","Numero di telefono per comunicazioni", 3);

            // New Tags - 2
            $tagPhase->addMergeTag("{user:comuneNascita}","Luogo di Nascita", 2);
            $tagPhase->addMergeTag("{user:targaAutovettura}","Targa autovettura", 2);
            $tagPhase->addMergeTag("{user:nomeIntestatario}","Nome Proprietario", 2);
            $tagPhase->addMergeTag("{user:cognomeIntestarario}","Cognome Proprietario", 2);
            $tagPhase->addMergeTag("{user:codFiscaleIntestarario}","Codice Fiscale Proprietario", 2);
            $tagPhase->addMergeTag("{user:indirizzoResidenzaIntestatario}","Indirizzo residenza proprietario", 2);
            $tagPhase->addMergeTag("{user:numeroCivicoResidenzaIntestatario}","Civico", 2);
            //ad_alimentazione_1628659442578
            //classe_ambientale_1628660498956
            //intestazione_veicolo_1628660691070
            // comune_di_residenza_intestatario_1628661849279
            //grado_di_parentela_tra_il_richiedente_il_contributo_e_il_proprietario_del_veicolo_da_rottamare_1628662013117
            //ad_alimentazione_1628662163882
            //datanascita_1630042831086
            //ad_alimentazione_1630047993724
            Ninja_Forms()->merge_tags['nf_custom_merge_tag_form']
                = new NF_Custom_MergeTags_Form();
        }

        /**
         * Optional. If your extension creates a new field interaction or display template...
         */
        public function register_fields($actions)
        {
            $actions['custom']
                = new NF_Custom_Fields_CustomExample(); // includes/Fields/CustomExample.php

            return $actions;
        }

        /**
         * Optional. If your extension processes or alters form submission data on a per form basis...
         */
        public function register_actions($actions)
        {
            $actions['nf_custom_insert_pdf_user_file']
                = new NF_Custom_Actions_InsertPDFUserFile(); // includes/Actions/CustomExample.php

            return $actions;
        }

        /**
         * Optional. If your extension collects a payment (ie Strip, PayPal, etc)...
         */
        public function register_payment_gateways($payment_gateways)
        {
            $payment_gateways['custom']
                = new NF_Custom_PaymentGateways_CustomExample(); // includes/PaymentGateways/CustomExample.php

            return $payment_gateways;
        }

        /*
         * Optional methods for convenience.
         */

        public function autoloader($class_name)
        {
            if (class_exists($class_name)) {
                return;
            }
			
            if (false === strpos($class_name, self::PREFIX)) {
                return;
            }
			
            $class_name = str_replace(self::PREFIX, '', $class_name);
            $classes_dir = realpath(plugin_dir_path(__FILE__))
                . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;

            $class_file = str_replace('_', DIRECTORY_SEPARATOR, $class_name)
                . '.php';
			
            if (file_exists($classes_dir . $class_file)) {
                require_once $classes_dir . $class_file;
            }
        }

        /**
         * Template
         *
         * @param string $file_name
         * @param array  $data
         */
        public static function template($file_name = '', array $data = array())
        {
            if (!$file_name) {
                return;
            }

            extract($data);

            include self::$dir . 'includes/Templates/' . $file_name;
        }

        /**
         * Config
         *
         * @param $file_name
         *
         * @return mixed
         */
        public static function config($file_name)
        {
            return include self::$dir . 'includes/Config/' . $file_name
                . '.php';
        }

        /*
         * Required methods for all extension.
         */

        public function setup_license()
        {
            if (!class_exists('NF_Extension_Updater')) {
                return;
            }

            new NF_Extension_Updater(self::NAME, self::VERSION, self::AUTHOR,
                __FILE__, self::SLUG);
        }
    }

    /**
     * The main function responsible for returning The Highlander Plugin
     * Instance to functions everywhere.
     *
     * Use this function like you would a global variable, except without needing
     * to declare the global.
     *
     * @return {class} Highlander Instance
     * @since  3.0
     */
    function NF_Custom()
    {
        return NF_Custom::instance();
    }

    NF_Custom();
}	
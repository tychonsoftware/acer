<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_Display_SubmissionModifyLink
{
    public function __construct()
    {
        add_filter( 'manage_posts_extra_tablenav', array( $this, 'manage_posts_extra_tablenav' ) );
        add_filter( 'redirect_post_location', array( $this, 'redirect_post_location' ) );
    }

    public function manage_posts_extra_tablenav($which)
    {
        if ('bottom' !== $which) {
            return;
        }

        global $pagenow;

        if ( $pagenow != 'edit.php' || !isset($_GET['post_type']) || 'nf_sub' !== $_GET['post_type'])
            return;

        echo <<<HTML
<script type="text/javascript">
jQuery(function ($) {
    $('.post-type-nf_sub .wp-list-table tr.type-nf_sub .row-actions .edit a').each(function (i, atag) {
        var url = new URL($(atag).attr('href'));
        url.searchParams.append('nf_sub_filter_link', window.location.href)
        $(atag).attr('href', url.toString());
    });
});
</script>
HTML;

    }

    public function redirect_post_location($url)
    {
        if (!isset($_POST['post_type']) || 'nf_sub' !== $_POST['post_type']) {
            return $url;
        }
        $wp_http_referer = $_POST['_wp_http_referer'] ?? '';
        if ($wp_http_referer) {
            return $wp_http_referer;
        }
        return $url;
    }
}
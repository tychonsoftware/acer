<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_Display_SubmissionDownloadColumn
{
    private $downloadFields = [];

    public function __construct()
    {
        add_filter( 'manage_nf_sub_posts_columns', array( $this, 'add_column' ), 11 );
        add_action( 'manage_posts_custom_column', array( $this, 'custom_columns' ), 10, 2 );
    }

    public function add_column($columns)
    {
        if( ! isset( $_GET[ 'form_id' ] ) ) return $columns;
        if( !$columns ) return $columns;

        static $cols, $processed;

        if( isset($processed) ) return $cols;

        $cols = $columns;

        $form_id = absint( $_GET[ 'form_id' ] );

        $form_fields = Ninja_Forms()->form( $form_id )->get_fields();

        /** @var NF_Database_Models_Field $field */
        foreach( $form_fields as $field ) {
            if ($field->get_setting('type') === 'file_upload') {
                $cols['nf_custom_allegati'] = 'Allegati';
                $this->downloadFields[] = $field->get_id();
            }
        }

        $cols[ 'state' ] = __( 'Stato', 'ninja-forms' );

       // $cols[ 'note' ] = __( 'Note', 'ninja-forms' );

        $processed = true;
        return $cols;
    }

    public function custom_columns( $column, $sub_id )
    {
        if( 'nf_sub' != get_post_type() ) {
            return;
        }

        if ($column === 'state') {
            echo get_post_meta($sub_id, 'state', true);
        }

        // if ($column === 'note') {
        //     echo get_post_meta($sub_id, 'note', true);
        // }

        // if ($column === 'documento_finale') {
        //     echo get_post_meta($sub_id, 'documento_finale', true);
        // }

        if ($column === 'nf_custom_allegati') {
            $sub = Ninja_Forms()->form()->get_sub( $sub_id );

            foreach ($this->downloadFields as $fieldId) {
                $value = $sub->get_field_value( $fieldId );
                // ninja_forms_export_subs_to_pdf
                $args = array('ninja_forms_all_attachments_subs_to_zip' => 21, 'sub_id' => $sub_id );
                $download_link = wp_nonce_url( add_query_arg( $args, admin_url() ), 'nf_download_attach' );
                if ($value) {
                    echo <<<HTML
<a href="$download_link">Scarica</a>
HTML;
                    break;
                }
            }
        }
    }
}
<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_WPAjax_Rinuncia
{
    /**
     * @var int
     */
    private $subID;
    /**
     * @var NF_Custom_Shortcode_MySubmission_MySubmissionItem
     */
    private $tableRow;

    public function __construct(int $subID)
    {
        $this->subID = $subID;
        if ( ! defined( 'NF_CUSTOM_RINUNCIA_API' ) ) {
            throw new Exception('Missing Config for NF_CUSTOM_RINUNCIA_API');
        }
        global $current_user;
        $post = WP_Post::get_instance($this->subID);

        if (!$current_user || intval($post->post_author) !== $current_user->ID) {
            throw new Exception('wrong user');
        }

        $sub = Ninja_Forms()->form()->get_sub($post->ID);
        $formId = $sub->get_form_id();
        $this->tableRow = new NF_Custom_Shortcode_MySubmission_MySubmissionItem($post, $formId);
    }

    public function process()
    {
		$result = $this->makeRequest();
        if ('ok' === $result->status) {
            update_post_meta($this->subID, 'state', 'Rinuncia');
            echo 1;
        } else {
            echo 0;
        }
        wp_die();
    }

    

    private function makeRequest()
    {
	
        //Encode the array into JSON.
        $data = json_encode([
            'request_id' => $this->getRequestId(),
            'state' => 'Rinuncia',
            'note' => '',
        ]);
		error_log("Corpo Rinuncia: ".$data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_URL, NF_CUSTOM_RINUNCIA_API);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		error_log("Richiesta Rinuncia: ".print_r($curl,true));
        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {die("Connection Failure");}
        curl_close($curl);
        // $result = curl_exec($curl);
        // if (!$result) {die("Connection Failure");}
        // curl_close($curl);
       $result = '{"status":"ok"}';
        error_log("Risposta Rinuncia: ".$result);
        return json_decode($result);
    }

    private function getRequestId()
    {
        $protocoldate = date("Y", strtotime($this->tableRow->getDataProtocollo()));
        $protocolnumber = $this->tableRow->getNumeroProtocollo();

        return $protocoldate . $protocolnumber;
    }
}

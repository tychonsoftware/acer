<?php

class NF_Custom_Admin_Download_AllSubAttachmentsZip
{
    public function __construct()
    {
        $this->init();
    }

    private function init()
    {
        if( isset( $_REQUEST['ninja_forms_all_attachments_subs_to_zip'] ) AND $_REQUEST['ninja_forms_all_attachments_subs_to_zip'] === '21' ) {
            $this->download();
        }
    }

    private function download()
    {
        // make sure we have the right data
        if( !isset( $_REQUEST['sub_id'] ) || $_REQUEST['sub_id'] == '' ) {
            return $this->redirectBack();
        }

        // verify nonce
        if (!isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'nf_download_attach')) {
            return $this->redirectBack();
        }

        wp_safe_redirect($this->createZipFile());
        return $this->createZipFile();
    }

    private function redirectBack($message = '')
    {
        wp_safe_redirect(wp_get_referer());
        exit();
    }

    /**
     * @return string
     */
    private function createZipFile(): string
    {
        $sub_id = (int)$_REQUEST['sub_id'];

        $sub = Ninja_Forms()->form()->get_sub($sub_id);
        $form = Ninja_Forms()->form($sub->get_form_id());
        $form_fields = $form->get_fields();

        $nfFileUpload = NF_File_Uploads();
        $zipFiles = [];
        $processedurls= array();
        /** @var NF_Database_Models_Field $field */
        foreach ($form_fields as $field) {
            if ($field->get_setting('type') === NF_FU_File_Uploads::TYPE) {
                $files = $sub->get_field_value($field->get_id());
                foreach ($files as $fileId => $file) {
                    $nfFile = $nfFileUpload->model->get($fileId);
                    $nfFile->dataObject = unserialize($nfFile->data);
                    $zipFiles[$nfFile->id] = $nfFile->dataObject['file_path'];
                    array_push($processedurls, $nfFile->dataObject['file_path']);
                }
            }
        }
      
        global $wpdb;
        $getFields
            = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}nf3_fields WHERE parent_id =" . $sub->get_form_id());
        foreach ($getFields as $field) {
            if (!$field  || $field->type !== 'file_upload'){
            continue;
            }
            $links = $sub->get_field_value($field->id);
            if (empty($links)) {
                continue;
            }
            
            $uploadDir = wp_upload_dir();
            $uploadDirBase = $uploadDir['basedir'];
            $uploadDirUrl = $uploadDir['baseurl'];

            if(array_key_exists('file_url', $links)){
                if( in_array( $links['file_url'] ,$processedurls ) ){
                }else {
                    $zipFiles[$links['upload_id']] = str_replace($uploadDirUrl, $uploadDirBase, $links['file_url']);
                }
            }
        }
        $fileName = 'all_attachments_' . $sub_id . '_' . md5(implode(',',
                array_keys($zipFiles)));
        $zip = new ZipArchive;
        $fullPathFile = implode(DIRECTORY_SEPARATOR, [
            $uploadDirBase,
            'ninja-forms',
            $fileName . '.zip'
        ]);
        if (!file_exists($fullPathFile)) {
            $zip->open($fullPathFile, ZipArchive::CREATE);
            foreach ($zipFiles as $file) {
                $zip->addFile($file, basename($file));
            }

            $zip->close();
        }
        return implode('/', [
            $uploadDirUrl,
            'ninja-forms',
            $fileName . '.zip'
        ]);
    }

}
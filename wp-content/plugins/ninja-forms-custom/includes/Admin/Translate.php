<?php

class NF_Custom_Admin_Translate
{
    public function __construct()
    {
        $this->init();
    }

    private function init()
    {
        add_action('admin_menu', [$this, 'actionAdminMenu11'], 11);
        add_filter( 'post_row_actions', array( $this, 'post_row_actions' ), 11, 2 );
    }

    public function actionAdminMenu11()
    {
        $this->addTranslateExcelsExportMenu();
    }

    private function addTranslateExcelsExportMenu()
    {
        if( !function_exists('Ninja_Forms') || !isset(Ninja_Forms()->menus[ 'excel-export' ]) ) {
            return;
        }

        Ninja_Forms()->menus[ 'excel-export' ]->page_title = __('Excel Export', 'ninja-forms-spreadsheet');
    }

    public function post_row_actions( $actions, $sub )
    {
        if ( 'nf_sub' == get_post_type() ){
            $export_url = add_query_arg( array( 'action' => 'export', 'post[]' => $sub->ID ) );
            $actions[ 'export' ] = sprintf( '<a href="%s">%s</a>', $export_url, 'Esporta CSV' );
        }

        return $actions;
    }
}
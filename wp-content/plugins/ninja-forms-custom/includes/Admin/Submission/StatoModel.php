<?php

class NF_Custom_Admin_Submission_StatoModel
{
    /**
     * @var NF_Custom_Admin_Submission_StatoModel
     */
    private static $instance;

    /**
     * @var NF_Custom_Admin_Submission_StatoItem[]
     */
    private $items;

    /**
     * @return NF_Custom_Admin_Submission_StatoModel
     */
    public static function getInstance()
    {
        if ( !isset( self::$instance ) ) {
            self::$instance = new self();
           // self::$instance->checkAndCreateTable();
        }
        return self::$instance;
    }

    private function checkAndCreateTable()
    {
        global $wpdb;

        $table_name = $this->getTableName();

        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) {
            return;
        }

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT,
            valore mediumint(9) UNSIGNED,
            descrizione varchar(255) DEFAULT '' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        $wpdb->query($sql);

        // Init data
        $data = [
            [1, 1, 'Inviata'],
            [2, 2, 'Ammessa alla fase 2'],
            [3, 3, 'Ammessa alla fase 3'],
        ];

        foreach ($data as $item) {
            $wpdb->insert(
                $table_name,
                array(
                    'id' => $item[0],
                    'valore' => $item[1],
                    'descrizione' => $item[2],
                )
            );
        }

        $args = array(
            'post_type' => 'nf_sub',
            'posts_per_page' => -1,
        );

        $subs = get_posts( $args );

        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if (!$state) continue;
            $stateId = $this->getIdByDes($state);
            if ($stateId > 0) {
                add_post_meta($sub->ID, 'state_id', $stateId, true);
            }
        }
    }

    private function getTableName()
    {
        global $wpdb;
        return $wpdb->prefix . "nf_stato";
    }

    /**
     * @return NF_Custom_Admin_Submission_StatoItem[]
     */
    public function getListItems()
    {
        if (isset($this->items)) {
            return $this->items;
        }
        $this->items = [];
        global $wpdb;
        $table = $this->getTableName();
        $rows = $wpdb->get_results( "SELECT * FROM $table");

        foreach ( $rows as $row ) {
            $this->items[$row->id] = new NF_Custom_Admin_Submission_StatoItem(
                $row->id,
                $row->valore,
                $row->descrizione
            );
        }
        return $this->items;
    }

    public function getIdByDes(string $descrizione)
    {
        foreach ($this->getListItems() as $item) {
            if ($item->getDescrizione() === $descrizione) {
                return $item->getId();
            }
        }
        return -1;
    }

    public function getDesById($id)
    {
        $items = $this->getListItems();
        return $items[$id] ?? '';
    }

}
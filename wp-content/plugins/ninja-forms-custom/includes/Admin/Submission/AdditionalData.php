<?php

class NF_Custom_Admin_Submission_AdditionalData
{
    public function __construct()
    {
        $this->init();
    }

    private function init()
    {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 10, 1 );
        // add_action( 'add_meta_boxes', array( $this, 'remove_meta_boxes' ), 20, 1 );
        // Save our metabox values
        add_action( 'save_post', array( $this, 'save_nf_sub' ), 5, 2 );
    }

    /**
     * Meta Boxes
     */
    public function add_meta_boxes( $post_type )
    {
        add_meta_box(
            'nf_sub_fields_extra',
            __( 'Lavorazione Richiesta', 'ninja-forms' ),
            array( $this, 'fields_meta_box' ),
            'nf_sub',
            'normal',
            'low'
        );
    }

    /**
     * Remove Meta Boxes
     */
    // public function remove_meta_boxes()
    // {
    //     if ($this->isSubmissionReaderOnly()) {
            // remove_meta_box( 'nf_sub_fields', 'nf_sub', 'normal' );
            // remove_meta_box( 'nf_sub_info', 'nf_sub', 'side' );
        // }
    // }

    /**
     * Fields Meta Box
     *
     * @param $post
     */
    public function fields_meta_box( $post )
    {
        $canEdit = $this->canEditPost($post);
        include dirname(__FILE__) . '/additional_data_template.phtml';
    }



    public function save_nf_sub( $nf_sub_id, $nf_sub )
    {
        global $pagenow;

        // if ( ! isset ( $_POST['nf_edit_sub'] ) || $_POST['nf_edit_sub'] != 1 )
        //     return $nf_sub_id;

        // verify if this is an auto save routine.
        // If it is our form has not been submitted, so we dont want to do anything
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $nf_sub_id;

        if ( $pagenow != 'post.php' )
            return $nf_sub_id;

        if ( $nf_sub->post_type != 'nf_sub' )
            return $nf_sub_id;

        if ($this->isSubmissionReaderOnly()) {
            unset($_POST['nf_edit_sub']);
            unset($_POST['fields']);
        }

        /* Get the post type object. */
        $post_type = get_post_type_object( $nf_sub->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $nf_sub_id ) )
            return $nf_sub_id;

        $statoId = $_POST['extra_fields']['stato'] ?? false;
        if ($statoId > 0) {
            $state = NF_Custom_Admin_Submission_StatoModel::getInstance()->getDesById($statoId);
            if ($state) {
                update_post_meta($nf_sub_id, 'state', $state->getDescrizione());
            }
            update_post_meta($nf_sub_id, 'state_id', $statoId);
        }

        $subnote = $_POST['extra_fields']['subnote'];
        if(empty( get_post_meta($nf_sub_id, 'note', true))){
            add_post_meta($nf_sub_id, 'note', $subnote, true);
        }else {
            update_post_meta($nf_sub_id, 'note', $subnote);
        }
        $upload_overrides = [
            'test_form' => false,
        ];
        $files = $_FILES['documentfinale'];
        
        if(!empty($files)){
            if ($files['name']) {
                $file = array(
                    'name'     => $files['name'],
                    'type'     => $files['type'],
                    'tmp_name' => $files['tmp_name'],
                    'error'    => $files['error'],
                    'size'     => $files['size']
                );

                function nf_submission_upload_dir($dir){
                    if(!defined('NF_SUBMISSION_ATTACHMENTS_DIR')){
                        define('NF_SUBMISSION_ATTACHMENTS_DIR', 'nf_submission_attachments');
                    }
                    $uploadDir = $dir['basedir'] . DIRECTORY_SEPARATOR . NF_SUBMISSION_ATTACHMENTS_DIR;
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir);
                    }
                    return array(
                            'path'   => $uploadDir,
                            'url'    => $dir['baseurl'] . '/' . NF_SUBMISSION_ATTACHMENTS_DIR,
                            'subdir' => '/' . NF_SUBMISSION_ATTACHMENTS_DIR,
                        ) + $dir;
                }
                
                add_filter('upload_dir', 'nf_submission_upload_dir');
                $moveFile = wp_handle_upload($file, $upload_overrides);
                $file_path = str_replace( '\\', '/', $moveFile['file'] );
                $file_path = preg_replace( '|(?<=.)/+|', '/', $file_path );
                if ( ':' === substr( $file_path, 1, 1 ) ) {
                    $file_path = ucfirst( $file_path );
                }
                remove_filter('upload_dir', 'nf_submission_upload_dir');
                if(metadata_exists('post',$nf_sub_id,'nf_documento_finale')){
                    update_post_meta($nf_sub_id, 'nf_documento_finale', $file_path);
                }else {
                    add_post_meta($nf_sub_id, 'nf_documento_finale', $file_path, true);
                }
            }
        }
    }

    public function isSubmissionReaderOnly() {
        if ( current_user_can( 'manage_options' ) ) {
            return false;
        }
        if ( current_user_can( 'submission_reader' ) ) {
            return true;
        }
        return false;
    }

    /**
     * @param $post WP_Post
     *
     * @return false
     */
    private function canEditPost($post)
    {
        if ($post->post_author && get_current_user_id() !== $post->post_author) {
            return current_user_can( 'nf_sub_edit_others_subs' );
        }
        return false;
    }

}
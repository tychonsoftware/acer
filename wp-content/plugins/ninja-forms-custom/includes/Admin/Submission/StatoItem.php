<?php

class NF_Custom_Admin_Submission_StatoItem
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $valore;
    /**
     * @var string
     */
    private $descrizione;

    public function __construct($id, $valore, $descrizione)
    {
        $this->id = $id;
        $this->valore = $valore;
        $this->descrizione = $descrizione;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->id;
    }

    /**
     * @return int
     */
    public function getValore(): int
    {
        return $this->valore;
    }

    /**
     * @return string
     */
    public function getDescrizione(): string
    {
        return $this->descrizione;
    }


}
<?php

class NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniSection
{
    public function __construct()
    {
        $this->init();
    }

    private function init()
    {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 10, 1 );
        add_action( 'admin_post_nf_sub_email_post', array($this,'nf_sub_email_post'));
    }

    /**
     * Meta Boxes
     */
    public function add_meta_boxes( $post_type )
    {
        add_meta_box(
            'nf_sub_comunicazioni',
            __( 'Comunicazioni', 'ninja-forms' ),
            array( $this, 'comunicazioni_meta_box' ),
            'nf_sub',
            'normal',
            'low'
        );
    }

    public function comunicazioni_meta_box()
    {
        $this->checkAndCreateTable();
        wp_enqueue_script( 'jquery-ui-dialog' );
        wp_enqueue_style( 'wp-jquery-ui-dialog' );

        add_action( 'in_admin_footer', array( $this, 'in_admin_footer' ), 10, 1 );

        include dirname(__FILE__) . '/comunicazioni_meta_box.phtml';
    }

    public function in_admin_footer()
    {
        include dirname(__FILE__) . '/comunicazioni_meta_box_popup.phtml';
    }

    public function nf_sub_email_post()
    {
        NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniPost::getInstance()->processPostRequest();
    }

    private function checkAndCreateTable()
    {
        NF_Custom_Admin_Submission_Comunicazioni_PostMail::checkAndCreateTable();
    }
}
<?php

class NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniPost
{
    /**
     * @var NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniPost
     */
    private static $instance;

    /**
     * @return NF_Custom_Admin_Submission_Comunicazioni_ComunicazioniPost
     */
    public static function getInstance()
    {
        if ( !isset( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function processPostRequest()
    {
        $this->validatePostEmailRequest();
        $postMail = NF_Custom_Admin_Submission_Comunicazioni_PostMail::createFromPostData();
        $postMail->sendEmail();
        wp_redirect(wp_get_referer());
        exit;
    }



    private function validatePostEmailRequest(): void
    {
        $nonce = sanitize_text_field($_POST['security']);
        /* Get the post type object. */
        $post_type = get_post_type_object('nf_sub');

        if (!wp_verify_nonce($nonce, 'nf_sub_email_post')
            || !current_user_can($post_type->cap->edit_post)
        ) {
            header('Location:' . $_SERVER["HTTP_REFERER"]
                . '?error=unauthenticated');
            exit();
        }
    }
}
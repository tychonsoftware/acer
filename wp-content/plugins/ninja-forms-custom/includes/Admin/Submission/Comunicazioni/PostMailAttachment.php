<?php

class NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment
{
        const TABLE = 'nf_posts_mails_attachments';

    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $nf_posts_mails_id;
    /**
     * @var string
     */
    public $nome_file;
    /**
     * @var string
     */
    public $path_file;

    /**
     * @return NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment[]
     */
    public static function createFromPostRequest($nf_posts_mails_id)
    {
        $items = [];
        $upload_overrides = [
            'test_form' => false,
        ];
        $files = $_FILES['allegati'];
        function nf_upload_dir($dir)
        {
            if(!defined('NF_POSTS_MAILS_ATTACHMENTS_DIR')){
                define('NF_POSTS_MAILS_ATTACHMENTS_DIR', 'nf_posts_mails_attachments');
            }
            $uploadDir = $dir['basedir'] . DIRECTORY_SEPARATOR . NF_POSTS_MAILS_ATTACHMENTS_DIR;
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir);
            }
            return array(
                    'path'   => $uploadDir,
                    'url'    => $dir['baseurl'] . '/' . NF_POSTS_MAILS_ATTACHMENTS_DIR,
                    'subdir' => '/' . NF_POSTS_MAILS_ATTACHMENTS_DIR,
                ) + $dir;
        }

        foreach ($files['name'] as $key => $value) {
            $instance = new self();
            $items[] = $instance;
            $instance->nf_posts_mails_id = $nf_posts_mails_id;
            if ($files['name'][$key]) {
                $file = array(
                    'name'     => $files['name'][$key],
                    'type'     => $files['type'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'error'    => $files['error'][$key],
                    'size'     => $files['size'][$key]
                );
                add_filter('upload_dir', 'nf_upload_dir');
                $moveFile = wp_handle_upload($file, $upload_overrides);
                remove_filter('upload_dir', 'nf_upload_dir');
                $instance->nome_file = $file['name'];
                $instance->path_file = $moveFile['file'];
                $instance->save();
            }
        }
        return $items;
    }

    private function initFromRow($row)
    {
        $this->id = $row->id;
        $this->nf_posts_mails_id = $row->nf_posts_mails_id;
        $this->nome_file = $row->nome_file;
        $this->path_file = $row->path_file;
    }

    public static function getListFromPostMailId($postMailId)
    {
        global $wpdb;
        $table = $wpdb->prefix . self::TABLE;
        $query = "SELECT * FROM $table WHERE nf_posts_mails_id = $postMailId";
        $get_results = $wpdb->get_results($query);
        if (!$get_results) return [];
        $items = [];
        foreach ($get_results as $row) {
            $instance = new self;
            $instance->initFromRow($row);
            $items[] = $instance;
        }
        return $items;
    }

    private function save()
    {
        if (isset($this->id) && $this->id > 0) {
            return $this->update();
        }
        return $this->create();
    }

    private function update()
    {
        throw new Exception('Not implement');
    }

    private function create()
    {
        global $wpdb;
        $table = $wpdb->prefix . self::TABLE;
        $data = [
            'nf_posts_mails_id' => $this->nf_posts_mails_id,
            'nome_file'         => $this->nome_file,
            'path_file'         => $this->path_file,
        ];
        $wpdb->insert($table, $data);
        $this->id = $wpdb->insert_id;
        return $this;
    }

    public function getDownloadUrl()
    {
        if(!defined('NF_POSTS_MAILS_ATTACHMENTS_DIR')){
            define('NF_POSTS_MAILS_ATTACHMENTS_DIR', 'nf_posts_mails_attachments');
        }
        $file = explode('wp-content/uploads',$this->path_file)[1];
        $uploads = wp_get_upload_dir();
        return $uploads['baseurl'] . $file;

    }
}
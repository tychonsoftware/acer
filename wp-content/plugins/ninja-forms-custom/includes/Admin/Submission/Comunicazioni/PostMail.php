<?php

class NF_Custom_Admin_Submission_Comunicazioni_PostMail
{
    const TABLE = 'nf_posts_mails';
    /**
     * @var int
     */
    public $id;
        /**
     * @var int
     */
    public $post_id;
        /**
     * @var string
     */
    public $mittente_mail;
        /**
     * @var string
     */
    public $oggetto_mail;
        /**
     * @var string
     */
    public $cc_mail;
        /**
     * @var string
     */
    public $destinatari_mail;
        /**
     * @var string
     */
    public $corpo_mail;
    /**
     * @var string
     */
    public $utente;
    /**
     * @var DateTime
     */
    public $data_invio;
    /**
     * @var NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment[]
     */
    public $attachments;

    public function save()
    {
        if (isset($this->id) && $this->id > 0) {
            return $this->update();
        }
        return $this->create();
    }

    public static function getListFromRows(?array $get_results)
    {
        if (!$get_results) return [];
        $items = [];
        foreach ($get_results as $row) {
            $instance = new self;
            $instance->initFromRow($row);
            $items[] = $instance;
        }
        return $items;
    }

    private function initFromRow($row)
    {
        $this->id = $row->id;
        $this->post_id = $row->post_id;
        $this->mittente_mail = $row->mittente_mail;
        $this->oggetto_mail = $row->oggetto_mail;
        $this->cc_mail = $row->cc_mail;
        $this->destinatari_mail = $row->destinatari_mail;
        $this->corpo_mail = $row->corpo_mail;
        $this->utente = $row->utente;
        $this->data_invio = $row->data_invio;
        $this->attachments = NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment::getListFromPostMailId($this->id);
    }

    public function sendEmail()
    {
        $headers = array( 'Content-Type: text/html; charset=UTF-8' );
        if (isset($this->cc_mail) && count(explode(';', $this->cc_mail))) {
            $ccMail = array_map(function ($mail) {
                return "Cc: $mail";
            }, explode(';', $this->cc_mail));
            $headers = array_merge($headers, $ccMail);
        }
        $sent = wp_mail(
            explode(';', $this->destinatari_mail),
            $this->oggetto_mail,
            $this->corpo_mail,
            $headers,
            $this->getAttachmentsForEmail()
        );
        return $sent;
    }

    public function nf_sub_post_mail_wp_mail_from_name($from)
    {
        $this->mittente_mail = $from;
    }

    /**
     * @return NF_Custom_Admin_Submission_Comunicazioni_PostMail
     */
    public static function createFromPostData()
    {
        $instance = new self();
        $instance->post_id = $_POST['post_id'];
        $instance->mittente_mail = apply_filters( 'wp_mail_from', '' );
        $instance->oggetto_mail = $_POST['oggetto'];
        $instance->cc_mail = $_POST['cc'];
        $instance->destinatari_mail = $_POST['destinatari'];
        $instance->corpo_mail = $_POST['corpo'];
        $instance->utente = get_current_user_id();

        $instance->save();
        $items = NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment::createFromPostRequest($instance->id);
        $instance->attachments = $items;
        return $instance;
    }

    private function update()
    {
        throw new Exception('Not implement');
    }

    private function create()
    {
        global $wpdb;
        $table = $wpdb->prefix . self::TABLE;
        $data = [
            'post_id' => $this->post_id,
            'mittente_mail' => $this->mittente_mail,
            'oggetto_mail' => $this->oggetto_mail,
            'cc_mail' => $this->cc_mail,
            'destinatari_mail' => $this->destinatari_mail,
            'corpo_mail' => $this->corpo_mail,
            'utente' => $this->utente,
        ];
        $wpdb->insert($table, $data);
        $this->id = $wpdb->insert_id;
        return $wpdb->insert_id;
    }

    private function getAttachmentsForEmail()
    {
        if (!isset($this->attachments)) {
            return [];
        }
        $items = [];
        foreach ($this->attachments as $attachment) {
            $items[] = $attachment->path_file;
        }
        return $items;
    }

    public static function checkAndCreateTable()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . self::TABLE;

        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) {
            return;
        }

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            post_id bigint(20) unsigned NOT NULL,
            mittente_mail varchar(512) DEFAULT '' NOT NULL,
            oggetto_mail varchar(512) DEFAULT '' NOT NULL,
            cc_mail varchar(512) DEFAULT '' NOT NULL,
            destinatari_mail varchar(512) DEFAULT '' NOT NULL,
            corpo_mail text,
            utente bigint(20) unsigned,
            data_invio datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        $wpdb->query($sql);

        $table_name = $wpdb->prefix . NF_Custom_Admin_Submission_Comunicazioni_PostMailAttachment::TABLE;

        $sql = "CREATE TABLE $table_name (
            id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            nf_posts_mails_id  bigint(20) unsigned NOT NULL,
            nome_file varchar(512) DEFAULT '' NOT NULL,
            path_file varchar(512) DEFAULT '' NOT NULL,
            KEY nf_posts_mails_attachments_nf_posts_mails_id (nf_posts_mails_id),
            PRIMARY KEY  (id)
        ) $charset_collate;";

        $wpdb->query($sql);
    }
}
<?php

class NF_Custom_Admin_Submission_Comunicazioni_PostMailListTable extends
    WP_List_Table
{
    /**
     * @var mixed
     */
    private $getUserDisplayNameCache = [];

    function __construct()
    {
        parent::__construct(array(
            'singular' => 'wp_list_text_link',
            'plural'   => 'wp_list_test_links',
            'ajax'     => false
        ));
    }

    function get_columns() {
        return array(
            'data_invio'=>__('Data Invio'),
            'utente'=>__('Operatore'),
            'mittente_mail'=>__('Mittente'),
            'destinatari_mail'=>__('Destinatario'),
            'oggetto_mail'=>__('Oggetto'),
            'cc_mail'=>__('CC'),
            'corpo_mail'=>__('Messaggio'),
            'allegati'=>__('Allegati'),
        );
    }

    function prepare_items() {
        global $wpdb;

        $columns  = $this->get_columns();
        $hidden   = [];
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array( $columns, $hidden, $sortable );

        $perPage = 5;
        $currentPage = $this->get_pagenum();
        $table = $wpdb->prefix . NF_Custom_Admin_Submission_Comunicazioni_PostMail::TABLE;
        $postId = intval($_GET['post']);

        $query = "SELECT * FROM $table WHERE post_id = $postId ORDER BY data_invio DESC";
        $items = NF_Custom_Admin_Submission_Comunicazioni_PostMail::getListFromRows($wpdb->get_results($query));
        $totalItems = count($items);
        $items = array_slice( $items, ( ( $currentPage - 1 ) * $perPage ), $perPage );
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $this->items = $items;
    }

    protected function column_default($item, $column_name)
    {
        return $item->$column_name;
    }

    /**
     * @param $item NF_Custom_Admin_Submission_Comunicazioni_PostMail
     *
     * @return mixed|string
     */
    public function column_utente($item)
    {
        return $this->getUserDisplayName($item->utente);
    }

    public function column_corpo_mail($item)
    {
        if (!trim($item->corpo_mail)) {
            return '';
        }
        return <<<HTML
<div class="corpo-mail">
    <a href="#" class="view">Visualizza</a>
    <div class="content">
        {$item->corpo_mail}
    </div>
</div>
HTML;

    }

    /**
     * @param $item NF_Custom_Admin_Submission_Comunicazioni_PostMail
     */
    public function column_allegati($item)
    {
        $links = [];
        foreach ($item->attachments as $attachment) {
            $links[] = <<<HTML
<a href="{$attachment->getDownloadUrl()}" download="">{$attachment->nome_file}</a>
HTML;
        }
        return implode('<br>', $links);
    }

    private function getUserDisplayName($userId)
    {

        if (!isset($this->getUserDisplayNameCache[$userId])) {
            $user = get_userdata( $userId );

            $this->getUserDisplayNameCache[$userId] = $user->display_name;
        }
        return $this->getUserDisplayNameCache[$userId];
    }

    protected function display_tablenav( $which ) {
        // if ( 'top' === $which ) {
        //     wp_nonce_field( 'bulk-' . $this->_args['plural'] );
        // }
        ?>
        <div class="tablenav <?php echo esc_attr( $which ); ?>">

            <?php if ( $this->has_items() ) : ?>
                <div class="alignleft actions bulkactions">
                    <?php $this->bulk_actions( $which ); ?>
                </div>
            <?php
            endif;
            $this->extra_tablenav( $which );
            $this->pagination( $which );
            ?>

            <br class="clear" />
        </div>
        <?php
    }
}
<?php

date_default_timezone_set('Europe/Rome');
class NF_Custom_Webservices_ComuneTerni
{
    private $form_data;
    private $action_settings;
    private $pdf_template_dat;
	private $attachments;

    /**
     * @var SoapClient
     */
    private $client;
    private $wsdl;
    private $soapUsername;
    private $soapPassword;

    public function __construct($form_data, $action_settings, $pdf_template_dat) {
        $this->initClient();
        $this->form_data = $form_data;
        $this->action_settings = $action_settings;
        $this->pdf_template_dat = $pdf_template_dat;

		$this->attachments = $action_settings["ATTACHMENTS"];

		error_log("ACTION SETTINGS: ".print_r($this->action_settings,true));

		error_log("ATTACHMENTS: ".print_r($this->attachments,true));
    }

    private function initClient()
    {
        global $wpdb;

        $table_name = "servmask_prefix_portal";
        $query = "SELECT * FROM $table_name WHERE id = 2";
        $soap_logins = $wpdb->get_results($query);
        $soap_login = $soap_logins[0];
        $this->soapUsername = $soap_login->soap_username;
        $this->soapPassword = $soap_login->soap_password;
        $this->wsdl = $soap_login->soap_url;
    }

    public function makeRequest()
    {
        try {
            error_log("XML Prepared " . $this->xml());
            $result = $this->send_request($this->wsdl, $this->xml());
            if (isset($result['http_status']) && $result['http_status'] > 300) {
                return [
                    'error' => 'Errore durante la protocollazione della domanda. Si prega di riprovare'
                ];
            }

            $this->processXMLResponse($result['response_body']);
            $sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
            $attachments = array();
            $attachments = apply_filters( 'ninja_forms_action_email_attachments', $attachments, $this->form_data, $this->action_settings );
            if(!empty($attachments)){
                $pdf_file_path = $attachments[0];
                $sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                $pdf_file_name = $this->get_pdf_file_name( $this->form_data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';
                $new_pdf_file = dirname($pdf_file_path) .'/' .$pdf_file_name;
                rename($pdf_file_path,$new_pdf_file);
                if (class_exists('UserFascicle')) {
                    $active_plugins_basenames = get_option( 'active_plugins' );
                    foreach ( $active_plugins_basenames as $plugin_basename ) {
                        if ( false !== strpos( $plugin_basename, '/UserFascicle.php' ) ) {
                            $plugin_dir = WP_PLUGIN_DIR . '/' . $plugin_basename;
                            require_once $plugin_dir;
                            $uf_plugin  = new UserFascicle();
                            $uf_plugin->store_pdf_fascicle_folder($new_pdf_file ,$this->form_data['settings']['title'], $sub_id, $this->form_data[ 'form_id' ]);
                            break;
                        }
                    }
                }else {
                    error_log("User Fascicle not installed");
                }
            }
            return [];
        } catch (\Exception $e) {
            if ( WP_DEBUG === true ) {
                error_log('No PDF template associated with ');
            }
            return [
                'error' => 'Error in sending document to external service. No PDF template associated with'
            ];
        }
    }

	private function getFieldValueFromLabel($fieldName){
		$returnValue = "";
		$formFields = $this->form_data['fields'];

		foreach($formFields as $field){
			$label = strtoupper($field['label']);
			$fieldName = strtoupper ($fieldName);
                    
			if($label == $fieldName){
				$returnValue = $field['value'];
				break;
			}
		}
		return $returnValue;
	}
    
	private function getByKey($keyvalue){
		$returnValue = "";
		$formFields = $this->form_data['fields'];
		error_log("KEY VALUE: ".$keyvalue." FORM FIELDS: ".$formFields);
		foreach($formFields as $field){
			$key = strtoupper($field['settings']['key']);
			$keyvalue = strtoupper ($keyvalue);
			if($key == $keyvalue){
				$returnValue = $field['value'];
				error_log("TROVATO -> ".print_r($field,true));
				break;
			}
		}
		return $returnValue;
	}

    private function getTableDataByKey($keyvalue){
		$returnValue = "";
		$formFields = $this->form_data['fields'];
		foreach($formFields as $field){
			$key = strtoupper($field['settings']['key']);
			$keyvalue = strtoupper ($keyvalue);
			if($key == $keyvalue){
                if(isset($field['raw_data']))
				    $returnValue = $field['raw_data'];
				break;
			}
		}
		return $returnValue;
	}

    private function getAllFiles(){
        $data = Array();
//		$returnValue = "";
		$formFields = $this->form_data['fields'];
		foreach($formFields as $field){
			$type = $field['settings']['type'];
			if($type == 'file_upload' && isset($field['settings']['files'])){
                array_push($data,$field['settings']['files']);
			}
		}
		return $data;
	}
    protected function xml()
    {
        error_log("Preparing Comuni Terni xml");
        $nonce = sha1(mt_rand());
		$createDate = gmdate('Y-m-d\TH:i:s\Z');
        //$user = wp_get_current_user();
        //$codice_fiscale = get_user_meta($user->ID, 'codice_fiscale', true);

		$sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;

		$cognome = $this->getMapDataFromFormField('comune_terni_cognome', "Cognome");
		$nome = $this->getMapDataFromFormField('comune_terni_nome', "Nome");
		$email = $this->getMapDataFromFormField('comune_terni_email', "Email per ricezione della ricevuta");
		$codiceFiscale =  $this->getMapDataFromFormField('comune_terni_codice_fiscale', "Codice Fiscale");
		$searchValue = $this->getSearchValue();
        $topicPath = $this->getTopicPath();
        $soggetto =$this->getSoggettoValue();
		$fullname = $cognome." ".$nome;

		error_log("DATI: ".$cognome." ".$nome." ".$email." ".$codiceFiscale);

        $pdf_file_name = $this->get_pdf_file_name( $this->form_data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';

		$attachmentsArea = "";

		$attachmentsArea .= "<attachments>\n";
		$fileSet = "isharedocMailAttach";
		foreach ($this->attachments as $value){
			if (strpos($value, 'ninja-forms-submission-')===false){
				$pdfName = pathinfo($value,PATHINFO_BASENAME);
				$fileContent = file_get_contents($value);
				$attachmentsArea .="<attachment>\n";
				$attachmentsArea .="	<fileset>$fileSet</fileset>\n";
				$attachmentsArea .="	<filename>$pdfName</filename>\n";
				$attachmentsArea .="	<contentType>application/pdf</contentType>\n";
				$attachmentsArea .="	<data>".base64_encode($fileContent)."</data>\n";
				$attachmentsArea .="</attachment>\n";
			}
		}

        $form_id = $this->form_data['form_id'];

        if($form_id == 61){
            error_log("Form data 61" . print_r( $this->form_data['fields'],1));
            $cognomeRichiedente = $this->getByKey('cognome_richiedente_1591626411300');
            $nomeRichiedente = $this->getByKey('nome_richiedente_1591626417938');
            $luogoDiNascita = $this->getByKey('luogo_di_nascita_1594726021150');
            $dataDiNascita = $this->getByKey('data_di_nascita_1594726068041');
            $residenteNelComuneDi = $this->getByKey('residente_nel_comune_di_1608128287457');
            $allaViaPiazza = $this->getByKey('alla_via_piazza_1608128323231');
            $nCivico = $this->getByKey('n_civico_1595516820746');
            $zip = $this->getByKey('zip_1595599856330');
            $codiceiscale = $this->getByKey('codice_fiscale_1591616243364');
           
           
            $sezione1Data = $this->getByKey('sezione_1_-_lett_a_-_di_essere_alla_data_di_pubblicazione_del_bando_1633339434083');
            $sezione1Period = $this->getByKey('sezione_1_-_lett_b_-_periodo_residenza_1633339505388');
            $sezione2Reddito = $this->getByKey('sezione_2_-_lett_a_-_reddito_1633339646667');
            $sezione2Euro = $this->getByKey('sezione_2_-_lett_b_-_che_l_attestazione_isee_2021_e_di_eururo_1633339819734');
            $tipologia = $this->getByKey('tipologia_1633612616690');
            $sezione2dellaDomanda = $this->getByKey('sezione_2_-_lett_c_-_presentazione_della_domanda_1633340162116');
            $sezione3Data = $this->getTableDataByKey('sezione_3_-_stato_di_famiglia_anagrafico_1633430430911');

            $sezione4DirittoDiProprieta = $this->getByKey('sezione_4_-_diritto_di_proprieta_comproprieta_usufrutto_1633352912672');
            $sezione5CanoneAffitto = $this->getByKey('sezione_5_-_dichiara_che_per_il_canone_d_affitto_2020_1633429051710');
									   
            $primoContrattoAffittoStipulatoCon = $this->getByKey('primo_contratto_affitto_stipulato_con_1637848976443');
	     $primoContrattoRegistratoInData = $this->getByKey('primo_contratto_registrato_in_data_1633502920450');
	     $primoContrattoAlN = $this->getByKey('primo_contratto_al_n_1633502905541');
            $primoContrattoUfficioDelRegistroDi = $this->getByKey('primo_contratto_presso_l_ufficio_del_registro_di_1633502926195');
            $primoContrattoAffittoDellAlloggioSitoInComuneDi = $this->getByKey('primo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633502931370');
            $primoContrattoIndirizzo = $this->getByKey('primo_contratto_indirizzo_1633502937026');
            $primoContrattoDiDimensionePariAMQ = $this->getByKey('primo_contratto_di_dimensione_pari_a_mq_1633502949315');
            $primoContrattoF = $this->getByKey('primo_contratto_f_1633502953987');
            $primoContrattoP = $this->getByKey('primo_contratto_p_1633502958554');
            $primoContrattoSub = $this->getByKey('primo_contratto_sub_1633502962590');
            $primoContrattoCat = $this->getByKey('primo_contratto_cat_1633502966706');
            $primoContrattoProprieta = $this->getByKey('primo_contratto_proprieta_1633502972107');
            $primoContrattoPerCui_eStato = $this->getByKey('primo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529177922');
            $primoContrattoPeriodoDiMensilita = $this->getByKey('primo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633502982106');
            $primoContrattoRegolaPagamenti = $this->getByKey('indicare_se_in_regola_con_i_pagamenti_1636724684210');

            
            $secondoContrattoAffittoStipulatoCon = $this->getByKey('secondo_contratto_affitto_stipulato_con_1633504536849');
            $secondoContrattoRegistratoInData = $this->getByKey('secondo_contratto_registrato_in_data_1633504545461');
            $secondoContrattoAlN = $this->getByKey('secondo_contratto_al_n_1633504742493');
            $secondoContrattoUfficioDelRegistroDi = $this->getByKey('secondo_contratto_presso_l_ufficio_del_registro_1633504561493');
            $secondoContrattoAffittoDellAlloggioSitoInComuneDi = $this->getByKey('secondo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633505669318');
            $secondoContrattoIndirizzo = $this->getByKey('secondo_contratto_indirizzo_1633504610787');
            $secondoContrattoDiDimensionePariAMQ = $this->getByKey('secondo_contratto_di_dimensione_pari_a_mq_1633506153446');
            $secondoContrattoF = $this->getByKey('secondo_contratto_f_1633506263052');
            $secondoContrattoP  = $this->getByKey('secondo_contratto_p_1633506274291');
            $secondoContrattoSub = $this->getByKey('secondo_contratto_sub_1633506282705');
            $secondoContrattoCat = $this->getByKey('secondo_contratto_cat_1633506302474');
            $secondoContrattoProprieta = $this->getByKey('secondo_contratto_proprieta_1633506678222');
            $secondoContrattoPerCui_eStato = $this->getByKey('secondo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529401440');
            $secondoContrattoPeriodoDiMensilita = $this->getByKey('secondo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633506700750');

            $terzoContrattoAffittoStipulatoCon = $this->getByKey('terzo_contratto_affitto_stipulato_con_1633508157510');
            $terzoContrattoRegistratoInData = $this->getByKey('terzo_contratto_registrato_in_data_1633508166161');
            $terzoContrattoAlN = $this->getByKey('terzo_contratto_al_n_1633508174162');
            $terzoContrattoUfficioDelRegistroDi = $this->getByKey('terzo_contratto_presso_l_ufficio_del_registro_1633508184002');
            $terzoContrattoAffittoDellAlloggioSitoInComuneDi = $this->getByKey('terzo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633508298424');
            $terzoContrattoIndirizzo = $this->getByKey('terzo_contratto_indirizzo_1633508307145');
            $terzoContrattoDiDimensionePariAMQ = $this->getByKey('terzo_contratto_di_dimensione_pari_a_mq_1633508315995');
            $terzoContrattoF = $this->getByKey('terzo_contratto_f_1633508421308');
            $terzoContrattoP  = $this->getByKey('terzo_contratto_p_1633508429031');
            $terzoContrattoSub = $this->getByKey('terzo_contratto_sub_1633508440982');
            $terzoContrattoCat = $this->getByKey('terzo_contratto_cat_1633508448974');
            $terzoContrattoProprieta = $this->getByKey('terzo_contratto_proprieta_1633508503142');
            $terzoContrattoPerCui_eStato = $this->getByKey('terzo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529575542');
            $terzoContrattoPeriodoDiMensilita = $this->getByKey('terzo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633508521461');
	     $inCasoDiAssegnazioneDelContributoEsso = $this->getByKey('in_caso_di_assegnazione_del_contributo_esso_dovra_essere_accreditato_sul_cc_n_1633431304011');
            $intestatoA = $this->getByKey('intestato_a_1633431335082');
            $CodiceIban = $this->getByKey('codice_iban_1633431349182');
            $CO = $this->getByKey('c_o_1633431322334');
            $signature = $this->getByKey('sig_1633431592191');
            $indirizzoRecapito = $this->getByKey('indirizzo_recapito_1633431614004');
            $indirizzoNumero = $this->getByKey('indirizzo_numero_1633509782911');
            $localita = $this->getByKey('localita_1633431647379');
            $cap = $this->getByKey('c_a_p_1633431660817');
            $provinicia = $this->getByKey('provincia_1633431677858');
            $telefonoAbitazione = $this->getByKey('telefono_abitazione_1633431712402');
            $cellulare = $this->getByKey('cellulare_1633431728138');
            $emailRecapito = $this->getByKey('email_recapito_1633431743817');
            $pec = $this->getByKey('pec_posta_elettronica_certificata_1636531106663');
            $numeroMarca = $this->getByKey('numero_della_marca_da_bollo_di_16_euro_1633611989245');
            $dataMarca = $this->getByKey('data_marca_da_bollo_1633612039871');
            $files = $this->getAllFiles();
            $basepath = NF_PDF()->dir( 'templates/' );


            $html = file_get_contents($basepath . "/index.html");
            $html = str_replace("%nome_richiedente_1591626417938%",$nomeRichiedente,$html);
            $html = str_replace("%cognome_richiedente_1591626411300%",$cognomeRichiedente,$html);
            $html = str_replace("%luogo_di_nascita_1594726021150%",$luogoDiNascita,$html);
            $html = str_replace("%data_di_nascita_1594726068041%",$dataDiNascita,$html);
            $html = str_replace("%residente_nel_comune_di_1608128287457%",$residenteNelComuneDi,$html);
            $html = str_replace("%alla_via_piazza_1608128323231%",$allaViaPiazza,$html);
            $html = str_replace("%n_civico_1595516820746%",$nCivico,$html);
            $html = str_replace("%zip_1595599856330%",$zip,$html);
            $html = str_replace("%codice_fiscale_1591616243364%",$codiceiscale,$html);
            $html = str_replace("%sezione_1_-_lett_a_-_di_essere_alla_data_di_pubblicazione_del_bando_1633339434083%",$sezione1Data,$html);
            $html = str_replace("%sezione_1_-_lett_b_-_periodo_residenza_1633339505388%",$sezione1Period,$html);
            $html = str_replace("%sezione_2_-_lett_a_-_reddito_1633339646667%",$sezione2Reddito,$html);
            $html = str_replace("%sezione_2_-_lett_b_-_che_l_attestazione_isee_2021_e_di_eururo_1633339819734%",$sezione2Euro,$html);
            $html = str_replace("%sezione_2_-_lett_c_-_presentazione_della_domanda_1633340162116%",$sezione2dellaDomanda,$html);
            $html = str_replace("%tipologia_1633612616690%",$tipologia,$html);


          
            $sezione3datahtml = '';
            $sezione3dataarray = [];
            if(isset($sezione3Data)){
                $sezione3dataarray = json_decode($sezione3Data, true);
            }
            
            
            if(count($sezione3dataarray) > 0){
                for ($i = 0; $i < count($sezione3dataarray); $i++) {
                    $tcognome  = $sezione3dataarray[$i][0];
                    $tnome 	  = $sezione3dataarray[$i][1];
                    $tcomune   = $sezione3dataarray[$i][2];
                    $tprovince 	  = $sezione3dataarray[$i][3];
                    $tdata_nascita = $sezione3dataarray[$i][4];
                    $tsesso = $sezione3dataarray[$i][5];
                    $tcivile = $sezione3dataarray[$i][6];
                    $tgrado = $sezione3dataarray[$i][7];
                    if(!empty($tcognome) || !empty($tnome) || !empty($tcomune) || !empty($tprovince) || !empty($tdata_nascita) || !empty($tsesso) 
                        || !empty($tcivile)  || !empty($tgrado)){
                            $sezione3datahtml .= '<tr class="t2-tr-2">
                      <td class="t2-td-2" colspan="1" rowspan="1">
                         <p class="t2-para-1"><span class="f-arial td-3">&nbsp; ' . ($i+1) . ')</span></p>
                      </td>
                      <td class="t2-td-3" colspan="1" rowspan="1">
                         <p class="para-4 pt-pt6 table-p t-para-4 pt-pt6"><span class="t2-span-4">Cognome <strong>' . $tcognome . '</strong> Nome <strong>' . $tnome . '</strong></span></p>
                         <p class="para-4 table-p"><span class="t2-span-4">Comune/Stato di nascita <strong>' . $tcomune . '</strong> Provincia <strong>' . $tprovince . ' </strong>data di nascita <strong>' . $tdata_nascita . '</strong> sesso <strong>' . $tsesso . '</strong> Stato civile <strong>' . $tcivile . '</strong> Parentela <strong>' . $tgrado . '</strong></span></p>
                         <p class="t-para-2 h-10"><span class="t2-span-1 span-7 t2-span-3"></span></p>
                        </td>';
                    }
                }
            }
            $html = str_replace("%sezione3_table_data_section%",$sezione3datahtml,$html);
            $html = str_replace("%sezione_4_-_diritto_di_proprieta_comproprieta_usufrutto_1633352912672%",$sezione4DirittoDiProprieta,$html);
            $html = str_replace("%sezione_5_-_dichiara_che_per_il_canone_d_affitto_2020_1633429051710%",$sezione5CanoneAffitto,$html);
        
            $html = str_replace("%primo_contratto_affitto_stipulato_con_1637848976443%",$primoContrattoAffittoStipulatoCon,$html);
            $html = str_replace("%primo_contratto_registrato_in_data_1633502920450%",$primoContrattoRegistratoInData,$html);
            $html = str_replace("%primo_contratto_al_n_1633502905541%",$primoContrattoAlN,$html);
            $html = str_replace("%primo_contratto_presso_l_ufficio_del_registro_di_1633502926195%",$primoContrattoUfficioDelRegistroDi,$html);
            $html = str_replace("%primo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633502931370%",$primoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
            $html = str_replace("%primo_contratto_indirizzo_1633502937026%",$primoContrattoIndirizzo,$html);
            $html = str_replace("%primo_contratto_di_dimensione_pari_a_mq_1633502949315%",$primoContrattoDiDimensionePariAMQ,$html);
            $html = str_replace("%primo_contratto_f_1633502953987%",$primoContrattoF,$html);
            $html = str_replace("%primo_contratto_p_1633502958554%",$primoContrattoP,$html);
            $html = str_replace("%primo_contratto_sub_1633502962590%",$primoContrattoSub,$html);
            $html = str_replace("%primo_contratto_cat_1633502966706%",$primoContrattoCat,$html);
            $html = str_replace("%primo_contratto_proprieta_1633502972107%",$primoContrattoProprieta,$html);
            $html = str_replace("%primo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529177922%",$primoContrattoPerCui_eStato,$html);
            $html = str_replace("%primo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633502982106%",$primoContrattoPeriodoDiMensilita,$html);
            $html = str_replace("%indicare_se_in_regola_con_i_pagamenti_1636724684210%",$primoContrattoRegolaPagamenti,$html);
        
            $html = str_replace("%secondo_contratto_affitto_stipulato_con_1633504536849%",$secondoContrattoAffittoStipulatoCon,$html);
            $html = str_replace("%secondo_contratto_registrato_in_data_1633504545461%",$secondoContrattoRegistratoInData,$html);
            $html = str_replace("%secondo_contratto_al_n_1633504742493%",$secondoContrattoAlN,$html);
            $html = str_replace("%secondo_contratto_presso_l_ufficio_del_registro_1633504561493%",$secondoContrattoUfficioDelRegistroDi,$html);
            $html = str_replace("%secondo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633505669318%",$secondoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
            $html = str_replace("%secondo_contratto_indirizzo_1633504610787%",$secondoContrattoIndirizzo,$html);
            $html = str_replace("%secondo_contratto_di_dimensione_pari_a_mq_1633506153446%",$secondoContrattoDiDimensionePariAMQ,$html);
            $html = str_replace("%secondo_contratto_f_1633506263052%",$secondoContrattoF,$html);
            $html = str_replace("%secondo_contratto_p_1633506274291%",$secondoContrattoP,$html);
            $html = str_replace("%secondo_contratto_sub_1633506282705%",$secondoContrattoSub,$html);
            $html = str_replace("%secondo_contratto_cat_1633506302474%",$secondoContrattoCat,$html);
            $html = str_replace("%secondo_contratto_proprieta_1633506678222%",$secondoContrattoProprieta,$html);
            $html = str_replace("%secondo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529401440%",$secondoContrattoPerCui_eStato,$html);
            $html = str_replace("%secondo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633506700750%",$secondoContrattoPeriodoDiMensilita,$html);
        
        
            $html = str_replace("%terzo_contratto_affitto_stipulato_con_1633508157510%",$terzoContrattoAffittoStipulatoCon,$html);
            $html = str_replace("%terzo_contratto_registrato_in_data_1633508166161%",$terzoContrattoRegistratoInData,$html);
            $html = str_replace("%terzo_contratto_al_n_1633508174162%",$terzoContrattoAlN,$html);
            $html = str_replace("%terzo_contratto_presso_l_ufficio_del_registro_1633508184002%",$terzoContrattoUfficioDelRegistroDi,$html);
            $html = str_replace("%terzo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1633508298424%",$terzoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
            $html = str_replace("%terzo_contratto_indirizzo_1633508307145%",$terzoContrattoIndirizzo,$html);
            $html = str_replace("%terzo_contratto_di_dimensione_pari_a_mq_1633508315995%",$terzoContrattoDiDimensionePariAMQ,$html);
            $html = str_replace("%terzo_contratto_f_1633508421308%",$terzoContrattoF,$html);
            $html = str_replace("%terzo_contratto_p_1633508429031%",$terzoContrattoP,$html);
            $html = str_replace("%terzo_contratto_sub_1633508440982%",$terzoContrattoSub,$html);
            $html = str_replace("%terzo_contratto_cat_1633508448974%",$terzoContrattoCat,$html);
            $html = str_replace("%terzo_contratto_proprieta_1633508503142%",$terzoContrattoProprieta,$html);
            $html = str_replace("%terzo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1636529575542%",$terzoContrattoPerCui_eStato,$html);
            $html = str_replace("%terzo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1633508521461%",$terzoContrattoPeriodoDiMensilita,$html);
            
        
            $html = str_replace("%in_caso_di_assegnazione_del_contributo_esso_dovra_essere_accreditato_sul_cc_n_1633431304011%",$inCasoDiAssegnazioneDelContributoEsso,$html);
            $html = str_replace("%intestato_a_1633431335082%",$intestatoA,$html);
            $html = str_replace("%codice_iban_1633431349182%",$CodiceIban,$html);
            $html = str_replace("%c_o_1633431322334%",$CO,$html);
            $html = str_replace("%sig_1633431592191%",$signature,$html);
            $html = str_replace("%indirizzo_recapito_1633431614004%",$indirizzoRecapito,$html);
            $html = str_replace("%indirizzo_numero_1633509782911%",$indirizzoNumero,$html);
            $html = str_replace("%localita_1633431647379%",$localita,$html);
            $html = str_replace("%c_a_p_1633431660817%",$cap,$html);
            $html = str_replace("%provincia_1633431677858%",$provinicia,$html);
            $html = str_replace("%telefono_abitazione_1633431712402%",$telefonoAbitazione,$html);
            $html = str_replace("%cellulare_1633431728138%",$cellulare,$html);
            $html = str_replace("%email_recapito_1633431743817%",emailRecapito ,$html);
            $html = str_replace("%pec_posta_elettronica_certificata_1636531106663%",$pec,$html);
        
            $html = str_replace("%numero_della_marca_da_bollo_di_16_euro_1633611989245%",$numeroMarca,$html);
            $html = str_replace("%data_marca_da_bollo_1633612039871%",$dataMarca,$html);
            $files_html = '';
            if(isset($files) && count($files) > 0 ){
        
                $files_html = "<ul>";
                for ($f = 0; $f < count($files); $f++) {
                    if(isset($files[$f][0]) && isset($files[$f][0]['name'])){
                        $files_html .= "<li>" . $files[$f][0]['name'] . "</li>";
                    }
                }
                $files_html .= "</ul>";
            }

            $html = str_replace("%elenco_della_documentazione_allegata%",$files_html,$html);
            $imagepath = NF_PDF()->dir( 'assets/img' );
            $html = str_replace("%domande_image_1%",$imagepath . '/domande1.jpg',$html);
            $tempname = tempnam('', 'domanda_');
            rename($tempname, $tempname .= '.pdf');
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
	        $pdf =  $mpdf->Output( $tempname, 'F');
	        $pdf_data = file_get_contents($tempname);
	        $pdf_encoded_string = base64_encode($pdf_data);
            error_log("pdf_encoded_string :".print_r($pdf_encoded_string,1));
            $attachmentsArea .="<attachment>\n";
            $attachmentsArea .="	<fileset>$fileSet</fileset>\n";
            $attachmentsArea .="	<filename>domanda aggiornata.pdf</filename>\n";
            $attachmentsArea .="	<contentType>application/pdf</contentType>\n";
            $attachmentsArea .="	<data>".$pdf_encoded_string."</data>\n";
            $attachmentsArea .="</attachment>\n";
            
        }
		$attachmentsArea .= "</attachments>";

        $sub_string = $soggetto . ": " . $email . ' ' . $fullname . ' - Cod.Fisc.:' . $codiceFiscale;
        if($form_id == 62){
            $cognome_studente = $this->getByKey('cognome_studente_1635424441647');
            $nome_studente = $this->getByKey('nome_studente_1635424450296');
            $sub_string .=  ' per studente:' . $cognome_studente . ' ' . $nome_studente;
        }

        return <<<XML
<soapenv:Envelope
	xmlns:ins="http://www.isharedoc.it/schemas/instance"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Header>
		<wsse:Security soapenv:mustUnderstand="1"
			xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
			xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
			<wsse:UsernameToken>
				<wsse:Username>$this->soapUsername</wsse:Username>
				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">$this->soapPassword</wsse:Password>
				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">$nonce</wsse:Nonce>
				<wsu:Created>$createDate</wsu:Created>
			</wsse:UsernameToken>
		</wsse:Security>
	</soapenv:Header>
	<soapenv:Body>
		<ins:InstanceMessageCreateRequest>
			<partitionId>3</partitionId>
			<messageBoxId>2</messageBoxId>
			<storyboardCode>protocollo</storyboardCode>
			<direction>IN</direction>
			<subject>$sub_string</subject>
			<contacts>
				<contact>
					<type>O</type>
					<referenceType>GBOX</referenceType>
					<searchType>CODE</searchType>
					<searchMode>NOTFOUNDDEF</searchMode>
					<description>$fullname</description>
					<email>$email</email>
					<searchValue>$searchValue</searchValue>
					</contact>
				<contact>
					<type>T</type>
					<referenceType>GBOX</referenceType>
					<searchType>CODE</searchType>
					<searchValue>$searchValue</searchValue>
				</contact>
				<contact>
					<type>F</type>
					<referenceType>AB</referenceType>
					<searchType>EMAIL</searchType>
					<searchValue>$email</searchValue>
					<searchMode>NOTFOUNDDEF</searchMode>
					<description>$fullname</description>
					<email>$email</email>
				</contact>
			</contacts>
			<topics>
				<topic>
					<path>$topicPath</path>
				</topic>
			</topics>
			<variables>
				<variable>
					<key>modTrasmissione</key>
					<type>string</type>
					<valueString>flusso digitale</valueString>
				</variable>
			</variables>
			$attachmentsArea
			<startWorkflow>true</startWorkflow>
			<instanceOperation>protocollo</instanceOperation>
		</ins:InstanceMessageCreateRequest>
	</soapenv:Body>
</soapenv:Envelope>
XML;

    }

    private function send_request($wsdl,$xml_post_string){
		//return $this->dumpResponse();
        $soap_do = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Content-Length: ".strlen($xml_post_string)
        );

		error_log("WSDL: ".$wsdl);
		error_log("POST: ".$xml_post_string);

        curl_setopt($soap_do, CURLOPT_URL, $wsdl);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 240);
        curl_setopt($soap_do, CURLOPT_TIMEOUT,        240);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST,           true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($soap_do, CURLOPT_HEADER, 1);
        curl_setopt($soap_do, CURLOPT_FAILONERROR, FALSE);
        $response  = curl_exec($soap_do);
        $header_size = curl_getinfo($soap_do, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $http_status = curl_getinfo($soap_do, CURLINFO_HTTP_CODE);
        $curl_errno= curl_errno($soap_do);
        curl_close($soap_do);

        //$response = str_replace("<soap:Body>","",$response) ;
        //$response = str_replace("</soap:Body>","",$response);

        $response_data = array(
            'response' => $response,
            'http_status' => $http_status,
            'curl_errno' => $curl_errno,
            'response_body' => $body
        );

		error_log("RESPONSE DATA:".print_r($response_data,true));
        return $response_data;
    }

    /**
     * Add PDF download link on the view form submission page
     *
     * @param $name   name of the file
     * @param $sub_id submission id
     * @since 1.3.3
     */
    public function get_pdf_file_name( $name = 'ninja-forms-submission', $sub_id = '' ) {
        return apply_filters( 'ninja_forms_submission_pdf_name', $name, $sub_id );
    }

    private function dumpResponse()
    {
        $response = <<<XML
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Header/>
   <SOAP-ENV:Body>
      <ns3:InstanceMessageCreateResponse xmlns:ns3="http://www.isharedoc.it/schemas/instance">
         <id>12393</id>
         <identifier>0000003</identifier>
         <identifierDate>2020-06-10T10:11:17.135+02:00</identifierDate>
         <viewUrl>http://interpademo.comter.net/isharedoc/workgroupHome/view.action?instance=12393</viewUrl>
      </ns3:InstanceMessageCreateResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
XML;

        // $xml = simplexml_load_string($response);
        //
        // $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        // var_dump(json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0])));

		$response_data= array(
            'response' => $response,
            'http_status' => 200,
            'curl_errno' => 0,
            'response_body' => $response
        );


		error_log("DUMP RESPONSE DATA:".print_r($response_data,true));
		return $response_data;
    }

    /**
     * @param $responseBody
     */
    private function processXMLResponse($responseBody)
    {
		$responseBody = trim($responseBody);
		$responseBody =  preg_replace("/[\n\r]/","",$responseBody);

		$pos = strpos($responseBody, 'Content-Type');
		$part = substr($responseBody,0,$pos);
		$responseBody = str_replace($part,"", $responseBody);
		$responseBody = str_replace("--","", $responseBody);

		$pos = strpos($responseBody, '<SOAP-ENV');
		$contentType = substr($responseBody,0,$pos);
		$responseBody = str_replace($contentType,"", $responseBody);
        error_log("RESPONSE-BODY: ".$responseBody);
		$xml = simplexml_load_string($responseBody);
		error_log("XML-BODY: ".$xml);
        $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        $InstanceMessageCreateResponse = json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0]));

        // $id = $InstanceMessageCreateResponse->id;
        $identifier = $InstanceMessageCreateResponse->identifier;
        $identifierDate = $InstanceMessageCreateResponse->identifierDate;

        global $wpdb;
        $form_id = $this->form_data['form_id'];
        $sub_id = (isset($this->form_data['actions']['save']))? $this->form_data['actions']['save']['sub_id'] : null;

        // $protocol_field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolid' AND `parent_id` = {$form_id}" );
        $protocol_field_number = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolnumber' AND `parent_id` = {$form_id}"
        );
        $protocol_field_date = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocoldate' AND `parent_id` = {$form_id}"
        );

		error_log("PROTOCOL FIELD NUMBER = ".$protocol_field_number);
		error_log("PROTOCOL FIELD DATE = ".$protocol_field_date);
		error_log("SUB_ID:".$sub_id);
		error_log("FORM_ID:".$form_id);

        // update_post_meta( $sub_id, '_field_' . $protocol_field_id , $id );
        update_post_meta($sub_id, '_field_' . $protocol_field_number, $identifier);
        $protocol_field_fmt = date('d-m-Y H:i:s', strtotime($identifierDate));
	    update_post_meta($sub_id, '_field_' . $protocol_field_date, $protocol_field_fmt);
    }

    protected function getPdfTemplateMetadata()
    {
        return json_decode($this->pdf_template_dat->metadata, true);
    }

    protected function getMapDataFromFormField($field, $fallbackLabel)
    {
        $fieldId = (int) ($this->getPdfTemplateMetadata()[$field] ?? '');
	 
        if (!$fieldId || !isset($this->form_data['fields'][$fieldId]['value'])) {
            return $this->getFieldValueFromLabel($fallbackLabel);
        }

        return $this->form_data['fields'][$fieldId]['value'];
    }

    protected function getSearchValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_searchvalue'] ?? '';
        if (!$val) {
            return 'DIPPFSF';
        }
        return $val;
    }

    protected function getTopicPath()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_path'] ?? '';
        if (!$val) {
            return '002/001/002';
        }
        return $val;
    }

    protected function getSoggettoValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_soggetto'] ?? '';
        if (!$val) {
            return 'Richiesta di assegnazioni del contributo sul canone d’affitto anno 2020 Mittente';
        }
        return $val;
    }
}

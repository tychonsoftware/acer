<?php

date_default_timezone_set('Europe/Rome');

class NF_Custom_Webservices_PagoPA
{
    private $form_data;
    private $action_settings;
    private $pdf_template_dat;
	private $attachments;

    /**
     * @var SoapClient
     */
    private $client;
    private $wsdl;
    private $soapUsername;
    private $soapPassword;
    private $tipoVersamento;

    public function __construct($form_data,$pdf_template_dat) {
        $this->initClient();
        $this->form_data = $form_data;
        $this->pdf_template_dat = $pdf_template_dat;
		error_log("PAGO PA Form Data: ".print_r($this->form_data,true));
    }

    private function initClient()
    {
        global $wpdb;

        $table_name = "servmask_prefix_portal";
        $query = "SELECT * FROM $table_name WHERE id = 3";
        $soap_logins = $wpdb->get_results($query);
        $soap_login = $soap_logins[0];
        $this->soapUsername = $soap_login->soap_username;
        $this->soapPassword = $soap_login->soap_password;
        $this->tipoVersamento = $soap_login->tipoVersamento;
        $this->wsdl = $soap_login->soap_url;
    }

    public function makeRequest()
    {
        try {
            //$this->xml();
            $result = $this->send_request($this->wsdl, $this->xml());
            if (isset($result['http_status']) && $result['http_status'] > 300) {
                return [
                    'error' => 'Errore durante la protocollazione della domanda. Si prega di riprovare'
                ];
            }
            $error = $this->processXMLResponse($result['response_body']);
            
            if(isset($error)){
                //$data[ 'error' ] =  $error;
                //return $data;
            }
            
            return [];
        } catch (\Exception $e) {
            if ( WP_DEBUG === true ) {
                error_log('No PDF template associated with ');
            }
            return [
                'error' => 'Error in sending document to external service. No PDF template associated with'
            ];
        }
    }

    public function checkStatus($sub_id, $form_id)
    {
       $result = $this->send_request($this->wsdl, $this->status_xml($sub_id, $form_id));

       if (isset($result['http_status']) && $result['http_status'] > 300) {
        return [
            'error' => 'Errore durante la protocollazione della domanda. Si prega di riprovare'
         ];
        }
        $xml_response = $this->processStatusXMLResponse($result['response_body']);
        if(isset($xml_response->fault)){
            return [
                'error' => "Si è verificato un errore durante la richiesta di pagamento: " . $xml_response->fault->faultCode . "(". $xml_response->fault->faultString . ")"
             ];
        }
        
        return $xml_response;
    }

	private function getFieldValueFromLabel($fieldName){
		$returnValue = "";
		$formFields = $this->form_data['fields'];

		foreach($formFields as $field){
			$label = strtoupper($field['label']);
			$fieldName = strtoupper ($fieldName);
			if($label == $fieldName){
				$returnValue = $field['value'];
				break;
			}
		}
		return $returnValue;
	}

    protected function xml()
    {
        $tipoidentificativo = $this->getMapDataFromFormField('pago_pa_tipo_identificativo', "TipoIdentIficativoUnivoco");
        $codiceidentificativo = $this->getMapDataFromFormField('pago_pa_codice_identificativo', "CodiceIdentificativoUnivoco");
        $anagraficapagatore = $this->getMapDataFromFormField('pago_pa_anagrafica_pagatore', "AnagraficaPagatore");
        $indirizzopagatore = $this->getMapDataFromFormField('pago_pa_indirizzo_pagatore', "IndirizzoPagatore");
        $civicopagatore = $this->getMapDataFromFormField('pago_pa_civico_pagatore', "CivicoPagatore");
        $cappagatore = $this->getMapDataFromFormField('pago_pa_cap_pagatore', "CapPagatore");
        $localitapagatore = $this->getMapDataFromFormField('pago_pa_localita_pagatore', "LocalitaPagatore");
        $provinciapagatore = $this->getMapDataFromFormField('pago_pa_provincia_pagatore', "ProvinciaPagatore");
        $nazionepagatore = $this->getMapDataFromFormField('pago_pa_nazione_pagatore', "NazionePagatore");
        $emailpagatore = $this->getMapDataFromFormField('pago_pa_email_pagatore', "EmailPagatore");
        $identificativounivocodovuto = $this->getMapDataFromFormField('pago_pa_identificativo_univoco_dovuto', "IdentificativoUnivocoDovuto");
        $importosingoloversamento = $this->getMapDataFromFormField('pago_pa_importo_singolo_versamento', "ImportoSingoloVersamento");
        $identificativotipodovuto = $this->getMapDataFromFormField('pago_pa_identificativo_tipo_dovuto', "IdentificativoTipoDovuto");
        $causaleversamento = $this->getMapDataFromFormField('pago_pa_causale_versamento', "CausaleVersamento");
        $datispecificiriscossione = $this->getMapDataFromFormField('pago_pa_dati_specifici_riscossione', "DatiSpecificiRiscossione");


        error_log(get_site_url() . "DATI: ".$tipoidentificativo . " Codice : " . $codiceidentificativo . " Nazione " . $nazionepagatore);

        $sample_xml_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:ppt="http://www.regione.veneto.it/pagamenti/ente/ppthead" xmlns:ente="http://www.regione.veneto.it/pagamenti/ente/">
        <soapenv:Header>
        <ppt:intestazionePPT>
        <codIpaEnte>COD_ENTE</codIpaEnte>
        </ppt:intestazionePPT>
        </soapenv:Header>
        <soapenv:Body>
        <ente:paaSILInviaDovuti>
        <password>XXXXXXXXXXXXX</password>
        <dovuti>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZyA9ICJVVEYtOCIgc3RhbmRhbG9uZSA9ICJ5ZXMiID8+DQo8RG92dXRpIHhtbG5zPSJodHRwOi8vd3d3Ln
        JlZ2lvbmUudmVuZXRvLml0L3NjaGVtYXMvMjAxMi9QYWdhbWVudGkvRW50ZS8iIHhtbG5zOm5zND0iaHR0cDovL3d3dy5yZWdpb25lLnZlbmV0by5pdC9wYWdhbWVu
        dGkvZW50ZS8iIHhtbG5zOm5zMz0iaHR0cDovL3d3dy5yZWdpb25lLnZlbmV0by5pdC9wYWdhbWVudGkvZW50ZS9wcHRoZWFkIj4NCgk8dmVyc2lvbmVPZ2dldHRvPj
        YuMi4wPC92ZXJzaW9uZU9nZ2V0dG8+DQoJPHNvZ2dldHRvUGFnYXRvcmU+DQoJCTxpZGVudGlmaWNhdGl2b1VuaXZvY29QYWdhdG9yZT4NCgkJCTx0aXBvSWRlbnRp
        ZmljYXRpdm9Vbml2b2NvPkY8L3RpcG9JZGVudGlmaWNhdGl2b1VuaXZvY28+DQoJCQk8Y29kaWNlSWRlbnRpZmljYXRpdm9Vbml2b2NvPlJTU01SQTg1VDEwQTU2Ml
        M8L2NvZGljZUlkZW50aWZpY2F0aXZvVW5pdm9jbz4NCgkJPC9pZGVudGlmaWNhdGl2b1VuaXZvY29QYWdhdG9yZT4NCgkJPGFuYWdyYWZpY2FQYWdhdG9yZT5ST1NT
        SSBNQVJJTzwvYW5hZ3JhZmljYVBhZ2F0b3JlPg0KCQk8aW5kaXJpenpvUGFnYXRvcmU+VklBIFJPTUE8L2luZGlyaXp6b1BhZ2F0b3JlPg0KCQk8Y2l2aWNvUGFnYX
        RvcmU+MjA8L2Npdmljb1BhZ2F0b3JlPg0KCQk8Y2FwUGFnYXRvcmU+NTYwMTc8L2NhcFBhZ2F0b3JlPg0KCQk8bG9jYWxpdGFQYWdhdG9yZT5TQU4gR0lVTElBTk8g
        VEVSTUU8L2xvY2FsaXRhUGFnYXRvcmU+DQoJCTxwcm92aW5jaWFQYWdhdG9yZT5QSTwvcHJvdmluY2lhUGFnYXRvcmU+DQoJCTxuYXppb25lUGFnYXRvcmU+SVQ8L2
        5hemlvbmVQYWdhdG9yZT4NCiAgICAgIDxlLW1haWxQYWdhdG9yZT5wYWdvcGEuY2FtcGFuaWFAYWxtYXZpdmEuaXQ8L2UtbWFpbFBhZ2F0b3JlPg0KCTwvc29nZ2V0
        dG9QYWdhdG9yZT4NCgk8ZGF0aVZlcnNhbWVudG8+DQoJCTx0aXBvVmVyc2FtZW50bz5BTEw8L3RpcG9WZXJzYW1lbnRvPg0KCQk8ZGF0aVNpbmdvbG9WZXJzYW1lbn
        RvPg0KCQkJPGlkZW50aWZpY2F0aXZvVW5pdm9jb0RvdnV0bz4zMDI3ODYzNjEyPC9pZGVudGlmaWNhdGl2b1VuaXZvY29Eb3Z1dG8+DQoJCQk8aW1wb3J0b1Npbmdv
        bG9WZXJzYW1lbnRvPjE1LjAwPC9pbXBvcnRvU2luZ29sb1ZlcnNhbWVudG8+DQoJCQk8aWRlbnRpZmljYXRpdm9UaXBvRG92dXRvPjAwMDE8L2lkZW50aWZpY2F0aX
        ZvVGlwb0RvdnV0bz4NCgkJCTxjYXVzYWxlVmVyc2FtZW50bz5URVNUIFBBR0FNRU5UTyBXUyMwMDAxIzMwMjc4NjM2MTI8L2NhdXNhbGVWZXJzYW1lbnRvPg0KCQkJ
        PGRhdGlTcGVjaWZpY2lSaXNjb3NzaW9uZT45LzMwMjc4NjM2MTI8L2RhdGlTcGVjaWZpY2lSaXNjb3NzaW9uZT4NCgkJPC9kYXRpU2luZ29sb1ZlcnNhbWVudG8+DQ
        oJPC9kYXRpVmVyc2FtZW50bz4NCjwvRG92dXRpPg==</dovuti>
        <enteSILInviaRispostaPagamentoUrl>http://localhost:9080/demoMyPayInteg/esitoTransazione.jsp?iud=3027863612</enteSILInviaRispostaPagamentoUrl>
        <showMyPay>0</showMyPay>
        </ente:paaSILInviaDovuti>
        </soapenv:Body>
        </soapenv:Envelope>';
        //return $sample_xml_string;

        $dovuti = '<?xml version="1.0" encoding = "UTF-8" standalone = "yes" ?>
        <Dovuti xmlns="http://www.regione.veneto.it/schemas/2012/Pagamenti/Ente/" xmlns:ns4="http://www.regione.veneto.it/pagamenti/ente/" xmlns:ns3="http://www.regione.veneto.it/pagamenti/ente/ppthead">
            <versioneOggetto>6.2.0</versioneOggetto>
            <soggettoPagatore>
                <identificativoUnivocoPagatore>
                    <tipoIdentificativoUnivoco>' . $tipoidentificativo . '</tipoIdentificativoUnivoco>
                    <codiceIdentificativoUnivoco>' . $codiceidentificativo . '</codiceIdentificativoUnivoco>
                </identificativoUnivocoPagatore>
                <anagraficaPagatore>'.$anagraficapagatore . '</anagraficaPagatore>
                <indirizzoPagatore>'.$indirizzopagatore . '</indirizzoPagatore>
                <civicoPagatore>'. $civicopagatore  . '</civicoPagatore>
                <capPagatore>'. $cappagatore .'</capPagatore>
                <localitaPagatore>'. $localitapagatore . '</localitaPagatore>
                <provinciaPagatore>'. $provinciapagatore .'</provinciaPagatore>
                <nazionePagatore>'. $nazionepagatore . '</nazionePagatore>
              <e-mailPagatore>'. $emailpagatore .'</e-mailPagatore>
            </soggettoPagatore>
            <datiVersamento>
                <tipoVersamento>' . $this->tipoVersamento . '</tipoVersamento>
                <datiSingoloVersamento>
                    <identificativoUnivocoDovuto>'. $identificativounivocodovuto . '</identificativoUnivocoDovuto>
                    <importoSingoloVersamento>'. $importosingoloversamento . '</importoSingoloVersamento>
                    <identificativoTipoDovuto>'. $identificativotipodovuto . '</identificativoTipoDovuto>
                    <causaleVersamento>'. $causaleversamento . '</causaleVersamento>
                    <datiSpecificiRiscossione>'. $datispecificiriscossione . '</datiSpecificiRiscossione>
                </datiSingoloVersamento>
            </datiVersamento>
        </Dovuti>';
        $base64 = base64_encode($dovuti);

        $xml_request = ' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:ppt="http://www.regione.veneto.it/pagamenti/ente/ppthead" xmlns:ente="http://www.regione.veneto.it/pagamenti/ente/">
        <soapenv:Header>
        <ppt:intestazionePPT>
        <codIpaEnte>' . $this->soapUsername . '</codIpaEnte>
        </ppt:intestazionePPT>
        </soapenv:Header>
        <soapenv:Body>
        <ente:paaSILInviaDovuti>
        <password>'. $this->soapPassword . '</password>
        <dovuti>'. $dovuti . '</dovuti>
        <enteSILInviaRispostaPagamentoUrl>'. get_site_url() .'</enteSILInviaRispostaPagamentoUrl>
        <showMyPay>0</showMyPay>
        </ente:paaSILInviaDovuti>
        </soapenv:Body>
        </soapenv:Envelope>';
        error_log("XML Request : " . $xml_request);
        return $xml_request;
    }

    protected function status_xml($sub_id, $form_id)
    {
        global $wpdb;
        $payment_session_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_session_id' AND `parent_id` = {$form_id}" );
        $idSession = get_post_meta($sub_id, '_field_' . $payment_session_id , true);

        $xml_status_request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:ente="http://www.regione.veneto.it/pagamenti/ente/">"
        <soapenv:Header/>
        <soapenv:Body>
        <ente:paaSILChiediPagatiConRicevuta>
        <codIpaEnte>' . $this->soapUsername . '</codIpaEnte>
        <password>'. $this->soapPassword . '</password>
        <idSession>'. $idSession . '</idSession>
        </ente:paaSILChiediPagatiConRicevuta>
        </soapenv:Body>
        </soapenv:Envelope>
        ';
        error_log("Status XML Request : " . $xml_status_request);
        return $xml_status_request;
    }


    private function send_request($wsdl,$xml_post_string){
		//return $this->dumpResponse();
      //  return $this->dumpPaymentResponse();
        $soap_do = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Content-Length: ".strlen($xml_post_string)
        );

		error_log("WSDL: ".$wsdl);
		error_log("POST: ".$xml_post_string);

        curl_setopt($soap_do, CURLOPT_URL, $wsdl);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($soap_do, CURLOPT_TIMEOUT,        120);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST,           true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($soap_do, CURLOPT_HEADER, 1);
        curl_setopt($soap_do, CURLOPT_FAILONERROR, FALSE);
        $response  = curl_exec($soap_do);
        $header_size = curl_getinfo($soap_do, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $http_status = curl_getinfo($soap_do, CURLINFO_HTTP_CODE);
        $curl_errno= curl_errno($soap_do);
        curl_close($soap_do);

        //$response = str_replace("<soap:Body>","",$response) ;
        //$response = str_replace("</soap:Body>","",$response);

        $response_data = array(
            'response' => $response,
            'http_status' => $http_status,
            'curl_errno' => $curl_errno,
            'response_body' => $body
        );

		error_log("RESPONSE DATA:".print_r($response_data,true));
        return $response_data;
    }
    

    private function dumpResponse()
    {
        $response = <<<XML
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
        <ns4:paaSILInviaDovutiRisposta
        xmlns:ns2="http://www.regione.veneto.it/pagamenti/ente/ppthead"
        xmlns:ns3="http://www.regione.veneto.it/schemas/2012/Pagamenti/Ente/"
        xmlns:ns4="http://www.regione.veneto.it/pagamenti/ente/">
        <esito>OK</esito>
        <idSession>dfd9170b-75ce-41ed-9afe-67c965ff765f</idSession>
        <redirect>1</redirect>
        <url>https://mypay-
        test.regione.campania.it/pa/public/carrello/paaSILInviaRichiestaPagamento.html?idSessi
        on=dfd9170b-75ce-41ed-9afe-67c965ff765f</url>
        </ns4:paaSILInviaDovutiRisposta>
        </soap:Body>
        </soap:Envelope>
XML;

        // $xml = simplexml_load_string($response);
        //
        // $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        // var_dump(json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0])));

		$response_data= array(
            'response' => $response,
            'http_status' => 200,
            'curl_errno' => 0,
            'response_body' => $response
        );


		error_log("RESPONSE DATA:".print_r($response_data,true));
		return $response_data;
    }

    private function dumpPaymentResponse()
    {
        $response = <<<XML
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<ns4:paaSILChiediPagatiConRicevutaRisposta xmlns:ns2="http://www.regione.veneto.it/pagamenti/ente/ppthead" xmlns:ns3="http://www.regione.veneto.it/schemas/2012/Pagamenti/Ente/"
xmlns:ns4="http://www.regione.veneto.it/pagamenti/ente/">
<pagati>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxwYXlfaTpSVCB4bWxuczpwYXlfaT0iaHR0cDovL3d3dy5kaWdpdHBhLmdvdi5pdC9zY2hlbWFzLzIwMTEvUGFnYW1lbnRpLyIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTF
NjaGVtYS1pbnN0YW5jZSIgeHNpOnNjaGVtYUxvY2F0aW9uPSIvb3B0L3Byb2N0ZWxlL3Jlc291cmNlcy9QYWdJbmZfUlBUX1JUXzZfMl8wLnhzZCI+PHBheV9pOnZlcnNpb25lT2dnZXR0bz42LjIuMDwvcGF5X2k6dmVyc2lvbmVPZ2dldHRvPjxwYXlfaTpkb21pbmlvPjxw
YXlfaTppZGVudGlmaWNhdGl2b0RvbWluaW8+MDIzMzYzNDAxNTk8L3BheV9pOmlkZW50aWZpY2F0aXZvRG9taW5pbz48L3BheV9pOmRvbWluaW8+PHBheV9pOmlkZW50aWZpY2F0aXZvTWVzc2FnZ2lvUmljZXZ1dGE+SW1yZjEzMjVtYTczamUzYjdzNm84cGdlODV2MGcxMm
RnPC9wYXlfaTppZGVudGlmaWNhdGl2b01lc3NhZ2dpb1JpY2V2dXRhPjxwYXlfaTpkYXRhT3JhTWVzc2FnZ2lvUmljZXZ1dGE+MjAxOC0xMi0xN1QxMToyMzo1NTwvcGF5X2k6ZGF0YU9yYU1lc3NhZ2dpb1JpY2V2dXRhPjxwYXlfaTpyaWZlcmltZW50b01lc3NhZ2dpb1Jp
Y2hpZXN0YT4zNTc0YWI5ZGRmNTVhOGY0OWY2OGJhNWIwZDdmZDk3OTkzYTwvcGF5X2k6cmlmZXJpbWVudG9NZXNzYWdnaW9SaWNoaWVzdGE+PHBheV9pOnJpZmVyaW1lbnRvRGF0YVJpY2hpZXN0YT4yMDE4LTEyLTE3PC9wYXlfaTpyaWZlcmltZW50b0RhdGFSaWNoaWVzdG
E+PHBheV9pOmlzdGl0dXRvQXR0ZXN0YW50ZT48cGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvQXR0ZXN0YW50ZT48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5CPC9wYXlfaTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudGlm
aWNhdGl2b1VuaXZvY28+QkNJVElUTU08L3BheV9pOmNvZGljZUlkZW50aWZpY2F0aXZvVW5pdm9jbz48L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0F0dGVzdGFudGU+PHBheV9pOmRlbm9taW5hemlvbmVBdHRlc3RhbnRlPkludGVzYSBTYW5wYW9sbzwvcGF5X2k6ZG
Vub21pbmF6aW9uZUF0dGVzdGFudGU+PC9wYXlfaTppc3RpdHV0b0F0dGVzdGFudGU+PHBheV9pOmVudGVCZW5lZmljaWFyaW8+PHBheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0JlbmVmaWNpYXJpbz48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5HPC9wYXlf
aTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudGlmaWNhdGl2b1VuaXZvY28+MDIzMzYzNDAxNTk8L3BheV9pOmNvZGljZUlkZW50aWZpY2F0aXZvVW5pdm9jbz48L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0JlbmVmaWNpYXJpbz48cG
F5X2k6ZGVub21pbmF6aW9uZUJlbmVmaWNpYXJpbz5Db211bmUgZGkgQmlhc3Nvbm88L3BheV9pOmRlbm9taW5hemlvbmVCZW5lZmljaWFyaW8+PHBheV9pOmluZGlyaXp6b0JlbmVmaWNpYXJpbz5WaWEgc2FuIE1hcnRpbm88L3BheV9pOmluZGlyaXp6b0JlbmVmaWNpYXJp
bz48cGF5X2k6Y2l2aWNvQmVuZWZpY2lhcmlvPjk8L3BheV9pOmNpdmljb0JlbmVmaWNpYXJpbz48cGF5X2k6Y2FwQmVuZWZpY2lhcmlvPjIwODUzPC9wYXlfaTpjYXBCZW5lZmljaWFyaW8+PHBheV9pOmxvY2FsaXRhQmVuZWZpY2lhcmlvPkJpYXNzb25vPC9wYXlfaTpsb2
NhbGl0YUJlbmVmaWNpYXJpbz48cGF5X2k6cHJvdmluY2lhQmVuZWZpY2lhcmlvPk1JPC9wYXlfaTpwcm92aW5jaWFCZW5lZmljaWFyaW8+PHBheV9pOm5hemlvbmVCZW5lZmljaWFyaW8+SVQ8L3BheV9pOm5hemlvbmVCZW5lZmljaWFyaW8+PC9wYXlfaTplbnRlQmVuZWZp
Y2lhcmlvPjxwYXlfaTpzb2dnZXR0b1BhZ2F0b3JlPjxwYXlfaTppZGVudGlmaWNhdGl2b1VuaXZvY29QYWdhdG9yZT48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5GPC9wYXlfaTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudG
lmaWNhdGl2b1VuaXZvY28+TERFR0xMNjlQNjdCOTg4RzwvcGF5X2k6Y29kaWNlSWRlbnRpZmljYXRpdm9Vbml2b2NvPjwvcGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvUGFnYXRvcmU+PHBheV9pOmFuYWdyYWZpY2FQYWdhdG9yZT5BTlRXSSBNQVNTSU1PPC9wYXlfaTph
bmFncmFmaWNhUGFnYXRvcmU+PHBheV9pOmluZGlyaXp6b1BhZ2F0b3JlPlZJQSBJVEFMSUEgMTU8L3BheV9pOmluZGlyaXp6b1BhZ2F0b3JlPjxwYXlfaTpjYXBQYWdhdG9yZT4yMDEwMDwvcGF5X2k6Y2FwUGFnYXRvcmU+PHBheV9pOmxvY2FsaXRhUGFnYXRvcmU+TUlMQU
5PPC9wYXlfaTpsb2NhbGl0YVBhZ2F0b3JlPjxwYXlfaTpwcm92aW5jaWFQYWdhdG9yZT5NSTwvcGF5X2k6cHJvdmluY2lhUGFnYXRvcmU+PHBheV9pOm5hemlvbmVQYWdhdG9yZT5JVDwvcGF5X2k6bmF6aW9uZVBhZ2F0b3JlPjxwYXlfaTplLW1haWxQYWdhdG9yZT5pLnRy
ZXNvbGRpQGFjbWVpdGFsaWEuaXQ8L3BheV9pOmUtbWFpbFBhZ2F0b3JlPjwvcGF5X2k6c29nZ2V0dG9QYWdhdG9yZT48cGF5X2k6ZGF0aVBhZ2FtZW50bz48cGF5X2k6Y29kaWNlRXNpdG9QYWdhbWVudG8+MDwvcGF5X2k6Y29kaWNlRXNpdG9QYWdhbWVudG8+PHBheV9pOm
ltcG9ydG9Ub3RhbGVQYWdhdG8+My4xNzwvcGF5X2k6aW1wb3J0b1RvdGFsZVBhZ2F0bz48cGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvVmVyc2FtZW50bz5SRjMzMDAzMzAwMDAwMDAwMDAzMjAwMDAwPC9wYXlfaTppZGVudGlmaWNhdGl2b1VuaXZvY29WZXJzYW1lbnRv
PjxwYXlfaTpDb2RpY2VDb250ZXN0b1BhZ2FtZW50bz5uL2E8L3BheV9pOkNvZGljZUNvbnRlc3RvUGFnYW1lbnRvPjxwYXlfaTpkYXRpU2luZ29sb1BhZ2FtZW50bz48cGF5X2k6c2luZ29sb0ltcG9ydG9QYWdhdG8+My4xNzwvcGF5X2k6c2luZ29sb0ltcG9ydG9QYWdhdG
8+PHBheV9pOmVzaXRvU2luZ29sb1BhZ2FtZW50bz5QQUdBVEE8L3BheV9pOmVzaXRvU2luZ29sb1BhZ2FtZW50bz48cGF5X2k6ZGF0YUVzaXRvU2luZ29sb1BhZ2FtZW50bz4yMDE4LTEyLTE3PC9wYXlfaTpkYXRhRXNpdG9TaW5nb2xvUGFnYW1lbnRvPjxwYXlfaTppZGVu
dGlmaWNhdGl2b1VuaXZvY29SaXNjb3NzaW9uZT4xODM1MTAwMDAwNDk8L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb1Jpc2Nvc3Npb25lPjxwYXlfaTpjYXVzYWxlVmVyc2FtZW50bz4vUkZTL1JGMzMwMDMzMDAwMDAwMDAwMDMyMDAwMDAvMy4xNy9UWFQvUmljYXJpY2
EgZGkgMy4xNyAgLSBSRUZFWklPTkU8L3BheV9pOmNhdXNhbGVWZXJzYW1lbnRvPjxwYXlfaTpkYXRpU3BlY2lmaWNpUmlzY29zc2lvbmU+OS8wMDAxMTU0NTA0NTY3NzAwMDwvcGF5X2k6ZGF0aVNwZWNpZmljaVJpc2Nvc3Npb25lPjwvcGF5X2k6ZGF0aVNpbmdvbG9QYWdh
bWVudG8+PC9wYXlfaTpkYXRpUGFnYW1lbnRvPjwvcGF5X2k6UlQ+DQo=</pagati>
<tipoFirma/>
<rt>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxwYXlfaTpSVCB4bWxuczpwYXlfaT0iaHR0cDovL3d3dy5kaWdpdHBhLmdvdi5pdC9zY2hlbWFzLzIwMTEvUGFnYW1lbnRpLyIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTF
NjaGVtYS1pbnN0YW5jZSIgeHNpOnNjaGVtYUxvY2F0aW9uPSIvb3B0L3Byb2N0ZWxlL3Jlc291cmNlcy9QYWdJbmZfUlBUX1JUXzZfMl8wLnhzZCI+PHBheV9pOnZlcnNpb25lT2dnZXR0bz42LjIuMDwvcGF5X2k6dmVyc2lvbmVPZ2dldHRvPjxwYXlfaTpkb21pbmlvPjxw
YXlfaTppZGVudGlmaWNhdGl2b0RvbWluaW8+MDIzMzYzNDAxNTk8L3BheV9pOmlkZW50aWZpY2F0aXZvRG9taW5pbz48L3BheV9pOmRvbWluaW8+PHBheV9pOmlkZW50aWZpY2F0aXZvTWVzc2FnZ2lvUmljZXZ1dGE+SW1yZjEzMjVtYTczamUzYjdzNm84cGdlODV2MGcxMm
RnPC9wYXlfaTppZGVudGlmaWNhdGl2b01lc3NhZ2dpb1JpY2V2dXRhPjxwYXlfaTpkYXRhT3JhTWVzc2FnZ2lvUmljZXZ1dGE+MjAxOC0xMi0xN1QxMToyMzo1NTwvcGF5X2k6ZGF0YU9yYU1lc3NhZ2dpb1JpY2V2dXRhPjxwYXlfaTpyaWZlcmltZW50b01lc3NhZ2dpb1Jp
Y2hpZXN0YT4zNTc0YWI5ZGRmNTVhOGY0OWY2OGJhNWIwZDdmZDk3OTkzYTwvcGF5X2k6cmlmZXJpbWVudG9NZXNzYWdnaW9SaWNoaWVzdGE+PHBheV9pOnJpZmVyaW1lbnRvRGF0YVJpY2hpZXN0YT4yMDE4LTEyLTE3PC9wYXlfaTpyaWZlcmltZW50b0RhdGFSaWNoaWVzdG
E+PHBheV9pOmlzdGl0dXRvQXR0ZXN0YW50ZT48cGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvQXR0ZXN0YW50ZT48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5CPC9wYXlfaTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudGlm
aWNhdGl2b1VuaXZvY28+QkNJVElUTU08L3BheV9pOmNvZGljZUlkZW50aWZpY2F0aXZvVW5pdm9jbz48L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0F0dGVzdGFudGU+PHBheV9pOmRlbm9taW5hemlvbmVBdHRlc3RhbnRlPkludGVzYSBTYW5wYW9sbzwvcGF5X2k6ZG
Vub21pbmF6aW9uZUF0dGVzdGFudGU+PC9wYXlfaTppc3RpdHV0b0F0dGVzdGFudGU+PHBheV9pOmVudGVCZW5lZmljaWFyaW8+PHBheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0JlbmVmaWNpYXJpbz48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5HPC9wYXlf
aTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudGlmaWNhdGl2b1VuaXZvY28+MDIzMzYzNDAxNTk8L3BheV9pOmNvZGljZUlkZW50aWZpY2F0aXZvVW5pdm9jbz48L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb0JlbmVmaWNpYXJpbz48cG
F5X2k6ZGVub21pbmF6aW9uZUJlbmVmaWNpYXJpbz5Db211bmUgZGkgQmlhc3Nvbm88L3BheV9pOmRlbm9taW5hemlvbmVCZW5lZmljaWFyaW8+PHBheV9pOmluZGlyaXp6b0JlbmVmaWNpYXJpbz5WaWEgc2FuIE1hcnRpbm88L3BheV9pOmluZGlyaXp6b0JlbmVmaWNpYXJp
bz48cGF5X2k6Y2l2aWNvQmVuZWZpY2lhcmlvPjk8L3BheV9pOmNpdmljb0JlbmVmaWNpYXJpbz48cGF5X2k6Y2FwQmVuZWZpY2lhcmlvPjIwODUzPC9wYXlfaTpjYXBCZW5lZmljaWFyaW8+PHBheV9pOmxvY2FsaXRhQmVuZWZpY2lhcmlvPkJpYXNzb25vPC9wYXlfaTpsb2
NhbGl0YUJlbmVmaWNpYXJpbz48cGF5X2k6cHJvdmluY2lhQmVuZWZpY2lhcmlvPk1JPC9wYXlfaTpwcm92aW5jaWFCZW5lZmljaWFyaW8+PHBheV9pOm5hemlvbmVCZW5lZmljaWFyaW8+SVQ8L3BheV9pOm5hemlvbmVCZW5lZmljaWFyaW8+PC9wYXlfaTplbnRlQmVuZWZp
Y2lhcmlvPjxwYXlfaTpzb2dnZXR0b1BhZ2F0b3JlPjxwYXlfaTppZGVudGlmaWNhdGl2b1VuaXZvY29QYWdhdG9yZT48cGF5X2k6dGlwb0lkZW50aWZpY2F0aXZvVW5pdm9jbz5GPC9wYXlfaTp0aXBvSWRlbnRpZmljYXRpdm9Vbml2b2NvPjxwYXlfaTpjb2RpY2VJZGVudG
lmaWNhdGl2b1VuaXZvY28+TERFR0xMNjlQNjdCOTg4RzwvcGF5X2k6Y29kaWNlSWRlbnRpZmljYXRpdm9Vbml2b2NvPjwvcGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvUGFnYXRvcmU+PHBheV9pOmFuYWdyYWZpY2FQYWdhdG9yZT5BTlRXSSBNQVNTSU1PPC9wYXlfaTph
bmFncmFmaWNhUGFnYXRvcmU+PHBheV9pOmluZGlyaXp6b1BhZ2F0b3JlPlZJQSBJVEFMSUEgMTU8L3BheV9pOmluZGlyaXp6b1BhZ2F0b3JlPjxwYXlfaTpjYXBQYWdhdG9yZT4yMDEwMDwvcGF5X2k6Y2FwUGFnYXRvcmU+PHBheV9pOmxvY2FsaXRhUGFnYXRvcmU+TUlMQU
5PPC9wYXlfaTpsb2NhbGl0YVBhZ2F0b3JlPjxwYXlfaTpwcm92aW5jaWFQYWdhdG9yZT5NSTwvcGF5X2k6cHJvdmluY2lhUGFnYXRvcmU+PHBheV9pOm5hemlvbmVQYWdhdG9yZT5JVDwvcGF5X2k6bmF6aW9uZVBhZ2F0b3JlPjxwYXlfaTplLW1haWxQYWdhdG9yZT5pLnRy
ZXNvbGRpQGFjbWVpdGFsaWEuaXQ8L3BheV9pOmUtbWFpbFBhZ2F0b3JlPjwvcGF5X2k6c29nZ2V0dG9QYWdhdG9yZT48cGF5X2k6ZGF0aVBhZ2FtZW50bz48cGF5X2k6Y29kaWNlRXNpdG9QYWdhbWVudG8+MDwvcGF5X2k6Y29kaWNlRXNpdG9QYWdhbWVudG8+PHBheV9pOm
ltcG9ydG9Ub3RhbGVQYWdhdG8+My4xNzwvcGF5X2k6aW1wb3J0b1RvdGFsZVBhZ2F0bz48cGF5X2k6aWRlbnRpZmljYXRpdm9Vbml2b2NvVmVyc2FtZW50bz5SRjMzMDAzMzAwMDAwMDAwMDAzMjAwMDAwPC9wYXlfaTppZGVudGlmaWNhdGl2b1VuaXZvY29WZXJzYW1lbnRv
PjxwYXlfaTpDb2RpY2VDb250ZXN0b1BhZ2FtZW50bz5uL2E8L3BheV9pOkNvZGljZUNvbnRlc3RvUGFnYW1lbnRvPjxwYXlfaTpkYXRpU2luZ29sb1BhZ2FtZW50bz48cGF5X2k6c2luZ29sb0ltcG9ydG9QYWdhdG8+My4xNzwvcGF5X2k6c2luZ29sb0ltcG9ydG9QYWdhdG
8+PHBheV9pOmVzaXRvU2luZ29sb1BhZ2FtZW50bz5QQUdBVEE8L3BheV9pOmVzaXRvU2luZ29sb1BhZ2FtZW50bz48cGF5X2k6ZGF0YUVzaXRvU2luZ29sb1BhZ2FtZW50bz4yMDE4LTEyLTE3PC9wYXlfaTpkYXRhRXNpdG9TaW5nb2xvUGFnYW1lbnRvPjxwYXlfaTppZGVu
dGlmaWNhdGl2b1VuaXZvY29SaXNjb3NzaW9uZT4xODM1MTAwMDAwNDk8L3BheV9pOmlkZW50aWZpY2F0aXZvVW5pdm9jb1Jpc2Nvc3Npb25lPjxwYXlfaTpjYXVzYWxlVmVyc2FtZW50bz4vUkZTL1JGMzMwMDMzMDAwMDAwMDAwMDMyMDAwMDAvMy4xNy9UWFQvUmljYXJpY2
EgZGkgMy4xNyAgLSBSRUZFWklPTkU8L3BheV9pOmNhdXNhbGVWZXJzYW1lbnRvPjxwYXlfaTpkYXRpU3BlY2lmaWNpUmlzY29zc2lvbmU+OS8wMDAxMTU0NTA0NTY3NzAwMDwvcGF5X2k6ZGF0aVNwZWNpZmljaVJpc2Nvc3Npb25lPjwvcGF5X2k6ZGF0aVNpbmdvbG9QYWdh
bWVudG8+PC9wYXlfaTpkYXRpUGFnYW1lbnRvPjwvcGF5X2k6UlQ+DQo=</rt>
</ns4:paaSILChiediPagatiConRicevutaRisposta>
</soap:Body>
</soap:Envelope>
XML;

        // $xml = simplexml_load_string($response);
        //
        // $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        // var_dump(json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0])));

		$response_data= array(
            'response' => $response,
            'http_status' => 200,
            'curl_errno' => 0,
            'response_body' => $response
        );


		error_log("RESPONSE DATA:".print_r($response_data,true));
		return $response_data;
    }

    /**
     * @param $responseBody
     */
    private function processXMLResponse($responseBody)
    {
		$responseBody = trim($responseBody);
		$responseBody =  preg_replace("/[\n\r]/","",$responseBody);
        $responseBody = str_replace("<soap:Body>","",$responseBody);
        $responseBody = str_replace("</soap:Body>","",$responseBody);
        $responseBody = preg_replace('/(<\s*)\w+:/','$1',$responseBody);   // removes <xxx:
        $responseBody = preg_replace('/(<\/\s*)\w+:/','$1',$responseBody); // removes </xxx:
        $responseBody = preg_replace('/\s+xmlns:[^>]+/','',$responseBody); // removes xmlns:...

		$xml = (simplexml_load_string(strtr($responseBody, array(' xmlns:'=>' '))));
        error_log("RESPONSE-BODY: ".$responseBody);
        $xml_response = json_decode(json_encode($xml->xpath('//paaSILInviaDovutiRisposta')[0]));
        error_log("RESPONSE: ". print_r($xml_response,1));
        $esito = $xml_response->esito;
        $form_id = $this->form_data['form_id'];
        $sub_id = (isset($this->form_data['actions']['save']))? $this->form_data['actions']['save']['sub_id'] : null;
        global $wpdb;
        //$payment_outcome_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_outcome' AND `parent_id` = {$form_id}" );
        //update_post_meta( $sub_id, '_field_' . $payment_outcome_id ,  $xml_response->esito);
        if(isset($esito) && $esito ==='KO'){
            return "Si è verificato un errore durante la richiesta di pagamento: " . $xml_response->fault->faultCode . "(". $xml_response->fault->faultString . ")";
        }
        $redirect_field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_redirect_url' AND `parent_id` = {$form_id}" );
        update_post_meta( $sub_id, '_field_' . $redirect_field_id ,  preg_replace('/\s+/', '',$xml_response->url));
        $payment_session_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_session_id' AND `parent_id` = {$form_id}" );
        update_post_meta( $sub_id, '_field_' . $payment_session_id ,  $xml_response->idSession);
    }

    private function processStatusXMLResponse($responseBody)
    {
		$responseBody = trim($responseBody);
		$responseBody =  preg_replace("/[\n\r]/","",$responseBody);
        $responseBody = str_replace("<soap:Body>","",$responseBody);
        $responseBody = str_replace("</soap:Body>","",$responseBody);
        $responseBody = preg_replace('/(<\s*)\w+:/','$1',$responseBody);   // removes <xxx:
        $responseBody = preg_replace('/(<\/\s*)\w+:/','$1',$responseBody); // removes </xxx:
        $responseBody = preg_replace('/\s+xmlns:[^>]+/','',$responseBody); // removes xmlns:...

		$xml = (simplexml_load_string(strtr($responseBody, array(' xmlns:'=>' '))));
        error_log("STATUS RESPONSE-BODY: ".$responseBody);
        $xml_response = json_decode(json_encode($xml->xpath('//paaSILChiediPagatiConRicevutaRisposta')[0]));
        error_log("STATUS XML RESPONSE: ". print_r($xml_response,1));
        return $xml_response;
    }

    protected function getPdfTemplateMetadata()
    {
        return json_decode($this->pdf_template_dat->metadata, true);
    }

    protected function getMapDataFromFormField($field, $fallbackLabel)
    {
        $fieldId = (int) ($this->getPdfTemplateMetadata()[$field] ?? '');

        if (!$fieldId || !isset($this->form_data['fields'][$fieldId]['value'])) {
            return $this->getFieldValueFromLabel($fallbackLabel);
        }

        return $this->form_data['fields'][$fieldId]['value'];
    }

    protected function getSearchValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_searchvalue'] ?? '';
        if (!$val) {
            return 'DIPPFSF';
        }
        return $val;
    }

    protected function getTopicPath()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_path'] ?? '';
        if (!$val) {
            return '002/001/002';
        }
        return $val;
    }

    protected function getSoggettoValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_soggetto'] ?? '';
        if (!$val) {
            return 'Richiesta di assegnazioni del contributo sul canone d’affitto anno 2020 Mittente';
        }
        return $val;
    }
}

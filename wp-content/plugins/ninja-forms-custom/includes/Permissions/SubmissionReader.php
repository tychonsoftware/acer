<?php
if (!defined('ABSPATH')) { exit; }

class NF_Custom_Permissions_SubmissionReader
{
    public function __construct() {

        $this->init();
    }

    private function init() {
        $filters = [
            'ninja_forms_submenu_edit.php?post_type=nf_sub_capability',
            'ninja_forms_submenu_nf-excel-export_capability',
            'ninja_forms_submenu_ninja-forms-uploads_capability',
            'ninja_forms_menu_ninja-forms_capability',
            'ninja_forms_admin_excel_export_capabilities',
        ];
        foreach ($filters as $filter) {
            add_filter($filter, [$this, 'getCustomCapabilityOfSubmissionReader'], 10, 1);
        }

        add_filter('ninja_forms_uploads_tabs', [$this, 'limitTabFileUpload'], 10, 1);
        // add_action('network_admin_menu', [$this, 'limitTabFileUploadAccess'], 100);
        // add_action('user_admin_menu', [$this, 'limitTabFileUploadAccess'], 100);
        add_action('admin_menu', [$this, 'limitTabFileUploadAccess'], 100);
        add_action('admin_menu', [$this, 'limitSubmissionAction'], 100);
        add_action('admin_menu', [$this, 'removeDashboardMenuItem'], 100);
        add_action('admin_page_access_denied', [$this, 'fixNinjaFormTopMenuLink']);

        add_filter('admin_body_class', [$this, 'adminBodyClass']);
        add_filter('register_post_type_args', [$this, 'fixMetaCapabilitiesForNfSub'], 10, 2);
        add_filter('user_has_cap', [$this, 'userHasCapForSubmissionAction'], 10, 4);
    }

    public function getCustomCapabilityOfSubmissionReader($capability) {
        if ( $this->isSubmissionReaderOnly() ) {
            $capability = 'submission_reader';
        }
        return $capability;
    }

    public function limitTabFileUpload($tabs) {
        if ( $this->isSubmissionReaderOnly() ) {
            unset($tabs['settings']);
            unset($tabs['external']);
            return $tabs;
        }
        return $tabs;
    }

    public function fixNinjaFormTopMenuLink() {
        if ( !$this->isSubmissionReaderOnly() ) {
            return;
        }
        global $plugin_page;
        $parent = get_admin_page_parent();

        if ( isset( $plugin_page ) && 'ninja-forms' === $plugin_page && 'ninja-forms' === $parent) {
            wp_redirect(admin_url('edit.php?post_type=nf_sub'));
            exit();
        }
    }

    public function limitTabFileUploadAccess() {
        if ( $this->isSubmissionReaderOnly() ) {
            global $plugin_page;
            $parent = get_admin_page_parent();
            $hookname = get_plugin_page_hookname( $plugin_page, $parent );

            if ('ninja-forms_page_ninja-forms-uploads' !== $hookname) {
                return;
            }
            // prevent tab
            if (isset($_GET['tab'])) {
                wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
            }
            // prevent action
            if (isset($_GET['action'])) {
                wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
            }
            if ($this->isPOSTRequest()) {
                if (isset($_POST['action']) && $_POST['action'] != -1) {
                    wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
                }
            }
        }
    }

    public function removeDashboardMenuItem() {
        if ( $this->isSubmissionReaderOnly() ) {
            global $menu, $pagenow;
            unset($menu[2]); // remove Menu Dashboard

            global $plugin_page;

            $parent = get_admin_page_parent();
            // redirect if access dashboard
            if ( null === $plugin_page && 'index.php' === $parent && 'index.php' === $pagenow && empty($_GET) && empty($_REQUEST)) {
                wp_redirect(admin_url('edit.php?post_type=nf_sub'));
                exit();
            }
        }
    }

    public function limitSubmissionAction() {
        if ( !$this->isSubmissionReaderOnly() ) {
            return;
        }
        global $plugin_page;
        $parent = get_admin_page_parent();
        $hookname = get_plugin_page_hookname( $plugin_page, $parent );

        if ('ninja-forms_page_ninja-forms-uploads' !== $hookname) {
            return;
        }
        // prevent tab
        if (isset($_GET['tab'])) {
            wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
        }
        // prevent action
        if (isset($_GET['action'])) {
            wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
        }
        // prevent POST request
        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        if ('POST' === strtoupper($method)) {
            if (isset($_POST['action']) && $_POST['action'] != -1) {
                wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
            }
        }
    }

    public function userHasCapForSubmissionAction($allcaps, $caps, $args, $that) {
        if ($this->isPOSTRequest()) {
            return $allcaps;
        }

        $processCaps = [
            'nf_sub_edit_subs',
            // 'nf_sub_edit_sub',
        ];

        $cap = $caps[0];

        if (!in_array($cap, $processCaps)
            || empty($allcaps['submission_reader'])
            // || !empty($allcaps['nf_sub_edit_sub'])
            || !empty($allcaps['nf_sub_edit_subs'])
            || !empty($allcaps['manage_options'])
            || !empty($allcaps['nf_sub'])
        ) {
            return $allcaps;
        }

        global $typenow;

        if ('nf_sub' !== $typenow) {
            return $allcaps;
        }


        $allcaps += [
            // 'nf_sub_edit_sub' => true,
            'nf_sub_edit_subs' => true,
        ];

        return $allcaps;
    }

    private function isSubmissionReaderOnly() {
        if ( current_user_can( 'manage_options' ) ) {
            return false;
        }
        if ( current_user_can( 'submission_reader' ) ) {
            return true;
        }
        return false;
    }

    public function adminBodyClass($classes) {
        if ($this->isSubmissionReaderOnly()) {
            $classes .= ' submission_reader_only ';
        }
        return $classes;
    }

    public function fixMetaCapabilitiesForNfSub($args, $name) {
        if ('nf_sub' !== $name) {
            return $args;
        }
        $args['capabilities'] = array_merge($args['capabilities'], array(
        //     'publish_posts' => 'nf_sub_publish_subs',
            'edit_posts' => 'nf_sub_edit_subs',
            'edit_others_posts' => 'nf_sub_edit_others_subs',
        //     'delete_posts' => 'nf_sub_delete_subs',
        //     'delete_others_posts' => 'nf_sub_delete_others_subs',
        //     'read_private_posts' => 'nf_sub_read_private_subs',
            'edit_post' => 'nf_sub_edit_sub',
        //     'delete_post' => 'nf_sub_delete_sub',
        //     'read_post' => 'nf_sub_read_sub',
        //     'moderate_comments' => 'nf_sub_moderate_comments_sub',
        //     'manage_categories' => 'nf_sub_manage_categories_sub',
        //     'edit_published_posts' => 'nf_sub_edit_published_posts_sub',
        //     'edit_private_posts' => 'nf_sub_edit_private_sub',
        //     'delete_published_posts' => 'nf_sub_delete_published_sub',
        //     'delete_private_posts' => 'nf_sub_delete_private_sub',
        ));

        return $args;
    }

    private function isPOSTRequest() {
        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        if ('POST' === strtoupper($method)) {
            return true;
        }
        return false;
    }

}
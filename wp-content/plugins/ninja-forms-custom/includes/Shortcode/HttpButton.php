<?php

class NF_Custom_Shortcode_HttpButton
{

    /**
     * @var string
     */
    private $button_label;
    /**
     * @var int
     */
    private $workflow_id;
    /**
     * @var string
     */
    private $base_url;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'workflow_id' => 0,
            'button_label' => 'Button',
            'base_url' => ''
        ), $atts, '' );

        $this->workflow_id = $atts['workflow_id'];
        $this->button_label = $atts['button_label'];
        $this->base_url = $atts['base_url'];

    }

    public function toHtml()
    {
        $logged_user_id = -1;
        if ( is_user_logged_in() ) {
            $logged_user_id = wp_get_current_user()->ID;
        }
        $auth_url = wp_login_url();

        if(defined('NINJA_CUSTOM_AUTH_URL')){
            $auth_url = NINJA_CUSTOM_AUTH_URL;
        }

        $html = '<input type="hidden" id="logged_user_id" name="logged_user_id" value="' . $logged_user_id . '">';
        $html .= '<input type="hidden" id="auth_url" name="auth_url" value="' . $auth_url . '">';
        $html .= <<<HTML
<a class="nf-external_app-link" 
onmouseover="this.style.textDecoration='underline';" 
    onmouseout="this.style.textDecoration='none';"
style="background: #10a870;color: white;padding: 5px 10px;font-weight:
bold;display: inline-block;" 
href="{$this->base_url}?workflow_id={$this->workflow_id}">{$this->button_label}</a>
HTML;
        return $html;
    }
}

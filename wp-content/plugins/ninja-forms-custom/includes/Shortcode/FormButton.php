<?php

class NF_Custom_Shortcode_FormButton
{
    /**
     * @var int
     */
    private $form_id;
    /**
     * @var string
     */
    private $button_label;
    /**
     * @var array
     */
    private $post_states;
    /**
     * @var string
     */
    private $link;
    /**
     * @var int
     */
    private $second_form_id;
    /**
     * @var string
     */
    private $post_state_second_form;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'form_id' => 0,
            'button_label' => 'Button',
            'post_states' => '',
            'second_form_id' => 0,
            'post_state_second_form' => '',
            'link' => '#',
        ), $atts, 'form_button' );

        $this->form_id = $atts['form_id'];
        $this->button_label = $atts['button_label'];
        $this->post_states = explode(';', $atts['post_states']);
        $this->second_form_id = $atts['second_form_id'];
        $this->post_state_second_form = $atts['post_state_second_form'];
        $this->link = $atts['link'];
    }

    public function toHtml()
    {
        if (!$this->isAllow()) {
            return '';
        }
        return <<<HTML
<a class="nf-customer-form-link" href="{$this->link}">{$this->button_label}</a>
HTML;
    }

    private function isAllow(): bool
    {
        if (!is_user_logged_in()) {
            return false;
        }

        if ($this->second_form_id > 0) {
            $secondFormCheck = $this->checkFromSecondForm();
            
            if (!$secondFormCheck) {
                return false;
            }
        }
        
        return $this->checkFromFirstForm();
    }

    /**
     * @return bool
     */
    private function checkFromFirstForm()
    {
        $subs = $this->getSubs($this->form_id);

        if (!count($subs)) {
            return true;
        }
        
        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if (!in_array($state, $this->post_states)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    private function checkFromSecondForm()
    {
        $subs = $this->getSubs($this->second_form_id);

        if (!count($subs)) {
            return false;
        }

        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if ($state == $this->post_state_second_form) {
                return true;
            }
        }
        return false;
    }

    private function getSubs($formId)
    {
        global $current_user;

        $form = Ninja_Forms()->form($formId);

        $form->get_subs([
            'author'         => $current_user->ID
        ]);

        $args = array(
            'post_type'      => 'nf_sub',
            'author'         => $current_user->ID,
            'meta_query'    => [
                [
                    'key' => '_form_id',
                    'value' => $formId,
                ]
            ]
        );

        $wp_query = new WP_Query($args);
        $subs = $wp_query->get_posts();

        return $subs;
    }
}

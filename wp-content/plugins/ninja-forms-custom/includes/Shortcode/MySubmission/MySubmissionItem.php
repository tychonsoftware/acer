<?php

class NF_Custom_Shortcode_MySubmission_MySubmissionItem
{
    /**
     * @var WP_Post
     */
    private $post;

    /**
     * @var NF_Database_Models_Submission
     */
    private $sub;

    public function __construct(WP_Post $post,$form_ids)
    {
        $this->post = $post;
        $this->form_ids=$form_ids;
        $this->sub = Ninja_Forms()->form()->get_sub($post->ID);
    }

    public function getTipoRichiesta()
    {
        return $this->sub->get_form_title();
    }

    public function getCognome()
    {
        return $this->getMapDataFromFormField('comune_terni_cognome');
    }

    public function getNome()
    {
        return $this->getMapDataFromFormField('comune_terni_nome');
    }

    public function getEmail()
    {
        return $this->getMapDataFromFormField('comune_terni_email');
    }

    public function getNumeroProtocollo()
    {
        $fieldId = $this->get_field_id_by_key('protocolnumber');
        if (!$fieldId) {
            return '';
        }
        return get_post_meta($this->sub->get_id(), '_field_' . $fieldId, true);
    }
    public function getDataProtocollo()
    {
        $fieldId = $this->get_field_id_by_key('protocoldate');
        if (!$fieldId) {
            return '';
        }
        return get_post_meta($this->sub->get_id(), '_field_' . $fieldId, true);
    }

    public function getCodiceFiscale()
    {
        return $this->getMapDataFromFormField('comune_terni_codice_fiscale');
    }

    public function getStato()
    {
        $status = get_post_meta($this->sub->get_id(), 'state', true);
        if (!strlen($status)) {
            $status = 'Inviata';
        }
        // return $status . '<input type="hidden" name="state__post-id-' . $this->sub->get_id() . '" value="' . $status . '"></input>';
        return <<<HTML
<span class="nf-custom-table-status-{$this->sub->get_id()}">$status</span>
HTML;

    }

    public function getAllegatoFinale()
    {
        $file = get_post_meta($this->sub->get_id(), 'nf_documento_finale', true);
        if(!empty($file)){
             $file_name = basename($file);
             $base_url = wp_upload_dir();
            
             $finalUrl = $base_url['baseurl'].'/nf_submission_attachments/'. $file_name;
            return <<<HTML
<span><a href="{$finalUrl}" target="_blank" download>$file_name</a></span>
HTML;
        }else{
            return <<<HTML
<span></span>
HTML;
        }
    }


    public function getNote()
    {
        $note = get_post_meta($this->sub->get_id(), 'note', true);
        if (!$note || !strlen($note)) {
            $note = '';
        }
        return $note . '<input type="hidden" name="note__post-id-' . $this->sub->get_id() . '" value="' . $note . '"></input>';
    }

    public function getAllegati()
    {
        $fields = Ninja_Forms()->form($this->sub->get_form_id() )->get_fields();
       
        $html = [];
        $i = 1;
        $j=0;
        $downloadlink= array();
        $linkD=array();
        $processedIds= array();
        foreach ($fields as $field) {
            /** @var NF_Database_Models_Field $field */
            if (!$field || $field->get_setting('type') !== 'file_upload') {
                continue;
            }
            $links = $this->sub->get_field_value($field->get_id());
            array_push($processedIds, $field->get_id());
            if (empty($links)) {
                continue;
            }
            foreach ($links as $link) {
                $name = basename($link);
                $linkD[] = $link;
                $i++;
                $j++;
            }
           
        }

         // add manual added uploads
         global $wpdb;
         $getFields
             = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}nf3_fields WHERE parent_id =" . $this->sub->get_form_id());

           
         foreach ($getFields as $field) {
             if (!$field || in_array($field->id, $processedIds) ||
                 $field->type !== 'file_upload'){
                 continue;
             }
             $links = $this->sub->get_field_value($field->id);
             if (empty($links)) {
                 continue;
             }
             if(array_key_exists('file_url', $links)){
                 if( in_array( $links['file_url'] ,$linkD ) ){
                 }else {
                     $linkD[] = $links['file_url'];
                     $i++;
                     $j++;
                 }
              
             }
         }
       $downloadlink = implode(",",$linkD);
                $html = <<<HTML
<a href="javascript:void(0)"  onclick="nf_scarica_allegeti('$downloadlink')">Scarica</a>
HTML;
    

       if(!empty($linkD)){
        return '<span>&nbsp;&nbsp;</span>'.$html;
       }else{
        return '<span>&nbsp;&nbsp;</span>';   
       }
        
    }

    public function getRinuncia()
    {
        $state = get_post_meta($this->sub->get_id(), 'state', true);

        if (!strlen($state)) {
            $state = 'Inviata';
        }

        $allowStates = [
            'inviata',
            'ammessa alla fase 2',
            'ammessa alla fase 3',
        ];

        if (!in_array(strtolower($state), $allowStates)) {
            return '';
        }
        $formId = $this->sub->get_form_id();
        $form_ids = explode(',',$this->form_ids);
        if(in_array($formId, $form_ids)){

        return <<<HTML
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<div align="center"><button style="font-size:13px;" onclick="nf_custom_rinuncia({$this->sub->get_id()}, this)">Rinuncia</button></div>
HTML;
        }else{
            return '';
        }
    }

    public function getIntegrazioni()
    {
       $status = get_post_meta($this->sub->get_id(), 'state', true);
       if (!strlen($status)) {
        $status = 'Inviata';
       }
       global $wpdb;
       $table_name = $wpdb->prefix . 'nf_stato';
       $result = $wpdb->get_results("SELECT allegati from ".$table_name." WHERE descrizione='".$status."' AND allegati=1");
        if($result){
            return '
            <span><button class="btn btn-sm btn-primary aggiungi_allegati" data-toggle="modal" data-sid="' . $this->sub->get_id() . '" data-fid="' . $this->sub->get_form_id() . '"
            data-target="#allegatiModal" >Aggiungi Allegato</button><!-- Modal -->
           </span>';
        }
    }

    public function getEsitoPagamento()
    {
        
       $status = get_post_meta($this->sub->get_id(), 'state', true);
       if (!strlen($status)) {
        $status = 'Inviata';
       }
       global $wpdb;
       $payment_outcome_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'payment_outcome' AND `parent_id` = {$this->sub->get_form_id()}" );
       $esito = get_post_meta($this->sub->get_id(), '_field_' . $payment_outcome_id, true);
       if(isset($esito) && strtolower($esito) == 'ok'){
        return '
        <span><button class="btn btn-sm btn-primary payment_status" onclick="nf_payment_status(' . $this->sub->get_id() .',' . $this->sub->get_form_id() . ')" >Verifica Pagamento</button><!-- Modal -->
       </span>';
       }
    }

    private function getConfigurazioneTemplateForm()
    {
        if (isset($this->pdf_template_dat)) {
            return $this->pdf_template_dat;
        }
        $formId = $this->sub->get_form_id();
        $pdfTemplate = new PDFTemplate;
        $this->pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($formId);

        return $this->pdf_template_dat;
    }

    private function getConfigurazioneTemplateFormMetadata()
    {
        $templateForm = $this->getConfigurazioneTemplateForm();

        if (!isset($templateForm->metadata)) {
            throw new Exception('missing metadata');
        }

        return json_decode($templateForm->metadata, true);
    }

    protected function getMapDataFromFormField($field)
    {
        try {
            $fieldId
                = (int)($this->getConfigurazioneTemplateFormMetadata()[$field]
                ?? '');
        } catch (Exception $e) {
            return '';
        }

        if (!$fieldId) {
            return '';
        }

        return $this->sub->get_field_value($fieldId);
    }

    protected function get_field_id_by_key( $field_key )
    {
        global $wpdb;

        $field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `key` = '{$field_key}' AND `parent_id` = {$this->sub->get_form_id()}" );

        return $field_id;
    }

    public function getFormId()
    {
        return $this->sub->get_form_id();
    }
}
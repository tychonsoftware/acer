<?php
require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
require_once(ABSPATH . 'wp-admin/includes/screen.php');
require_once(ABSPATH . 'wp-admin/includes/class-wp-screen.php');
require_once(ABSPATH . 'wp-admin/includes/template.php');
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class NF_Custom_Shortcode_MySubmission_MySubmissionList extends WP_List_Table
{
    const DEFAULT_PER_PAGE = 10;
    const DEFAULT_TIPO_RICHIESTA= true;
    const DEFAULT_COGNOME= true;
    const DEFAULT_NOME= true;
    const DEFAULT_CODICE_FISCALE= true;
    const DEFAULT_EMAIL= true;
    const DEFAULT_NUMERO_PROTOCOLLO= true;
    const DEFAULT_DATA_PROTOCOLLO= true;
    const DEFAULT_STATO= true;
    const DEFAULT_NOTE= true;
    const DEFAULT_RINUNCIA= true;
    const DEFAULT_ALLEGATI= true;
    const DEFAULT_ALLEGATO_FINALE= true;
    const DEFAULT_INTEGRAZIONI = true;
    const DEFAULT_NUMERO_PROTOCOLLO_LABEL= 'Numero Protocollo';
    const DEFAULT_DATA_PROTOCOLLO_LABEL= 'Data Protocollo';
    const DEFAULT_FORM_IDS = 0;
    const DEFAULT_ESITO_PAGAMENTO = true;
    const DEFAULT_FILTER_FORM_IDS = 0;
    /**
     * @var int
     */
    private $per_page;
    private $tipo_richiesta;
    private $cognome;
    private $nome;
    private $codice_fiscale;
    private $email;
    private $numero_protocollo;
    private $data_protocollo;
    private $stato;
    private $note;
    private $rinuncia;
    private $allegati;
    private $allegato_finale;
    private $integrazioni;
    private $numero_protocollo_lable;
    private $data_protocollo_lable;
    private $esito_pagamento;
    private $form_ids;
    private $filter_forms_ids;

    public function __construct(
        $args
        = array(
            'per_page' => self::DEFAULT_PER_PAGE,
            'tipo_richiesta' => self::DEFAULT_TIPO_RICHIESTA,
            'cognome' => self::DEFAULT_COGNOME,
            'nome' => self::DEFAULT_NOME,
            'codice_fiscale'=> self::DEFAULT_CODICE_FISCALE,
            'email' => self::DEFAULT_EMAIL,
            'numero_protocollo' => self::DEFAULT_NUMERO_PROTOCOLLO,
            'data_protocollo' => self::DEFAULT_DATA_PROTOCOLLO,
            'stato' => self::DEFAULT_STATO,
            'note' => self::DEFAULT_NOTE,
            'rinuncia' => self::DEFAULT_RINUNCIA,
            'allegati' =>  self::DEFAULT_ALLEGATI,
            'allegato_finale'=>self::DEFAULT_ALLEGATO_FINALE,
            'integrazioni'=>self::DEFAULT_INTEGRAZIONI,
            'numero_protocollo_lable'=>self::DEFAULT_NUMERO_PROTOCOLLO_LABEL,
            'data_protocollo_lable'=>self::DEFAULT_DATA_PROTOCOLLO_LABEL,
            'form_ids'=>self::DEFAULT_FORM_IDS,
            'esito_pagamento'=>self::DEFAULT_ESITO_PAGAMENTO,
            'filter_forms_ids'=>self::DEFAULT_FILTER_FORM_IDS,
        )
    ) {
        $this->per_page = $args['per_page'];
        $this->tipo_richiesta = $args['tipo_richiesta'];
        $this->cognome = $args['cognome'];
        $this->nome = $args['nome'];
        $this->codice_fiscale = $args['codice_fiscale'];
        $this->email = $args['email'];
        $this->numero_protocollo = $args['numero_protocollo'];
        $this->data_protocollo = $args['data_protocollo'];
        $this->stato = $args['stato'];
        $this->note = $args['note'];
        $this->rinuncia = $args['rinuncia'];
        $this->allegati = $args['allegati'];
        $this->allegato_finale = $args['allegato_finale'];
        $this->integrazioni = $args['integrazioni'];
        $this->numero_protocollo_lable = $args['numero_protocollo_lable'];
        $this->data_protocollo_lable = $args['data_protocollo_lable'];
        $this->form_ids = $args['form_ids'];
        $this->esito_pagamento = $args['esito_pagamento'];
        $this->filter_forms_ids = $args['filter_forms_ids'];

        parent::__construct([
            'singular' => __('Submission', 'ninja-forms-custom'),
            'plural'   => __('Submissions', 'ninja-forms-custom'),
            'ajax'     => false,
        ]);
    }

    public function prepare_items()
    {
        global $current_user;

        $this->_column_headers = $this->get_column_info();
        $current_page = $this->get_pagenum();
        $filter_ids = array();
        $args = array(
            'post_type'      => 'nf_sub',
            'posts_per_page' => $this->per_page,
            'paged'          => $current_page,
            'author'         => $current_user->ID,
            'orderby'        => 'post_date',
            'order'          => 'DESC',
        );
        if($this->filter_forms_ids){
            $filter_ids = explode(',',$this->filter_forms_ids);
            $meta_query = array( 'relation' => 'OR' );
            foreach( $filter_ids as $filter_id ) {
                $meta_query[] = array(
					'key'     => '_form_id',
					'value'   => $filter_id
				);
            }
            $args['meta_query'] = $meta_query;
        }
        error_log('Arguements for my sub :  ' . print_r($args,1));
        $wp_query = new WP_Query($args);
        $subs = $wp_query->get_posts();

        $this->set_pagination_args([
            'total_items' => $wp_query->found_posts,
            'per_page'    => $this->per_page
        ]);

        $return = array();

        
        foreach ($subs as $sub) {
            $item = new NF_Custom_Shortcode_MySubmission_MySubmissionItem($sub,$this->form_ids);
            
            if(count($filter_ids) > 0 && in_array($item->getFormId(), $filter_ids)){
            }
             $return[] = $item;
        }

        $this->items = $return;
    }


    /**
     *  Associative array of columns
     *
     * @return array
     */
    function get_columns()
    {
        $columns = [];

        if($this->esito_pagamento=='true'){
            $columns14 = [
                'esito_pagamento'          => __('Esita Pagamento', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns14,$columns);
            
        }

        if($this->integrazioni=='true'){
            $columns13 = [
                'integrazioni'          => __('Integrazioni', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns13,$columns);
            
        }
        
        if($this->allegato_finale=='true'){
            $columns12 = [
                'allegato_finale'          => __('Documento Finale', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns12,$columns);
            
        }

        if($this->allegati=='true'){
            $columns11 = [
                'allegati'          => __('Allegati', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns11,$columns);
            
        }
       
        if($this->rinuncia=='true'){
            $columns10 = [
                'rinuncia'             => __('Rinuncia', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns10,$columns);
            
        }
        if($this->note=='true'){
            $columns9 = [
                'note'              => __('Note', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns9,$columns);
        }
        if($this->stato=='true'){
            $columns8 = [
                'stato'             => __('Stato', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns8,$columns);
        }
        if($this->data_protocollo=='true'){
            $columns7 = [
                'data_protocollo'   => __($this->data_protocollo_lable, 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns7,$columns);
        }
        if($this->numero_protocollo=='true'){
            $columns6 = [
                'numero_protocollo' => __($this->numero_protocollo_lable,
                    'ninja-forms-custom')
            ];
            $columns = array_merge( $columns6,$columns);
        }
        if($this->email=='true'){
            $columns5 = [
                'email'             => __('Email', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns5,$columns);
        }
        if($this->codice_fiscale=='true'){
            $columns4 = [
                'codice_fiscale'    => __('Codice Fiscale', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns4,$columns);
        }
        if($this->nome=='true'){
            $columns3 = [
                'nome'              => __('Nome', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns3,$columns);
        }
        if($this->cognome=='true'){
            $columns2 = [
                'cognome'           => __('Cognome', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns2,$columns);
         }
        
         if($this->tipo_richiesta=='true'){
            $columns1 = [
                'tipo_richiesta'    => __('Tipo Richiesta', 'ninja-forms-custom')
            ];
            $columns = array_merge( $columns1,$columns);
        }

        return $columns;
    }

    public function column_default($item, $column_name)
    {
        $method = 'get' . str_replace(' ', '',
                ucwords(str_replace('_', ' ', $column_name)));
        if (method_exists($item, $method)) {
            return $item->$method();
        }
        return 'No method';
    }

    public function get_pagenum()
    {
        $pagenum = isset( $_REQUEST['fpaged'] ) ? absint( $_REQUEST['fpaged'] ) : 0;

        if ( isset( $this->_pagination_args['total_pages'] ) && $pagenum > $this->_pagination_args['total_pages'] ) {
            $pagenum = $this->_pagination_args['total_pages'];
        }

        return max( 1, $pagenum );
    }

    /**
     * Display the pagination.
     *
     * @since 3.1.0
     *
     * @param string $which
     */
    protected function pagination( $which ) {
        if ( empty( $this->_pagination_args ) ) {
            return;
        }

        $total_items     = $this->_pagination_args['total_items'];
        $total_pages     = $this->_pagination_args['total_pages'];
        $infinite_scroll = false;
        if ( isset( $this->_pagination_args['infinite_scroll'] ) ) {
            $infinite_scroll = $this->_pagination_args['infinite_scroll'];
        }

        if ( 'top' === $which && $total_pages > 1 ) {
            $this->screen->render_screen_reader_content( 'heading_pagination' );
        }

        $output = '<span class="displaying-num">' . sprintf( _n( '%s item', '%s items', $total_items ), number_format_i18n( $total_items ) ) . '</span>';

        $current              = $this->get_pagenum();
        $removable_query_args = wp_removable_query_args();

        $current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

        $current_url = remove_query_arg( $removable_query_args, $current_url );

        $page_links = array();

        $total_pages_before = '<span class="paging-input">';
        $total_pages_after  = '</span></span>';

        $disable_first = $disable_last = $disable_prev = $disable_next = false;

        if ( $current == 1 ) {
            $disable_first = true;
            $disable_prev  = true;
        }
        if ( $current == 2 ) {
            $disable_first = true;
        }
        if ( $current == $total_pages ) {
            $disable_last = true;
            $disable_next = true;
        }
        if ( $current == $total_pages - 1 ) {
            $disable_last = true;
        }

        if ( $disable_first ) {
            $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&laquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='first-page button' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url( remove_query_arg( 'fpaged', $current_url ) ),
                __( 'First page' ),
                '&laquo;'
            );
        }

        if ( $disable_prev ) {
            $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&lsaquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='prev-page button' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url( add_query_arg( 'fpaged', max( 1, $current - 1 ), $current_url ) ),
                __( 'Previous page' ),
                '&lsaquo;'
            );
        }

        if ( 'bottom' === $which ) {
            $html_current_page  = $current;
            $total_pages_before = '<span class="screen-reader-text">' . __( 'Current Page' ) . '</span><span id="table-paging" class="paging-input"><span class="tablenav-paging-text">';
        } else {
            $html_current_page = sprintf(
                "%s<input class='current-page' id='current-page-selector' type='text' name='paged' value='%s' size='%d' aria-describedby='table-paging' /><span class='tablenav-paging-text'>",
                '<label for="current-page-selector" class="screen-reader-text">' . __( 'Current Page' ) . '</label>',
                $current,
                strlen( $total_pages )
            );
        }
        $html_total_pages = sprintf( "<span class='total-pages'>%s</span>", number_format_i18n( $total_pages ) );
        $page_links[]     = $total_pages_before . sprintf( _x( '%1$s di %2$s', 'paging' ), $html_current_page, $html_total_pages ) . $total_pages_after;

        if ( $disable_next ) {
            $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&rsaquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='next-page button' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url( add_query_arg( 'fpaged', min( $total_pages, $current + 1 ), $current_url ) ),
                __( 'Next page' ),
                '&rsaquo;'
            );
        }

        if ( $disable_last ) {
            $page_links[] = '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">&raquo;</span>';
        } else {
            $page_links[] = sprintf(
                "<a class='last-page button' href='%s'><span class='screen-reader-text'>%s</span><span aria-hidden='true'>%s</span></a>",
                esc_url( add_query_arg( 'fpaged', $total_pages, $current_url ) ),
                __( 'Last page' ),
                '&raquo;'
            );
        }

        $pagination_links_class = 'pagination-links';
        if ( ! empty( $infinite_scroll ) ) {
            $pagination_links_class .= ' hide-if-js';
        }
        $output .= "\n<span class='$pagination_links_class'>" . join( "\n", $page_links ) . '</span>';

        if ( $total_pages ) {
            $page_class = $total_pages < 2 ? ' one-page' : '';
        } else {
            $page_class = ' no-pages';
        }
        $this->_pagination = "<div class='tablenav-pages{$page_class}'>$output</div>";

        echo $this->_pagination;
    }
}

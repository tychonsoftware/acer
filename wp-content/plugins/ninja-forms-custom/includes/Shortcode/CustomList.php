<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery('summary').click();
        jQuery('summary').on('click', function(e)  {
            e.preventDefault();
            jQuery('details #details-content-menu').hide();
            jQuery(this).next().toggle();
        });
	});
</script>
<?php

class NF_Custom_Shortcode_CustomList
{
    /**
     * @var string
     */
    private $items;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'items' => '{}'
        ), $atts, 'custom_list' );

        // 
        if (array_key_exists("items",$atts)) {
            $this->items = urldecode($atts['items']);
        }
    }

    public function toHtml()
    {
        error_log('Items ' . print_r($this->items,1));
        $items = json_decode($this->items, true);
        error_log('Items json ' . print_r($items,1));
        $index = 1;
        $logged_user_id = -1;
        if ( is_user_logged_in() ) {
            $logged_user_id = wp_get_current_user()->ID;
        }
        $auth_url = wp_login_url();

        if(defined('NINJA_CUSTOM_AUTH_URL')){
            $auth_url = NINJA_CUSTOM_AUTH_URL;
        }
        $html = '<input type="hidden" id="logged_user_id_custom" name="logged_user_id_custom" value="' . $logged_user_id . '">';
        $html .= '<input type="hidden" id="auth_url_custom" name="auth_url_custom" value="' . $auth_url . '">';

        $html .= '<div style="width: 100%;"><div style="width: 100%;margin-top: 20px;margin: 0 auto;">';
        foreach($items as $item) {
            error_log('Item ' . print_r($item,1));
            $html .= '<details id="details-' . $index . '">';
            $item =  str_replace("'", "\"", $item);
            $json_item = json_decode($item);
            error_log('Item JSON ' . print_r($json_item,1));
            $html .= '<summary style="width: 50%;" class="menu green-bg white-bold-text"
                onmouseover="this.style.textDecoration=\'underline\';" onmouseout="this.style.textDecoration=\'none\';">' . $json_item->label  . '</summary>';
            $html .= '<div id="details-content-menu" style="background-color: #F9F9F9;padding-left: 40px;padding-top: 20px;width: 45%;float: right;margin-top: -70px;display: none;">';
            if((isset($json_item->subitems) && count($json_item->subitems) > 0) || count($json_item->subitems) == 0){
                $html .= '<div style="display: flex;margin-bottom: 25px;"><div class="servizi-online-img"></div><span class="servizi-online-txt">Servizi online</span></div><ul class="custom-ul">';
            }
            foreach($json_item->subitems as $subitem) {
                error_log('json_item ' . print_r($subitem,1));
                if(isset($subitem->type) && $subitem->type === 'form_button'){
                    $post_states = explode(';', $subitem->post_states);
                    if ($this->isFormAllow($subitem->form_id, $post_states,$subitem->second_form_id, $subitem->post_state_second_form)) {
                        
                        $html .= '<li class=sub-menu white-bold-text white-bold-text-a">';
                        $html .= '<a class="a-link" style="text-decoration: none !important;" href="'. $subitem->link . '">' . $subitem->button_label . '</a>';
                        $html .= '</li>';     
                    }
                }else  if(isset($subitem->type) && $subitem->type === 'http_button'){
                    $html .= '<li class="sub-menu white-bold-text white-bold-text-a">';
                    $html .= '<a class="a-link nf-external_app-link" style="text-decoration: none !important;" href="'. $subitem->base_url . '?workflow_id=' . $subitem->workflow_id . '">' . $subitem->button_label . '</a>';
                    $html .= '</li>';  
                }else  if(isset($subitem->type) && $subitem->type === 'link'){
                    $html .= '<li class="sub-menu white-bold-text white-bold-text-a">';
                    $html .= '<a class="a-link" style="text-decoration: none !important;" href="'. $subitem->url . '">' . $subitem->button_label . '</a>';
                    $html .= '</li>';  
                }
            }
            if((isset($json_item->subitems) && count($json_item->subitems) > 0) || count($json_item->subitems) == 0){
                $html .= '</ul>';     
            }
            $html .= '</details>';
            $index = $index + 1;
        }
        $html .= '</div></div>';
        error_log('html ' . print_r($html,1));
        return <<<HTML
        $html 
HTML;
    }

    private function isFormAllow($form_id,$post_states, $second_form_id, $post_state_second_form): bool
    {
        if (!is_user_logged_in()) {
            return false;
        }

        if ($second_form_id > 0) {
            
            $secondFormCheck = $this->checkFromSecondForm($second_form_id, $post_state_second_form);
            
            if (!$secondFormCheck) {
                return false;
            }
        }
        return $this->checkFromFirstForm($form_id, $post_states);
    }

     /**
     * @return bool
     */
    private function checkFromFirstForm($form_id, $post_states)
    {
        $subs = $this->getSubs($form_id);

        if (!count($subs)) {
            return true;
        }

        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if (!in_array($state, $post_states)) {
                return false;
            }
        }
        return true;
    }

      /**
     * @return bool
     */
    private function checkFromSecondForm($second_form_id, $post_state_second_form)
    {
        $subs = $this->getSubs($second_form_id);

        if (!count($subs)) {
            return false;
        }

        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if ($state == $post_state_second_form) {
                return true;
            }
        }
        return false;
    }

    private function getSubs($formId)
    {
        global $current_user;

        $form = Ninja_Forms()->form($formId);

        $form->get_subs([
            'author'         => $current_user->ID
        ]);

        $args = array(
            'post_type'      => 'nf_sub',
            'author'         => $current_user->ID,
            'meta_query'    => [
                [
                    'key' => '_form_id',
                    'value' => $formId,
                ]
            ]
        );

        $wp_query = new WP_Query($args);
        $subs = $wp_query->get_posts();

        return $subs;
    }
}

<?php

class NF_Custom_Shortcode_MySubmission
{
    /**
     * @var int
     */
    private $per_page;

    private $tipo_richiesta;

    private $cognome;

    private $nome;

    private $codice_fiscale;

    private $email;

    private $numero_protocollo;

    private $data_protocollo;

    private $stato;

    private $note;

    private $rinuncia;

    private $allegati;

    private $allegato_finale;

    private $integrazioni;

    private $numero_protocollo_lable;

    private $data_protocollo_lable;

    private $form_ids;

    private $esito_pagamento;

    private $filter_forms_ids;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'per_page' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_PER_PAGE,
            'tipo_richiesta' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_TIPO_RICHIESTA,
            'cognome' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_COGNOME,
            'nome' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_NOME,
            'codice_fiscale' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_CODICE_FISCALE,
            'email' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_EMAIL,
            'numero_protocollo' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_NUMERO_PROTOCOLLO,
            'data_protocollo' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_DATA_PROTOCOLLO,
            'stato' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_STATO,
            'note' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_NOTE,
            'rinuncia' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_RINUNCIA,
            'allegati'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_ALLEGATI,
            'allegato_finale'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_ALLEGATO_FINALE,
            'integrazioni'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_INTEGRAZIONI,
            'numero_protocollo_lable'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_NUMERO_PROTOCOLLO_LABEL,
            'data_protocollo_lable'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_DATA_PROTOCOLLO_LABEL,
            'form_ids'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_FORM_IDS,
            'esito_pagamento'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_ESITO_PAGAMENTO,
            'filter_forms'  => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_FILTER_FORM_IDS,
        ), $atts, 'bartag' );

        $this->per_page = $atts['per_page'];
        $this->tipo_richiesta = $atts['tipo_richiesta'];
        $this->cognome = $atts['cognome'];
        $this->nome = $atts['nome'];
        $this->codice_fiscale = $atts['codice_fiscale'];
        $this->email = $atts['email'];
        $this->numero_protocollo = $atts['numero_protocollo'];
        $this->data_protocollo = $atts['data_protocollo'];
        $this->stato = $atts['stato'];
        $this->note = $atts['note'];
        $this->rinuncia = $atts['rinuncia'];
        $this->allegati = $atts['allegati'];
        $this->allegato_finale = $atts['allegato_finale'];
        $this->integrazioni = $atts['integrazioni'];
        $this->numero_protocollo_lable = $atts['numero_protocollo_lable'];
        $this->data_protocollo_lable = $atts['data_protocollo_lable'];
        $this->form_ids = $atts['form_ids'];
        $this->esito_pagamento = $atts['esito_pagamento'];
        $this->filter_forms_ids = $atts['filter_forms'];
    }

    public function toHtml()
    {
        $block = $this;
        ob_start();
        include ABSPATH . 'wp-content/plugins/ninja-forms-custom/includes/Shortcode/MySubmission/template.phtml';
        $html = ob_get_clean();
        return $html;
    }

    public function getList()
    {
        $list = (new NF_Custom_Shortcode_MySubmission_MySubmissionList([
            'per_page' => $this->per_page,
            'tipo_richiesta' => $this->tipo_richiesta,
            'cognome' => $this->cognome,
            'nome' => $this->nome,
            'codice_fiscale' => $this->codice_fiscale,
            'email' => $this->email,
            'numero_protocollo' => $this->numero_protocollo,
            'data_protocollo' => $this->data_protocollo,
            'stato' => $this->stato,
            'note' => $this->note,
            'rinuncia' => $this->rinuncia,
            'allegati'  =>$this->allegati,
            'allegato_finale'  =>$this->allegato_finale,
            'integrazioni'  =>$this->integrazioni,
            'numero_protocollo_lable'  =>$this->numero_protocollo_lable,
            'data_protocollo_lable'  =>$this->data_protocollo_lable,
            'form_ids'  =>$this->form_ids,
            'esito_pagamento'  =>$this->esito_pagamento,
            'filter_forms_ids'  => $this->filter_forms_ids,
        ]));
        $list->prepare_items();
        $list->display();
    }

    public function isLoggedIn()
    {
        return is_user_logged_in();
    }
}
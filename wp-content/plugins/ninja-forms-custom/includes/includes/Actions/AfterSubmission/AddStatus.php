<?php


class NF_Custom_Actions_AfterSubmission_AddStatus
{
    public static function process($form_data) {
        if (!isset($form_data['actions']['save']['sub_id'])) {
            return;
        }
        add_post_meta($form_data['actions']['save']['sub_id'], 'state', 'Inviata');
    }
}

<?php

class NF_Custom_Shortcode_FormButton
{
    /**
     * @var int
     */
    private $form_id;
    /**
     * @var string
     */
    private $button_label;
    /**
     * @var array
     */
    private $post_states;
    /**
     * @var string
     */
    private $link;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'form_id' => 0,
            'button_label' => 'Button',
            'post_states' => '',
            'link' => '#',
        ), $atts, 'form_button' );

        $this->form_id = $atts['form_id'];
        $this->button_label = $atts['button_label'];
        $this->post_states = explode(';', $atts['post_states']);
        $this->link = $atts['link'];
    }

    public function toHtml()
    {
        if (!$this->isAllow()) {
            return '';
        }
        return <<<HTML
<a class="nf-customer-form-link" href="{$this->link}">{$this->button_label}</a>
HTML;
    }

    private function isAllow(): bool
    {
        if (!is_user_logged_in()) {
            return false;
        }

        global $current_user;

        $form = Ninja_Forms()->form( $this->form_id );
        $form->get_subs([
            'author'         => $current_user->ID
        ]);

        $args = array(
            'post_type'      => 'nf_sub',
            'author'         => $current_user->ID,
            'meta_query'    => [
                [
                    'key' => '_form_id',
                    'value' => $this->form_id,
                ]
            ]
        );

        $wp_query = new WP_Query($args);
        $subs = $wp_query->get_posts();

        if (!count($subs)) {
            return true;
        }

        foreach ($subs as $sub) {
            $state = get_post_meta($sub->ID, 'state', true);
            if (!in_array($state, $this->post_states)) {
                return false;
            }
        }
        return true;
    }
}

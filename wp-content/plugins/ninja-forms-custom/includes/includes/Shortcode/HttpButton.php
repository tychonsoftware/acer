<?php

class NF_Custom_Shortcode_HttpButton
{

    /**
     * @var string
     */
    private $button_label;
    /**
     * @var int
     */
    private $workflow_id;
    /**
     * @var string
     */
    private $base_url;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'workflow_id' => 0,
            'button_label' => 'Button',
            'base_url' => ''
        ), $atts, '' );

        $this->workflow_id = $atts['workflow_id'];
        $this->button_label = $atts['button_label'];
        $this->base_url = $atts['base_url'];

    }

    public function toHtml()
    {
        return <<<HTML
<a class="nf-external_app-link" href="{$this->base_url}?workflow_id={$this->workflow_id}">{$this->button_label}</a>
HTML;
    }
}

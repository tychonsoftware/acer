<?php

class NF_Custom_Shortcode_MySubmission
{
    /**
     * @var int
     */
    private $per_page;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'per_page' => NF_Custom_Shortcode_MySubmission_MySubmissionList::DEFAULT_PER_PAGE,
        ), $atts, 'bartag' );

        $this->per_page = $atts['per_page'];
    }

    public function toHtml()
    {
        $block = $this;
        ob_start();
        include ABSPATH . 'wp-content/plugins/ninja-forms-custom/includes/Shortcode/MySubmission/template.phtml';
        $html = ob_get_clean();
        return $html;
    }

    public function getList()
    {
        $list = (new NF_Custom_Shortcode_MySubmission_MySubmissionList([
            'per_page' => $this->per_page
        ]));
        $list->prepare_items();
        $list->display();
    }

    public function isLoggedIn()
    {
        return is_user_logged_in();
    }
}

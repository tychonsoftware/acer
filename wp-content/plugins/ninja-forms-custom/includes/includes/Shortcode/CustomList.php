<?php

class NF_Custom_Shortcode_CustomList
{
    /**
     * @var string
     */
    private $items;

    public function __construct($atts)
    {
        $atts = shortcode_atts( array(
            'items' => '{}'
        ), $atts, 'custom_list' );

        error_log('................................. ' . print_r($atts,1));
        $this->items = $atts['items'];
    }

    public function toHtml()
    {
        return <<<HTML
<a class="nf-customer-form-link" href="{$this->items}">anshuman</a>
HTML;
    }

    private function isAllow(): bool
    {
        return true;
    }
}

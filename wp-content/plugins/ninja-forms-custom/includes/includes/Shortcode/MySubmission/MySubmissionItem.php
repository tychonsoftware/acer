<?php

class NF_Custom_Shortcode_MySubmission_MySubmissionItem
{
    /**
     * @var WP_Post
     */
    private $post;

    /**
     * @var NF_Database_Models_Submission
     */
    private $sub;

    public function __construct(WP_Post $post)
    {
        $this->post = $post;
        $this->sub = Ninja_Forms()->form()->get_sub($post->ID);
    }

    public function getTipoRichiesta()
    {
        return $this->sub->get_form_title();
    }

    public function getCognome()
    {
        return $this->getMapDataFromFormField('comune_terni_cognome');
    }

    public function getNome()
    {
        return $this->getMapDataFromFormField('comune_terni_nome');
    }

    public function getEmail()
    {
        return $this->getMapDataFromFormField('comune_terni_email');
    }

    public function getNumeroProtocollo()
    {
        $fieldId = $this->get_field_id_by_key('protocolnumber');
        if (!$fieldId) {
            return '';
        }
        return get_post_meta($this->sub->get_id(), '_field_' . $fieldId, true);
    }
    public function getDataProtocollo()
    {
        $fieldId = $this->get_field_id_by_key('protocoldate');
        if (!$fieldId) {
            return '';
        }
        return get_post_meta($this->sub->get_id(), '_field_' . $fieldId, true);
    }

    public function getCodiceFiscale()
    {
        return $this->getMapDataFromFormField('comune_terni_codice_fiscale');
    }

    public function getStato()
    {
        $status = get_post_meta($this->sub->get_id(), 'state', true);
        if (!strlen($status)) {
            $status = 'Inviata';
        }
        return $status . '<input type="hidden" name="state__post-id-' . $this->sub->get_id() . '" value="' . $status . '"></input>';
    }

    public function getNote()
    {
        $note = get_post_meta($this->sub->get_id(), 'note', true);
        if (!$note || !strlen($note)) {
            $note = '';
        }
        return $note . '<input type="hidden" name="note__post-id-' . $this->sub->get_id() . '" value="' . $note . '"></input>';
    }

    public function getAllegati()
    {
        $fields = Ninja_Forms()->form($this->sub->get_form_id() )->get_fields();

        $html = [];
        $i = 1;

        foreach ($fields as $field) {
            /** @var NF_Database_Models_Field $field */
            if (!$field || $field->get_setting('type') !== 'file_upload') {
                continue;
            }
            $links = $this->sub->get_field_value($field->get_id());
            if (empty($links)) {
                continue;
            }
            foreach ($links as $link) {
                $name = basename($link);
                $html[] = <<<HTML
<a href="$link" target="_blank">$name</a>
HTML;
                $i++;
            }
        }

        return implode('<span>&nbsp;&nbsp;</span>', $html);
    }

    private function getConfigurazioneTemplateForm()
    {
        if (isset($this->pdf_template_dat)) {
            return $this->pdf_template_dat;
        }
        $formId = $this->sub->get_form_id();
        $pdfTemplate = new PDFTemplate;
        $this->pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($formId);

        return $this->pdf_template_dat;
    }

    private function getConfigurazioneTemplateFormMetadata()
    {
        $templateForm = $this->getConfigurazioneTemplateForm();

        if (!isset($templateForm->metadata)) {
            throw new Exception('missing metadata');
        }

        return json_decode($templateForm->metadata, true);
    }

    protected function getMapDataFromFormField($field)
    {
        try {
            $fieldId
                = (int)($this->getConfigurazioneTemplateFormMetadata()[$field]
                ?? '');
        } catch (Exception $e) {
            return '';
        }

        if (!$fieldId) {
            return '';
        }

        return $this->sub->get_field_value($fieldId);
    }

    protected function get_field_id_by_key( $field_key )
    {
        global $wpdb;

        $field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `key` = '{$field_key}' AND `parent_id` = {$this->sub->get_form_id()}" );

        return $field_id;
    }
}

<?php

date_default_timezone_set('Europe/Rome');

class NF_Custom_Webservices_ComuneTerni
{
    private $form_data;
    private $action_settings;
    private $pdf_template_dat;
	private $attachments;

    /**
     * @var SoapClient
     */
    private $client;
    private $wsdl;
    private $soapUsername;
    private $soapPassword;

    public function __construct($form_data, $action_settings, $pdf_template_dat) {
        $this->initClient();
        $this->form_data = $form_data;
        $this->action_settings = $action_settings;
        $this->pdf_template_dat = $pdf_template_dat;

		$this->attachments = $action_settings["ATTACHMENTS"];

		error_log("ACTION SETTINGS: ".print_r($this->action_settings,true));

		error_log("ATTACHMENTS: ".print_r($this->attachments,true));
    }

    private function initClient()
    {
        global $wpdb;

        $table_name = "servmask_prefix_portal";
        $query = "SELECT * FROM $table_name WHERE id = 2";
        $soap_logins = $wpdb->get_results($query);
        $soap_login = $soap_logins[0];
        $this->soapUsername = $soap_login->soap_username;
        $this->soapPassword = $soap_login->soap_password;
        $this->wsdl = $soap_login->soap_url;
    }

    public function makeRequest()
    {
        try {
            $result = $this->send_request($this->wsdl, $this->xml());
            if (isset($result['http_status']) && $result['http_status'] > 300) {
                return [
                    'error' => 'Errore durante la protocollazione della domanda. Si prega di riprovare'
                ];
            }

            $this->processXMLResponse($result['response_body']);
            $sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
            $attachments = array();
            $attachments = apply_filters( 'ninja_forms_action_email_attachments', $attachments, $this->form_data, $this->action_settings );
            if(!empty($attachments)){
                $pdf_file_path = $attachments[0];
                $sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                $pdf_file_name = $this->get_pdf_file_name( $this->form_data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';
                $new_pdf_file = dirname($pdf_file_path) .'/' .$pdf_file_name;
                rename($pdf_file_path,$new_pdf_file);
                if (class_exists('UserFascicle')) {
                    $active_plugins_basenames = get_option( 'active_plugins' );
                    foreach ( $active_plugins_basenames as $plugin_basename ) {
                        if ( false !== strpos( $plugin_basename, '/UserFascicle.php' ) ) {
                            $plugin_dir = WP_PLUGIN_DIR . '/' . $plugin_basename;
                            require_once $plugin_dir;
                            $uf_plugin  = new UserFascicle();
                            $uf_plugin->store_pdf_fascicle_folder($new_pdf_file ,$this->form_data['settings']['title'], $sub_id, $this->form_data[ 'form_id' ]);
                            break;
                        }
                    }
                }else {
                    error_log("User Fascicle not installed");
                }
            }
            return [];
        } catch (\Exception $e) {
            if ( WP_DEBUG === true ) {
                error_log('No PDF template associated with ');
            }
            return [
                'error' => 'Error in sending document to external service. No PDF template associated with'
            ];
        }
    }

	private function getFieldValueFromLabel($fieldName){
		$returnValue = "";
		$formFields = $this->form_data['fields'];

		foreach($formFields as $field){
			$label = strtoupper($field['label']);
			$fieldName = strtoupper ($fieldName);
			if($label == $fieldName){
				$returnValue = $field['value'];
				break;
			}
		}
		return $returnValue;
	}

    protected function xml()
    {
        $nonce = sha1(mt_rand());
		$createDate = gmdate('Y-m-d\TH:i:s\Z');
        //$user = wp_get_current_user();
        //$codice_fiscale = get_user_meta($user->ID, 'codice_fiscale', true);

		$sub_id = ( isset( $this->form_data[ 'actions' ][ 'save' ] ) ) ? $this->form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;

		$cognome = $this->getMapDataFromFormField('comune_terni_cognome', "Cognome");
		$nome = $this->getMapDataFromFormField('comune_terni_nome', "Nome");
		$email = $this->getMapDataFromFormField('comune_terni_email', "Email per ricezione della ricevuta");
		$codiceFiscale =  $this->getMapDataFromFormField('comune_terni_codice_fiscale', "Codice Fiscale");
		$searchValue = $this->getSearchValue();
        $topicPath = $this->getTopicPath();
        $soggetto =$this->getSoggettoValue();
		$fullname = $cognome." ".$nome;

		error_log("DATI: ".$cognome." ".$nome." ".$email." ".$codiceFiscale);

        $pdf_file_name = $this->get_pdf_file_name( $this->form_data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';

		$attachmentsArea = "";

		$attachmentsArea .= "<attachments>\n";
		$fileSet = "isharedocMailAttach";
		foreach ($this->attachments as $value){
			if (strpos($value, 'ninja-forms-submission-')===false){
				$pdfName = pathinfo($value,PATHINFO_BASENAME);
				$fileContent = file_get_contents($value);
				$attachmentsArea .="<attachment>\n";
				$attachmentsArea .="	<fileset>$fileSet</fileset>\n";
				$attachmentsArea .="	<filename>$pdfName</filename>\n";
				$attachmentsArea .="	<contentType>application/pdf</contentType>\n";
				$attachmentsArea .="	<data>".base64_encode($fileContent)."</data>\n";
				$attachmentsArea .="</attachment>\n";
			}
		}
		$attachmentsArea .= "</attachments>";

        return <<<XML
<soapenv:Envelope
	xmlns:ins="http://www.isharedoc.it/schemas/instance"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Header>
		<wsse:Security soapenv:mustUnderstand="1"
			xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
			xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
			<wsse:UsernameToken>
				<wsse:Username>$this->soapUsername</wsse:Username>
				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">$this->soapPassword</wsse:Password>
				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">$nonce</wsse:Nonce>
				<wsu:Created>$createDate</wsu:Created>
			</wsse:UsernameToken>
		</wsse:Security>
	</soapenv:Header>
	<soapenv:Body>
		<ins:InstanceMessageCreateRequest>
			<partitionId>3</partitionId>
			<messageBoxId>2</messageBoxId>
			<storyboardCode>protocollo</storyboardCode>
			<direction>IN</direction>
			<subject>$soggetto: $email  $fullname - Cod.Fisc.: $codiceFiscale</subject>
			<contacts>
				<contact>
					<type>O</type>
					<referenceType>GBOX</referenceType>
					<searchType>CODE</searchType>
					<searchMode>NOTFOUNDDEF</searchMode>
					<description>$fullname</description>
					<email>$email</email>
					<searchValue>$searchValue</searchValue>
					</contact>
				<contact>
					<type>T</type>
					<referenceType>GBOX</referenceType>
					<searchType>CODE</searchType>
					<searchValue>$searchValue</searchValue>
				</contact>
				<contact>
					<type>F</type>
					<referenceType>AB</referenceType>
					<searchType>EMAIL</searchType>
					<searchValue>$email</searchValue>
					<searchMode>NOTFOUNDDEF</searchMode>
					<description>$fullname</description>
					<email>$email</email>
				</contact>
			</contacts>
			<topics>
				<topic>
					<path>$topicPath</path>
				</topic>
			</topics>
			<variables>
				<variable>
					<key>modTrasmissione</key>
					<type>string</type>
					<valueString>flusso digitale</valueString>
				</variable>
			</variables>
			$attachmentsArea
			<startWorkflow>true</startWorkflow>
			<instanceOperation>protocollo</instanceOperation>
		</ins:InstanceMessageCreateRequest>
	</soapenv:Body>
</soapenv:Envelope>
XML;

    }

    private function send_request($wsdl,$xml_post_string){
		//return $this->dumpResponse();
        $soap_do = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Content-Length: ".strlen($xml_post_string)
        );

		error_log("WSDL: ".$wsdl);
		error_log("POST: ".$xml_post_string);

        curl_setopt($soap_do, CURLOPT_URL, $wsdl);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($soap_do, CURLOPT_TIMEOUT,        120);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST,           true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($soap_do, CURLOPT_HEADER, 1);
        curl_setopt($soap_do, CURLOPT_FAILONERROR, FALSE);
        $response  = curl_exec($soap_do);
        $header_size = curl_getinfo($soap_do, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $http_status = curl_getinfo($soap_do, CURLINFO_HTTP_CODE);
        $curl_errno= curl_errno($soap_do);
        curl_close($soap_do);

        //$response = str_replace("<soap:Body>","",$response) ;
        //$response = str_replace("</soap:Body>","",$response);

        $response_data = array(
            'response' => $response,
            'http_status' => $http_status,
            'curl_errno' => $curl_errno,
            'response_body' => $body
        );

		error_log("RESPONSE DATA:".print_r($response_data,true));
        return $response_data;
    }

    /**
     * Add PDF download link on the view form submission page
     *
     * @param $name   name of the file
     * @param $sub_id submission id
     * @since 1.3.3
     */
    public function get_pdf_file_name( $name = 'ninja-forms-submission', $sub_id = '' ) {
        return apply_filters( 'ninja_forms_submission_pdf_name', $name, $sub_id );
    }

    private function dumpResponse()
    {
        $response = <<<XML
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Header/>
   <SOAP-ENV:Body>
      <ns3:InstanceMessageCreateResponse xmlns:ns3="http://www.isharedoc.it/schemas/instance">
         <id>12393</id>
         <identifier>0000003</identifier>
         <identifierDate>2020-06-10T10:11:17.135+02:00</identifierDate>
         <viewUrl>http://interpademo.comter.net/isharedoc/workgroupHome/view.action?instance=12393</viewUrl>
      </ns3:InstanceMessageCreateResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
XML;

        // $xml = simplexml_load_string($response);
        //
        // $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        // var_dump(json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0])));

		$response_data= array(
            'response' => $response,
            'http_status' => 200,
            'curl_errno' => 0,
            'response_body' => $response
        );


		error_log("RESPONSE DATA:".print_r($response_data,true));
		return $response_data;
    }

    /**
     * @param $responseBody
     */
    private function processXMLResponse($responseBody)
    {
		$responseBody = trim($responseBody);
		$responseBody =  preg_replace("/[\n\r]/","",$responseBody);

		$pos = strpos($responseBody, 'Content-Type');
		$part = substr($responseBody,0,$pos);
		$responseBody = str_replace($part,"", $responseBody);
		$responseBody = str_replace("--","", $responseBody);

		$pos = strpos($responseBody, '<SOAP-ENV');
		$contentType = substr($responseBody,0,$pos);
		$responseBody = str_replace($contentType,"", $responseBody);
        error_log("RESPONSE-BODY: ".$responseBody);
		$xml = simplexml_load_string($responseBody);
		error_log("XML-BODY: ".$xml);
        $xml->registerXPathNamespace('ns3', 'http://www.isharedoc.it/schemas/instance');
        $InstanceMessageCreateResponse = json_decode(json_encode($xml->xpath('//ns3:InstanceMessageCreateResponse')[0]));

        // $id = $InstanceMessageCreateResponse->id;
        $identifier = $InstanceMessageCreateResponse->identifier;
        $identifierDate = $InstanceMessageCreateResponse->identifierDate;

        global $wpdb;
        $form_id = $this->form_data['form_id'];
        $sub_id = (isset($this->form_data['actions']['save']))? $this->form_data['actions']['save']['sub_id'] : null;

        // $protocol_field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolid' AND `parent_id` = {$form_id}" );
        $protocol_field_number = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolnumber' AND `parent_id` = {$form_id}"
        );
        $protocol_field_date = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocoldate' AND `parent_id` = {$form_id}"
        );

		error_log("PROTOCOL FIELD NUMBER = ".$protocol_field_number);
		error_log("PROTOCOL FIELD DATE = ".$protocol_field_date);
		error_log("SUB_ID:".$sub_id);
		error_log("FORM_ID:".$form_id);

        // update_post_meta( $sub_id, '_field_' . $protocol_field_id , $id );
        update_post_meta($sub_id, '_field_' . $protocol_field_number, $identifier);
        $protocol_field_fmt = date('d-m-Y H:i:s', strtotime($identifierDate));
	    update_post_meta($sub_id, '_field_' . $protocol_field_date, $protocol_field_fmt);
    }

    protected function getPdfTemplateMetadata()
    {
        return json_decode($this->pdf_template_dat->metadata, true);
    }

    protected function getMapDataFromFormField($field, $fallbackLabel)
    {
        $fieldId = (int) ($this->getPdfTemplateMetadata()[$field] ?? '');

        if (!$fieldId || !isset($this->form_data['fields'][$fieldId]['value'])) {
            return $this->getFieldValueFromLabel($fallbackLabel);
        }

        return $this->form_data['fields'][$fieldId]['value'];
    }

    protected function getSearchValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_searchvalue'] ?? '';
        if (!$val) {
            return 'DIPPFSF';
        }
        return $val;
    }

    protected function getTopicPath()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_path'] ?? '';
        if (!$val) {
            return '002/001/002';
        }
        return $val;
    }

    protected function getSoggettoValue()
    {
        $val = $this->getPdfTemplateMetadata()['comune_terni_soggetto'] ?? '';
        if (!$val) {
            return 'Richiesta di assegnazioni del contributo sul canone d’affitto anno 2020 Mittente';
        }
        return $val;
    }
}

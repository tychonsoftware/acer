<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class NF_Field_CustomExample
 */
class NF_Custom_Fields_CustomExample extends NF_Fields_Textbox
{
    protected $_name = 'custom';

    protected $_section = 'common';

    protected $_type = 'textbox';

    protected $_templates = 'textbox';

    public function __construct()
    {
        parent::__construct();

        $this->_nicename = __( 'Custom Example Field', 'ninja-forms' );
    }
}
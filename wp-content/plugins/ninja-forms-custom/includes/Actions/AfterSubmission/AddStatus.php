<?php


class NF_Custom_Actions_AfterSubmission_AddStatus
{
    public static function process($form_data) {
        if (!isset($form_data['actions']['save']['sub_id'])) {
            return;
        }
        add_post_meta($form_data['actions']['save']['sub_id'], 'state', 'Inviata');
        $id = NF_Custom_Admin_Submission_StatoModel::getInstance()->getIdByDes('Inviata');
        add_post_meta($form_data['actions']['save']['sub_id'], 'state_id', $id, true);
    }
}

<?php if ( ! defined( 'ABSPATH' ) || ! class_exists( 'NF_Abstracts_Action' )) exit;

/**
 * Class NF_Action_CustomExample
 */
final class NF_Custom_Actions_InsertPDFUserFile extends NF_Abstracts_Action
{
    /**
     * @var string
     */
    protected $_name  = 'nf_custom_insert_pdf_user_file';

    /**
     * @var array
     */
    protected $_tags = array();

    /**
     * @var string
     */
    protected $_timing = 'late';

    /**
     * @var int
     */
    protected $_priority = '0';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_nicename = __( 'Insert Pdf User File', 'ninja-forms' );
    }

    /*
    * PUBLIC METHODS
    */

    // public function save( $action_settings )
    // {
    // }

    public function process( $action_settings, $form_id, $data )
    {
        $sub_id = ( isset( $data[ 'actions' ][ 'save' ] ) ) ? $data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
        if (!$sub_id) {
            return $data;
        }
        global $wpdb;

        $submission = Ninja_Forms()->form()->sub( $sub_id )->get();
        $protocol_field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolid' AND `parent_id` = {$form_id}" );
        $protocol_field_number = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolnumber' AND `parent_id` = {$form_id}"
        );
        $protocol_field_date = $wpdb->get_var(
            "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocoldate' AND `parent_id` = {$form_id}"
        );

        $protocol_field_id && update_post_meta( $sub_id, '_field_' . $protocol_field_id , $submission->get_seq_num() );
        $protocol_field_number && update_post_meta($sub_id, '_field_' . $protocol_field_number, $submission->get_seq_num());
        $protocol_field_date && update_post_meta($sub_id, '_field_' . $protocol_field_date, $submission->get_sub_date('d/m/Y H:i:s'));

        $attachments = array();
        $action_settings['attach_pdf'] = 1;
        $attachments = apply_filters( 'ninja_forms_action_email_attachments', $attachments, $data, $action_settings );
        if(empty($attachments)) {
            return $data;
        }
        $pdf_file_path = $attachments[0];
        $pdf_file_name = $this->get_pdf_file_name( $data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';
        $new_pdf_file = dirname($pdf_file_path) .'/' . $pdf_file_name;
        rename($pdf_file_path,$new_pdf_file);
        if (class_exists('UserFascicle')) {
            $active_plugins_basenames = get_option( 'active_plugins' );
            foreach ( $active_plugins_basenames as $plugin_basename ) {
                if ( false !== strpos( $plugin_basename, '/UserFascicle.php' ) ) {
                    $plugin_dir = WP_PLUGIN_DIR . '/' . $plugin_basename;
                    require_once $plugin_dir;
                    $uf_plugin  = new UserFascicle();
                    $uf_plugin->store_pdf_fascicle_folder($new_pdf_file ,$data['settings']['title'], $sub_id, $data[ 'form_id' ]);
                    break;
                }
            }
        } else {
            error_log("User Fascicle not installed");
        }

        return $data;
    }

    public function get_pdf_file_name( $name = 'ninja-forms-submission', $sub_id = '' ) {
        $seq_num = get_post_meta( $sub_id, '_seq_num', TRUE );
        $name = "RichiestaOnLine_" . $seq_num . $sub_id . $this->random_strings(6);
        return apply_filters( 'ninja_forms_submission_pdf_name', $name, $sub_id );
    }

    private function random_strings($length_of_string) 
    { 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
        return substr(str_shuffle($str_result),  
                        0, $length_of_string); 
    } 
}

<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_MergeTags_SPID extends NF_Abstracts_MergeTags
{
    protected $id = 'mergetag_spid';

    public function __construct()
    {
        parent::__construct();

        $this->title = __( 'Merge Tag SPID', 'ninja-forms-custom' );

        $this->merge_tags = array(
            'user_spid' => array(
                'id' => 'user_spid',
                'tag' => '{user:spid}', // The tag to be  used.
                'label' => __( 'User SPID', 'ninja-forms-custom' ), // Translatable label for tag selection.
                'callback' => 'callback' // Class method for processing the tag. See below.
            ),
        );
    }

    /**
     * Example Tag Callback
     *
     * This is the call back method for our merge tag set in our config array.
     *
     * @return string
     */
    public function callback()
    {
        $current_user = wp_get_current_user();

        return ( $current_user ) ? get_user_meta($current_user->ID, 'codice_fiscale', true) : '';
    }
}

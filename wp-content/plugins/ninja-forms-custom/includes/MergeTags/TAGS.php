<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_MergeTags_TAGS extends NF_Abstracts_MergeTags
{
	const PHASE_2_ACCEPTED = "Ammessa alla fase 2"; // keep, use on \NF_Custom::getValueFromTag
	
    public function __construct()
    {
        parent::__construct();
		$this->title = __( "Merge Tag Phase 2&3", 'ninja-forms-custom' );
    }

    public function addMergeTag($tagName, $tagLabel, $phase)
    {
        if ($phase === 2) {
            $tag = new NF_Custom_MergeTags_Phase_Phase2($tagName, $tagLabel);
        } else {
            $tag = new NF_Custom_MergeTags_Phase_Phase3($tagName, $tagLabel);
        }
        $this->merge_tags += $tag->getTag();
    }
}

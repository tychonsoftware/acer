<?php

abstract class NF_Custom_MergeTags_Phase_PhaseAbstract
{
    const PHASE_2_ACCEPTED = "Ammessa alla fase 2"; // keep, use on \NF_Custom::getValueFromTag

    const ADDRESS = "Indirizzo";
    const NUM_CIVICO = "N. Civico";
    const CAP = "Codice di avviamento postale (CAP) ";

    protected $id = 'mergetag_tags';

    private $tagName;
    private $tagLabel;

    public function __construct($tagName, $tagLabel)
    {
        $this->tagName = $tagName;
        $this->tagLabel = $tagLabel;
    }

    /**
     * @return array
     */
    public function getTag()
    {
        $callBack = "callback";
        if($this->tagLabel == "Indirizzo"){
            $callBack = "callbackAddress";
        }

        return array(
            "user_tags_{$this->phase}_{$this->tagName}" => array(
                'id' => "user_tags_{$this->phase}_$this->tagLabel",
                'tag' => $this->tagName, // The tag to be  used.
                'label' => "Phase {$this->phase}: {$this->tagLabel}", // Translatable label for tag selection.
                'callback' => [$this, $callBack] // Class method for processing the tag. See below.
            ),
        );
    }


    protected function customQuery($data)
    {
        $result = '(';
        foreach ($data as $index => $field) {
            $result .= '\'_field_' . $field->id . '\'';
            if ($index == count($data) - 1) {
                continue;
            }
            $result .= ',';
        }
        $result .= ')';
        return $result;
    }

    protected function getMaxPostId($data)
    {
        //error_log("getMaxPostId");
        $max = -1;
        foreach ($data as $index => $field) {
            if($max<$field->ID){
                $max = $field->ID;
            }
        }
        return $max;
    }

    protected function getValueFromTag($tagLabel) {
        global $wpdb;

        $formId = $this->getPreviousFormId();
        $phaseAcceptedLabel = $this->getPhaseAcceptedLabel();

        $returnValue = "";

        $current_user = wp_get_current_user();

        if($current_user){

            $query = "SELECT * FROM {$wpdb->prefix}posts WHERE post_author=$current_user->ID AND ID IN (SELECT post_id FROM	{$wpdb->prefix}postmeta WHERE 
									meta_key='state' AND meta_value='$phaseAcceptedLabel')";

            $postArray = $wpdb->get_results($query);
            //error_log("POST_ARRAY:".print_r($postArray,1));

            if(!empty($postArray) && count($postArray)>0){
                $postId = $this->getMaxPostId($postArray);
                //error_log("POST ID: $postId");
                $query = "SELECT * FROM {$wpdb->prefix}nf3_fields WHERE label = '$tagLabel' AND parent_id = ".$formId;
                // error_log("QUERY {$this->phase}: $query");
                $getFieldsNumber = $wpdb->get_results($query);
                //error_log("getFieldsNumber: ".print_r($getFieldsNumber,1));

                if(!empty($getFieldsNumber) && count($getFieldsNumber)>0){
                    $customQuery = $this->customQuery($getFieldsNumber);

                    $query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key IN $customQuery and post_id = $postId";
                    //error_log("QUERY3: $query");
                    $resultValue = $wpdb->get_results($query);
                    //error_log("resultValue: ".print_r($resultValue));

                    if(!empty($resultValue) && count($resultValue)>0){
                        $returnValue = $resultValue[0]->meta_value;
                    }
                }
            }
        }
        //error_log("RETURN VALUE: $returnValue");
        return $returnValue;
    }

    public function callback()
    {
        return $this->getValueFromTag($this->tagLabel);
    }

    public function callbackAddress()
    {
        $indirizzoValue = "";
        $numCivicoValue = "";
        $capValue = "";

        $keyAddress = self::ADDRESS;
        $keyNumCivico = self::NUM_CIVICO;
        $keyCap = self::CAP;

        $indirizzoValue = $this->getValueFromTag($keyAddress);
        $numCivicoValue = $this->getValueFromTag($keyNumCivico);
        $capValue = $this->getValueFromTag($keyCap);
        $returnValue = $indirizzoValue.", ".$numCivicoValue." ".$capValue;

        return $returnValue;
    }

    /**
     * @return string
     */
    protected function getPhaseAcceptedLabel()
    {
        return "Ammessa alla fase {$this->phase}";
    }

    /**
     * @return string
     */
    protected function getPreviousFormId()
    {
        $previousPhase = $this->phase - 1;
        return constant("FORM_ID_PHASE_{$previousPhase}");
    }
}

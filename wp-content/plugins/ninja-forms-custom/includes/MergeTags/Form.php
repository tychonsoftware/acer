<?php if ( ! defined( 'ABSPATH' ) ) exit;

class NF_Custom_MergeTags_Form extends NF_Abstracts_MergeTags
{
    protected $id = 'nf_custom_form';

    protected $form_id;

    protected $sub_seq;

    private $sub_id;
    /**
     * @var NF_Database_Models_Submission
     */
    private $submission;

    public function __construct()
    {
        parent::__construct();

        $this->title = __( 'Form Custom', 'ninja-forms' );

        $this->merge_tags = array(
            'nf_seq_num' => array(
                'id' => 'nf_seq_num',
                'tag' => '{field:nf_seq_num}',
                'label' => __( 'Submission Sequence Number', 'ninja-forms-custom' ),
                'callback' => 'getSubSeq'
            ),
            'postdate' => array(
                'id' => 'postdate',
                'tag' => '{field:postdate}',
                'label' => __( 'Submission Post Date', 'ninja-forms-custom' ),
                'callback' => 'getPostDate'
            ),
            'sub_id' => array(
                'id' => 'sub_id',
                'tag' => '{submission:sub_id}',
                'label' => __( 'Submission ID', 'ninja-forms-custom' ),
                'callback' => 'getSubId'
            ),
        );

        add_action( 'ninja_forms_save_sub', array( $this, 'setSubId' ) );

        // Gets the form ID.
        add_action( 'nf_get_form_id', array( $this, 'set_form_id' ), 15, 1 );
    }

    /**
     * @return mixed
     */
    public function getSubSeq()
    {
        if (!isset($this->sub_seq) && isset($this->submission)) {
            $this->sub_seq = $this->submission->get_seq_num();
        }
        return $this->sub_seq;
    }

    /**
     * Setter method for the form_id and callback for the nf_get_form_id action.
     * @since 3.2.2
     *
     * @param string $form_id The ID of the current form.
     * @return void
     */
    public function set_form_id( $form_id )
    {
        $this->form_id = $form_id;
    }

    /**
     * @return mixed
     */
    public function getSubId()
    {
        return $this->sub_id;
    }

    /**
     * @param mixed $sub_id
     */
    public function setSubId($sub_id)
    {
        $this->submission = Ninja_Forms()->form()->sub( $sub_id )->get();
        $this->sub_id = $sub_id;
    }

    protected function getPostDate()
    {
        if (isset($this->submission)) {
            return $this->submission->get_sub_date('d/m/Y H:i:s');
        }
    }
}

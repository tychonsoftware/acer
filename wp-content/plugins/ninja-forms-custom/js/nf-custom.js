
jQuery(document).ready(function () {

	jQuery('#allegatiModal').hide();
    jQuery(".aggiungi_allegati").click(function () {
		jQuery('#int_sub_id').val(jQuery(this).data('sid'));
		jQuery('#int_form_id').val(jQuery(this).data('fid'));
		jQuery('#interror').hide();
    });

	jQuery(".nf-external_app-link").click(function (event) {
		event.preventDefault();
		var logged_user_id_custom = jQuery('input[name=logged_user_id_custom]').val();
		console.log('logged_user_id_custom: ' , logged_user_id_custom);
		var logged_user_id = jQuery('input[name=logged_user_id]').val();
		console.log('logged_user_id: ' , logged_user_id);
		if(logged_user_id_custom && logged_user_id_custom == -1){
			var auth_url_custom = jQuery('input[name=auth_url_custom]').val();
			console.log('auth_url_custom: ' , auth_url_custom);
			window.open(auth_url_custom, '_self');
		}else if(logged_user_id && logged_user_id == -1){
			var auth_url = jQuery('input[name=auth_url]').val();
			console.log('auth_url: ' , auth_url);
			window.open(auth_url, '_self');
		}else {
			var url =jQuery(this).attr('href');
			var data = {
				'action': 'nf_custom_external_app',
			};

			jQuery('body').addClass('loader-overlay');
			jQuery('body').css('position','relative');
			jQuery('.loader-gif').css('display','block');

			jQuery.ajax({
				type: 'POST',
				url: nf_custom_js.ajax_url,
				data: data,
			}).done(function (response) {
				if(response.logout_url){
					window.open(response.logout_url, '_self');
				}else {
					window.open(url + '&token=' + response.auth_token, '_blank');
				}
			}).fail(function () {

			}).always(function () {
				jQuery('body').removeClass('loader-overlay');
				jQuery('.loader-gif').css('display','none');
			});
		}
	});


});

function getByKey(arr, value, isText) {

	for (var i=0, iLen=arr.length; i<iLen; i++) {

		if (arr[i].key == value) {
			if(isText===true){
				var key = "nf-field-"+arr[i].id;
				return jQuery("#"+key).val();
			} else{
				var options = arr[i].options;
				if(options != undefined){
					for (var i=0, iLen=options.length; i<iLen; i++) {
						var key = "nf-field-"+options[i].fieldID + "-" + i;
						if(jQuery("#"+key)[0]!==undefined && jQuery("#"+key)[0].checked == true)
							return jQuery("#"+key).val();
					}
				}
				
			}
		}
	}
	return '';
}

function getSelectedByKey(arr, value) {

	for (var i=0, iLen=arr.length; i<iLen; i++) {

		if (arr[i].key == value) {
			var options = arr[i].options;
			if(options != undefined){
				for (var i=0, iLen=options.length; i<iLen; i++) {
					var key = "nf-field-"+options[i].fieldID;
					
					var value = jQuery('#' + key + ' option:selected').val();
					if(!isBlank(value ))
						return value;
				}
			}
		}
	}
	return '';
}


function getTableDataByKey(arr, value) {
	var data = Array();
	for (var i=0, iLen=arr.length; i<iLen; i++) {
		if (arr[i].key == value) {
			jQuery("#table_" + arr[i].id + " table tbody tr").each(function(i, v){
				data[i] = Array();
				var tableData = '';
				jQuery(this).children('td').each(function(ii, vv){
					//tableData = tableData + '``' + jQuery(this).text();
					data[i][ii] = jQuery(this).text();
					
				}); 
			}); 
		}
	}
	return data;
}

function getFilesByKey(arr, value) {
	var data = Array();
	for (var i=0, iLen=arr.length; i<iLen; i++) {
		if (arr[i].type == "file_upload") {
			var key = "nf-field-"+arr[i].id;
			var files_div = jQuery('#' + key + '-container div[class="files_uploaded"');
			if(files_div.length > 0){
				var ps = files_div.find("p");
				for (var p=0, pLen=ps.length; p<pLen; p++) {

					var wrapped = jQuery(ps[p].outerHTML);
					wrapped.find("a").remove();
					data.push( wrapped.text().replace(/(\r\n|\n|\r|\t)/gm, ""));
				}
			}
			
		}
	}
	return data;
}
function nf_print_pdf(){
	console.log('Inside nf print pdf');
	var fields = this.form.fields;
	
	var cognomeRichiedente = getByKey(fields,'cognome_richiedente_1591626411300',true);
	var nomeRichiedente = getByKey(fields,'nome_richiedente_1591626417938',true);
	var luogoDiNascita = getByKey(fields,'luogo_di_nascita_comune_stato_1646665259885',true);
	var dataDiNascita = getByKey(fields,'data_di_nascita_1594726068041',true);
	var residenteNelComuneDi = getByKey(fields,'comune_di_residenza_della_provincia_di_perugia_1646924152552',true);
	var residenteNelComuneDiTerni = getByKey(fields,'comune_di_residenza_della_provincia_di_terni_1646924282047',true);
	var ProvinciaResidenza = getByKey(fields,'provincia_di_residenza_1646666343250',true);
	var allaViaPiazza = getByKey(fields,'alla_via_piazza_1608128323231',true);
       var sessoRichiedente = getByKey(fields,'sesso_1646666584563',true);
       var ProvinciaNascita = getByKey(fields,'provincia_di_nascita_1646926017658',true);
	var nCivico = getByKey(fields,'n_civico_1595516820746',true);
	var zip = getByKey(fields,'zip_1595599856330',true);
	var codiceiscale = getByKey(fields,'codice_fiscale_1591616243364',true);
	var sezione1Data = getSelectedByKey(fields,'sezione_1_-_lett_a_-_di_essere_alla_data_di_pubblicazione_del_bando_1633339434083');
	var sezione1Period = getSelectedByKey(fields,'sezione_1_-_lett_b_-_periodo_residenza_1633339505388');
	var sezione2Reddito = getSelectedByKey(fields,'sezione_2_-_lett_a_-_reddito_1633339646667');
	var sezione2Euro = getByKey(fields,'sezione_2_-_lett_b_-_che_l_attestazione_isee_2022_e_di_eururo_1666012198254', true);
	var tipologia = getByKey(fields,'tipologia_1633612616690',false);
	var sezione2dellaDomanda = getSelectedByKey(fields,'sezione_2_-_lett_c_-_presentazione_della_domanda_1633340162116');
	var sezione3Data = getTableDataByKey(fields,'sezione_3_-_stato_di_famiglia_anagrafico_1633430430911');
	var sezione4DirittoDiProprieta = getByKey(fields,'sezione_4_-_diritto_di_proprieta_comproprieta_usufrutto_1633352912672',false);
	var sezione5CanoneAffitto = getByKey(fields,'sezione_5_-_dichiara_che_per_il_canone_d_affitto_2021_1666012438811',false);
       var sezione5ImportoStraordinario = getByKey(fields,'importo_contributo_straordinario_1666012816161',true);

	var primoContrattoAffittoStipulatoCon = getByKey(fields,'primo_contratto_affitto_stipulato_con_1637848976443',true);
	var primoContrattoRegistratoInData = getByKey(fields,'primo_contratto_registrato_in_data_1633502920450',true);
	var primoContrattoAlN = getByKey(fields,'primo_contratto_al_n_1633502905541',true);
	var primoContrattoUfficioDelRegistroDi = getByKey(fields,'primo_contratto_presso_l_ufficio_del_registro_di_1633502926195',true);
	var primoContrattoAffittoDellAlloggioSitoInComuneDi = getByKey(fields,'primo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756433470',true);
	var primoContrattoIndirizzo = getByKey(fields,'primo_contratto_indirizzo_1633502937026',true);
	var primoContrattoDiDimensionePariAMQ = getByKey(fields,'primo_contratto_di_dimensione_pari_a_mq_1646757473917',true);
	var primoContrattoF = getByKey(fields,'primo_contratto_foglio_1646671456110',true);
	var primoContrattoP = getByKey(fields,'primo_contratto_particella_1646670308706',true);
	var primoContrattoSub = getByKey(fields,'primo_contratto_sub_1633502962590',true);
	var primoContrattoCat = getByKey(fields,'primo_contratto_cat_1646671913544',true);
	var primoContrattoProprieta = getByKey(fields,'primo_contratto_proprieta_1633502972107',false);
	var primoContrattoPerCui_eStato = getByKey(fields,'primo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724360342',true);
	var primoContrattoPeriodoDiMensilita = getByKey(fields,'primo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646667150890',true);
	var primoContrattoRegolaPagamenti = getByKey(fields,'indicare_se_in_regola_con_i_pagamenti_1636724684210',true);

	var secondoContrattoAffittoStipulatoCon = getByKey(fields,'secondo_contratto_affitto_stipulato_con_1633504536849',true);
	var secondoContrattoRegistratoInData = getByKey(fields,'secondo_contratto_registrato_in_data_1633504545461',true);
	var secondoContrattoAlN = getByKey(fields,'secondo_contratto_al_n_1633504742493',true);
	var secondoContrattoUfficioDelRegistroDi = getByKey(fields,'secondo_contratto_presso_l_ufficio_del_registro_1633504561493',true);
	var secondoContrattoAffittoDellAlloggioSitoInComuneDi = getByKey(fields,'secondo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756729863',true);
	var secondoContrattoIndirizzo = getByKey(fields,'secondo_contratto_indirizzo_1633504610787',true);
	var secondoContrattoDiDimensionePariAMQ = getByKey(fields,'secondo_contratto_di_dimensione_pari_a_mq_1646906758907',true);
	var secondoContrattoF = getByKey(fields,'secondo_contratto_foglio_1646722960493',true);
	var secondoContrattoP  = getByKey(fields,'secondo_contratto_particella_1646670448761',true);
	var secondoContrattoSub = getByKey(fields,'secondo_contratto_sub_1633506282705',true);
	var secondoContrattoCat = getByKey(fields,'secondo_contratto_cat_1646723558366',true);
	var secondoContrattoProprieta = getByKey(fields,'secondo_contratto_proprieta_1633506678222',false);
	var secondoContrattoPerCui_eStato = getByKey(fields,'secondo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724604037',true);
	var secondoContrattoPeriodoDiMensilita = getByKey(fields,'secondo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646668673320',true);

	var terzoContrattoAffittoStipulatoCon = getByKey(fields,'terzo_contratto_affitto_stipulato_con_1633508157510',true);
	var terzoContrattoRegistratoInData = getByKey(fields,'terzo_contratto_registrato_in_data_1633508166161',true);
	var terzoContrattoAlN = getByKey(fields,'terzo_contratto_al_n_1633508174162',true);
	var terzoContrattoUfficioDelRegistroDi = getByKey(fields,'terzo_contratto_presso_l_ufficio_del_registro_1633508184002',true);
	var terzoContrattoAffittoDellAlloggioSitoInComuneDi = getByKey(fields,'terzo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756957473',true);
	var terzoContrattoIndirizzo = getByKey(fields,'terzo_contratto_indirizzo_1633508307145',true);
	var terzoContrattoDiDimensionePariAMQ = getByKey(fields,'terzo_contratto_di_dimensione_pari_a_mq_1646906953242',true);
	var terzoContrattoF = getByKey(fields,'terzo_contratto_foglio_1646723343203',true);
	var terzoContrattoP  = getByKey(fields,'terzo_contratto_particella_1646670743807',true);
	var terzoContrattoSub = getByKey(fields,'terzo_contratto_sub_1633508440982',true);
	var terzoContrattoCat = getByKey(fields,'terzo_contratto_cat_1646723964406',true);
	var terzoContrattoProprieta = getByKey(fields,'terzo_contratto_proprieta_1633508503142',false);
	var terzoContrattoPerCui_eStato = getByKey(fields,'terzo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724934482',true);
	var terzoContrattoPeriodoDiMensilita = getByKey(fields,'terzo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646668631558',true);

	var inCasoDiAssegnazioneDelContributoEsso = getByKey(fields,'in_caso_di_assegnazione_del_contributo_esso_dovra_essere_accreditato_sul_cc_n_1633431304011',true);
	var intestatoA = getByKey(fields,'intestato_a_1633431335082',true);
	var CodiceIban = getByKey(fields,'codice_iban_1633431349182',true);
	var co = getByKey(fields,'c_o_1633431322334',true);
	var signature = getByKey(fields,'sig_1633431592191',true);
	var indirizzoRecapito = getByKey(fields,'indirizzo_recapito_1633431614004',true);
	var indirizzoNumero = getByKey(fields,'indirizzo_numero_1633509782911',true);
	var localita = getByKey(fields,'localita_1633431647379',true);
	var cap = getByKey(fields,'c_a_p_1633431660817',true);
	var provinicia = getByKey(fields,'provincia_sigla_1646923531753',true);
	var telefonoAbitazione = getByKey(fields,'telefono_abitazione_1633431712402',true);
	var cellulare = getByKey(fields,'cellulare_1633431728138',true);
	var email = getByKey(fields,'email_recapito_1633431743817',true);
	var pec = getByKey(fields,'pec_posta_elettronica_certificata_1636531106663',true);
	var numeroMarca = getByKey(fields,'numero_della_marca_da_bollo_di_16_euro_1646926320163',true);
	var dataMarca = getByKey(fields,'data_marca_da_bollo_1633612039871',true);
	
	var files = getFilesByKey(fields,'allegato_scansione_della_marca_da_bollo_annullata_1633612116242');

	Swal.fire({
		text: "Download in corso",
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Si',
		onBeforeOpen: () => {
		  Swal.showLoading();
		},
		allowOutsideClick: () => !Swal.isLoading()
  	});

	  jQuery.ajax({
		method: "POST",
		url: nf_custom_js.ajax_url,
		data: {
			'action': 'print_PDF_templateAjax',
			'nomeRichiedente': nomeRichiedente,
			'cognomeRichiedente': cognomeRichiedente,
			'luogoDiNascita': luogoDiNascita,
			'dataDiNascita':dataDiNascita,
			'residenteNelComuneDi':residenteNelComuneDi,
                     'sessoRichiedente':sessoRichiedente,
                     'ProvinciaNascita':ProvinciaNascita,
                     'residenteNelComuneDiTerni':residenteNelComuneDiTerni,
                     'ProvinciaResidenza': ProvinciaResidenza,
			'allaViaPiazza':allaViaPiazza,
			'nCivico':nCivico,
			'zip':zip,
			'codiceiscale':codiceiscale,
			'sezione1Data':sezione1Data,
			'sezione1Period':sezione1Period,
			'sezione2Reddito':sezione2Reddito,
			'sezione2Euro':sezione2Euro,
			'tipologia': tipologia,
			'sezione2dellaDomanda': sezione2dellaDomanda,
			'sezione3Data':sezione3Data,
			'sezione4DirittoDiProprieta': sezione4DirittoDiProprieta,
			'sezione5CanoneAffitto': sezione5CanoneAffitto,
                     'sezione5ImportoStraordinario': sezione5ImportoStraordinario,

			'primoContrattoAffittoStipulatoCon':primoContrattoAffittoStipulatoCon,
			'primoContrattoRegistratoInData':primoContrattoRegistratoInData,
			'primoContrattoAlN':primoContrattoAlN,
			'primoContrattoUfficioDelRegistroDi':primoContrattoUfficioDelRegistroDi,
			'primoContrattoAffittoDellAlloggioSitoInComuneDi':primoContrattoAffittoDellAlloggioSitoInComuneDi,
			'primoContrattoIndirizzo':primoContrattoIndirizzo,
			'primoContrattoDiDimensionePariAMQ':primoContrattoDiDimensionePariAMQ,
			'primoContrattoF':primoContrattoF,
			'primoContrattoP': primoContrattoP,
			'primoContrattoSub': primoContrattoSub,
			'primoContrattoCat':primoContrattoCat,
			'primoContrattoProprieta': primoContrattoProprieta,
			'primoContrattoPerCui_eStato': primoContrattoPerCui_eStato,
			"primoContrattoPeriodoDiMensilita": primoContrattoPeriodoDiMensilita,
			"primoContrattoRegolaPagamenti": primoContrattoRegolaPagamenti,

			'secondoContrattoAffittoStipulatoCon':secondoContrattoAffittoStipulatoCon,
			'secondoContrattoRegistratoInData':secondoContrattoRegistratoInData,
			'secondoContrattoAlN':secondoContrattoAlN,
			'secondoContrattoUfficioDelRegistroDi':secondoContrattoUfficioDelRegistroDi,
			'secondoContrattoAffittoDellAlloggioSitoInComuneDi':secondoContrattoAffittoDellAlloggioSitoInComuneDi,
			'secondoContrattoIndirizzo':secondoContrattoIndirizzo,
			'secondoContrattoDiDimensionePariAMQ':secondoContrattoDiDimensionePariAMQ,
			'secondoContrattoF':secondoContrattoF,
			'secondoContrattoP': secondoContrattoP,
			'secondoContrattoSub': secondoContrattoSub,
			'secondoContrattoCat':secondoContrattoCat,
			'secondoContrattoProprieta': secondoContrattoProprieta,
			'secondoContrattoPerCui_eStato': secondoContrattoPerCui_eStato,
			"secondoContrattoPeriodoDiMensilita": secondoContrattoPeriodoDiMensilita,

			'terzoContrattoAffittoStipulatoCon':terzoContrattoAffittoStipulatoCon,
			'terzoContrattoRegistratoInData':terzoContrattoRegistratoInData,
			'terzoContrattoAlN':terzoContrattoAlN,
			'terzoContrattoUfficioDelRegistroDi':terzoContrattoUfficioDelRegistroDi,
			'terzoContrattoAffittoDellAlloggioSitoInComuneDi':terzoContrattoAffittoDellAlloggioSitoInComuneDi,
			'terzoContrattoIndirizzo':terzoContrattoIndirizzo,
			'terzoContrattoDiDimensionePariAMQ':terzoContrattoDiDimensionePariAMQ,
			'terzoContrattoF':terzoContrattoF,
			'terzoContrattoP': terzoContrattoP,
			'terzoContrattoSub': terzoContrattoSub,
			'terzoContrattoCat':terzoContrattoCat,
			'terzoContrattoProprieta': terzoContrattoProprieta,
			'terzoContrattoPerCui_eStato': terzoContrattoPerCui_eStato,
			"terzoContrattoPeriodoDiMensilita": terzoContrattoPeriodoDiMensilita,

			'inCasoDiAssegnazioneDelContributoEsso':inCasoDiAssegnazioneDelContributoEsso,
			'intestatoA':intestatoA,
			'CodiceIban':CodiceIban,
			'CO': co,
			'signature':signature,
			'indirizzoRecapito': indirizzoRecapito,
			'indirizzoNumero': indirizzoNumero,
			'localita':localita,
			'cap': cap,
			'provinicia': provinicia,
			"telefonoAbitazione": telefonoAbitazione,
			'cellulare': cellulare,
			'email': email,
			'pec': pec,
			'numeroMarca': numeroMarca,
			'dataMarca': dataMarca,
			'files': files,

		},
		success: function (output) {
		
			download(output, 'domanda aggiornata.pdf', 'application/pdf')
		
			Swal.update({
			  'text': 'Il documento è stato scaricato correttamente',
			  'icon': 'success'
			});
			Swal.hideLoading();	
	
		},
		error: function(r, o, n) {
			Swal.update({
				'text': 'Si è verificato un errore durante il download del documento, si prega di riprovare',
				'icon': 'error'
			});
			Swal.hideLoading();	
	
		},
	});

  
}

function nf_download_pdf(){
	var fields = this.form.fields;
	var nomeRichiedente = getByKey(fields,'nome_richiedente_1596638557958',true);
	var cognomeRichiedente = getByKey(fields,'cognome_richiedente_1596638604326',true);
	var codiceFiscale = getByKey(fields,'codice_fiscale_1596638684501',true);
	var email = getByKey(fields,'email_1596794400214',true);
	var numTelXComunicazioni = getByKey(fields,'numero_di_telefono_per_comunicazioni_1596794502034',true);
	var indirizzoResidenza = getByKey(fields,'indirizzo_di_residenza_1596794801479',true);
	var comuneResidenza = getByKey(fields,'comune_di_residenza_1596794837959',false);
	var numProtocollo = getByKey(fields,'numero_protocollo_della_domanda_1596631897406',true);
	var dataProtocollo = getByKey(fields,'data_protocollo_della_domanda_1598865874724',true);
	var confORettdati = getByKey(fields,'conferma_e_o_rettifica_dati_1596632194062',false);
	var acqAutBassEmAdAlimen = getByKey(fields,'acquista_una_autovettura_m1_a_basse_emissioni_ad_alimentazione_1596633371902',false);
	var numSerieMarcaBollo = getByKey(fields,'numero_serie_marca_da_bollo_1596634192725',true);
	var dataMarcaBollo = getByKey(fields,'data_marca_da_bollo_1596634244112',true);
	
	var adAlimentazione = getByKey(fields,'ad_alimentazione_1628659442578',false);
	var classeAmbientale = getByKey(fields,'classe_ambientale_1628660498956',false);
	var intestazioneVeicolo = getByKey(fields,'intestazione_veicolo_1628660691070',false);
	var comuneDiresidenzaIntestatario = getByKey(fields,'comune_di_residenza_intestatario_1628661849279',false);
	var gradoDiParentela = getByKey(fields,'grado_di_parentela_tra_il_richiedente_il_contributo_e_il_proprietario_del_veicolo_da_rottamare_1628662013117',false);
	var alimentazioneNuovaVettura = getByKey(fields,'ad_alimentazione_1630047993724',false);

	var comuneNascita = getByKey(fields,'comune_nascita_1629727268349',true);
	var nomeIntestatario = getByKey(fields,'nome_intestatario_1629729590859',true);
	var cognomeIntestarario = getByKey(fields,'cognome_intestarario_1629730020423',true);
	var codiceFiscaleIntestarario = getByKey(fields,'codice_fiscale_intestarario_1629730074843',true);
	var targaAutovettura = getByKey(fields,'targa_autovettura_1629727714767',true);
	var indirizzoResidenzaIntestatario = getByKey(fields,'indirizzo_residenza_intestatario_1629730260171',true);
	var numCivicoResidenzaIntestatario = getByKey(fields,'num_civico_residenza_intestatario_1629730364331',true);
	var datanascita = getByKey(fields,'datanascita_1630042831086',true);
	
	
	Swal.fire({
		  text: "Download in corso",
		  confirmButtonColor: '#3085d6',
		  confirmButtonText: 'Si',
		  onBeforeOpen: () => {
			Swal.showLoading();
		  },
		  allowOutsideClick: () => !Swal.isLoading()
	});
		
	jQuery.ajax({
		method: "POST",
		url: nf_custom_js.ajax_url,
		data: {
			'action': 'save_PDF_templateAjax',
			'nomeRichiedente': nomeRichiedente,
			'cognomeRichiedente': cognomeRichiedente,
			'codiceFiscale': codiceFiscale,
			'email':email,
			'numTelXComunicazioni':numTelXComunicazioni,
			'indirizzoResidenza':indirizzoResidenza,
			'comuneResidenza':comuneResidenza,
			'numProtocollo':numProtocollo,
			'dataProtocollo':dataProtocollo,
			'confORettdati':confORettdati,
			'acqAutBassEmAdAlimen':acqAutBassEmAdAlimen,
			'numSerieMarcaBollo':numSerieMarcaBollo,
			'dataMarcaBollo':dataMarcaBollo,
			'comuneNascita': comuneNascita,
			'targaAutovettura': targaAutovettura,
			'classeAmbientale': classeAmbientale,
			'adAlimentazione': adAlimentazione,
			'intestazioneVeicolo': intestazioneVeicolo,
			'nomeIntestatario':nomeIntestatario,
			'cognomeIntestarario':cognomeIntestarario,
			'codiceFiscaleIntestarario':codiceFiscaleIntestarario,
			'comuneDiresidenzaIntestatario':comuneDiresidenzaIntestatario,
			'indirizzoResidenzaIntestatario':indirizzoResidenzaIntestatario,
			'numCivicoResidenzaIntestatario':numCivicoResidenzaIntestatario,
			'gradoDiParentela':gradoDiParentela,
			'alimentazioneNuovaVettura':alimentazioneNuovaVettura,
			'datanascita':datanascita
		},
		success: function (output) {
		
			download(output, 'Contributo_Sostituzione_Veicoli_Inquinanti_Fase_2.pdf', 'application/pdf')
		
			Swal.update({
			  'text': 'Il documento è stato scaricato correttamente',
			  'icon': 'success'
			});
			Swal.hideLoading();	
	
		},
		error: function(r, o, n) {
			Swal.update({
				'text': 'Si è verificato un errore durante il download del documento, si prega di riprovare',
				'icon': 'error'
			});
			Swal.hideLoading();	
	
		},
	});
			
}

function nf_custom_rinuncia(sub_id, ele) {
  Swal.fire({
    text: "Sei sicuro di voler procedure con la rinuncia del contributo?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'No',
    cancelButtonText: 'Si',
  }).then((result) => {
    if (result.value) {
      return;
    }
    Swal.fire({
      text: "Invio in corso della richiesta di rinuncia, si prega di attendere",
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Si',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
      allowOutsideClick: () => !Swal.isLoading()
    });

    //
    var data = {
      'action': 'nf_custom_rinuncia',
      'sub_id': sub_id
    };

    $.ajax({
      type: 'POST',
      url: nf_custom_js.ajax_url,
      data: data,
    }).done(function (response) {
		console.log('Success: ',response);
		if(response !== undefined){
			response = response.replace(/^\s+|\s+$/gm,'');
		} 		
      if ('1' === response || 'ok' === response) {
        $(ele).remove();
        $('.nf-custom-table-status-' + sub_id).text('Rinuncia');
        Swal.update({
          'text': 'La rinuncia al contributo è stata notificata con successo',
          'icon': 'success'
        });
      } else {
        Swal.update({
          'text': 'Si è verificato un errore durante l’invio della notifica di rinuncia , si prega di riprovare',
          'icon': 'error'
        });
      }
    }).fail(function () {
		console.log('Failure');
      Swal.update({
        'text': 'Si è verificato un errore durante l’invio della notifica di rinuncia , si prega di riprovare',
        'icon': 'error'
      });
    }).always(function () {
      Swal.hideLoading();
    });

  });

}
function nf_scarica_allegeti(files) {
	
	  //
	  var data = {
		'action': 'nf_allegati_scarica',
		'files': files
	  };
	  
	  jQuery('body').addClass('loader-overlay');
	  jQuery('body').css('position','relative');
	  jQuery('.loader-gif').css('display','block');

	  jQuery.ajax({
		type: 'POST',
		url: nf_custom_js.ajax_url,
		data: data,
	  }).done(function (response) {
		window.open(response, '_blank');
		jQuery('body').removeClass('loader-overlay');
		jQuery('.loader-gif').css('display','none');
		var dataremove = {
			'action': 'nf_allegati_scarica_remove',
			'file': response
		  };
		jQuery.ajax({
			type: 'POST',
			url: nf_custom_js.ajax_url,
			data: dataremove,
		  });
	  }).fail(function () {
		
	  }).always(function () {
		
	  });
}

function nf_remove_documento_finale(postId,slug) {
	
	//
	var data = {
	  'action': 'nf_remove_documento_finale',
	  'id': postId,
	  'slug':slug
	};
	jQuery.ajax({
	  type: 'POST',
	  url: nf_custom_js.ajax_url,
	  data: data,
	}).done(function (response) {
		if(response){
			window.location.reload();
		}
	}).fail(function () {
	  
	}).always(function () {
		jQuery('body').removeClass('loader-overlay');
		jQuery('.loader-gif').css('display','none');
	});

  
}

function nf_aggiungi_allegati() {
	sub_id = jQuery('input[name=int_sub_id]').val();
	form_id = jQuery('input[name=int_form_id]').val();
	var file_data = jQuery('#integrazioni_submission_file').prop('files');
	if(file_data.length == 0){
		document.getElementById('interror').innerHTML="Please select file";
		jQuery('#interror').show();
	}else {
		jQuery('#interror').hide();
		  jQuery('body').addClass('loader-overlay');
		  jQuery('body').css('position','relative');
		  jQuery('.loader-gif').css('display','block');

		  var fd = new FormData();
		  fd.append("file", file_data[0]);
		  fd.append('action', 'nf_aggiungi_allegati');  
		  fd.append('sub_id', sub_id);
		  fd.append('form_id', form_id);
		  jQuery.ajax({
			type: 'POST',
			processData : false,
			contentType: false,
			url: nf_custom_js.ajax_url,
			data: fd,
		  }).done(function (response) {
			if(response){
				window.location.reload();
			}
		  }).fail(function () {
			
		  }).always(function () {
			
		  });
		
	}
}

function nf_payment_status(sub_id,form_id) {
	jQuery('#payerror').hide();
	jQuery('body').addClass('loader-overlay');
	jQuery('body').css('position','relative');
	jQuery('.loader-gif').css('display','block');
	var fd = new FormData();
	fd.append('action', 'nf_get_payment_status');  
	fd.append('sub_id', sub_id);
	fd.append('form_id', form_id);
	jQuery.ajax({
		type: 'POST',
		processData : false,
		contentType: false,
		url: nf_custom_js.ajax_url,
		data: fd,
	  }).done(function (response) {
		console.log("nf_payment_status Response" ,response);
		if(response.error !== undefined){
			jQuery('#epxml_body').hide();
			jQuery('#rtxml_body').hide();
			jQuery('#payerror').show()
			jQuery('#payerror').html(response.error); 
			jQuery('#paymentStatusModal').modal('show'); 
		}else {
			jQuery('#epxml_body').show();
			jQuery('#rtxml_body').show();
			if(response.pagati !== undefined){
				var epblob = b64toBlob(response.pagati, "application/xml");
				var epblobUrl = URL.createObjectURL(epblob);
				jQuery('#epxml').attr('href', epblobUrl);
			}
			if(response.rt !== undefined){
				var rtblob = b64toBlob(response.rt, "application/xml");
				var rtblobUrl = URL.createObjectURL(rtblob);
				jQuery('#rtxml').attr('href', rtblobUrl);
			}
			jQuery('#paymentStatusModal').modal('show'); 
		}
		
	  }).fail(function () {
			
	  }).always(function () {
		jQuery('body').removeClass('loader-overlay');
		jQuery('.loader-gif').css('display','none');
	  });
}

function b64toBlob(b64Data, contentType, sliceSize) {
	contentType = contentType || '';
	sliceSize = sliceSize || 512;

	var byteCharacters = atob(b64Data);
	var byteArrays = [];

	for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		var slice = byteCharacters.slice(offset, offset + sliceSize);

		var byteNumbers = new Array(slice.length);
		for (var i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		var byteArray = new Uint8Array(byteNumbers);

		byteArrays.push(byteArray);
	}

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}


function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

;(function ($) {
    const NFCustomFieldValidateController = Marionette.Object.extend({
        initialize: function() {
            // On the Form Submission's field validaiton…
            var submitChannel = Backbone.Radio.channel( 'submit' );
            this.listenTo( submitChannel, 'validate:field', this.validate );

            // on the Field's model value change…
            var fieldsChannel = Backbone.Radio.channel( 'fields' );
            this.listenTo( fieldsChannel, 'change:modelValue', this.validate );
            this.listenTo( fieldsChannel, 'render:view', function (view) {
                if (view.model.get('type') !== 'file_upload') {
                    return;
                }
                var elementClass = view.model.get('element_class');
                if (!elementClass || !elementClass.includes('nf-custom-file-not-empty')) {
                    return;
                }
                var $file = $( view.el ).find( '.nf-element' );
                $file.fileupload('option', 'minFileSize', 1);
                var messages = $file.fileupload('option', 'messages');
                messages.minFileSize = "Il file è vuoto";
            } );

        },

        regex1: function (model) {
            const regex = /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i;
            return this._regexCheck(model, regex);
        },

        regexIban: function (model) {
            const regex = /^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}$/i;
            return this._regexCheck(model, regex);
        },
        regexEuro: function (model) {
            const regex = /^(0|[1-9]\d*)(\,\d{1,2})?$/i;
            return this._regexCheckEuro(model, regex);
        },
        _regexCheck: function (model, regex, message = 'Formato non valido per il campo %label%') {
            const str = model.get('value');

            const modelID = model.get('id');
            const errorID = 'custom-field-error';
            const errorMessage = message.replace('%label%', model.get('label'));
            const fieldsChannel = Backbone.Radio.channel('fields');

            if (regex.exec(str) !== null) {
                fieldsChannel.request('remove:error', modelID, errorID);
            } else {
                // Add Error
                fieldsChannel.request('add:error', modelID, errorID, errorMessage);
            }
        },
		_regexCheckEuro: function (model, regex, message = 'Formato importo non corretto, per esempio utilizzare 19453 oppure 19453,00') {
            const str = model.get('value');

            const modelID = model.get('id');
            const errorID = 'custom-field-error';
            const errorMessage = message.replace('%label%', model.get('label'));
            const fieldsChannel = Backbone.Radio.channel('fields');

            if (regex.exec(str) !== null) {
                fieldsChannel.request('remove:error', modelID, errorID);
            } else {
                // Add Error
                fieldsChannel.request('add:error', modelID, errorID, errorMessage);
            }
        },

        validate: function(model ) {
            const elementClass = model.get('element_class');

            if (!elementClass) {
                return;
            }
            if (elementClass.includes('nf-custom-regex-1')) {
                return this.regex1(model);
            }

            if (elementClass.includes('nf-custom-regex-iban')) {
                return this.regexIban(model);
            }
			if (elementClass.includes('nf-custom-regex-euro')) {
                return this.regexEuro(model);
            }
        }

    });

    $( document ).ready( function() {
        new NFCustomFieldValidateController();
    });
})(jQuery);

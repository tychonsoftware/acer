<?php if( !empty( $instance['title'] ) ) echo $args['before_title'] . $instance['title'] . $args['after_title'] ?>

<div class="siteorigin-widget-tinymce textwidget">
	<?php
		echo $text;
    $dom = new DOMDocument;
    $dom->loadHTML($text,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $as = $dom->getElementsByTagName('a');
    $href = '';
    if($as && count($as) > 0){
        $a = $as[0];
        $href =  $a->getAttribute('href');
    }
    ?>
    <p class="visualizza-block">
        <?php
    if(strlen($href) > 0){
        ?>
        <a class="visualizza-btn" href="<?php echo $href ?>">VISUALIZZA</a>
        <?php
    }
	?>
    </p>
<!--	<p class="visualizza-block">-->

<!--		<a class="visualizza-btn" href="--><?php //if ( ! empty( $url ) ) : $this->generate_anchor_open( $url, $link_attributes ); endif; ?><!--">VISUALIZZA</a>-->
<!--	</p>-->
</div>

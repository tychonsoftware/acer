
<script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>includes/excellentexport.min.js"></script>
<script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>includes/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>includes/jspdf.plugin.autotable.min.js"></script>
<style>
    .avcp_export_btn{
        display:inline-block;
    }
    .footer_pgination{
        text-align:right;
    }
    #gare_length {
        max-width: 8% !important;
    }

</style>



<table class="order-table table" id="gare_export" style="display: none">
    <thead>
    <tr><td colspan="7">
            Bandi di gara - <strong><?php if ($anno != 'all') { echo $anno; } else { echo 'Tutti gli anni'; } ?></strong>
            <input style="float:right;" type="search" id="s" class="light-table-filter" data-table="order-table" placeholder="Cerca...">
        </td></tr>
    <tr>
        <th colspan="2">Oggetto</th>
        <th>CIG</th>
        <th>Importo<br/>agg.</th>
        <th>Durata<br/>lavori</th>
        <th>Modalità<br/>affidamento</th>
        <th style="display: none">Struttura<br/>proponente</th>
        <th style="display: none">Importo delle<br/>somme liquidate</th>
        <th style="display: none">Anno di<br/>riferimento</th>
        <th style="display: none">Responsabile<br/>provvedimento</th>
        <th style="display: none">Numero<br/>provvedimento</th>
        <th style="display: none">Data<br/>provvedimento</th>
    </tr>
    </thead>
    <tbody>

    <?php


    $pdf_table = '<div id="sourcedata" style="display: none" class="table-responsive">';
    $pdf_table .= '<table class="order-table table" id="pdftableexport"> <thead> 
                    <tr> <th>CIG</th> <th >NUM. E<br/>DATA<br/>PROVVEDIMENTO</th><th>OGGETTO</th> <th>SCELTA<br/>CONTRAENTE</th><th>OPERATORI<br>INVITATI</th><th>DITTE<br>AGGIUDICATARIE</th><th>IMPORTO<br>AGGIUDICATO</th> <th>SOMME<br>LIQUIDATE</th><th>DATA<br>INIZIO/<br>DATA FINE</th><th>RESPONSABILE<br>PROVVEDIMENTO</th></tr></thead>';
    $pdf_table .= '<tbody>';
    if ($anno == "all") {
        $anno = '';
    }


    // $the_query  = query_posts(
    //     array( 'post_type' => 'avcp', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 5, 'annirif' => $anno, 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 )
    // );

    $the_query = new WP_Query( array( 'post_type' => 'avcp', 'orderby' => 'date', 'order' => 'DESC',
            'posts_per_page'   => -1,
            'annirif' => $anno, 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 )
    );


    while ( $the_query -> have_posts() ) : $the_query -> the_post();

        // global $post;
        // if(strpos(get_post_meta($post->ID, 'avcp_aggiudicazione', true), ",") !== false){
        //     $price = get_post_meta($post->ID, 'avcp_aggiudicazione', true);
        // }else{

        $price = number_format(get_post_meta(get_the_ID(), 'avcp_aggiudicazione', true), 2, ',', '.');
        // }
        ///print_r($price); echo "<br>";

        $d_i = get_post_meta(get_the_ID(), 'avcp_data_inizio', true);
        $d_f = get_post_meta(get_the_ID(), 'avcp_data_fine', true);

        $d_i =  $d_i ? date("d/m/Y", strtotime( $d_i )) : '-';
        $d_f =  $d_f ? date("d/m/Y", strtotime( $d_f )) : '-';
        $pdf_table .= '<tr style="display: table-row;">';


        // CIG
        $pdf_table .= '<td>' . get_post_meta(get_the_ID(), 'avcp_cig', true) . '</td>';
        // Numero provvedimento e Data provvedimento
        $pdf_table .=  '<td> n.' . get_post_meta(get_the_ID(), 'avcp_numero_provvedimento', true) . ' del ';
        $pdf_table .=  get_post_meta(get_the_ID(), 'avcp_data_provvedimento', true). '</td>';
        // Oggetto
        $pdf_table .= '<td><a href="' . get_permalink() . '">' . get_the_title() . '</a></td>';

        // Scelta contraente
        $pdf_table .=  '<td>' . strtolower(substr(get_post_meta(get_the_ID(), 'avcp_contraente', true), 3)) . '</td>';
        // Opertatori Invitati
        $terms_ditte = get_the_terms( get_the_ID(), 'ditte' );
        $operatori_invitati  = '';
        foreach ($terms_ditte as $term) {
            if(!empty($operatori_invitati)){
                $operatori_invitati .= ',';
            }
            $t_id = $term->term_id;
            $term_meta = get_option( "taxonomy_$t_id" );
            $codice_fiscale = esc_attr( $term_meta['avcp_codice_fiscale'] );
            if(!empty($codice_fiscale)){
                $operatori_invitati .= '[' . $codice_fiscale . ']';
            }
            if(!empty($term->name)){
                if(!empty($codice_fiscale)){
                    $operatori_invitati .= ' - ';
                }
                $operatori_invitati .= $term->name;
            }
        }
        $pdf_table .= '<td align="center"> ' . $operatori_invitati . '</td>';
        // Ditte Aggiudicatarie
        $cats = get_post_meta(get_the_ID(),'avcp_aggiudicatari',true);
        $ditte_aggiudicatarie  = '';

        foreach ( $cats as $ditte ) {
            if(!empty($ditte_aggiudicatarie)) {
                $ditte_aggiudicatarie .= ", ";
            }
            $cterm = get_term_by('id', $ditte, 'ditte');
            $ditte_aggiudicatarie  .= $cterm->name;
        }
        $pdf_table .= '<td align="center"> ' . $ditte_aggiudicatarie . '</td>';
        // Importo Aggiudicato
        $pdf_table .= '<td align="center">€<strong>' . $price . '</strong></td>';

        // Somme Liquidate
        $some_liquidate = 0.0;
        for ($i = 2013; $i < 2025; $i++) {
            if ( get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true) > 0) {
                $some_liquidate +=   floatval(get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true));
            }
        }
        $pdf_table .= '<td align="center">' .number_format($some_liquidate,2) . '</td>';

        // Data Inizio/Data Fine
        $pdf_table .= '<td align="center">' . $d_i . '<br/>' . $d_f . '<br/>';
        // Responsabile Provvedimento
        $pdf_table .=  '<td>' . get_post_meta(get_the_ID(), 'avcp_responsabile_provvedimento', true) . '</td>';
        echo '<tr style="display: table-row;">';
        echo '<td colspan="2"><a href="' . get_permalink() . '">' . get_the_title() . '</a></td>';
        echo '<td>' . get_post_meta($post->ID, 'avcp_cig', true) . '</td>';
        echo '<td align="center">€<strong>' . $price . '</strong></td>';
        echo '<td align="center">' . $d_i . '<br/>' . $d_f . '<br/>';
        if (function_exists('DateTime')) {
            $date1 = new DateTime(get_post_meta(get_the_ID(), 'avcp_data_inizio', true));
            $date2 = new DateTime(get_post_meta(get_the_ID(), 'avcp_data_fine', true));
            $diff = $date2->diff($date1)->format("%a");
            echo '<small><strong>' . $diff . '</strong> gg</small>';
        }

        echo '</td>';
        echo '<td>' . strtolower(substr(get_post_meta(get_the_ID(), 'avcp_contraente', true), 3)) . '</td>';
        echo '<td style="display: none">' . get_option('avcp_denominazione_ente') . ' ' . get_option('avcp_codicefiscale_ente') . '</td>';

        $pdf_table .=  '</td>';
       // $pdf_table .=  '<td>' . strtolower(substr(get_post_meta(get_the_ID(), 'avcp_contraente', true), 3)) . '</td>';
       //  $pdf_table .=  '<td style="display: none">' . get_option('avcp_denominazione_ente') . ' ' . get_option('avcp_codicefiscale_ente') . '</td>';

        $liquidate = '';
        for ($i = 2013; $i < 2025; $i++) {
            if ( get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true) > 0) {
                $liquidate .= $i . ' - ' . get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true).' <br/>';
            }
        }
        echo '<td style="display: none">' . $liquidate . '</td>';
       // $pdf_table .=  '<td style="display: none">' . $liquidate . '</td>';

        $term_list = wp_get_post_terms( get_the_ID(), 'annirif', array( 'fields' => 'all' ) );
        $annodiref = [];
        foreach ($term_list as $term) {
            array_push($annodiref,$term->name);
        }
        echo '<td style="display: none">' . implode(', ', $annodiref). '</td>';
        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_responsabile_provvedimento', true) . '</td>';
        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_numero_provvedimento', true) . '</td>';
        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_data_provvedimento', true). '</td>';
        echo '</tr>';

      //  $pdf_table .=  '<td>' . implode(', ', $annodiref). '</td>';
      //  $pdf_table .=  '<td>' . get_post_meta(get_the_ID(), 'avcp_responsabile_provvedimento', true) . '</td>';
        $pdf_table .=  '</tr>';
    endwhile;
    wp_reset_query();

    echo '</tbody>
    </table>';
    $pdf_table .= '</tbody></table></div>';
    echo $pdf_table;
    ?>
    <style>
        div#gare_filter {
            display: inline-grid;
            width: 90%;
        }
        div#gare_filter label {
            text-align: left;
        }
        .dataTables_wrapper .dataTables_filter input{
            margin-left:0px !important;
            height: 48px;
        }
        /* .dataTables_length label {
            display: inline-flex;
        } */
        /* .dataTables_length label select {
            margin: -7px 10px 10px 10px;
        } */
        .entry-title{
            margin-bottom:30px;
        }
    </style>
    <table class="order-table table" id="gare">
        <thead>
        <tr><td colspan="5">
                Bandi di gara - <strong><?php if ($anno != 'all') { echo $anno; } else { echo 'Tutti gli anni'; } ?></strong>

            </td></tr>
        <tr>
            <th >Oggetto</th>
            <th>CIG</th>
            <th>Importo<br/>agg.</th>
            <th>Durata<br/>lavori</th>
            <th>Modalità<br/>affidamento</th>
            <!-- <th style="display: none">Struttura<br/>proponente</th>
            <th style="display: none">Importo delle<br/>somme liquidate</th>
            <th style="display: none">Anno di<br/>riferimento</th>
            <th style="display: none">Responsabile<br/>provvedimento</th>
            <th style="display: none">Numero<br/>provvedimento</th>
            <th style="display: none">Data<br/>provvedimento</th> -->
        </tr>
        </thead>
        <tbody>

        <?php


        $pdf_table = '<div id="sourcedata" style="display: none" class="table-responsive">';
        $pdf_table .= '<table class="order-table table" id="pdftable"> <thead> <tr> <th>Oggetto</th> <th>CIG</th> <th>Importo<br>agg.</th> <th>Durata<br>lavori</th> <th>Modalità<br>affidamento</th> <th style="display: none">Struttura<br>proponente</th> <th style="display: none">Importo delle<br>somme liquidate</th> <th >Anno di<br>riferimento</th> <th >Responsabile<br>provvedimento</th> <th >Numero<br>provvedimento</th> <th >Data<br>provvedimento</th> </tr></thead>';
        $pdf_table .= '<tbody>';
        if ($anno == "all") {
            $anno = '';
        }

        // $the_query  = query_posts(
        //     array( 'post_type' => 'avcp', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 5, 'annirif' => $anno, 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 )
        // );

        //        $the_query = new WP_Query( array(
        //                'post_type' => 'avcp',
        //                'orderby' => 'date',
        //                'order' => 'DESC',
        //                'posts_per_page' => -1
        //
        //                 )
        //        );


        $the_query = new WP_Query( array( 'post_type' => 'avcp', 'orderby' => 'date', 'order' => 'DESC',
                'posts_per_page'   => -1,
                'annirif' => $anno, 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 )
        );


        while ( $the_query -> have_posts() ) : $the_query -> the_post();

            global $post;

            // if(strpos(get_post_meta($post->ID, 'avcp_aggiudicazione', true), ",") !== false){
            //     $price = get_post_meta($post->ID, 'avcp_aggiudicazione', true);
            // }else{
            $price = number_format(get_post_meta($post->ID, 'avcp_aggiudicazione', true), 2, ',', '.');

            // }

            $d_i = get_post_meta(get_the_ID(), 'avcp_data_inizio', true);
            $d_f = get_post_meta(get_the_ID(), 'avcp_data_fine', true);

            $d_i =  $d_i ? date("d/m/Y", strtotime( $d_i )) : '-';
            $d_f =  $d_f ? date("d/m/Y", strtotime( $d_f )) : '-';
            $pdf_table .= '<tr style="display: table-row;">';
            $pdf_table .= '<td><a href="' . get_permalink() . '">' . get_the_title() . '</a></td>';

            $pdf_table .= '<td>' . get_post_meta($post->ID, 'avcp_cig', true) . '</td>';
            $pdf_table .= '<td align="center">€<strong>' . $price . '</strong></td>';
            $pdf_table .= '<td align="center">' . $d_i . '<br/>' . $d_f . '<br/>';


            echo '<tr style="display: table-row;">';
            echo '<td ><a href="' . get_permalink() . '">' . get_the_title() . '</a></td>';
            echo '<td>' . get_post_meta($post->ID, 'avcp_cig', true) . '</td>';
            echo '<td align="center">€<strong>' . $price . '</strong></td>';
            echo '<td align="center">' . $d_i . '<br/>' . $d_f . '<br/>';

            if (function_exists('DateTime')) {
                $date1 = new DateTime(get_post_meta(get_the_ID(), 'avcp_data_inizio', true));
                $date2 = new DateTime(get_post_meta(get_the_ID(), 'avcp_data_fine', true));
                $diff = $date2->diff($date1)->format("%a");
                echo '<small><strong>' . $diff . '</strong> gg</small>';
                $pdf_table .=  '<small><strong>' . $diff . '</strong> gg</small>';
            }

            echo '</td>';
            echo '<td>' . strtolower(substr(get_post_meta(get_the_ID(), 'avcp_contraente', true), 3)) . '</td>';
//        echo '<td style="display: none">' . get_option('avcp_denominazione_ente') . ' ' . get_option('avcp_codicefiscale_ente') . '</td>';
//
//        $pdf_table .=  '</td>';
            $pdf_table .=  '<td>' . strtolower(substr(get_post_meta(get_the_ID(), 'avcp_contraente', true), 3)) . '</td>';
//        $pdf_table .=  '<td style="display: none">' . get_option('avcp_denominazione_ente') . ' ' . get_option('avcp_codicefiscale_ente') . '</td>';

            $liquidate = '';
            for ($i = 2013; $i < 2025; $i++) {
                if ( get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true) > 0) {
                    $liquidate .= $i . ' - ' . get_post_meta(get_the_ID(), 'avcp_s_l_'.$i, true).' <br/>';
                }
            }
//        echo '<td style="display: none">' . $liquidate . '</td>';
//        $pdf_table .=  '<td style="display: none">' . $liquidate . '</td>';

            $term_list = wp_get_post_terms( get_the_ID(), 'annirif', array( 'fields' => 'all' ) );
            $annodiref = [];
            foreach ($term_list as $term) {
                array_push($annodiref,$term->name);
            }
//        echo '<td style="display: none">' . implode(', ', $annodiref). '</td>';
//        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_responsabile_provvedimento', true) . '</td>';
//        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_numero_provvedimento', true) . '</td>';
//        echo '<td style="display: none">' . get_post_meta(get_the_ID(), 'avcp_data_provvedimento', true). '</td>';
            echo '</tr>';

            $pdf_table .=  '<td>' . implode(', ', $annodiref). '</td>';
            $pdf_table .=  '<td>' . get_post_meta(get_the_ID(), 'avcp_responsabile_provvedimento', true) . '</td>';
            $pdf_table .=  '<td>' . get_post_meta(get_the_ID(), 'avcp_numero_provvedimento', true) . '</td>';
            $pdf_table .=  '<td>' . get_post_meta(get_the_ID(), 'avcp_data_provvedimento', true). '</td>';
            $pdf_table .=  '</tr>';
        endwhile;
        wp_reset_query();

        echo '</tbody>

    <tfoot>
        <tr>
            <td colspan="5">';

        $file_name = preg_replace( '/[^a-z0-9]+/', '-', strtolower( get_bloginfo('name') . '-gare' . $anno ) );
        echo '<div style="text-align:right; width:100%">
                        Scarica in

            <a class="avcp_export_btn" href="' . get_site_url() . '/avcp" target="_blank" title="File .xml"><button>XML</button></a>
            <a class="avcp_export_btn" download="' . get_bloginfo('name') . '-gare' . $anno . '.xls" href="#" onclick="return ExcellentExport.excel(this, \'gare_export\', \'Gare\');"><button>EXCEL</button></a>
            <a class="avcp_export_btn" download="' . get_bloginfo('name') . '-gare' . $anno . '.csv" href="#" onclick="return ExcellentExport.csv(this, \'gare_export\');"><button>CSV</button></a>
            
            <a class="avcp_export_btn" href="#" onclick="generatePDF(\'' . $file_name  .'.pdf\')"><button>PDF</button></a>
            </div>';

        if (get_option('wpgov_show_love')) {
            echo '<a href="http://www.wpgov.it" target="_blank" title="Software &copy; WPGov"><img src="' . plugin_dir_url(__FILE__) . 'images/wpgov.png" /></a>';
        }

        echo '</td>
        </tr>
    </tfoot>
    </table>';
        echo '<div class="clear"></div>';

        $pdf_table .= '</tbody></table></div>';
        echo $pdf_table;
        //    echo '<div id="sourcedata" style="display: none" class="table-responsive"> <table class="order-table table" id="pdftable"> <thead> <tr> <th>Oggetto</th> <th>CIG</th> <th>Importo<br>agg.</th> <th>Durata<br>lavori</th> <th>Modalità<br>affidamento</th> <th style="display: none">Struttura<br>proponente</th> <th style="display: none">Importo delle<br>somme liquidate</th> <th >Anno di<br>riferimento</th> <th >Responsabile<br>provvedimento</th> <th >Numero<br>provvedimento</th> <th >Data<br>provvedimento</th> </tr></thead> <tbody><tr style="display: table-row;"><td><a href="http://localhost:8082/artensys/avcp/3926/"></a></td><td>0000000000</td><td align="center">€<strong></strong></td><td align="center">04/12/2021<br>17/12/2021<br></td><td>affidamento diretto ex art. 5 della legge 381/91</td><td >A.O.R.N. Santobono-Pausilipon 06854100630</td><td >2013 - 1.00 <br>2014 - 2.00 <br>2015 - 3.00 <br>2017 - 3.00 <br>2021 - 3.00 <br></td><td >2013, 2014, 2015, 2016, 2017, 2018, 2019, 2021</td><td >Pallini Maria</td><td >ANSHUMAN123</td><td >2021-12-14</td></tr></tbody> </table> </div>';
        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <link rel="stylesheet"
              href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript"
                src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script>

            jQuery(document).ready(function(){

                jQuery("#gare").dataTable({
                    "ordering":false,
                    "language": {
                        "info":           "Da _START_ a _END_ di _TOTAL_ elementi",
                        "lengthMenu":     "Mostra _MENU_ elementi",
                        "search":         "Ricerca:",
                        "paginate": {
                            "previous": "prec",
                            "next": "succ"
                        }
                    },
                    // "oLanguage": {
                    //     "sLengthMenu": "Mostra _MENU_ elementi",
                    //     "sSearch": "<span>Ricerca:</span> _INPUT_" //search
                    // }
                });

                // jQuery("#gare_previous").text("prec");
                // jQuery("#gare_next").text("succ");
                jQuery("#gare_filter input").attr("placeholder", "Cerca..");
                jQuery("#gare_filter input").attr("style", "display:table-cell; width:100%");


                jQuery("#gare_paginate .paginate_button").on('click',function(){
                    // setTimeout(function() {
                    //     jQuery("#gare_previous").text("prec");
                    //     jQuery("#gare_next").text("succ");
                    // }, 500);
                });
            });
            function generatePDF(fileName) {

                var pdf = new jsPDF('l', 'pt', 'a4');
                source = '<div class="table-responsive"> <table id="tbl" class="table table-hover"> <thead> <tr> <th colspan="2">Oggetto</th> <th>CIG</th> <th>Importo<br>agg.</th> <th>Durata<br>lavori</th> <th>Modalit<br>affidamento</th> </tr></thead> <tbody> <tr style="display: table-row;"><td colspan="2"><a href="http://localhost:8082/artensys/avcp/3926/"></a></td></tr></tbody> </table> </div>';
                specialElementHandlers = {
                    // element with id of "bypass" - jQuery style selector
                    '#bypassme': function (element, renderer) {
                        return true
                    }
                };
                var margins = {
                    top: 80,
                    bottom: 60,
                    left: 1,
                    right: 1,
                    width: 1200
                };

                pdf.autoTable({ html: '#pdftableexport' ,
                    theme: 'grid',
                    styles: {
                        fontSize: 7
                    },
                    startY: 60,
                    tableWidth: 'wrap',

                    headStyles : {
                        fillColor: [255, 255, 255],
                        textColor: '#000000',
                        fontSize: 7,
                        lineWidth: 0.2,
                        lineColor: [0, 0, 0],
                        valign: 'middle',
                        halign : 'center'
                    },
                    columnStyles: {
                        0: {cellWidth: 50},
                        1: {cellWidth: 80},
                        2: {cellWidth: 120},
                        3: {cellWidth: 80},
                        4: {cellWidth: 60},
                        5: {cellWidth: 60},
                        6: {cellWidth: 70},
                        7: {cellWidth: 50},
                        8: {cellWidth: 50},
                        9: {cellWidth: 90}
                    },
                    bodyStyles: {lineColor: [0, 0, 0]},
                    margin: {
                        left: 65,
                        right: 35,
                        //width: 120



                    }


                })
                pdf.save(fileName);
                return false;
            }


            (function(document) {
                'use strict';

                var LightTableFilter = (function(Arr) {

                    var _input;

                    function _onInputEvent(e) {
                        _input = e.target;
                        var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                        Arr.forEach.call(tables, function(table) {
                            Arr.forEach.call(table.tBodies, function(tbody) {
                                Arr.forEach.call(tbody.rows, _filter);
                            });
                        });
                    }

                    function _filter(row) {
                        var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                        row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                    }

                    return {
                        init: function() {
                            var inputs = document.getElementsByClassName('light-table-filter');
                            Arr.forEach.call(inputs, function(input) {
                                input.oninput = _onInputEvent;
                            });
                        }
                    };
                })(Array.prototype);

                document.addEventListener('readystatechange', function() {
                    if (document.readyState === 'complete') {
                        LightTableFilter.init();
                    }
                });

            })(document);




        </script>

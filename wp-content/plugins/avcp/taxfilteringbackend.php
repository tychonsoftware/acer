<?php
//FILTRI
add_action( 'restrict_manage_posts', 'avcp_restrict_manage_posts' );
function avcp_restrict_manage_posts() {
    global $typenow;
    $taxonomy = 'ditte';
    if ($typenow == 'avcp') {
        $filters = array($taxonomy);
        foreach ($filters as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>Tutte le ditte</option>";
            foreach ($terms as $term) {
                $label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : ''; // Fix
                echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
            }
            echo "</select>";
        }
    }
    $taxonomy = 'annirif';
    if ($typenow == 'avcp') {
        $filters = array($taxonomy);
        foreach ($filters as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>Tutti gli Anni di Riferimento</option>";
            foreach ($terms as $term) {
                $label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : ''; // Fix
                echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
            }
            echo "</select>";
        }
    }
}

//HOOK PER LE COLONNE DELLA NUOVA VISUALIZZAZIONE AMMINISTRATORE

function avcp_modify_post_table( $column ) {
    $column['avcp_CIG'] = 'CIG';
    $column['avcp_AGG'] = 'Aggiudicatari';
    return $column;
}
add_filter( 'manage_edit-avcp_columns', 'avcp_modify_post_table' );

function remove_post_columns($defaults) {
  unset($defaults['date']);
  return $defaults;
}
add_filter('manage_edit-avcp_columns', 'remove_post_columns');

function avcp_modify_post_table_row( $column_name, $post_id ) {
    $custom_fields = get_post_custom( $post_id );

    switch ($column_name) {
        case 'avcp_CIG' :
            if ( isset($custom_fields['avcp_cig'][0]) ) {
                echo $custom_fields['avcp_cig'][0];
            } else {
                echo '<center><font style="background-color:red;color:white;padding:2px;border-radius:3px;font-weight:bold;">Nessuno</font></center>';
            
            }
            break;
        case 'avcp_AGG' :
            $checkok = 0;
            $dittepartecipanti = get_the_terms( $post_id, 'ditte' );
            $cats = get_post_meta($post_id,'avcp_aggiudicatari',true);
            if(is_array($dittepartecipanti)) {
                foreach ($dittepartecipanti as $term) {
                    $cterm = get_term_by('name',$term->name,'ditte');
                    $cat_id = $cterm->term_id; //Prende l'id del termine
                    $term_meta = get_option( "taxonomy_$cat_id" );
                    $term_return = esc_attr( $term_meta['avcp_codice_fiscale'] );
                    $checked = (in_array($cat_id,(array)$cats)? ' checked="checked"': "");
                    if ($checked) {
                        echo $term->name . ', ';
                        $checkok++;
                    }
                }
            }
            if ($checkok == 0) {
                echo '<center><font style="background-color:red;color:white;padding:2px;border-radius:3px;font-weight:bold;">Nessuno</font></center>';
            }
            break;

        default:
    }
}
add_filter( 'manage_posts_custom_column', 'avcp_modify_post_table_row', 10, 2 );

function my_sortable_cake_column( $columns ) {
    $column['avcp_CIG'] = 'CIG';
    $column['avcp_AGG'] = 'AGG';

    return $columns;
}
add_filter( 'manage_edit-cake_sortable_columns', 'my_sortable_cake_column' );

/*
 * ADMIN COLUMN - SORTING - MAKE HEADERS SORTABLE
 * https://gist.github.com/906872
 */
add_filter("manage_edit-avcp_sortable_columns", 'avcp_date_sort');
function avcp_date_sort($columns) {
    $column['avcp_CIG'] = 'CIG';
    return $column;
}
/*
add_filter( "pre_get_posts", "custom_search_query");
function custom_search_query( $query ) {
    $custom_fields = array(
        "avcp_cig"
    );
    $searchterm = $query->query_vars['s'];
    $query->query_vars['s'] = "";

    if ($searchterm != "") {
        $meta_query = array('relation' => 'OR');
        foreach($custom_fields as $cf) {
            array_push($meta_query, array(
                'key' => $cf,
                'value' => $searchterm,
                'compare' => 'LIKE'
            ));
        }
        $query->set("meta_query", $meta_query);
        add_filter( 'posts_where', 'avcp_post_search', 10, 2 );
    };
}*/

function avcp_extend_search( $query ) {
    //$search_term = filter_input( INPUT_GET, 's', FILTER_SANITIZE_NUMBER_INT) ?: 0;
    $search_term = $query->query_vars['s'];
        $query->set('meta_query', [
            [
                'key' => 'avcp_cig',
                'value' => $search_term,
                'compare' => 'LIKE'
            ]
        ]);

        add_filter( 'get_meta_sql', function( $sql )
        {
            global $wpdb;

            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            $sql['where'] = mb_eregi_replace( '^ AND', ' OR', $sql['where']);
            error_log("AAAAAAAAAAAAAAAAAA " . print_r($sql,1));
            return $sql;
        });
    add_filter( 'posts_where', 'avcp_post_search', 10, 2 );

    return $query;
}

//add_filter( 'pre_get_posts', 'avcp_extend_search');

function avcp_post_search( $where, $wp_query )
{
    global $wpdb;
    $searchterm = $wp_query->query_vars['s'];
    $where .= 'OR (' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $searchterm ) ) . '%\'';
    return $where;
}

add_filter( 'pre_get_posts', 'avcp_custom_search');

function avcp_custom_search( $query ) {
    if($query->query['post_type'] === 'avcp'){
        $search_term = sanitize_text_field( get_query_var( 's' ) );
        global $wpdb;
        error_Log("query " . print_r($query,1));
        $complex_query = "SELECT SQL_CALC_FOUND_ROWS {$wpdb->posts}.ID FROM {$wpdb->posts} 
                        INNER JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id
                        WHERE {$wpdb->posts}.post_type = 'avcp' AND (({$wpdb->posts}.post_title LIKE '%$search_term%') OR 
                           {$wpdb->postmeta}.meta_key = 'avcp_cig' AND {$wpdb->postmeta}.meta_value LIKE  '%$search_term%')";

        error_Log("Custom query " . print_r($complex_query,1));
        $custom_search_query = $wpdb->get_col( $complex_query );
        $query->set('post__in',$custom_search_query);
        add_action( 'posts_search', '__return_empty_string' );
    }
}
?>

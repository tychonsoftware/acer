<?php


if (!(is_plugin_active( 'avcp/avcp.php' ))) { echo 'Plugin non installato!'; return;}

define( 'AVCP_IMPORT_PATH', dirname( __FILE__ ) );
include_once(dirname(__FILE__) . '/AVCPExcelUpdate.php');


if (isset($_REQUEST['post_type']) && isset($_REQUEST['page']) && $_REQUEST['post_type'] === 'avcp'
    && $_REQUEST['page'] === 'anac_import_somme_liquidate' && isset($_FILES) &&
    isset($_FILES['avcp-excel']['tmp_name'])) {
    (new AVCPExcelUpdate)->import();
}
$per_pages = 10;
if ( ! isset( $_REQUEST['Pag'] ) ) {
    $st = 0;
    $pp = $per_pages;
} else {
    $st = ( $_REQUEST['Pag'] - 1 ) * $per_pages;
    $pp = $per_pages;
}
$items = avcp_get_imports($st, $pp);
$itemscounts = avcp_get_imports_count();
error_log($itemscounts . '$items ' . print_r($items,1));
function avcp_get_imports($DaRiga=0,$ARiga=10) {
    global $wpdb;
    if ($DaRiga==0 AND $ARiga==0)
        $Limite="";
    else
        $Limite=" Limit ".$DaRiga.",".$ARiga;
    return $wpdb->get_results("SELECT * FROM $wpdb->table_name_Import order by uploaded_date " . $Limite . " ;");
}

function avcp_get_imports_count() {
    global $wpdb;
    return $wpdb->get_var("SELECT COUNT(*)  FROM $wpdb->table_name_Import;");
}


?>
    <div id="loading-overlay">
        <div class="loading-icon"></div>
    </div>
<?php
//Qui inizia la sezione delle impostazioni
echo '

<div class="avcp-import-container">
	            <div class="avcp-import-row">
	            <div class="warning-msg" id="warning-msg" style="display: none">
  <i class="fa fa-warning"></i>
  Importazione in corso, si prega di attendere
</div>
		            <div class="avcp-import-left">
			            <div class="avcp-import-holder"> 
			                <h1>
					            <i class="avcp-import-icon-publish"></i>
					            Import Somme Liquidate
				            </h1>
				            <form method="post" id="avcp-import-form" class="avcp-clear" enctype="multipart/form-data">';

                    // Get the 'Profiles' post type
                    $args = array(
                        'post_type' => 'anac-xml-view',
                        'posts_per_page' => -1
                    );
                    $anac_posts = get_posts( $args );
                    $post_titles = [];
                    foreach($anac_posts as $rows){
                        if(!empty($rows->post_title))
                            $post_titles[$rows->ID] = $rows->post_title;
                    }
                    $options = '';
                    $options .= "<option value=\"\">-- Select Dataset --</option>\n";
                    foreach ($post_titles as $key => $value) {
                        $options .= "<option value=\"$key\">$value</option>\n";
                    }
                    $select = "<select required id=\"data-sets\" name=\"data-sets\" style='margin-left:20px;display: none'>\n$options\n</select>";

?>
    <script>
        function handleModality(modality) {
            if(modality && modality.value === 'Storico'){
                document.getElementById("data-sets").style.display = '';
            }else {
                document.getElementById("data-sets").style.display = 'none';
            }

        }

    </script>
                            <label class="allega-excel-label">Modality</label><br>
                            <input type="radio" id="corrente" name="modality" value="Corrente"
                                   onclick="handleModality(this);" checked>
                            <label for="corrente" style="margin-bottom: 0px">Corrente</label>
                            <input type="radio" id="storico" name="modality" value="Storico"
                                   onclick="handleModality(this);" >
                            <label for="storico" style="margin-bottom: 0px">Storico</label>
                            <?php echo $select?><br>
                            <label class="allega-excel-label">Allega Excel</label>
                            <input type="file" id="avcp-excel" name="avcp-excel" required accept="*.xlsx" class="avcp-excel-input"/>
                            <br/>
                            <div class="swal-button-container">
                                <button class="import_avcp_excel" id="import_avcp_excel">Importa</button>
                            </div>
				            <?php //include AVCP_IMPORT_PATH . '/import-buttons.php'; ?>
				            <?php echo '</form>
			            </div> 
			        </div>
			    </div>
			    <br/>
			    <table class="widefat">
                    <thead>
                        <tr>
                           <th class="row-title">Modalit&#224;</th>
                              <th class="row-title">Dataset</th>
                            <th class="row-title">Nome File</th>
                            <th>Data Caricamento</th>
                            <th>Utente</th>
                            <th>Totale righe</th>
                            <th>Numero righe importate</th>
                             <th>Numero righe scartate</th>
                        </tr>
                    </thead>
                    <tbody>';
				    $a = '';
                    foreach ($items as $item) {
                        if ($a == '') {
                            $a = ' class="alternate"';
                        } else {
                            $a = '';
                        }
                        echo '<tr' . $a . '>';
                        echo '<td class="row-title"><label for="tablecell">' . $item->modality . '</label></td>';
                        echo '<td class="row-title"><label for="tablecell">' . $item->dataset . '</label></td>';
                        echo '<td class="row-title"><label for="tablecell">' . $item->file_name . '</label></td>';

                        $uploaded_date = '';
                        if($item->uploaded_date){
                            $d = DateTime::createFromFormat("Y-m-d H:i:s", $item->uploaded_date);
                            $uploaded_date = $d->format("d/m/Y H:i:s");
                        }
                        echo '<td class="row-title"><label for="tablecell">' . $uploaded_date . '</label></td>';
                        echo '<td>' . $item->uploaded_by . '</td>';
                        echo '<td>' . $item->total_rows . '</td>';
                        echo '<td>' . $item->total_imported . '</td>';
                        if(!is_null($item->failed_imported) && $item->failed_imported > 0)
                            echo '<td><a class ="failed_import" href="#" data-id="' . $item->Id .'">' . $item->failed_imported . '</a></td>';
                        else
                            echo '<td>' . $item->failed_imported . '</td>';
                        echo '  </tr>';
                    }
				    echo '</tbody>        
                </table>';

//                if ( $_REQUEST ) {
//                    $_REQUEST = array_map( 'sanitize', $_REQUEST );
//                }
                echo '<div class="clearfix"></div>';

                if ( isset( $itemscounts ) && $itemscounts > $per_pages ) {
                    $query = [];
                    foreach ( $_REQUEST as $k => $v ) {

                        if ( $k != "Pag") {
                            if (is_array($v)) {
                                if (count($v)) {
                                    $query[] = implode('&amp;', array_map(function ($i) use ($k) {
                                        return  "$k%5B%5D=$i";
                                    }, $v));
                                }
                            } else {
                                if ( $k == "page" and empty($v)) {
                                    $v = 'anac_import_somme_liquidate';
                                }
                                $query[] = $k . '=' . trim($v);
                            }
                        }
                    }
                    $Para = implode('&amp;', $query);
                    if ( $Para == '' ) {
                        $Para = "?Pag=";
                    } else {
                        $Para = "?" . $Para . "&amp;Pag=";
                    }
                    $Npag = (int) ( $itemscounts / $per_pages );
                    if ( $itemscounts % $per_pages > 0 ) {
                        $Npag ++;
                    } ?>
                    <div class="tablenav" style="float:right;" id="risultati">
                        <div class="tablenav-pages">
                            <p>Pagine
                                <?php
                                if ( isset( $_REQUEST['Pag'] ) and $_REQUEST['Pag'] > 1 ) {
                                    $Pagcur = $_REQUEST['Pag'];
                                    $PagPre = $Pagcur - 1;
                                    ?>
                                    &nbsp;<a href="<?php echo $Para ?>1" class="page-numbers numero-pagina"
                                             title="Vai alla prima pagina">&laquo;</a>
                                    &nbsp;<a href="<?php echo $Para . $PagPre ?>" class="page-numbers numero-pagina"
                                             title="Vai alla pagina precedente">&lsaquo;</a>
                                    <?php
                                } else {
                                    $Pagcur = 1;
                                    ?>
                                    &nbsp;<span class="page-numbers current"
                                                title="Sei gi&agrave; nella prima pagina">&laquo;</span>
                                    &nbsp;<span class="page-numbers current"
                                                title="Sei gi&agrave; nella prima pagina">&lsaquo;</span>
                                    <?php
                                }
                                ?>
                                &nbsp;<span class="page-numbers current"><?php echo $Pagcur . '/' . $Npag ?></span>
                                <?php
                                $PagSuc = $Pagcur + 1;
                                if ( $PagSuc <= $Npag ) {
                                    ?>
                                    &nbsp;<a href="<?php echo $Para . $PagSuc ?>" class="page-numbers numero-pagina"
                                             title="Vai alla pagina successiva">&rsaquo;</a>&nbsp;<a
                                            href="<?php echo $Para . $Npag ?>" class="page-numbers numero-pagina"
                                            title="Vai all\'ultima pagina">&raquo;</a>
                                    <?php
                                } else {
                                    ?>
                                    &nbsp;<span class="page-numbers current"
                                                title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>&nbsp;<span
                                            class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>'
                                    <?php
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                        <?php
                }
                ?>

<?php
                echo '</div>';


<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

require 'lib/vendor/autoload.php';

class AVCPExcelUpdate {
    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    private $spreadsheet;

    private $filename;

    private $modality;

    private $selectedDataSet;

    public function __construct() {
        $this->initFile();
    }
    private function initFile() {
        error_log('initFile ' . print_r($_REQUEST,1) . '>>>> ' . print_r($_FILES,1));
        if(isset($_REQUEST) and isset($_REQUEST['modality'])){
            $this->modality = $_REQUEST['modality'];
            $this->selectedDataSet = $_REQUEST['data-sets'];
        }
        error_log('modality ' . print_r($this->modality ,1));
        if (isset($_FILES['avcp-excel']['tmp_name'])) {
            $this->filename = $_FILES['avcp-excel']['name'];
            $inputFileName = $_FILES['avcp-excel']['tmp_name'];
            $inputFileType = ltrim(strstr($this->filename, '.'), '.');
            if(  $inputFileType === 'xlsx')
                $inputFileType = 'Xlsx';
            $reader = IOFactory::createReader($inputFileType);
            $reader->setReadEmptyCells(false);
            $this->spreadsheet = $reader->load($inputFileName);
            if ($this->spreadsheet->getActiveSheet()->getHighestRow() < 2) {
                throw new \Exception('excel file does not have data');
            }
        }
    }

    public function import() {
        try {
            $data = $this->getData();
            $report = $this->processData($data);
        } catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
            return wp_send_json_error([
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function getData() {
        $rows = $this->spreadsheet->getActiveSheet()->getRowIterator();

        // firstRow - label
        $labels = [];
        $firstRow = true;
        $data = [];
        foreach ($rows as $rowNum => $row) {
            foreach ($row->getCellIterator() as $col => $cell) {
                if ($firstRow) {
                    $labels[$col] = strtolower($cell->getValue());
                    $data[$labels[$col]] = [];
                } else {

                    if(\PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)){
                        $cellDataType = $cell->getDataType();
                        $cellFormat = 'm/d/Y';
                        if ($cellDataType=='s'){
                            $cellFormat = 'd/m/Y';
                        }
                        $data[$labels[$col]][$rowNum]=\DateTime::createFromFormat($cellFormat, $cell->getFormattedValue());
                    }else {
                        $data[$labels[$col]][$rowNum] = $cell->getFormattedValue();
                    }
                }
            }
            if ($firstRow) {
                $firstRow = false;
            }
        }
        return $data;
    }

    /**
     * @return array
     */
    private function processData( array $data ) {
        global $wpdb;
        $errors = [];
        $report = [
            'row_errors' => []
        ];
        $total_rows = 0;
        $imported_rows = 0;
        $failed_rows = 0;
        $gare_xml = null;
        $xmlDataArray = [];
        $cigXmlArray = [];
        $dataSetValue = null;
        if(isset($this->modality) and $this->modality === 'Storico'){
            $selectedPost = get_post($this->selectedDataSet);
            $dataSetValue = $selectedPost->post_title;
            $selectedXml = $selectedPost->post_content;
            $selectedXml = str_replace('&','&amp;',$selectedXml);
            $selectedXml = preg_replace('/(\x{0004}(?:\x{201A}|\x{FFFD})(?:\x{0003}|\x{0004}).)/u', '', $selectedXml);
            $regex = '/(
            [\xC0-\xC1] # Invalid UTF-8 Bytes
            | [\xF5-\xFF] # Invalid UTF-8 Bytes
            | \xE0[\x80-\x9F] # Overlong encoding of prior code point
            | \xF0[\x80-\x8F] # Overlong encoding of prior code point
            | [\xC2-\xDF](?![\x80-\xBF]) # Invalid UTF-8 Sequence Start
            | [\xE0-\xEF](?![\x80-\xBF]{2}) # Invalid UTF-8 Sequence Start
            | [\xF0-\xF4](?![\x80-\xBF]{3}) # Invalid UTF-8 Sequence Start
            | (?<=[\x0-\x7F\xF5-\xFF])[\x80-\xBF] # Invalid UTF-8 Sequence Middle
            | (?<![\xC2-\xDF]|[\xE0-\xEF]|[\xE0-\xEF][\x80-\xBF]|[\xF0-\xF4]|[\xF0-\xF4][\x80-\xBF]|[\xF0-\xF4][\x80-\xBF]{2})[\x80-\xBF] # Overlong Sequence
            | (?<=[\xE0-\xEF])[\x80-\xBF](?![\x80-\xBF]) # Short 3 byte sequence
            | (?<=[\xF0-\xF4])[\x80-\xBF](?![\x80-\xBF]{2}) # Short 4 byte sequence
            | (?<=[\xF0-\xF4][\x80-\xBF])[\x80-\xBF](?![\x80-\xBF]) # Short 4 byte sequence (2)
        )/x';
            $selectedXml = preg_replace($regex, '', $selectedXml);
            $selectedXml = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $selectedXml);
            $gare_xml = new SimpleXMLElement($selectedXml);
            $XmlData=$gare_xml->xpath('//lotto');
            foreach ($XmlData as $lotto) {
                if(isset($lotto->cig)){
                    $xmlDataArray["'" . strtoupper($lotto->cig) . "'"] = $lotto;
                }
            }
            error_log('$xmlDataArray ' . print_r($xmlDataArray,1));
        }

        foreach ($data['codice cig'] as $key => $cig) {
            $record_year = '';
            $data_mand = $data['data mand.'][$key];
            error_log('Date is $data_mand' . print_r($data_mand,1));
            if(!isset($this->modality) or $this->modality === 'Corrente'){
                if(!empty($cig)){
                    $total_rows++;
                    error_log(print_r($key,1) . '- processData -' . print_r($cig,1));
                    $post_records = $wpdb->get_results( "select post_id,meta_value from $wpdb->postmeta 
			where meta_key='avcp_cig' and UPPER(meta_value) = '" . strtoupper($cig) . "' order by post_id DESC", ARRAY_A );

                    if(count($post_records) > 0){
                        $importo = $data['importo'][$key];
                        //   $ddmmyyyy = str_replace('/', '-', $data_mand);
                        // $transformedDate = date("d-m-Y", strtotime($ddmmyyyy));
                        $transformedDate =  $data_mand->format('d-m-Y');
                        error_log('    $transformedDate ' . $transformedDate);
                        if(empty($data_mand)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "DATA MAND. vuota");
                            $report['row_errors'][$key] = "DATA MAND. vuota";
                        }else if(empty($importo)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "IMPORTO vuoto");
                            $report['row_errors'][$key] = "IMPORTO vuoto";
                        }else if(!$this->isNumeric($importo)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato IMPORTO non valido");
                            $report['row_errors'][$key] = "formato IMPORTO non valido";
                        }else if ($this->validateAVCPDate($transformedDate,'d-m-Y')) {
                            error_log( 'data_mand : ' . print_r($data_mand,1));
                            $record_year =  $data_mand->format('Y');
                            error_log( $importo. ' - record_year -' . print_r($record_year,1));
                            if(!empty($record_year) && $record_year != 1970){
                                $imported_rows++;
                                $post_id = $post_records[0]['post_id'];
                                $post_year_records = $wpdb->get_results( "select meta_value from $wpdb->postmeta 
			                    where meta_key='avcp_s_l_" . trim($record_year) ."' and post_id = " . $post_id , ARRAY_A );
                                error_log('$post_year_records '  . print_r($post_year_records,1));
                                if(count($post_year_records) > 0){
                                    $meta_value = $post_year_records[0]['meta_value'];
                                    $import_value = 0.0;
                                    if(!empty($meta_value)){
                                        $import_value =  floatval($meta_value);
                                    }
                                    $import_value = $import_value +  floatval( str_replace(",","",trim($importo)));
                                    error_log( $import_value . ' : avcp_s_l_'.trim($record_year) .'update post id:' . print_r($post_id,1));
                                    update_field('avcp_s_l_'.trim($record_year), $import_value, $post_id);
                                }
                            }else {
                                $failed_rows++;
                                array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato data DATA MAND.  non valido");
                                $report['row_errors'][$key] = "formato data DATA MAND.  non valido";
                            }
                        }else {
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato data DATA MAND.  non valido");
                            $report['row_errors'][$key] = "formato data DATA MAND.  non valido";
                        }
                    }else {
                        $failed_rows++;
                        array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "CIG non trovato");
                        $report['row_errors'][$key] = "CIG non trovato";
                    }
                }
            }else {
                if(!empty($cig) and array_key_exists("'" . strtoupper($cig) . "'", $xmlDataArray)){
                    $total_rows++;
                    error_log(print_r($key,1) . '- processData(XML) -' . print_r($cig,1));
                    $xmlObject = $xmlDataArray["'" . strtoupper($cig) . "'"];
                    if(isset($xmlObject)){
                        error_log('xmlObject ' . print_r($xmlObject,1));

                        $importo = $data['importo'][$key];
                        $transformedDate =  $data_mand->format('d-m-Y');
                        error_log('    $transformedDate(XML) ' . $transformedDate);
                        if(empty($data_mand)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "DATA MAND. vuota");
                            $report['row_errors'][$key] = "DATA MAND. vuota";
                        }else if(empty($importo)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "IMPORTO vuoto");
                            $report['row_errors'][$key] = "IMPORTO vuoto";
                        }else if(!$this->isNumeric($importo)){
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato IMPORTO non valido");
                            $report['row_errors'][$key] = "formato IMPORTO non valido";
                        }else if ($this->validateAVCPDate($transformedDate,'d-m-Y')) {
                            error_log( 'data_mand(XML) : ' . print_r($data_mand,1));
                            $record_year =  $data_mand->format('Y');
                            error_log( $importo. ' - record_year -' . print_r($record_year,1));
                            if(!empty($record_year) && $record_year != 1970){
                                $imported_rows++;
                                if( array_key_exists("'" . strtoupper($cig) . "'", $cigXmlArray)){
                                    $liq = (double)$cigXmlArray["'" . strtoupper($cig) . "'"];
                                }else {
                                    $liq = (double)$xmlObject->importoSommeLiquidate;
                                }
                                if(!$liq)
                                    $liq = 0.0;
                                $import_value =  floatval(str_replace(",","",trim($liq)));
                                $import_value = $import_value +  floatval( str_replace(",","",trim($importo)));
                                error_log('import value (XML) : ' . $import_value);
                                $cigXmlArray["'" . strtoupper($cig) . "'"] = $import_value;

                            }else {
                                $failed_rows++;
                                array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato data DATA MAND.  non valido");
                                $report['row_errors'][$key] = "formato data DATA MAND.  non valido";
                            }
                        }else {
                            $failed_rows++;
                            array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "formato data DATA MAND.  non valido");
                            $report['row_errors'][$key] = "formato data DATA MAND.  non valido";
                        }


                    }else {
                        $failed_rows++;
                        array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "CIG non trovato");
                        $report['row_errors'][$key] = "CIG non trovato";
                    }
                }else {
                    $failed_rows++;
                    array_push($errors, $key . "|" .  $cig . "|" . $data_mand->format('d/m/Y') . "|" . "CIG non trovato");
                    $report['row_errors'][$key] = "CIG non trovato";
                }
            }
        }


        if(isset($this->modality) and $this->modality === 'Storico'){
            error_log('cig array ' . print_r($cigXmlArray,1));
            if(count($cigXmlArray) > 0){
                foreach ($gare_xml->xpath('//lotto/cig') as $cigNode) {
                    if(isset($cigNode) and array_key_exists("'" . strtoupper($cigNode) . "'", $cigXmlArray)){
                        $lotto_node = $cigNode->xpath("parent::*");
                        error_log( '$lotto_node:' . print_r($lotto_node,1));
                        if(count($lotto_node) > 0)
                            $lotto_node[0]->importoSommeLiquidate = $cigXmlArray["'" . strtoupper($cigNode) . "'"];
                    }
                }
            }
            if(isset($selectedPost)){
                $dom = new DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($gare_xml->asXML());
                $xml = new SimpleXMLElement($dom->saveXML());
                $selectedPost->post_content = $xml->asXML();
                wp_update_post( $selectedPost );
            }else {
                error_log( 'Post not set');
            }
        }
        error_log( 'report :' . print_r($report,1));
        error_log( '$errors :' . print_r($errors,1));
        $current_user = wp_get_current_user();

        $profile_user = get_user_meta( $current_user->ID );
       // $profile_user = get_user_to_edit( $current_user->ID );
        $firstName = '';
        $lastName = '';
        if(count($profile_user) > 0){
            if(count($profile_user['first_name']) > 0)
                $firstName = $profile_user['first_name'][0];
            if(count($profile_user['last_name']) > 0)
                $lastName = $profile_user['last_name'][0];
        }

        $ROOT_PATH = AVCP_ROOT_PATH;
        if(!file_exists($ROOT_PATH)){
            mkdir($ROOT_PATH, 0755, true);
        }

        if ( false === $wpdb->insert($wpdb->table_name_Import,array('file_name' => $this->filename,
                'uploaded_by' => $firstName . ' ' . $lastName,
                'total_rows' => $total_rows,
                'total_imported' => $imported_rows,
                'failed_imported' => $failed_rows,
                'modality' => $this->modality,
                'dataset' => $dataSetValue),
                array('%s',
                    '%s',
                    '%d',
                    '%d',
                    '%s',
                    '%s'))){

        }else {
            $text = "N.RIGA|CODICE CIG|DATA MAND|ERRORE\n";
            foreach($errors as $value)
            {
                $text .= $value."\n";
            }
            if(!empty($errors)){
                $folder_name = $wpdb->insert_id;
                mkdir($ROOT_PATH . DIRECTORY_SEPARATOR . $folder_name, 0755, TRUE);
                $fileLocation = $ROOT_PATH . DIRECTORY_SEPARATOR . $folder_name . DIRECTORY_SEPARATOR .
                    "avcp_error_report_" .  $wpdb->insert_id . date('_Y_m_d') . ".txt";
                $file = fopen($fileLocation,"w");

                fwrite($file, $text) or die("Could not write file!");
                fclose($file);
                $wpdb->update($wpdb->table_name_Import,
                    array('errors_report' => $fileLocation),
                    array( 'Id'   =>  $wpdb->insert_id),
                    array( '%s'),
                    array( '%d'));
            }
        }
    }

    private function validateAVCPDate($date, $format)
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    private function isNumeric($num) {
        $num = str_replace(",","",trim($num));
        return is_numeric($num);
    }
}

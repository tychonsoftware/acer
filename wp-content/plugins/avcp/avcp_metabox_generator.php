<?php

add_action( 'admin_menu' , function() {
    remove_meta_box( 'postcustom' , 'avcp' , 'normal' );
} );

add_action( 'add_meta_boxes', function() {
    add_meta_box( 'avcp_metabox', 'Dettagli', 'avcp_metabox_details', 'avcp' );
    add_meta_box( 'avcp_metabox_importi', 'Somme liquidate', 'avcp_metabox_details_money', 'avcp' );
} );

add_action('save_post', function($post_id) {
    if (array_key_exists('avcp_contraente', $_POST)) {
        update_post_meta(
            $post_id,
            'avcp_contraente',
            $_POST['avcp_contraente']
        );
    }
    if (array_key_exists('avcp_cig', $_POST)) {
        update_post_meta(
            $post_id,
            'avcp_cig',
            $_POST['avcp_cig']
        );
    }
    if (array_key_exists('avcp_aggiudicazione', $_POST)) {
        update_post_meta(
            $post_id,
            'avcp_aggiudicazione',
            $_POST['avcp_aggiudicazione']
        );
    }

    if (array_key_exists('avcp_numero_provvedimento', $_POST)) {
        update_post_meta(
            $post_id,
            'avcp_numero_provvedimento',
            $_POST['avcp_numero_provvedimento']
        );
    }
    $terms = get_terms( 'annirif', array('hide_empty' => 0) );
    foreach ( $terms as $term ) {
        if (array_key_exists('avcp_s_l_'.$term->name, $_POST)) {
            if ( $_POST['avcp_s_l_'.$term->name] > 0 || ( $term->name > date("Y", strtotime( $_POST['avcp_data_inizio'] )) && $term->name < date("Y", strtotime( $_POST['avcp_data_fine'] )) )) {
                wp_set_object_terms( $post_id, $term->name, 'annirif', true );
            }
            update_post_meta(
                $post_id,
                'avcp_s_l_'.$term->name,
                $_POST['avcp_s_l_'.$term->name]
            );
        }
    }
    if (array_key_exists('avcp_data_inizio', $_POST)) {
        if ( date("Y", strtotime( $_POST['avcp_data_inizio'] )) > 2012 && date("Y", strtotime( $_POST['avcp_data_inizio'] )) < 2030  ) {
            wp_set_object_terms( $post_id, date("Y", strtotime( $_POST['avcp_data_inizio'] ) ), 'annirif', true );
        } else {
            wp_die("Data di inizio invalida");
        }
        update_post_meta(
            $post_id,
            'avcp_data_inizio',
            $_POST['avcp_data_inizio']
        );
    }
    if (array_key_exists('avcp_data_fine', $_POST)) {
        if ( date("Y", strtotime( $_POST['avcp_data_fine'] )) > 2012 && date("Y", strtotime( $_POST['avcp_data_fine'] )) < 2030 ) {
            wp_set_object_terms( $post_id, date("Y", strtotime( $_POST['avcp_data_fine'] ) ), 'annirif', true );
        }
//        else {
//            wp_die("Data di fine invalida");
//        }
        update_post_meta(
            $post_id,
            'avcp_data_fine',
            $_POST['avcp_data_fine']
        );
    }

    if (array_key_exists('avcp_data_provvedimento', $_POST)) {
        if ( date("Y", strtotime( $_POST['avcp_data_provvedimento'] )) > 2012 && date("Y", strtotime( $_POST['avcp_data_provvedimento'] )) < 2030 ) {
            wp_set_object_terms( $post_id, date("Y", strtotime( $_POST['avcp_data_provvedimento'] ) ), 'annirif', true );
        } else {
            wp_die("Data provvedimento");
        }
        update_post_meta(
            $post_id,
            'avcp_data_provvedimento',
            $_POST['avcp_data_provvedimento']
        );
    }

    if (array_key_exists('avcp_responsabile_provvedimento', $_POST)) {
        update_post_meta(
            $post_id,
            'avcp_responsabile_provvedimento',
            $_POST['avcp_responsabile_provvedimento']
        );
    }
});

function avcp_metabox_details( $post ) {
    ?>
    <div class="hcf_box">
        <script>
            function numbers_only(myfield, e, int, float) {

                var key;
                var keychar;
                var value = myfield.value;
                var isCtrl = false;

                if (window.event) {
                    key = window.event.keyCode;
                } else if (e) {
                    key = e.which;
                    isCtrl = e.ctrlKey;
                } else {
                    return true;
                }
                keychar = String.fromCharCode(key);

                // control keys
                if ((key == null)
                    || (key == 0)
                    || (key == 8)
                    || (key == 9)
                    || (key == 13)
                    || (key == 27)
                    || (isCtrl && ((key == 120) || (key == 97) || (key == 99) || (key == 118)))) {
                    return true;
                } else if ((("0123456789").indexOf(keychar) > -1)) {
                    if (value.indexOf(".") != -1
                        && (typeof myfield.selectionStart == "number"
                            && myfield.selectionStart > value.indexOf(".") || typeof myfield.selectionStart != "number")
                        && value.indexOf(".") <= value.length - (float + 1)) {
                        return false;
                    } else if (value.indexOf(",") != -1
                        && (typeof myfield.selectionStart == "number"
                            && myfield.selectionStart > value.indexOf(",") || typeof myfield.selectionStart != "number")
                        && value.indexOf(",") <= value.length - (float + 1)) {
                        return false;
                    } else {
                        if (value.length >= int && value.indexOf(",") == -1
                            && value.indexOf(".") == -1) {
                            return false;
                        } else if (value.indexOf(".") != -1
                            && value.substring(0, value.indexOf(".")).length >= int
                            && (typeof myfield.selectionStart == "number" && myfield.selectionStart <= value
                                .indexOf("."))) {
                            return false;
                        } else if (value.indexOf(",") != -1
                            && value.substring(0, value.indexOf(",")).length >= int
                            && (typeof myfield.selectionStart == "number" && myfield.selectionStart <= value
                                .indexOf(","))) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else if (((keychar == ".")) && float > 0) {
                    if (value.indexOf(".") != -1 || value.indexOf(",") != -1) {
                        return false;
                    } else if (value.length == 0) {
                        return false;
                    } else {
                        return true;
                    }
                } else
                    return false;
            }

            function check_numbers(field, int, float) {
                var valueStr = field.value;
                if (!isNaN(valueStr.replace(",", "."))
                    || !isNaN(valueStr.replace(".", ","))) {
                    if (valueStr.indexOf(".") != -1
                        && valueStr.indexOf(".") < valueStr.length - (float + 1)
                        && valueStr.substring(0, valueStr.indexOf(".")).length <= int) {
                        if (isNaN(valueStr)) {
                            valueStr = valueStr.replace(".", ",");
                        }
                        var value = parseFloat(valueStr);
                        field.value = value.toFixed(float);
                        return;
                    } else if (valueStr.indexOf(",") != -1
                        && valueStr.indexOf(",") < valueStr.length - (float + 1)
                        && valueStr.substring(0, valueStr.indexOf(",")).length <= int) {
                        if (isNaN(valueStr)) {
                            valueStr = valueStr.replace(",", ".");
                        }
                        var value = parseFloat(valueStr);
                        field.value = value.toFixed(float);
                        return;
                    } else if (valueStr.length > int && valueStr.indexOf(",") == -1
                        && valueStr.indexOf(".") == -1)

                    {
                        field.value = "";
                        return;
                    } else if ((valueStr.indexOf(".") != -1 && valueStr.substring(0,
                        valueStr.indexOf(".")).length > int)
                        || (valueStr.indexOf(",") != -1 && valueStr.substring(0,
                            valueStr.indexOf(",")).length > int)) {
                        field.value = "";
                        return;
                    }

                } else if (field.value == "-") {
                    return;
                } else {
                    field.value = "";
                }
            }

        </script>
        <style scoped>
            .hcf_box{
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
            }
            .hcf_field{
                display: contents;
            }
        </style>
        <p class="meta-options hcf_field">
            <label for="avcp_cig">Codice CIG (10 caratteri)*</label>
            <input id="avcp_cig" type="text" name="avcp_cig" minlength="10" maxlength="10" value="<?php echo ( get_post_meta($post->ID, 'avcp_cig', true) ? get_post_meta($post->ID, 'avcp_cig', true) : '0000000000'); ?>">
        </p>
        <p class="meta-options hcf_field">
            <label for="avcp_contraente">Scelta contraente*</label>
            <select name="avcp_contraente" id="avcp_contraente" class="postbox">
                <option value="">Seleziona...</option>
                <?php
                $contraente = get_post_meta($post->ID, 'avcp_contraente', true);
                $tipi_contraente = array(
                    array('01-PROCEDURA APERTA','1. Procedura Aperta'),
                    array('02-PROCEDURA RISTRETTA','2. Procedura Ristretta'),
                    array('03-PROCEDURA NEGOZIATA PREVIA PUBBLICAZIONE','3. Procedura negoziata previa pubblicazione'),
                    array('04-PROCEDURA NEGOZIATA SENZA PREVIA PUBBLICAZIONE','4. Procedura negoziata senza previa pubblicazione'),
                    array('05-DIALOGO COMPETITIVO','5. Dialogo Competitivo'),
                    array('06-PROCEDURA NEGOZIATA SENZA PREVIA INDIZIONE DI GARA (SETTORI SPECIALI)','6. Procedura negoziata senza previa indizione di gara (settori speciali)'),
                    array('07-SISTEMA DINAMICO DI ACQUISIZIONE','7. Sistema dinamico di acquisizione'),
                    array('08-AFFIDAMENTO IN ECONOMIA - COTTIMO FIDUCIARIO','8. Affidamento in economia - cottimo fiduciario'),
                    array('14-PROCEDURA SELETTIVA EX ART 238 C.7, D.LGS. 163/2006','14. Procedura selettiva ex art. 238 C.7 D.LGS. 163/2006'),
                    array('17-AFFIDAMENTO DIRETTO EX ART. 5 DELLA LEGGE 381/91','17. Affidamento diretto ex art. 5 legge 381/91'),
                    array('21-PROCEDURA RISTRETTA DERIVANTE DA AVVISI CON CUI SI INDICE LA GARA','21. Procedura ristretta derivante da avvisi con cui si indice la gara'),
                    array('22-PROCEDURA NEGOZIATA CON PREVIA INDIZIONE DI GARA (SETTORI SPECIALI)','22. Procedura negoziata con previa indizione di gara (settori speciali)'),
                    array('23-AFFIDAMENTO DIRETTO','23. Affidamento diretto'),
                    array('24-AFFIDAMENTO DIRETTO A SOCIETA&apos; IN HOUSE','24. Affidamento diretto a Società in-house'),
                    array('25-AFFIDAMENTO DIRETTO A SOCIETA&apos; RAGGRUPPATE/CONSORZIATE O CONTROLLATE NELLE CONCESSIONI E NEI PARTENARIATI','25. Affidamento diretto a Società raggruppate/consorziate o controllate nelle concessioni e nei partenariati'),
                    array('26-AFFIDAMENTO DIRETTO IN ADESIONE AD ACCORDO QUADRO/CONVENZIONE','26. Affidamento diretto in adesione ad accordo quadro/convenzione'),
                    array('27-CONFRONTO COMPETITIVO IN ADESIONE AD ACCORDO QUADRO/CONVENZIONE','27. Confronto competitivo in adesione ad accordo quadro/convenzione'),
                    array('28-PROCEDURA AI SENSI DEI REGOLAMENTI DEGLI ORGANI COSTITUZIONALI','28. Procedura ai sensi dei regolamenti degli organi costituzionali'),

                    array( '29-PROCEDURA RISTRETTA SEMPLIFICATA', '29. Procedura ristretta semplificata'),
                    array( '30-PROCEDURA DERIVANTE DA LEGGE REGIONALE', '30. Procedura derivante da legge regionale'),
                    array( '31-AFFIDAMENTO DIRETTO PER VARIANTE SUPERIORE AL 20% DELL\'IMPORTO CONTRATTUALE', '31. Affidamento diretto per variante superiore al 20% dell\'importo contrattuale'),
                    array( '32-AFFIDAMENTO RISERVATO', '32. Affidamento riservato'),
                    array( '33-PROCEDURA NEGOZIATA PER AFFIDAMENTI SOTTO SOGLIA', '33. Procedura negoziata per affidamenti sotto soglia'),
                    array( '34-PROCEDURA ART.16 COMMA 2-BIS DPR 380/2001 PER OPERE URBANIZZAZIONE A SCOMPUTO PRIMARIE SOTTO SOGLIA COMUNITARIA', '34. Procedura art.16 comma 2-bis dpr 380/2001 per opere urbanizzazione a scomputo primarie sotto soglia comunitaria'),
                    array( '35-PARTERNARIATO PER L’INNOVAZIONE', '35. Parternariato per l’innovazione'),
                    array( '36-AFFIDAMENTO DIRETTO PER LAVORI, SERVIZI O FORNITURE SUPPLEMENTARI', '36. Affidamento diretto per lavori, servizi o forniture supplementari'),
                    array( '37-PROCEDURA COMPETITIVA CON NEGOZIAZIONE', '37. Procedura competitiva con negoziazione'),
                    array( '38-PROCEDURA DISCIPLINATA DA REGOLAMENTO INTERNO PER SETTORI SPECIALI', '38. Procedura disciplinata da regolamento interno per settori speciali')
                );
                foreach ( $tipi_contraente as $tc ) {
                    echo '<option value="'.$tc[0].'" '.selected( $contraente, $tc[0] ) .'>'.$tc[1].'</option>';
                }
                ?>
            </select>
        </p>
        <p class="meta-options hcf_field">
            <label for="avcp_aggiudicazione">Importo aggiudicazione*</label>
            <input id="avcp_aggiudicazione" type="text" name="avcp_aggiudicazione"
                   value="<?php echo get_post_meta($post->ID, 'avcp_aggiudicazione', true); ?>"
                   onkeypress="return numbers_only(this, event, 10, 2)"
                   onkeyup="check_numbers(this, 10, 2);">
        </p>
        <p class="meta-options hcf_field" style="width:40%;">
            <label for="avcp_data_inizio">Data inizio*</label>
            <input id="avcp_data_inizio" type="date" name="avcp_data_inizio" value="<?php echo ( get_post_meta($post->ID, 'avcp_data_inizio', true) ? date("Y-m-d", strtotime( get_post_meta($post->ID, 'avcp_data_inizio', true) ) ) : '') ?>">
        </p>
        <p class="meta-options hcf_field">
            <label for="avcp_data_fine">Data fine</label>
            <input id="avcp_data_fine" type="date" name="avcp_data_fine" value="<?php echo ( get_post_meta($post->ID, 'avcp_data_fine', true) ? date("Y-m-d", strtotime( get_post_meta($post->ID, 'avcp_data_fine', true) ) ) : '') ?>">
        </p>
        <p class="meta-options hcf_field">
            <label for="avcp_numero_provvedimento">Numero provvedimento*</label>
            <input id="avcp_numero_provvedimento" type="text" name="avcp_numero_provvedimento"
                   value="<?php echo get_post_meta($post->ID, 'avcp_numero_provvedimento', true); ?>">
        </p>
        <p class="meta-options hcf_field">
            <label for="avcp_data_provvedimento">Data provvedimento*</label>
            <input id="avcp_data_provvedimento" type="date" name="avcp_data_provvedimento" value="<?php echo ( get_post_meta($post->ID, 'avcp_data_provvedimento', true) ? date("Y-m-d", strtotime( get_post_meta($post->ID, 'avcp_data_provvedimento', true) ) ) : '') ?>">
        </p>

        <p class="meta-options hcf_field">
            <label for="avcp_responsabile_provvedimento">Responsabile provvedimento</label>
            <select name="avcp_responsabile_provvedimento" id="avcp_responsabile_provvedimento" class="postbox">
                <option value="">Seleziona...</option>
                <?php
                $responsabile_provvedimento = get_post_meta($post->ID, 'avcp_responsabile_provvedimento', true);

                $args = array(
                    'meta_key' => 'last_name',
                    'orderby' => 'meta_value',
                    'order'   => 'ASC'
                );
                $users = get_users( $args);
                $avcp_dataset_roles = get_option('avcp_dataset_roles');
                error_log("avcp_dataset_roles " . print_r($avcp_dataset_roles,1));
                foreach ( $users as $user ) {
                    if($avcp_dataset_roles && !empty($avcp_dataset_roles)){
                        if(!in_array( $avcp_dataset_roles, (array) $user->roles )){
                            continue;
                        }
                    }
                    $user_last_name = get_user_meta( $user->ID, 'last_name', true );
                    $user_first_name = get_user_meta( $user->ID, 'first_name', true );
                    $option_value = '';
                    if((isset($user_last_name) && trim($user_last_name) !== '')){
                        $option_value = $user_last_name;
                    }
                    if((isset($user_first_name) && trim($user_first_name) !== '')){
                        $option_value .= ' ' . $user_first_name;
                    }

                    if((!isset($option_value) || trim($option_value) === '')){
                        $option_value = get_user_meta( $user->ID, 'nickname', true );
                    }
                    $option_value = trim($option_value);

                    echo '<option value="'.$option_value.'" '.selected( $responsabile_provvedimento, $option_value ) .'>'.$option_value.'</option>';
                }
                ?>
            </select>
        </p>
    </div>
    <?php
}

function avcp_metabox_details_money( $post ) {
    $terms = get_terms( 'annirif', array('hide_empty' => 0) );

    echo '<p>Formato da utilizzare: <b>1234.67</b></p>';
    $fields = array();
    foreach ( $terms as $term ) {
        if ( !get_post_meta($post->ID, 'avcp_s_l_'.$term->name, true) || get_post_meta($post->ID, 'avcp_s_l_'.$term->name, true) == '0.00' ) {
            $val = '';
        } else {
            $val = get_post_meta($post->ID, 'avcp_s_l_'.$term->name, true);
        }
        echo '<div style="width:30%;float:left;margin:20px 0;">'.$term->name.' ';
        echo '<input id="avcp_s_l_'.$term->name.'" onchange="formattaimporto(\'#avcp_s_l_'.$term->name.'\');updateLiqTot();" placeholder="0.00" type="text" name="avcp_s_l_'.$term->name.'" id="avcp_s_l_'.$term->name.'" value="'.$val.'">';
        echo '</div>';
        $fields[] = '"avcp_s_l_'.$term->name.'"';
    }
    echo '<div class="clear"></div>';
    echo '<p style="text-align:center;font-size:1.3em;">TOTALE: € <span style="font-weight:bold;" id="slTot">0.00</span></p>';
    foreach ( $terms as $term )

        echo '<script>
  updateLiqTot();
  function updateLiqTot() {
    var fields = ['.implode(", ", $fields).'];
    var totale = 0;
    fields.forEach(function(entry) {
      totale += +document.getElementById( entry ).value;
    });
    document.getElementById("slTot").textContent= totale.toFixed(2);
  }
  </script>
  ';
    echo '<div class="clear"></div>';
}

function avcp_pages_inner_custom_box3( $post ) { //Inizializzazione Metabox 2, senza api bainternet

    echo '<div>';
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'pages_noncename' );

    // The actual fields for data entry
    echo 'Le ditte inserite nel riquadro "Ditte partecipanti" compariranno qui solo dopo avere aggiornato/pubblicato questa gara.';
    $dittepartecipanti = get_the_terms( $post->ID, 'ditte' );

    $cats = get_post_meta($post->ID,'avcp_aggiudicatari',true);
    $parents_data = get_post_meta($post->ID,'avcp_aggiudicatari_parents',true);
    echo '<ul>';
    if(is_array($dittepartecipanti)) {
        foreach ($dittepartecipanti as $term) {
            $cterm = get_term_by('name',$term->name,'ditte');
            $cat_id = $cterm->term_id; //Prende l'id del termine
            $checked = (in_array($cat_id,(array)$cats)? ' checked="checked"': "");
            echo'<li id="cat-'.$cat_id.'">
            <input type="checkbox" name="avcp_aggiudicatari[]" id="'.$cat_id.'" value="'.$cat_id.'"'.$checked.' style="">
            <label for="'.$cat_id.'">'.__($term->name, 'pages_textdomain' ).'</label>
            <select name="avcp_aggiudicatari_parent_' .$cat_id.'"  id="avcp_aggiudicatari_parent_' .$cat_id.'" class="postbox" 
                style="margin-bottom: 0px;margin-left: 20px">
            <option value="">Seleziona capofila</option>';
            $selected_item = array_filter($parents_data, function($data) use ($term) {
                return $data->child_id == $term->term_id;
            });

            $selected_id = -1;
            if(count($selected_item) > 0){
                $selected_id = reset($selected_item)->id;
            }
            foreach ( $dittepartecipanti as $parent_term ) {
                if($parent_term->term_id ==  $term->term_id){
                    continue;
                }
                echo '<option value="'.$parent_term->term_id.'" ' .selected( $selected_id, $parent_term->term_id ) .'>'.$parent_term->name.'</option>';
            }
            echo '</select>
            </li>';
        }
    } else {
        echo '<code>Nessuna ditta partecipante collegata a questa gara.</code>';
    }
    echo '</ul><input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Pubblica/Aggiorna Gara" accesskey="p" />';
    echo '</div>';
}

add_action( 'add_meta_boxes', 'avcp_meta_box_add' );
function avcp_meta_box_add() {
    add_meta_box( 'anac-metabox-ditte', 'Ditte aggiudicatarie', 'avcp_pages_inner_custom_box3', 'avcp', 'normal', 'high' );
}

add_action( 'save_post_avcp', 'avcp_custom_save_post' );
function avcp_custom_save_post( $post_id ) {
    if ( isset($_POST['avcp_aggiudicatari']) ) {
        update_post_meta($post_id,'avcp_aggiudicatari',$_POST['avcp_aggiudicatari']);
//        $parents = [];
//        foreach ( $_POST['avcp_aggiudicatari'] as $ditte ) {
//            $parent_element = 'avcp_aggiudicatari_parent_' . $ditte;
//            if ( isset($_POST[$parent_element]) && !empty($_POST[$parent_element])) {
//                $parent = new stdClass();
//                $parent->child_id = $ditte;
//                $parent->id = $_POST[$parent_element];
//                array_push($parents, $parent);
//            }
//        }
//        update_post_meta($post_id,'avcp_aggiudicatari_parents',$parents);
    }else {
        update_post_meta($post_id,'avcp_aggiudicatari',[]);
    }

    $parents = [];
    $dittepartecipanti = get_the_terms( $post_id, 'ditte' );
    foreach ($dittepartecipanti as $term) {
        $cterm = get_term_by('name',$term->name,'ditte');
        $cat_id = $cterm->term_id; //Prende l'id del termine
        $parent_element = 'avcp_aggiudicatari_parent_' . $cat_id;
        if ( isset($_POST[$parent_element]) && !empty($_POST[$parent_element])) {
            $parent = new stdClass();
            $parent->child_id = $cat_id;
            $parent->id = $_POST[$parent_element];
            array_push($parents, $parent);
        }
    }
    update_post_meta($post_id,'avcp_aggiudicatari_parents',$parents);

}


add_action('add_meta_boxes','avcp_add_meta_boxes',10,2);
function avcp_add_meta_boxes($post_type, $post) {
    ob_start();
}
add_action('dbx_post_sidebar','avcp_dbx_post_sidebar');
function avcp_dbx_post_sidebar() {
    $html = ob_get_clean();
    $html = str_replace('type="checkbox" name="tax_input[areesettori][]"','type="radio" name="tax_input[areesettori][]"',$html);
    echo $html;
}

?>

<?php
/**
 * Plugin Name: Ninja Forms - SOAP
 * Plugin URI: 
 * Description: Automatically send form submissions to external server.
 * Version: 100
 * Author: Custom
 * Author URI: 
 * License: 
 */

add_action( 'ninja_forms_soap', 'ninja_forms_soap_request' );

/*
 * not used
 * I think remove this
 * */
function ninja_forms_soap_request( $form_data ){
	global $wpdb;
	
	$table_name = "servmask_prefix_portal";
	$query = "SELECT * FROM $table_name ";
	$soap_logins = $wpdb->get_results($query);
	$soap_login = $soap_logins[0];

/*
	$credential = array(
		 'username' => $soap_login->soap_username,
		 'password' => $soap_login->soap_password,
	);
	$wsdl = $soap_login->soap_url;
	$soapClient = new SoapClient($wsdl);

	
	try {
		$loginStat = $soapClient->login($credential);
	} catch (SoapFault $fault) {
        print("ERROR: ".$fault->faultcode."-".$fault->faultstring);
		exit;
    }
	
	$logoutStat = $soapClient->logout();

	add_action( 'ninja_forms_after_submission', 'do_soap_request' );
*/
}

/*
 * not used
 * I think remove this
 * */
function do_soap_request( $form_data ){
	
	global $wpdb;
	
	$table_name = "servmask_prefix_portal";
	$query = "SELECT * FROM $table_name ";
	$soap_logins = $wpdb->get_results($query);
	$soap_login = $soap_logins[0];

	$credential = array(
		 'username' => $soap_login->soap_username,
		 'password' => $soap_login->soap_password,
	);
	$wsdl = $soap_login->soap_url;
	$soapClient = new SoapClient($wsdl);

	try {
		$loginStat = $soapClient->login($credential);
	} catch (SoapFault $fault) {
        print("ERROR: ".$fault->faultcode."-".$fault->faultstring);
    }
	
	var_dump($loginStat);

	$table_name = "servmask_prefix_documenttemplate";
	$query = "SELECT * FROM $table_name WHERE IsValid = 1 ";
	$docs_temp = $wpdb->get_results($query);
	$doc_temp = $docs_temp[1];

//	var_dump($docs_temp[1]);
/*
	$options = array(
		'p' => array(
			'Document' => array(
				 'Subject' => $doc_temp->Subject,
				 'Note' => $doc_temp->Name,
				 'BusinessUnitID' => $doc_temp->BuId,
				 'SourceID' => '3',
				 'CategoryID' => $doc_temp->ProtocolCategoryId,
				 'TitolarioID' => $doc_temp->TitolarioID,
				 'FascicleID' => $doc_temp->FascicleID,
				 'TypeID' => $doc_temp->TypeID,
				 'SenderUORs' => $doc_temp->SenderUOR,
				 'RecipientUORs' => $doc_temp->RecipientUOR,
				 'ShowCase' => '0',
				 'IsSubscribed' => '0',
				 'Inventory' => '0',
				 'IsPublic' => '0',
			),
			'ProtocolDocument' => '1',
			'Data' => 'dGVzdA==',
			'FileName' => 'pdf_dsddsoc.txt',
			'FileType' => '11',
			'IsSigned' => '0',
			'SignDate' => '04/02/2019',
			'CheckOriginality' => '0'
		)
	);
*/

	$options = array(
			'Document' => array(
				 'Subject' => 'TEST WS 1',
				 'Note' => 'TEST WS 1',
				 'BusinessUnitID' => '1',
				 'SourceID' => '3',
				 'CategoryID' => '44',
				 'TitolarioID' => '628',
				 'FascicleID' => '1',
				 'TypeID' => '9',
				 'SenderUORs' => '8',
				 'RecipientUORs' => '8',
				 'ShowCase' => '0',
				 'IsSubscribed' => '0',
				 'Inventory' => '0',
				 'IsPublic' => '0'
			),
			'ProtocolDocument' => '1',
			'Data' => 'dGVzdERvY1A=',
			'FileName' => 'pdafhjjd_doc3.pdf',
			'FileType' => '11',
			'IsSigned' => '0',
			'SignDate' => '04/02/2019',
			'CheckOriginality' => '0'
	);


	try {
		$saveFile = $soapClient->CreateDocumentFromFile($options);
	} catch (SoapFault $fault) {
        print("ERROR: ".$fault->faultcode."-".$fault->faultstring);
    }
	var_dump($saveFile);
	$logoutStat = $soapClient->logout();
//	var_dump($logoutStat);
		
/*    
        // Prepare SoapHeader parameters 
        $sh_param = array(
                    'Username'    =>    'admin', 
                    'Password'    =>    'password'); 
        $headers = new SoapHeader('http://212.237.40.30/wldemo/welodge/ws/webservices', 'UserCredentials', $sh_param); 
    
        // Prepare Soap Client 
        $soapClient->__setSoapHeaders(array($headers)); 
    
        // Setup the RemoteFunction parameters 
        $ap_param = array( 
                    'amount'     =>    $irow['total_price']); 
                    
        // Call RemoteFunction () 
        $error = 0; 
        try { 
            $info = $soapClient->__call("CreateDocumentFromFile", $options);
        } catch (SoapFault $fault) { 
            $error = 1; 
            print(" 
            alert('Sorry, blah returned the following ERROR: ".$fault->faultcode."-".$fault->faultstring.". We will now take you back to our home page.'); 
            window.location = 'main.php'; 
            "); 
        }
        
        if ($error == 0) {
            $auth_num = $info->RemoteFunctionResult; 
            
            if ($auth_num < 0) {
                // Setup the OtherRemoteFunction() parameters 
            } else {
                    // Record the transaction error in the database 
                
                // Kill the link to Soap 
                unset($soapClient);
            }
        }
*/

/*
//Data, connection, auth
        $dataFromTheForm = $_POST['fieldName']; // request data from the form
        $soapUrl = "https://connecting.website.com/soap.asmx?op=DoSomething"; // asmx URL of WSDL
        $soapUser = "username";  //  username
        $soapPassword = "password"; // password

        // xml post structure

        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <GetItemPrice xmlns="http://connecting.website.com/WSDL_Service">
                                  <PRICE>'.$dataFromTheForm.'</PRICE> 
                                </GetItemPrice >
                              </soap:Body>
                            </soap:Envelope>';

           $headers = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: http://connecting.website.com/WSDL_Service/GetPrice", 
                        "Content-length: ".strlen($xml_post_string),
                    ); //SOAPAction: your op URL

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            curl_close($ch);

            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

            // convertingc to XML
            $parser = simplexml_load_string($response2);
            // user $parser to get your data out of XML response and to display it.
*/
}
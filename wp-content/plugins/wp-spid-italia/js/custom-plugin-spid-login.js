(function ($) {
    'use strict';

    $(function () {
        var $ul = $('#spid-idp-list-small-root-post');
        var $listLi = $('#spid-idp-list-small-root-post > li.spid-idp-button-link');

        if (!$ul.length || !$listLi.length) {
            return;
        }

        $listLi.sort(function () {
            return parseInt( Math.random() * 3 ) - 1;
        }).prependTo($ul);
    });

})(jQuery);

[![WP compatibility](https://plugintests.com/plugins/wp-spid-italia/wp-badge.svg)](https://plugintests.com/plugins/wp-spid-italia/latest-report) [![PHP compatibility](https://plugintests.com/plugins/wp-spid-italia/php-badge.svg)](https://plugintests.com/plugins/wp-spid-italia/latest-report)
![SPID WP](https://raw.githubusercontent.com/WPGov/wp-spid-italia/asset/banner-1544x500.png)

Plugin WordPress per l'interfacciamento con il Sistema Pubblico di Identità Digitale (SPID)

* Repository: https://wordpress.org/plugins/wp-spid-italia/
* Segnalazioni GitHub: https://github.com/WPGov/wp-spid-italia/issues
* Pull requests: https://github.com/WPGov/wp-spid-italia/pulls
* Documentazione: https://github.com/WPGov/wp-spid-italia/wiki

### Credits
* **Marco Milesi**: sviluppatore e mantainer nell'ambito del progetto [WPGov.it - WordPress per la Pubblica Amministrazione](https://wpgov.it/)
* **Christian Ghellere, Andrea Smith**: beta testing

### Ringraziamenti
* **Paolo Bozzo**: sviluppo libreria Drupal-PASW
* **Nadia Caprotti**: condivisione know-how Drupal-PASW
* **Comune di Firenze**: sviluppo libreria SimpleSaml riadattata da Paolo
* **Italian Linux Society** per il contributo economico
* **Porte Aperte sul Web**

Copyright © 2017 Marco Milesi

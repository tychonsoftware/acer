<?php
/*
Plugin Name: WP SPID Italia
Description: SPID - Sistema Pubblico di Identità Digitale
Author: Marco Milesi
Version: 1.4
Author URI: http://www.marcomilesi.ml
*/

include( plugin_dir_path( __FILE__ ) . 'constants.php');
include( plugin_dir_path( __FILE__ ) . 'wp-spid-italia-custom-plugin.php');

register_activation_hook( __FILE__, function(){

    $dir = array(
        SPID__PERM_DIR,
        SPID__CONFIG_DIR,
        SPID__CERT_DIR,
        SPID__TEMP_DIR,
        SPID__DATA_DIR,
        SPID__LOG_DIR
    );

    foreach ($dir as $value) {
        if ( !is_dir( $value ) && !mkdir( $value, 0755, false)) {
            die('Errore durante la creazione della directory <b>'.$value.'</b>!<br>
            Verificare che la directory sia scrivibile, provare a riattivare il plugin o crearla manualmente.');
        }
    }

    if ( !file_exists( SPID__PERM_DIR . '/.htaccess' ) && !copy( SPID__LIB_DIR . '/.htaccess', SPID__PERM_DIR . '/.htaccess') ) {
        die('Cannot write .htaccess...');
    }

    if ( !file_exists( SPID__CONFIG_DIR . '/config.php' ) && !copy( SPID__LIB_DIR . '/config-templates-pasw/config.php', SPID__CONFIG_DIR . '/config.php') ) {
        die('Cannot write config.php...');
    }

    if ( !file_exists( SPID__CONFIG_DIR . '/authsources.php' ) && !copy( SPID__LIB_DIR . '/config-templates-pasw/authsources.php', SPID__CONFIG_DIR . '/authsources.php') ) {
        die('Cannot write authsources.php...');
    }
});

add_action( 'admin_menu', function() {
    add_submenu_page(
        'options-general.php',
        'SPID', 'SPID',
        'manage_options', 'spid_menu',
        function() { include( plugin_dir_path( __FILE__ ) . 'admin/settings.php'); spid_menu_func(); }
    );
} );

add_action( 'admin_init', function() {
    register_setting('spid_options', 'spid');
    $arrayatpv = get_plugin_data ( __FILE__ );
    $nuova_versione = $arrayatpv['Version'];
    if ( version_compare( get_option('spid_version'), $nuova_versione, '<')) {
        update_option( 'spid_version', $nuova_versione );
    }
});

add_action('wp_ajax_wp_spid_create_user', function () {
    error_log("wp_spid_create_user " . print_r($_POST, 1));
    $fakeEMail = trim($_POST["spidUserFakeEMail"]);
    $userEmail = trim($_POST["spidUserEmail"]);
    $user = get_user_by( 'email', $fakeEMail );
    $userCheck = get_user_by( 'email', $userEmail );
    if(!empty( $user ) && empty($userCheck)){
        wp_update_user([
            'ID' => $user->ID,
            'user_email' => $userEmail
        ]);
        error_log("User email Updated");
        delete_user_meta($user->ID, 'spid_email_type');
        $result['status'] = 'success';
        wp_send_json($result);
    }else {
        delete_user_meta($user->ID, 'spid_email_type');
        error_log("User empty/email Exist");
        $result['status'] = 'email Exist';
        wp_send_json($result);
    }
});
add_action('wp_ajax_wp_spid_cancel_fake_email', function () {
    error_log("wp_spid_cancel_fake_email " . print_r($_POST, 1));
    $userId = trim($_POST["spidUserId"]);
    $result['status'] = 'success';
    if(!empty( $userId )){
        if ( metadata_exists( 'user', $userId, 'spid_email_type' ) ) {
            delete_user_meta($userId, 'spid_email_type');
        }
    }
    wp_send_json($result);
});
add_action( 'init', function() {

    if (isset($_GET)) {
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'logout') {

                if ( !is_file (SPID__CERT_DIR.'/saml.pem') || !spid_option('enabled') ) {
                    return;
                }

                require_once( SPID__LIB_DIR . '/lib/_autoload.php');

                $auth = new SimpleSAML_Auth_Simple( 'default-sp' );

                if ( $auth->isAuthenticated() ) {
                    $auth->logout();
                }
            }
        }
    }

});

include( plugin_dir_path( __FILE__ ) . 'user.php');

add_filter( 'plugin_action_links_'.plugin_basename( __FILE__ ), function( $links ) {
    $settings_link = '<a href="options-general.php?page=spid_menu">Impostazioni</a>';
    array_push( $links, $settings_link );
    return $links;
} );

add_filter( 'login_message', function( $message ) {
    spidRedirect($message);
});

add_filter( 'spid-redirect', function( $message ) {
    spidRedirect($message);
});

function spidRedirect($message){
    if ( !is_file (SPID__CERT_DIR.'/saml.pem') || !spid_option('enabled') ) {
        return;
    }
    if ( isset($_GET['SimpleSAML_Auth_State_exceptionId']) ) {
        echo '<div id="login_error"><b>ERRORE</b>: Login SPID non riuscito: l\'utente ha negato il consenso all\'invio di dati al SP</div>';
        /*if($_SERVER['HTTP_HOST'] == 'www.acercampania.it'){
            header('location: https://www.acercampania.it/index.php/login?SimpleSAML_Auth_State_exceptionId');
            exit;
        }*/
    }

    if ( isset($_GET['spid']) && $_GET['spid'] == "nouser" ) {
        echo '<div id="login_error"><b>ERRORE</b>: non è stata trovata alcuna utenza associata all\'indirizzo email o codice fiscale di SPID</div>';
    }
    require_once( SPID__LIB_DIR . '/lib/_autoload.php');

    $auth = new SimpleSAML_Auth_Simple( 'default-sp' );
    $_simplesamlphp_auth_saml_attributes = $auth->getAttributes();

    if ( isset( $_GET['saml_login'] ) && $_GET['saml_login'] == 'spid' ) {
        if ((isset($_REQUEST['infocert_id']) && $_REQUEST['infocert_id'])) {
            $options['saml:idp'] = $_REQUEST['infocert_id'];
        } elseif ((isset($_REQUEST['poste_id']) && $_REQUEST['poste_id'])) {
            $options['saml:idp'] = $_REQUEST['poste_id'];
        } elseif ((isset($_REQUEST['tim_id']) && $_REQUEST['tim_id'])) {
            $options['saml:idp'] = $_REQUEST['tim_id'];
        } elseif ((isset($_REQUEST['sielte_id']) && $_REQUEST['sielte_id'])) {
            $options['saml:idp'] = $_REQUEST['sielte_id'];
        } elseif ((isset($_REQUEST['aruba_id']) && $_REQUEST['aruba_id'])) {
            $options['saml:idp'] = $_REQUEST['aruba_id'];
        } elseif ((isset($_REQUEST['namirial_id']) && $_REQUEST['namirial_id'])) {
            $options['saml:idp'] = $_REQUEST['namirial_id'];
        } elseif ((isset($_REQUEST['register_id']) && $_REQUEST['register_id'])) {
            $options['saml:idp'] = $_REQUEST['register_id'];
        }
        elseif ((isset($_REQUEST['intesa_id']) && $_REQUEST['intesa_id'])) {
            $options['saml:idp'] = $_REQUEST['intesa_id'];
        }
        elseif ((isset($_REQUEST['lepida_id']) && $_REQUEST['lepida_id'])) {
            $options['saml:idp'] = $_REQUEST['lepida_id'];
        }
        elseif ((isset($_REQUEST['umbria_id']) && $_REQUEST['umbria_id'])) {
            $options['saml:idp'] = $_REQUEST['umbria_id'];
        }
        elseif ((isset($_REQUEST['spidvalidator_id']) && $_REQUEST['spidvalidator_id'])) {
            $options['saml:idp'] = $_REQUEST['spidvalidator_id'];
        }
        elseif ((isset($_REQUEST['idptest_id']) && $_REQUEST['idptest_id'])) {
            $options['saml:idp'] = $_REQUEST['idptest_id'];
        }
        else {
            echo '<b>ERRORE</b>';
        }

        if ( is_user_logged_in() ) {
            wp_logout();
        }

        // $options['saml:AuthnContextClassRef'] = 'https://www.spid.gov.it/SpidL1';
        $options['samlp:RequestedAuthnContext'] = array("Comparison" => "minimum");
        $options['ErrorURL'] = wp_login_url();
        error_log("9bis:".$options['ErrorURL']);

        error_log("auth:" . print_r($auth,true));
        $auth->requireAuth( $options );
    }

    if ( $auth->isAuthenticated() ) {
        error_log("Authenticated");
        $attributes = $auth->getAttributes();
        $email = isset($attributes['email'][0])?$attributes['email'][0]:"";
        if($email==""){
            $email = isset($attributes['emailAddressPersonale'][0])?$attributes['emailAddressPersonale'][0]:"";
        }
        $lastName = isset($attributes['familyName'][0])?$attributes['familyName'][0]:"";
        if(empty($lastName)){
            $lastName = isset($attributes['cognome'][0])?$attributes['cognome'][0]:"";
        }
        $name = isset($attributes["name"][0])?$attributes["name"][0]:"";
        if(empty($name)){
            $name = isset($attributes["nome"][0])?$attributes["nome"][0]:"";
        }
        $fullName = $lastName." ".$name;
        $alreadyRegistered = true;
        $user = get_user_by( 'email', $email );
        $cf =  isset($attributes["fiscalNumber"][0])?$attributes["fiscalNumber"][0]:"";
        if(empty($cf)){
            $cf =  isset($attributes["CodiceFiscale"][0])?$attributes["CodiceFiscale"][0]:"";
        }
        $cf = str_replace( 'TINIT-', '', $cf);
        $fakeEmail = false;
        if ( empty( $user ) ) {
            $user = reset(
                get_users(
                    array(
                        'meta_key' => 'codice_fiscale',
                        'meta_value' => $cf,
                        'number' => 1,
                        'count_total' => false,
                    )
                )
            );

            if(empty($user) && empty($email) && !empty($cf)){
                error_log("Case for fake email");
                $email = trim($cf) . '@mail.it';
                $user = get_user_by( 'email', $email );
                $fakeEmail = true;
            }
            $enabledCreateUser = spid_option('enabledcreateusers');

            if(empty( $user ) && $enabledCreateUser!=false){
                $password = wp_generate_password();
                $userdata = array(
                    'user_login' => $email,
                    'user_email' => $email,
                    'user_pass' => $password,
                    'display_name' => $fullName,
                    'first_name' => $name,
                    'last_name' => $lastName
                );
                $user = wp_insert_user($userdata);
                $alreadyRegistered = false;
                $user = get_user_by( 'email', $email );
            }
        }

        if ( !is_wp_error( $user ) && !empty( $user )) {
            if($alreadyRegistered && (!empty($name) || !empty($lastName))){
                $display_name = trim($name . " " . $lastName);
                error_log('Display name to be set : ' . print_r($display_name,1));
                wp_update_user([
                    'ID' => $user->ID,
                    'first_name' => $name,
                    'last_name' => $lastName,
                    'display_name' => $display_name
                ]);
            }
            $expiry_date = date('Y-m-d H:i:s', strtotime('now +'. EXPIRE_TIME . ' minutes'));
            $manager = WP_Session_Tokens::get_instance( $user->ID );
            $token   = $manager->create( $expiry_date );
            global $wpdb;
            $prefix = $wpdb->prefix;
            $table_name = $prefix . 'login_tokens';
            $wpdb->query( "DELETE FROM $table_name WHERE user_id IN($user->ID)");
            $wpdb->insert($table_name, array(
                'auth_token' => $token,
                'auth_token_expiration' => $expiry_date,
                'user_id' => $user->ID
            ));

            UserFascicle::create_fascicle_object($user->ID);

            update_user_meta( $user->ID, 'spid_attributes', $attributes);
            update_user_meta( $user->ID, 'codice_fiscale', $cf);
            wp_clear_auth_cookie();
            wp_set_current_user ( $user->ID );
            wp_set_auth_cookie  ( $user->ID );
            if($fakeEmail){
                update_user_meta( $user->ID , 'spid_email_type', 'fake');
            }else if ( metadata_exists( 'user', $user->ID, 'spid_email_type' ) ) {
                delete_user_meta($user->ID, 'spid_email_type');
            }
            $url="https://servizidigitali.comune.terni.it/index.php/servizi-con-autenticazione/";
            wp_safe_redirect($url);
            exit();
            //wp_safe_redirect( home_url() );

        } else {
            $auth->logout( add_query_arg( 'spid', 'nouser', wp_login_url() ) );
            //$auth->logout( add_query_arg( 'spid', 'nouser', 'https://www.acercampania.it/index.php/login/' ) );
            exit();
        }
    }

    $plugin_dir = plugin_dir_url( __FILE__ );
    $spid_ico_circle_svg = $plugin_dir . '/img/spid-ico-circle-bb.svg';
    $spid_ico_circle_png = $plugin_dir . '/img/spid-ico-circle-bb.png';

    $spid_idp_infocert_svg =  $plugin_dir . '/img/spid-idp-infocertid.svg';
    $spid_idp_infocert_png = $plugin_dir . '/img/spid-idp-infocertid.png';

    $spid_idp_timid_svg = $plugin_dir . '/img/spid-idp-timid.svg';
    $spid_idp_timid_png = $plugin_dir . '/img/spid-idp-timid.png';

    $spid_idp_posteid_svg = $plugin_dir . '/img/spid-idp-posteid.svg';
    $spid_idp_posteid_png = $plugin_dir . '/img/spid-idp-posteid.png';

    $spid_idp_sielteid_svg = $plugin_dir . '/img/spid-idp-sielteid.svg';
    $spid_idp_sielteid_png = $plugin_dir . '/img/spid-idp-sielteid.png';

    $spid_idp_arubaid_svg = $plugin_dir . '/img/spid-idp-arubaid.svg';
    $spid_idp_arubaid_png = $plugin_dir . '/img/spid-idp-arubaid.png';

    $spid_idp_namirialid_svg = $plugin_dir . '/img/spid-idp-namirialid.svg';
    $spid_idp_namirialid_png = $plugin_dir . '/img/spid-idp-namirialid.png';

    $spid_idp_registerid_svg = $plugin_dir . '/img/spid-idp-spiditalia.svg';
    $spid_idp_registerid_png = $plugin_dir . '/img/spid-idp-spiditalia.png';

    $spid_idp_intesaid_svg = $plugin_dir . '/img/spid-idp-intesaid.svg';
    $spid_idp_intesaid_png = $plugin_dir . '/img/spid-idp-intesaid.png';

    $spid_idp_lepidaid_svg = $plugin_dir . '/img/spid-idp-lepidaid.svg';
    $spid_idp_lepidaid_png = $plugin_dir . '/img/spid-idp-lepidaid.png';

    $spid_idp_umbriaid_svg = $plugin_dir . '/img/spid-idp-umbriaid.svg';
    $spid_idp_umbriaid_png = $plugin_dir . '/img/spid-idp-umbriaid.png';

    $spid_idp_validatorid_svg = $plugin_dir . '/img/spid-idp-validatorid.svg';
    $spid_idp_validatorid_png = $plugin_dir . '/img/spid-idp-validatorid.png';
    $idptest_svg = $plugin_dir . '/img/idptest.svg';
    $idptest_png = $plugin_dir . '/img/idptest.png';

    $infocert_id = 'https://identity.infocert.it';
    $poste_id = 'https://posteid.poste.it';
    $tim_id = 'https://login.id.tim.it/affwebservices/public/saml2sso';
    $sielte_id = 'https://identity.sieltecloud.it';
    $aruba_id = 'https://loginspid.aruba.it';
    $namirial_id = 'https://idp.namirialtsp.com/idp';
    $register_id = 'https://spid.register.it';
    $intesa_id= 'https://spid.intesa.it';
    $lepida_id= 'https://www.lepida.it';
    $umbria_id= 'https://login.regione.umbria.it/gw/metadata';
    $spidvalidator_id= 'https://validator.spid.gov.it';
    $idptest_id='https://idptest.spid.gov.it';

    $formaction = $auth->getLoginURL();
    ?>
    <div style="font-size:17px"><strong>Per poter accedere ai servizi online del Comune è necessaria l’autenticazione al portale.</strong> <br> <br>
        <strong>Clicca sul pulsante sotto per accedere tramite le credenziali personali SPID</strong> <br></div>
    <form name="spid_idp_access" action="?saml_login=spid" method="post" style="text-align: center;background:none;box-shadow:none;margin: 10px 0;padding: 0;">
        <!--
            <a href="#" class="italia-it-button italia-it-button-size-m button-spid" spid-idp-button="#spid-idp-button-small-post" aria-haspopup="true" aria-expanded="false">
                <span class="italia-it-button-icon"><img src="<?php echo $spid_ico_circle_svg; ?>" onerror="this.src='<?php echo $spid_ico_circle_png; ?>'; this.onerror=null;" alt="" /></span>
                <span class="italia-it-button-text">Login SPID</span>
            </a>
			-->
        <button class="idp-button-idp-logo" name="umbria_id" type="submit" value="<?php echo $umbria_id; ?>" style="background-color:#0066cc;color:white;width:300px;height:46px;font-size:15px;display:inline-block;position:relative;padding:0;font-weight:600;line-height:1em">
            <span class="italia-it-button-icon"><img src="<?php echo $spid_ico_circle_svg; ?>" onerror="this.src='<?php echo $spid_ico_circle_png; ?>'; this.onerror=null;" alt="" /></span>
            <span class="italia-it-button-text">Login SPID</span></button>

        <div id="spid-idp-button-small-post" class="spid-idp-button spid-idp-button-tip spid-idp-button-relative">
            <ul id="spid-idp-list-small-root-post" class="spid-idp-button-menu" aria-labelledby="spid-idp">
                <!--
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="infocert_id" type="submit" value="<?php echo $infocert_id; ?>"><span class="spid-sr-only">Infocert ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_infocert_svg; ?>" onerror="this.src='<?php echo $spid_idp_infocert_png; ?>'; this.onerror=null;" alt="Infocert ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="poste_id" type="submit" value="<?php echo $poste_id; ?>"><span class="spid-sr-only">Poste ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_posteid_svg; ?>" onerror="this.src='<?php echo $spid_idp_posteid_png; ?>'; this.onerror=null;" alt="Poste ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="tim_id" type="submit" value="<?php echo $tim_id; ?>"><span class="spid-sr-only">Tim ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_timid_png; ?>" onerror="this.src='<?php echo $spid_idp_timid_svg; ?>'; this.onerror=null;" alt="Tim ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="sielte_id" type="submit" value="<?php echo $sielte_id; ?>"><span class="spid-sr-only">Sielte ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_sielteid_png; ?>" onerror="this.src='<?php echo $spid_idp_sielteid_svg; ?>'; this.onerror=null;" alt="Sielte ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="aruba_id" type="submit" value="<?php echo $aruba_id; ?>"><span class="spid-sr-only">Aruba ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_arubaid_png; ?>" onerror="this.src='<?php echo $spid_idp_arubaid_svg; ?>'; this.onerror=null;" alt="Aruba ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="namirial_id" type="submit" value="<?php echo $namirial_id; ?>"><span class="spid-sr-only">Namirial ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_namirialid_png; ?>" onerror="this.src='<?php echo $spid_idp_namirialid_svg; ?>'; this.onerror=null;" alt="Namirial ID" /></button>
                    </li>
                    <li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="register_id" type="submit" value="<?php echo $register_id; ?>"><span class="spid-sr-only">SpidItalia ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_registerid_png; ?>" onerror="this.src='<?php echo $spid_idp_registerid_svg; ?>'; this.onerror=null;" alt="SpidItalia ID" /></button>
                    </li>
					<li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="intesa_id" type="submit" value="<?php echo $intesa_id; ?>"><span class="spid-sr-only">Intesa ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_intesaid_png; ?>" onerror="this.src='<?php echo $spid_idp_intesaid_svg; ?>'; this.onerror=null;" alt="Intesa ID" /></button>
                    </li>
					<li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="lepida_id" type="submit" value="<?php echo $lepida_id; ?>"><span class="spid-sr-only">Lepida ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_lepidaid_png; ?>" onerror="this.src='<?php echo $spid_idp_lepidaid_svg; ?>'; this.onerror=null;" alt="Lepida ID" /></button>
                    </li>
					<li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="spidvalidator_id" type="submit" value="<?php echo $spidvalidator_id; ?>"><span class="spid-sr-only">SPID Validator ID</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_validatorid_png; ?>" onerror="this.src='<?php echo $spid_idp_validatorid_svg; ?>'; this.onerror=null;" alt="SPID Validator ID" /></button>
                    </li>
					<li class="spid-idp-button-link">
                        <button class="idp-button-idp-logo" name="idptest_id" type="submit" value="<?php echo $idptest_id; ?>"><span class="spid-sr-only">IDP TEST</span><img class="spid-idp-button-logo" src="<?php echo $idptest_png; ?>" onerror="this.src='<?php echo $idptest_svg; ?>'; this.onerror=null;" alt="IDPTEST" /></button>
                    </li>
					-->
                <li class="spid-idp-button-link">
                    <button class="idp-button-idp-logo" name="umbria_id" type="submit" value="<?php echo $umbria_id; ?>"><span class="spid-sr-only">Login Umbria</span><img class="spid-idp-button-logo" src="<?php echo $spid_idp_umbriaid_png; ?>" onerror="this.src='<?php echo $spid_idp_umbriaid_svg; ?>'; this.onerror=null;" alt="Login Umbria" /></button>
                </li>
                <li class="spid-idp-support-link">
                    <a href="https://identity.pa.umbria.it/amserver/UI/Login">Maggiori info</a>
                </li>
                <li class="spid-idp-support-link">
                    <a href="https://identity.pa.umbria.it/amserver/UI/Login">Non hai FedUmbria?</a>
                </li>
                <li class="spid-idp-support-link">
                    <a href="https://identity.pa.umbria.it/amserver/UI/Login">Serve aiuto?</a>
                </li>
            </ul>
        </div>
    </form>
    <?php
}
add_action( 'login_enqueue_scripts', function() {
    wp_enqueue_style( 'core', plugins_url( 'css/spid-sp-access-button.min.css', __FILE__ ), false );
}, 10 );

add_action( 'login_enqueue_scripts', function() {
    wp_enqueue_script( 'spid-js', plugins_url( 'js/spid-sp-access-button.min.js', __FILE__ ), array( 'jquery' )  );
}, 1 );

function spid_option($name) {
    $options = get_option('spid');
    if (isset($options[$name])) {
        return $options[$name];
    }
    return false;
}?>
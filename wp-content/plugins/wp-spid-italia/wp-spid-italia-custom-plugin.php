<?php

add_filter('login_message', function () {
    wp_enqueue_script('custom-plugin-spid-login',
        plugin_dir_url(__FILE__) . 'js/custom-plugin-spid-login.js',
        array('jquery'));
});


add_action('um_before_login_form_is_loaded', function () {
    wp_enqueue_script('custom-plugin-spid-login',
        plugin_dir_url(__FILE__) . 'js/custom-plugin-spid-login.js',
        array('jquery'));
});

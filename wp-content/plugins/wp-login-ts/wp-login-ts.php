<?php
/*
Plugin Name: WP LOGIN TS
Description: new authentication mode
Version: 1.0
*/

include( plugin_dir_path( __FILE__ ) . 'constants.php');

register_activation_hook( __FILE__, function(){

});

add_action( 'admin_init', function() {
    register_setting('logints_options', 'logints');
    $arrayatpv = get_plugin_data ( __FILE__ );
    $nuova_versione = $arrayatpv['Version'];
    if ( version_compare( get_option('logints_version'), $nuova_versione, '<')) {
      update_option( 'spid_version', $nuova_versione );
    }
});

add_action( 'init', function() {
  
});


add_action( 'login_enqueue_scripts', function() {
    wp_enqueue_script( 'logints-js', plugins_url( 'js/wp-login-ts.js', __FILE__ ), array( 'jquery' )  );
    wp_enqueue_script( 'fancybox-js', plugins_url( 'js/jquery.fancybox.js', __FILE__ ), array( 'jquery' )  );
}, 1 );

add_action( 'login_enqueue_scripts', function() {
    wp_enqueue_style( 'login-ts', plugins_url( 'css/login-ts-style.css', __FILE__ ), false );
    wp_enqueue_style( 'fancybox-ts', plugins_url( 'css/jquery.fancybox.css', __FILE__ ), false );
}, 10 );


function logints_disable_password_reset() {
    return false;
}
//add_filter ( 'allow_password_reset', 'logints_disable_password_reset' );

//add_action( 'login_head', 'hide_login_nav' );
function hide_login_nav()
{
    ?><style>#nav,#backtoblog{display:none}</style><?php
}

$_wpLoginTsloginError = null;

add_action('um_before_login_form_is_loaded', function ($args) use (&$_wpLoginTsloginError) {
    wp_enqueue_script( 'logints-js', plugins_url( 'js/wp-login-ts.js', __FILE__ ), array( 'jquery' )  );
    wp_enqueue_script( 'fancybox-js', plugins_url( 'js/jquery.fancybox.js', __FILE__ ), array( 'jquery' )  );
    wp_enqueue_style( 'login-ts', plugins_url( 'css/login-ts-style.css', __FILE__ ), false );
    wp_enqueue_style( 'fancybox-ts', plugins_url( 'css/jquery.fancybox.css', __FILE__ ), false );
    include 'form.phtml';
});

add_filter( 'login_message', function( $message ) {
    global $pagenow;

    if ('wp-login.php' !== $pagenow) {
        return;
    }

    $plugin_dir = plugin_dir_url( __FILE__ );
    $card_img = $plugin_dir . '/img/card.png';
    $info_img = $plugin_dir . '/img/info.png';
    $ts_info_img = $plugin_dir . '/img/ts_info.jpg';
    echo '<script>jQuery(document).ready(function($) { 
        $("<input type=\"hidden\" name=\"method\" value=\"standardlogin\">").appendTo($("#loginform"));
        var div = $("<div style=\"text-align: center;\" id=\"logints\">").insertAfter("form[name=spid_idp_access]");
        var a = $("<a href=\"#\" class=\"login-ts-button login-ts-button-size-m button-login-ts\" aria-haspopup=\"true\" aria-expanded=\"false\">").appendTo(div);
        var span = $("<span class=\"login-ts-button-icon\">").appendTo(a);
       
        var txspan = $("<span class=\"login-ts-button-text\">").appendTo(a);
        
        var login_error = $("#login_error");
        if( login_error && (login_error.text().indexOf("Ultime cinque") !=  -1 
            || login_error.text().indexOf("Ultime cinque") !=  -1
            || login_error.text().indexOf("Codice Fiscale") !=  -1
            || login_error.text().indexOf(" Dati non corretti") !=  -1
            || login_error.text().indexOf(" Internal Error") !=  -1
            || login_error.text().indexOf("Corretti") !=  -1)
            || login_error.text().indexOf("La tessera sanitaria inserita risulta") !=  -1){
            $(txspan).text("Torna alla login");
            $("label[for=user_login]").html($("label[for=user_login]").html().replace(/Nome utente o indirizzo email/g,\'Codice Fiscale\'));
            $("label[for=user_pass]").html($("label[for=user_pass]").html().replace(/Password/g,\'Ultime cinque cifre TS<img id="passinfo" data-fancybox data-src="#hidden-content" src="' . $info_img . '" style="vertical-align: middle;margin-left:5px;width:20px" alt="">\'));
            $("#user_login").attr(\'maxlength\',\'16\');
            $("#user_pass").attr(\'maxlength\',\'5\');
            $("<p id=\'expiration_date\'>").insertAfter($("label[for=user_pass]").parent());
            $("<label for=\'user_date\'>").appendTo($("#expiration_date"));
            $("label[for=user_date]").html("Data di scadenza");
            $("<br><input type=\"date\" id=\"scadenza\" name=\"scadenza\" class=\"input\">").appendTo($("label[for=user_date]"));
            $("#backtoblog").hide();
            $("#nav").hide();
            $(".login-ts-button-text").text("Torna alla login");
            $("#nav").hide();
            $("input[name=method]").attr("value","tslogin");
            $("#cardimg").remove();
        }else {
            $("<img src=\"' . $card_img . '\" alt=\"\" id=\"cardimg\"/>").appendTo(span);
            $(txspan).text("Entra con TS");
            $("#expiration_date").remove();
        }

        $("#logints a").on(\'click\', function(event){
            $("#login_error").hide();
            var text =  $(".login-ts-button-text").text();
            if(text == "Entra con TS"){
                $("label[for=user_login]").html($("label[for=user_login]").html().replace(/Nome utente o indirizzo email/g,\'Codice Fiscale\'));
                $("label[for=user_pass]").html($("label[for=user_pass]").html().replace(/Password/g,\'Ultime cinque cifre TS<img id="passinfo" data-fancybox data-src="#hidden-content" src="' . $info_img . '" style="vertical-align: middle;margin-left:5px;width:20px" alt="">\'));
                $("#user_login").attr(\'maxlength\',\'16\');
                $("#user_pass").attr(\'maxlength\',\'5\');
                $("<p id=\'expiration_date\'>").insertAfter($("label[for=user_pass]").parent());
                $("<label for=\'user_date\'>").appendTo($("#expiration_date"));
                $("label[for=user_date]").html("Data di scadenza");
                $("<br><input type=\"date\" id=\"scadenza\" name=\"scadenza\" class=\"input\">").appendTo($("label[for=user_date]"));
                $("#backtoblog").hide();
                $("#nav").hide();
                $(".login-ts-button-text").text("Torna alla login");
                $("#nav").hide();
                $("input[name=method]").attr("value","tslogin");
                $("#cardimg").remove();
            }else {
                $("label[for=user_login]").html($("label[for=user_login]").html().replace(/Codice Fiscale/g,\'Nome utente o indirizzo email\'));
                $("label[for=user_pass]").html($("label[for=user_pass]").html().replace(/Ultime cinque cifre TS/g,\'Password\'));
                $("#user_login").removeAttr(\'maxlength\');
                $("#user_pass").removeAttr(\'maxlength\');
                $("#passinfo").remove();
                $("#backtoblog").show();
                $("#nav").show();
                $(".login-ts-button-text").text("Entra con TS");
                $("input[name=method]").attr("value","standardlogin");
                $("<img src=\"' . $card_img . '\" alt=\"\" id=\"cardimg\"/>").appendTo(".login-ts-button-icon");
                $("#expiration_date").remove();
            }
        });
    });</script>';
    ?>
        <div style="display: none;" id="hidden-content">
            <img src="<?php echo $ts_info_img ?>" />
        </div>

    <?php
});
add_filter( 'authenticate', 'logints_auth', 10, 3 );
add_action('template_redirect', function () use (&$_wpLoginTsloginError) {
    if (!isset($_POST['method']) || $_POST['method'] !== 'tslogin') {
        return;
    }

    $user = wp_signon( array(), '' );
    if ( is_wp_error( $user ) ) {
        $_wpLoginTsloginError = $user;
        return;
    }

    wp_safe_redirect( home_url() );
    exit();
});

function logints_auth( $user, $codice_fiscale, $lastFive ){

    if(isset($_POST['method']) && $_POST['method'] == 'tslogin'){
        remove_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
    
    if($codice_fiscale == '' || $lastFive == '' || !isset($_POST['scadenza']) || empty($_POST['scadenza'])){
        $error = new WP_Error();
        if(empty($codice_fiscale)){ 
            $error->add('empty_username', __('<strong>ERROR</strong>: il campo Codice Fiscale è vuoto.'));
        }

        if(empty($lastFive)){
            $error->add('empty_password', __('<strong>ERROR</strong>: il campo Ultime cinque cifre TS è vuoto.'));
            
        }
        if(!isset($_POST['scadenza']) || empty($_POST['scadenza'])){
            $error->add('empty_date', __('<strong>ERROR</strong>:il campo della data di scadenza è vuoto.'));
            
        }
        return $error;
    }

    if ( is_user_logged_in() ) {
        wp_logout();
    }
    
    $data = array(
        'codice_fiscale'      => $codice_fiscale,
        'lastFive'    => $lastFive
      );
    $options = array(
        'http' => array(
          'method'  => 'POST',
          'content' => json_encode( $data ),
          'header'=>  "Content-Type: application/json\r\n" .
                      "Accept: application/json\r\n"
          )
    );
      
    $context  = stream_context_create( $options );
    $result = file_get_contents( LOGINTS_URL, false, $context );
    $response = json_decode( $result );
    if(!empty($response)){
        $results = $response->result;
        if(!empty($results)){
            $result = $results[0];
            $expiration_date = $_POST['scadenza'];
            $expiration_date = date("d/m/Y", strtotime($expiration_date));
            $lastName = $result->COGNOME;
            $name = $result->NOME;
            $fullName = $lastName." ".$name;
            $scadenza_ts = $result->SCADENZA_TS;
            if($scadenza_ts != $expiration_date){
                $user = new WP_Error( 'denied', __('La tessera sanitaria inserita risulta scaduta o la data di scadenza non è corretta') );
            }else if(!empty(trim($fullName))){
                $codiceFiscale = $result->COD_FISCALE;
                $users =
                    get_users(
                        array(
                            'meta_key' => 'codice_fiscale',
                            'meta_value' => $codiceFiscale,
                            'number' => 1,
                            'count_total' => false,
                        )
                    );
                
                $user = reset( $users );
                if(empty( $user )){
                    $password = wp_generate_password();
                    $userdata = array(
                        'user_login' => $codiceFiscale,
                        'user_pass' => $password,
                        'display_name' => $fullName,
                        'first_name' => $name,
                        'last_name' => $lastName
                    );
                    $user_id = wp_insert_user($userdata);
                    $user = get_user_by('id', $user_id);
                }
                
                if ( !is_wp_error( $user ) && !empty( $user )) {
                    update_user_meta( $user->ID, 'codice_fiscale', $codiceFiscale);
                    wp_clear_auth_cookie();
                    wp_set_current_user ( $user->ID );
                    wp_set_auth_cookie  ( $user->ID );
                    wp_safe_redirect( home_url() );
                    add_filter( 'authenticate', 'um_wp_form_errors_hook_logincheck', 50, 3 );
                    exit();
                }else {
                    $user = new WP_Error('denied', __('<strong>ERROR</strong>: Internal Error'));
                }
            }else {
                $user = new WP_Error( 'denied', __($response->message) );
            }
        }else {
            $user = new WP_Error('denied', __('<strong>ERROR</strong>: Dati non corretti.'));
        }

        // if($response->error){
        //     $user = new WP_Error( 'denied', __($response->message) );
        // }else {
        //     $results = $response->result;
        //     if(!empty($results)){
        //         $result = $results[0];
        //         $lastName = $result->COGNOME;
        //         $name = $result->NOME;
        //         $fullName = $lastName." ".$name;
                
                
        //     }
        // }     
    }else {
        $user = new WP_Error('denied', __('<strong>ERROR</strong>: Dati non corretti.'));
    }
    }else {
        //add_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );
        //add_filter( 'authenticate', 'um_wp_form_errors_hook_logincheck', 50, 3 );
    }
  
    return $user;
}

?>
<?php

/**
 * Fired during plugin deactivation
 *
 * @link       acercampania
 * @since      1.0.0
 *
 * @package    Acercampania
 * @subpackage Acercampania/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Acercampania
 * @subpackage Acercampania/includes
 * @author     acercampania <acercampania@email.com>
 */
class Acercampania_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

<?php

/**
 * Fired during plugin activation
 *
 * @link       acercampania
 * @since      1.0.0
 *
 * @package    Acercampania
 * @subpackage Acercampania/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Acercampania
 * @subpackage Acercampania/includes
 * @author     acercampania <acercampania@email.com>
 */
class Acercampania_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

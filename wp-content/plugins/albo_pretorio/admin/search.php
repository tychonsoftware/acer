<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {
    die( 'You are not allowed to call this page directly.' );
}

if ( isset( $_REQUEST['action'] ) ) {
} else {
    error_log('annullaricerca' . print_r($_REQUEST ,1));

    if ( isset( $_REQUEST['annullaricerca'] ) ) {
        unset( $_REQUEST['pubstartdate'] );
        unset( $_REQUEST['pubenddate'] );
        unset( $_REQUEST['numerodal'] );
        unset( $_REQUEST['numeroal'] );
        unset( $_REQUEST['datadal'] );
        unset( $_REQUEST['dataal'] );
        unset( $_REQUEST['proponente'] );
        unset( $_REQUEST['subject'] );
        unset( $_REQUEST['annullaricerca'] );
        unset( $_REQUEST['pagenumber'] );
    }
    VisualizzaAlboRicerca();
}
function VisualizzaAlboRicerca() {
    error_log('Request ' . print_r($_REQUEST,1));
    if ( $_REQUEST ) {
        $_REQUEST = array_map( 'albo_sanitize_request', $_REQUEST );
    }

    $items = albo_get_all_atti();
    $items_counts = albo_get_count_atti();
    ?>
    <?php include 'search/form.phtml' ?>

    <div style="margin-top:20px;" class="alignfull">
        <?php
        if ( isset( $items ) && count($items)> 0 ) {
            $index = 0;
            ?>
            <table id="alboPretorioTable" class="wp-list-table widefat striped posts" style="width:100%;table-layout: fixed;border: none;border-collapse: collapse;">
                <thead>
                    <tr>
                        <th scope="col" style="width: 8%;text-align: center!important;">Numero</th>
                        <th scope="col" style="width: 10%;text-align: center!important;">Data Atto</th>
                        <th scope="col" style="width: 15%;text-align: center!important;">Proponente</th>
                        <th scope="col" style="width: 20%;text-align: center!important;">Orgetto</th>
                        <th scope="col" style="width: 12%;text-align: center!important;">Data Pubblicazione</th>
                        <th scope="col" style="width: 12%;text-align: center!important;">Esecutivita</th>
                        <th scope="col" style="width: 20%;text-align: center!important;">File Download</th>
                        <th scope="col" style="width: 10%;text-align: center!important;">Dettagli</th>
                    </tr>
                </thead>
                <tbody id="the-list">
                <?php
                foreach ($items as $item) {
                    ?>
                <tr>
                    <td style="text-align: center!important;">
                        <?php echo $item -> numero; ?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php
                        if (isset($item->data)) {
                            $data = strtotime($item->data);
                            echo date('d/m/Y', $data);
                        }?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php echo $item -> proponenti; ?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php echo $item -> oggetto; ?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php
                        if (isset($item->iniziopubblicazione)) {
                            $data = strtotime($item->iniziopubblicazione);
                            echo date('d/m/Y', $data);
                        }?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php
                        $ggesecutivita = 'Per Approvazione';
                        if (isset($item->ggesecutivita)) {
                            if($item->ggesecutivita == '0'){
                                $ggesecutivita = 'Immediata';
                            }else if($item->ggesecutivita == '10'){
                                $ggesecutivita = 'Ordinaria – 10 Giorni';
                            }
                        }?>
                        <?php echo $ggesecutivita; ?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php
                        if (isset($item->path_file) && !empty($item->path_file)) {
                            $filename = basename($item->path_file);
                            echo '<a  href="#" class="path_file" data-href="' . $item->id . '">' . $filename . '</a>';
                            if (isset($item->upload_file_path) && !empty($item->upload_file_path)) {
                                $filename = basename($item->upload_file_path);
                                echo '<br/><a  href="#" class="path_document" data-href="' . $item->id . '">' . $filename . '</a>';
                            }
                        }?>
                    </td>
                    <td style="text-align: center!important;">
                        <?php
                        // data-target="#alboPretorioModal"
                            echo '<a  href="#" class="dettagli_link" data-toggle="modal"  data-href="' . $item->id . '">Dettagli</a>';
                        ?>
                    </td>

                </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>



        <?php
            $per_page = get_option('opt_albo_per_page',10);
            $search_result = ' <div class="clearfix"></div>';
            if (isset($items_counts) && $items_counts > $per_page) {
                $Npag = (int)($items_counts / $per_page);
                if ($items_counts % $per_page > 0) {
                    $Npag++;
                }
                $search_result .= '<div class="tablenav" style="float:right;" id="risultati">
                <div class="tablenav-pages"> <p>Pagine';
                $page_num = $_REQUEST['pagenumber'];
                if (isset($page_num) and $page_num > 1) {
                    $Pagcur = $page_num;
                    $PagPre = $Pagcur - 1;
                    $search_result .= '&nbsp;<a href="#" value="1" class="albo-page-numbers numero-pagina"
                                     title="Vai alla prima pagina">&laquo;</a>
                            &nbsp;<a href="#" value="' . $PagPre . '" class="albo-page-numbers numero-pagina"
                                     title="Vai alla pagina precedente">&lsaquo;</a>';

                } else {
                    $Pagcur = 1;
                    $search_result .= '&nbsp;<span class="albo-page-numbers current"
                                        title="Sei gi&agrave; nella prima pagina">&laquo;</span>
                            &nbsp;<span class="albo-page-numbers current"
                                        title="Sei gi&agrave; nella prima pagina">&lsaquo;</span>';
                }
                $search_result .= '&nbsp;<span class="albo-page-numbers current">' . $Pagcur . '/' . $Npag . '</span>';
                $PagSuc = $Pagcur + 1;
                if ($PagSuc <= $Npag) {
                    $search_result .= '&nbsp;<a href="#" value="' . $PagSuc . '" class="albo-page-numbers numero-pagina"
                                     title="Vai alla pagina successiva">&rsaquo;</a>&nbsp;<a
                                    href="#" value="' . $Npag . '" class="albo-page-numbers numero-pagina"
                                    title="Vai all\'ultima pagina">&raquo;</a>';
                } else {
                    $search_result .= '&nbsp;<span class="albo-page-numbers current"
                                        title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>&nbsp;<span
                                    class="albo-page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>';
                }
                $search_result .= '</p>';
            }
            echo $search_result;
        }
        ?>
        <div class="clearfix"></div>
    </div>
    <?php
}

?>

<div class="modal fade" id="proceduraAccessoCertificazioneModal" tabindex="-1" role="dialog"
     aria-labelledby="proceduraAccessoCertificazioneModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ProceduraAccessoCertificazioneLabel">Procedura accesso certificazione</h5>
                <button type="button" class="close btn-sm" data-dismiss="modal" aria-label="Close"
                        onclick="jQuery('#proceduraAccessoCertificazioneModal').modal('toggle');">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 1rem !important;">
                <div id="proceduraAccessoCertificazioneText"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"
                        onclick="jQuery('#proceduraAccessoCertificazioneModal').modal('toggle');">Chiudi</button>
            </div>
        </div>
    </div>
</div>
</div>

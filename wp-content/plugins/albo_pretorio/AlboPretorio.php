<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Albo Pretorio
 * Description:       Search/Extract Data
 * Version:           1.0.0
 * Text Domain:       albo-pretorio
 */


if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/AlboPretorioFunctions.php');
define("PMR_URL", plugin_dir_url(dirname(__FILE__) . '/AlboPretorio.php'));

if (!class_exists('AlboPretorio')) {
    class AlboPretorio

    {
        var $version;
        var $minium_WP   = '3.1';
        
        function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->define_tables();
            $this->load_dependencies();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('AlboPretorio', 'activate'));
            register_deactivation_hook(__FILE__, array('AlboPretorio', 'deactivate'));
            register_uninstall_hook(__FILE__, array('AlboPretorio', 'uninstall'));
            add_action('wp_enqueue_scripts', array(&$this, 'AlboPretorio_Enqueue_Scripts'));

            add_action('wp_ajax_albo_pretorio_file', function () {
                $recordId = intval($_POST['fileRecordId']);
                $type = $_POST['type'];
                albo_download_file($recordId, $type);
            });

            add_action('wp_ajax_albo_pretorio_data', function () {
                $recordId = intval($_POST['recordId']);
                albo_pretorio_data($recordId);
            });
        }

        function load_dependencies()
        {
            if (is_admin()) {
                require_once(dirname(__FILE__) . '/admin/admin.php');
            }
        }

        function define_tables()
        {
            global $wpdb, $table_prefix;
            $wpdb->table_name_Albo_Pretorio_Atti = $table_prefix  . "albo_pretorio_atti";
        }

        static function init()
	    {

        }
        static function screen_option()
	    {
            
        }

        static function dizionari_set_option($status, $option, $value)
        {
        }

        static function AlboPretorio_Enqueue_Scripts($hook_suffix)
		{
            $path = plugins_url('', __FILE__);
            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-tabs', '', array('jquery'));
            wp_enqueue_script('jquery-ui-dialog', '', array('jquery'));
            wp_enqueue_script('jquery-ui-datepicker', '', array('jquery'));
            wp_enqueue_script( 'bootstrap-min', $path . '/js/bootstrap.min.js', array(), true );
            wp_enqueue_script('AlboPretorio', $path . '/js/albo_pretorio.js', array('jquery'));
            wp_enqueue_style( 'bootstrap',  $path . '/css/bootstrap.min.css' );
            wp_enqueue_style('jquery.ui.theme', $path . '/css/jquery-ui-custom.css');
            wp_register_style('AlboPretorio', $path . '/css/albo_style.css');
            wp_enqueue_style('AlboPretorio');
            wp_localize_script('AlboPretorio', 'AlboPretorio',
                array('ajax_url' => admin_url('admin-ajax.php')));
        }
        
        static function activate()
        {
            global $wpdb;
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);
        }

        
        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
        }
        static function uninstall()
        {
        }
    }
    global $PMR_OnLine;
    $PMR_OnLine = new AlboPretorio();
}

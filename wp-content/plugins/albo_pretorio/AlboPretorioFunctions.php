<?php
/**
 * Functions necessary to the plugin
 * @package    Albo Pretorio
 */
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

add_shortcode('albo', 'VisualizzaAlboSearch');
function VisualizzaAlboSearch($Parametri)
{
    $ret = "";
    $Parametri = shortcode_atts(array('per_page' => '10', 'filtro_date' => 'false'), $Parametri, "albo");
    $PerPage = $Parametri['per_page'];
    if(get_option('opt_albo_per_page')){
        update_option('opt_albo_per_page', $PerPage);
    }
    else {
        add_option('opt_albo_per_page', $PerPage);
    }

    if(get_option('opt_albo_filtro_date')){
        update_option('opt_albo_filtro_date', $Parametri['filtro_date']);
    }
    else {
        add_option('opt_albo_filtro_date', $Parametri['filtro_date']);
    }
    require_once(dirname(__FILE__) . '/admin/search.php');
    return $ret;
}

function albo_get_all_proponenti() {
    global $wpdb;
    $Selezione = ' WHERE proponenti <> \'\'';
    $sql = <<<SQL
    SELECT DISTINCT proponenti FROM $wpdb->table_name_Albo_Pretorio_Atti p
    $Selezione
    SQL;
    return $wpdb->get_results( $sql);
}

function albo_get_all_atti() {
    global $wpdb;

    $per_page = get_option('opt_albo_per_page',10);
    if (!isset($per_page))
        $per_page = 10;
    $request = get_albo_search_request();
    if(isset($request['pagenumber']))
        $page_num = $request['pagenumber'];

    if (!isset($page_num)) {
        $st = 0;
    } else {
        $st = ($page_num - 1) * $per_page;
    }
    $pp = intval( trim($per_page) );
    if ($st == 0 and $pp == 0)
        $limit = '';
    else
        $limit = ' LIMIT ' . $st .",". $pp ;

    $where = array_merge([], albo_where_condition_from_search());

    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }
    $sql = <<<SQL
SELECT SQL_CALC_FOUND_ROWS p.*
FROM $wpdb->table_name_Albo_Pretorio_Atti p
$Selezione
SQL;

    $sql .= $limit;
    error_log('albo_get_all_atti : ' . $sql);
    return $wpdb->get_results( $sql);
}

function albo_get_count_atti() {
    global $wpdb;
    return $wpdb->get_var('SELECT FOUND_ROWS()');
}

function albo_where_condition_from_search() {
    $request = get_albo_search_request();
    $subject = $request['subject'] ?? '';
    $proponente = $request['proponente'] ?? '';
    $data_inizio_pubblicazione = $request['pubstartdate'] ?? '';
    $data_fine_pubblicazione = $request['pubenddate'] ?? '';
    $numero_dal = $request['numerodal'] ?? 0;
    $numero_al = $request['numeroal'] ?? 0;
    $data_dal = $request['datadal'] ?? '';
    $data_al = $request['dataal'] ?? '';

    $where = [];

    if (isset($proponente) && !empty($proponente) && $proponente != '--Non selezionato-') {
        $where[]  = 'proponenti=\'' . $proponente . '\'';
    }

    if (isset($subject) && !empty($subject)) {
        $where[] ='oggetto LIKE \'%' . $subject . '%\'';
    }

    if (isset($data_inizio_pubblicazione) && !empty($data_inizio_pubblicazione)) {
        $where[] = 'iniziopubblicazione >=\'' . $data_inizio_pubblicazione . '\'';
    }

    if (isset($data_fine_pubblicazione) && !empty($data_fine_pubblicazione)) {
        $where[] = 'finepubblicazione <=\'' . $data_fine_pubblicazione . '\'';
    }

    if (isset($numero_dal) && $numero_dal > 0) {
        $where[] = 'numero >=' . $numero_dal;
    }

    if (isset($numero_al) && $numero_al > 0) {
        $where[] = 'numero <=' . $numero_al;
    }
    $filtro_date = get_option('opt_albo_filtro_date',false);
    if (isset($data_dal) && !empty($data_dal)) {
        $where[] = 'data >=\'' . $data_dal . '\'';
    }else if($filtro_date === 'true'){
        $default_date=Date('Y-m-d', strtotime('-15 days'));
        $where[]  = 'data >=\'' . $default_date . '\'';
    }

    if (isset($data_al) && !empty($data_al)) {
        $where[] = 'data <=\'' . $data_al . '\'';
    }

    return $where;
}

/**
 * @return array
 */
function get_albo_search_request(): array {

    $request = $_REQUEST;

    if($request != null){
        $request = array_map( 'albo_sanitize_request', $request );
    }else {
        $request = array();
    }
    return $request;
}


function albo_sanitize_request($value){
    global $wpdb;
    if (is_array($value)) {
        return array_map('albo_sanitize_request', $value);
    }
    if (is_string($value)) {
        $value = trim($value);
    }
    return $wpdb->_real_escape( $value );
}

function albo_download_file($recordId, $type) {
    global $wpdb;
    $sql = 'SELECT * from' . $wpdb->table_name_Albo_Pretorio_Atti . ' WHERE id=' . $recordId;
    error_log($type . '..........albo_download_file........  ' . $recordId . '  SQL ' . $sql);
    $items  = $wpdb->get_results( $sql);
    error_log('items ' . print_r($items,1));
    if ( isset( $items ) && count($items)> 0 ) {
        $record = $items[0];
        if($type == 'first'){
            $fileData = base64_encode(file_get_contents($record->path_file));
            $filename = basename($record->path_file);
            $mimetype = mime_content_type($record->path_file);
        }else if($type == 'second'){
            $fileData = base64_encode(file_get_contents($record->upload_file_path));
            $filename = basename($record->upload_file_path);
            $mimetype = mime_content_type($record->upload_file_path);
        }
        $data = ['success' => 'true','filename' =>  $filename, 'mimetype' =>  $mimetype,'record_data' => $fileData];
        echo json_encode( $data );
    }
}

function albo_pretorio_data($recordId) {

    global $wpdb;
    $sql = 'SELECT * from ' . $wpdb->table_name_Albo_Pretorio_Atti . ' WHERE id=' . $recordId;
    error_log('..........albo_pretorio_data........  ' . $recordId . '  SQL ' . $sql);
    $items  = $wpdb->get_results( $sql);
    error_log('items ' . print_r($items,1));
    $record_data = '';
    if ( isset( $items ) && count($items)> 0 ) {
        $record = $items[0];
        $record_data = '<table class="wp-list-table widefat striped posts" style="width:100%;table-layout: fixed;border: none;border-collapse: collapse;">';
        $record_data .= '<tbody>';
        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Tipo:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        $record_data .= '<input type="text" style="height: 85%;" id="tipo" value="' . $record->tipo . '" disabled>';
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Numero:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        $record_data .= '<input type="text" style="height: 85%;" id="numero" value="' . $record->numero . '" disabled>';
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Data Atto:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        if (isset($record->data)) {
            $data = strtotime($record->data);
            $record_data .= '<input type="text" style="height: 85%;" id="dataatto" value="'. date('d/m/Y', $data) .'" disabled>';
        }
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Proponente:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        $record_data .= '<input type="text" style="height: 85%;" id="propente" value="' . $record->proponenti . '" disabled>';
        $record_data .= '</td>';

        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Oggetto:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        $record_data .= '<input type="text" style="height: 85%;" id="oggetto" value="' . $record->oggetto . '" disabled>';
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Inizio Pubblicazione:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        if (isset($record->iniziopubblicazione)) {
            $data = strtotime($record->iniziopubblicazione);
            $record_data .= '<input type="text" style="height: 85%;" id="iniziopubblicazione" value="'. date('d/m/Y', $data) .'" disabled>';
        }
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Fine Pubblicaziones:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        if (isset($record->finepubblicazione)) {
            $data = strtotime($record->finepubblicazione);
            $record_data .= '<input type="text" style="height: 85%;" id="finepubblicazione" value="'. date('d/m/Y', $data) .'" disabled>';
        }
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">Esecutivit&agrave;:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);">';
        $ggesecutivita = 'Per Approvazione';
        if(isset($record->ggesecutivita)){
            if($record->ggesecutivita == '0'){
                $ggesecutivita = 'Immediata';
            }else if($record->ggesecutivita == '10'){
                $ggesecutivita = 'Ordinaria – 10 Giorni';
            }
        }
        $record_data .= '<input type="text" style="height: 85%;" id="esecutivita" value="' . $ggesecutivita . '" disabled>';
        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '<tr>';
        $record_data .= '<td style="width: 180px;"><h5 style="font-size: 17px;margin-top: 15px;margin-bottom: 15px;">File Download:</h5></td>';
        $record_data .= '<td style="width: calc(100% - 180px);gap: 10px;min-height: 67px;align-items: center;">';

        if(isset($record->path_file) && !empty($record->path_file)){
            $filename = basename($record->path_file);
            $record_data .= '<a class="deleteLink" href="#" id="att_file" onclick="return downloadFile(event,1)" data-href="' . $record->id . '" style="margin-right:10px">' . $filename . '</a>';
        }

        if(isset($record->upload_file_path) && !empty($record->upload_file_path)){
            $filename = basename($record->upload_file_path);
            $record_data .= '<a  class="deleteLink" href="#" id="att_second_file" onclick="return downloadFile(event,2)" data-href="' . $record->id . '">' . $filename . '</a>';
        }

        $record_data .= '</td>';
        $record_data .= '</tr>';

        $record_data .= '</tbody>';
        $record_data .= '</tr>';
        $record_data .= '</table>';
    }
    $data = ['success' => 'true','record_data' =>  $record_data];
    echo json_encode( $data );
}

add_action( 'plugins_loaded', function () {
    global $wpdb;
    $prefix = $wpdb->prefix;
    $table_name = $prefix . 'albo_pretorio_atti';

    $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

    if ( ! $wpdb->get_var( $query ) == $table_name ) {
        $charset = $wpdb->get_charset_collate();
        $sql = "
    CREATE TABLE IF NOT EXISTS $table_name(
        id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        anno int(4) NOT NULL DEFAULT '0',
        numero int(4) NOT NULL DEFAULT '0',
        tipo varchar(255) NOT NULL,
        tipoattoid int(8) NOT NULL,
        data date NOT NULL DEFAULT '0000-00-00',
        atto varchar(255) NOT NULL,
        proponenti varchar(255) NULL,
        iniziopubblicazione timestamp NULL,
        finepubblicazione timestamp NULL,
        esecutivitaimmediata varchar(255) NULL,
        ggesecutivita int(8) NULL,
        dataesecutivita timestamp NULL,
        tipopubblicazione varchar(255) NULL,
        oggetto varchar(1024) NULL,
        sorgente varchar(255) NULL,
        path_file varchar(1024) NULL,
        attoid int(8) NULL,
        tipocodice varchar(5) NULL,
        uploaddocid int(8) NULL,
      	upload_file_path varchar(1024) NULL,
        PRIMARY KEY (id)
    ) $charset;
    ";
	$wpdb->query( $sql );
    }
} );

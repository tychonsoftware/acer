jQuery(document).ready(function ($) {
        $("#reset_albo_form").click(function() {
            console.log('.........dsdds.............');
            if ($('input[name="annullaricerca"]').length == 0) {
                $('form#albo-pretorio-form').append('<input type="hidden" name="annullaricerca" value="true" />');
            }
            $('form#albo-pretorio-form').submit();
        });

    $(".path_document").click(function(event) {

        $("body").addClass("loader-overlay");
        $("body").css("position","relative");
        $(".loader-gif").css("display","block");

        var data = {
            'action': 'albo_pretorio_file',
            'fileRecordId': $(this).attr("data-href"),
            'type': 'second'
        };

        $.ajax({
            type: 'POST',
            url: AlboPretorio.ajax_url,
            data: data,
        }).done(function (response) {
            if(response.endsWith(0)){
                response = response.slice(0, -1);
            }
            fileData = JSON.parse(response);
            console.log('Second file download', fileData);
            if(fileData.success){
                var a = document.createElement("a");
                a.href = "data:" + fileData.mimetype + ";base64," + fileData.record_data;
                a.download = fileData.filename;
                a.click();
            }
        }).fail(function () {
            console.log('second file download failed');
        }).always(function () {
            $("body").removeClass("loader-overlay");
            $(".loader-gif").css("display","none");
        });
        event.preventDefault();
    });


    $(".path_file").click(function(event) {

        $("body").addClass("loader-overlay");
        $("body").css("position","relative");
        $(".loader-gif").css("display","block");

        var data = {
            'action': 'albo_pretorio_file',
            'fileRecordId': $(this).attr("data-href"),
            'type': 'first'
        };

        $.ajax({
            type: 'POST',
            url: AlboPretorio.ajax_url,
            data: data,
        }).done(function (response) {
            if(response.endsWith(0)){
                response = response.slice(0, -1);
            }
            fileData = JSON.parse(response);
            console.log('First file download', fileData);
            if(fileData.success){
                var a = document.createElement("a");
                a.href = "data:" + fileData.mimetype + ";base64," + fileData.record_data;
                a.download = fileData.filename;
                a.click();
            }
        }).fail(function () {
            console.log('First file download failed');
        }).always(function () {
            $("body").removeClass("loader-overlay");
            $(".loader-gif").css("display","none");
        });
        event.preventDefault();
    });

    $(".dettagli_link").click(function(event) {

        $("body").addClass("loader-overlay");
        $("body").css("position","relative");
        $(".loader-gif").css("display","block");

        var data = {
            'action': 'albo_pretorio_data',
            'recordId': $(this).attr("data-href")
        };

        $.ajax({
            type: 'POST',
            url: AlboPretorio.ajax_url,
            data: data,
        }).done(function (response) {
            if(response.endsWith(0)){
                response = response.slice(0, -1);
            }
            data = JSON.parse(response);
            console.log('Data download', data);
            $("#albo_modal").html("");
            $("#alboPretorioModal").modal("show");
            if(data.success){
                $("#albo_modal").html(data.record_data);
            }

        }).fail(function () {
            console.log('Data download failed');
        }).always(function () {
            $("body").removeClass("loader-overlay");
            $(".loader-gif").css("display","none");
        });
        event.preventDefault();
    });

    $(".albo-page-numbers").click(function() {
        var page_num = $(this).attr("value");
        if ($('input[name="pagenumber"]').length == 0) {
            $('form#albo-pretorio-form').append('<input type="hidden" name="pagenumber" value="' + page_num + '" />');
        }else {
            $("input[name=pagenumber]").val(page_num);
        }
        $('form#albo-pretorio-form').submit();
    });



});

function downloadFile(event, fileNumber) {
    event.preventDefault();
    var id_ele = null;
    var type = '';
    if(fileNumber === 1){
        id_ele = document.getElementById("att_file");
        type = 'first';
    }else if(fileNumber === 2){
        id_ele = document.getElementById("att_second_file");
        type = 'second';
    }
    jQuery("body").addClass("loader-overlay");
    jQuery("body").css("position","relative");
    jQuery(".loader-gif").css("display","block");

    jQuery("body").addClass("loader-overlay");
    jQuery("body").css("position","relative");
    jQuery(".loader-gif").css("display","block");

    var data = {
        'action': 'albo_pretorio_file',
        'fileRecordId': id_ele.getAttribute("data-href"),
        'type': type
    };

    jQuery.ajax({
        type: 'POST',
        url: AlboPretorio.ajax_url,
        data: data,
    }).done(function (response) {
        if(response.endsWith(0)){
            response = response.slice(0, -1);
        }
        fileData = JSON.parse(response);
        console.log('First file download', fileData);
        if(fileData.success){
            var a = document.createElement("a");
            a.href = "data:" + fileData.mimetype + ";base64," + fileData.record_data;
            a.download = fileData.filename;
            a.click();
        }
    }).fail(function () {
        console.log('second file download failed');
    }).always(function () {
        jQuery("body").removeClass("loader-overlay");
        jQuery(".loader-gif").css("display","none");
    });
}


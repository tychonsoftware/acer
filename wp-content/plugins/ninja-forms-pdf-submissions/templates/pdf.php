<?php
/**
 * PDF Included With Form Submission
 *
 * @author 		Patrick Rauland
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<html><head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="charset=utf-8" />
  <style type="text/css">
    * {
	  font-family: "DejaVu Sans Mono";
    }
  </style>
  
	<link type="text/css" href="<?php echo $css_path; ?>" rel="stylesheet" />
</head><body>

<?php echo $table; ?>

</body></html>

<?php
if(isset($_REQUEST['action'])){
    $file_path = $_REQUEST['action'];
    $pdf = file_get_contents($file_path);
    header('Content-Description: File Transfer');
    header('Content-Type: application/pdf');
    header("Content-Disposition: inline; filename=\"" . basename($file_path) . "\";");
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_path));
    ob_clean();
    flush();
    echo $pdf;
    exit;
}

?>
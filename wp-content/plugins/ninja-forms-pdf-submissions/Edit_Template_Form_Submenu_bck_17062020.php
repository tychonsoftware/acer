<?php
if ( ! defined( 'ABSPATH' ) ) exit;

add_action('admin_menu', 'wpdocs_register_edit_template_form');
 
function wpdocs_register_edit_template_form() {
    add_submenu_page(
        'admin.php?page=ninja-forms',
        'Edit Template Form',
        'Edit Template Form',
        'manage_options',
        'edit-template-form',
        'edit_template_form_callback' );
}

function edit_template_form_callback() {

include_once plugin_dir_path( __FILE__ ) . 'pdf_template_includes.php';

$pdfTemplate = new PDFTemplate;

if (isset($_REQUEST['form_id']) && $_REQUEST['form_id'] != '') {
	$form_id = $_REQUEST['form_id'];
}
if (isset($_REQUEST['pdf_template_Id']) && $_REQUEST['pdf_template_Id'] != '') {
	$pdf_template_Id = $_REQUEST['pdf_template_Id'];
}

$pageLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$isWeblogeService = false;
if($pdf_template_Id > 0) {
	$pdf_template_dat = $pdfTemplate->get_PDF_template($pdf_template_Id);
	$pdf_template_name = $pdf_template_dat->templateName;
	$mp_id = $pdf_template_dat->mpId;
  $webServiceId = $pdf_template_dat->web_service_id;
  if ($webServiceId == 1) {
      $isWeblogeService = true;
  }
	$mp_Name = $pdf_template_dat->mpName;
	$pdf_template_content = $pdf_template_dat->templateDat;
	if(!isset($pdf_template_dat->templateSuffix))
	{
		$pdfTemplate->ninja_forms_suffix_column();
		$pdf_template_dat = $pdfTemplate->get_PDF_template($pdf_template_Id);
	}
	$pdf_template_suffix = $pdf_template_dat->templateSuffix;
}

$forms = Ninja_Forms()->form()->get_forms();
$pdf_templates = $pdfTemplate->get_PDF_templates();
$pdf_forms = array();
foreach( $pdf_templates as $pdf_template ){
	$pdf_forms[] = $pdf_template->formID;
}

$doc_templates = $pdfTemplate->get_doc_templates();
$webServices = $pdfTemplate->get_web_services();
/*
$nf_field = array();
$nf_field_labels = array();
$nf_fields = Ninja_Forms()->form( 1 )->get_fields();
foreach( $nf_fields as $field ){
	$nf_field = $field->get_settings();
	$nf_field_labels[] = $field->get_setting( 'label' );
}
*/
?>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<div id="errori" style="display:none"></div>
    <div class="wrap">
        <h2>Edit Template Form</h2>
		<form name="post" action="<?php echo $pageLink; ?>" method="post" id="pdfTemplateForm">
		<div id="message"></div>
		<input type="hidden" name="pdf_template_Id" id="pdf_template_Id" value="<?php echo $pdf_template_Id; ?>" />
		<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content" style="position: relative;">
		<div id="titlediv"><div id="titlewrap">
			<input type="text" name="pdf_template_name" id="pdf_template_name" spellcheck="true" autocomplete="off" value="<?php echo $pdf_template_name; ?>" placeholder="Nome Template" />
		</div></div>
		<?php // var_dump($nf_field_labels); ?>
		<p><select name="form_id" id="form_id">
			<option value="">Select Form</option>
			<?php foreach( $forms as $form ){
			$formID = $form->get_id();
			if( !in_array($formID, $pdf_forms) || $formID == $form_id ) {
			if($formID == $form_id) { 
				$selForm = 'selected';
			} else {
				$selForm = '';
			} ?>
			<option value="<?php echo $form->get_id(); ?>" <?php echo $selForm; ?> ><?php echo $form->get_setting( 'title' ); ?></option>
		<?php } } ?>
		</select></p>

    <p>
      <select name="web_service_id" id="web_service_id" onchange="">
        <option value="">Seleziona web service</option>
        <?php foreach( $webServices as $webService ): ?>
        <option value="<?= $webService->id ?>" <?php if ($webServiceId == $webService->id): ?> selected <?php endif; ?> ><?= $webService->web_service_name ?></option>
        <?php endforeach; ?>
      </select>
    </p>

		<p <?php if (!$isWeblogeService): ?> class="hidden" <?php endif; ?>>
      <select name="mp_id" id="mp_id">
			<option value="">Modello Protocollo</option>
			<?php foreach( $doc_templates as $doc_template ) {
				$sel = '';
				if($mp_id == $doc_template->Id) {
					$sel = 'selected';
				}
				echo '<option value="'.$doc_template->Id.'" '.$sel.'>'.$doc_template->Name.'</option>';
			} ?>
		</select>
		<input type="hidden" name="mp_Name" id="mp_Name" value="<?php echo $mp_Name; ?>" /></p>
    <p <?php if (!$isWeblogeService): ?> class="hidden" <?php endif; ?>>
      <input type="text" name="pdf_template_suffix" id="pdf_template_suffix" autocomplete="off" value="<?php echo $pdf_template_suffix; ?>" placeholder="Suffisso Oggetto Protocollo" />
    </p>

		<div class="half"><p><select name="form_keys" id="form_keys">
		<option value="">Select Key</option>
		</select></p></div>
		<div class="half last"><p><input type="text" name="form_key" id="form_key" placeholder="Copy field tag!" /></p></div>
		<div class="clear"></div>
		<p><?php 
		
		// $args = array(
		// 	    'media_buttons' => false,
		// 		'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close,table' ),
		// 		'tinymce'       => array(
		// 			'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink,table'
		// 		),
		// 	);

		$args =  array(
			'wpautop'=>true,
			'textarea_name' => 'Note',
			'textarea_rows' => 10,
			  'teeny' => TRUE,
			  'media_buttons' => false,
			  'tinymce' => array(
				  'toolbar1' => 'formatselect, fontsizeselect,fontselect, bold, italic, strikethrough,bullist,numlist,outdent,indent,blockquote,forecolor,backcolor,|,alignleft,aligncenter,alignright,alignjustify,|,link,unlink,wp_more,|,spellchecker,fullscreen,image,media,|,removeformat, table',
				  'toolbar2' => '',
				  'plugins' => 'colorpicker,lists,fullscreen,image,wordpress,wpeditimage,wplink,textcolor,media,spellchecker, table'
			  ));
			wp_editor( $pdf_template_content, 'pdf_template_content', $args ); ?>
		<?php /* <textarea name="pdf_template_content" id="pdf_template_content"><?php echo $pdf_template_content; ?></textarea>*/ ?></p>
		<input type="hidden" id="previewurl" name="previewurl" value="<?php echo plugin_dir_url(dirname(__FILE__) . '/Edit_Template_Form_Submenu.php') ?>">
		<p><button type="button" id="save_pdf_template" class="button button-primary button-large">Salva</button> <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=configurazione-template-form" class="button button-primary button-large">Annulla</a> 
		<button type="button" id="preview_pdf_template" class="button button-primary button-large">Preview PDF</button></p>
		</div>
		</div>
		</div>
		</form>
    </div>

<style>
.request-overlay {
    z-index: 9999;
    position: fixed; /*Important to cover the screen in case of scolling content*/
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: block;
    text-align: center;
    background: rgba(200,200,200,0.5) url("<?php echo plugin_dir_url(dirname(__FILE__) . '/Edit_Template_Form_Submenu.php') . 'assets/img/pbar-ani.gif'?>") no-repeat center; /*.gif file or just div with message etc. however you like*/
}
</style>

<script>
/*
var editor;
    ClassicEditor
        .create( document.querySelector( '#pdf_template_content' ) )
		.then( newEditor => {
			editor = newEditor;
		} )
        .catch( error => {
            console.error( error );
        } );
*/


jQuery( document ).ready(function($) {

	var nwform_id = $("#form_id").val();
	load_NF_Fields(nwform_id);
	function load_NF_Fields(form_id) {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'get_NF_FieldsAjax',
					'form_id': form_id,
				},
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
						var form_keys = data.success;
						$("#form_keys").html( '<option value="">Select Key</option>' );
						$("#form_key").val('');
						for(var key  in form_keys) {
							$("#form_keys").append( "<option value='%" + key + "%'>" + form_keys[key] + "</option>" );
						}
					}
				}
			});
	}
	$("#mp_id").on('change', function() {
	  var mp_id =  $(this).children('option:selected').text();
	  $("#mp_Name").val( mp_id );
	});
	$("#form_id").on('change', function() {
	  var form_id = this.value;
	  if ( form_id.length != 0 ) {
		  load_NF_Fields(form_id);
	  }
	});
	$("#form_keys").on('change', function() {
	  $("#form_key").val( this.value );
	});

	$("#preview_pdf_template").click(function() {
		var pdf_template_Id = $("#pdf_template_Id").val();
		var pdf_template_name = $("#pdf_template_name").val();
		var mp_id = $("#mp_id").val();
		var mp_Name = $("#mp_Name").val();
		var form_id = $("#form_id").children("option:selected").val();
		var pdf_template_content = $("#pdf_template_content").val();
		var previewurl = $("#previewurl").val();
		var iframe = document.getElementById("pdf_template_content_ifr");
		var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
		var ps = iframe.contentWindow.document.getElementById('tinymce').getElementsByTagName('p');
		for(var p=0; p < ps.length;p++){
			if(pdf_template_content.length == 0)
				pdf_template_content = ps.get(p).textContent;
			else
				break;
		}
		if ( pdf_template_name.length == 0 || form_id.length == 0 || pdf_template_content.length == 0 ) {
			$("#message").html('<div class="error">All Fields are required!</div>');
		} else {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'preview_PDF_templateAjax',
					'pdf_template_Id': pdf_template_Id,
					'pdf_template_name': pdf_template_name,
					'mp_id': mp_id,
					'mp_Name': mp_Name,
					'form_id': form_id,
					'pdf_template_content': pdf_template_content,
					'file_path' : 'D:/xampp/htdocs/acer/wp-content/Fascicoli/1/vcomitini@artensys.it/Contattaci-1120.pdf'
				},
				beforeSend: function() {
					$('body').append('<div id="requestOverlay" class="request-overlay"></div>'); /*Create overlay on demand*/
                    $("#requestOverlay").show();
				},
				success: function (output) {
					var win = window.open('', '_blank');
    				win.location.href = output;
					$("#requestOverlay").remove();
				},
				error: function(r, o, n) {
					$("#requestOverlay").remove();
				}
			});
		}
	});
	$("#save_pdf_template").click(function() {
		var pdf_template_Id = $("#pdf_template_Id").val();
		var pdf_template_name = $("#pdf_template_name").val();
		var web_service_id = $("#web_service_id").val();
		var mp_id = $("#mp_id").val();
		var mp_Name = $("#mp_Name").val();
		var form_id = $("#form_id").children("option:selected").val();
		var pdf_template_content = $("#pdf_template_content").val();
		var pdf_template_suffix = $("#pdf_template_suffix").val();
		var iframe = document.getElementById("pdf_template_content_ifr");
		var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
		var ps = iframe.contentWindow.document.getElementById('tinymce').getElementsByTagName('p');
		for(var p=0; p < ps.length;p++){
			if(pdf_template_content.length == 0)
				pdf_template_content = ps.get(p).textContent;
			else
				break;
		}
		
		$("#message").text('');
		if ( pdf_template_name.length == 0 || form_id.length == 0 || pdf_template_content.length == 0 ) {
			$("#message").html('<div class="error">All Fields are required!</div>');
		} else {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'add_PDF_templateAjax',
					'pdf_template_Id': pdf_template_Id,
					'pdf_template_name': pdf_template_name,
					'web_service_id': web_service_id,
					'mp_id': mp_id,
					'mp_Name': mp_Name,
					'form_id': form_id,
					'pdf_template_content': pdf_template_content,
					'pdf_template_suffix': pdf_template_suffix
				},
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
						$("#message").html('<div class="notice notice-success">PDF Template is saved.</div>');
						window.setTimeout(function () {
							window.location.href = '?page=configurazione-template-form';
						}, 500);
					} else {
						$("#message").html('<div class="error">'+data.error+'</div>');
					}
				}
			});
		}
		$("html, body").animate({scrollTop : 0},500);
	});

	$('#web_service_id').change(function () {
	    if ($(this).val() == 1) {
          $('#mp_id, #pdf_template_suffix').parent().removeClass('hidden');
      } else {
          $('#mp_id, #pdf_template_suffix').parent().addClass('hidden');
      }
  });
});
</script>
<style>
/*
.ck-editor__editable {
    min-height: 400px;
}
*/
#message .error, #message .notice {
    padding: 12px;
}
#titlediv input {
    padding: 3px 8px;
    font-size: 1.7em;
    line-height: 100%;
    height: 1.7em;
    width: 100%;
    outline: 0;
    margin: 0 0 5px;
    background-color: #fff;
}
#post-body select, #post-body input[type=text] {
    width: 100%;
    height: 38px;
    margin: 0 0 5px;
    font-size: 1.2em;
    font-weight: 500;
}
.half {
    float: left;
    width: 49%;
    margin: 0 2% 0 0;
}
.half.last {
    margin: 0;
}
</style>
<?php }

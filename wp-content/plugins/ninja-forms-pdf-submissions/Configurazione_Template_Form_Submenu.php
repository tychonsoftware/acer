<?php
if ( ! defined( 'ABSPATH' ) ) exit;
add_action('admin_menu', 'wpdocs_register_configurazione_template_form');
 
function wpdocs_register_configurazione_template_form() {
    add_submenu_page(
        'admin.php?page=ninja-forms',
        'Configurazione Template Form',
        'Configurazione Template Form',
        'manage_options',
        'configurazione-template-form',
        'configurazione_template_form_callback' );
	add_submenu_page(
        'ninja-forms',
        'Configurazione Template Form',
        'Configurazione Template Form',
        'manage_options',
        'admin.php?page=configurazione-template-form',
        'configurazione_template_form_callback' );
}

function configurazione_template_form_callback() {

include_once plugin_dir_path( __FILE__ ) . 'pdf_template_includes.php';

$pdfTemplate = new PDFTemplate;

$pdf_templates = $pdfTemplate->get_PDF_templates();
$pdf_forms = array();
$pdf_formsNm = array();
foreach( $pdf_templates as $pdf_template ){
	$pdf_forms[] = $pdf_template->formID;
	$pdf_formsNm[$pdf_template->formID]['temp_name'] = $pdf_template->templateName;
	$pdf_formsNm[$pdf_template->formID]['temp_id'] = $pdf_template->id;
}
?>
    <div class="wrap">
        <h1 class="wp-heading-inline">Configurazione Template Form</h1>
		<a href="<?php echo admin_url( 'admin.php?page=edit-template-form'); ?>" class="page-title-action">Aggiungi Template</a>
		<div id="message"></div>
		<table class="wp-list-table widefat fixed striped posts">
			<thead>
			<tr>
				<th scope="col" id="14" class="manage-column column-14">Nome Form</th><th scope="col" id="19" class="manage-column column-19">Nome Template</th>
			</tr>
			</thead>
			<tbody id="the-list">
			<?php $forms = Ninja_Forms()->form()->get_forms();
				foreach( $forms as $form ) {
				$formID = $form->get_id();
				if( in_array($formID, $pdf_forms) ) { ?>
			<tr id="form-<?php echo $formID; ?>" class="iedit author-self level-0 form-<?php echo $formID; ?> type-post status-publish format-standard hentry category-annunci">
				<td class="14 column-14"><strong><?php echo $form->get_setting( 'title' ); ?></strong></td>
				<td class="title column-title has-row-actions column-primary page-title">
					<strong><a class="row-title" href="<?php echo admin_url( 'admin.php?page=edit-template-form&form_id='.$formID.'&pdf_template_Id='.$pdf_formsNm[$formID]['temp_id']); ?>"><?php echo $pdf_formsNm[$formID]['temp_name']; ?></a></strong>
					<div class="row-actions">
					<input type="hidden" class="pdf_template_Id" value="<?php echo $pdf_formsNm[$formID]['temp_id']; ?>" />
					<input type="hidden" class="form_id" value="<?php echo $formID; ?>" />
					<span class="edit"><a href="<?php echo admin_url( 'admin.php?page=edit-template-form&form_id='.$formID.'&pdf_template_Id='.$pdf_formsNm[$formID]['temp_id']); ?>">Modifica</a> | </span>
					<span class="trash"><a class="submitdelete del_pdf_template" aria-label="Sposta “LOCAZIONE LOCALE COMMERCIALE” nel cestino">Cestina</a></span>
					</div>
				</td>
			</tr>
				<?php } } ?>
			</tbody>
		</table>
    </div>
<script>
jQuery( document ).ready(function($) {
	$(".del_pdf_template").click(function() {
		var pdf_template_Id = $(this).closest(".row-actions").find(".pdf_template_Id").val();
		var form_id = $(this).closest(".row-actions").find(".form_id").val();

		if ( pdf_template_Id.length != 0 && form_id.length != 0 ) {
			var result = confirm("Want to delete the pdf Template?");
			if (result) {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'remove_PDF_templateAjax',
					'pdf_template_Id': pdf_template_Id,
					'form_id': form_id,
				},
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
						$("#message").html('<div class="notice notice-success">'+data.success+'</div>');
						window.setTimeout(function () {
							window.location.href = '?page=configurazione-template-form';
						}, 500);
					} else {
						$("#message").html('<div class="error">'+data.error+'</div>');
					}
				}
			});
			$("html, body").animate({scrollTop : 0},500);
			}
		}
	});
});
</script>
<style>
.row-actions a {
    cursor: pointer;
}
#message {
    padding: 20px 0;
}
#message .error, #message .notice {
    padding: 12px;
}
</style>
<?php }

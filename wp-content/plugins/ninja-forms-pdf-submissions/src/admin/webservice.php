<?php if (!defined('ABSPATH')) exit;

class NF_PDF_WebService
{

    public function __construct()
    {
        add_filter( 'ninja_forms_soap_filter', array($this, 'ninja_forms_soap_request'), 10,2 );
        add_filter('ninja_forms_soap', array($this, 'ninja_forms_soap_request_depreceated'), 10, 1);
    }

    // $form_actions = Ninja_Forms()->form( $form_data[ 'form_id' ] )->get_actions();
    //     $attach_pdf = false;
    //     foreach( $form_actions as $action ){
    //         $attach_pdf = $action->get_setting( 'attach_pdf' );
    //     }
    public function ninja_forms_soap_request( $action_settings,$form_data) {

        if($action_settings == null)
            return;
            
        $actions = Ninja_Forms()->form( $form_data[ 'form_id' ] )->get_actions();
        $sent_pdf = FALSE;
        
        if( ! empty( $actions ) ) {
            
            foreach ($actions as $action) {
                $type = $action->get_setting( 'type' );
                if( ! isset( Ninja_Forms()->actions[ $type ] ) ) continue;
                if($type === 'custom'){
                    $settings = $action->get_settings();
                    if( ! empty( $settings ) && isset($settings['tag']) && isset($settings['active'])) {
                        if($settings['tag'] === 'ninja_forms_soap' && $settings['active'] == 1){
                            $sent_pdf = true;
                            //break;
                        }
                    }
                }
            }
        }
        $form_id =  $form_data[ 'form_id' ];
        $pdfTemplate = new PDFTemplate;
        $pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($form_id);
		if (!isset($pdf_template_dat) || $pdf_template_dat->web_service_id == 3) {
            return apply_filters( 'ninja_forms_payment_soap_filter', $form_data);
        }

        if($sent_pdf)
        {
            $attachments = $this->_get_attachments( $action_settings, $form_data );
            return $this ->do_soap_request($form_data, $attachments,$action_settings);
        }
    }

    
    /**
     * Attach the PDF to the email.
     *
     * @param array $data
     *
     * @return array
     */
    public function ninja_forms_soap_request_depreceated($form_data)
    {   
        return true;
    }

    public function do_soap_request($form_data, $attachments,$action_settings)
    {
        global $wpdb;

        $data = array();

        $form_id =  $form_data[ 'form_id' ];
        $pdfTemplate = new PDFTemplate;
        $pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($form_id);
		
		$action_settings["ATTACHMENTS"] = $attachments;
		if (!isset($pdf_template_dat) || $pdf_template_dat->web_service_id == 2) {
            return apply_filters( 'ninja_forms_do_soap_request_filter', $data, $form_data, $action_settings, $pdf_template_dat);
        }

        $table_name = "servmask_prefix_portal";
        $query = "SELECT * FROM $table_name ";
        $soap_logins = $wpdb->get_results($query);
        $soap_login = $soap_logins[0];
        $wsdl = $soap_login->soap_url;
        $xml_post_string = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
          <Login xmlns="http://artensys.it/WeLodge/">
            <userName>'. $soap_login->soap_username .'</userName>
            <password>' . $soap_login->soap_password .'</password>
          </Login>
        </soap:Body>
      </soap:Envelope>';
      try {
        if ( WP_DEBUG === true ) {
            error_log($xml_post_string);
        }
      
        $response_data = $this->send_request($wsdl,$xml_post_string, 'http://artensys.it/WeLodge/Login', null);
        if(empty($response_data) || $response_data['http_status'] != 200){
            if ( WP_DEBUG === true ) {
                error_log('Error in sending request to ' . $wsdl);
                error_log("Error No " . $response_data['curl_errno']);
                error_log("Response is " . $response_data['response_body']);
            }
            $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
        }else {
            $auth_cookie = '';
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response_data['response'], $matches);
            foreach($matches[1] as $item) {
                parse_str($item, $cookie);
                foreach($cookie as $key=>$value){
                    if(stripos($key,"WeLodge") === 1){
                        $key = str_replace('_WeLodge', '.WeLodge',$key );
                        $auth_cookie = $key . '=' .$value;                      
                        break;
                    }
                    // else if(stripos($key,"ASPXANONYMOUS=") === 1){
                    //     $auth_cookie = ".ASPXANONYMOUS==" .$value;
                    // }
                }
            }
            $form_id =  $form_data[ 'form_id' ];


            $pdf_file_path = '';
             if(!empty($attachments)){
                $pdf_file_path = $attachments[0];
            }

            //$pdf_file_name = basename($pdf_file_path);
            $pdf = file_get_contents($pdf_file_path);
            $pdf_encoded_string = base64_encode($pdf);
            $subject = '';
            $business = '';
            $protocol_category_id = '';
            $titolario_id = '';  
            $fascicle_id = '';
            $type_id = '';
            $sender_uor = '';
            $recipient_uor = '';
            $pdfTemplate = new PDFTemplate;
            $pdf_template_dat = $pdfTemplate->get_PDF_temppale_byFormId($form_id);
            if (!empty($pdf_template_dat)) {
                
                $mp_id = $pdf_template_dat->mpId;
                $pdf_template_suffix = $pdf_template_dat->templateSuffix;
                $document_template = $pdfTemplate->get_document_data($mp_id);
                if(!empty($document_template)){
                    $user = wp_get_current_user();
                    $predefined_address_dat = $pdfTemplate->get_address_data($user->user_email);
                    $predefined_address_id = -1;
                    if(!empty($predefined_address_dat)){
                        $predefined_address_id = $predefined_address_dat[0]->id;
                    }
                    $user_name = $user->display_name;
                    $user_first_name = trim($user_name);
                    $user_last_name = (strpos($user_name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $user_name);
                    $user_first_name = trim( preg_replace('#'.$user_last_name.'#', '', $user_name ) );
                    $subject= $form_data['settings']['title'] . ' - ' . $user_last_name . ' - '. $user_first_name . ' - ' . $user->user_email;
                    $sub_id = ( isset( $form_data[ 'actions' ][ 'save' ] ) ) ? $form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                    if(!empty($pdf_template_suffix)){
                        $nf_fields = Ninja_Forms()->form( $form_id )->get_fields();
                        $sub = Ninja_Forms()->form()->get_sub( $sub_id );
                        $fields = new NF_PDF_Adapter_Submission( $nf_fields, $form_id, $sub );
                        $fields = apply_filters( 'nf_sub_document_fields',$fields );
                        foreach ( $fields as $field ) {
                            if( isset( $field[ 'admin_label' ] ) && $field[ 'admin_label' ] ) {
                                $field_label = $field[ 'admin_label' ];
                            } else {
                                $field_label = $field[ 'label' ];
                            }

                            foreach( $nf_fields as $nf_field ){
                                if($nf_field->get_setting( 'label' ) == $field_label ) {
                                    $field['pdf_format'] = 'html';
                                    $fieldKey = '%'.$nf_field->get_setting( 'key' ).'%';
                                    $field_value = ( isset( $field[ 'value' ] ) ) ? $field[ 'value' ] : null;
            
                                    $field_value = apply_filters( 'ninja_forms_pdf_pre_user_value', $field_value, array() );
            
                                    if ( is_array( $field_value ) ) {
                                        $field_value = implode( ", ", $field_value );
                                    }
            
                                    $field_value = apply_filters( 'ninja_forms_pdf_field_value', html_entity_decode( $field_value ), $field_value, $field );
                                    $pdf_template_suffix = str_replace($fieldKey, $field_value, $pdf_template_suffix);
                                }
                            }
                        }
                        $subject = $subject . ' - ' .  $pdf_template_suffix;
                    }
                    $pdf_file_name = $this->get_pdf_file_name( $form_data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';
                    //$subject = $document_template[0]->Subject;
                    $business = $document_template[0]->BuId;
                    $protocol_category_id= $document_template[0]->ProtocolCategoryId;
                    $titolario_id= $document_template[0]->TitolarioID;
                    $fascicle_id= $document_template[0]->FascicleID;
                    $type_id= $document_template[0]->TypeID;
                    $sender_uor_val= $document_template[0]->SenderUOR;
                    $recipient_uor_val= $document_template[0]->RecipientUOR;
                    $sender_uor = '';
                    $recipient_uor = '';

                    if (!empty($sender_uor_val)) {
                        $sender_uor_arr = explode(",",$sender_uor_val);
                        foreach ($sender_uor_arr as $value) {
                            if(empty($sender_uor)){
                                $sender_uor = '<wel1:int>' . $value . '</wel1:int>';
                            }else {
                                $sender_uor .=  '<wel1:int>' . $value . '</wel1:int>';
                            }
                        }
                    }
                    
                    if (!empty($recipient_uor_val)) {
                        $recipient_uor_arr = explode(",",$recipient_uor_val);
                        foreach ($recipient_uor_arr as $value) {
                            if(empty($recipient_uor)){
                                $recipient_uor = '<wel1:int>' . $value . '</wel1:int>';
                            }else {
                                $recipient_uor .= '<wel1:int>' . $value . '</wel1:int>';
                            }
                        }
                    }

                    $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wel="http://artensys.it/WeLodge/"
                    xmlns:wel1="http://sviluppoimpresa.it/WeLodge/">
                        <soapenv:Header/>
                        <soapenv:Body>
                        <wel:CreateDocumentFromFile>
                        <wel:p>
                        <wel:Document>
                        <wel1:Subject>' . $subject . '</wel1:Subject>
                        <wel1:Note></wel1:Note>
                        <wel1:BusinessUnitID>' . $business . '</wel1:BusinessUnitID>
                        <wel1:SourceID>1</wel1:SourceID>
                        <wel1:CategoryID>' . $protocol_category_id . '</wel1:CategoryID>
                        <wel1:TitolarioID>' . $titolario_id . '</wel1:TitolarioID>
                        <wel1:FascicleID>' . $fascicle_id . '</wel1:FascicleID>
                        <wel1:TypeID>' . $type_id . '</wel1:TypeID>
                        <wel1:SenderUORs>' . $sender_uor . '</wel1:SenderUORs>
                        <wel1:RecipientUORs>' . $recipient_uor . '</wel1:RecipientUORs>
                        <wel1:ShowCase>0</wel1:ShowCase>
                        <wel1:IsSubscribed>true</wel1:IsSubscribed>
                        <wel1:Inventory></wel1:Inventory>
                        <wel1:IsPublic>false</wel1:IsPublic>
                        </wel:Document>';

                        if($predefined_address_id > 0){
                            $xml_post_string = $xml_post_string . 
                            '<wel:PredefinedAddresses>
                                <wel:PredefinedAddressParameters>
                                    <wel:AddressId>' .  $predefined_address_id . '</wel:AddressId>
                                    <wel:GwToCC>0</wel:GwToCC>
                                    <wel:GwIsConfirmationRequired>0</wel:GwIsConfirmationRequired>
                                </wel:PredefinedAddressParameters>
                            </wel:PredefinedAddresses> 
                            ';
                        }else {
                            $xml_post_string = $xml_post_string . '
                            <wel:CustomAddresses>
                                <wel:CustomAddressParameters>
                                    <wel1:OrganizationCode></wel1:OrganizationCode>
                                    <wel1:OrganizationName>' . $user_last_name . ' '. $user_first_name  . '</wel1:OrganizationName>
                                    <wel1:BusinessUnit></wel1:BusinessUnit>
                                    <wel1:Address></wel1:Address>
                                    <wel1:ZIPCode></wel1:ZIPCode>
                                    <wel1:City></wel1:City>
                                    <wel1:Phone></wel1:Phone>
                                    <wel1:Email> ' . $user->user_email  . '</wel1:Email>
                                    <wel1:PecEmail></wel1:PecEmail>
                                    <wel1:Note></wel1:Note>
                                    <wel:GwToCC>0</wel:GwToCC>
                                    <wel:GwIsConfirmationRequired>0</wel:GwIsConfirmationRequired>
                                </wel:CustomAddressParameters>
                            </wel:CustomAddresses>
                            ';
                        }

                        $xml_post_string = $xml_post_string . '<wel:Data>' . $pdf_encoded_string . '</wel:Data>
                        <wel:FileName>' . $pdf_file_name . '</wel:FileName>
                        <wel:FileType>11</wel:FileType>
                        <wel:IsSigned>false</wel:IsSigned>
                        <wel:CheckOriginality>false</wel:CheckOriginality>
                        </wel:p>
                        </wel:CreateDocumentFromFile>
                        </soapenv:Body>
                        </soapenv:Envelope>';

                        //<wel1:Subject>Oggetto</wel1:Subject>
                    // $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wel="http://artensys.it/WeLodge/"
                    // xmlns:wel1="http://sviluppoimpresa.it/WeLodge/">
                    // <soapenv:Header/>
                    // <soapenv:Body>
                    //                         <wel:CreateDocumentFromFile>
                    //                         <wel:p>
                    //                         <wel:Document>
                    //                         <wel1:Subject>' . $subject . '</wel1:Subject>                  
                    //                         <wel1:Note>note</wel1:Note>
                    //                         <wel1:BusinessUnitID>1</wel1:BusinessUnitID>
                    //                         <wel1:SourceID>1</wel1:SourceID>
                    //                         <wel1:CategoryID>68</wel1:CategoryID>
                    //                         <wel1:TitolarioID>2</wel1:TitolarioID>
                    //                         <wel1:FascicleID>30</wel1:FascicleID>
                    //                         <wel1:TypeID>1</wel1:TypeID>
                    //                         <wel1:SenderUORs>
                    //                         <wel1:int>8</wel1:int>
                    //                         </wel1:SenderUORs>
                    //                         <wel1:RecipientUORs>
                    //                         <wel1:int>8</wel1:int>
                    //                         </wel1:RecipientUORs>
                    //                         <wel1:ShowCase>true</wel1:ShowCase>
                    //                         <wel1:IsSubscribed>true</wel1:IsSubscribed>
                    //                         <wel1:Inventory></wel1:Inventory>
                    //                         <wel1:IsPublic>true</wel1:IsPublic>
                    //                         </wel:Document>';
                    //                         if($predefined_address_id > 0){
                    //                             $xml_post_string = $xml_post_string . 
                    //                             '<wel:PredefinedAddresses>
                    //                                 <wel:PredefinedAddressParameters>
                    //                                     <wel:AddressId>' .  $predefined_address_id . '</wel:AddressId>
                    //                                     <wel:GwToCC>0</wel:GwToCC>
                    //                                     <wel:GwIsConfirmationRequired>0</wel:GwIsConfirmationRequired>
                    //                                 </wel:PredefinedAddressParameters>
                    //                             </wel:PredefinedAddresses> 
                    //                             ';
                    //                         }else {
                    //                             $xml_post_string = $xml_post_string . '
                    //                             <wel:CustomAddresses>
                    //                                 <wel:CustomAddressParameters>
                    //                                     <wel1:OrganizationCode></wel1:OrganizationCode>
                    //                                     <wel1:OrganizationName>' . $user_last_name . ' '. $user_first_name  . '</wel1:OrganizationName>
                    //                                     <wel1:BusinessUnit></wel1:BusinessUnit>
                    //                                     <wel1:Address></wel1:Address>
                    //                                     <wel1:ZIPCode></wel1:ZIPCode>
                    //                                     <wel1:City></wel1:City>
                    //                                     <wel1:Phone></wel1:Phone>
                    //                                     <wel1:Email> ' . $user->user_email  . '</wel1:Email>
                    //                                     <wel1:PecEmail></wel1:PecEmail>
                    //                                     <wel1:Note></wel1:Note>
                    //                                     <wel:GwToCC>0</wel:GwToCC>
                    //                                     <wel:GwIsConfirmationRequired>0</wel:GwIsConfirmationRequired>
                    //                                 </wel:CustomAddressParameters>
                    //                             </wel:CustomAddresses>
                    //                             ';
                    //                         }
                                            
                    //                         $xml_post_string = $xml_post_string . '<wel:Data>' . $pdf_encoded_string . '</wel:Data>
                    //                         <wel:FileName>' . $pdf_file_name . '</wel:FileName>
                    //                         <wel:FileType>11</wel:FileType>
                    //                         <wel:IsSigned>false</wel:IsSigned>
                    //                         <wel:CheckOriginality>false</wel:CheckOriginality>
                    //                         </wel:p>
                    //                         </wel:CreateDocumentFromFile>
                    //                         </soapenv:Body>
                    //                         </soapenv:Envelope>';
                        
                        if ( WP_DEBUG === true ) {
                            error_log($xml_post_string);
                        }

                        $response_data = $this->send_request($wsdl,$xml_post_string,'http://artensys.it/WeLodge/CreateDocumentFromFile', $auth_cookie);
                        if(empty($response_data) || $response_data['http_status'] != 200){
                            if ( WP_DEBUG === true ) {
                                error_log('Error in sending request to ' .'http://artensys.it/WeLodge/CreateDocumentFromFile');
                                error_log($response_data['response']);
                            }
                            $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                        }else {
                            $response_body = $response_data['response_body'];
                            $response_body = str_replace("<soap:Body>","",$response_body);
                            $response_body = str_replace("</soap:Body>","",$response_body);
                            
                            $xml = simplexml_load_string($response_body);
                            $result_element = $xml->CreateDocumentFromFileResponse->CreateDocumentFromFileResult;
                            $error_code = $this->xmlobject_to_array($result_element->ErrorCode);
                            if($error_code[0] != 0){
                                if ( WP_DEBUG === true ) {
                                    error_log("CreateDocumentFromFile Response Error code is " . $error_code[0]);
                                    error_log($response_data['response']);
                                }
                                $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                            }else {
                               // $sub_id = ( isset( $form_data[ 'actions' ][ 'save' ] ) ) ? $form_data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                                $protocol_field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolid' AND `parent_id` = {$form_id}" );
                                $protocol_field_number = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolnumber' AND `parent_id` = {$form_id}" );
                                $protocol_field_date = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocoldate' AND `parent_id` = {$form_id}" );

                                $tag = $this->xmlobject_to_array($result_element->ProtocolResult->ProtocolId);
                                $protocol_id =  $tag[0];
                                update_post_meta( $sub_id, '_field_' . $protocol_field_id , $tag[0] );
                                $tag = $this->xmlobject_to_array($result_element->ProtocolResult->ProtocolNumber);
                                update_post_meta( $sub_id, '_field_' . $protocol_field_number , $tag[0] );
                                $tag = $this->xmlobject_to_array($result_element->ProtocolResult->ProtocolDate);
                                $protocol_field_fmt = date('d/m/Y', strtotime($tag[0]));
                                update_post_meta( $sub_id, '_field_' . $protocol_field_date , $protocol_field_fmt);
                                $xml_post_string = '
                                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
                                    xmlns:wel="http://artensys.it/WeLodge/">
                                    <soapenv:Header/>
                                    <soapenv:Body>
                                        <wel:GetDocumentPdfById>
                                            <wel:id>' . $protocol_id .  '</wel:id>
                                        </wel:GetDocumentPdfById>
                                    </soapenv:Body>
                                </soapenv:Envelope>';
                              try {
                                if ( WP_DEBUG === true ) {
                                    error_log($xml_post_string);
                                }
                              
                                $response_data = $this->send_request($wsdl,$xml_post_string, 
                                        'http://artensys.it/WeLodge/GetDocumentPdfById', $auth_cookie);

                                if(empty($response_data) || $response_data['http_status'] != 200){
                                    if ( WP_DEBUG === true ) {
                                        error_log('Error in sending request to ' .'http://artensys.it/WeLodge/GetDocumentPdfById');
                                        error_log($response_data['response']);
                                    }
                                    $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                                }else {
                                    $response_body = $response_data['response_body'];
                                    $response_body = str_replace("<soap:Body>","",$response_body);
                                    $response_body = str_replace("</soap:Body>","",$response_body);
                                    if ( WP_DEBUG === true ) {
                                        error_log("GetDocumentPdfById Response is " . $response_body);
                                    }
                                    
                                    $xml = simplexml_load_string($response_body);
                                    $result_element = $xml->GetDocumentPdfByIdResponse->GetDocumentPdfByIdResult;
                                    $error_code = $this->xmlobject_to_array($result_element->ErrorCode);
                                    if($error_code[0] != 0){
                                        if ( WP_DEBUG === true ) {
                                            error_log("GetDocumentPdfById Response Error code is " . $error_code[0]);
                                        }
                                        $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                                    }else {
                                        $tag = $this->xmlobject_to_array($result_element->Data);
                                        $pdf_content = $tag[0];
                                        $pdf_decoded = base64_decode ($pdf_content);
                                        // create temporary file
                                        $new_pdf_file_name = $this->create_temp_file( $pdf_decoded, $sub_id,$form_data['settings']['title'] );
                                        if (class_exists('UserFascicle')) {
                                            $active_plugins_basenames = get_option( 'active_plugins' );
                                            foreach ( $active_plugins_basenames as $plugin_basename ) {
                                                if ( false !== strpos( $plugin_basename, '/UserFascicle.php' ) ) {
                                                    $plugin_dir = WP_PLUGIN_DIR . '/' . $plugin_basename;
                                                    require_once $plugin_dir;
                                                    $uf_plugin  = new UserFascicle();
                                                    $sub_id = ( isset( $data[ 'actions' ][ 'save' ] ) ) ? $data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                                                    $uf_plugin->store_pdf_fascicle_folder($new_pdf_file_name,$form_data['settings']['title'], $sub_id, $form_id);
                                                    break;
                                                }
                                            }
                                          }else {
                                            error_log("User Fascicle not installed");
                                          }
                                    }
                                }
                                
                            }catch ( Exception $e ){
                                if ( WP_DEBUG === true ) {
                                    error_log('Error in sending request to ' . $wsdl . ' ' .$e->getMessage());
                                }
                                $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                                return $data;
                            }
                                
                            }
                        }
                        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wel="http://artensys.it/WeLodge/">
                        <soapenv:Header/>
                            <soapenv:Body>
                                <wel:Logout/>
                            </soapenv:Body>
                        </soapenv:Envelope>';
                        $response_data = $this->send_request($wsdl,$xml_post_string,'http://artensys.it/WeLodge/Logout', $auth_cookie);
                }else {
                    if ( WP_DEBUG === true ) {
                        error_log('No DocumentTemplate associated with protocol_template_id ' . $mp_id);
                    }
                    $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
                }
            }else {
                if ( WP_DEBUG === true ) {
                    error_log('No PDF template associated with ' . $form_id);
                }
                $data[ 'error' ] = 'Error in sending document to external service. No PDF template associated with ' . $form_id;
            }
            
        }
        // if(!empty($data[ 'error' ])){
        //     $errors = array();
        //     $errors[ 'pdf_not_sent' ] = $data[ 'error' ];
        //     $form_data[ 'errors' ][ 'form' ] = $errors;
        // }
        
        return $data;
      }catch ( Exception $e ){
        if ( WP_DEBUG === true ) {
            error_log('Error in sending request to ' . $wsdl . ' ' .$e->getMessage());
            
        }
        $data[ 'error' ] = 'Si sono verificati problem con l’invio della richiesta al sistema di protocollazione, si prega di riprovare';
        return $data;
    }
    }

    public function send_request($wsdl,$xml_post_string, $action, $cookie){
        $soap_do = curl_init();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction:". $action, 
            "Content-Length: ".strlen($xml_post_string)
        );

        if(!empty($cookie)){
            $headers[] = 'COOKIE:' . $cookie;
        }

        curl_setopt($soap_do, CURLOPT_URL, $wsdl);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT,        10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST,           true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $headers);
        curl_setopt($soap_do, CURLOPT_HEADER, 1);
        curl_setopt($soap_do, CURLOPT_FAILONERROR, FALSE);
        $response  = curl_exec($soap_do);
        $header_size = curl_getinfo($soap_do, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $http_status = curl_getinfo($soap_do, CURLINFO_HTTP_CODE);
        $curl_errno= curl_errno($soap_do);
        curl_close($soap_do);

        $response = str_replace("<soap:Body>","",$response) ;
        $response = str_replace("</soap:Body>","",$response);

        $response_data = array(
            'response' => $response,
            'http_status' => $http_status,
            'curl_errno' => $curl_errno,
            'response_body' => $body
       );
       
        return $response_data;
    }

    public function xmlobject_to_array($simle_xml_obj){
        return json_decode(json_encode((array) $simle_xml_obj), 1);
    }

    private function _get_attachments( $settings, $data )
    {
        $attachments = array();

        if( ! isset( $settings[ 'id' ] ) ) $settings[ 'id' ] = '';

        $attachments = apply_filters( 'ninja_forms_action_email_attachments', $attachments, $data, $settings );
        return $attachments;
    }

    	/**
	 * Create temporary file
	 *
	 * @return string - location of file
	 */
	public function create_temp_file( $content, $sub_id, $form_name ) {

		// create temporary file
		$path = tempnam( get_temp_dir(), 'Sub' );
		$temp_file = fopen( $path, 'r+' );

		// write to temp file
		fwrite( $temp_file, $content );
		fclose( $temp_file );

		// find the directory we will be using for the final file
		$path = pathinfo( $path );
		$dir = $path['dirname'];
		$basename = $path['basename'];

		// create name for file
		$new_name = $this->get_pdf_file_name( $form_name . '-' . $sub_id, $sub_id );
		// remove a file if it already exists
		if ( file_exists( $dir.'/'.$new_name.'.pdf' ) ) {
			unlink( $dir.'/'.$new_name.'.pdf' );
		}

		// move file
        rename( $dir.'/'.$basename, $dir.'/'.$new_name.'.pdf' );
        return $dir.'/'.$new_name.'.pdf';
    }
    
    	/**
	 * Add PDF download link on the view form submission page
	 *
	 * @param $name   name of the file
	 * @param $sub_id submission id
	 * @since 1.3.3
	 */
	public function get_pdf_file_name( $name = 'ninja-forms-submission', $sub_id = '' ) {
        $seq_num = get_post_meta( $sub_id, '_seq_num', TRUE );
        $name = "RichiestaOnLine_" . $seq_num . $sub_id . $this->random_strings(6);
		return apply_filters( 'ninja_forms_submission_pdf_name', $name, $sub_id );
    }
    
    public function random_strings($length_of_string) 
    { 
    
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
    
        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result),  
                        0, $length_of_string); 
    } 
}

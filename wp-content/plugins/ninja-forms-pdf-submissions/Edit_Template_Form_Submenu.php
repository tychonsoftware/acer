<?php
if ( ! defined( 'ABSPATH' ) ) exit;

add_action('admin_menu', 'wpdocs_register_edit_template_form');

function wpdocs_register_edit_template_form() {
    $submenu = add_submenu_page(
        'admin.php?page=ninja-forms',
        'Edit Template Form',
        'Edit Template Form',
        'manage_options',
        'edit-template-form',
        'edit_template_form_callback' );
    add_action( 'admin_print_scripts-' . $submenu,  function () {
        wp_enqueue_script('underscore');
    });
}

function edit_template_form_callback() {

include_once plugin_dir_path( __FILE__ ) . 'pdf_template_includes.php';

$pdfTemplate = new PDFTemplate;

if (isset($_REQUEST['form_id']) && $_REQUEST['form_id'] != '') {
	$form_id = $_REQUEST['form_id'];
}
if (isset($_REQUEST['pdf_template_Id']) && $_REQUEST['pdf_template_Id'] != '') {
	$pdf_template_Id = $_REQUEST['pdf_template_Id'];
}

$pageLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$isWeblogeService = false;
$comune_terni_cognome = '';
$comune_terni_nome = '';
$comune_terni_email = '';
$comune_terni_codice_fiscale = '';
$comune_terni_searchvalue = '';
$comune_terni_soggetto = '';
$comune_terni_path = '';
$pago_pa_tipo_identificativo = '';
$pago_pa_codice_identificativo = '';
$pago_pa_anagrafica_pagatore = '';
$pago_pa_indirizzo_pagatore = '';
$pago_pa_civico_pagatore = '';
$pago_pa_cap_pagatore = '';
$pago_pa_localita_pagatore = '';
$pago_pa_provincia_pagatore = '';
$pago_pa_nazione_pagatore = '';
$pago_pa_email_pagatore = '';
$pago_pa_identificativo_univoco_dovuto = '';
$pago_pa_importo_singolo_versamento = '';
$pago_pa_identificativo_tipo_dovuto = '';
$pago_pa_causale_versamento = '';
$pago_pa_dati_specifici_riscossione = '';

if($pdf_template_Id > 0) {
	$pdf_template_dat = $pdfTemplate->get_PDF_template($pdf_template_Id);
	$pdf_template_name = $pdf_template_dat->templateName;
	$mp_id = $pdf_template_dat->mpId;
    $webServiceId = $pdf_template_dat->web_service_id;
    if ($webServiceId == 1) {
        $isWeblogeService = true;
    }
	$mp_Name = $pdf_template_dat->mpName;
	$pdf_template_content = $pdf_template_dat->templateDat;
	if(!isset($pdf_template_dat->templateSuffix))
	{
		$pdfTemplate->ninja_forms_suffix_column();
		$pdf_template_dat = $pdfTemplate->get_PDF_template($pdf_template_Id);
	}
	$pdf_template_suffix = $pdf_template_dat->templateSuffix;
	$userIds = explode(',',$pdf_template_dat->user_ids);
	$metadata = json_decode($pdf_template_dat->metadata, true);
    $comune_terni_cognome = $metadata['comune_terni_cognome'];
    $comune_terni_nome = $metadata['comune_terni_nome'];
    $comune_terni_email = $metadata['comune_terni_email'];
    $comune_terni_codice_fiscale = $metadata['comune_terni_codice_fiscale'];
    $comune_terni_searchvalue = $metadata['comune_terni_searchvalue'];
    $comune_terni_path = $metadata['comune_terni_path'];
    $comune_terni_soggetto = $metadata['comune_terni_soggetto'];
	$pago_pa_tipo_identificativo = $metadata['pago_pa_tipo_identificativo'];
	$pago_pa_codice_identificativo = $metadata['pago_pa_codice_identificativo'];
	$pago_pa_anagrafica_pagatore = $metadata['pago_pa_anagrafica_pagatore'];
	$pago_pa_indirizzo_pagatore = $metadata['pago_pa_indirizzo_pagatore'];
	$pago_pa_civico_pagatore = $metadata['pago_pa_civico_pagatore'];
	$pago_pa_cap_pagatore = $metadata['pago_pa_cap_pagatore'];
	$pago_pa_localita_pagatore = $metadata['pago_pa_localita_pagatore'];
	$pago_pa_provincia_pagatore = $metadata['pago_pa_provincia_pagatore'];
	$pago_pa_nazione_pagatore = $metadata['pago_pa_nazione_pagatore'];
	$pago_pa_email_pagatore = $metadata['pago_pa_email_pagatore'];
	$pago_pa_identificativo_univoco_dovuto = $metadata['pago_pa_identificativo_univoco_dovuto'];
	$pago_pa_importo_singolo_versamento = $metadata['pago_pa_importo_singolo_versamento'];
	$pago_pa_identificativo_tipo_dovuto = $metadata['pago_pa_identificativo_tipo_dovuto'];
	$pago_pa_causale_versamento = $metadata['pago_pa_causale_versamento'];
	$pago_pa_dati_specifici_riscossione = $metadata['pago_pa_dati_specifici_riscossione'];

}

$forms = Ninja_Forms()->form()->get_forms();
$pdf_templates = $pdfTemplate->get_PDF_templates();
$pdf_forms = array();
foreach( $pdf_templates as $pdf_template ){
	$pdf_forms[] = $pdf_template->formID;
}

$doc_templates = $pdfTemplate->get_doc_templates();
$webServices = $pdfTemplate->get_web_services();
$wpusers = get_users(array());
/*
$nf_field = array();
$nf_field_labels = array();
$nf_fields = Ninja_Forms()->form( 1 )->get_fields();
foreach( $nf_fields as $field ){
	$nf_field = $field->get_settings();
	$nf_field_labels[] = $field->get_setting( 'label' );
}
*/
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" rel="stylesheet" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>

<div id="errori" style="display:none"></div>
    <div class="wrap">
        <h2>Edit Template Form</h2>
		<form name="post" action="<?php echo $pageLink; ?>" method="post" id="pdfTemplateForm">
		<div id="message"></div>
		<input type="hidden" name="pdf_template_Id" id="pdf_template_Id" value="<?php echo $pdf_template_Id; ?>" />
		<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
		<div id="post-body-content" style="position: relative;">
		<div id="titlediv"><div id="titlewrap">
			<input type="text" name="pdf_template_name" id="pdf_template_name" spellcheck="true" autocomplete="off" value="<?php echo $pdf_template_name; ?>" placeholder="Nome Template" />
		</div></div>
		<?php // var_dump($nf_field_labels); ?>
		<p><select name="form_id" id="form_id">
			<option value="">Select Form</option>
			<?php foreach( $forms as $form ){
			$formID = $form->get_id();
			if( !in_array($formID, $pdf_forms) || $formID == $form_id ) {
			if($formID == $form_id) {
				$selForm = 'selected';
			} else {
				$selForm = '';
			} ?>
			<option value="<?php echo $form->get_id(); ?>" <?php echo $selForm; ?> ><?php echo $form->get_setting( 'title' ); ?></option>
		<?php } } ?>
		</select></p>

    <p>
      <select name="web_service_id" id="web_service_id" onchange="">
        <option value="">Seleziona web service</option>
        <?php foreach( $webServices as $webService ): ?>
        <option value="<?= $webService->id ?>" <?php if ($webServiceId == $webService->id): ?> selected <?php endif; ?> ><?= $webService->web_service_name ?></option>
        <?php endforeach; ?>
      </select>
    </p>

		<p <?php if (!$isWeblogeService): ?> class="hidden" <?php endif; ?>>
      <select name="mp_id" id="mp_id">
			<option value="">Modello Protocollo</option>
			<?php foreach( $doc_templates as $doc_template ) {
				$sel = '';
				if($mp_id == $doc_template->Id) {
					$sel = 'selected';
				}
				echo '<option value="'.$doc_template->Id.'" '.$sel.'>'.$doc_template->Name.'</option>';
			} ?>
		</select>
		<input type="hidden" name="mp_Name" id="mp_Name" value="<?php echo $mp_Name; ?>" /></p>
    <p <?php if (!$isWeblogeService): ?> class="hidden" <?php endif; ?>>
      <input type="text" name="pdf_template_suffix" id="pdf_template_suffix" autocomplete="off" value="<?php echo $pdf_template_suffix; ?>" placeholder="Suffisso Oggetto Protocollo" />
    </p>

        <div class="comune_terni_service_options">
            <div class="half">
                <select name="comune_terni_cognome" id="comune_terni_cognome"
                        data-value="<?php echo $comune_terni_cognome  ?>"
                        class="comune_terni_service_select_map">
                    <option value="">Select Cognome</option>
                </select>
            </div>
            <div class="half last">
                <select name="comune_terni_nome" id="comune_terni_nome"
                        data-value="<?php echo $comune_terni_nome  ?>"
                        class="comune_terni_service_select_map">
                    <option value="">Select Nome</option>
                </select>
            </div>
            <div class="half">
                <select name="comune_terni_email" id="comune_terni_email"
                        data-value="<?php echo $comune_terni_email  ?>"
                        class="comune_terni_service_select_map">
                    <option value="">Select Email</option>
                </select>
            </div>
            <div class="half last">
                <select name="comune_terni_codice_fiscale" id="comune_terni_codice_fiscale"
                        data-value="<?php echo $comune_terni_codice_fiscale  ?>"
                        class="comune_terni_service_select_map">
                    <option value="">Select Codice Fiscale</option>
                </select>
            </div>
            <div class="half">
                <input type="text" placeholder="Search Value" name="comune_terni_searchvalue" id="comune_terni_searchvalue" value="<?php echo $comune_terni_searchvalue ?>">
            </div>
            <div class="half last">
                <input type="text" placeholder="Oggetto" name="comune_terni_soggetto" id="comune_terni_soggetto" value="<?php echo $comune_terni_soggetto ?>">
            </div>
            <div class="half">
                <input type="text" placeholder="Topic Path" name="comune_terni_path" id="comune_terni_path" value="<?php echo $comune_terni_path ?>">
            </div>
            <div class="clear"></div>
        </div>

		<div class="pago_pa_service_options">
            <div class="half">
                <select name="pago_pa_tipo_identificativo" id="pago_pa_tipo_identificativo"
                        data-value="<?php echo $pago_pa_tipo_identificativo  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Tipo Identificativo Univoco</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_codice_identificativo" id="pago_pa_codice_identificativo"
                        data-value="<?php echo $pago_pa_codice_identificativo  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Codice Identificativo Univoco</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_anagrafica_pagatore" id="pago_pa_anagrafica_pagatore"
                        data-value="<?php echo $pago_pa_anagrafica_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Anagrafica Pagatore</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_indirizzo_pagatore" id="pago_pa_indirizzo_pagatore"
                        data-value="<?php echo $pago_pa_indirizzo_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Indirizzo Pagatore</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_civico_pagatore" id="pago_pa_civico_pagatore"
                        data-value="<?php echo $pago_pa_civico_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Civico Pagatore</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_cap_pagatore" id="pago_pa_cap_pagatore"
                        data-value="<?php echo $pago_pa_cap_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Cap Pagatore</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_localita_pagatore" id="pago_pa_localita_pagatore"
                        data-value="<?php echo $pago_pa_localita_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Localita Pagatore</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_provincia_pagatore" id="pago_pa_provincia_pagatore"
                        data-value="<?php echo $pago_pa_provincia_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Provincia Pagatore</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_nazione_pagatore" id="pago_pa_nazione_pagatore"
                        data-value="<?php echo $pago_pa_nazione_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Nazione Pagatore</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_email_pagatore" id="pago_pa_email_pagatore"
                        data-value="<?php echo $pago_pa_email_pagatore  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Email Pagatore</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_identificativo_univoco_dovuto" id="pago_pa_identificativo_univoco_dovuto"
                        data-value="<?php echo $pago_pa_identificativo_univoco_dovuto  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Identificativo Univoco Dovuto</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_importo_singolo_versamento" id="pago_pa_importo_singolo_versamento"
                        data-value="<?php echo $pago_pa_importo_singolo_versamento  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Importo Singolo Versamento</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_identificativo_tipo_dovuto" id="pago_pa_identificativo_tipo_dovuto"
                        data-value="<?php echo $pago_pa_identificativo_tipo_dovuto  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Identificativo Tipo Dovuto</option>
                </select>
            </div>
			<div class="half last">
                <select name="pago_pa_causale_versamento" id="pago_pa_causale_versamento"
                        data-value="<?php echo $pago_pa_causale_versamento  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Causale Versamento</option>
                </select>
            </div>
			<div class="half">
                <select name="pago_pa_dati_specifici_riscossione" id="pago_pa_dati_specifici_riscossione"
                        data-value="<?php echo $pago_pa_dati_specifici_riscossione  ?>"
                        class="pago_pa_service_select_map">
                    <option value="">Select Dati Specifici Riscossione</option>
                </select>
            </div>
            <div class="clear"></div>
        </div>
		<div class="half"><p><select name="form_keys" id="form_keys">
		<option value="">Select Key</option>
		</select></p></div>
		<div class="half last"><p><input type="text" name="form_key" id="form_key" placeholder="Copy field tag!" /></p></div>
		<div class="clear"></div>
		<p>
		<label class="control-label">Seleziona Utente</label>
		<select name="user_ids" id="user_ids" multiple="multiple">
			<?php   foreach( $wpusers as $user ) {
				$sel = '';
				
				for($i=0; $i<count($userIds); $i++){
					if($userIds[$i] == $user->data->ID) {
						$sel = 'selected';
					}
					
				}
				echo '<option value="'.$user->data->ID.'" '.$sel.'>'.$user->data->display_name.'</option>';
			}  ?>
		</select>
		</p>
		<p><?php

		// $args = array(
		// 	    'media_buttons' => false,
		// 		'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close,table' ),
		// 		'tinymce'       => array(
		// 			'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink,table'
		// 		),
		// 	);

		$args =  array(
			'wpautop'=>true,
			'textarea_name' => 'Note',
			'textarea_rows' => 10,
			  'teeny' => TRUE,
			  'media_buttons' => false,
			  'tinymce' => array(
				  'toolbar1' => 'formatselect, fontsizeselect,fontselect, bold, italic, strikethrough,bullist,numlist,outdent,indent,blockquote,forecolor,backcolor,|,alignleft,aligncenter,alignright,alignjustify,|,link,unlink,wp_more,|,spellchecker,fullscreen,image,media,|,removeformat, table',
				  'toolbar2' => '',
				  'plugins' => 'colorpicker,lists,fullscreen,image,wordpress,wpeditimage,wplink,textcolor,media,spellchecker, table'
			  ));
			wp_editor( $pdf_template_content, 'pdf_template_content', $args ); ?>
		<?php /* <textarea name="pdf_template_content" id="pdf_template_content"><?php echo $pdf_template_content; ?></textarea>*/ ?></p>
		<input type="hidden" id="previewurl" name="previewurl" value="<?php echo plugin_dir_url(dirname(__FILE__) . '/Edit_Template_Form_Submenu.php') ?>">
		<p><button type="button" id="save_pdf_template" class="button button-primary button-large">Salva</button> <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=configurazione-template-form" class="button button-primary button-large">Annulla</a>
		<button type="button" id="preview_pdf_template" class="button button-primary button-large">Preview PDF</button></p>
		</div>
		</div>
		</div>
		</form>
    </div>

<style>
.request-overlay {
    z-index: 9999;
    position: fixed; /*Important to cover the screen in case of scolling content*/
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: block;
    text-align: center;
    background: rgba(200,200,200,0.5) url("<?php echo plugin_dir_url(dirname(__FILE__) . '/Edit_Template_Form_Submenu.php') . 'assets/img/pbar-ani.gif'?>") no-repeat center; /*.gif file or just div with message etc. however you like*/
}
.btn-group{
	width:100% !important;
}
button.multiselect.dropdown-toggle.btn.btn-default{
	text-align:left;
	width:100% !important;
}
ul.multiselect-container.dropdown-menu{
	width:100% !important;
	height: 200px;
    overflow-y: scroll;
	
}
.multiselect-container>li>a>label>input[type=checkbox]{
	margin-top:2px;
}
</style>

<script>
/*
var editor;
    ClassicEditor
        .create( document.querySelector( '#pdf_template_content' ) )
		.then( newEditor => {
			editor = newEditor;
		} )
        .catch( error => {
            console.error( error );
        } );
*/

var jsonNinjaFormFieldsGroupByFormId = <?php echo $pdfTemplate->getJSONNinjaFormFieldsGroupByFormId() ?>;

jQuery( document ).ready(function($) {
	$('#user_ids').multiselect();
	var nwform_id = $("#form_id").val();
	load_NF_Fields(nwform_id);
	function load_NF_Fields(form_id) {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'get_NF_FieldsAjax',
					'form_id': form_id,
				},
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
						var form_keys = data.success;
						$("#form_keys").html( '<option value="">Select Key</option>' );
						$("#form_key").val('');
						for(var key  in form_keys) {
							$("#form_keys").append( "<option value='%" + key + "%'>" + form_keys[key] + "</option>" );
						}
					}
				}
			});
	}
	$("#mp_id").on('change', function() {
	  var mp_id =  $(this).children('option:selected').text();
	  $("#mp_Name").val( mp_id );
	});
	$("#form_id").on('change', function() {
	  var form_id = this.value;
	  if ( form_id.length != 0 ) {
		  load_NF_Fields(form_id);
	  }
	});
	$("#form_keys").on('change', function() {
	  $("#form_key").val( this.value );
	});

	$("#preview_pdf_template").click(function() {
		var pdf_template_Id = $("#pdf_template_Id").val();
		var pdf_template_name = $("#pdf_template_name").val();
		var mp_id = $("#mp_id").val();
		var mp_Name = $("#mp_Name").val();
		var form_id = $("#form_id").children("option:selected").val();
		var pdf_template_content = $("#pdf_template_content").val();
		var previewurl = $("#previewurl").val();
		var iframe = document.getElementById("pdf_template_content_ifr");
		var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
		var ps = iframe.contentWindow.document.getElementById('tinymce').getElementsByTagName('p');
		for(var p=0; p < ps.length;p++){
			if(pdf_template_content.length == 0)
				pdf_template_content = ps.get(p).textContent;
			else
				break;
		}
		if ( pdf_template_name.length == 0 || form_id.length == 0 || pdf_template_content.length == 0 ) {
			$("#message").html('<div class="error">All Fields are required!</div>');
		} else {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'preview_PDF_templateAjax',
					'pdf_template_Id': pdf_template_Id,
					'pdf_template_name': pdf_template_name,
					'mp_id': mp_id,
					'mp_Name': mp_Name,
					'form_id': form_id,
                    'comune_terni_cognome': $('#comune_terni_cognome').val(),
                    'comune_terni_nome': $('#comune_terni_nome').val(),
                    'comune_terni_email': $('#comune_terni_email').val(),
                    'comune_terni_codice_fiscale': $('#comune_terni_codice_fiscale').val(),
                    'comune_terni_searchvalue': $('#comune_terni_searchvalue').val(),
                    'comune_terni_path': $('#comune_terni_path').val(),
                    'comune_terni_soggetto': $('#comune_terni_soggetto').val(),
					'pago_pa_tipo_identificativo': $('#pago_pa_tipo_identificativo').val(),
					'pago_pa_codice_identificativo': $('#pago_pa_codice_identificativo').val(),
					'pago_pa_anagrafica_pagatore': $(' #pago_pa_anagrafica_pagatore').val(),
					'pago_pa_indirizzo_pagatore': $(' #pago_pa_indirizzo_pagatore').val(),
					'pago_pa_civico_pagatore': $(' #pago_pa_civico_pagatore').val(),
					'pago_pa_cap_pagatore': $(' #pago_pa_cap_pagatore').val(),
					'pago_pa_localita_pagatore': $(' #pago_pa_localita_pagatore').val(),
					'pago_pa_provincia_pagatore': $(' #pago_pa_provincia_pagatore').val(),
					'pago_pa_nazione_pagatore': $(' #pago_pa_nazione_pagatore').val(),
					'pago_pa_email_pagatore': $(' #pago_pa_email_pagatore').val(),
					'pago_pa_identificativo_univoco_dovuto': $(' #pago_pa_identificativo_univoco_dovuto').val(),
					'pago_pa_importo_singolo_versamento': $(' #pago_pa_importo_singolo_versamento').val(),
					'pago_pa_identificativo_tipo_dovuto': $(' #pago_pa_identificativo_tipo_dovuto').val(),
					'pago_pa_causale_versamento': $(' #pago_pa_causale_versamento').val(),
					'pago_pa_dati_specifici_riscossione': $(' #pago_pa_dati_specifici_riscossione').val(),
					'pdf_template_content': pdf_template_content,
					'file_path' : 'D:/xampp/htdocs/acer/wp-content/Fascicoli/1/vcomitini@artensys.it/Contattaci-1120.pdf'
				},
				beforeSend: function() {
					$('body').append('<div id="requestOverlay" class="request-overlay"></div>'); /*Create overlay on demand*/
                    $("#requestOverlay").show();
				},
				success: function (output) {
					var win = window.open('', '_blank');
    				win.location.href = output;
					$("#requestOverlay").remove();
				},
				error: function(r, o, n) {
					$("#requestOverlay").remove();
				}
			});
		}
	});
	$("#save_pdf_template").click(function() {
		var pdf_template_Id = $("#pdf_template_Id").val();
		var pdf_template_name = $("#pdf_template_name").val();
		var web_service_id = $("#web_service_id").val();
		var mp_id = $("#mp_id").val();
		var mp_Name = $("#mp_Name").val();
		var userIds = $("#user_ids").val();
		var form_id = $("#form_id").children("option:selected").val();
		var pdf_template_content = $("#pdf_template_content").val();
		var pdf_template_suffix = $("#pdf_template_suffix").val();
		var iframe = document.getElementById("pdf_template_content_ifr");
		var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
		var ps = iframe.contentWindow.document.getElementById('tinymce').getElementsByTagName('p');
		var body = iframe.contentWindow.document.getElementById('tinymce');
		// for(var p=0; p < ps.length;p++){
		// 	if(pdf_template_content.length == 0)
		// 		pdf_template_content = ps.get(p).textContent;
		// 	else
		// 		break;
		// }

		pdf_template_content = body.innerHTML;

		$("#message").text('');
		if ( pdf_template_name.length == 0 || form_id.length == 0 || pdf_template_content.length == 0 ) {
			$("#message").html('<div class="error">All Fields are required!</div>');
		} else {
			jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'add_PDF_templateAjax',
					'pdf_template_Id': pdf_template_Id,
					'pdf_template_name': pdf_template_name,
					'web_service_id': web_service_id,
					'mp_id': mp_id,
					'user_ids': userIds,
					'mp_Name': mp_Name,
					'form_id': form_id,
					'pdf_template_content': pdf_template_content,
					'pdf_template_suffix': pdf_template_suffix,
                    'comune_terni_cognome': $('#comune_terni_cognome').val(),
                    'comune_terni_nome': $('#comune_terni_nome').val(),
                    'comune_terni_email': $('#comune_terni_email').val(),
                    'comune_terni_codice_fiscale': $('#comune_terni_codice_fiscale').val(),
                    'comune_terni_searchvalue': $('#comune_terni_searchvalue').val(),
                    'comune_terni_path': $('#comune_terni_path').val(),
                    'comune_terni_soggetto': $('#comune_terni_soggetto').val(),
					'pago_pa_tipo_identificativo': $('#pago_pa_tipo_identificativo').val(),
					'pago_pa_codice_identificativo': $('#pago_pa_codice_identificativo').val(),
					'pago_pa_anagrafica_pagatore': $(' #pago_pa_anagrafica_pagatore').val(),
					'pago_pa_indirizzo_pagatore': $(' #pago_pa_indirizzo_pagatore').val(),
					'pago_pa_civico_pagatore': $(' #pago_pa_civico_pagatore').val(),
					'pago_pa_cap_pagatore': $(' #pago_pa_cap_pagatore').val(),
					'pago_pa_localita_pagatore': $(' #pago_pa_localita_pagatore').val(),
					'pago_pa_provincia_pagatore': $(' #pago_pa_provincia_pagatore').val(),
					'pago_pa_nazione_pagatore': $(' #pago_pa_nazione_pagatore').val(),
					'pago_pa_email_pagatore': $(' #pago_pa_email_pagatore').val(),
					'pago_pa_identificativo_univoco_dovuto': $(' #pago_pa_identificativo_univoco_dovuto').val(),
					'pago_pa_importo_singolo_versamento': $(' #pago_pa_importo_singolo_versamento').val(),
					'pago_pa_identificativo_tipo_dovuto': $(' #pago_pa_identificativo_tipo_dovuto').val(),
					'pago_pa_causale_versamento': $(' #pago_pa_causale_versamento').val(),
					'pago_pa_dati_specifici_riscossione': $(' #pago_pa_dati_specifici_riscossione').val()
                },
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
						$("#message").html('<div class="notice notice-success">PDF Template is saved.</div>');
						window.setTimeout(function () {
							window.location.href = '?page=configurazione-template-form';
						}, 500);
					} else {
						$("#message").html('<div class="error">'+data.error+'</div>');
					}
				}
			});
		}
		$("html, body").animate({scrollTop : 0},500);
	});

    $('#web_service_id').change(function () {
        if ($(this).val() == 1) {
            $('#mp_id, #pdf_template_suffix').parent().removeClass('hidden');
        } else {
            $('#mp_id, #pdf_template_suffix').parent().addClass('hidden');
        }
        if ($(this).val() == 2) {
            $('.comune_terni_service_options').show();
            renderComuneTerniSelect();
        } else {
            $('.comune_terni_service_options').hide();
        }
		if ($(this).val() == 3) {
            $('.pago_pa_service_options').show();
			renderPagoPaSelect();
        } else {
            $('.pago_pa_service_options').hide();
        }
    });
    $('#form_id').change(function () {
        if ($('#web_service_id').val() != 1) {
            return;
        }
		if ($('#web_service_id').val() == 2) {
			renderComuneTerniSelect();
		}
        if ($('#web_service_id').val() == 3) {
			renderPagoPaSelect();
		}
    });

    $('#web_service_id').trigger('change');

    function renderComuneTerniSelect() {
        var formId = $('#form_id').val();

        $('.comune_terni_service_select_map').each(function (i, e) {
            $(e).find('option').each(function (i, e) {
                if (i) {
                    $(e).remove();
                }
            })
        });

        if (!formId || jsonNinjaFormFieldsGroupByFormId[formId] === undefined) {
            return;
        }

        _.each(jsonNinjaFormFieldsGroupByFormId[formId], function (o) {
            $('.comune_terni_service_select_map').append($('<option>', {
                value: o.id,
                text: o.label
            }));
        });

        $('.comune_terni_service_select_map').each(function (i, e) {
            if ($(e).find('option[value="'+$(e).data('value')+'"]').length) {
                $(e).val($(e).data('value'));
            }
        })
    }

	function renderPagoPaSelect() {
        var formId = $('#form_id').val();

        $('.pago_pa_service_select_map').each(function (i, e) {
            $(e).find('option').each(function (i, e) {
                if (i) {
                    $(e).remove();
                }
            })
        });

        if (!formId || jsonNinjaFormFieldsGroupByFormId[formId] === undefined) {
            return;
        }

        _.each(jsonNinjaFormFieldsGroupByFormId[formId], function (o) {
            $('.pago_pa_service_select_map').append($('<option>', {
                value: o.id,
                text: o.label
            }));
        });

        $('.pago_pa_service_select_map').each(function (i, e) {
            if ($(e).find('option[value="'+$(e).data('value')+'"]').length) {
                $(e).val($(e).data('value'));
            }
        })
    }
});
</script>
<style>
/*
.ck-editor__editable {
    min-height: 400px;
}
*/
#message .error, #message .notice {
    padding: 12px;
}
#titlediv input {
    padding: 3px 8px;
    font-size: 1.7em;
    line-height: 100%;
    height: 1.7em;
    width: 100%;
    outline: 0;
    margin: 0 0 5px;
    background-color: #fff;
}
#post-body select, #post-body input[type=text] {
    width: 100%;
    height: 38px;
    margin: 0 0 5px;
    font-size: 1.2em;
    font-weight: 500;
}
.half {
    float: left;
    width: 49%;
    margin: 0 2% 0 0;
}
.half.last {
    margin: 0;
}
</style>
<?php }

<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class PDFTemplate {

public function PDFPluginActivation() {
        global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) :
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE `$table_name` (
                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                `formID` bigint(50) NOT NULL,
				`templateName` varchar(255) NOT NULL,
				`mpId` bigint(50) NOT NULL,
                `mpName` varchar(255) NOT NULL,
                `templateDat` longtext NOT NULL,
                `date` date NOT NULL,
                PRIMARY KEY (`id`)
            )";
			if(!function_exists('dbDelta')) {
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            }
            dbDelta($sql);
        endif;
}

public function add_PDF_templateAjax() {
		global $wpdb;
		$result = array();
		$formID = $_REQUEST['form_id'];
		$pdf_template_Id = $_REQUEST['pdf_template_Id'];
		$templateName = $_REQUEST['pdf_template_name'];
		$templateSuffix = $_REQUEST['pdf_template_suffix'];
		$mpId = $_REQUEST['mp_id'];
		$webServiceId = $_REQUEST['web_service_id'];
		$mpName = $_REQUEST['mp_Name'];
		$templateDat = "" . $_REQUEST['pdf_template_content'];
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$todayDate = esc_sql(date('Y-m-d'));
		if($webServiceId == 2){
			$mpId = -1;			
		}
		if(!empty($pdf_template_Id)) {
			$id = $wpdb->query("UPDATE $table_name SET formID='".$formID."', web_service_id='".$webServiceId."', templateName='".$templateName."', mpId='".$mpId."', mpName='".$mpName."', templateDat='".$templateDat."', templateSuffix='".$templateSuffix."' WHERE id = $pdf_template_Id ");
		} else {
			$id = $wpdb->query("INSERT INTO $table_name (`formID`, `templateName`, `web_service_id`, `mpId`, `mpName`, `templateDat`, `date`, `templateSuffix`) VALUES('$formID', '$templateName', $webServiceId, '$mpId', '$mpName', '$templateDat', '$todayDate', '$templateSuffix')");
		}
		
		if(empty($id) && !empty($pdf_template_Id)){
			$id = $pdf_template_Id;
		}
		
		if(empty($wpdb->last_error)) {
			$result['success'] = $id;
        } else {
            $result['error'] = 'Could not save the template.';
        }
		echo json_encode($result);
        exit;
}

public function ninja_forms_suffix_column(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'nf_pdf_template';
    $check_column = (array) $wpdb->get_results(  "SELECT count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME = '{$table_name}' AND COLUMN_NAME = 'templateSuffix'")[0];
    $check_column = (int) array_shift($check_column);
    if($check_column == 0) {
        $wpdb->query("ALTER TABLE $table_name ADD COLUMN `templateSuffix` VARCHAR(255) NOT NULL");
    }
}

public function preview_PDF_templateAjax() {
	global $wpdb;
	$result = array();
	$formID = $_REQUEST['form_id'];
	$pdf_template_Id = $_REQUEST['pdf_template_Id'];
	$templateName = $_REQUEST['pdf_template_name'];
	$mpId = $_REQUEST['mp_id'];
	$mpName = $_REQUEST['mp_Name'];
	$templateDat = $_REQUEST['pdf_template_content']; 
	$templateDat = stripslashes($templateDat);
	$nf_fields = Ninja_Forms()->form( $formId )->get_fields();
	$data = array();
	$fields = array();
	$args = array_merge( array(
	    'title'    => 'preview',
	    'table'    => $templateDat,
	    'fields'   => $fields,
	    'css_path' => NF_PDF()->locate_template( 'pdf.css' )
	), $data );

	$html = NF_PDF()->get_template( 'pdf.php', $args );
	
	$basepath = NF_PDF()->dir( 'templates/' );


	$dompdf = new DOMPDF();
	$dompdf->set_option('enable_font_subsetting', true);
    $dompdf->set_base_path( $basepath );
    $dompdf->load_html( $html );
    $dompdf->render();
	$pdf = $dompdf->output();
	$pdf_encoded_string = base64_encode($pdf);
	//$result['pdf'] = $pdf_encoded_string;
	//$file_path = $_REQUEST['file_path'];
	//$pdf = file_get_contents($file_path);
	echo 'data:application/pdf;base64,' . $pdf_encoded_string;
	exit;
}


public function remove_PDF_templateAjax() {
        global $wpdb;
		$result = array();
		$formID = $_REQUEST['form_id'];
		$pdf_template_Id = $_REQUEST['pdf_template_Id'];
		$table_name = $wpdb->prefix . "nf_pdf_template";
		if(!empty($pdf_template_Id) && !empty($formID)) {
			$rowDel = $wpdb->delete( $table_name, array( 'id' => $pdf_template_Id, 'formID' => $formID ) );
		}
		if($rowDel) {
            $result['success'] = 'PDF Template is deleted.';
        } else {
            $result['error'] = 'Could not delete the template.';
        }
		echo json_encode($result);
        exit;
}

function get_PDF_template($pdf_template_Id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name WHERE id = $pdf_template_Id ";
		$data = $wpdb->get_results($query);

		return $data[0];
}

function get_doc_templates() {
		
		$ismssqlconnected = false;
		if(function_exists('sqlsrv_connect')){
			$serverName = MSSQL_DB_HOST;
			if(isset($serverName) && ($serverName != null)){
				$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
				$conn = sqlsrv_connect( $serverName, $connectionInfo);
				if( $conn ) {
					$ismssqlconnected = true;
					echo "<br />";
			   }else{
					echo "Connection could not be established to MSSQL.Switching to local DB<br />";
			   }
			}
		}
		
		if($ismssqlconnected === true){
			$table_name = "ViewDocumentTemplate";
			$query = "SELECT * FROM $table_name WHERE IsValid = 1";
			$sqlquery = sqlsrv_query($conn,$query);
			if ($sqlquery) {
				while ($row = sqlsrv_fetch_object($sqlquery)) {
					$data[] = $row;
				}
				sqlsrv_free_stmt($sqlquery);
			}
			sqlsrv_close($conn);
		}else {
			global $wpdb;
			$table_name = "servmask_prefix_documenttemplate";
			$query = "SELECT * FROM $table_name WHERE IsValid = 1";
			$data = $wpdb->get_results($query);
		}
		return $data;
}

function get_web_services() {
    global $wpdb;
    $table_name = "servmask_prefix_portal";
    $query = "SELECT id, web_service_name  FROM $table_name";
    $data = $wpdb->get_results($query);

    return $data;
}

function get_PDF_temppale_byFormId($formId) {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name WHERE formID = $formId ";
		$data = $wpdb->get_results($query);

		return $data[0];
}

function get_PDF_templates() {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name ";
		$data = $wpdb->get_results($query);

		return $data;
}

public function get_NF_FieldsAjax() {
		$result = array();
		$formID = $_REQUEST['form_id'];
		$nf_field = array();
		if(!empty($formID)) {
			$nf_fields = Ninja_Forms()->form( $formID )->get_fields();
			foreach( $nf_fields as $field ){
				$nf_field[$field->get_setting( 'key' )] = $field->get_setting( 'label' );
			}
		}
		if(!empty($nf_field)) {
            $result['success'] = $nf_field;
        } else {
            $result['error'] = 'Could not get the fields.';
        }
		echo json_encode($result);
        exit;
}

function get_document_data($protocol_template_id) {
	$serverName = MSSQL_DB_HOST;
	$ismssqlconnected = false;
	if(isset($serverName) && ($serverName != null)){
		$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);
		if( $conn ) {
			$ismssqlconnected = true;
			
	   }else{
			if ( WP_DEBUG === true ) {
				error_log('Connection could not be established to MSSQL.Switching to local DB');
			}
	   }
	}
	if($ismssqlconnected === true){
		$data = [];
		$query = "Select * from ViewDocumentTemplate where ID=" . $protocol_template_id;
		$sqlquery = sqlsrv_query($conn,$query);
		if ($sqlquery) {
			while ($row = sqlsrv_fetch_object($sqlquery)) {
				$data[] = $row;
			}
			sqlsrv_free_stmt($sqlquery);
			return $data;
		}else {
			if ( WP_DEBUG === true ) {
				error_log('No data fetched from ViewDocumentTemplate for ID ' . $protocol_template_id);
			}
		}
		sqlsrv_close($conn);
	}
}

function get_address_data($email_address) {
	$serverName = MSSQL_DB_HOST;
	$ismssqlconnected = false;
	if(isset($serverName) && ($serverName != null)){
		$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);
		if( $conn ) {
			$ismssqlconnected = true;
			
	   }else{
			if ( WP_DEBUG === true ) {
				error_log('Connection could not be established to MSSQL.Switching to local DB');
			}
	   }
	}
	if($ismssqlconnected === true){
		$query = "Select id from ViewAddress where Email='" . $email_address . "' order by id desc";
		$sqlquery = sqlsrv_query($conn,$query);
		if ($sqlquery) {
			while ($row = sqlsrv_fetch_object($sqlquery)) {
				$data[] = $row;
			}
			sqlsrv_free_stmt($sqlquery);
			sqlsrv_close($conn);
			return $data;
		}else {
			if ( WP_DEBUG === true ) {
				error_log('No data fetched from ViewAddress for Email ' . $email_address);
			}
			sqlsrv_close($conn);
		}
	}
}

}

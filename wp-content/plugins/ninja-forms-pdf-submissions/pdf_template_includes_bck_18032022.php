<?php
require_once  'lib/vendor/autoload.php';
if ( ! defined( 'ABSPATH' ) ) exit;

class PDFTemplate {

public function PDFPluginActivation() {
        global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) :
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE `$table_name` (
                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                `formID` bigint(50) NOT NULL,
				`templateName` varchar(255) NOT NULL,
				`mpId` bigint(50) NOT NULL,
                `mpName` varchar(255) NOT NULL,
                `templateDat` longtext NOT NULL,
                `date` date NOT NULL,
                PRIMARY KEY (`id`)
            )";
			if(!function_exists('dbDelta')) {
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            }
            dbDelta($sql);
        endif;
}

public function print_PDF_templateAjax() {

	$nomeRichiedente = $_POST["nomeRichiedente"];
	$cognomeRichiedente = $_POST["cognomeRichiedente"];
	$luogoDiNascita = $_POST["luogoDiNascita"];
	$dataDiNascita= $_POST["dataDiNascita"];
       $sessoRichiedente= $_POST["sessoRichiedente"];
       $ProvinciaNascita= $_POST["ProvinciaNascita"];
	$residenteNelComuneDi = $_POST["residenteNelComuneDi"];
	$residenteNelComuneDiTerni = $_POST["residenteNelComuneDiTerni"];
       $ProvinciaResidenza= $_POST["ProvinciaResidenza"];
	$allaViaPiazza = $_POST["allaViaPiazza"];
	$nCivico = $_POST["nCivico"];
	$zip = $_POST["zip"];
	$codiceiscale = $_POST["codiceiscale"];
	$sezione1Data = $_POST["sezione1Data"];
	$sezione1Period = $_POST["sezione1Period"];
	$sezione2Reddito = $_POST["sezione2Reddito"];
	$sezione2Euro = $_POST["sezione2Euro"];
	$tipologia = $_POST["tipologia"];
	$sezione2dellaDomanda = $_POST["sezione2dellaDomanda"];
	$sezione3Data = $_POST["sezione3Data"];
	$sezione4DirittoDiProprieta= $_POST["sezione4DirittoDiProprieta"];
	$sezione5CanoneAffitto = $_POST["sezione5CanoneAffitto"];

	$primoContrattoAffittoStipulatoCon = $_POST["primoContrattoAffittoStipulatoCon"];
	$primoContrattoRegistratoInData = $_POST["primoContrattoRegistratoInData"];
	$primoContrattoAlN = $_POST["primoContrattoAlN"];
	$primoContrattoUfficioDelRegistroDi = $_POST["primoContrattoUfficioDelRegistroDi"];
	$primoContrattoAffittoDellAlloggioSitoInComuneDi = $_POST["primoContrattoAffittoDellAlloggioSitoInComuneDi"];
	$primoContrattoIndirizzo = $_POST["primoContrattoIndirizzo"];
	$primoContrattoDiDimensionePariAMQ = $_POST["primoContrattoDiDimensionePariAMQ"];
	$primoContrattoF = $_POST["primoContrattoF"];
	$primoContrattoP = $_POST["primoContrattoP"];
	$primoContrattoSub = $_POST["primoContrattoSub"];
	$primoContrattoCat= $_POST["primoContrattoCat"];
	$primoContrattoProprieta = $_POST["primoContrattoProprieta"];
	$primoContrattoPerCui_eStato = $_POST["primoContrattoPerCui_eStato"];
	$primoContrattoPeriodoDiMensilita = $_POST["primoContrattoPeriodoDiMensilita"];
    $primoContrattoRegolaPagamenti = $_POST["primoContrattoRegolaPagamenti"];
	
	$secondoContrattoAffittoStipulatoCon = $_POST["secondoContrattoAffittoStipulatoCon"];
	$secondoContrattoRegistratoInData = $_POST["secondoContrattoRegistratoInData"];
	$secondoContrattoAlN = $_POST["secondoContrattoAlN"];
	$secondoContrattoUfficioDelRegistroDi = $_POST["secondoContrattoUfficioDelRegistroDi"];
	$secondoContrattoAffittoDellAlloggioSitoInComuneDi = $_POST["secondoContrattoAffittoDellAlloggioSitoInComuneDi"];
	$secondoContrattoIndirizzo = $_POST["secondoContrattoIndirizzo"];
	$secondoContrattoDiDimensionePariAMQ = $_POST["secondoContrattoDiDimensionePariAMQ"];
	$secondoContrattoF = $_POST["secondoContrattoF"];
	$secondoContrattoP = $_POST["secondoContrattoP"];
	$secondoContrattoSub = $_POST["secondoContrattoSub"];
	$secondoContrattoCat= $_POST["secondoContrattoCat"];
	$secondoContrattoProprieta = $_POST["secondoContrattoProprieta"];
	$secondoContrattoPerCui_eStato = $_POST["secondoContrattoPerCui_eStato"];
	$secondoContrattoPeriodoDiMensilita = $_POST["secondoContrattoPeriodoDiMensilita"];

	
	$terzoContrattoAffittoStipulatoCon = $_POST["terzoContrattoAffittoStipulatoCon"];
	$terzoContrattoRegistratoInData = $_POST["terzoContrattoRegistratoInData"];
	$terzoContrattoAlN = $_POST["terzoContrattoAlN"];
	$terzoContrattoUfficioDelRegistroDi = $_POST["terzoContrattoUfficioDelRegistroDi"];
	$terzoContrattoAffittoDellAlloggioSitoInComuneDi = $_POST["terzoContrattoAffittoDellAlloggioSitoInComuneDi"];
	$terzoContrattoIndirizzo = $_POST["terzoContrattoIndirizzo"];
	$terzoContrattoDiDimensionePariAMQ = $_POST["terzoContrattoDiDimensionePariAMQ"];
	$terzoContrattoF = $_POST["terzoContrattoF"];
	$terzoContrattoP = $_POST["terzoContrattoP"];
	$terzoContrattoSub = $_POST["terzoContrattoSub"];
	$terzoContrattoCat= $_POST["terzoContrattoCat"];
	$terzoContrattoProprieta = $_POST["terzoContrattoProprieta"];
	$terzoContrattoPerCui_eStato = $_POST["terzoContrattoPerCui_eStato"];
	$terzoContrattoPeriodoDiMensilita = $_POST["terzoContrattoPeriodoDiMensilita"];

	$inCasoDiAssegnazioneDelContributoEsso = $_POST["inCasoDiAssegnazioneDelContributoEsso"];
	$intestatoA = $_POST["intestatoA"];
	$CodiceIban = $_POST["CodiceIban"];
	$CO = $_POST["CO"];
	$signature = $_POST["signature"];
	$indirizzoRecapito = $_POST["indirizzoRecapito"];
	$indirizzoNumero = $_POST["indirizzoNumero"];
	$localita = $_POST["localita"];
	$cap = $_POST["cap"];
	$provinicia = $_POST["provinicia"];
	$telefonoAbitazione= $_POST["telefonoAbitazione"];
	$cellulare = $_POST["cellulare"];
	$email = $_POST["email"];
	$pec = $_POST["pec"];
	$dataMarca = $_POST["dataMarca"];
	$numeroMarca = $_POST["numeroMarca"];
	$files = $_POST["files"];

	$basepath = NF_PDF()->dir( 'templates/' );
	$html = file_get_contents($basepath . "/index.html");
	$html = str_replace("%nome_richiedente_1591626417938%",$nomeRichiedente,$html);
	$html = str_replace("%cognome_richiedente_1591626411300%",$cognomeRichiedente,$html);
	$html = str_replace("%luogo_di_nascita_comune_stato_1646665259885%",$luogoDiNascita,$html);
	$html = str_replace("%data_di_nascita_1594726068041%",$dataDiNascita,$html);
	$html = str_replace("%comune_di_residenza_della_provincia_di_perugia_1646924152552%",$residenteNelComuneDi,$html);
	$html = str_replace("%comune_di_residenza_della_provincia_di_terni_1646924282047%",$residenteNelComuneDiTerni,$html);
	$html = str_replace("%provincia_di_residenza_1646666343250%",$ProvinciaResidenza,$html);
	$html = str_replace("%alla_via_piazza_1608128323231%",$allaViaPiazza,$html);
	$html = str_replace("%sesso_1646666584563%",$sessoRichiedente,$html);
	$html = str_replace("%provincia_di_nascita_1646926017658%",$ProvinciaNascita,$html);
	$html = str_replace("%n_civico_1595516820746%",$nCivico,$html);
	$html = str_replace("%zip_1595599856330%",$zip,$html);
	$html = str_replace("%codice_fiscale_1591616243364%",$codiceiscale,$html);
	$html = str_replace("%sezione_1_-_lett_a_-_di_essere_alla_data_di_pubblicazione_del_bando_1633339434083%",$sezione1Data,$html);
	$html = str_replace("%sezione_1_-_lett_b_-_periodo_residenza_1633339505388%",$sezione1Period,$html);
	$html = str_replace("%sezione_2_-_lett_a_-_reddito_1633339646667%",$sezione2Reddito,$html);
	$html = str_replace("%sezione_2_-_lett_b_-_che_l_attestazione_isee_2021_e_di_eururo_1646725529457%",$sezione2Euro,$html);
	$html = str_replace("%sezione_2_-_lett_c_-_presentazione_della_domanda_1633340162116%",$sezione2dellaDomanda,$html);
	$html = str_replace("%tipologia_1633612616690%",$tipologia,$html);
	$sezione3datahtml = '';
	if(count($sezione3Data) > 0){
		
		for ($i = 0; $i < count($sezione3Data); $i++) {
			
			$tcognome  = $sezione3Data[$i][0];
			$tnome 	  = $sezione3Data[$i][1];
			$tcomune   = $sezione3Data[$i][2];
			$tprovince 	  = $sezione3Data[$i][3];
			$tdata_nascita = $sezione3Data[$i][4];
			$tsesso = $sezione3Data[$i][5];
			$tcivile = $sezione3Data[$i][6];
			$tgrado = $sezione3Data[$i][7];
			if(!empty($tcognome) || !empty($tnome) || !empty($tcomune) || !empty($tprovince) || !empty($tdata_nascita) || !empty($tsesso) 
				|| !empty($tcivile)  || !empty($tgrado)){
					$sezione3datahtml .= '<tr class="t2-tr-2">
              <td class="t2-td-2" colspan="1" rowspan="1">
                 <p class="t2-para-1"><span class="f-arial td-3">&nbsp; ' . ($i+1) . ')</span></p>
              </td>
              <td class="t2-td-3" colspan="1" rowspan="1">
                 <p class="para-4 pt-pt6 table-p t-para-4 pt-pt6"><span class="t2-span-4">Cognome <strong>' . $tcognome . '</strong> Nome <strong>' . $tnome . '</strong></span></p>
                 <p class="para-4 table-p"><span class="t2-span-4">Comune/Stato di nascita <strong>' . $tcomune . '</strong> Provincia <strong>' . $tprovince . ' </strong>data di nascita <strong>' . $tdata_nascita . '</strong> sesso <strong>' . $tsesso . '</strong> Stato civile <strong>' . $tcivile . '</strong> Parentela <strong>' . $tgrado . '</strong></span></p>
                 <p class="t-para-2 h-10"><span class="t2-span-1 span-7 t2-span-3"></span></p>
                </td>';
			}
		}

	}
	$html = str_replace("%sezione3_table_data_section%",$sezione3datahtml,$html);
	$html = str_replace("%sezione_4_-_diritto_di_proprieta_comproprieta_usufrutto_1633352912672%",$sezione4DirittoDiProprieta,$html);
	$html = str_replace("%sezione_5_-_dichiara_che_per_il_canone_d_affitto_2020_1633429051710%",$sezione5CanoneAffitto,$html);

	$html = str_replace("%primo_contratto_affitto_stipulato_con_1637848976443%",$primoContrattoAffittoStipulatoCon,$html);
	$html = str_replace("%primo_contratto_registrato_in_data_1633502920450%",$primoContrattoRegistratoInData,$html);
	$html = str_replace("%primo_contratto_al_n_1633502905541%",$primoContrattoAlN,$html);
	$html = str_replace("%primo_contratto_presso_l_ufficio_del_registro_di_1633502926195%",$primoContrattoUfficioDelRegistroDi,$html);
	$html = str_replace("%primo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756433470%",$primoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
	$html = str_replace("%primo_contratto_indirizzo_1633502937026%",$primoContrattoIndirizzo,$html);
	$html = str_replace("%primo_contratto_di_dimensione_pari_a_mq_1646757473917%",$primoContrattoDiDimensionePariAMQ,$html);
	$html = str_replace("%primo_contratto_foglio_1646671456110%",$primoContrattoF,$html);
	$html = str_replace("%primo_contratto_particella_1646670308706%",$primoContrattoP,$html);
	$html = str_replace("%primo_contratto_sub_1633502962590%",$primoContrattoSub,$html);
	$html = str_replace("%primo_contratto_cat_1646671913544%",$primoContrattoCat,$html);
	$html = str_replace("%primo_contratto_proprieta_1633502972107%",$primoContrattoProprieta,$html);
	$html = str_replace("%primo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724360342%",$primoContrattoPerCui_eStato,$html);
	$html = str_replace("%primo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646667150890%",$primoContrattoPeriodoDiMensilita,$html);
	$html = str_replace("%indicare_se_in_regola_con_i_pagamenti_1636724684210%",$primoContrattoRegolaPagamenti,$html);

	$html = str_replace("%secondo_contratto_affitto_stipulato_con_1633504536849%",$secondoContrattoAffittoStipulatoCon,$html);
	$html = str_replace("%secondo_contratto_registrato_in_data_1633504545461%",$secondoContrattoRegistratoInData,$html);
	$html = str_replace("%secondo_contratto_al_n_1633504742493%",$secondoContrattoAlN,$html);
	$html = str_replace("%secondo_contratto_presso_l_ufficio_del_registro_1633504561493%",$secondoContrattoUfficioDelRegistroDi,$html);
	$html = str_replace("%secondo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756729863%",$secondoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
	$html = str_replace("%secondo_contratto_indirizzo_1633504610787%",$secondoContrattoIndirizzo,$html);
	$html = str_replace("%secondo_contratto_di_dimensione_pari_a_mq_1646906758907%",$secondoContrattoDiDimensionePariAMQ,$html);
	$html = str_replace("%secondo_contratto_foglio_1646722960493%",$secondoContrattoF,$html);
	$html = str_replace("%secondo_contratto_particella_1646670448761%",$secondoContrattoP,$html);
	$html = str_replace("%secondo_contratto_sub_1633506282705%",$secondoContrattoSub,$html);
	$html = str_replace("%secondo_contratto_cat_1646723558366%",$secondoContrattoCat,$html);
	$html = str_replace("%secondo_contratto_proprieta_1633506678222%",$secondoContrattoProprieta,$html);
	$html = str_replace("%secondo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724604037%",$secondoContrattoPerCui_eStato,$html);
	$html = str_replace("%secondo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646668673320%",$secondoContrattoPeriodoDiMensilita,$html);


	$html = str_replace("%terzo_contratto_affitto_stipulato_con_1633508157510%",$terzoContrattoAffittoStipulatoCon,$html);
	$html = str_replace("%terzo_contratto_registrato_in_data_1633508166161%",$terzoContrattoRegistratoInData,$html);
	$html = str_replace("%terzo_contratto_al_n_1633508174162%",$terzoContrattoAlN,$html);
	$html = str_replace("%terzo_contratto_presso_l_ufficio_del_registro_1633508184002%",$terzoContrattoUfficioDelRegistroDi,$html);
	$html = str_replace("%terzo_contratto_per_l_affitto_dell_alloggio_sito_in_comune_di_1646756957473%",$terzoContrattoAffittoDellAlloggioSitoInComuneDi,$html);
	$html = str_replace("%terzo_contratto_indirizzo_1633508307145%",$terzoContrattoIndirizzo,$html);
	$html = str_replace("%terzo_contratto_di_dimensione_pari_a_mq_1646906953242%",$terzoContrattoDiDimensionePariAMQ,$html);
	$html = str_replace("%terzo_contratto_foglio_1646723343203%",$terzoContrattoF,$html);
	$html = str_replace("%terzo_contratto_particella_1646670743807%",$terzoContrattoP,$html);
	$html = str_replace("%terzo_contratto_sub_1633508440982%",$terzoContrattoSub,$html);
	$html = str_replace("%terzo_contratto_cat_1646723964406%",$terzoContrattoCat,$html);
	$html = str_replace("%terzo_contratto_proprieta_1633508503142%",$terzoContrattoProprieta,$html);
	$html = str_replace("%terzo_contratto_con_un_canone_complessivo_risultante_dal_contratto_di_eur_1646724934482%",$terzoContrattoPerCui_eStato,$html);
	$html = str_replace("%terzo_contratto_per_un_periodo_di_mensilita_pari_a_numero_1646668631558%",$terzoContrattoPeriodoDiMensilita,$html);
	

	$html = str_replace("%in_caso_di_assegnazione_del_contributo_esso_dovra_essere_accreditato_sul_cc_n_1633431304011%",$inCasoDiAssegnazioneDelContributoEsso,$html);
	$html = str_replace("%intestato_a_1633431335082%",$intestatoA,$html);
	$html = str_replace("%codice_iban_1633431349182%",$CodiceIban,$html);
	$html = str_replace("%c_o_1633431322334%",$CO,$html);
	$html = str_replace("%sig_1633431592191%",$signature,$html);
	$html = str_replace("%indirizzo_recapito_1633431614004%",$indirizzoRecapito,$html);
	$html = str_replace("%indirizzo_numero_1633509782911%",$indirizzoNumero,$html);
	$html = str_replace("%localita_1633431647379%",$localita,$html);
	$html = str_replace("%c_a_p_1633431660817%",$cap,$html);
	$html = str_replace("%provincia_sigla_1646923531753%",$provinicia,$html);
	$html = str_replace("%telefono_abitazione_1633431712402%",$telefonoAbitazione,$html);
	$html = str_replace("%cellulare_1633431728138%",$cellulare,$html);
	$html = str_replace("%email_recapito_1633431743817%",$email,$html);
	$html = str_replace("%pec_posta_elettronica_certificata_1636531106663%",$pec,$html);

	$html = str_replace("%numero_della_marca_da_bollo_di_16_euro_1633611989245%",$numeroMarca,$html);
	$html = str_replace("%data_marca_da_bollo_1633612039871%",$dataMarca,$html);
	$files_html = '';
	if(isset($files) && count($files) > 0 ){

		$files_html = "<ul>";
		for ($f = 0; $f <= count($files); $f++) {
			$files_html .= "<li>" . $files[$f] . "</li>";
		}
		$files_html .= "</ul>";
	}

	$html = str_replace("%elenco_della_documentazione_allegata%",$files_html,$html);
	$imagepath = NF_PDF()->dir( 'assets/img' );
	$html = str_replace("%domande_image_1%",$imagepath . '/domande1.jpg',$html);
	$tempname = tempnam('', 'domanda_');
	rename($tempname, $tempname .= '.pdf');

	error_log("PRINT PDF CONTENT:".$html);
	//error_log("NAME PDF:".print_r($tempname,1));
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->WriteHTML($html);
	$pdf =  $mpdf->Output( $tempname, 'F');
	$pdf_data = file_get_contents($tempname);
	$pdf_encoded_string = base64_encode($pdf_data);
	echo 'data:application/pdf;base64,' . $pdf_encoded_string;
	exit;
}
public function save_PDF_templateAjax() {
	global $wpdb;
	
	$nomeRichiedente = $_POST["nomeRichiedente"];
	$cognomeRichiedente = $_POST["cognomeRichiedente"];
	$codiceFiscale = $_POST["codiceFiscale"];
	$email= $_POST["email"];
	$numTelXComunicazioni = $_POST["numTelXComunicazioni"];
	$indirizzoResidenza = $_POST["indirizzoResidenza"];
	$comuneResidenza = $_POST["comuneResidenza"];
	$numProtocollo = $_POST["numProtocollo"];
	$dataProtocollo = $_POST["dataProtocollo"];
	$confORettdati = $_POST["confORettdati"];
	$acqAutBassEmAdAlimen = $_POST["acqAutBassEmAdAlimen"];
	$numSerieMarcaBollo = $_POST["numSerieMarcaBollo"];
	$dataMarcaBollo = $_POST["dataMarcaBollo"];

	$comuneNascita = $_POST["comuneNascita"];
	$targaAutovettura = $_POST["targaAutovettura"];
	$classeAmbientale = $_POST["classeAmbientale"];
	$adAlimentazione = $_POST["adAlimentazione"];
	$intestazioneVeicolo = $_POST["intestazioneVeicolo"];
	$nomeIntestatario= $_POST["nomeIntestatario"];
	$cognomeIntestarario = $_POST["cognomeIntestarario"];
	$codiceFiscaleIntestarario = $_POST["codiceFiscaleIntestarario"];
	$comuneDiresidenzaIntestatario = $_POST["comuneDiresidenzaIntestatario"];
	$indirizzoResidenzaIntestatario = $_POST["indirizzoResidenzaIntestatario"];
	$numCivicoResidenzaIntestatario = $_POST["numCivicoResidenzaIntestatario"];
	$gradoDiParentela = $_POST["gradoDiParentela"];
	$alimentazioneNuovaVettura = $_POST["alimentazioneNuovaVettura"];
	$datanascita = $_POST["datanascita"];
	
	$templateId = "24";
	$pdf_template_dat = $this->get_PDF_template($templateId);
	$basepath = NF_PDF()->dir( 'templates/' );
	$html = $pdf_template_dat->templateDat;
	error_log("PDF CONTENT:".$html);
	$html = str_replace("%nome_richiedente_1596638557958%",$nomeRichiedente,$html);
	$html = str_replace("%cognome_richiedente_1596638604326%",$cognomeRichiedente,$html);
	$html = str_replace("%codice_fiscale_1596638684501%",$codiceFiscale,$html);
	$html = str_replace("%email_1596794400214%",$email,$html);
	$html = str_replace("%numero_di_telefono_per_comunicazioni_1596794502034%",$numTelXComunicazioni,$html);
	$html = str_replace("%indirizzo_di_residenza_1596794801479%",$indirizzoResidenza,$html);
	$html = str_replace("%comune_di_residenza_1596794837959%",$comuneResidenza,$html);
	$html = str_replace("%numero_protocollo_della_domanda_1596631897406%",$numProtocollo,$html);
	$html = str_replace("%data_protocollo_della_domanda_1596631906838%",$dataProtocollo,$html);
	$html = str_replace("%conferma_e_o_rettifica_dati_1596632194062%",$confORettdati,$html);

	$html = str_replace("%comune_nascita_1629727268349%",$comuneNascita,$html);
	$html = str_replace("%targa_autovettura_1629727714767%",$targaAutovettura,$html);
	$html = str_replace("%classe_ambientale_1628660498956%",$classeAmbientale,$html);
	$html = str_replace("%ad_alimentazione_1628659442578%",$adAlimentazione,$html);
	$html = str_replace("%intestazione_veicolo_1628660691070%",$intestazioneVeicolo,$html);
	$html = str_replace("%nome_intestatario_1629729590859%",$nomeIntestatario,$html);
	$html = str_replace("%cognome_intestarario_1629730020423%",$cognomeIntestarario,$html);
	$html = str_replace("%codice_fiscale_intestarario_1629730074843%",$codiceFiscaleIntestarario,$html);
	$html = str_replace("%comune_di_residenza_intestatario_1628661849279%",$comuneDiresidenzaIntestatario,$html);
	$html = str_replace("%indirizzo_residenza_intestatario_1629730260171%",$indirizzoResidenzaIntestatario,$html);
	$html = str_replace("%num_civico_residenza_intestatario_1629730364331%",$numCivicoResidenzaIntestatario,$html);
	$html = str_replace("%grado_di_parentela_tra_il_richiedente_il_contributo_e_il_proprietario_del_veicolo_da_rottamare_1628662013117%",$gradoDiParentela,$html);
       $html = str_replace("lintestatario della vettura di cui al punto precedente","l'intestatario della vettura di cui al punto precedente è",$html);
       $html = str_replace("padremadre del richiedente","padre-madre del richiedente",$html);
       $html = str_replace("figliofiglia del richiedente","figlio-figlia del richiedente",$html);
       $html = str_replace("nonnononna del richiedente","nonno-nonna del richiedente",$html);
       $html = str_replace("fratellosorella del richiedente","fratello-sorella del richiedente",$html);
       $html = str_replace("nipote del richiedente figlio del figliofiglia","nipote del richiedente (figlio del figlio-figlia)",$html);
       //$html = str_replace("%ad_alimentazione_1630047993724%",$alimentazioneNuovaVettura,$html);
       $html = str_replace("%datanascita_1630042831086%",$datanascita,$html);
	
	$html = utf8_decode($html);
	$html = str_replace("\'","'",$html);
	$pos = strpos($confORettdati,"Conferma");
	if($pos!==false){
		$exception = "<li>acquista una autovettura M1 a basse emissioni ad alimentazione:&nbsp;%acquista_una_autovettura_m1_a_basse_emissioni_ad_alimentazione_1596633371902%</li>";
		
		$html = str_replace($exception,"",$html);
              $html = str_replace("%ad_alimentazione_1630047993724%",$alimentazioneNuovaVettura,$html);
              $html = str_replace("%acquista_una_autovettura_m1_a_basse_emissioni_ad_alimentazione_1596633371902%","",$html);
              
		
	} else {
		$html = str_replace("%acquista_una_autovettura_m1_a_basse_emissioni_ad_alimentazione_1596633371902%",$acqAutBassEmAdAlimen,$html);
              $html = str_replace("%ad_alimentazione_1630047993724%","",$html);
	}

	$html = str_replace("%numero_serie_marca_da_bollo_1596634192725%",$numSerieMarcaBollo,$html);
	$html = str_replace("%data_marca_da_bollo_1596634244112%",$dataMarcaBollo,$html);
	
	$dompdf = new DOMPDF();
	$dompdf->set_option('enable_font_subsetting', true);
    $dompdf->set_base_path( $basepath );
    $dompdf->load_html( $html );
    $dompdf->render();
	$pdf = $dompdf->output();
	
	$pdf_encoded_string = base64_encode($pdf);
	echo 'data:application/pdf;base64,' . $pdf_encoded_string;
	exit;
}

public function add_PDF_templateAjax() {
		global $wpdb;
		$result = array();
		$formID = $_REQUEST['form_id'];
		$pdf_template_Id = $_REQUEST['pdf_template_Id'];
		$templateName = $_REQUEST['pdf_template_name'];
		$templateSuffix = $_REQUEST['pdf_template_suffix'];
		$mpId = $_REQUEST['mp_id'];
		$webServiceId = $_REQUEST['web_service_id'];
		$mpName = $_REQUEST['mp_Name'];
		$userIds = implode(",",$_REQUEST['user_ids']);
        $templateDat = "" . $_REQUEST['pdf_template_content'];
		$metadata = [
            'comune_terni_cognome' => $_REQUEST['comune_terni_cognome'] ?? '',
            'comune_terni_nome' => $_REQUEST['comune_terni_nome'] ?? '',
            'comune_terni_email' => $_REQUEST['comune_terni_email'] ?? '',
            'comune_terni_codice_fiscale' => $_REQUEST['comune_terni_codice_fiscale'] ?? '',
            'comune_terni_searchvalue' => $_REQUEST['comune_terni_searchvalue'] ?? '',
            'comune_terni_path' => $_REQUEST['comune_terni_path'] ?? '',
            'comune_terni_soggetto' => $_REQUEST['comune_terni_soggetto'] ?? '',
			'pago_pa_tipo_identificativo' => $_REQUEST['pago_pa_tipo_identificativo'] ?? '',
			'pago_pa_codice_identificativo' => $_REQUEST['pago_pa_codice_identificativo'] ?? '',
			'pago_pa_anagrafica_pagatore' => $_REQUEST['pago_pa_anagrafica_pagatore'] ?? '',
			'pago_pa_indirizzo_pagatore' => $_REQUEST['pago_pa_indirizzo_pagatore'] ?? '',
			'pago_pa_civico_pagatore' => $_REQUEST['pago_pa_civico_pagatore'] ?? '',
			'pago_pa_cap_pagatore' => $_REQUEST['pago_pa_cap_pagatore'] ?? '',
			'pago_pa_localita_pagatore' => $_REQUEST['pago_pa_localita_pagatore'] ?? '',
			'pago_pa_provincia_pagatore' => $_REQUEST['pago_pa_provincia_pagatore'] ?? '',
			'pago_pa_nazione_pagatore' => $_REQUEST['pago_pa_nazione_pagatore'] ?? '',
			'pago_pa_email_pagatore' => $_REQUEST['pago_pa_email_pagatore'] ?? '',
			'pago_pa_identificativo_univoco_dovuto' => $_REQUEST['pago_pa_identificativo_univoco_dovuto'] ?? '',
			'pago_pa_importo_singolo_versamento' => $_REQUEST['pago_pa_importo_singolo_versamento'] ?? '',
			'pago_pa_identificativo_tipo_dovuto' => $_REQUEST['pago_pa_identificativo_tipo_dovuto'] ?? '',
			'pago_pa_causale_versamento' => $_REQUEST['pago_pa_causale_versamento'] ?? '',
			'pago_pa_dati_specifici_riscossione' => $_REQUEST['pago_pa_dati_specifici_riscossione'] ?? '',

        ];
		if(!empty($templateDat)){
			$templateDat = stripcslashes(html_entity_decode($templateDat));
		}
        $metadata = json_encode(stripslashes_deep($metadata));
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$todayDate = esc_sql(date('Y-m-d'));
		if($webServiceId == 2){
			$mpId = -1;
		}
		if(!empty($pdf_template_Id)) {
			$id = $wpdb->query(
			    $wpdb->prepare(
			        "UPDATE $table_name SET formID=%s, web_service_id=%s, templateName=%s, mpId=%s, mpName=%s, templateDat=%s, templateSuffix=%s, metadata=%s, user_ids=%s WHERE id = %d ",
                    $formID, $webServiceId, $templateName, $mpId, $mpName, $templateDat, $templateSuffix, $metadata, $userIds, $pdf_template_Id
                )
            );
		} else {
			$id = $wpdb->query(
                $wpdb->prepare(
                    "INSERT INTO $table_name (`formID`, `templateName`, `web_service_id`, `mpId`, `mpName`, `templateDat`, `date`, `templateSuffix`, `metadata`, `user_ids`) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    $formID, $templateName, $webServiceId, $mpId, $mpName, $templateDat, $todayDate, $templateSuffix, $metadata, $userIds
                )
            );
        }

		if(empty($id) && !empty($pdf_template_Id)){
			$id = $pdf_template_Id;
		}

		if(empty($wpdb->last_error)) {
			$result['success'] = $id;
        } else {
            $result['error'] = 'Could not save the template.';
        }
		echo json_encode($result);
        exit;
}

public function ninja_forms_suffix_column(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'nf_pdf_template';
    $check_column = (array) $wpdb->get_results(  "SELECT count(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME = '{$table_name}' AND COLUMN_NAME = 'templateSuffix'")[0];
    $check_column = (int) array_shift($check_column);
    if($check_column == 0) {
        $wpdb->query("ALTER TABLE $table_name ADD COLUMN `templateSuffix` VARCHAR(255) NOT NULL");
    }
}

public function preview_PDF_templateAjax() {
	global $wpdb;
	$result = array();
	$formID = $_REQUEST['form_id'];
	$pdf_template_Id = $_REQUEST['pdf_template_Id'];
	$templateName = $_REQUEST['pdf_template_name'];
	$mpId = $_REQUEST['mp_id'];
	$mpName = $_REQUEST['mp_Name'];
	$templateDat = $_REQUEST['pdf_template_content'];
	$templateDat = stripslashes($templateDat);
	$nf_fields = Ninja_Forms()->form( $formId )->get_fields();
	$data = array();
	$fields = array();
	$args = array_merge( array(
	    'title'    => 'preview',
	    'table'    => $templateDat,
	    'fields'   => $fields,
	    'css_path' => NF_PDF()->locate_template( 'pdf.css' )
	), $data );

	$html = NF_PDF()->get_template( 'pdf.php', $args );

	$basepath = NF_PDF()->dir( 'templates/' );


	$dompdf = new DOMPDF();
	$dompdf->set_option('enable_font_subsetting', true);
    $dompdf->set_base_path( $basepath );
    $dompdf->load_html( $html );
    $dompdf->render();
	$pdf = $dompdf->output();
	$pdf_encoded_string = base64_encode($pdf);
	//$result['pdf'] = $pdf_encoded_string;
	//$file_path = $_REQUEST['file_path'];
	//$pdf = file_get_contents($file_path);
	echo 'data:application/pdf;base64,' . $pdf_encoded_string;
	exit;
}


public function remove_PDF_templateAjax() {
        global $wpdb;
		$result = array();
		$formID = $_REQUEST['form_id'];
		$pdf_template_Id = $_REQUEST['pdf_template_Id'];
		$table_name = $wpdb->prefix . "nf_pdf_template";
		if(!empty($pdf_template_Id) && !empty($formID)) {
			$rowDel = $wpdb->delete( $table_name, array( 'id' => $pdf_template_Id, 'formID' => $formID ) );
		}
		if($rowDel) {
            $result['success'] = 'PDF Template is deleted.';
        } else {
            $result['error'] = 'Could not delete the template.';
        }
		echo json_encode($result);
        exit;
}

function get_PDF_template($pdf_template_Id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name WHERE id = $pdf_template_Id ";
		$data = $wpdb->get_results($query);

		return $data[0];
}

function get_doc_templates() {

		$ismssqlconnected = false;
		if(function_exists('sqlsrv_connect')){
			$serverName = MSSQL_DB_HOST;
			if(isset($serverName) && ($serverName != null)){
				$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
				$conn = sqlsrv_connect( $serverName, $connectionInfo);
				if( $conn ) {
					$ismssqlconnected = true;
					echo "<br />";
			   }else{
					echo "Connection could not be established to MSSQL.Switching to local DB<br />";
			   }
			}
		}

		if($ismssqlconnected === true){
			$table_name = "ViewDocumentTemplate";
			$query = "SELECT * FROM $table_name WHERE IsValid = 1";
			$sqlquery = sqlsrv_query($conn,$query);
			if ($sqlquery) {
				while ($row = sqlsrv_fetch_object($sqlquery)) {
					$data[] = $row;
				}
				sqlsrv_free_stmt($sqlquery);
			}
			sqlsrv_close($conn);
		}else {
			global $wpdb;
			$table_name = "servmask_prefix_documenttemplate";
			$query = "SELECT * FROM $table_name WHERE IsValid = 1";
			$data = $wpdb->get_results($query);
		}
		return $data;
}

function get_web_services() {
    global $wpdb;
    $table_name = "servmask_prefix_portal";
    $query = "SELECT id, web_service_name  FROM $table_name";
    $data = $wpdb->get_results($query);

    return $data;
}

function getJSONNinjaFormFieldsGroupByFormId() {
    global $wpdb;
    $results = $wpdb->get_results( "SELECT parent_id, id, label, type FROM {$wpdb->prefix}nf3_fields" );

    foreach ($results as $field) {
        if ($field->type == 'submit') {
            continue;
        }
        if (!isset($data[$field->parent_id])) {
            $data[$field->parent_id] = [];
        }
        $data[$field->parent_id][] = [
            'id' => $field->id,
            'label' => $field->label,
        ];
    }

    return json_encode($data);
}

function get_PDF_temppale_byFormId($formId) {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name WHERE formID = $formId ";
		$data = $wpdb->get_results($query);

		return $data[0];
}

function get_PDF_templates() {
		global $wpdb;
		$table_name = $wpdb->prefix . "nf_pdf_template";
		$query = "SELECT * FROM $table_name ";
		$data = $wpdb->get_results($query);

		return $data;
}

public function get_NF_FieldsAjax() {
		$result = array();
		$formID = $_REQUEST['form_id'];
		$nf_field = array();
		if(!empty($formID)) {
			$nf_fields = Ninja_Forms()->form( $formID )->get_fields();
			foreach( $nf_fields as $field ){
				$nf_field[$field->get_setting( 'key' )] = $field->get_setting( 'label' );
			}
		}
		if(!empty($nf_field)) {
            $result['success'] = $nf_field;
        } else {
            $result['error'] = 'Could not get the fields.';
        }
		echo json_encode($result);
        exit;
}

function get_document_data($protocol_template_id) {
	$serverName = MSSQL_DB_HOST;
	$ismssqlconnected = false;
	if(isset($serverName) && ($serverName != null)){
		$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);
		if( $conn ) {
			$ismssqlconnected = true;

	   }else{
			if ( WP_DEBUG === true ) {
				error_log('Connection could not be established to MSSQL.Switching to local DB');
			}
	   }
	}
	if($ismssqlconnected === true){
		$data = [];
		$query = "Select * from ViewDocumentTemplate where ID=" . $protocol_template_id;
		$sqlquery = sqlsrv_query($conn,$query);
		if ($sqlquery) {
			while ($row = sqlsrv_fetch_object($sqlquery)) {
				$data[] = $row;
			}
			sqlsrv_free_stmt($sqlquery);
			return $data;
		}else {
			if ( WP_DEBUG === true ) {
				error_log('No data fetched from ViewDocumentTemplate for ID ' . $protocol_template_id);
			}
		}
		sqlsrv_close($conn);
	}
}

function get_address_data($email_address) {
	$serverName = MSSQL_DB_HOST;
	$ismssqlconnected = false;
	if(isset($serverName) && ($serverName != null)){
		$connectionInfo = array( "Database"=>MSSQL_DB_NAME, "UID"=>MSSQL_DB_USER, "PWD"=>MSSQL_DB_PASSWORD);
		$conn = sqlsrv_connect( $serverName, $connectionInfo);
		if( $conn ) {
			$ismssqlconnected = true;

	   }else{
			if ( WP_DEBUG === true ) {
				error_log('Connection could not be established to MSSQL.Switching to local DB');
			}
	   }
	}
	if($ismssqlconnected === true){
		$query = "Select id from ViewAddress where Email='" . $email_address . "' order by id desc";
		$sqlquery = sqlsrv_query($conn,$query);
		if ($sqlquery) {
			while ($row = sqlsrv_fetch_object($sqlquery)) {
				$data[] = $row;
			}
			sqlsrv_free_stmt($sqlquery);
			sqlsrv_close($conn);
			return $data;
		}else {
			if ( WP_DEBUG === true ) {
				error_log('No data fetched from ViewAddress for Email ' . $email_address);
			}
			sqlsrv_close($conn);
		}
	}
}

}
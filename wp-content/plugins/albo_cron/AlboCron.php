<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Albo Cron
 * Description:       Run Albo Job
 * Version:           1.0.0
 * Text Domain:       albo-cron
 */


if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/AlboCronFunctions.php');
define("PAC_URL", plugin_dir_url(dirname(__FILE__) . '/AlboCron.php'));

if (!class_exists('AlboCron')) {
    class AlboCron

    {
        var $version;
        var $minium_WP   = '3.1';
        
        function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->define_tables();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('AlboCron', 'activate'));
            register_deactivation_hook(__FILE__, array('AlboCron', 'deactivate'));
            register_uninstall_hook(__FILE__, array('AlboCron', 'uninstall'));

            add_action('init', array('AlboCron', 'init'));
        }

        function define_tables()
        {
            global $wpdb, $table_prefix;
            $wpdb->table_name_Albo_Pretorio_Atti = $table_prefix  . "albo_pretorio_atti";
            $wpdb->table_name_Albo_Portal = $table_prefix  . "albo_portal";
            $wpdb->table_name_Albo_Config = $table_prefix  . "albo_config";
        }

        static function init()
	    {
	    }

        static function screen_option()
	    {
            
        }

        
        static function activate()
        {
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);

            global $wpdb;
            $table_name = $wpdb->table_name_Albo_Pretorio_Atti;
            $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );
            if ( ! $wpdb->get_var( $query ) == $table_name ) {
                $charset = $wpdb->get_charset_collate();
                $sql = "
                CREATE TABLE IF NOT EXISTS $table_name(
                    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                    anno int(4) NOT NULL DEFAULT '0',
                    numero int(4) NOT NULL DEFAULT '0',
                    tipo varchar(255) NOT NULL,
                    tipoattoid int(8) NOT NULL,
                    data date NOT NULL DEFAULT '0000-00-00',
                    atto varchar(255) NOT NULL,
                    proponenti varchar(255) NULL,
                    iniziopubblicazione timestamp NULL,
                    finepubblicazione timestamp NULL,
                    esecutivitaimmediata varchar(255) NULL,
                    ggesecutivita int(8) NULL,
                    dataesecutivita timestamp NULL,
                    tipopubblicazione varchar(255) NULL,
                    oggetto varchar(1024) NULL,
                    sorgente varchar(255) NULL,
                    path_file varchar(1024) NULL,
                    attoid int(8) NULL,
                    tipocodice varchar(5) NULL,
                    uploaddocid int(8) NULL,
                    upload_file_path varchar(1024) NULL,
                    PRIMARY KEY (id)
                ) $charset;
                ";
                $wpdb->query( $sql );
            }


            $table_name = $wpdb->table_name_Albo_Portal;
            $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );
            if ( ! $wpdb->get_var( $query ) == $table_name ) {
                $charset = $wpdb->get_charset_collate();
                $sql = "
                CREATE TABLE IF NOT EXISTS $table_name(
                    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                    web_service_name varchar(255) NOT NULL,
                    soap_url varchar(255) NOT NULL,
                    soap_username varchar(255) NULL,
                    soap_password varchar(255) NULL,
                    name_space varchar(255) NULL,
                    PRIMARY KEY (id)
                ) $charset;
                ";
                $wpdb->query( $sql );
            }

            $table_name = $wpdb->table_name_Albo_Config;
            $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );
            if ( ! $wpdb->get_var( $query ) == $table_name ) {
                $charset = $wpdb->get_charset_collate();
                $sql = "
                CREATE TABLE IF NOT EXISTS $table_name(
                    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                    config_name varchar(255) NOT NULL,
                    config_value varchar(255) NOT NULL,
                    PRIMARY KEY (id)
                ) $charset;
                ";
                $wpdb->query( $sql );
            }

            if ( ! wp_get_schedule( 'albo_cron_delivery' ) ) {
                $interval = '60s';
                if (defined('ALBO_CRON_INTERVAL_SECONDS')) {
                    $interval = ALBO_CRON_INTERVAL_SECONDS . 's';
                }
                wp_schedule_event( time(), $interval, 'albo_cron_delivery' );
            }
        }

        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
            wp_clear_scheduled_hook('albo_cron_delivery');
        }
        static function uninstall()
        {
        }


    }
    global $PMR_OnLine;
    $PMR_OnLine = new AlboCron();
}

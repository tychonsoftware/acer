<?php
/**
 * Functions necessary to the plugin
 * @package    Albo Cron
 */
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }




//Add cron schedules filter with upper defined schedule.


//Custom function to be called on schedule triggered.

add_filter( 'cron_schedules', 'albo_cron_schedules' );


function albo_cron_schedules( $schedules ) {
    $interval = '10';

    if (defined('ALBO_CRON_INTERVAL_SECONDS')) {
        $interval = ALBO_CRON_INTERVAL_SECONDS;
    }
    if ( ! isset( $schedules[$interval.  's'] ) ) {
        $schedules[$interval . 's'] = array(
            'interval' => $interval,
            'display'  => __( 'Once every' . $interval . ' seconds' )
        );
    }

    return $schedules;
}


//Custom function to be called on schedule triggered.
function scheduleAlboCron() {
    error_log( "Albo Cron triggered!" );
    $limit_records = 10;
    $file_path = '';
    $metadata_year = date("Y");
    $number_to = false;
    // Read Config Start
    $configs = albo_get_config();
    if($configs && count($configs> 0 )) {
        foreach ($configs as $config) {
            if ($config->config_name === 'limit_records') {
                $limit_records = $config->config_value;
            }else if ($config->config_name === 'file_path') {
                $file_path = $config->config_value;
            } else if ($config->config_name === 'year_getmetadata' && $config->config_value !== 'CURRENT') {
                $metadata_year = $config->config_value;
            } else if ($config->config_name === 'number_to' && $config->config_value !== '') {
                $number_to = $config->config_value === 'true';
            }
        }
    }
    error_log('albo_cron :' . print_r($metadata_year, 1));
    // Read Config End

    // Call Soap Services Start
    $ws_configs = albo_get_portal();
    if($ws_configs && count($ws_configs) > 0) {
        $ws_config = $ws_configs[0];
        $ws_url = $ws_config->soap_url;
        $soap_username = $ws_config->soap_username;
        $soap_password = $ws_config->soap_password;
        $name_space = $ws_config->name_space;
        $soap_do = curl_init();
        $result = send_request($ws_url,get_login_xml($soap_username, $soap_password), 'LoginRequestType',
            $soap_do, '');
        error_log('albo_cron: ------------Login Request result-----------' . print_r($result,1));
        if (isset($result['http_status']) && $result['http_status'] > 300) {
            error_log('albo_cron: ------------Login Errore durante la protocollazione della domanda. Si prega di riprovare-----------');
        } else {
            $xml_response = null;
            $responseBody = $result['response_body'];
            preg_match('/<(?i)S[\s\S]*nvelope>/', $responseBody, $xml_response);
            $xml_response = reset($xml_response);
            $xml = simplexml_load_string($xml_response);
            $xml->registerXPathNamespace('ns2', 'http://www.consorziocsa.it/easypad/ws');
            $login_response = json_decode(json_encode($xml->xpath('//ns2:LoginResponse')[0]));
            if($login_response != null && $login_response->LoginResponse != null){
                $token = $login_response->LoginResponse->Token;
                error_log('albo_cron: ------------Token' . print_r($token,1));
                // DE
                $NumeroDal = 1;
                $tipo = 'DE';
                $annodata = albo_get_atti($tipo, $metadata_year);
                if ($annodata && count($annodata) > 0) {
                    $annorow = $annodata[0];
                    $NumeroDal = $annorow->numero + 1;
                }
                get_meta_data($ws_url, $tipo, $metadata_year, $NumeroDal, $token, $limit_records, $soap_do, $name_space, $file_path, $number_to);

                // DT
                $NumeroDal = 1;
                $tipo = 'DT';
                $annodata = albo_get_atti($tipo, $metadata_year);
                if ($annodata && count($annodata) > 0) {
                    $annorow = $annodata[0];
                    $NumeroDal = $annorow->numero + 1;
                }
                get_meta_data($ws_url, $tipo, $metadata_year, $NumeroDal, $token, $limit_records, $soap_do, $name_space, $file_path, $number_to);

                // CL
                $NumeroDal = 1;
                $tipo = 'CL';
                $annodata = albo_get_atti($tipo, $metadata_year);
                if ($annodata && count($annodata) > 0) {
                    $annorow = $annodata[0];
                    $NumeroDal = $annorow->numero + 1;
                }
                get_meta_data($ws_url, $tipo, $metadata_year, $NumeroDal, $token, $limit_records, $soap_do, $name_space, $file_path, $number_to);
                curl_close($soap_do);
            } // login response token
        }// Login Success
    }
    // Call Soap Services End


}

function albo_get_config() {
    global $wpdb;
    return $wpdb->get_results('SELECT config_name,config_value FROM ' . $wpdb->table_name_Albo_Config);
}

function albo_get_portal() {
    global $wpdb;
    return $wpdb->get_results("select web_service_name, soap_url, soap_username, soap_password, name_space FROM " . $wpdb->table_name_Albo_Portal . " where web_service_name='Albo WS'");
}


function albo_get_atti($tipocodice, $metadata_year) {
    global $wpdb;
    return $wpdb->get_results("Select numero from " . $wpdb->table_name_Albo_Pretorio_Atti . " where tipocodice='" . $tipocodice . "' and Anno = " . $metadata_year . "  order by Anno,Numero desc");
}


function send_request($wsdl,$xml_post_string,$request,  $soap_do, $auth_header): array
{
    error_log('albo_cron :------------' . $request . ' Request Start-----------');
    error_log('albo_cron :------------ XML -----------' . print_r($xml_post_string,1));
    $headers = array(
        "Accept-Encoding: gzip,deflate",
        "Content-Type: text/xml;charset=UTF-8",
        "SOAPAction: \"\"",
        "Content-Length: ".strlen($xml_post_string)
    );
    if(!empty($auth_header)){
        $headers[] = $auth_header;
    }
    curl_setopt($soap_do, CURLOPT_URL, $wsdl);
    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 180);
    curl_setopt($soap_do, CURLOPT_TIMEOUT,        180);
    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($soap_do, CURLOPT_POST,           true);
    curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_post_string);
    curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $headers);
    curl_setopt($soap_do, CURLOPT_HEADER, 1);
    curl_setopt($soap_do, CURLOPT_FAILONERROR, FALSE);
    $response  = curl_exec($soap_do);
    $header_size = curl_getinfo($soap_do, CURLINFO_HEADER_SIZE);
    $body = substr($response, $header_size);
    $http_status = curl_getinfo($soap_do, CURLINFO_HTTP_CODE);
    $curl_errno= curl_errno($soap_do);


    error_log('albo_cron :-----------' . $request . ' Request End------------');
    return array(
        'response' => $response,
        'http_status' => $http_status,
        'curl_errno' => $curl_errno,
        'response_body' => $body,
        '$soap_do' => $soap_do
    );
}

function get_meta_data($ws_url, $tipo, $year, $numero_dal, $token, $limit_records, $soap_do,
                       $name_space, $file_path, $number_to){
    $IpaMetadataItems = array();
    $result = send_request($ws_url,
        get_metadata_by_parameters_xml($year, $numero_dal, $tipo, 'T',$number_to, $limit_records),'GetMetadataByParameters', $soap_do, 'TOKEN_SESSION_ID:' . $token);
    error_log('albo_cron: ------------GetMetadataByParameters(' .  $tipo . '-' . $year . '-' . $numero_dal . ') result-----------' . print_r($result,1));
    if (isset($result['http_status']) && $result['http_status'] > 300) {
        error_log('albo_cron: ------------GetMetadataByParameters(' .  $tipo . ') Errore durante la protocollazione della domanda. Si prega di riprovare-----------');
    }else {

        $xml_response = null;
        $responseBody = $result['response_body'];
        preg_match('/<(?i)S[\s\S]*nvelope>/', $responseBody, $xml_response);
        $xml_response = reset($xml_response);
        $xml = simplexml_load_string($xml_response);
        $xml->registerXPathNamespace('ns2', 'http://www.consorziocsa.it/easypad/ws');
        $metadata_by_parameter_response = json_decode(json_encode($xml->xpath('//ns2:GetMetadataByParametersResponse')[0]));
       error_log('albo_cron:------------GetMetadataByParameters(' . $tipo . ') response-----------' . print_r($metadata_by_parameter_response,1));
        $IpaMetadataItems = [];
        if($metadata_by_parameter_response != null && $metadata_by_parameter_response->GetMetadataResponseType != null
            && isset($metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem)){
            $item = $metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem;
            if(is_array($item)){
                usort($metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem,
                    function($IpaMetadataItemFirst,$IpaMetadataItemSecond){
                        return $IpaMetadataItemFirst->Numero > $IpaMetadataItemSecond->Numero;
                    });
                $IpaMetadataItems = array_slice($metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem, 0, $limit_records);
            }else {
                array_push($IpaMetadataItems,$metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem);

            }
            //$IpaMetadataItems = array_slice($metadata_by_parameter_response->GetMetadataResponseType->IpaMetadataItem, 0, $limit_records);
        }
        if (count($IpaMetadataItems) > 0) {
            error_log('albo_cron: -----------IpaMetadataItems(' . $tipo . ')-----------');
            processMetadata($ws_url, $IpaMetadataItems, $tipo, $soap_do, 'TOKEN_SESSION_ID:' . $token, $name_space, $file_path);
        }

    }
}

function processMetadata($ws_url, $IpaMetadataItems, $tipo, $soap_do, $auth_header, $name_space, $file_path){
    foreach ($IpaMetadataItems as $IpaMetadataItem) {
        $result = send_request($ws_url,
            get_documents_by_parameters( $IpaMetadataItem->Anno, $IpaMetadataItem->Numero, $IpaMetadataItem->TipoAttoId), 'GetDocumentsByParameters', $soap_do, $auth_header);
        error_log('albo_cron: ------------GetDocumentsByParameters(' . $tipo . ')result-----------' . print_r($result,1));
        if (isset($result['http_status']) && $result['http_status'] > 300) {
            error_log('albo_cron: ------------GetDocumentsByParameters('. $tipo . ')Errore durante la protocollazione della domanda. Si prega di riprovare-----------');
        }else {
            $xml_response = null;
            $responseBody = $result['response_body'];
            preg_match('/<(?i)S[\s\S]*nvelope>/', $responseBody, $xml_response);
            $xml_response = reset($xml_response);
            $xml = simplexml_load_string($xml_response);
            $xml->registerXPathNamespace('ns2', 'http://www.consorziocsa.it/easypad/ws');
            $documents_by_parameter_response = json_decode(json_encode($xml->xpath('//ns2:GetDocumentsByParametersResponse')[0]));
            if($documents_by_parameter_response != null && $documents_by_parameter_response->GetDocumentsResponseType != null
                && isset($documents_by_parameter_response->GetDocumentsResponseType->IpaDocumentItem)
                && isset($documents_by_parameter_response->GetDocumentsResponseType->IpaDocumentItem->AttoId)){
                $attachment_path = null;
                $attachment_upload_path = null;
                $IpaDocumentItem = $documents_by_parameter_response->GetDocumentsResponseType->IpaDocumentItem;
                $result = send_request($ws_url,
                    get_retrieve_document( $IpaDocumentItem->AttoId), 'RetrieveDocumentMode', $soap_do, $auth_header);
                if (isset($result['http_status']) && $result['http_status'] > 300) {
                    error_log('albo_cron: ------------RetrieveDocumentMode Errore durante la protocollazione della domanda. Si prega di riprovare-----------');
                }else {
                    error_log('albo_cron: ------------File pah is -----------' . $file_path . '/' . $IpaDocumentItem->AttoId);
                    if (!file_exists($file_path . '/' . $IpaDocumentItem->AttoId)) {
                        mkdir($file_path . '/' . $IpaDocumentItem->AttoId, 0777, true);
                    }
                    $xml_response = null;
                    $responseBody = $result['response_body'];
                    $responseBody = trim($responseBody);
                    preg_match('/<(?i)S[\s\S]*nvelope>/', $responseBody, $xml_response);
                    $xml_response = reset($xml_response);
                    $xml = simplexml_load_string($xml_response);
                    $xml->registerXPathNamespace('ns2', 'http://www.consorziocsa.it/easypad/ws');
                    $retrieve_document_response = json_decode(json_encode($xml->xpath('//ns2:RetrieveDocumentModeResponse')[0]));

                    if($retrieve_document_response != null && isset($retrieve_document_response->GetDocumentResponseType)
                        && isset($retrieve_document_response->GetDocumentResponseType->MessageResponse)
                        && isset($retrieve_document_response->GetDocumentResponseType->MessageResponse->Message)){
                        if(strcasecmp($retrieve_document_response->GetDocumentResponseType->MessageResponse->Message,
                                'Documento non trovato per la richiesta effettuata!') < 0){
                            $contentType = curl_getinfo($soap_do, CURLINFO_CONTENT_TYPE);
                            error_log('albo_cron: ------------RetrieveDocumentMode Response ContentType-----------'. print_r($contentType,1));
                            //if (false !== stripos($contentType, 'multipart/related')) {
                            error_log('albo_cron: ------------multipart/related content type found-----------');
                            // parse mime message
                            error_log('albo_cron: ------------Parse Mime Message -----------');
                            $headers = array(
                                'Content-Type' => trim($contentType),
                            );

                            $multipart = parseMimeMessage($responseBody, $headers);
                            $doc = new DOMDocument();
                            $doc->loadXML($xml_response);
                            $includes = $doc->getElementsByTagNameNS(Helper::NS_XOP, 'Include');
                            $include = $includes->item(0);
                            $ref = $include->getAttribute('href');
                            error_log('albo_cron: ------------REF -----------' . print_r($ref,1));
                            if ('cid:' === substr($ref, 0, 4)) {
                                $contentId = urldecode(substr($ref, 4));
                                error_log('albo_cron: ------------Attachment $contentId-----------' . print_r($contentId,1));
                            }
                            $attachments = $multipart->getParts(false);

                            if(isset($retrieve_document_response->GetDocumentResponseType->FileName)){
                                error_log('albo_cron: ------------FileName-----------' . print_r($retrieve_document_response->GetDocumentResponseType->FileName,1));
                                if(isset($attachments)){
                                    error_log('albo_cron: ------------Attachements-----------' . print_r(count($attachments),1));
                                    foreach ($attachments as $cid => $attachment) {
                                        $attachment_path = $file_path . '/' . $IpaDocumentItem->AttoId
                                            . '/' . $retrieve_document_response->GetDocumentResponseType->FileName;
                                        $fp = fopen($attachment_path, 'w');
                                        fwrite($fp, base64_decode(base64_encode($attachment)));
                                        fclose($fp);
                                    }
                                }else {
                                    error_log('albo_cron: ------------NO ATTACHMENTS-----------');
                                }
                            }
                        }
                    }
                    //}
                }
                $result = send_request($ws_url,
                    get_retrieve_second_document( $IpaDocumentItem->AttoId), 'RetrieveDocumentMode - 2', $soap_do, $auth_header);
                if (isset($result['http_status']) && $result['http_status'] > 300) {
                    error_log('albo_cron: ------------RetrieveDocumentMode 2 Errore durante la protocollazione della domanda. Si prega di riprovare-----------');
                }else {
                    error_log('albo_cron: ------------File path 2 is -----------' . $file_path . '/' . $IpaDocumentItem->AttoId);
                    $xml_response = null;
                    $responseBody = $result['response_body'];
                    $responseBody = trim($responseBody);
                    preg_match('/<(?i)S[\s\S]*nvelope>/', $responseBody, $xml_response);
                    $xml_response = reset($xml_response);
                    $xml = simplexml_load_string($xml_response);
                    $xml->registerXPathNamespace('ns2', 'http://www.consorziocsa.it/easypad/ws');
                    $retrieve_document_response = json_decode(json_encode($xml->xpath('//ns2:RetrieveDocumentModeResponse')[0]));

                    if($retrieve_document_response != null && isset($retrieve_document_response->GetDocumentResponseType)
                        && isset($retrieve_document_response->GetDocumentResponseType->MessageResponse)
                        && isset($retrieve_document_response->GetDocumentResponseType->MessageResponse->Message)){
                        if(strcasecmp($retrieve_document_response->GetDocumentResponseType->MessageResponse->Message,
                                'Documento non trovato per la richiesta effettuata!') < 0){
                            $contentType = curl_getinfo($soap_do, CURLINFO_CONTENT_TYPE);
                            error_log('albo_cron: ------------RetrieveDocumentMode Response 2 ContentType-----------'. print_r($contentType,1));
                            //if (false !== stripos($contentType, 'multipart/related')) {
                            error_log('albo_cron: ------------multipart/related content type 2 found-----------');
                            // parse mime message
                            error_log('albo_cron: ------------Parse Mime Message 2-----------');
                            $headers = array(
                                'Content-Type' => trim($contentType),
                            );

                            $multipart = parseMimeMessage($responseBody, $headers);
                            $doc = new DOMDocument();
                            $doc->loadXML($xml_response);
                            $includes = $doc->getElementsByTagNameNS(Helper::NS_XOP, 'Include');
                            $include = $includes->item(0);
                            $ref = $include->getAttribute('href');
                            error_log('albo_cron: ------------REF 2-----------' . print_r($ref,1));
                            if ('cid:' === substr($ref, 0, 4)) {
                                $contentId = urldecode(substr($ref, 4));
                                error_log('albo_cron: ------------Attachment $contentId 2-----------' . print_r($contentId,1));
                            }
                            $attachments = $multipart->getParts(false);

                            if(isset($retrieve_document_response->GetDocumentResponseType->FileName)){
                                error_log('albo_cron: ------------FileName 2-----------' . print_r($retrieve_document_response->GetDocumentResponseType->FileName,1));
                                if(isset($attachments)){
                                    error_log('albo_cron: ------------Attachements 2-----------' . print_r(count($attachments),1));
                                    foreach ($attachments as $cid => $attachment) {
                                        $attachment_upload_path = $file_path . '/' . $IpaDocumentItem->AttoId
                                            . '/' . $retrieve_document_response->GetDocumentResponseType->FileName;
                                        $fp = fopen($attachment_upload_path, 'w');
                                        fwrite($fp, base64_decode(base64_encode($attachment)));
                                        fclose($fp);
                                    }
                                }else {
                                    error_log('albo_cron: ------------NO ATTACHMENTS 2-----------');
                                }
                            }
                        }
                    }
                    //}
                }
                error_log('albo_cron: ------------INSERT IN DB-----------');
                error_log('albo_cron: ------------IpaMetadataItem----------' . print_r($IpaMetadataItem,1));
                $field_arr = [
                    'anno' => $IpaMetadataItem->Anno,
                    'numero' => $IpaMetadataItem->Numero,
                    'tipo' => $IpaMetadataItem->Tipo,
                    'tipoattoid' => $IpaMetadataItem->TipoAttoId,
                    'data' => $IpaMetadataItem->Data,
                    'atto' => $IpaMetadataItem->Atto,
                    'proponenti' => $IpaMetadataItem->Proponenti,
                    'iniziopubblicazione' => $IpaMetadataItem->InizioPubblicazione,
                    'finepubblicazione' => $IpaMetadataItem->FinePubblicazione,
                    'esecutivitaimmediata' => $IpaMetadataItem->EsecutivitaImmediata,
                    'ggesecutivita' => $IpaMetadataItem->GgEsecutivita,
                    'dataesecutivita' => $IpaMetadataItem->DataEsecutivita,
                    'tipopubblicazione' => $IpaMetadataItem->TipoPubblicazione,
                    'oggetto' => $IpaMetadataItem->Oggetto,
                    'sorgente' => $IpaMetadataItem->Sorgente,
                    'path_file' => $attachment_path,
                    'upload_file_path' => $attachment_upload_path,
                    'attoid' => $IpaDocumentItem->AttoId,
                    'tipocodice'=> $tipo,
                ];
                error_log('albo_cron: ------------Insert Data('. $tipo . ') response-----------' . print_r($field_arr,1));
                global $wpdb;
                $wpdb->insert('albo_pretorio_atti',$field_arr);
            }
        }
    }
}

function get_login_xml($soap_user, $soap_password): string
{
    return <<<XML
<soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:ws='http://www.consorziocsa.it/easypad/ws'>
    <soapenv:Header/>
    <soapenv:Body>
      <ws:Login>
         <LoginRequestType>
            <Username>$soap_user</Username>
            <Password>$soap_password</Password>
         </LoginRequestType>
      </ws:Login>
   </soapenv:Body>
  </soapenv:Envelope>
XML;
}

function get_documents_by_parameters($year, $numero, $tipo_atto_id): string
{
    return <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:ws="http://www.consorziocsa.it/easypad/ws">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:GetDocumentsByParameters>
         <GetDocumentsRequestType>
            <Anno>$year</Anno>
            <Numero>$numero</Numero>
            <TipoAttoId>$tipo_atto_id</TipoAttoId>
         </GetDocumentsRequestType>
      </ws:GetDocumentsByParameters>
   </soapenv:Body>
</soapenv:Envelope>
XML;
}

function get_retrieve_second_document($atto_id): string
{
    return <<<XML
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://www.consorziocsa.it/easypad/ws">
       <soapenv:Header/>
       <soapenv:Body>
          <ws:RetrieveDocumentMode>
             <!--Optional:-->
             <GetDocumentModeRequestType>
                <Mode>ALL</Mode>
                <Sorgente>N</Sorgente>
                <AttoId>$atto_id</AttoId>
                <TipoVersione>S</TipoVersione>
             </GetDocumentModeRequestType>
          </ws:RetrieveDocumentMode>
       </soapenv:Body>
    </soapenv:Envelope>
XML;
}

function parseMimeMessage($mimeMessage, array $headers = array())
{
    $boundary = null;
    $start = null;
    $multipart = new MultiPart();
    $hitFirstBoundary = false;
    $inHeader = true;
    // add given headers, e.g. coming from HTTP headers
    if (count($headers) > 0) {
        foreach ($headers as $name => $value) {
            if ($name == 'Content-Type') {
                parseContentTypeHeader($multipart, $name, $value);
                $boundary = $multipart->getHeader('Content-Type', 'boundary');
                $start = $multipart->getHeader('Content-Type', 'start');
            } else {
                $multipart->setHeader($name, $value);
            }
        }
        $inHeader = false;
    }
    $content = '';
    $currentPart = $multipart;
    $lines = preg_split("/(\r\n)/", $mimeMessage);
    foreach ($lines as $line) {
        // ignore http status code and POST *
        if (substr($line, 0, 5) == 'HTTP/' || substr($line, 0, 4) == 'POST') {
            continue;
        }
        if (isset($currentHeader)) {
            if (isset($line[0]) && ($line[0] === ' ' || $line[0] === "\t")) {
                $currentHeader .= $line;
                continue;
            }
            if (strpos($currentHeader, ':') !== false) {
                list($headerName, $headerValue) = explode(':', $currentHeader, 2);
                $headerValue = iconv_mime_decode($headerValue, 0, 'utf-8');
                if (strpos($headerValue, ';') !== false) {
                    parseContentTypeHeader($currentPart, $headerName, $headerValue);
                    $boundary = $multipart->getHeader('Content-Type', 'boundary');
                    $start = $multipart->getHeader('Content-Type', 'start');
                } else {
                    $currentPart->setHeader($headerName, trim($headerValue));
                }
            }
            unset($currentHeader);
        }
        if ($inHeader) {
            if (trim($line) == '') {
                $inHeader = false;
                continue;
            }
            $currentHeader = $line;
            continue;
        } else {
            // check if we hit any of the boundaries
            if (strlen($line) > 0 && $line[0] == "-") {
                if (strcmp(trim($line), '--' . $boundary) === 0) {
                    if ($currentPart instanceof Part) {
                        $content = substr($content, 0, -2);
                        decodeContent($currentPart, $content);
                        // check if there is a start parameter given, if not set first part
                        $isMain = (is_null($start) || $start == $currentPart->getHeader('Content-ID')) ? true : false;
                        if ($isMain === true) {
                            $start = $currentPart->getHeader('Content-ID');
                        }
                        $multipart->addPart($currentPart, $isMain);
                    }
                    $currentPart = new Part();
                    $hitFirstBoundary = true;
                    $inHeader = true;
                    $content = '';
                } elseif (strcmp(trim($line), '--' . $boundary . '--') === 0) {
                    $content = substr($content, 0, -2);
                    decodeContent($currentPart, $content);
                    // check if there is a start parameter given, if not set first part
                    $isMain = (is_null($start) || $start == $currentPart->getHeader('Content-ID')) ? true : false;
                    if ($isMain === true) {
                        $start = $currentPart->getHeader('Content-ID');
                    }
                    $multipart->addPart($currentPart, $isMain);
                    $content = '';
                }
            } else {
                if ($hitFirstBoundary === false) {
                    if (trim($line) != '') {
                        $inHeader = true;
                        $currentHeader = $line;
                        continue;
                    }
                }
                $content .= $line . "\r\n";
            }
        }
    }
    return $multipart;
}

function decodeContent(Part $part, $content)
{
    $encoding = strtolower($part->getHeader('Content-Transfer-Encoding'));
    $charset = strtolower($part->getHeader('Content-Type', 'charset'));
    if ($encoding == Part::ENCODING_BASE64) {
        $content = base64_decode($content);
    } elseif ($encoding == Part::ENCODING_QUOTED_PRINTABLE) {
        $content = quoted_printable_decode($content);
    }
    if ($charset != 'utf-8') {
        $content = iconv($charset, 'utf-8', $content);
    }
    $part->setContent($content);
}

function parseContentTypeHeader(PartHeader $part, $headerName, $headerValue)
{
    list($value, $remainder) = explode(';', $headerValue, 2);
    $value = trim($value);
    $part->setHeader($headerName, $value);
    $remainder = trim($remainder);
    while (strlen($remainder) > 0) {
        if (!preg_match('/^([a-zA-Z0-9_-]+)=(.{1})/', $remainder, $matches)) {
            break;
        }
        $name = $matches[1];
        $delimiter = $matches[2];
        $remainder = substr($remainder, strlen($name)+1);
        if (!preg_match('/([^;]+)(;)?(\s|$)?/', $remainder, $matches)) {
            break;
        }
        $value = rtrim($matches[1], ';');
        if ($delimiter == "'" || $delimiter == '"') {
            $value = trim($value, $delimiter);
        }
        $part->setHeader($headerName, $name, $value);
        $remainder = substr($remainder, strlen($matches[0]));
    }
}

/**
 * Mime multi part container.
 *
 * Headers:
 * - MIME-Version
 * - Content-Type
 * - Content-ID
 * - Content-Location
 * - Content-Description
 *
 * @author Andreas Schamberger <mail@andreass.net>
 */
class MultiPart extends PartHeader
{
    /**
     * Content-ID of main part.
     *
     * @var string
     */
    protected $mainPartContentId;

    /**
     * Mime parts.
     *
     * @var array(\BeSimple\SoapCommon\Mime\Part)
     */
    protected $parts = array();

    /**
     * Construct new mime object.
     *
     * @param string $boundary Boundary string
     *
     * @return void
     */
    public function __construct($boundary = null)
    {
        $this->setHeader('MIME-Version', '1.0');
        $this->setHeader('Content-Type', 'multipart/related');
        $this->setHeader('Content-Type', 'type', 'text/xml');
        $this->setHeader('Content-Type', 'charset', 'utf-8');
        if (is_null($boundary)) {
            $boundary = $this->generateBoundary();
        }
        $this->setHeader('Content-Type', 'boundary', $boundary);
    }

    /**
     * Get mime message of this object (without headers).
     *
     * @param boolean $withHeaders Returned mime message contains headers
     *
     * @return string
     */
    public function getMimeMessage($withHeaders = false)
    {
        $message = ($withHeaders === true) ? $this->generateHeaders() : "";
        // add parts
        foreach ($this->parts as $part) {
            $message .= "\r\n" . '--' . $this->getHeader('Content-Type', 'boundary') . "\r\n";
            $message .= $part->getMessagePart();
        }
        $message .= "\r\n" . '--' . $this->getHeader('Content-Type', 'boundary') . '--';
        return $message;
    }

    /**
     * Get string array with MIME headers for usage in HTTP header (with CURL).
     * Only 'Content-Type' and 'Content-Description' headers are returned.
     *
     * @return arrray(string)
     */
    public function getHeadersForHttp()
    {
        $allowed = array(
            'Content-Type',
            'Content-Description',
        );
        $headers = array();
        foreach ($this->headers as $fieldName => $value) {
            if (in_array($fieldName, $allowed)) {
                $fieldValue = $this->generateHeaderFieldValue($value);
                // for http only ISO-8859-1
                $headers[] = $fieldName . ': '. iconv('utf-8', 'ISO-8859-1//TRANSLIT', $fieldValue);
            }
        }
        return $headers;
    }

    /**
     * Add new part to MIME message.
     *
     * @param \BeSimple\SoapCommon\Mime\Part $part   Part that is added
     * @param boolean                        $isMain Is the given part the main part of mime message
     *
     * @return void
     */
    public function addPart(Part $part, $isMain = false)
    {
        $contentId = trim($part->getHeader('Content-ID'), '<>');
        if ($isMain === true) {
            $this->mainPartContentId = $contentId;
            $this->setHeader('Content-Type', 'start', $part->getHeader('Content-ID'));
        }
        $this->parts[$contentId] = $part;
    }

    /**
     * Get part with given content id. If there is no content id given it
     * returns the main part that is defined through the content-id start
     * parameter.
     *
     * @param string $contentId Content id of desired part
     *
     * @return \BeSimple\SoapCommon\Mime\Part|null
     */
    public function getPart($contentId = null)
    {
        if (is_null($contentId)) {
            $contentId = $this->mainPartContentId;
        }
        if (isset($this->parts[$contentId])) {
            return $this->parts[$contentId];
        }
        return null;
    }

    /**
     * Get all parts.
     *
     * @param boolean $includeMainPart Should main part be in result set
     *
     * @return array(\BeSimple\SoapCommon\Mime\Part)
     */
    public function getParts($includeMainPart = false)
    {
        if ($includeMainPart === true) {
            $parts = $this->parts;
        } else {
            $parts = array();
            foreach ($this->parts as $cid => $part) {
                if ($cid != $this->mainPartContentId) {
                    $parts[$cid] = $part;
                }
            }
        }
        return $parts;
    }

    /**
     * Returns a unique boundary string.
     *
     * @return string
     */
    protected function generateBoundary()
    {
        return 'urn:uuid:' . Helper::generateUUID();
    }
}


/**
 * Mime part base class.
 *
 * @author Andreas Schamberger <mail@andreass.net>
 */
abstract class PartHeader
{
    /**
     * Mime headers.
     *
     * @var array(string=>mixed|array(mixed))
     */
    protected $headers = array();

    /**
     * Add a new header to the mime part.
     *
     * @param string $name     Header name
     * @param string $value    Header value
     * @param string $subValue Is sub value?
     *
     * @return void
     */
    public function setHeader($name, $value, $subValue = null)
    {
        if (isset($this->headers[$name]) && !is_null($subValue)) {
            if (!is_array($this->headers[$name])) {
                $this->headers[$name] = array(
                    '@'    => $this->headers[$name],
                    $value => $subValue,
                );
            } else {
                $this->headers[$name][$value] = $subValue;
            }
        } elseif (isset($this->headers[$name]) && is_array($this->headers[$name]) && isset($this->headers[$name]['@'])) {
            $this->headers[$name]['@'] = $value;
        } else {
            $this->headers[$name] = $value;
        }
    }

    /**
     * Get given mime header.
     *
     * @param string $name     Header name
     * @param string $subValue Sub value name
     *
     * @return mixed|array(mixed)
     */
    public function getHeader($name, $subValue = null)
    {
        if (isset($this->headers[$name])) {
            if (!is_null($subValue)) {
                if (is_array($this->headers[$name]) && isset($this->headers[$name][$subValue])) {
                    return $this->headers[$name][$subValue];
                } else {
                    return null;
                }
            } elseif (is_array($this->headers[$name]) && isset($this->headers[$name]['@'])) {
                return $this->headers[$name]['@'];
            } else {
                return $this->headers[$name];
            }
        }
        return null;
    }

    /**
     * Generate headers.
     *
     * @return string
     */
    protected function generateHeaders()
    {
        $charset = strtolower($this->getHeader('Content-Type', 'charset'));
        $preferences = array(
            'scheme' => 'Q',
            'input-charset' => 'utf-8',
            'output-charset' => $charset,
        );
        $headers = '';
        foreach ($this->headers as $fieldName => $value) {
            $fieldValue = $this->generateHeaderFieldValue($value);
            // do not use proper encoding as Apache Axis does not understand this
            // $headers .= iconv_mime_encode($field_name, $field_value, $preferences) . "\r\n";
            $headers .= $fieldName . ': ' . $fieldValue . "\r\n";
        }
        return $headers;
    }

    /**
     * Generates a header field value from the given value paramater.
     *
     * @param array(string=>string)|string $value Header value
     *
     * @return string
     */
    protected function generateHeaderFieldValue($value)
    {
        $fieldValue = '';
        if (is_array($value)) {
            if (isset($value['@'])) {
                $fieldValue .= $value['@'];
            }
            foreach ($value as $subName => $subValue) {
                if ($subName != '@') {
                    $fieldValue .= '; ' . $subName . '=' . $this->quoteValueString($subValue);
                }
            }
        } else {
            $fieldValue .= $value;
        }
        return $fieldValue;
    }

    /**
     * Quote string with '"' if it contains one of the special characters:
     * "(" / ")" / "<" / ">" / "@" / "," / ";" / ":" / "\" / <"> / "/" / "[" / "]" / "?" / "="
     *
     * @param string $string String to quote
     *
     * @return string
     */
    private function quoteValueString($string)
    {
        if (preg_match('~[()<>@,;:\\"/\[\]?=]~', $string)) {
            return '"' . $string . '"';
        } else {
            return $string;
        }
    }
}

/**
 * Soap helper class with static functions that are used in the client and
 * server implementations. It also provides namespace and configuration
 * constants.
 *
 * @author Andreas Schamberger <mail@andreass.net>
 */
class Helper
{
    /**
     * Attachment type: xsd:base64Binary (native in ext/soap).
     */
    const ATTACHMENTS_TYPE_BASE64 = 1;

    /**
     * Attachment type: MTOM (SOAP Message Transmission Optimization Mechanism).
     */
    const ATTACHMENTS_TYPE_MTOM = 2;

    /**
     * Attachment type: SWA (SOAP Messages with Attachments).
     */
    const ATTACHMENTS_TYPE_SWA = 4;

    /**
     * Web Services Security: SOAP Message Security 1.0 (WS-Security 2004)
     */
    const NAME_WSS_SMS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0';

    /**
     * Web Services Security: SOAP Message Security 1.1 (WS-Security 2004)
     */
    const NAME_WSS_SMS_1_1 = 'http://docs.oasis-open.org/wss/oasis-wss-soap-message-security-1.1';

    /**
     * Web Services Security UsernameToken Profile 1.0
     */
    const NAME_WSS_UTP = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0';

    /**
     * Web Services Security X.509 Certificate Token Profile
     */
    const NAME_WSS_X509 = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0';

    /**
     * Soap 1.1 namespace.
     */
    const NS_SOAP_1_1 = 'http://schemas.xmlsoap.org/soap/envelope/';

    /**
     * Soap 1.1 namespace.
     */
    const NS_SOAP_1_2 = 'http://www.w3.org/2003/05/soap-envelope/';

    /**
     * Web Services Addressing 1.0 namespace.
     */
    const NS_WSA = 'http://www.w3.org/2005/08/addressing';

    /**
     * WSDL 1.1 namespace.
     */
    const NS_WSDL = 'http://schemas.xmlsoap.org/wsdl/';

    /**
     * WSDL MIME namespace.
     */
    const NS_WSDL_MIME = 'http://schemas.xmlsoap.org/wsdl/mime/';

    /**
     * WSDL SOAP 1.1 namespace.
     */
    const NS_WSDL_SOAP_1_1 = 'http://schemas.xmlsoap.org/wsdl/soap/';

    /**
     * WSDL SOAP 1.2 namespace.
     */
    const NS_WSDL_SOAP_1_2 = 'http://schemas.xmlsoap.org/wsdl/soap12/';

    /**
     * Web Services Security Extension namespace.
     */
    const NS_WSS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    /**
     * Web Services Security Utility namespace.
     */
    const NS_WSU = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';

    /**
     * Describing Media Content of Binary Data in XML namespace.
     */
    const NS_XMLMIME = 'http://www.w3.org/2004/11/xmlmime';

    /**
     * XML Schema namespace.
     */
    const NS_XML_SCHEMA = 'http://www.w3.org/2001/XMLSchema';

    /**
     * XML Schema instance namespace.
     */
    const NS_XML_SCHEMA_INSTANCE = 'http://www.w3.org/2001/XMLSchema-instance';

    /**
     * XML-binary Optimized Packaging namespace.
     */
    const NS_XOP = 'http://www.w3.org/2004/08/xop/include';

    /**
     * Web Services Addressing 1.0 prefix.
     */
    const PFX_WSA = 'wsa';

    /**
     * WSDL 1.1 namespace. prefix.
     */
    const PFX_WSDL = 'wsdl';

    /**
     * Web Services Security Extension namespace.
     */
    const PFX_WSS = 'wsse';

    /**
     * Web Services Security Utility namespace prefix.
     */
    const PFX_WSU  = 'wsu';

    /**
     * Describing Media Content of Binary Data in XML namespace prefix.
     */
    const PFX_XMLMIME = 'xmlmime';

    /**
     * XML Schema namespace prefix.
     */
    const PFX_XML_SCHEMA = 'xsd';

    /**
     * XML Schema instance namespace prefix.
     */
    const PFX_XML_SCHEMA_INSTANCE = 'xsi';

    /**
     * XML-binary Optimized Packaging namespace prefix.
     */
    const PFX_XOP = 'xop';

    /**
     * Generate a pseudo-random version 4 UUID.
     *
     * @see http://de.php.net/manual/en/function.uniqid.php#94959
     * @return string
     */
    public static function generateUUID()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}

/**
 * Mime part. Everything must be UTF-8. Default charset for text is UTF-8.
 *
 * Headers:
 * - Content-Type
 * - Content-Transfer-Encoding
 * - Content-ID
 * - Content-Location
 * - Content-Description
 *
 * @author Andreas Schamberger <mail@andreass.net>
 */
class Part extends PartHeader
{
    /**
     * Encoding type base 64
     */
    const ENCODING_BASE64 = 'base64';

    /**
     * Encoding type binary
     */
    const ENCODING_BINARY = 'binary';

    /**
     * Encoding type eight bit
     */
    const ENCODING_EIGHT_BIT = '8bit';

    /**
     * Encoding type seven bit
     */
    const ENCODING_SEVEN_BIT = '7bit';

    /**
     * Encoding type quoted printable
     */
    const ENCODING_QUOTED_PRINTABLE = 'quoted-printable';

    /**
     * Content.
     *
     * @var mixed
     */
    protected $content;

    /**
     * Construct new mime object.
     *
     * @param mixed  $content     Content
     * @param string $contentType Content type
     * @param string $charset     Charset
     * @param string $encoding    Encoding
     * @param string $contentId   Content id
     *
     * @return void
     */
    public function __construct($content = null, $contentType = 'application/octet-stream', $charset = null, $encoding = 'binary', $contentId = null)
    {
        $this->content = $content;
        $this->setHeader('Content-Type', $contentType);
        if (!is_null($charset)) {
            $this->setHeader('Content-Type', 'charset', $charset);
        } else {
            $this->setHeader('Content-Type', 'charset', 'utf-8');
        }
        $this->setHeader('Content-Transfer-Encoding', $encoding);
        if (is_null($contentId)) {
            $contentId = $this->generateContentId();
        }
        $this->setHeader('Content-ID', '<' . $contentId . '>');
    }

    /**
     * __toString.
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->content;
    }

    /**
     * Get mime content.
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set mime content.
     *
     * @param mixed $content Content to set
     *
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get complete mime message of this object.
     *
     * @return string
     */
    public function getMessagePart()
    {
        return $this->generateHeaders() . "\r\n" . $this->generateBody();
    }

    /**
     * Generate body.
     *
     * @return string
     */
    protected function generateBody()
    {
        $encoding = strtolower($this->getHeader('Content-Transfer-Encoding'));
        $charset = strtolower($this->getHeader('Content-Type', 'charset'));
        if ($charset != 'utf-8') {
            $content = iconv('utf-8', $charset . '//TRANSLIT', $this->content);
        } else {
            $content = $this->content;
        }
        switch ($encoding) {
            case ENCODING_BASE64:
                return substr(chunk_split(base64_encode($content), 76, "\r\n"), -2);
            case ENCODING_QUOTED_PRINTABLE:
                return quoted_printable_encode($content);
            case ENCODING_BINARY:
                return $content;
            case ENCODING_SEVEN_BIT:
            case ENCODING_EIGHT_BIT:
            default:
                return preg_replace("/\r\n|\r|\n/", "\r\n", $content);
        }
    }

    /**
     * Returns a unique ID to be used for the Content-ID header.
     *
     * @return string
     */
    protected function generateContentId()
    {
        return 'urn:uuid:' . Helper::generateUUID();
    }
}
add_action( 'albo_cron_delivery', 'scheduleAlboCron' );

<?php

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

################################################################################
// Funzioni 
################################################################################
function ap_get_postids_from_term($term_id_list,$Conteggio=false,$DaRiga=0,$ARiga=20){
	global $wpdb;
	if ($DaRiga==0 AND $ARiga==0)
		$Limite="";
	else
		$Limite=" Limit ".$DaRiga.",".$ARiga;

	if ($Conteggio){
		// $Sql="SELECT COUNT(*)
		// FROM $wpdb->prefix" . "posts as a
		// JOIN $wpdb->prefix" . "term_relationships as b on ( a.ID = b.object_id)
		// JOIN $wpdb->prefix" . "term_taxonomy as c on (b.term_taxonomy_id = c.term_taxonomy_id)
		// JOIN $wpdb->prefix" . "terms as d on (c.term_id = d.term_id)
		// WHERE b.term_taxonomy_id in ($term_id_list) ORDER BY a.id" ;

		$Sql = "SELECT count(*) from (SELECT distinct  
		a.ID, a.post_date, a.post_title, a.post_content, a.guid, d.name,c.term_id,
				(case e.meta_key 
				when '_atexpirationdate' then e.meta_value
				end) as expiration_date
				FROM $wpdb->prefix" . "posts as a
				  JOIN $wpdb->prefix" . "term_relationships as b on ( a.ID = b.object_id)
				  JOIN $wpdb->prefix" . "term_taxonomy as c on (b.term_taxonomy_id = c.term_taxonomy_id)
				  JOIN  $wpdb->prefix" . "terms as d on (c.term_id = d.term_id)
				  JOIN $wpdb->prefix" . "postmeta as e on (a.id = e.post_id)
				  WHERE b.term_taxonomy_id in ($term_id_list) and 
					  (case e.meta_key 
				when '_atexpirationdate' then e.meta_value
				end) is not null) as TMP where STR_TO_DATE(TMP.expiration_date,'%Y-%m-%d,') >= DATE(NOW())";
		return $wpdb->get_var($Sql);
	}else {
		$Sql = "SELECT TMP.* from (SELECT distinct  a.ID, a.post_date, a.post_title, a.post_content,a.guid, d.name,c.term_id,
		(case e.meta_key 
		when '_atexpirationdate' then e.meta_value
		end) as expiration_date
		FROM $wpdb->prefix" . "posts as a
		  JOIN $wpdb->prefix" . "term_relationships as b on ( a.ID = b.object_id)
		  JOIN $wpdb->prefix" . "term_taxonomy as c on (b.term_taxonomy_id = c.term_taxonomy_id)
		  JOIN  $wpdb->prefix" . "terms as d on (c.term_id = d.term_id)
		  JOIN $wpdb->prefix" . "postmeta as e on (a.id = e.post_id)
		  WHERE b.term_taxonomy_id in ($term_id_list) and 
			  (case e.meta_key 
		when '_atexpirationdate' then e.meta_value
		end) is not null) as TMP where STR_TO_DATE(TMP.expiration_date,'%Y-%m-%d,') >= DATE(NOW()) $Limite";

		// $Sql="SELECT a.ID, a.post_date, a.post_title, a.post_content, d.name
		// FROM $wpdb->prefix" . "posts as a
		// JOIN $wpdb->prefix" . "term_relationships as b on ( a.ID = b.object_id)
		// JOIN $wpdb->prefix" . "term_taxonomy as c on (b.term_taxonomy_id = c.term_taxonomy_id)
		// JOIN $wpdb->prefix" . "terms as d on (c.term_id = d.term_id)
		// WHERE b.term_taxonomy_id in ($term_id_list) ORDER BY a.id" ;
		return $wpdb->get_results($Sql);
	}
	
	
	
}

function at_VisualizzaData($dataDB){
	$dataDB=substr($dataDB,0,10);
	$rsl = explode ('-',$dataDB);
	$rsl = array_reverse($rsl);
	return implode($rsl,'/');
}

function at_get_taxonomy_childs($parent_id) {
    global $wpdb;
	return $wpdb->get_results("SELECT * FROM $wpdb->prefix" . "term_taxonomy WHERE parent = ($parent_id)");
}

function at_get_posts_term($term_taxonomy_id){
	global $wpdb;
	$Sql = "SELECT object_id FROM $wpdb->prefix" . "term_relationships WHERE term_taxonomy_id=" .$term_taxonomy_id;
	return $wpdb->get_results($Sql);
}


function at_get_tipo_term_ids($parent_id) {
    global $wpdb;
	return $wpdb->get_results("SELECT * FROM $wpdb->prefix" . "term_taxonomy WHERE taxonomy ='tipologie'");
}

function at_get_tipologies($taxonomy='tipologie') {
    global $wpdb;
	$Sql = "SELECT a.term_id , b.name, b.slug FROM $wpdb->prefix" . "term_taxonomy as a
	  JOIN  $wpdb->prefix" . "terms as b on (a.term_id = b.term_id)
	  WHERE a.taxonomy='$taxonomy' order by b.name";
	return $wpdb->get_results($Sql);
}


function at_get_current_user_sections($user_id = 0) {
	$term_ids = array();
	if($user_id > 0){
		global $wpdb, $table_prefix;
		$wpdb->table_name_UserSections = $table_prefix . "at_user_sections";
		if($user_id > 0) {
			$data = $wpdb->get_results("Select * from $wpdb->table_name_UserSections WHERE user_id=$user_id");
			foreach( $data as $d ){
				array_push($term_ids, $d->term_id);
			}
		}
	}
	return $term_ids;
}


function at_CreaTabella(){
	global $wpdb, $table_prefix;
	$wpdb->table_name_UserSections = $table_prefix . "at_user_sections";
	$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->table_name_UserSections." (
		`user_id` bigint(20) unsigned NOT NULL,
		`term_id` bigint(20) unsigned NOT NULL);";
	$wpdb->query( $sql );

}

function at_save_user_section_mapping() {
	if(isset($_REQUEST['user_id']) && isset($_REQUEST['action']) 
	&& $_REQUEST['action'] == 'at_save_user_section_mapping'){
		$user_id = $_REQUEST['user_id'];
		$term_ids = $_REQUEST['term_ids'];
		global $wpdb, $table_prefix;
		$wpdb->table_name_UserSections = $table_prefix . "at_user_sections";
		if($user_id > 0) {
			$wpdb->delete( $wpdb->table_name_UserSections, array( 'user_id' => $user_id ) );
			if(!empty($term_ids)){
				foreach( $term_ids as $term_id ){
					$insert_sql="INSERT INTO $wpdb->table_name_UserSections (user_id, term_id) VALUES($user_id, $term_id)";
					$wpdb->query($insert_sql);
				}
			}
		}
	}
}

function at_get_user_sections() {
	$result = array();
	if(isset($_REQUEST['at_user_id']) && isset($_REQUEST['action']) 
	&& $_REQUEST['action'] == 'at_get_user_sections'){
		$user_id = $_REQUEST['at_user_id'];
		global $wpdb, $table_prefix;
		$wpdb->table_name_UserSections = $table_prefix . "at_user_sections";
		if($user_id > 0) {
			$term_ids = array();
			$data = $wpdb->get_results("Select * from $wpdb->table_name_UserSections WHERE user_id=$user_id");
			foreach( $data as $d ){
				array_push($term_ids, $d->term_id);
			}
			if(!empty($term_ids)){
				$result['success'] = true;
				$result['terms'] = implode(',', $term_ids);
			}
		}
		echo json_encode($result);
		exit;
	}
	
	return $result;
}

add_filter('pre_get_posts', 'filter_id_to_term_in_query');
function filter_id_to_term_in_query($query) {

	global $pagenow;
	$post_type = 'amm-trasparente';
	$taxonomy  = 'tipologie';
	$q_vars    = &$query->query_vars;
	
	
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type
		&& empty($q_vars[$taxonomy])) {
			$user_term_ids = at_get_current_user_sections(get_current_user_id());
			if(!empty($user_term_ids)){
				$tax_query[] = [
					'taxonomy' => 'tipologie',
					'terms'    => $user_term_ids,
				];
				$query->set( 'tax_query', $tax_query );
			}
	}
}

function at_hide_categories_for_specific_user( $exclusions, $args ){
	global $typenow;
	if ($typenow == 'amm-trasparente') {
		$user_term_ids = at_get_current_user_sections(get_current_user_id());
		$exterms = '';
		if(!empty($user_term_ids)){
			$exterms =  implode(',', $user_term_ids);
		}
		if(!empty($exterms)){
			$exclusions = ' AND  t.term_id IN (' . $exterms . ')';
		}
	}
	return $exclusions;
}

add_filter( 'list_terms_exclusions', 'at_hide_categories_for_specific_user', 10, 2 );

add_filter('wp_count_posts', function($counts, $type, $perm) {
    global $wpdb, $table_prefix;
	$post_type_object = get_post_type_object($type);
	if ($type == 'amm-trasparente') {
		$user_term_ids = at_get_current_user_sections(get_current_user_id());
		if(!empty($user_term_ids)){
			$publish_posts = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "posts WHERE (post_status = 'publish') 
			AND (post_author = '" . get_current_user_id() . "'  AND post_type = 'amm-trasparente' ) ");
			
			$publish_total = 0;
			foreach( $publish_posts as $post ){
				$count = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix . "term_relationships WHERE object_id =  $post->ID
				 AND term_taxonomy_id IN (" . implode(",", $user_term_ids) . ")");
				if($count > 0){
					$publish_total++;
				}
			}
			$draft_posts = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "posts WHERE (post_status = 'draft') 
			AND (post_author = '" . get_current_user_id() . "'  AND post_type = 'amm-trasparente' ) ");
			$draft_total = 0;
			foreach( $draft_posts as $post ){
				$count = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix . "term_relationships WHERE object_id =  $post->ID
				 AND term_taxonomy_id IN (" . implode(",", $user_term_ids) . ")");
				if($count > 0){
					$draft_total++;
				}
			}
			$trash_posts = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "posts WHERE (post_status = 'trash') 
			AND (post_author = '" . get_current_user_id() . "'  AND post_type = 'amm-trasparente' ) ");
			$trash_total = 0;
			foreach( $trash_posts as $post ){
				$count = $wpdb->get_var("SELECT COUNT(*) FROM " . $wpdb->prefix . "term_relationships WHERE object_id =  $post->ID
				 AND term_taxonomy_id IN (" . implode(",", $user_term_ids) . ")");
				if($count > 0){
					$trash_total++;
				}
			}
			$counts -> publish = $publish_total;
			$counts -> draft = $draft_total;
			$counts -> trash = $trash_total;
		}

	}
	
    return (object)$counts;
}, 10, 3);

// add_action( 'wp_loaded', function () {
// 			$user_term_ids = at_get_current_user_sections(get_current_user_id());
// 			if(!empty($user_term_ids)){
// 				$yourJS = '<script type="text/javascript">
// 				window.onload = dothis();
// 				function dothis(){
// 					document.getElementsByClassName("mine")[0].textContent = \'anshuman\';
// 				}
// 		   </script>';
	
// 			echo $yourJS;
// 			}
			
			
// 			// Only target the front end
// 			// Do what you need to do
// 	});

?>
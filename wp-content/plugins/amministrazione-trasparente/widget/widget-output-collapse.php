<!--CSS Code-->
<style type="text/css">
    @media (min-width: 768px) {
        .w-78 {
            width: 78.446%;
            max-width: 78.446%;
        }
        .w-21 {
            width: 21.554%;
            max-width: 21.554%;
        }
    }
.row {vertical-align: top; height:auto !important; font-size: 0.9em;}
.list {display:none;}
.show {display:none;}
.hide:target + .show {display:inline;}
.hide:target {display:none;}
.hide:target ~ .list {display:inline;}
.hideChild {display:none;}
.showChild {display:inline;}
.listChild {display:inline;}


.widget-area .row {
    width: 100% !important;
}

.widget_atwidget .hide b:after{
    content: '\e818';
    font-family: italia-icon-font;
    font-size: 10px;
    font-weight: 100;
    color: #000;
    position: absolute;
    left: 60%;
}
.widget_atwidget .show b:after  {
    content: '\e818';
    font-family: italia-icon-font;
    font-size: 10px;
    font-weight: 100;
    color: #fff;
    position: absolute;
    left: 60%;
}
.widget_atwidget .row {
    width: 70%;
}
.widget_atwidget a.hide, a.show {
    border-bottom: 1px solid #ccc;
    padding-bottom: 10px;
    width: 100%;
}
.widget_atwidget a#hide1{
    border-top: 1px solid #ccc;
    padding-bottom: 10px;
    width: 100%;
}
.widget_atwidget a.hide b {
    background: #f2f2f2;
    padding: 10px 30px;
    width: 100%;
    float: left;
    margin-top: 10px;
}
.widget_atwidget a.show b {
    background: #247b97;
    padding: 10px 30px;
    width: 100%;
    float: left;
    margin-top: 10px;
}

.widget_atwidget li{
    border-top: none;
}
.widget_atwidget a.hide, a.hide:hover{
    color:#000 !important;
}
.widget_atwidget  a.show,a.show:hover{
    color:#fff !important;
}
.widget_atwidget .list li {
    background: #33a6cc;
    margin-bottom: 6px;
    padding:5px 10px;
}
.widget_atwidget .list li > a {
    color: #fff;
}

.widget_atwidget .list {
    width: 100%;
}

a.hide {font-weight: 600 !important;}


.elementor-widget-container .row {
    width: 100% !important;
}

.elementor-widget-container .widget_atwidget .hide b:after {
    left: 90% !important;
}

.elementor-widget-container .widget_atwidget .show b:after {
    left: 90% !important;
}

.widget_atwidget_link .list li > a {
    color: #fff !important;
}

@media print {.hide, .show {display:none;}}

</style>
<script>
    function childClick(id) {
        console.log(id);
        var showChildEle = document.getElementById("show"+id);
        var listChildEle = document.getElementById("list"+id);
        var hideChildEle = document.getElementById("hide"+id);
        if(showChildEle.classList.contains("showChild")) {
            showChildEle.classList.remove('showChild');
            hideChildEle.classList.remove("hideChild");
            listChildEle.classList.remove("listChild");
        } else {
            hideChildEle.classList.add('hideChild');
            showChildEle.classList.add("showChild");
            listChildEle.classList.add("listChild");
        }
    }
</script>
 
<?php
$atcontatore = 0;
foreach (amministrazionetrasparente_getarray() as $inner) {

    echo '<div class="row widget_atwidget_link">
    <a href="#hide'.++$atcontatore.'" class="hide" id="hide'.$atcontatore.'"><b>'.$inner[0].'</b></a>
    <a href="#show'.$atcontatore.'" class="show" id="show'.$atcontatore.'"><b>'.$inner[0].'</b> </a>
    <div class="list">';
    $atreturn = '';
    $urlMatch = 0;
    $reqUrl = $_SERVER['REQUEST_URI'];
    foreach ($inner[1] as $value) {
        $term = get_term_by('name', $value, 'tipologie');
        $childs = at_get_taxonomy_childs($term->term_id);
        if(!empty($childs)){
            $atchild = '<ul>';
            foreach ($childs as $child) {
                $termc = get_term_by('id', $child->term_id, 'tipologie');
                $atchild .= '<li><a href="' . get_term_link( $termc, 'tipologie' ) . '#hide'.$atcontatore.'" title="' . $termc->name . '">' . $termc->name . '</a></li>';
                $link = get_term_link( $termc, 'tipologie' );
                if(strpos($link, $reqUrl)) {
                    $urlMatch = $atcontatore+1;
                }
            }
            $atchild .= '</ul>';
            $atchildrow = '<div class="row" style="margin-left: 1px;width: 100%;">';
            $atchildrow .= '<a href="javascript:void(0);" onClick="childClick('.++$atcontatore.')" class="hide" id="hide'.$atcontatore.'"><b>'.$term->name.'</b></a>';
            $atchildrow .= '<a style="border-bottom: none;" href="javascript:void(0);" onClick="childClick('.$atcontatore.')" class="show" id="show'.$atcontatore.'"><b>'.$term->name.'</b> </a>';
            $atchildrow .=  '<div class="list" style="padding-left: 10px;" id="list'.$atcontatore.'">';
            $atchildrow .= $atchild;
            $atchildrow .= '</div></div>';
            $atreturn .= $atchildrow;
        }else {
            $t_link = get_term_link( $term, 'tipologie' );
            error_log(">>>>>>>>>>>>>>>>>>>> " . print_r($t_link,1 ));
            if(is_string($t_link)){
                $atreturn .= '<li><a href="' . get_term_link( $term, 'tipologie' ) . '#hide'.$atcontatore.'" title="' . $value . '">' . $value . '</a></li>';
            }
        }
        
        
    }
    echo '<ul>'.$atreturn.'</ul>';
    echo '</div></div>';
}
?>

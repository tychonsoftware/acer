<?php get_header(); ?>

    <?php get_sidebar(); ?>

<div id="centrecontent" class="column">

<!-- breadcrumbs -->
<div id="path"><?php if(function_exists('bcn_display')) { bcn_display(); } ?></div>
<!-- fine breadcrumbs -->

	<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
	<h2 class="page-title"><?php echo $term->name; ?></h2>

	<?php
  error_log("::::::::::::::::::::::::::::: " . print_r($term,1));
  echo '
  <style type="text/css">
  .at-tableclass {padding:0px 0px 0px 5px;position:relative;min-width: 200px;}
  ';

  echo '
  .at-tableclass h3 a { text-decoration:none; cursor: default; }
  .at-tableclass h3 {  border: 1px solid #eee; padding: 8px 10px; background: #FBFBFB; }
  
  .at-number { float: right;
    border-radius: 20px;
    background-color: white;
    height: 20px;
    width: 20px;
    border: 1px solid #eee;
    text-align: center;
    font-size: 0.8em;
    font-weight: bold; }

    ul, ul li {
      padding-left: 0;
      list-style: none;
      list-style-position: outside;
  }
  a {
    color: #000000;
    text-underline-offset: 3px;
    text-decoration: underline;
}
  </style>';
    $childs = at_get_taxonomy_childs($term->term_id);
    if(!empty($childs)){
      $atreturn = '<ul>';
      foreach ($childs as $child) {
        $termc = get_term_by('id', $child->term_id, 'tipologie');
        $atreturn .= '<li>';
        $atreturn .= '<a href="' . get_term_link( $termc, 'tipologie' ) . '" title="' . $termc->name . '">' . $termc->name . '</a>';
        $atreturn .= '</li>';
      }
      $atreturn .= '</ul>';
      ?>
      <div class="at-tableclass" id="at-s-s-1" style="width: 70%">
      <?php echo $atreturn; ?>
      </div>
    <?php
    } // if


	if (is_tax( 'tipologie' )) {
		if (function_exists('at_archive_buttons')) {
			at_archive_buttons();
		}
	}

?>
	<?php if (have_posts()) :

    $id_exist = 0;

    $related_post_ids = at_get_posts_term($term->term_taxonomy_id);
    $post_ids = [];
    foreach ($related_post_ids as $related_post_id) {
      array_push($post_ids, $related_post_id->object_id);
    }
    ?>
        <?php while (have_posts()) : the_post();
        if (!in_array(get_the_ID(), $post_ids)){
          continue;
        }
        //if($id_exist == 0)
         //

        ?>
        <div class="at-list"><div class="at-no-img"></div>
	      <div class="at-info">
          <!-- <div class="post type-post hentry"> -->
            <h3 class="entry-title" style="line-height: 0.8 !important;">
            <strong><a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">
                <?php the_title(); ?>
              </a>
              </strong>
            </h3>
            <?php
              $expiration_date = get_post_meta( get_the_ID(), '_atexpirationdate', true );
              $date_now = null;
              if(!empty($expiration_date)){
                $date = strtotime($expiration_date);
                $formatteddate = date('d/m/Y',$date);
              }
            ?>
            <span class="atdateOne"><?php echo get_the_date('d/m/Y'); ?></span>
            <span class="atdateOne" style="margin-left:25%"><?php echo $formatteddate; ?></span>
            <div class="entry-summary multi-line-truncate">
              <?php
                $excerpt = get_the_excerpt();
                $data = explode(" Allegati ", $excerpt);
                echo $data[0];
              ?>
            </div><!-- .entry-summary -->
          <!-- </div> -->
          </div>
        </div>
        <?php endwhile; ?>

      <?php endif; ?>

<div class="nav">
<div class="alignleft"><?php next_posts_link('&laquo; Comunicazioni precedenti') ?></div>
<div class="alignright"><?php previous_posts_link('Comunicazioni successive &raquo;') ?></div>
</div>

<div style="clear:both"></div>

</div>
<?php include(TEMPLATEPATH . '/rightsidebar.php'); ?>
<?php get_footer(); ?>

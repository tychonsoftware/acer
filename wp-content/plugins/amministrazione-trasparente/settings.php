<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

  include(plugin_dir_path(__FILE__) . 'settings-banner.php');
  echo '<form method="post" action="options.php">';
  settings_fields( 'wpgov_at_options');
  $options=get_option( 'wpgov_at');
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" rel="stylesheet" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>

<style>
</style>

<table class="form-table">
  <tr valign="top">
          <th scope="row">
            <label for="at_option_id">ID Pagina</label>
          </th>
          <td>
            <input id="at_option_id" type="number" min="0" name="wpgov_at[page_id]" value="<?php echo $options['page_id']; ?>" size="55" />
            <br>
            <small>ID della pagina di WordPress in cui è stato inserito lo shortcode del plugin).<br>
            Lista shortcode: <a href="https://wpgov.it/docs/amministrazione-trasparente/">link</a></small>
          </td>
        </tr>

        <tr valign="top">
  <th scope="row">
    <label for="at_option_opacity">Sfuma sezioni vuote</label>
  </th>
  <td>
    <input id="at_option_opacity" name="wpgov_at[opacity]" type="checkbox" value="1"
      <?php checked( '1', isset($options['opacity']) && $options['opacity'] ); ?> />
    <br>
    <small>Aumenta la trasparenza visiva delle sezioni senza alcun contenuto<br>Consigliato: <b>ON</b></small>
  </td>
</tr>
<tr valign="top">
<th scope="row">
<label for="at_enable_tag">Abilita tag</label>
</th>
<td>
<input id="at_enable_tag" name="wpgov_at[enable_tag]" type="checkbox" value="1"
   <?php checked( '1', isset( $options['enable_tag'] ) && $options['enable_tag'] ); ?> />
<br>
<small>Consenti di associare dei tag ai post di Amministrazione Trasparente<br>Consigliato: <b>OFF</b></small>
</td>
</tr>
<tr valign="top">
<th scope="row">
<label for="at_show_love">Mostra credits</label>
</th>
<td>
<input id="at_show_love" name="wpgov_at[show_love]" type="checkbox" value="1"
  <?php checked( '1', isset( $options['show_love'] ) && $options['show_love'] ); ?> />
<br>
<small>Aiutaci a far conoscere il progetto e ottieni una via preferenziale per il supporto<br>Consigliato: <b>ON</b></small>
</td>
</tr>
<tr valign="top">
<th scope="row" colspan="2">
  <h2>Avanzate</h2>
  <tr valign="top">
  <th scope="row">
  <label for="at_enable_ucc">Abilita uffici e Centri di costo</label>
  </th>
  <td>
  <input id="at_enable_ucc" name="wpgov_at[enable_ucc]" type="checkbox" value="1"
     <?php checked( '1', isset( $options['enable_ucc'] ) && $options['enable_ucc'] ); ?> />
  <br>
  <small>Consenti di associare i contenuti a una nuova tassonomia basata su divsione aggiuntiva per uffici e centri di costo</small>
  </td>
  </tr>
  <tr valign="top">
  <th scope="row">
  <label for="at_map_cap">Mappa capacità</label>
  </th>
  <td>
  <input id="at_map_cap" name="wpgov_at[map_cap]" type="checkbox" value="1"
     <?php checked( '1', isset( $options['map_cap'] ) && $options['map_cap'] ); ?> />
  <br>
  <small>Mappa le meta capacità alle capacità primitive per personalizzare ruoli e permessi<br>[per utenti esperti] + [richiede componenti aggiuntivi]<br>Consigliato: <b>OFF</b></small>
  </td>
  </tr>
</th>
</tr>
<tr valign="top">
<th scope="row">
<label for="at_pasw_2013">Forza PASW2013</label>
</th>
<td>
<input id="at_pasw_2013" name="wpgov_at[pasw_2013]" type="checkbox" value="1"
   <?php checked( '1', isset( $options['pasw_2013'] ) && $options['pasw_2013'] ); ?> />
<br>
<small>Spunta casella se vuoi attivare ottimizzazioni per il template PASW2013<br>[abilitare solo se il tema attivo è una versione precedente al 2013 o se è stato cambiato il nome della cartella di "pasw2013"]</small>
</td>
</tr>
<?php if ( is_admin() ) { 
  $wpusers = get_users(array());
  $terms = at_get_tipologies('tipologie');
?>
 
<tr valign="top">
<th scope="row">
<label for="user_id">User Sections</label>
</th>
<td>
    <select name="user_id" id="user_id"  style="margin-right: 10px;">
      <?php echo '<option value="-1">Selezione Utente</option>'; ?>
			<?php   foreach( $wpusers as $user ) {
				echo '<option value="'.$user->data->ID.'">'.$user->data->display_name.'</option>';
			}  ?>
		</select>
    <select name="term_ids" id="term_ids" multiple="multiple">
			<?php   foreach( $terms as $term ) {
				echo '<option value="'.$term->term_id.'">'.$term->name .'</option>';
			}  ?>
		</select>
</td>
<td>
</td>
</tr>
<?php } ?>
</th>
</tr>
  <tr valign="top" <?php if ( !isset( $options['debug'] ) || $options['debug'] ) { echo 'style="display:none;"'; } ?>>
    <th scope="row">
      <label for="debug">Modalità DEBUG</label>
    </th>
    <td>
      <input id="debug" name="wpgov_at[debug]" type="checkbox" value="1"
        <?php checked( '1', isset( $options['debug'] ) && $options['debug'] ); ?> />
      <br>
      <small>Utile in caso di test da parte del servizio di supporto. <b>Mantienila disattivata!</b></small>
    </td>
  </tr>
  <tr valign="top" <?php if ( !isset( $options['custom_terms'] ) || !$options['custom_terms'] ) { echo 'style="display:none;"'; } ?>>
    <th scope="row">
      <label for="custom_terms">Custom Terms</label>
    </th>
    <td>
      <input id="custom_terms" name="wpgov_at[custom_terms]" type="checkbox" value="1"
        <?php checked( '1', isset( $options['custom_terms'] ) && $options['custom_terms'] ); ?> />
      <br>
      <small>Utile in caso di test da parte del servizio di supporto. <b>Mantienila disattivata!</b></small>
    </td>
  </tr>
</tr>
  </table>

<?php
  echo '<p class="submit"><input type="submit" class="button-primary" value="'.__('Save Changes').'" /></p>';
  echo '</form>';
  if ( at_option('debug') ) {
    echo '<hr><h3>DEBUG</h3>';
    $terms = get_terms( array( 'taxonomy' => 'tipologie', 'hide_empty' => false ) );
    echo 'Numero sezioni installate: '.count($terms).'<br>';
    $count = 0;
    $merge = array();
    foreach ( amministrazionetrasparente_getarray() as $inner ) {
      $count+= count($inner[1]);
      $merge = array_merge( $merge, $inner[1] );
    }
    sort($merge);
    echo 'Numero sezioni supportate dal plugin: '.$count;
    echo '<hr>';
    echo '<div style="width:45%;float:left;"><h4>Installate:</h4>';
    echo '<ul>';
    foreach ( $terms as $term ) {
      echo '<li>' . $term->name . '</li>';
    }
    echo '</ul>';
    echo '</div>';
    echo '<div style="width:45%;float:left;"><h4>Supportate:</h4>';
    echo '<ul>';
    foreach ( $merge as $merge_item ) {
      echo '<li>' . $merge_item . '</li>';
    }
    echo '</ul>';
    echo '</div>';
    echo '<hr>';
  }
  
?>
<script>
 jQuery(document).ready(function ($) {
    $("#term_ids").multiselect({		
			nonSelectedText: "Select Tipologie",
      numberDisplayed: 0,
      includeSelectAllOption: true,
      selectAllText:'Select all / Deselect all'
		});
    $("#user_id").on('change', function() {
	  var user_id =  $(this).children('option:selected').val();
    jQuery.ajax({
				method: "POST",
				url: ajaxurl,
				data: {
					'action': 'at_get_user_sections',
					'at_user_id': user_id
				},
				success: function (output) {
					var data = JSON.parse(output);
					if(data.success) {
            var dataarray=data.terms.split(",");
            $("#term_ids").val(dataarray);
            $("#term_ids").multiselect("refresh");
					}else {
            $("#term_ids").val('');
            $("#term_ids").multiselect("refresh");
          }
				}
			});
	}); 
  });

  jQuery("form").submit(function(){
    var userId = jQuery("#user_id").val();
    var termIds = jQuery("#term_ids").val();
		jQuery.ajax({
		  method: "POST",
				url: ajaxurl,
				data: {
					'action': 'at_save_user_section_mapping',
          'user_id': userId,
          'term_ids': termIds
				},
				success: function (output) {

				}
		});
  });

  
  

</script>
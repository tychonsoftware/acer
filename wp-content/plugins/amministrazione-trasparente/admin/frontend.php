<?php
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

// if(isset($_REQUEST['action'])){

// }else{
//     $ret=Lista_AT($atts);
// }

//function Lista_AT($atts,$typology=''){
    $Contenuto='';
    if (isset($atts['per_page'])){
		$N_A_pp=$atts['per_page'];	
	}else{
		$N_A_pp=10;
	}
    if (!isset($_REQUEST['ATPag'])){
		$Da=0;
		$A=$N_A_pp;
	}else{
		$Da=($_REQUEST['ATPag']-1)*$N_A_pp;
		$A=$N_A_pp;
	}
    
    if (isset($atts['typology']) and $atts['typology'] != ''){


        $typologie=explode("~",$atts['typology']);
        $term_id_list = "";
        foreach($typologie as $tipo){
            if(!empty($term_id_list)){
                $term_id_list = $term_id_list . ", ";
            }
            $term = get_term_by('name',$tipo,'tipologie');
            $term_id_list = $term_id_list . $term->term_taxonomy_id;
		}

        
        if(!empty($term_id_list)){
            $TotAt = ap_get_postids_from_term($term_id_list,true,0,0);
            $results = ap_get_postids_from_term($term_id_list,false,$Da,$A);

            
            $width = 'width:31.5% !important;';
            $infomargin = 'margin-left:25%';
            if (isset($atts['box_per_row'])){
                $boxPerRow = $atts['box_per_row'];
                if($boxPerRow == 2){
                    $width = 'width:48% !important;';
                    $infomargin = 'margin-left:30%';
                }
                // else if($boxPerRow == 1){
                //     $width = 'width:40% !important;';
                    
                // }
            }

            if(!empty($results)){
                $Contenuto.= '<div class="atVisalbo">	<div class="attabalbo">';
                foreach($results as $index=>$item){
                    $atexpirationdate = get_post_meta($item->ID, '_atexpirationdate', true);
                    
                    if(!empty($atexpirationdate)){
                        $date = strtotime($atexpirationdate);
                        $formatteddate = date('d/m/Y',$date);
                        $date_now = date("d/m/Y");
                      //  if ($formatteddate >= $date_now) {
                            $description = '';
                            $post_content = $item->post_content;
                            if(!empty($post_content)){
                                $dom = new DomDocument();
                                $dom->loadHTML($post_content);
                                $p = $dom->getElementsByTagName('p');
                                if(!empty($p)){
                                    $description = $p[0]->textContent;
                                }
                            }

                            $Contenuto.= '<div class="at-list" style="'. $width . '"><div class="at-no-img"></div>';
                            $Contenuto.= '<div class="at-info"><strong>';
                            $Contenuto.= '<a href="' . $item->guid . '">' . $item->name . '</a>';
                            $Contenuto.='</strong><br/>';
                            $Contenuto.='<span class="atdateOne">' . ap_VisualizzaData($item->post_date);
                            $Contenuto.='</span><span class="atdateOne" style="' . $infomargin . '">'. ap_VisualizzaData($formatteddate);
    
                            $Contenuto.='</span><p style="font-size: 13px !important;">'.$item->post_title.'</p>';
                            $Contenuto.='<p style="font-size: 13px !important;" class="multi-line-truncate">'.$description.'</p>';
                            $Contenuto.='</div></div>';

                            if (isset($atts['box_per_row'])){
                                $boxPerRow = $atts['box_per_row'];
                                if($boxPerRow == 1){
                                    $Contenuto.= '<div class="at-list" style="border: 0px"></div>';
                                    $Contenuto.= '<div class="at-list" style="border: 0px"></div>';
                                }
                            }
                          
                            
                        //}
                    }
                }
                $Contenuto.= '<div class="clearfix"></div>';
                if ($TotAt>$N_A_pp){
                    $Para='';
                    foreach ($_REQUEST as $k => $v){
                        if ($k!="ATPag" and $k!="vf")
                            if ($Para=='')
                                $Para.=$k.'='.$v;
                            else
                                $Para.='&amp;'.$k.'='.$v;
                    }
                    if ($Para=='')
                        $Para="?ATPag=";
                    else
                        $Para="?".$Para."&amp;ATPag=";
                    $Npag=(int)($TotAt/$N_A_pp);
                    if ($TotAt%$N_A_pp>0){
                        $Npag++;
                    }

                    $Contenuto.= ' 
                    <div class="tablenav" style="float:right;" id="risultati">
                    <div class="tablenav-pages">
                    <p>Pagine';
                    if (isset($_REQUEST['ATPag']) And $_REQUEST['ATPag']>1 ){
                        $Pagcur=$_REQUEST['ATPag'];
                        $PagPre=$Pagcur-1;
                            $Contenuto.= '&nbsp;<a href="'.$Para.'1" class="page-numbers numero-pagina" title="Vai alla prima pagina">&laquo;</a>
                &nbsp;<a href="'.$Para.$PagPre.'" class="page-numbers numero-pagina" title="Vai alla pagina precedente">&lsaquo;</a> ';
                    }else{
                        $Pagcur=1;
                        $Contenuto.= '&nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&laquo;</span>
                &nbsp;<span class="page-numbers current" title="Sei gi&agrave; nella prima pagina">&lsaquo;</span> ';
                    }
                    $Contenuto.= '&nbsp;<span class="page-numbers current">'.$Pagcur.'/'.$Npag.'</span>';
                    $PagSuc=$Pagcur+1;
                    if ($PagSuc<=$Npag){
                        $Contenuto.= '&nbsp;<a href="'.$Para.$PagSuc.'" class="page-numbers numero-pagina" title="Vai alla pagina successiva">&rsaquo;</a>
                &nbsp;<a href="'.$Para.$Npag.'" class="page-numbers numero-pagina" title="Vai all\'ultima pagina">&raquo;</a>';
                    }else{
                        $Contenuto.= '&nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>
                &nbsp;<span class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>';			
                    }
                $Contenuto.='			</p>
                    </div>
                </div>';
                }
                $Contenuto.='</div></div>';
            }
        }
    }
    $ret = $Contenuto;
    //return $Contenuto;
//}
?>
jQuery(document).ready(function($){

    var ele = jQuery('#avcp_is_ditta_estera');
    if(ele){
        jQuery('input[name="term_meta[avcp_codice_fiscale]"]').css("border-color", "");
        jQuery('input[name="term_meta[avcp_codice_fiscale]"]').css("border", "");
        var submit = ele.closest('form').find(':submit');
        submit.click(function(e) {
            var cf = jQuery('input[name="term_meta[avcp_codice_fiscale]"]').val()
            if(!cf || cf.trim().length == 0){
                e.preventDefault()
                e.stopPropagation();
                e.stopImmediatePropagation();
                jQuery('input[name="term_meta[avcp_codice_fiscale]"]').css("border-color", "red");
                jQuery('input[name="term_meta[avcp_codice_fiscale]"]').css("border", "1px solid red");
                return false;
            }
        });
    }

    jQuery(".import_avcp_excel").click(function(event) {
        var valid = true;
        var dataSet = jQuery('#data-sets').val()
        var modality = jQuery('input[name="modality"]:checked').val();
        if(modality && modality.value === 'Storico'){
            if(!dataSet || dataSet.trim().length == 0){
                valid = false;
            }
        }else {
            jQuery('#data-sets').removeAttr('required');
        }
        if(document.getElementById("avcp-excel").files.length == 0){
            valid = false;
        }
        if(valid) {
            console.log('Start importing');
            $("#warning-msg").show();
        }
    });
    $(".failed_import").click(function(event) {
        console.log("event triggered", event.target.getAttribute("data-id"));
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data:{
                action:'downloadReport',
                importId:event.target.getAttribute("data-id")
            },
            beforeSend: function() {
                $("#loading-overlay").show();
            },
            success: function(response){
                $("#loading-overlay").hide();

                var jsonData = JSON.parse(response);
                console.log('response ', jsonData);
                if(jsonData.length > 0){
                    var data = jsonData[0];
                    if(data.success && data.file_data && data.file_data != ''){
                        console.log('file data  ', data.file_data);
                        downloadText(data.file_data, data.file_name);
                    }
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#loading-overlay").hide();
                alert(xhr.status+ " "+thrownError);
            }
        });
    });

    function downloadText(data, strFileName, strMimeType) {
        console.log('downloadText' , strFileName);
        data = data.replace(/(\t\r\n|\n|\r|\t)/gm,"");
        var self = window, // this script is only for browsers anyway...
            u = "application/octet-stream", // this default mime also triggers iframe downloads
            m = strMimeType || u,
            x = data,
            D = document,
            a = D.createElement("a"),
            z = function(a){return String(a);},


            B = self.Blob || self.MozBlob || self.WebKitBlob || z,
            BB = self.MSBlobBuilder || self.WebKitBlobBuilder || self.BlobBuilder,
            fn = strFileName || "download",
            blob,
            b,
            ua,
            fr;

        //if(typeof B.bind === 'function' ){ B=B.bind(self); }

        if(String(this)==="true"){ //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            x=[x, m];
            m=x[0];
            x=x[1];
        }



        //go ahead and download dataURLs right away
        if(String(x).match(/^data\:[\w+\-]+\/[\w+\-]+[,;]/)){
            return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
                navigator.msSaveBlob(d2b(x), fn) :
                saver(x) ; // everyone else can save dataURLs un-processed
        }//end if dataURL passed?

        try{

            blob = x instanceof B ?
                x :
                new B([x], {type: m}) ;
        }catch(y){
            if(BB){
                b = new BB();
                b.append([x]);
                blob = b.getBlob(m); // the blob
            }

        }



        function d2b(u) {
            var p= u.split(/[:;,]/),
                t= p[1],
                dec= p[2] == "base64" ? atob : decodeURIComponent,
                bin= dec(p.pop()),
                mx= bin.length,
                i= 0,
                uia= new Uint8Array(mx);

            for(i;i<mx;++i) uia[i]= bin.charCodeAt(i);

            return new B([uia], {type: t});
        }

        function saver(url, winMode){


            if ('download' in a) { //html5 A[download]
                a.href = url;
                a.setAttribute("download", fn);
                a.innerHTML = "downloading...";
                D.body.appendChild(a);
                setTimeout(function() {
                    a.click();
                    D.body.removeChild(a);
                    if(winMode===true){setTimeout(function(){ self.URL.revokeObjectURL(a.href);}, 250 );}
                }, 66);
                return true;
            }

            //do iframe dataURL download (old ch+FF):
            var f = D.createElement("iframe");
            D.body.appendChild(f);
            if(!winMode){ // force a mime that will download:
                url="data:"+url.replace(/^data:([\w\/\-\+]+)/, u);
            }


            f.src = url;
            setTimeout(function(){ D.body.removeChild(f); }, 333);

        }//end saver


        if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
            return navigator.msSaveBlob(blob, fn);
        }

        if(self.URL){ // simple fast and modern way using Blob and URL:
            saver(self.URL.createObjectURL(blob), true);
        }else{
            // handle non-Blob()+non-URL browsers:
            if(typeof blob === "string" || blob.constructor===z ){
                try{
                    return saver( "data:" +  m   + ";base64,"  +  self.btoa(blob)  );
                }catch(y){
                    return saver( "data:" +  m   + "," + encodeURIComponent(blob)  );
                }
            }

            // Blob but not URL:
            fr=new FileReader();
            fr.onload=function(e){
                saver(this.result);
            };
            fr.readAsDataURL(blob);
        }
        return true;
    }

    $("input[name='avcp-excel']").change(function () {
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(xls|xlsx)$");
        if (!(regex.test(val))) {
            $(this).val('');
            alert('Please select correct file format');
            return;
        }else {

        }

    });

});

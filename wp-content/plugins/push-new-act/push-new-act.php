<?php

/*
Plugin Name: Push New Act WS
Description: WordPress Web Service that allow to third application to send to the wp portal new documents and this documents received must be save as new act in the plugin Albo Pretorio
Version: 1.0.0
*/

require_once(dirname(__FILE__) . "/includes/push-new-act-ws-entry.php");
include_once(dirname(__FILE__) . '/includes/pushws-functions.php');

function push_new_act_ws_handle_request($wp) {
	$push_ws_found = false;
	$push_wsdl_requested = false;

	foreach($_SERVER as $val) {
		if(is_string($val) && strlen($val) >= 7 && substr($val, 0, 7) == "/pushws") {
			$push_ws_found = true;
			if(isset($_SERVER["QUERY_STRING"]) && strpos($_SERVER["QUERY_STRING"], "wsdl") !== false) $push_wsdl_requested = true;
			break;
		}
	}

	if($push_ws_found) {
// make sure the QUERY_STRING is correctly set to ?wsdl so the SoapServer instance delivers the wsdl file
		if($push_wsdl_requested) {
			header("Content-type: text/xml");
			$wsdl = file_get_contents(WS_WSDL);
			echo $wsdl;
			exit;
		}else if(!isset($_SERVER["HTTP_SOAPACTION"])) {
			include(WS_INDEX_FILE);
		}else {
			ws_createWSDL();
			header("Content-type: text/xml");
			require_once(WS_SOAP_SERVER_FILE);

			ini_set("soap.wsdl_cache_enabled", "0");
			$server = new SoapServer(WS_WSDL, array("cache_wsdl" => WSDL_CACHE_NONE));
			$server->setClass(WS_SOAP_SERVER_CLASS);
			$server->handle();
			//$soapXml = ob_get_contents();
			//ob_end_clean();
			//echo $soapXml;
		}
		exit;
	}
}

// creates a customized WSDL on plugin activation
register_activation_hook(__FILE__, 'ws_createWSDL');

// checks whether the request should be handled by WPWS
add_action("parse_request", "push_new_act_ws_handle_request");

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest, * or null if none.
 */
function publish_document( $request_data ) {

	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];
	$user = wp_authenticate( $username, $password );
	$response_data = array(
		'resultCode' => "00",
		'resultDescription' => ''
	);
	if ( is_wp_error( $user ) ) {
		$response_data["resultCode"] = "01";
		$response_data["resultDescription"] = 'Autenticazione fallita, verificare che username e password siano corrette';
		$response = new WP_REST_Response( $response_data );
		$response->set_status( 403 );
		return $response;
	}else {

		$parameters = $request_data->get_params();
		$title = $parameters['Title'];
		$typology = $parameters['Typology'];
		$document_data = $parameters['Documents'];
		$exp_date = $parameters['Expiration Date'];

		$typology_id = -1;
		$description = '';
		$username = '';
		$post_author = $user->ID;
		global $wpdb, $table_prefix;
		$wpdb->terms = $table_prefix . "terms";

		if(isset($parameters['Description'])){
			$description = $parameters['Description'];
		}

		if(isset($parameters['User Author'])){
			$username = $parameters['User Author'];
			$author = get_user_by('login', $username);
			if($author){
				$post_author = $author->id;
			}else {
				$response_data["resultCode"] = "04";
				$response_data["resultDescription"] = 'Utente autore della pubblicazione non trovato sul portale';
				$response = new WP_REST_Response( $response_data);
				$response->set_status( 412 );
				return $response;
			}
		}

		if(isset($parameters['Typology_id'])){
			$typology_id = $parameters['Typology_id'];
			if($typology_id > 0){
				$Sql="SELECT name FROM $wpdb->terms WHERE term_id=%d;";
				$results = $wpdb->get_results($wpdb->prepare($Sql,$typology_id));
				if(!empty($results)){
					$typology = $results[0]->name;
				}
			}
		}
		if(empty($title)){
			$response_data["resultCode"] = "02";
			$response_data["resultDescription"] = 'Parametri di input obbligatori non valorizzati o mancanti';
			$response = new WP_REST_Response( $response_data);
			$response->set_status( 412 );
			return $response;
		}

		if(isset($exp_date) && !empty($exp_date) && !pushws_validate_date($exp_date, 'd/m/Y')){
			$response_data["resultCode"] = "05";
			$response_data["resultDescription"] = ' Data di scadenza deve essere in formato GG/MM/AAAA';
			$response = new WP_REST_Response( $response_data);
			$response->set_status( 412 );
			return $response;
		}
		foreach($document_data as  $val) {
			if (empty($val['Document Name']) || empty($val['Document'])){
				$response_data["resultCode"] = "02";
				$response_data["resultDescription"] = 'Parametri di input obbligatori non valorizzati o mancanti';
				$response = new WP_REST_Response( $response_data);
				$response->set_status( 412 );
				return $response;
			}
		}
		$Sql="SELECT term_id FROM $wpdb->terms WHERE name=%s;";
		$results = $wpdb->get_results($wpdb->prepare($Sql,$typology));
		if(empty($results)){
			$response_data["resultCode"] = "03";
			$response_data["resultDescription"] = 'Tipologia non trovata';
			$response = new WP_REST_Response( $response_data);
			$response->set_status( 412 );
			return $response;
		}

		$post = array(
			'post_author' => $post_author,
			'post_title' => $title,
			'post_status' => 'publish',
			'comment_status' => 'closed',
			'ping_status' => 'closed',
			'post_name' => strtolower($title),
			'post_parent' => 0,
			'post_type' => 'amm-trasparente'
		);
		$post_id = wp_insert_post( $post );
		$upload_dir = wp_upload_dir();
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;
		$post_content = '';
		$index = 0;
		$post_content .= '<p>' . $description . '</p>';

		if(isset($exp_date) && !empty($exp_date)){
			$date_obj = DateTime::createFromFormat('!d/m/Y', $exp_date);
			add_post_meta($post_id, '_atexpirationdate', $date_obj->format('Y-m-d'));
		}
		foreach($document_data as  $val) {
			$document_name = $val['Document Name'];
			$document_data = $val['Document'];
			$hashed_filename = $document_name;
			if(file_exists($upload_path . $document_name)){
				$hashed_filename = md5( $document_name . microtime() ) . '_' . $document_name;
			}
			file_put_contents( $upload_path . $hashed_filename, base64_decode($document_data));
			$fpath = $upload_path . $hashed_filename;
			$fpath = str_replace('\\', '/', $fpath);
			$post_attachment = array(
				'post_author' => $post_author,
				'post_title' => $document_name,
				'post_status' => 'inherit',
				'comment_status' => 'open',
				'ping_status' => 'closed',
				'post_name' => strtolower($document_name),
				'guid' => $upload_dir['url'] . '/' . $hashed_filename,
				'post_parent' => $post_id,
				'post_type' => 'attachment',
				'post_mime_type' => mime_content_type($fpath)

			);
			if ( ! function_exists( 'wp_generate_attachment_metadata' ) ) {
				include( ABSPATH . 'wp-admin/includes/image.php' );
			}


			$attach_id = wp_insert_attachment($post_attachment,  $hashed_filename);

			$attach_data = wp_generate_attachment_metadata( $attach_id, $fpath);
			wp_update_attachment_metadata($attach_id, $attach_data);

			wp_update_attachment_metadata($attach_id, $attach_data);
			// if($index != 0){
			// 	$post_content .= '<br/>';
			// }
			// $post_content .= "<!-- wp:file ";
			// $post_content .= '{"id":' . $attach_id;
			// $post_content .= ',"href":"' . $post_attachment['guid'] . '"} -->';
			// $index++;
		}
		$post['ID'] = $post_id;
		$post['post_content'] = $post_content;
		$post['guid'] = get_site_url(). '/?post_type=amm-trasparente&#038;p=' . $post_id;
		wp_update_post($post, true);
		$wpdb->term_relationships = $table_prefix . "term_relationships";
		$wpdb->insert(
			$wpdb->term_relationships,
			array(
				'object_id' => $post_id,
				'term_taxonomy_id' => $results[0]->term_id,
				'term_order' => '0'
			)
		);
		$Sql="SELECT term_id, `count` FROM $wpdb->term_taxonomy WHERE term_id=%s;";
		$term_taxonomy = $wpdb->get_results($wpdb->prepare($Sql,$results[0]->term_id));
		if(!empty($term_taxonomy)){
			$count = $term_taxonomy[0]->count + 1;
			$wpdb->update(
				$wpdb->term_taxonomy, array('term_id'=>$term_taxonomy[0]->term_id,
				'count'=>$count), array('term_id'=>$term_taxonomy[0]->term_id));
		}
	}

	$response_data["resultCode"] = "00";
	$response_data["resultDescription"] = 'Documento pubblicato con successo';
	$response = new WP_REST_Response( $response_data);
	$response->set_status( 200 );
	return $response;
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'wp/v2', '/publish_document', array(
		'methods' => 'POST',
		'callback' => 'publish_document',
	) );

	register_rest_route( 'wp/v2', '/check_user', array(
		'methods' => 'POST',
		'callback' => 'check_user',
	) );

	register_rest_route( 'wp/v2', '/pubblica_gara', array(
		'methods' => 'POST',
		'callback' => 'public_game',
	) );
} );

function getTypologies( $request_data ) {

	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];
	$user = wp_authenticate( $username, $password );
	$response_data = array(
		'resultCode' => "00",
		'resultDescription' => ''
	);
	if ( is_wp_error( $user ) ) {
		$response_data["resultCode"] = "01";
		$response_data["resultDescription"] = 'Autenticazione fallita, verificare che username e password siano corrette';
		$response = new WP_REST_Response( $response_data );
		$response->set_status( 403 );
		return $response;
	}else {

		$terms = get_terms([
			'taxonomy' => 'tipologie',
			'hide_empty' => false,
		]);
		$typologies = [];
		foreach($terms as $term) {
			$typology = new stdClass();
			$typology->typology_id = $term->term_id;
			$typology->typology = $term->name;
			array_push($typologies, $typology);
		}
	}

	$response_data["resultCode"] = "00";
	$response_data["resultDescription"] = 'Servizio eseguito con successo';
	$response_data["typologies"] = $typologies;
	$response = new WP_REST_Response( $response_data);
	$response->set_status( 200 );
	return $response;
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'wp/v2', '/getTypologies', array(
		'methods' => 'GET',
		'callback' => 'getTypologies',
	) );
} );


function check_user( $request_data ) {
	$parameters = $request_data->get_params();

	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];
	$user = wp_authenticate( $username, $password );
	$response_data = array(
		'resultCode' => "00",
		'resultDescription' => ''
	);
	if ( is_wp_error( $user ) ) {
		$response_data["resultCode"] = "01";
		$response_data["resultDescription"] = 'Autenticazione fallita, verificare che username e password siano corrette';
		$response = new WP_REST_Response($response_data);
		$response->set_status(403);
		return $response;
	}

	if(!isset($parameters['token'])){
		$response_data["resultCode"] = "02";
		$response_data["resultDescription"] = 'Parametro obbligatorio token non presente o non valorizzato';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 412 );
		return $response;
	}else {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix . 'login_tokens';
		$auth_token = $parameters['token'];

		$sql = "SELECT * FROM $table_name WHERE auth_token='$auth_token'";
		$results = $wpdb->get_results($sql);
		if(count($results) > 0){
			$date_now = new DateTime();
			if($date_now->date > $results[0]->auth_token_expiration){
				$response_data["resultCode"] = "03";
				$response_data["resultDescription"] = 'Token non valido oppure scaduto';
				$response = new WP_REST_Response( $response_data);
				$response->set_status( 403 );
				return $response;
			}
			$user = get_user_by('id', $results[0]->user_id);
			$response_data["resultCode"] = "00";
			$response_data["resultDescription"] = 'Token valido';
			$userData =  new stdClass;
			$userData->nome = get_user_meta( $user->ID, 'first_name' , true );
			$userData->cognome = get_user_meta( $user->ID, 'last_name' , true );
			$userData->codice_fiscale = get_user_meta( $user->ID, 'codice_fiscale' , true );
			$userData->email = $user->user_email;
			$response_data["user"] = $userData;

		}else {
			$response_data["resultCode"] = "03";
			$response_data["resultDescription"] = 'Token non valido oppure scaduto';
			$response = new WP_REST_Response( $response_data);
			$response->set_status( 403 );
			return $response;
		}

	}

	$response = new WP_REST_Response( $response_data);
	$response->set_status( 200 );
	return $response;

}

function public_game( $request_data ) {
	$parameters = $request_data->get_params();



	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];
	$user = wp_authenticate( $username, $password );
	$response_data = array(
		'resultCode' => "00",
		'resultDescription' => ''
	);
	if ( is_wp_error( $user ) ) {
		$response_data["resultCode"] = "01";
		$response_data["resultDescription"] = 'Autenticazione fallita, verificare che username e password siano corrette';
		$response = new WP_REST_Response($response_data);
		$response->set_status(403);
		return $response;
	}
	$unicodeChar = '�';
	if(emptyStr($parameters['Oggetto']) || emptyStr($parameters['Descrizione']) || emptyStr($parameters['Cig']) ||
		emptyStr($parameters['SceltaContraente']) || emptyStr($parameters['ImportoAggiudicazione'])
		|| emptyStr($parameters['DataInizio']) || emptyStr($parameters['DataFine']) || emptyStr($parameters['NumeroProvvedimento'])
		|| emptyStr($parameters['DataProvvedimento']) || emptyStr($parameters['Uffici']) || count($parameters['Uffici']) == 0
		|| emptyStr($parameters['Partecipanti']) || count($parameters['Partecipanti']) == 0){
		$response_data["resultCode"] = "02";
		$response_data["resultDescription"] = 'Uno o pi' . utf8_encode($unicodeChar) . ' campi obbligatori mancanti o non valorizzati';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 412 );
		return $response;
	}
	$invalidDate = false;

	if (!validateDate($parameters['DataInizio']) || !validateDate($parameters['DataFine'])
		|| !validateDate($parameters['DataProvvedimento'])) {
		$invalidDate = true;
	}
	if($invalidDate) {
		$response_data["resultCode"] = "03";
		$response_data["resultDescription"] = 'Per i campi DataInizio, DataFine e DataProvvedimento la data deve essere nel formato GG/MM/AAAA';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 412 );
		return $response;
	}

	$formatInvalid  = false;
	$sommeLiquidate = $parameters['SommeLiquidate'];
	$anno_data = [];
	if($sommeLiquidate && count($sommeLiquidate) > 0) {
		foreach($sommeLiquidate as $val) {
			if(isset($val['Importo']) && !validateDecimal($val['Importo'])){
				$formatInvalid  = true;
				//break;
			}
			if(isset($val['Anno'])){
				$anno_data[$val['Anno']] = $val['Importo'];
				//array_push($anno_data, $val['Anno']);
			}
		}
	}
	if($formatInvalid){
		$response_data["resultCode"] = "04";
		$response_data["resultDescription"] = 'Nel campo  ImportoAggiudicazione e in Importo delle SommeLiquidate gli importi devono essere nel formato di esempio 10200.55';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 200 );
		return $response;
	}
	if(!get_contraente($parameters['SceltaContraente'])){
		$response_data["resultCode"] = "05";
		$response_data["resultDescription"] = 'Scelta Contraente non ammesso';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 200 );
		return $response;
	}
//	if (!in_array(strtoupper($parameters['SceltaContraente']), $tipi_contraente)) {
//
//	}
	$responsabileProvvedimento = get_user_by('login', $parameters['ResponsabileProvvedimento']);
	if(!$responsabileProvvedimento){
		$response_data["resultCode"] = "06";
		$response_data["resultDescription"] = 'Responsabile provvedimento non registrato sul portale';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 412 );
		return $response;
	}
	$taxonomies = get_terms( array(
		'taxonomy' => 'areesettori',
		'hide_empty' => false
	) );

	$taxonomies_name = [];
	if ( !empty($taxonomies) ) :
		foreach( $taxonomies as $category ) {

			array_push($taxonomies_name, strtolower($category->name));
		}
	endif;
	$uffici = array_map('strtolower', $parameters['Uffici']);
	$containsSearch = count(array_intersect($uffici, $taxonomies_name)) === count( $uffici);
	if (!$containsSearch) {
		$response_data["resultCode"] = "07";
		$response_data["resultDescription"] = 'Uno o pi' . utf8_encode($unicodeChar) . ' uffici non ammessi';
		$response = new WP_REST_Response( $response_data);
		$response->set_status( 412 );
		return $response;
	}


	global $wpdb;
	$cig = $parameters['Cig'];
	$post_records = $wpdb->get_results( "select post_id from $wpdb->postmeta 
			where meta_key='avcp_cig' and meta_value = '" . $cig . "' order by post_id DESC", ARRAY_A );

	$post = array(
		'post_author' => $user->ID,
		'post_title' => $parameters['Oggetto'],
		'post_status' => 'publish',
		'post_name' => strtolower($parameters['Oggetto']),
		'post_content' => $parameters['Descrizione'],
		'comment_status' => 'closed',
		'ping_status' => 'closed',
		'post_parent' => 0,
		'post_type' => 'avcp',
	);

	if(count($post_records) > 0){
		$post_id = $post_records[0]['post_id'];
		$post['ID'] = $post_id;
		wp_update_post($post);
		$attachments = get_attached_media( '', $post_id);
		foreach ($attachments as $attachment) {
			wp_delete_attachment( $attachment->ID, 'true' );
		}
	}else {
		$post_id = wp_insert_post( $post );
	}

	update_post_meta(
		$post_id,
		'avcp_cig',
		$parameters['Cig']
	);

	update_post_meta(
		$post_id,
		'avcp_contraente',
		$parameters['SceltaContraente']
	);

	update_post_meta(
		$post_id,
		'avcp_aggiudicazione',
		$parameters['ImportoAggiudicazione']
	);
	$date_inizio = DateTime::createFromFormat('d/m/Y', $parameters['DataInizio']);
	update_post_meta(
		$post_id,
		'avcp_data_inizio',
		$date_inizio->format('Y-m-d')
	);
	$date_fine = DateTime::createFromFormat('d/m/Y', $parameters['DataFine']);
	update_post_meta(
		$post_id,
		'avcp_data_fine',
		$date_fine->format('Y-m-d')
	);

	update_post_meta(
		$post_id,
		'avcp_numero_provvedimento',
		$parameters['NumeroProvvedimento']
	);

	$date_pro = DateTime::createFromFormat('d/m/Y', $parameters['DataProvvedimento']);
	update_post_meta(
		$post_id,
		'avcp_data_provvedimento',
		$date_pro->format('Y-m-d')
	);

	if(isset($parameters['ResponsabileProvvedimento'])){

		$user_last_name = get_user_meta( $responsabileProvvedimento->ID, 'last_name', true );
		$user_first_name = get_user_meta( $responsabileProvvedimento->ID, 'first_name', true );
		$user_name = '';
		if((isset($user_last_name) && trim($user_last_name) !== '')){
			$user_name = $user_last_name;
		}
		if((isset($user_first_name) && trim($user_first_name) !== '')){
			$user_name .= ' ' . $user_first_name;
		}

		if((!isset($user_name) || trim($user_name) === '')){
			$user_name = get_user_meta( $user->ID, 'nickname', true );
		}
		$user_name = trim($user_name);

		update_post_meta(
			$post_id,
			'avcp_responsabile_provvedimento',
			$user_name
		);
	}

	$terms = get_terms( 'annirif', array('hide_empty' => 0) );
	foreach ( $terms as $term ) {
		if (array_key_exists($term->name, $anno_data)) {
			if ( $anno_data[$term->name] > 0 ||
				( $term->name > date("Y", strtotime( $parameters['DataInizio'] ))
					&& $term->name < date("Y", strtotime( $parameters['DataFine'] )) )) {
				wp_set_object_terms( $post_id, $term->name, 'annirif', true );
			}
			update_post_meta(
				$post_id,
				'avcp_s_l_'.$term->name,
				$anno_data[$term->name]
			);
		}

		$anniRiferimento = $parameters['AnniRiferimento'];

		if($anniRiferimento && count($anniRiferimento) > 0) {
			if (in_array($term->name, $anniRiferimento)) {
				error_log('$term->name' . print_r($term->name,1));
				wp_set_object_terms( $post_id, $term->name, 'annirif', true );
			}
		}


	}

	//
	$ufficio_ids = [];
	foreach($parameters['Uffici'] as $ufficio) {
		$get_term = get_term_by('name', $ufficio, 'areesettori');
		if($get_term && isset($get_term->term_id)){
			array_push($ufficio_ids, $get_term->term_id);
		}
	}

	if(count($ufficio_ids) > 0){
		error_log("ufficio_ids " . print_r($ufficio_ids,1));
		wp_set_object_terms( $post_id, $ufficio_ids, 'areesettori', true );
	}

	$terms_ditte = get_terms( 'ditte', array('hide_empty' => 0) );

	$codice_fiscales = [];
	if ($terms_ditte) {
		foreach($terms_ditte as $term) {
			$get_term = get_term_by('name', $term->name, 'ditte');
			$t_id = $get_term->term_id;
			$term_meta = get_option( "taxonomy_$t_id" );
			$codice_fiscale = esc_attr( $term_meta['avcp_codice_fiscale'] );
			if(!emptyStr($codice_fiscale)){
				array_push($codice_fiscales, strtoupper($codice_fiscale));
			}
		}
	}
	$participant_ids = [];
	$parents = [];
	foreach($parameters['Partecipanti'] as $participant) {
		$participant_cf = $participant['CodiceFiscale'];
		$slug = strtolower(str_replace(" ", "-", $participant['RagioneSociale']));
		$term_id = '';

		$term = get_term_by('slug', $slug, 'ditte');
		error_log("term " . print_r($term,1));

		if(isset($term) && isset($term->term_id)){
			$term_id = $term->term_id;

		}else {
			$cid = wp_insert_term(
				$participant['RagioneSociale'],   // the term
				'ditte', // the taxonomy
				array(
					'description' => '.',
					'slug'        => $slug,
					'parent'      => 0,
				)
			);
			error_log("cid " . print_r($cid,1));
			if(count($cid) > 0){
				$term_id = $cid['term_id'];
			}
		}
		error_log("term_id " . print_r($term_id,1));
		array_push($participant_ids, $term_id);
		if(!in_array(strtoupper($participant_cf), $codice_fiscales)) {
			if(isset($term_id) && $term_id > 0){
				array_push($participant_ids, $term_id);
				$cf_array	 =  array(
					'avcp_codice_fiscale' => $participant_cf
				);
				add_option( 'taxonomy_'. $term_id, $cf_array, '', 'no' );
			}

		}
		if(isset($participant['Mandante'])){
			$mandate = $participant['Mandante'];
			error_log("mandate " . print_r($mandate,1));
			if(isset($mandate['CodiceFiscale'])){
				$parent_cf = $mandate['CodiceFiscale'];
				$parent_slug = strtolower(str_replace(" ", "-", $mandate['RagioneSociale']));
				$parent_term_id = '';
				$parent_term = get_term_by('slug', $parent_slug, 'ditte');
				error_log("parent_term " . print_r($parent_term,1));
				if(isset($parent_term) && isset($parent_term->term_id)){
					$parent_term_id = $parent_term->term_id;

				}else {
					$parent_cid = wp_insert_term(
						$mandate['RagioneSociale'],   // the term
						'ditte', // the taxonomy
						array(
							'description' => '.',
							'slug'        => $parent_slug,
							'parent'      => 0,
						)
					);
					error_log("parent_cid " . print_r($parent_cid,1));
					if(count($parent_cid) > 0){
						$parent_term_id = $parent_cid['term_id'];
					}
				}
				error_log("parent_term_id " . print_r($parent_term_id,1));
				$parent = new stdClass();
				$parent->child_id = $term_id;
				$parent->id =$parent_term_id;
				array_push($parents, $parent);
				if(!in_array(strtoupper($parent_cf), $codice_fiscales)) {
					if(isset($parent_term_id) && $parent_term_id > 0){
						$cf_array	 =  array(
							'avcp_codice_fiscale' => $parent_cf
						);
						add_option( 'taxonomy_'. $term_id, $cf_array, '', 'no' );
					}

				}
			}

		}
	}
	$aggiudicatari_ids = [];
	foreach($parameters['Aggiudicatari'] as $aggiudicatari) {
		$aggiudicatari_cf = $aggiudicatari['CodiceFiscale'];
		error_log("aggiudicatari_cf " . print_r($aggiudicatari_cf,1));
		$slug = strtolower(str_replace(" ", "-", $aggiudicatari['RagioneSociale']));
		$term_id = '';
		$term = get_term_by('slug', $slug, 'ditte');
		error_log("aggiudicatari term " . print_r($term,1));
		if(isset($term) && isset($term->term_id)){
			$term_id = $term->term_id;

		}else {
			$cid = wp_insert_term(
				$participant['RagioneSociale'],   // the term
				'ditte', // the taxonomy
				array(
					'description' => '.',
					'slug'        => $slug,
					'parent'      => 0,
				)
			);
			error_log("agg cid " . print_r($cid,1));
			if(count($cid) > 0){
				$term_id = $cid['term_id'];
			}
		}
		error_log("agg term_id " . print_r($term_id,1));
		array_push($aggiudicatari_ids, $term_id);
		if(!in_array(strtoupper($aggiudicatari_cf), $codice_fiscales)) {
			if(isset($term_id) && $term_id > 0){
				array_push($aggiudicatari_ids, $term_id);
				$cf_array	 =  array(
					'avcp_codice_fiscale' => $aggiudicatari_cf
				);
				add_option( 'taxonomy_'. $term_id, $cf_array, '', 'no' );
			}
		}

	}
	if(count($participant_ids) > 0){
		error_log("participant_ids " . print_r($participant_ids,1));
		wp_set_object_terms( $post_id, $participant_ids, 'ditte', true );
	}

	if(count($aggiudicatari_ids) > 0){
		error_log("aggiudicatari_ids " . print_r($aggiudicatari_ids,1));
		update_post_meta($post_id,'avcp_aggiudicatari',$aggiudicatari_ids);
	}

	if(count($parents) > 0){
		error_log("parents " . print_r($parents,1));
		update_post_meta($post_id,'avcp_aggiudicatari_parents',$parents);
	}


	foreach($parameters['Allegati'] as $attachment) {
		if(isset($attachment['NomeDocumento']) && isset($attachment['File'])){
			$attachment_name = $attachment['NomeDocumento'];
			$decoded         = base64_decode( $attachment['File'] );

			$upload_dir  = wp_upload_dir();
			$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;
			file_put_contents( $upload_path . $attachment_name, $decoded );
			$content_type = get_mime_type($attachment_name);
			$attachment = array(
				'post_mime_type' => $content_type,
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $attachment_name ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
				'guid'           => $upload_dir['url'] . '/' . basename( $attachment_name )
			);
			wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $attachment_name, $post_id);
		}


	}

	$response_data["resultCode"] = "00";
	$response_data["idGara"] = $post_id;
	$response_data["resultDescription"] = 'Gara pubblicata con successo';
	$response = new WP_REST_Response( $response_data);
	$response->set_status( 200 );
	return $response;

}

function get_mime_type($filename) {
	$idx = explode( '.', $filename );
	$count_explode = count($idx);
	$idx = strtolower($idx[$count_explode-1]);

	$mimet = array(
		'txt' => 'text/plain',
		'htm' => 'text/html',
		'html' => 'text/html',
		'php' => 'text/html',
		'css' => 'text/css',
		'js' => 'application/javascript',
		'json' => 'application/json',
		'xml' => 'application/xml',
		'swf' => 'application/x-shockwave-flash',
		'flv' => 'video/x-flv',

		// images
		'png' => 'image/png',
		'jpe' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',
		'bmp' => 'image/bmp',
		'ico' => 'image/vnd.microsoft.icon',
		'tiff' => 'image/tiff',
		'tif' => 'image/tiff',
		'svg' => 'image/svg+xml',
		'svgz' => 'image/svg+xml',

		// archives
		'zip' => 'application/zip',
		'rar' => 'application/x-rar-compressed',
		'exe' => 'application/x-msdownload',
		'msi' => 'application/x-msdownload',
		'cab' => 'application/vnd.ms-cab-compressed',

		// audio/video
		'mp3' => 'audio/mpeg',
		'qt' => 'video/quicktime',
		'mov' => 'video/quicktime',

		// adobe
		'pdf' => 'application/pdf',
		'psd' => 'image/vnd.adobe.photoshop',
		'ai' => 'application/postscript',
		'eps' => 'application/postscript',
		'ps' => 'application/postscript',

		// ms office
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',
		'docx' => 'application/msword',
		'xlsx' => 'application/vnd.ms-excel',
		'pptx' => 'application/vnd.ms-powerpoint',


		// open office
		'odt' => 'application/vnd.oasis.opendocument.text',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
	);

	if (isset( $mimet[$idx] )) {
		return $mimet[$idx];
	} else {
		return 'application/octet-stream';
	}
}

function emptyStr($str) {
	return is_string($str) && strlen($str) === 0;
}


function validateDate($date, $format = 'd/m/Y')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}


function validateDecimal($amount)
{
	if (preg_match('/^[0-9]*(\.[0-9]{0,2})?$/', $amount)){
		return true;
	}
	return false;
}


function get_contraente($input) {

	$tipi_contraente = array(
		'01-PROCEDURA APERTA',
		'02-PROCEDURA RISTRETTA',
		'03-PROCEDURA NEGOZIATA PREVIA PUBBLICAZIONE',
		'04-PROCEDURA NEGOZIATA SENZA PREVIA PUBBLICAZIONE',
		'05-DIALOGO COMPETITIVO',
		'06-PROCEDURA NEGOZIATA SENZA PREVIA INDIZIONE DI GARA (SETTORI SPECIALI)',
		'07-SISTEMA DINAMICO DI ACQUISIZIONE',
		'08-AFFIDAMENTO IN ECONOMIA - COTTIMO FIDUCIARIO',
		'14-PROCEDURA SELTTIVA EX ART 238 C.7, D.LGS. 163/2006',
		'17-AFFIDAMENTO DIRETTO EX ART. 5 DELLA LEGGE 381/91',
		'21-PROCEDURA RISTRETTA DERIVANTE DA AVVISI CON CUI SI INDICE LA GARA',
		'22-PROCEDURA NEGOZIATA CON PREVIA INDIZIONE DI GARA (SETTORI SPECIALI)',
		'23-AFFIDAMENTO DIRETTO',
		'24-AFFIDAMENTO DIRETTO A SOCIETA\' IN HOUSE',
		'25-AFFIDAMENTO DIRETTO A SOCIETA\' RAGGRUPPATE/CONSORZIATE O CONTROLLATE NELLE CONCESSIONI E NEI PARTENARIATI',
		'26-AFFIDAMENTO DIRETTO IN ADESIONE AD ACCORDO QUADRO/CONVENZIONE',
		'27-CONFRONTO COMPETITIVO IN ADESIONE AD ACCORDO QUADRO/CONVENZIONE',
		'28-PROCEDURA AI SENSI DEI REGOLAMENTI DEGLI ORGANI COSTITUZIONALI',
		'29-PROCEDURA RISTRETTA SEMPLIFICATA',
		'30-PROCEDURA DERIVANTE DA LEGGE REGIONALE',
		'31-AFFIDAMENTO DIRETTO PER VARIANTE SUPERIORE AL 20% DELL\'IMPORTO CONTRATTUALE',
		'32-AFFIDAMENTO RISERVATO',
		'33-PROCEDURA NEGOZIATA PER AFFIDAMENTI SOTTO SOGLIA',
		'34-PROCEDURA ART.16 COMMA 2-BIS DPR 380/2001 PER OPERE URBANIZZAZIONE A SCOMPUTO PRIMARIE SOTTO SOGLIA COMUNITARIA',
		'35-PARTERNARIATO PER L�INNOVAZIONE',
		'36-AFFIDAMENTO DIRETTO PER LAVORI, SERVIZI O FORNITURE SUPPLEMENTARI',
		'37-PROCEDURA COMPETITIVA CON NEGOZIAZIONE',
		'38-PROCEDURA DISCIPLINATA DA REGOLAMENTO INTERNO PER SETTORI SPECIALI'
	);
	if( in_array(strtoupper($input), $tipi_contraente)){
		return true;
	}
	return false;
}

function generate_token( $user_login, $user ) {
// Set expiry_timestamp..
	$expiry_date = date('Y-m-d H:i:s', strtotime('now +'. EXPIRE_TIME . ' minutes'));
	$manager = WP_Session_Tokens::get_instance( $user->ID );
	$token   = $manager->create( $expiry_date );
	global $wpdb;
	$prefix = $wpdb->prefix;
	$table_name = $prefix . 'login_tokens';
	$wpdb->query( "DELETE FROM $table_name WHERE user_id IN($user->ID)");
	$wpdb->insert($table_name, array(
		'auth_token' => $token,
		'auth_token_expiration' => $expiry_date,
		'user_id' => $user->ID
	));
}
add_action('wp_login', 'generate_token', 10, 2);

add_action( 'plugins_loaded', function () {
	global $wpdb;
	$prefix = $wpdb->prefix;
	$table_name = $prefix . 'login_tokens';

	$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

	if ( ! $wpdb->get_var( $query ) == $table_name ) {
		$charset = $wpdb->get_charset_collate();
		$sql = "
    CREATE TABLE $table_name (
        id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        auth_token VARCHAR(100),
        auth_token_expiration datetime,
        user_id INT NOT NULL,
        PRIMARY KEY (id)
    ) $charset;
    ";
		// maybe_create_table( $wpdb->prefix . $table_name, $sql );
	}
} );

?>

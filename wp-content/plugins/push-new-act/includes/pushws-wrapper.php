<?php

class pushws_Response {
	public $ErrorCode;
	public $ErrorDescription;
	public $ActID;
	function __construct($ErrorCode, $ErrorDescription, $ActID) {
		$this->ErrorCode = $ErrorCode;
		$this->ErrorDescription = $ErrorDescription;
		$this->ActID = $ActID;
	}
	
	/**
	 * Converts a post retrieved trough the WordPress method get_post
	 * to a wpws_Post object.
	 * @return pushws_Response
	 */
	function construct_response($errorCode, $errorDescription,$actId=' ') {
			$pushws_Auth_Failed = new pushws_Response($errorCode,$errorDescription,$actId);
			return $pushws_Auth_Failed;
	}
}



/* SOAP compatible Post definition */
class wpws_Post {
	public $id;
	public $author;
	public $date;
	public $dateGmt;
	public $content;
	public $title;
	public $excerpt;
	public $status;
	public $commentStatus;
	public $pingStatus;
	public $password;
	public $name;
	public $toPing;
	public $pinged;
	public $modified;
	public $modifiedGmt;
	public $contentFiltered;
	public $parentId;
	public $guid;
	public $menuOrder;
	public $type;
	public $mimeType;
	public $commentCount;
	public $filter;
 
	function __construct($id, $author, $date, $dateGmt,
						 $content, $title, $excerpt,
						 $status, $commentStatus, $pingStatus,
						 $password, $name, $toPing, $pinged,
						 $modified, $modifiedGmt, $contentFiltered,
						 $parentId, $guid, $menuOrder, $type, $mimeType,
						 $commentCount, $filter) {
		$this->id = $id;
		$this->author = $author;
		$this->date = $date;
		$this->dateGmt = $dateGmt;
		$this->content = $content;
		$this->title = $title;
		$this->excerpt = $excerpt;
		$this->status = $status;
		$this->commentStatus = $commentStatus;
		$this->pingStatus = $pingStatus;
		$this->password = $password;
		$this->name = $name;
		$this->toPing = $toPing;
		$this->pinged = $pinged;
		$this->modified = $modified;
		$this->modifiedGmt = $modifiedGmt;
		$this->contentFiltered = $contentFiltered;
		$this->parentId = $parentId;
		$this->guid = $guid;
		$this->menuOrder = $menuOrder;
		$this->type = $type;
		$this->mimeType = $mimeType;
		$this->commentCount = $commentCount;
		$this->filter = $filter;
	}
	
	/**
	 * Converts a post retrieved trough the WordPress method get_post
	 * to a wpws_Post object.
	 * @return wpws_Post
	 */
	function convert($post) {
		if(!$post) return null;
		else {
			$wpws_post = new wpws_Post(
					intval($post->ID),
					$post->post_author,
					$post->post_date,
					$post->post_date_gmt,
					$post->post_content,
					$post->post_title,
					$post->post_excerpt,
					$post->post_status,
					$post->comment_status,
					$post->ping_status,
					$post->post_password,
					$post->post_name,
					$post->to_ping,
					$post->pinged,
					$post->post_modified,
					$post->post_modified_gmt,
					$post->post_content_filtered,
					intval($post->post_parent),
					$post->guid,
					intval($post->menu_order),
					$post->post_type,
					$post->post_mime_type,
					$post->comment_count,
					$post->filter);
			return $wpws_post;
		}
	}
}
?>
<?php
define("WS_PLUGIN_NAME", "push-new-act");
define("WS_ENTRY_FILE", dirname(__FILE__) . "/../" . WS_PLUGIN_NAME . ".php");
define("WS_INDEX_FILE", dirname(__FILE__) . "/pushws-index.php");
define("WS_WSDL_TEMPLATE", dirname(__FILE__) . "/../pushws.template.wsdl");
define("WS_WSDL", dirname(__FILE__) . "/../pushws.wsdl");
define("WS_SOAP_SERVER_FILE", dirname(__FILE__) . "/push-ws-soap.php");
define("WS_SOAP_SERVER_CLASS", "push_WebService");

define("WS_APP_URL", "%{APP_URL}");
define("WS_CACHE_DIR", dirname(__FILE__) . "/../cache");
define("WP_UPLOAD_DIR", "/wp-content/uploads");


function ws_getVersion() {
	$entry_file = file_get_contents(WS_ENTRY_FILE);
	$version = preg_replace("~.*Version:\W*([a-zA-Z0-9\._]*).*~sm", "\\1", $entry_file);
	return $version;
}

function ws_WSDLcustomized() {
	if(file_exists(WS_WSDL)) {
		$wsdl = file_get_contents(WS_WSDL_TEMPLATE);
		return (strpos($wsdl, WS_APP_URL) !== false);
	}
}

function ws_createWSDL() {
	if (!class_exists('AlboPretorio')) {
		wp_die('Sorry, but this plugin requires the AlboPretorio Plugin to be installed and active.');
	}
	
    $wsdl = ws_getWSDLfromTemplate();
    @file_put_contents(WS_WSDL, $wsdl);
}

function ws_getWSDLfromTemplate() {
	$wsdl = file_get_contents(WS_WSDL_TEMPLATE);
	return str_replace(WS_APP_URL, ws_getAppUrl(), $wsdl);
}

function ws_getAppUrl() {
	return (defined("WP_HOME")) ? WP_HOME : get_option("home", "");
}

function ws_getWsdlUrl() {
	return ws_getAppUrl() . "/index.php?/pushws/?wsdl";
}

function ws_getPluginUrl() {
	return ws_getAppUrl() . "/wp-content/plugins/" . WS_PLUGIN_NAME;
}


function ws_getBaseDir() {
	$current_path = $_SERVER['SCRIPT_FILENAME'];
	while(true) {
		$slash_pos = strrpos($current_path, "/");
		if($slash_pos === false) return false;
		
		$current_path = substr($current_path, 0, $slash_pos);
		if(file_exists($current_path . "/wp-load.php")) {
			return $current_path;
		}
	}
}

function ws_cacheIsFunctional() {
	return is_dir(WS_CACHE_DIR) && is_writable(WS_CACHE_DIR);
}

function ws_getCacheDir() {
	return WS_CACHE_DIR;
}

function ws_genUniqueCode($length = 8) {	
	$code = md5(uniqid(rand(), true));
	if(is_int($length)) return substr($code, 0, $length);
	else return $code;
}
?>
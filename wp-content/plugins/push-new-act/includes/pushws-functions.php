<?php

/**
 * Functions necessary to the plugin
 * @package    Push New Act WS
 */

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

function pushws_get_tipologia_id($tipo){
    $tipo_codice = 0;
    $Tipo_data=get_option( 'opt_ap_Tipologia' );  
	$Tipo_data=json_decode($Tipo_data);
    foreach($Tipo_data as $tiplology){
        if(strcasecmp($tipo,$tiplology->Funzione) == 0){
            $tipo_codice = $tiplology->Codice;
            break; 
        }
    }
    return $tipo_codice;
}

function pushws_get_giorni_albopretorio_categorie($nome){
    global $wpdb, $table_prefix;
    $wpdb->table_name_Categorie = $table_prefix . "albopretorio_categorie";
    $Sql="SELECT Giorni FROM $wpdb->table_name_Categorie WHERE Nome=%s;";
	return $wpdb->get_results($wpdb->prepare($Sql,$nome));
}

function pushws_get_id_albopretorio_categorie($nome){
    global $wpdb, $table_prefix;
    $wpdb->table_name_Categorie = $table_prefix . "albopretorio_categorie";
    $Sql="SELECT IdCategoria FROM $wpdb->table_name_Categorie WHERE Nome=%s;";
	return $wpdb->get_results($wpdb->prepare($Sql,$nome));
}

function pushws_get_id_albopretorio_enti($nome){
    global $wpdb, $table_prefix;
    $wpdb->table_name_Categorie = $table_prefix . "albopretorio_enti";
    $Sql="SELECT IdEnte FROM $wpdb->table_name_Categorie WHERE Nome=%s;";
	return $wpdb->get_results($wpdb->prepare($Sql,$nome));
}

function pushws_get_max_id_atti(){
    global $wpdb;
    $Sql="SELECT MAX(Numero) AS ID FROM $wpdb->table_name_Atti";
	return $wpdb->get_results($Sql);
}

function pushws_insert_atto($Ente,$Tipologia,$Riferimento,$Oggetto,$DataInizio,
                                $DataFine,$DataOblio,$Note,$Categoria,$Responsabile,$Soggetti,$Uffici){
    global $wpdb;
    $newIDAtto = -1;
	$Anno=date("Y");
	$Numero=0;
	$maxNumeroRes = pushws_get_max_id_atti();

	if(count($maxNumeroRes) > 0)
	{
		$Numero = $maxNumeroRes[0]->ID;
	}
	$Numero = $Numero + 1;
	$DataInizio=ap_convertiData($DataInizio);
	$DataFine=ap_convertiData($DataFine);
	$DataOblio=ap_convertiData($DataOblio);
	if(!$Responsabile){
		$Responsabile=0;
	}
	if ( false === $wpdb->insert(
		$wpdb->table_name_Atti,array(
				'Ente' => $Ente,
				'Numero' => $Numero,
				'Anno' =>  $Anno,
				'Tipologia' => $Tipologia,
				'Riferimento' => $Riferimento,
				'Oggetto' => $Oggetto,
				'DataInizio' => $DataInizio,
				'DataFine' => $DataFine,
				'DataOblio' => $DataOblio,
				'Informazioni' => $Note,
				'IdCategoria' => $Categoria,
				'RespProc' => $Responsabile,
				'Soggetti' => $Soggetti,
				'Uffici' => $Uffici),
				array(
				'%d',
				'%d',
                '%d',
                '%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s')))	{
                    return 'Non sono riuscito ad inserire il nuovo Atto Sql=='.$wpdb->last_query .' Ultimo errore=='.$wpdb->last_error;
    }else{
		$newIDAtto=$wpdb->insert_id;
    }
    
    return $newIDAtto;
}

function pushws_add_attimeta($idAtto,$metaName,$metaValue){
	global $wpdb;
	$Sql="SELECT Value FROM $wpdb->table_name_Attimeta WHERE IdAtto=%d And Meta=%s;";
	$Res=$wpdb->get_results($wpdb->prepare($Sql,$idAtto,$metaName));
	if(count($Res)==0){
		if ( false === $wpdb->insert($wpdb->table_name_Attimeta,
										array('IdAtto' => $idAtto,
											   'Meta'  => $metaName,
											   'Value' => $metaValue),
										array('%d','%s','%s')))
			return FALSE;
		else
			return TRUE;	
	}else{
		if ( $wpdb->update($wpdb->table_name_Attimeta,
								array('Value' => $metaValue),
								array( 'IdAtto' => $idAtto,
									   'Meta'   => $metaName),
								array( '%s'),
								array( '%d','%s')))
				return TRUE;
			else
				return FALSE;
	}
}

function pushws_del_atto($id) {
	global $wpdb;
	$wpdb->query($wpdb->prepare( "DELETE FROM $wpdb->table_name_Atti WHERE	IdAtto=%d",$id));
	return True;
}

function pushws_delete_metaatto($IDAtto,$MetaName){
	global $wpdb;
	$Sql="DELETE FROM $wpdb->table_name_Attimeta WHERE	IdAtto=%d And Meta=%s";
	$Sql=$wpdb->prepare( $Sql,$IDAtto,$MetaName);
	$Res=$wpdb->query($Sql);
	if($Res!==FALSE And $Res==0){
		return TRUE;
	}else{
		return FALSE;
	}
}

function pushws_insert_allegato($Allegato,$IdAtto){
	
    global $wpdb;
        $IdAtto=(int)$IdAtto;
        if ( false === $wpdb->insert(
            $wpdb->table_name_Allegati,array(
                    'Allegato' =>  $Allegato,
                    'IdAtto' => $IdAtto),array(
                    '%s',
                    '%d')))	
            return false;
        else
            return true;
    }

	
function pushws_check_atti_id($idAct){
    global $wpdb;
	$Sql="SELECT IdAtto FROM $wpdb->table_name_Atti WHERE IdAtto=%d";
	$Sql=$wpdb->prepare( $Sql,$idAct);
	return $wpdb->get_var($Sql);
}

function pushws_update_atto($IdAtto,$idNewAtto, $Typology, $Reference){
	global $wpdb;
	//$motivoAnnullamento = 'Annullato da determina numero ' . $idNewAtto . ' anno ' . date("Y");
	$motivoAnnullamento = 'Annullato da ' . $Typology . ' ' . $Reference;
	$wpdb->update($wpdb->table_name_Atti,
	array('DataAnnullamento' => date('Y-m-d'),
			'MotivoAnnullamento' => $motivoAnnullamento),
	array( 'IdAtto' => $IdAtto),
	array( '%s','%s'),
	array( '%d'));
}

function pushws_get_term_by_name($nome){
    global $wpdb, $table_prefix;
    $wpdb->terms = $table_prefix . "terms";
    $Sql="SELECT term_id FROM $wpdb->terms WHERE name=%s;";
	return $wpdb->get_results($wpdb->prepare($Sql,$nome));
}

function push_new_name($path, $file_name) {
	$res = "$path/$file_name";
	if (!file_exists($res)) return $file_name;
	
	$actual_name = pathinfo($file_name,PATHINFO_FILENAME);
	$original_name = $actual_name;
	$extension = pathinfo($file_name, PATHINFO_EXTENSION);
	$i = 1;

	while(file_exists($path.'/'.$actual_name.".".$extension))
	{           
		$actual_name = (string)$original_name.$i;
		$file_name = $actual_name.".".$extension;
		$i++;
	}
	return $file_name;
}

function pushws_validate_date($date, $format = 'd-m-Y')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

?>
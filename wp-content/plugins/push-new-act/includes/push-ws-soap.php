<?php

require_once(dirname(__FILE__) . "/pushws-wrapper.php");
include_once(dirname(__FILE__) . '/pushws-functions.php');	

class push_WebService {
	/**
	 * Create new act
	 */
	function pushNewAct($args=null) {
		 $request = $args;
		 if(isset($request->Document->Data) && !empty($request->Document->Data)){
			$request->Document->Data = base64_encode($request->Document->Data);
		 }
		 
		 if(isset($request->Document->Attachments) 
		 	&& isset($request->Document->Attachments->Attachment)) {
			if(is_array($request->Document->Attachments->Attachment)){
				foreach($request->Document->Attachments as $attachments) {
					foreach($attachments as $attachment) {
						if(!isset($attachment->Data) || empty($attachment->Data)){
							continue;
						}
						$attachment->Data = base64_encode($attachment->Data);
					}
				}
			}else if(isset($request->Document->Attachments->Attachment) && 
				isset($request->Document->Attachments->Attachment->Data)){
					$request->Document->Attachments->Attachment->Data = base64_encode($request->Document->Attachments->Attachment->Data);
			}
		 }

		 error_log("Push request recieved is " . print_r($request,1));
		 $headers = apache_request_headers();
		 $auth = $headers['Authorization'];
		 $isValid = true;
		if(empty($auth)){
			$isValid = false;
		}else {
				list($username,$password) = array_pad(explode(':',base64_decode(substr( $auth, 6))),2,null);
				$user = apply_filters( 'authenticate', null, $username, $password );
				if ( $user == null ) {
					$isValid = false;
				}
				$ignore_codes = array( 'empty_username', 'empty_password' );
				if ( empty($password) || (is_wp_error( $user ) && ! in_array( $user->get_error_code(), $ignore_codes )) ) {
					$isValid = false;
				}
		}
		if(!$isValid)
			return pushws_Response::construct_response(1,"Accesso Negato");

		$isValidRequest = true;
		$isUpdateReuquest = false;
		
		if($args != null) {
			
			if (!isset($args->Document)
				|| !isset($args->Document->Typology)
				|| !isset($args->Document->Category)
				|| !isset($args->Document->Reference)
				|| !isset($args->Document->Subject)
				// || !isset($args->Document->Note)
				|| !isset($args->Document->Entity)
			//	|| !isset($args->Document->Data)
			//	|| !isset($args->Document->FileName)
				|| !isset($args->Document->Annulment)
				|| empty($args->Document->Annulment)
				|| (trim(strtoupper($args->Document->Annulment)) !== "TRUE" 
					&& trim(strtoupper($args->Document->Annulment)) !== "FALSE")
				|| (trim(strtoupper($args->Document->Annulment)) === "TRUE" &&
					!isset($args->Document->ActID))
			) 
			{
				$isValidRequest = false;
			}
			if(isset($args->Document->Data) && !empty($args->Document->Data) && empty($args->Document->FileName)){
				$isValidRequest = false;
			}
		}else {
			$isValidRequest = false;
		}
		
		

		if(!$isValidRequest)
		{
			return pushws_Response::construct_response(2,"Struttura XML non corretta o metadati obbligatori mancanti");
		}else if(trim(strtoupper($args->Document->Annulment)) === "TRUE"){
			$actId = pushws_check_atti_id($args->Document->ActID);
			if($actId <= 0)
			{
				return pushws_Response::construct_response(3,"Atto da annullare non trovato");
			}else 
			{
				$isUpdateReuquest = true;
			}
		}

		if(trim(strtoupper($args->Document->Annulment)) === "FALSE" || $isUpdateReuquest){
			$anno=date("Y");
			$numero=0;
			$idTipologia = pushws_get_tipologia_id($args->Document->Typology);
			if($idTipologia == 0)
			{
				error_log("No tipologia found for category " . $args->Document->Typology);
				return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
			}
			$riferimento = $args->Document->Reference;
			$oggetto =  $args->Document->Subject;
			$dataInizio = date('Y-m-d');
			$giorniRes = pushws_get_giorni_albopretorio_categorie($args->Document->Category);
			if(count($giorniRes) == 0)
			{
				if ( WP_DEBUG === true ) 
				{
					error_log("No Giorni found for category " . $args->Document->Category);
				}
				return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
			}

			$giorni = $giorniRes[0]->Giorni;
			$dataFine = date('d/m/Y', strtotime("+" . $giorni . " days"));
			$informazioni = $args->Document->Note;
			$idCategoryRes = pushws_get_id_albopretorio_categorie($args->Document->Category);
			if(count($idCategoryRes) == 0)
			{
				if ( WP_DEBUG === true ) 
				{
					error_log("No Id found for category " . $args->Document->Category);
				}
				return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
			}
			$idCategoria = $idCategoryRes[0]->IdCategoria;
			$soggetti = 'N';
			$uffici = 'N';
			$idEnteRes= pushws_get_id_albopretorio_enti($args->Document->Entity);
			if(count($idEnteRes) == 0)
			{
				if ( WP_DEBUG === true ) 
				{
					error_log("No Id found for enti " . $args->Document->Entity);
				}
				return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
			}
			$idEnte = $idEnteRes[0]->IdEnte;
			$respProc = 0;
			$dataOblio = '2025-01-01';
			$ultimamodifica = date('YYYY-MM-DD hh:mm:ss');
			
			$newIDAtto=pushws_insert_atto($idEnte,
										  $idTipologia,
										  $riferimento,
										  $oggetto,
										  $dataInizio,
										  $dataFine,
										  $dataOblio,
										  $informazioni,
										  $idCategoria,
										  $respProc,
										  $soggetti,
										  $uffici);
			
			if ( is_numeric( $newIDAtto )) {
				$add_attimeta = pushws_add_attimeta($newIDAtto,'_evidenza',1);
				if($add_attimeta){
					$files = array();
					$destination_path =AP_BASE_DIR.get_option('opt_AP_FolderUpload');
					$is_file_created = true;
					$is_insert = true;
					if(isset($request->Document->Data) && !empty($request->Document->Data)){
						$pdf_decoded = base64_decode($request->Document->Data);
						$file_name = $args->Document->FileName;
						try {
							$path = tempnam( get_temp_dir(), 'push' );
							$temp_file = fopen( $path, 'r+' );
							fwrite( $temp_file, $pdf_decoded );
							fclose( $temp_file );
							$path = pathinfo( $path );
							$dir = $path['dirname'];
							$basename = $path['basename'];
							$file_name = push_new_name($destination_path, $file_name);
							rename( $dir.'/'.$basename, $destination_path.'/'. $file_name );
							
							array_push( $files , $destination_path.'/'. $file_name);
						} catch (Exception $e) {
							if ( WP_DEBUG === true ) 
							{
								error_log('Caught exception: ',  $e->getMessage());
							}
							$is_file_created = false;
						}
					}
					if(isset($request->Document->Attachments) && isset($request->Document->Attachments->Attachment)) {
						if(is_array($request->Document->Attachments->Attachment)){
							foreach($request->Document->Attachments as $attachments) {
								foreach($attachments as $attachment) {
									if(!isset($attachment->Data) || empty($attachment->Data)){
										continue;
									}
									try {
										$attachment_decoded = base64_decode($attachment->Data);
										$attachment_file_name = $attachment->FileName;
										$path = tempnam( get_temp_dir(), 'push' .  $attachment_file_name);
										$temp_attachment_file = fopen( $path, 'r+' );
										fwrite( $temp_attachment_file, $attachment_decoded );
										fclose( $temp_attachment_file );
										$path = pathinfo( $path );
										$dir = $path['dirname'];
										$basename = $path['basename'];
										$attachment_file_name = push_new_name($destination_path, $attachment_file_name);
	
										if ( file_exists( $dir.'/'. $attachment_file_name ) ) {
											unlink( $dir.'/'. $attachment_file_name );
										}
										rename( $dir.'/'.$basename, $destination_path.'/'. $attachment_file_name );
										array_push( $files , $destination_path.'/'. $attachment_file_name);
									} catch (Exception $e) {
										if ( WP_DEBUG === true ) 
										{
											error_log('Caught exception: ',  $e->getMessage());
										}
										$is_file_created = false;
									}
								}
							}
						}else if(isset($request->Document->Attachments->Attachment) && 
								isset($request->Document->Attachments->Attachment->Data)){
							try {
								
								$attachment_decoded = base64_decode($request->Document->Attachments->Attachment->Data);
								$attachment_file_name = $request->Document->Attachments->Attachment->FileName;
								$path = tempnam( get_temp_dir(), 'push' .  $attachment_file_name);
								$temp_attachment_file = fopen( $path, 'r+' );
								fwrite( $temp_attachment_file, $attachment_decoded );
								fclose( $temp_attachment_file );
								$path = pathinfo( $path );
								$dir = $path['dirname'];
								$basename = $path['basename'];
								$attachment_file_name = push_new_name($destination_path, $attachment_file_name);
								if ( file_exists( $dir.'/'. $attachment_file_name ) ) {
									unlink( $dir.'/'. $attachment_file_name );
								}
								rename( $dir.'/'.$basename, $destination_path.'/'. $attachment_file_name );
								array_push( $files , $destination_path.'/'. $attachment_file_name);
							} catch (Exception $e) {
								if ( WP_DEBUG === true ) 
								{
									error_log('Caught exception: ',  $e->getMessage());
								}
								$is_file_created = false;
							}
						}
					}
					
					if($is_file_created){
						foreach($files as $file){
							$is_insert = pushws_insert_allegato($file,$newIDAtto);
						}
						
						if($is_insert){
							if ($isUpdateReuquest) {
								pushws_update_atto($args->Document->ActID, $newIDAtto,$args->Document->Typology, $riferimento);
							}
							return pushws_Response::construct_response(0,"Documento salvato correttamente",$newIDAtto);
						}
						else {
							foreach($files as $file){
								if ( file_exists( $file ) ) {
									unlink( $file );
								}
							}
						}
					}else {
						foreach($files as $file){
							if ( file_exists( $file ) ) {
								unlink( $file );
							}
						}
					}

					if(!$is_file_created || !$is_insert){
						if ( WP_DEBUG === true ) 
						{
							error_log("Error in file creation/allegato insertion");
						}
						pushws_delete_metaatto($newIDAtto,'_evidenza');
						pushws_del_atto($newIDAtto);
						return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
					}
					
				}else {
					pushws_del_atto($newIDAtto);
					if ( WP_DEBUG === true ) 
					{
						error_log("Error in inserting  atti meta");
					}
					return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
				}
			}else {
				if ( WP_DEBUG === true ) 
				{
					error_log("Error in inserting  atto" . $newIDAtto);
				}
				return pushws_Response::construct_response(4,"Si sono verificati errori durante il salvataggio del documento");
			}
		}

		
	 }

	
}

?>
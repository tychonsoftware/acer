<?php if ( ! defined( 'ABSPATH' ) ) exit;

/*
 * Plugin Name: Ninja Forms - Sendy
 * Plugin URI: http://ninjaforms.com
 * Description: Add user to the Sendy List when Ninja Form is submitted.
 * Version: 3.0
 * Author: Aman Saini
 * Author URI: https://webholics.in
 * Text Domain: nf-sendy
 *
 * Copyright 2016 Aman Saini.
 */

if ( version_compare( get_option( 'ninja_forms_version', '0.0.0' ), '3.0.0', '<' ) || get_option( 'ninja_forms_load_deprecated', FALSE ) ) {

	include 'deprecated/ninja-forms-sendy.php';

} else {

	/**
	 * Class NF_Sendy
	 */
	final class NF_Sendy {
		const VERSION = '3.0';
		const SLUG    = 'sendy';
		const NAME    = 'Sendy';
		const AUTHOR  = 'Aman Saini';
		const PREFIX  = 'NF_Sendy';

		/**
		 *
		 *
		 * @var NF_Sendy
		 * @since 2.0
		 */
		private static $instance;

		/**
		 * Plugin Directory
		 *
		 * @since 2.0
		 * @var string $dir
		 */
		public static $dir = '';

		/**
		 * Plugin URL
		 *
		 * @since 2.0
		 * @var string $url
		 */
		public static $url = '';
		/**
		 *
		 *
		 * @var string
		 */
		protected $_api_endpoint = 'https://api.sendy.com/v3';

		/**
		 * Main Plugin Instance
		 *
		 * Insures that only one instance of a plugin class exists in memory at any one
		 * time. Also prevents needing to define globals all over the place.
		 *
		 * @since 2.0
		 * @static
		 * @static var array $instance
		 * @return NF_Sendy Highlander Instance
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof NF_Sendy ) ) {
				self::$instance = new NF_Sendy();

				self::$dir = plugin_dir_path( __FILE__ );

				self::$url = plugin_dir_url( __FILE__ );

				/*
				 * Register our autoloader
				 */
				spl_autoload_register( array( self::$instance, 'autoloader' ) );

				new NF_Sendy_Admin_Settings();
			}
			return self::$instance;
		}

		public function __construct() {
			/*
			 * Required for all Extensions.
			 */
			add_action( 'admin_init', array( $this, 'setup_license' ) );

			add_filter( 'ninja_forms_register_actions', array( $this, 'register_actions' ) );
			add_filter( 'ninja_forms_register_fields', array( $this, 'register_fields' ) );


		}

		/**
		 * Register Fields
		 *
		 * @param array   $actions
		 * @return array $actions
		 */
		public function register_fields( $actions ) {
			$actions[ 'sendy-optin' ] = new NF_Sendy_Fields_OptIn();

			return $actions;
		}

		/**
		 * Register Actions
		 *
		 * @param array   $actions
		 * @return array $actions
		 */
		public function register_actions( $actions ) {
			$actions[ 'sendy' ] = new NF_Sendy_Actions_Sendy(); // includes/Actions/SendyExample.php

			return $actions;
		}


		/*
		 * Optional methods for convenience.
		 */

		public function autoloader( $class_name ) {

			if ( class_exists( $class_name ) ) return;

			if ( false === strpos( $class_name, self::PREFIX ) ) return;

			$class_name = str_replace( self::PREFIX, '', $class_name );
			$classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
			$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
			if ( file_exists( $classes_dir . $class_file ) ) {
				require_once $classes_dir . $class_file;
			}
		}

		/*
		 * Required methods for all extension.
		 */

		public function setup_license() {

			if ( ! class_exists( 'NF_Extension_Updater' ) ) return;

			new NF_Extension_Updater( self::NAME, self::VERSION, self::AUTHOR, __FILE__, self::SLUG );
		}

		/**
		 * Load Config File
		 *
		 * @param unknown $file_name
		 * @return array
		 */
		public static function config( $file_name ) {
			return include self::$dir . 'includes/Config/' . $file_name . '.php';
		}



		public function subscribe( $params ) {

			$sendy_url = untrailingslashit( trim( Ninja_Forms()->get_setting( 'nf_sendy_url' ) ) );
			if ( ! empty( $sendy_url ) ) {
				$url       = $sendy_url .'/subscribe';
				$response  = wp_remote_post( $url, array( 'body' => $params ) );
			}
		}

	}


	/**
	 * The main function responsible for returning The Highlander Plugin
	 * Instance to functions everywhere.
	 *
	 * Use this function like you would a global variable, except without needing
	 * to declare the global.
	 *
	 * @since 2.0
	 * @return {class} Highlander Instance
	 */
	function NF_Sendy() {
		return NF_Sendy::instance();
	}

	NF_Sendy();

}

// Upgrade form settings to 3.0
add_filter( 'ninja_forms_upgrade_settings',  'convert_nf_sendy_settings_to_action'  );

function convert_nf_sendy_settings_to_action( $form_data ) {
	if ( isset( $form_data[ 'settings' ][ 'enable_sendy' ] ) && 1 == $form_data[ 'settings' ][ 'enable_sendy' ] ) {

		$new_action = array(
			'type' => 'sendy',
			'label' => __( 'Sendy', 'nf-sendy' ),
			'sendy_list' => $form_data[ 'settings' ][ 'sendy_list_id' ]
		);

		foreach ( $form_data['fields'] as $field ) {
			$tag = 'field_'.$field['id'];
			if ( $field['data']['email'] ) {
				$new_action['email'] = $tag;
			} elseif ( $field['data']['first_name'] ) {
				$new_action['contactname'] =  $tag;
			}
		}

		$form_data[ 'actions' ][] = $new_action;

		$settings = Ninja_Forms()->get_settings();
		if ( isset( $settings['sendy_url'] ) ) {
			Ninja_Forms()->update_setting( 'nf_sendy_url', $settings['sendy_url'] );
		}
	}

	return $form_data;
}


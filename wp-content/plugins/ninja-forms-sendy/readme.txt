=== Ninja Forms Sendy Integration ===
Contributors: aman086
Tags: ninjaforms sendy, sendy, newslettter, sendy plugin,ninjaforms addon
Donate link: https://webholics.in
Requires at least: 3.5
Tested up to: 4.2
Stable tag: 3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Sendy Addon for Ninja Forms lets you add user’s to your Campaigns in Sendy using Ninja Forms

== Description ==
Add users to Sendy Campaigns using Ninja forms. You can create unlimited forms and place it anywhere on site.Each form can have different Campaigns which let's you add users to different Campaigns in Sendy.
Additionally you can also map the custom fields with Ninja Forms Fields.

Features:

* Unlimited Forms
* Option to enable/disable for each form
* Option to use different campaign for each form
* Option to Send IP Address of user
* Map custom fields of your campaign to fields in Ninja Forms


== Installation ==
To install plugin, follow these steps:

1. Download and unzip the plugin.

2. Upload the entire ninja-forms-sendy/ directory to the /wp-content/plugins/ directory.

3. Activate the plugin through the Plugins menu in WordPress.

4. Goto Forms -> Settings-> License in Admin panel.

5. Add your Plugin license key which you received in email.

6. Now goto Forms ->Sendy.

7. Enter Sendy API key .

8. Create/Edit the form.

9. Goto Forms Settings , click on enable Sendy checkbox and select the campaign to which you want users to subscribe.



== Changelog ==

= 3.0 ( 01 May 2017 ) =
New:
 - Added compatibility for Ninja Forms 3.0

= 1.2.1 ( 5th June 2015 ) =
Bugs:
 - Fixed autoresponder cycle value not passing to sendy.

= 1.2 ( 30 April 2015 ) =
New:
 - Added Autoresponder Cycle Day option.

= 1.1 ( 22 April 2015 ) =
Bugs:
 * Fixed fatal error if Send user IP is enabled in settings.

= 1.0 ( 14 Jan 2015 ) =

* Plugin released

== Upgrade Notice ==

= 1.0 ( 14 Jan 2015) =

* Plugin released
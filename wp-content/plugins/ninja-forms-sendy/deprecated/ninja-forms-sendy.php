<?php
/*
	Plugin Name: Ninja Forms - Sendy Integration
	Description: Subscribe user to the Sendy list when Ninja form is submitted.
	Author: Aman Saini
	Author URI: http://amansaini.me
	Plugin URI: http://amansaini.me
	Version: 3.0
	Requires at least: 3.5
	Tested up to: 4.1
*/
add_action( 'plugins_loaded', array( 'Ninja_Sendy', 'setup' ) );

class Ninja_Sendy {

	function __construct() {

		add_action( 'admin_init', array( $this, 'ninja_forms_extension_setup_license' ) );
		add_action( 'init', array( $this, 'ninja_forms_register_sendy' ) );
		add_action( 'init', array( $this, 'ninja_forms_register_tab_sendy_settings' ), 10 );
		add_action( 'init', array( $this, 'ninja_forms_sendy_settings_metabox' ) );
		add_filter( 'ninja_forms_form_settings_basic', array( $this, 'add_form_settings' ) );

	}

	//Plugin starting point.
	public static function setup() {
		if ( ! self::is_ninjaform_installed() ) {
			return;
		}
		$class = __CLASS__;
		new $class;
	}

	/**
	 * Setup the license
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @return void
	 */
	function ninja_forms_extension_setup_license() {
		if ( class_exists( 'NF_Extension_Updater' ) ) {
			$NF_Extension_Updater = new NF_Extension_Updater( 'Sendy', '3.0', 'Aman Saini', __FILE__ );
		}
	}


	/**
	 *  Hook ninja_subscribe_user_sendy to process submitted form data
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @return void
	 */
	function ninja_forms_register_sendy() {
		add_action( 'ninja_forms_post_process', array( $this, 'ninja_subscribe_user_sendy' ), 20 );
	}


	/**
	 * Register the sendy settings tab in ninja form settings
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @return  void
	 */
	function ninja_forms_register_tab_sendy_settings() {
		$args = array(
			'name' => __( 'Sendy Settings', 'ninja-sendy' ),
			'page' => 'ninja-forms-settings',
			'display_function' => '',
			'save_function' => array( $this, 'ninja_forms_save_settings' ),
		);
		ninja_forms_register_tab( 'sendy_settings', $args );
	}

	/**
	 * Add the metaboxes to the sendy settings tab
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @return void
	 */
	function ninja_forms_sendy_settings_metabox() {

		$args = array(
			'page' => 'ninja-forms-settings',
			'tab' => 'sendy_settings',
			'slug' => 'sendy_settings',
			'title' => __( 'Sendy Settings', 'ninja-sendy' ),
			'settings' => array(

				array(
					'name' => 'sendy_url',
					'type' => 'text',
					'label' => __( 'Sendy Installation Url', 'ninja-sendy' ),
					'desc' => 'Url where sendy is installed',
				),

				array(
					'name' => 'sendy_api_key',
					'type' => 'text',
					'label' => __( 'Sendy Api Key', 'ninja-sendy' ),
					'desc' => __( 'Enter the sendy api key. Api key is visible on your sendy dashboard', 'ninja-sendy' ),
				),
			),
		);
		//if(function_exists(ninja_forms_register_tab_metabox) )
		ninja_forms_register_tab_metabox( $args );

	}

	/**
	 * Add Enable Sendy checkbox and List ID textbox on form settings
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @param unknown $basic_settings Form setting fields
	 */
	function add_form_settings( $basic_settings ) {

		$sendy_fields = array(
			array(
				'name' => 'enable_sendy',
				'type' => 'checkbox',
				'label' => __( 'Enable Sendy', 'ninja-forms' ),
				'desc' => __( 'Enable users to subscribe to sendy newsletter on this form', 'ninja-sendy' ),
			),

			array(
				'name' => 'sendy_list_id',
				'type' => 'text',
				'label' => __( 'Sendy list Id', 'ninja-forms' ),
				'desc' => __( 'Enter the list Id to which you want to subscribe users.', 'ninja-sendy' ),
			),

		);
		foreach ( $sendy_fields as $fields ) {
			$basic_settings[ 'settings' ][] = $fields;
		}

		return $basic_settings;

	}

	/*
	 * Check if Ninja form is  installed
	 */
	private static function is_ninjaform_installed() {
		return defined( 'NINJA_FORMS_VERSION' );
	}

	/**
	 * Save the metabox value form sendy settings tab
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @param array   $data Setting Values
	 * @return $update_msg
	 */
	function ninja_forms_save_settings( $data ) {
		$plugin_settings = nf_get_settings();

		foreach ( $data as $key => $val ) {
			$plugin_settings[$key] = $val;
		}

		update_option( 'ninja_forms_settings', $plugin_settings );
		$update_msg = __( 'Settings Saved', 'ninja-forms' );
		return $update_msg;
	}


	/**
	 * Perform validation of fields
	 * Add user to list if everything is ok
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @return void
	 */
	function ninja_subscribe_user_sendy() {
		global $ninja_forms_processing;

		$plugin_settings = nf_get_settings();

		if( empty($plugin_settings[ 'sendy_url' ] ))
			return;

		$sendy_url = $plugin_settings[ 'sendy_url' ];

		// Get the current form_id
		$form_id = $ninja_forms_processing->get_form_ID();

		//check if sendy is enabled for this form
		$sendy_enabled = $ninja_forms_processing->get_form_setting( 'enable_sendy' );

		$sendy_list_id = $ninja_forms_processing->get_form_setting( 'sendy_list_id' );


		if ( $sendy_enabled && !empty( $sendy_list_id ) && $sendy_url ) {

			// get the user info
			$user_info = $ninja_forms_processing->get_user_info();

			$name = '';

			if ( !empty( $user_info['first_name'] ) ) {
				$name = $user_info['first_name'];
			}

			if ( !empty( $user_info['last_name'] ) ) {
				$name .= ' '.$user_info['last_name'];
			}

			if ( !empty(  $user_info['email'] ) ) {
				$email = $user_info['email'];

				// Everything fine , add user to list
				$this->add_user_to_sendy( $sendy_url, $sendy_list_id, $name, $email );

			}

		}

	}

	/**
	 * Add user to sendy
	 *
	 * @author Aman Saini
	 * @since  1.0
	 * @param  $sendy_url Sendy Installation url
	 * @param  $list_id   Sendy list id
	 * @param  $name      Subscriber Name
	 * @param  $email     Subscriber Email
	 */
	function add_user_to_sendy( $sendy_url, $list_id, $name, $email ) {

		$sendy_url = untrailingslashit( $sendy_url ).'/subscribe';

		$response= wp_remote_post( $sendy_url, array( 'body' => array( 'name' => $name, 'email' => $email, 'list' => $list_id, 'boolean' => 'true' ) ) );

	}

}

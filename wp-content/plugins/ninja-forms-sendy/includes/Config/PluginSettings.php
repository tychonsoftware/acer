<?php if ( ! defined( 'ABSPATH' ) ) exit;
return apply_filters( 'nf_sendy_plugin_settings', array(
		/*
	|--------------------------------------------------------------------------
	| API Key
	|--------------------------------------------------------------------------
	*/
		'nf_sendy_url' => array(
			'id'    => 'nf_sendy_url',
			'type'  => 'textbox',
			'label' => __( 'Sendy Url', 'nf-sendy' ),
			'desc'  =>  __( 'Enter the url where you have Sendy App installed e.g http://sendy.mysite.in/', 'nf-sendy' )

		),
		// 'nf_sendy_api' => array(
		// 	'id'    => 'nf_sendy_api',
		// 	'type'  => 'textbox',
		// 	'label' => __( 'API Key', 'nf-sendy' ),
		// 	'desc'  =>  __( 'Enter the sendy API key. API key is visible on your Sendy dashboard', 'nf-sendy' )

		// ),
		'nf_sendy_custom_fields' => array(
			'id'    => 'nf_sendy_custom_fields',
			'type'  => 'textbox',
			'label' => __( 'Custom Fields', 'nf-sendy' ),
			'desc'  =>  __( 'Add the Custom fields name as in Personalization tag separated by comma e.g Birthday,Company,Age', 'nf-sendy' )

		),
		/*
	|--------------------------------------------------------------------------
	| Disable SSL Verification
	|--------------------------------------------------------------------------
	*/
		'nf_sendy_disable_ssl_verify' => array(
			'id'    => 'nf_sendy_disable_ssl_verify',
			'type'  => 'checkbox',
			'label' => __( 'Disable SSL Verification', 'nf-sendy' ),
			'desc'  => __( 'If you receive an error about validating the SSL certificate, enable this option', 'nf-sendy' )
		),
	) );

<?php if ( ! defined( 'ABSPATH' ) ) exit;
return apply_filters( 'nf_sendy_plugin_settings_groups', array(
		'sendy' => array(
			'id' => 'sendy',
			'label' => __( 'Sendy', 'nf-sendy' ),
		),
	) );

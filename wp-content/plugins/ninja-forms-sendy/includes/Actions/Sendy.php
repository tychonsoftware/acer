<?php if ( ! defined( 'ABSPATH' ) || ! class_exists( 'NF_Abstracts_Action' ) ) exit;

/**
 * Class NF_Action_Sendy
 */
final class NF_Sendy_Actions_Sendy extends NF_Abstracts_Action
{
	/**
	 *
	 *
	 * @var string
	 */
	protected $_name  = 'sendy';

	/**
	 *
	 *
	 * @var array
	 */
	protected $_tags = array( 'newsletter' );

	/**
	 *
	 *
	 * @var string
	 */
	protected $_timing = 'normal';

	/**
	 *
	 *
	 * @var int
	 */
	protected $_priority = '10';



	protected $_transient = '';

	protected $_transient_expiration = '';

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		$this->_nicename = __( 'Sendy', 'nf-sendy' );
		$this->get_setting_fields();

	}


	public function process( $action_settings, $form_id, $data ) {
		// Optin check
		if ( ! $this->is_opt_in( $data ) ) return $data;

		// Required Fields  List ID, Email & Name
		$listid = isset( $action_settings['sendy_list'] )?$action_settings['sendy_list']: '' ;
		$email = isset( $action_settings['email'] )?$action_settings['email']: '' ;

		if ( !empty( $listid ) && !empty( $email ) ) {
			$name = isset( $action_settings['contactname'] )?$action_settings['contactname']: '' ;
			$subscriber = array();
			$subscriber['email'] = $email;
			$subscriber['name'] = $name;
			$subscriber['list'] =$listid;

			// check for custom fields
			foreach ( $action_settings as $key => $value ) {
				if ( strpos( $key, $this->_name.'_custom_' ) !== false && !empty( $value ) ) {
					$field_id = trim( str_replace( $this->_name.'_custom_', '', $key ) );
					$subscriber[$field_id]= $value;
				}
			}

			$repsonse = NF_Sendy()->subscribe( $subscriber );
		}

		return $data;
	}

	protected function is_opt_in( $data ) {
		$opt_in = TRUE;
		foreach ( $data[ 'fields' ]as $field ) {

			if ( 'sendy-optin' != $field[ 'type' ] ) continue;

			if ( ! $field[ 'value' ] ) $opt_in = FALSE;
		}
		return $opt_in;
	}


	function get_default_fields() {

		$dfields = array( array(
				'name' => 'email',
				'type' => 'textbox',
				'label' => 'Email <small style="color:red">(required)</small>',
				'use_merge_tags' => array(
					'exclude' => array(
						'system', 'querystrings'
					)
				),
			),
			array(
				'name' => 'contactname',
				'type' => 'textbox',
				'label' => 'Name',
				'use_merge_tags' => array(
					'exclude' => array(
						'system', 'querystrings'
					)
				),
			),

		);

		return $dfields;
	}


	private function get_setting_fields() {


		$this->_settings[ 'sendy_list' ] = array(
			'name'    => 'sendy_list',
			'type'    => 'textbox',
			'width' => 'full',
			'label'   => __( 'List ID', 'nf-sendy' ),
			'group'   => 'primary',
		);

		// Default Fields - Name & Email
		$dfields = $this->get_default_fields();

		$this->_settings[ $this->_name.'_default_fieldset' ] = array(
			'name'     => $this->_name.'_default_fieldset',
			'label'    => __( 'Default Fields Mapping', 'nf-sendy' ),
			'type'     => 'fieldset',
			'group'    => 'primary',
			'settings' => $dfields
		);


		// Custom Fields
		$customfields=array();
		$cfields = trim( Ninja_Forms()->get_setting( 'nf_sendy_custom_fields' ) );
		if ( !empty( $cfields ) ) {
			$cfields = explode( ',', $cfields );
			foreach ( $cfields as $cfieldname ) {
				$customfields[] = array(
					'name'  => $this->_name.'_custom_'.$cfieldname,
					'type'  => 'textbox',
					'label' => $cfieldname,
					'use_merge_tags' => array(
						'exclude' => array(
							'system', 'querystrings'
						)
					),
				);
			}


			$this->_settings[ $this->_name.'_customfield_fieldset' ] = array(
				'name'     => $this->_name.'_customfield_fieldset',
				'label'    => __( 'Custom Fields Mapping', 'nf-sendy' ),
				'type'     => 'fieldset',
				'group'    => 'primary',
				'settings' => $customfields,
			);

		}


	}

}

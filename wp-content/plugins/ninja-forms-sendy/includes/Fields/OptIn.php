<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class NF_Field_OptIn
 */
class NF_Sendy_Fields_OptIn extends NF_Abstracts_FieldOptIn
{
	protected $_name = 'sendy-optin';

	protected $_section = 'common';

	protected $_type = 'sendy-optin';

	protected $_templates = 'checkbox';

	public function __construct() {
		parent::__construct();

		$this->_nicename = __( 'Sendy OptIn', 'nf-sendy' );
	}
}

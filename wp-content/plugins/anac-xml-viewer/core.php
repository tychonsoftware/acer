<?php

add_shortcode('anac-xml', function($atts) {
    extract(shortcode_atts(array('id' => 'all'), $atts));
    ob_start();
    anacxmlviewer_generatabella($id);
    $anacxmlshortcode = ob_get_clean();
    return $anacxmlshortcode;
} );

add_shortcode('wpgov-xmlviewer', function($atts) {
    extract(shortcode_atts(array('id' => 'all'), $atts));
    ob_start();
    anacxmlviewer_generatabella($id);
    $anacxmlshortcode = ob_get_clean();
    return $anacxmlshortcode;
} );

add_filter('user_can_richedit', function($default) {
    global $post;
    if ('anac-xml-view' == get_post_type($post)) {
        add_filter('quicktags_settings', 'anacxmlviewer_47010');
        wp_deregister_script('postbox');
        remove_action('media_buttons', 'media_buttons');
        return false;
    }
    return $default;
});

function anacxmlviewer_47010($qtInit) {
    $qtInit['buttons'] = 'fullscreen';
    return $qtInit;
}
add_action('add_meta_boxes', function() {
    add_meta_box('anacxmlviewer_info', ' ', 'anacxmlviewer_metabox_callback', 'anac-xml-view', 'side', 'high');
});

function anacxmlviewer_metabox_callback ($post, $metabox) {
    global $post;
    echo '<center><a href="https://wpgov.it" target="_blank" title="WordPress per la Pubblica Amministrazione">
        <img src="' . plugins_url('wpg.png', __FILE__) . '" ></a>
        <br><br><small>&copy; 2015-2018 <strong>Marco Milesi</strong></small>
        </center>
        </div>
        </div>
        <div id="anacxmlviewer_copyright" class="postbox ">
        <div class="inside">
        <center>Puoi visualizzare la tabella con<br><br><code>[anac-xml id="' . $post->ID . '"]</code></center>
    <script>
        jQuery("#anacxmlviewer_copyright h3.hndle").each(function(e){
            jQuery(this).attr("class", "hndlle");
            $("#anacxmlviewer_copyright.handlediv").hide();
        });
    </script>
    <style>.wp-editor-area {max-height:300px;}#screen-options-link-wrap{display:none;}</style>';
}

function add_anacxmlviewer_table_box() {
    add_meta_box('anacxmlviewer_table', 'Anteprima tabella', 'anacxmlviewer_table_callback', 'anac-xml-view', 'normal', 'default');
}
function anacxmlviewer_table_callback($post, $metabox) {
    anacxmlviewer_generatabella($post->ID);
}

function anacxmlviewer_generatabella($id) {
    remove_filter('the_content', 'wpautop');
    if (get_post_field('post_content', $id) == null) {
        return;
    }
    $time_start = microtime(true);

    //$xml_file = $_SERVER['DOCUMENT_ROOT'] . parse_url(stripslashes(get_post_field('post_content', $id)), PHP_URL_PATH);

    if ( substr( get_post_field('post_content', $id), 0, 4 ) === "http" ) {
        $gare_xml = new SimpleXMLElement( get_post_field('post_content', $id), LIBXML_NOCDATA, true);
    } else {
        $content = stripslashes(get_post_field('post_content', $id));
        $content = str_replace('&','&amp;',$content);

        $content = preg_replace('/(\x{0004}(?:\x{201A}|\x{FFFD})(?:\x{0003}|\x{0004}).)/u', '', $content);

        $regex = '/(
            [\xC0-\xC1] # Invalid UTF-8 Bytes
            | [\xF5-\xFF] # Invalid UTF-8 Bytes
            | \xE0[\x80-\x9F] # Overlong encoding of prior code point
            | \xF0[\x80-\x8F] # Overlong encoding of prior code point
            | [\xC2-\xDF](?![\x80-\xBF]) # Invalid UTF-8 Sequence Start
            | [\xE0-\xEF](?![\x80-\xBF]{2}) # Invalid UTF-8 Sequence Start
            | [\xF0-\xF4](?![\x80-\xBF]{3}) # Invalid UTF-8 Sequence Start
            | (?<=[\x0-\x7F\xF5-\xFF])[\x80-\xBF] # Invalid UTF-8 Sequence Middle
            | (?<![\xC2-\xDF]|[\xE0-\xEF]|[\xE0-\xEF][\x80-\xBF]|[\xF0-\xF4]|[\xF0-\xF4][\x80-\xBF]|[\xF0-\xF4][\x80-\xBF]{2})[\x80-\xBF] # Overlong Sequence
            | (?<=[\xE0-\xEF])[\x80-\xBF](?![\x80-\xBF]) # Short 3 byte sequence
            | (?<=[\xF0-\xF4])[\x80-\xBF](?![\x80-\xBF]{2}) # Short 4 byte sequence
            | (?<=[\xF0-\xF4][\x80-\xBF])[\x80-\xBF](?![\x80-\xBF]) # Short 4 byte sequence (2)
        )/x';
        $content = preg_replace($regex, '', $content);
        $content = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $content);
        $gare_xml = new SimpleXMLElement($content);
    }

    echo '<script type="text/javascript" src="'.plugin_dir_url(__FILE__).'includes/excellentexport.min.js"></script>';

    echo '<strong>' . $gare_xml->metadata->entePubblicatore . '</strong><br><small>Aggiornato al ' . date("d.m.Y", strtotime($gare_xml->metadata->dataUltimoAggiornamentoDataset)) . '
        <br>URL originale: <a href="' . $gare_xml->metadata->urlFile . '" target="_blank">' . $gare_xml->metadata->urlFile . '</a></small><br>
         
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <link rel="stylesheet" 
        href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
        <script type="text/javascript" 
        src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<style>
div#gare_filter {
    display: inline-grid;
    width: 90%;
}
#gare_length label {
    display: flex;
    flex-direction: column;
    gap: 2px;
    align-items: center;
}
div#gare_filter label {
    text-align: left;
}
.dataTables_wrapper{
    max-width:60% !important;
}
.scarica-in-btns {
    display: flex;
    gap: 5px;
    justify-content: end;
}

#gare_length, #gare_filter {
    margin: 5px 0;
}
#gare_length label {
   
    gap: 5px;
    align-items: center;
}
#gare_wrapper {
    max-width: 98% !important;
}
input#gare-serach {
    height: 48px;
}
#gare tbody tr:not(.height-auto) {
    height: 216px;
    vertical-align: baseline;
}
#gare tbody tr:not(.height-auto) td {
    padding-top: 20px;
}
.detail-row {
    position: absolute;
    width: 100%;
    left: 0;
    border-top: 1px solid rgba(0,0,0,.1);
    display: -webkit-inline-box;
}
.first-col {
    min-width: 122px;
    width: calc(9% - 2px);
}
.last-col {
    padding: 8px 10px;
    width: calc(91% + 2px);
    max-width: calc(100% - 147px);
    background-color: #fff;
    margin-left: 5px;
    height: calc(100% - 24px);
    display: flex;
}

#gare tbody tr.even:not(.height-auto) .last-col {
    background-color: #f2f2f2;
}

</style>



<table class="widefat data-table" id="gare">
    <thead>
        <tr>
            <td colspan="5">
                Bandi di gara - <strong>'.$gare_xml->metadata->annoRiferimento.'</strong><br/>
                
            </td>
        </tr>
        <tr>
            <th class="row-title">CIG</th>
            <th>Oggetto</th>
            <th>Importo aggiudicazione</th>
            <th>Importo somme liquidate</th>
            <th>Data inizio<br>Data fine</th>
        </tr>
    </thead>
    <tbody>';

    $tot_agg = 0.00;
    $tot_liq = 0.00;
    $tot_lotti = 0;

    $startPage = isset($_GET['cpage']) ? $_GET['cpage']  :1;
    $perPage = 10;
    $fromPage = $perPage * ($startPage - 1);
    $currentRecord = 0;
    $totalCount = count($gare_xml->xpath('//lotto'));
    $pages = ceil($totalCount / $perPage);
    $oggettoDatas = [];
    // $XmlData = array_slice($gare_xml->xpath('//lotto'),$fromPage,$perPage);
    $XmlData=$gare_xml->xpath('//lotto');
    foreach ($XmlData as $lotto) {


        $tot_agg += (double)$lotto->importoAggiudicazione;
        $tot_liq += (double)$lotto->importoSommeLiquidate;
        $tot_lotti++;

        if ($a == '') {
            $a = ' class="alternate"';
        } else {
            $a = '';
        }

        if ( $lotto->tempiCompletamento->dataInizio ) {
            $dataInizio =  date("d/m/Y", strtotime($lotto->tempiCompletamento->dataInizio));
        } else { $dataInizio = 'N.D.'; }
        if ( $lotto->tempiCompletamento->dataUltimazione ) {
            $dataUltimazione =  date("d/m/Y", strtotime($lotto->tempiCompletamento->dataUltimazione));
        } else { $dataUltimazione = 'N.D.'; }

        echo '<tr' . $a . '>
            <td class="row-title"><label for="tablecell">' . $lotto->cig . '</label></td>
            <td>' . $lotto->oggetto . '</td>
            <td> ' . number_format((double)$lotto->importoAggiudicazione, 2, ',', '.') . '</td>
            <td> ' . number_format((double)$lotto->importoSommeLiquidate, 2, ',', '.') . '</td>
            <td>' . $dataInizio . '<br>' . $dataUltimazione . '<br>
           <div class="detail-row"><div class="first-col"></div><div class="last-col"><small>
            ' . $lotto->sceltaContraente . '<br>Partecipanti:<br>';
        $oggettoData = '';

        foreach ($lotto->partecipanti->partecipante as $partecipante) {
            $oggettoData = $oggettoData . $partecipante->ragioneSociale . ' (' . $partecipante->codiceFiscale . ')<br>';
        }
        $oggettoData = $oggettoData . 'Aggiudicatari:<br>';
        foreach ($lotto->aggiudicatari->aggiudicatario as $aggiudicatario) {
            $oggettoData = $oggettoData . $aggiudicatario->ragioneSociale . ' (' . $aggiudicatario->codiceFiscale . ')<br>';
        }
        $oggettoData = $oggettoData .
            '</small></div></div>
            </td></tr>';

        echo $oggettoData;


//        $oggettoData = '<tr><td></td><td colspan="4"><small>' . $lotto->sceltaContraente . '<br>Partecipanti:<br>';
//        foreach ($lotto->partecipanti->partecipante as $partecipante) {
//            $oggettoData = $oggettoData . $partecipante->ragioneSociale . ' (' . $partecipante->codiceFiscale . ')<br>';
//        }
//        $oggettoData = $oggettoData . 'Aggiudicatari:<br>';
//        foreach ($lotto->aggiudicatari->aggiudicatario as $aggiudicatario) {
//            $oggettoData = $oggettoData . $aggiudicatario->ragioneSociale . ' (' . $aggiudicatario->codiceFiscale . ')<br>';
//        }
//        $oggettoData = $oggettoData . '</small></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td><td style="display:none;"></td></tr>';
        array_push($oggettoDatas, $oggettoData);
    }

    $customPagHTML     = '<div style="text-align:center">'.paginate_links( array(
            'base' => add_query_arg( 'cpage', '%#%' ),
            'format' => '',
            'prev_text' =>'prec',
            'next_text' =>'succ',
            'total' =>  $pages,
            'current' => $startPage
        )).'</div>';


    echo '<tfoot>
              <tr>
                <td>Totali</td>
                <td>Numero Lotti: <strong>'. number_format((double)$tot_lotti, 0, ',', '.') . '</strong></td>
                <td>'. number_format((double)$tot_agg, 2, ',', '.') . '</td>
                <td>'. number_format((double)$tot_liq, 2, ',', '.') . '</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="2">
                   &nbsp;
                </td>
                <td colspan="3" style="text-align:right;">';

    echo 'Scarica in <div class="scarica-in-btns"><a href="' . $gare_xml->metadata->urlFile . '" target="_blank" title="File .xml"><button>XML</button></a>
            <a download="' . get_bloginfo('name') . '-gare' . $anno . '.xls" href="#" onclick="return ExcellentExport.excel(this, \'gare\', \'Gare\');"><button>EXCEL</button></a>
            <a download="' . get_bloginfo('name') . '-gare' . $anno . '.csv" href="#" onclick="return ExcellentExport.csv(this, \'gare\');"><button>CSV</button></a></div>';

    echo '</td></tr>
            </tfoot>';

    echo '</tbody></table>';
    //echo $customPagHTML;
    echo '<div class="clear"></div>';

    echo '<script>
    jQuery(document).ready(function(){
      jQuery("#gare").dataTable({
                "ordering":false,
                "language": {
                    "info":           "Da _START_ a _END_ di _TOTAL_ elementi",
                    "lengthMenu":     "Mostra _MENU_ elementi",
                    "search":         "Ricerca:",
                    "paginate": {
                        "previous": "prec",
                        "next": "succ"
                    }
                },
            });

        jQuery("#gare_filter input").attr("placeholder", "Cerca..");
        jQuery("#gare_filter input").attr("style", "display:table-cell; width:100%");
         $("#gare-serach").keyup(function(){
            var oTable = $("#gare").DataTable();
            oTable.search($(this).val()).draw();
        });
         
         jQuery("#gare_paginate").bind("DOMSubtreeModified", function () {
              jQuery("#gare tbody tr").has(".dataTables_empty").addClass("height-auto");
              setHeightRows();
        });
        setHeightRows();
    });
    function setHeightRows() {
        jQuery("#gare tbody tr").each(function () {
            var height = ($(this).find("td .detail-row small").html().split("<br>").length * 22);
            $(this).find("td .detail-row").height(height);
            $(this).find("td .detail-row small").height(height);
            $(this).height(height+65);
        });
    }
</script>';
    ?>
    <script>

        (function(document) {
            'use strict';

            var LightTableFilter = (function(Arr) {

                var _input;

                function _onInputEvent(e) {
                    _input = e.target;
                    var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                    Arr.forEach.call(tables, function(table) {
                        Arr.forEach.call(table.tBodies, function(tbody) {
                            Arr.forEach.call(tbody.rows, _filter);
                        });
                    });
                }

                function _filter(row) {
                    var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                    row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                }

                return {
                    init: function() {
                        var inputs = document.getElementsByClassName('light-table-filter');
                        Arr.forEach.call(inputs, function(input) {
                            input.oninput = _onInputEvent;
                        });
                    }
                };
            })(Array.prototype);

            document.addEventListener('readystatechange', function() {
                if (document.readyState === 'complete') {
                    LightTableFilter.init();
                }
            });

        })(document);

    </script>
    <?php
}
add_action('template_redirect', 'anacxmlviewer_template');
function anacxmlviewer_template() {
    global $wp, $wp_query;
    if (isset($wp->query_vars['post_type']) && $wp->query_vars['post_type'] == 'anac-xml-view') {
        if (have_posts()) {
            add_filter('the_content', 'anacxmlviewer_template_filter');
        } else {
            $wp_query->is_404 = true;
        }
    }
}
function anacxmlviewer_template_filter($content) {
    global $wp_query;
    ob_start();
    anacxmlviewer_generatabella($wp_query->post->ID);
    $anacxmlshortcode = ob_get_clean();
    return $anacxmlshortcode;
}
?>

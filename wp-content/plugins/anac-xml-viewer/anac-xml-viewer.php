<?php
/*
Plugin Name: ANAC XML Viewer
Plugin URI: https://wordpress.org/plugins/anac-xml-viewer/
Description: Visualizzatore XML per file generati da applicativi esterni
Author: Marco Milesi
Version: 1.6.1
Author URI: https://marcomilesi.com
*/

add_action( 'init', 'register_cpt_anacimporter' );

function register_cpt_anacimporter() {

    $labels = array(
        'name' => _x( 'Visualizzatore dataset XML Anac', 'avcp' ),
        'singular_name' => _x( 'Dataset XML', 'avcp' ),
        'add_new' => _x( 'Nuovo Dataset', 'avcp' ),
        'add_new_item' => _x( 'Importa Nuovo Dataset', 'avcp' ),
        'edit_item' => _x( 'Modifica Dataset', 'avcp' ),
        'new_item' => _x( 'Nuovo Dataset', 'avcp' ),
        'view_item' => _x( 'Vedi Dataset', 'avcp' ),
        'search_items' => _x( 'Cerca Dataset', 'avcp' ),
        'not_found' => _x( 'Nessuna voce trovata', 'avcp' ),
        'not_found_in_trash' => _x( 'Nessuna voce trovata', 'avcp' ),
        'parent_item_colon' => _x( 'Parent:', 'avcp' ),
        'menu_name' => _x( 'Anac XML', 'avcp' ),
    );

    if ( function_exists( 'register_cpt_avcp' ) ) {
       $showinmenu = 'edit.php?post_type=avcp';
    } else {
        $showinmenu = true;
    }

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Gare AVCP',
        'supports' => array( 'title', 'editor'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => $showinmenu,
        'menu_position' => 37,
        'menu_icon'    => 'dashicons-list-view',
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => false,
        'rewrite' => false
    );

    register_post_type( 'anac-xml-view', $args );
}
require_once(plugin_dir_path(__FILE__) . 'core.php');

function axv_notices() {

    global $current_screen;

    if ( 'anac-xml-view' == $current_screen->post_type  ) {
        echo '
        <div class="notice">
            <p>Puoi definire un file XML copiando e incollando l\'intero contenuto o l\'URL completo. Info: <a href="https://youtu.be/cdn082kZogk" target="_blank">youtu.be/cdn082kZogk</a></p>
        </div>';
    }

} add_action( 'admin_notices', 'axv_notices' );

function axv_columns($columns) {
    $columns['atype'] = 'Dettagli';
    unset($columns['date']);
    $columns['date'] = 'Data';
    unset($columns['cb']);
    return $columns;
}
add_filter('manage_edit-anac-xml-view_columns', 'axv_columns');

add_filter('bulk_actions-edit-anac-xml-view', '__return_empty_array');

add_action('manage_anac-xml-view_posts_custom_column', 'axv_manage_columns', 10, 2);
function axv_manage_columns($column, $post_id)
{
    global $post;
    switch ($column) {
        case 'atype':
            if ( substr( get_post_field('post_content', $post_id), 0, 4 ) === "http" ) {
                printf(  '<a target="_blank" href="'.get_post_field('post_content', $post_id ).'">'.get_post_field('post_content', $post_id ).'</a><br>' );
            }
            printf( 'Shortcode: [anac-xml id="' . $post_id . '"]');
            break;
        default:
            break;
    }
}
?>

<?php if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class NF_Action_SuccessMessage
 */
final class NF_Actions_SuccessMessage extends NF_Abstracts_Action
{
    /**
    * @var string
    */
    protected $_name  = 'successmessage';

    /**
    * @var array
    */
    protected $_tags = array();

    /**
    * @var string
    */
    protected $_timing = 'late';

    /**
    * @var int
    */
    protected $_priority = 10;

    /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();

        $this->_nicename = __( 'Success Message', 'ninja-forms' );

        $settings = Ninja_Forms::config( 'ActionSuccessMessageSettings' );

        $this->_settings = array_merge( $this->_settings, $settings );

        add_action( 'nf_before_import_form', array( $this, 'import_form_action_success_message' ), 11 );
    }

    /*
    * PUBLIC METHODS
    */

    public function save( $action_settings )
    {

    }

    public function process( $action_settings, $form_id, $data )
    {
       
        if( isset( $action_settings[ 'success_msg' ] ) ) {

            if( ! isset( $data[ 'actions' ] ) || ! isset( $data[ 'actions' ][ 'success_message' ] ) ) {
                $data[ 'actions' ][ 'success_message' ] = '';
            }

            global $wpdb;
            preg_match_all("/{([^}]*)}/", $action_settings[ 'success_msg'], $matches );
            if( !empty( $matches[0]) ){
                $sub_id = ( isset( $data[ 'actions' ][ 'save' ] ) ) ? $data[ 'actions' ][ 'save' ][ 'sub_id' ] : null;
                foreach ($matches[0] as $key => $value) {
                    $field = str_replace( '{field:', '', $value );
                    $field = str_replace( '}', '', $field );
                    $field_id = $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = '$field' AND `parent_id` = {$form_id}" );
                    $meta_value = get_post_meta( $sub_id, '_field_' . $field_id , true  );
                    error_log("Field is :::::: " . print_r($field, true));
                    error_log("Field Id is :::::: " . print_r($field_id, true));
                    error_log("meta_value is :::::: " . print_r($meta_value, true));

                    if(strcmp($field,'protocolnumber') == 0)
                    {
                        $pdf_file_name = apply_filters( 'ninja_forms_submission_pdf_name', $data['settings']['title'] . '-' . $sub_id, $sub_id ) . '.pdf';
                        error_log("pdf_file_name is :::::: " . print_r($pdf_file_name, true));
                        $pdf_file_path_array = uf_get_documents_by_name($pdf_file_name);
                        //error_log("pdf_file_path_array is :::::: " . print_r($pdf_file_path_array, true));
                        if(sizeof($pdf_file_path_array) > 0){
                            $pdf_file_path = $pdf_file_path_array[0]->Path;
                            error_log("pdf_file_path is :::::: " . print_r($pdf_file_path, true));
                            $link = '<a class="vfpdf" href="' . UF_URL .'DowloadDocument.php?action='. $pdf_file_path .'" title="Cliccare per visualizzare il documento">' . $meta_value . '</a>';
                            $meta_value = $link;
                            error_log("Updated meta value is  :::::: " . print_r($meta_value, true));
                        }
                    }   
                    $action_settings[ 'success_msg' ] = str_replace( $value,$meta_value , $action_settings[ 'success_msg' ]);
                }
            }

            ob_start();
            do_shortcode( $action_settings['success_msg'] );
            $ob = ob_get_clean();

            if( $ob ) {
                $data[ 'debug' ][ 'console' ][] = sprintf( __( 'Shortcodes should return and not echo, see: %s', 'ninja-forms' ), 'https://codex.wordpress.org/Shortcode_API#Output' );
                $data['actions']['success_message'] .= $action_settings['success_msg'];
            } else {
                $message = do_shortcode( $action_settings['success_msg'] );
                $data['actions']['success_message'] .= wpautop( $message );
            }
        }

        return $data;
    }

    public function import_form_action_success_message( $import )
    {
        if( ! isset( $import[ 'actions' ] ) ) return $import;

        foreach( $import[ 'actions' ] as &$action ){

            if( 'success_message' == $action[ 'type' ] ){

                $action[ 'type' ] = 'successmessage';
            }
        }

        return $import;
    }
}

<?php

require 'lib/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InvoiceSearchExportExcel {

    public function exportData() {
        $date         = date( "ymd", time() );
        $randomString = bin2hex( random_bytes( 20 ) );
        $fileDir      = 'export' . DIRECTORY_SEPARATOR . 'exported' . DIRECTORY_SEPARATOR . 'excel' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $randomString;
        $fileName     = 'Tavola per gli accertamenti.xlsx';
        wp_mkdir_p( plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir );
        $file = plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir . DIRECTORY_SEPARATOR . $fileName;
        $this->getExportContent($file);

        echo plugins_url( $fileDir . DIRECTORY_SEPARATOR . $fileName, __FILE__ );

        wp_die();


    }

    private function getExportContent($file) {
        \PhpOffice\PhpSpreadsheet\Settings::setLocale('it_it');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $arrayData = $this->getDataFromDb();
        $sheet->fromArray(
                        $arrayData,
                        NULL,
                        'A1'
                    );

         $n = count($arrayData);
         $sheet->setCellValue('L'.($n+1), "=SUM(L2:L$n)");

        $sheet->getStyle('L')->getNumberFormat()->setFormatCode('0.00');
        $sheet->getStyle('A')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
        $sheet->getStyle('D')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

        foreach($sheet->getColumnDimensions() as $column_dimension) {
            $column_dimension->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->setPreCalculateFormulas(false);
        $writer->save($file);
    }

    private function getDataFromDb() {
        $request = getInvoiceSearchRequest();
		$GLOBALS['uploadYear'] = $request['uploadYear'];
        $items = rf_get_fattura_export_excel();
        $items2 = rf_get_fattura_export_excel_second_data();
        $items3 = rf_get_fattura_export_excel_third_data();
        $result = [];   

        if (!$items && !$items3 && !$items2) {
            return $result;
        }

        $dbFields = ['Titolare', 'Cespite', 'Ambito', 'Voce', 'Categoria', 'Descrizione','Capitolo', 'Natura', 'Stato SDI', 'Totale'];

        $result[] = array_merge(['Dal', 'Al'], $dbFields);
        $dal = $_POST['data']['DataInizio'] ?? '';
        $al = $_POST['data']['DataFine'] ?? '';

        while ($row = $items->fetch_assoc()) {
            $result[] = array_merge([$dal, $al], array_map(function ($col) use ($row) {
                return $row[$col];
            }, $dbFields));
        }

        while ($row = $items2->fetch_assoc()) {
            $result[] = array_merge([$dal, $al], array_map(function ($col) use ($row) {
                return $row[$col];
            }, $dbFields));
        }

        while ($row = $items3->fetch_assoc()) {
            $result[] = array_merge([$dal, $al], array_map(function ($col) use ($row) {
                return $row[$col];
            }, $dbFields));
        }

        return $result;
    }
}
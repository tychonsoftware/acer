<?php
/**
 * Functions necessary to the plugin
 * @package    Invoice Search
 */
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }
################################################################################
// Funzioni 
################################################################################

$GLOBALS['fatturaDBConnect'] = false;

/**
 * @return mysqli
 */
function fatturaDBConnect() {
    global $uploadYear;
	global $fatturaDBConnect;

	if ($fatturaDBConnect !== false) {
		return $fatturaDBConnect;
	}
	$serverName = UPLOAD_DB_HOST;
	$isuploaddbconnected = false;
	$fatturaDBConnect = null;
	if(isset($serverName) && ($serverName != null)){
        if(!empty($uploadYear)){
            $yDB = '_'.$uploadYear;
        }else{
            $yDB ='';
        }
		try {
			$fatturaDBConnect = new mysqli($serverName,UPLOAD_DB_USER,UPLOAD_DB_PASSWORD,UPLOAD_DB_NAME.$yDB);
            if ($fatturaDBConnect -> connect_errno) {
                //wp_redirect('ricerca-fatture?err=1&db='.$yDB.'');die;
                //wp_redirect($_SERVER['HTTP_REFERER'] . '?err=1&db='.$uploadYear.'');die;
				$isuploaddbconnected = false;
			}else {
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
	}
	if($isuploaddbconnected === true){
		return $fatturaDBConnect;
	}
	return null;
}

function rf_get_fattura($DaRiga=0,$ARiga=20) {
    global $wpdb;

    if ($DaRiga==0 AND $ARiga==0)
		$Limite="";
	else
        $Limite=" Limit ".$DaRiga.",".$ARiga;

    $where = array_merge([], getWhereCondFromSearch());

    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    $extraJoinsFromQuery = getExtraJoinsFromQuery();

    if($mysqli = fatturaDBConnect()){
        $sql = <<<SQL
SELECT SQL_CALC_FOUND_ROWS f.id, f.errore, f.messaggio_esito_csa, f.lavorazione_scarto,
fdb.numero_fattura,fdh.denominazione_cesCom,fdh.cognome_cesCom,
fdh.nome_cesCom,fdh.codiceFiscale_cesCom,
fdb.data_fattura,
fdb.importo_totale,f.stato,
f.csa_path,f.nome_file_xml_csa,f.dipartimento,fdh.idCodice_cesCom,fdh.rif_ammin_ced_prest
FROM $wpdb->table_name_Fattura f
JOIN $wpdb->table_name_Fattura_Dati_Header fdh ON f.id = fdh.fattura_id
JOIN $wpdb->table_name_Fattura_Dati_Body fdb ON f.id = fdb.fattura_id
$extraJoinsFromQuery
$Selezione
ORDER BY 
    fdb.numero_fattura, 
    fdb.data_fattura
$Limite
SQL;
        // echo '<pre>' . $sql . '</pre>';
        error_log("SQL : " . $sql);

        return [
            $mysqli->query($sql),
            $mysqli->query('SELECT FOUND_ROWS()'),
        ];
    }
    
    
    return null;	
}

function rf_get_fattura_export($isShowDaySummary = true) {
	global $wpdb;
    global $uploadYear;
    $where = array_merge(['f.stato IN (20, 31, 32, 33, 34,35)'], getWhereCondFromSearch());

    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

	if($mysqli = fatturaDBConnect()){
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
	    if ($isShowDaySummary) {
            $orderBy = "ORDER BY fdb.data_fattura, fdb.numero_fattura";
        } else {
            $orderBy = "ORDER BY MONTH(fdb.data_fattura), fdb.numero_fattura";
        }

		$sql = "SELECT f.id, fdh.codice_destinatario,fdh.pec_destinatario,progressivo_acer, fdh.rif_ammin_ced_prest,";
		$sql .= "fdb.numero_fattura,fdh.denominazione_cesCom,fdh.cognome_cesCom,fdb.tipologia_documento_id,";
		$sql .= "fdh.nome_cesCom,fdh.codiceFiscale_cesCom,fdh.denominazione_cesCom,";
		$sql .= "fdb.data_fattura, fdh.denominazione_cedPrest,";
		$sql .= "fdh.idCodice_cedPrest, fdb.importo_totale,f.stato,fdb.importo_bollo,";
		$sql .= " f.csa_path,f.nome_file_xml_csa,f.dipartimento,fdh.idCodice_cesCom FROM $wpdb->table_name_Fattura f ";
		$sql .= "JOIN $wpdb->table_name_Fattura_Dati_Header fdh ON f.id = fdh.fattura_id ";
		$sql .= "JOIN $wpdb->table_name_Fattura_Dati_Body fdb ON f.id = fdb.fattura_id";
        $sql .= getExtraJoinsFromQuery();
        $sql .= " $Selezione";
		$sql .= $orderBy;

		return $mysqli->query($sql);
	}
	return null;
}

function rf_get_fattura_export_error_summary() {
    global $wpdb;
    global $uploadYear;
    $where = array_merge(['f.stato IN (33, 34)'], getWhereCondFromSearch());

    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    if($mysqli = fatturaDBConnect()){
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
        $sql = "SELECT progressivo_acer, f.lavorazione_scarto, f.stato, fdh.rif_ammin_ced_prest,";
        $sql .= "fdb.numero_fattura,";
        $sql .= "fdb.data_fattura,";
        $sql .= "fdb.importo_totale FROM $wpdb->table_name_Fattura f ";
        $sql .= "JOIN $wpdb->table_name_Fattura_Dati_Header fdh ON f.id = fdh.fattura_id ";
        $sql .= "JOIN $wpdb->table_name_Fattura_Dati_Body fdb ON f.id = fdb.fattura_id";
        $sql .= getExtraJoinsFromQuery();
        $sql .= " $Selezione";
        $sql .= "ORDER BY fdb.data_fattura";

        return $mysqli->query($sql);
    }
    return null;
}

function rf_get_fattura_export_excel() {
    global $uploadYear;
    $where = getWhereCondFromSearch();

    $request = getInvoiceSearchRequest();
    $dipartimento = $request['Dipartimento'] ?? '';

    if ($dipartimento!= ''){
        $where[] = "fv.dipartimento='" .$dipartimento."'";
    }

	$where[] = "f.stato not in (33,34,40)";
    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    if($mysqli = fatturaDBConnect()){
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
        $sql = <<<SQL
SELECT f.titolare AS "Titolare",
       f.cespite AS "Cespite",
       f.ambito_contrattuale AS "Ambito",
       frb.riferimento_ammm AS "Voce",
       fv.categoria AS "Categoria",
       fv.capitolo AS "Capitolo",
       fv.descrizione AS "Descrizione", 
       CASE
       WHEN frb.tipo_natura_id is not null THEN ftn.codi_xxml
       ELSE frb.aliquota
       END
       AS "Natura",
       fs.descrizione as "Stato SDI",
       sum(frb.importo_totale) AS "Totale"
FROM fattura f
JOIN fattura_righe_beni frb ON f.id=frb.fattura_id
LEFT JOIN fattura_tipo_natura ftn on ftn.id=frb.tipo_natura_id
JOIN fattura_dati_header fh ON fh.fattura_id=f.id
JOIN fattura_voci fv ON trim(frb.riferimento_ammm)=fv.voce
JOIN fattura_state fs on f.stato = fs.stato 
$Selezione
GROUP BY f.titolare,
         f.cespite,
         f.ambito_contrattuale,
         frb.riferimento_ammm,
         fv.categoria,
         fv.capitolo,
         fv.descrizione,
         frb.tipo_natura_id,
         frb.aliquota,
         fs.descrizione;
SQL;
        error_log("SQL 1: " . $sql);
        return $mysqli->query($sql);
    }
    return null;
}

function rf_get_fattura_export_excel_second_data() {
    global $uploadYear;
    $where = getSecondQueryWhereCondFromSearch();

    $request = getInvoiceSearchRequest();
    $dipartimento = $request['Dipartimento'] ?? '';
    $where[] = "trim(riferimento_ammm) not in (select voce from fattura_voci where dipartimento='$dipartimento')";

	$where[] = "f.stato not in (33,34,40)";
    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    if($mysqli = fatturaDBConnect()){
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
        $sql = <<<SQL
        SELECT f.titolare AS "Titolare",
        f.cespite AS "Cespite",
        f.ambito_contrattuale AS "Ambito",
        frb.riferimento_ammm as "Voce",
        "UNKNOWN" AS "Categoria",
        "UNKNOWN" AS "Capitolo",
        "UNKNOWN" AS "Descrizione",
        CASE
        WHEN frb.tipo_natura_id is not null THEN ftn.codi_xxml
        ELSE frb.aliquota
        END
        AS "Natura",
        fs.descrizione as "Stato SDI",
        sum(frb.importo_totale) AS "Totale"
        FROM fattura f
        JOIN fattura_state fs on f.stato = fs.stato
        JOIN fattura_righe_beni frb ON f.id=frb.fattura_id
        LEFT JOIN fattura_tipo_natura ftn on ftn.id=frb.tipo_natura_id
        $Selezione
        GROUP BY f.titolare,
            f.cespite,
            f.ambito_contrattuale,
            frb.riferimento_ammm,
            fs.descrizione,
            frb.tipo_natura_id,
            frb.aliquota;
SQL;
        error_log("SQL 2: " . $sql);
        return $mysqli->query($sql);
    }
    return null;
}

function rf_get_fattura_export_excel_third_data() {
    global $uploadYear;
    $where = getWhereCondFromSearch();

    $where[] = "fdb.importo_bollo is not null";

	$where[] = "f.stato not in (33,34,40)";
    $Selezione = '';

    if (count($where)) {
        $Selezione = " WHERE " . implode(' AND ', $where);
    }

    if($mysqli = fatturaDBConnect()){
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
        $sql = <<<SQL
SELECT f.titolare AS "Titolare",
       f.cespite AS "Cespite",
       f.ambito_contrattuale AS "Ambito",
       "BOLLO" AS "Voce",
       "BOLLO" AS "Categoria",
       "BOLLO" AS "Capitolo",
       "BOLLO" AS "Descrizione",
       "BOLLO" AS "Natura",
       fs.descrizione as "Stato SDI",
       sum(fdb.importo_bollo) AS "Totale"
FROM fattura f
JOIN fattura_dati_body fdb ON f.id=fdb.fattura_id
JOIN fattura_state fs on f.stato = fs.stato
$Selezione
GROUP BY f.titolare,
         f.cespite,
         f.ambito_contrattuale,
         fs.descrizione;
SQL;
        error_log("SQL 3 : " . $sql);
        return $mysqli->query($sql);
    }
    return null;
}

function rf_get_fattura_export_csv() {
        global $wpdb;
        global $uploadYear;
        $where = array_merge([], getWhereCondFromSearch());

        $Selezione = '';

        if (count($where)) {
            $Selezione = " WHERE " . implode(' AND ', $where);
        }

        $extraJoin = getExtraJoinsFromQuery();

        if($mysqli = fatturaDBConnect()){
            $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
            $sql = <<<SQL
SELECT fdb.numero_fattura, fdh.rif_ammin_ced_prest, fs.descrizione as stato, f.stato as stato_code, f.data_esito_csa, f.messaggio_esito_csa,f.nome_file_zip
FROM $wpdb->table_name_Fattura f 
JOIN $wpdb->table_name_Fattura_Dati_Header fdh ON f.id = fdh.fattura_id
JOIN $wpdb->table_name_Fattura_Dati_Body fdb ON f.id = fdb.fattura_id
LEFT JOIN fattura_state fs on f.stato = fs.stato 
$extraJoin 
$Selezione 
SQL;
            return $mysqli->query($sql);
        }

        return null;
    }

function getWhereCondFromSearch() {

    $request = getInvoiceSearchRequest();

    $numerofattura = $request['numerofattura'] ?? '';
    $cliente = $request['cliente'] ?? '';
    $codicsfiscale = $request['codicsfiscale'] ?? '';
    $codicedestinatario = $request['codicedestinatario'] ?? '';
    $Dadata = $request['DataInizio'] ?? '';
    $Adata = $request['DataFine'] ?? '';
    $dipartimento = $request['Dipartimento'] ?? '';
    $sezionale = $request['sezionale'] ?? '';
    $stato = $request['Stato'] ?? [];
    $codiceRiferimento = $request['codice-riferimento'] ?? '';
    $titolare = $request['titolare'] ?? [];
    $cespite = $request['cespite'] ?? [];
    $ambitoContrattuale = $request['ambito_contrattuale'] ?? [];
    $voce = $request['voce'] ?? [];
    $natura = $request['natura'] ?? [];
    $Esitodata = $request['DataEsitoSdiDal'] ?? '';

    $where = [];

    if ($Dadata!=0) {
        $where[] = 'f.data_fattura >="'.rf_convertiData($Dadata).'"';
    }

    if ($Adata!=0) {
        $where[] = 'f.data_fattura <="'.rf_convertiData($Adata).'"';
    }

    
    if ($Esitodata!=0 ){
        $where[] = 'f.data_esito_csa >="'.rf_convertiData($Esitodata).'"';
    }


    $numerofattura = trim($numerofattura);
    if ($numerofattura!= ''){
        $where[] = 'fdb.numero_fattura like "%'.$numerofattura.'%"';
    }

    $cliente = trim($cliente);
    if ($cliente!= ''){
        $where[] = '(fdh.denominazione_cesCom like "%'.$cliente.'%"'
                   . ' OR fdh.nome_cesCom like "%'.$cliente.'%" '
                   . ' OR fdh.cognome_cesCom like "%'.$cliente.'%") ';
    }

    if ($codicsfiscale!= ''){
        $where[] = '(fdh.codiceFiscale_cesCom like "%'.$codicsfiscale.'%" OR fdh.idCodice_cesCom like "%'.$codicsfiscale.'%")';
    }

    if ($codicedestinatario!= ''){
        $where[] = 'fdh.codice_destinatario like "%'.$codicedestinatario.'%"';
    }

    if ($dipartimento!= ''){
        $where[] = "f.dipartimento='" .$dipartimento."'";
    }

    if ($sezionale != ''){
        $where[] = "f.sezionale='" .$sezionale."'";
    }

    if (!empty($stato)) {
        $where[] ='f.stato IN (' . implode(', ', array_map(function ($item) {
                return intval($item);
            }, $stato)) . ')';
    }

    if ($codiceRiferimento) {
        $where[] ='fdh.rif_ammin_ced_prest = "' .$codiceRiferimento . '"';
    }

    if (!empty($titolare)) {
        $where[] ='f.titolare IN (' . implode(', ', array_map(function ($item) {
            return "'$item'";
            }, $titolare)) . ')';
    }

    if (!empty($cespite)) {
        $where[] ='f.cespite IN (' . implode(', ', array_map(function ($item) {
                return "'$item'";
            }, $cespite)) . ')';
    }

    if (!empty($ambitoContrattuale)) {
        $where[] ='f.ambito_contrattuale IN (' . implode(', ', array_map(function ($item) {
                return "'$item'";
            }, $ambitoContrattuale)) . ')';
    }

    if (!empty($voce)) {
        $where[] ='frb.riferimento_ammm IN (' . implode(', ', array_map(function ($item) {
                return "'$item'";
            }, $voce)) . ')';
    }

    if (!empty($natura)) {
        $where[] ='frb.tipo_natura_id IN (' . implode(', ', $natura) . ')';
    }

    return $where;
}

function getSecondQueryWhereCondFromSearch() {

    $request = getInvoiceSearchRequest();

    $Dadata = $request['DataInizio'] ?? '';
    $Adata = $request['DataFine'] ?? '';
    $dipartimento = $request['Dipartimento'] ?? '';
    $sezionale = $request['sezionale'] ?? '';
    $stato = $request['Stato'] ?? [];
    $codiceRiferimento = $request['codice-riferimento'] ?? '';
   

    $where = [];

    if ($Dadata!=0) {
        $where[] = 'f.data_fattura >="'.rf_convertiData($Dadata).'"';
    }

    if ($Adata!=0) {
        $where[] = 'f.data_fattura <="'.rf_convertiData($Adata).'"';
    }

    if ($dipartimento!= ''){
        $where[] = "f.dipartimento='" .$dipartimento."'";
    }

    if ($sezionale != ''){
        $where[] = "f.sezionale='" .$sezionale."'";
    }

    if (!empty($stato)) {
        $where[] ='f.stato IN (' . implode(', ', array_map(function ($item) {
                return intval($item);
            }, $stato)) . ')';
    }
    return $where;
}


/**
 * @return array
 */
function getInvoiceSearchRequest(): array {
    if ( isset( $_POST['action'] ) && in_array($_POST['action'], ['export_invoice_search', 'export_excel_invoice_search', 'export_csv_invoice_search'] ) ) {
        $request = $_POST['data'];
    } else {
        $request = $_REQUEST;
    }

    $request = array_map( 'sanitize', $request );

    return $request;
}

function getExtraJoinsFromQuery() {
    $request = getInvoiceSearchRequest();

    $voce = $request['voce'] ?? [];
    $natura = $request['natura'] ?? [];

    $joins = [];

    if (!empty($voce) || !empty($natura)) {
        $joins[] = " JOIN fattura_righe_beni frb ON f.id = frb.fattura_id ";

    }

    return implode(' ', $joins);
}

function sanitize($value){
    if (is_array($value)) {
        return array_map('sanitize', $value);
    }
    if (is_string($value)) {
        $value = trim($value);
    }
    $mysqli = fatturaDBConnect();
    if($mysqli != null)
        return $mysqli->real_escape_string($value);
    else
       return $value;
}

function rf_GetStatuses(){
    global $uploadYear;
    $serverName = UPLOAD_DB_HOST;
    $isuploaddbconnected = false;
    $mysqli = null;
    if(isset($serverName) && ($serverName != null)){
		try {
            $mysqli = fatturaDBConnect();
			if ($mysqli == null || $mysqli -> connect_errno) {
				$isuploaddbconnected = false;
			}else {
                $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
				$isuploaddbconnected = true;
			}
		} catch (Exception $e) {
			$isuploaddbconnected = false;
		}
    }
    if($isuploaddbconnected === true){
        global $wpdb;
        $sql = "SELECT * FROM " . $wpdb->table_name_Fattura_State ;
        
        return $mysqli->query($sql);
    }

    return null;
}

$rf_GetStatusLabelCache = array();
function rf_GetStatusLabel($statusId) {
    global $rf_GetStatusLabelCache;
    if (!isset($rf_GetStatusLabelCache[$statusId])) {
        $rf_GetStatusLabelCache[$statusId] = null;

        $mysqli = fatturaDBConnect();

        if ($mysqli) {
            global $wpdb;
            $sql = "SELECT descrizione FROM " . $wpdb->table_name_Fattura_State . " WHERE stato=" . $statusId;
            $result = $mysqli->query($sql);

            if ($result->num_rows > 0) {
                $stat_row = $result->fetch_assoc();
                $rf_GetStatusLabelCache[$statusId] = $stat_row['descrizione'];
            }
        }
    }

    return $rf_GetStatusLabelCache[$statusId];
}

function contains($string, $search)
{
    return strpos($string, $search) !== false;
}

function rf_convertiData($dataEur){
    $rsl = explode ('/',$dataEur);
    $rsl = array_reverse($rsl);
    return implode($rsl,'-');
}

add_shortcode('InvoiceSearch', 'VisualizzaInvoiceSearch');
function VisualizzaInvoiceSearch($Parametri)
{
    $ret = "";
    $Parametri = shortcode_atts(array('type' => '1'), $Parametri, "InvoiceSearch");
    require_once(dirname(__FILE__) . '/admin/search.php');
    return $ret;
}

function getDistinctValues($table, $column, $idColumn = false) {
    global $uploadYear;
    $mysqli = fatturaDBConnect();
    if($mysqli != null)
        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
    if ($idColumn) {
        $sql = "SELECT $column, $idColumn FROM $table WHERE $column IS NOT NULL";
    } else {
        $sql = "SELECT distinct($column) FROM $table WHERE $column IS NOT NULL";
    }

    $result = $mysqli->query($sql);
    $r = [];

    if (isset($result->num_rows) && $result->num_rows > 0) {
        foreach ($result->fetch_all(MYSQLI_NUM) as $key => $item) {
            $r[$idColumn ? $item[1] : $key] = $item[0];
        }
    }

    return $r;
}

function getRequestTextValue($field) {
    if ( isset( $_REQUEST[$field] ) ) {
        return trim( $_REQUEST[$field] );
    } else {
        return "";
    }
}

function isCheckedCheckBox($field, $value) {
    if (!isset( $_REQUEST[$field] )) {
        return false;
    }
    if (is_array($_REQUEST[$field])) {
        return in_array($value, $_REQUEST[$field] );
    } else {
        return $_REQUEST[$field] == $value;
    }
}

function invoiceSearchHiddenFieldHasData() {
    $fields = [
        'codice-riferimento',
        'titolare',
        'cespite',
        'ambito_contrattuale',
        'voce',
        'natura',
        'DataEsitoSdiDal',
    ];

    foreach ($fields as $field) {
        if (!empty($_REQUEST[$field])) {
            return true;
        }
    }
    return false;
}
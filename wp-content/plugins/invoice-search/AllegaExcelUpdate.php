<?php

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

require 'lib/vendor/autoload.php';

class AllegaExcelUpdate {
    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    private $spreadsheet;

    public function __construct() {
        $this->initFile();
    }

    /**
     * @return void
     */
    public function import() {
        try {
            $data = $this->getData();
        } catch ( \PhpOffice\PhpSpreadsheet\Exception $e ) {
            return wp_send_json_error([
                'message' => $e->getMessage()
            ]);
        }
        $GLOBALS['uploadYear'] = $_REQUEST['upload_year'];
        $report = $this->processData($data);

        if (!$report['total_errors']) {
            return wp_send_json_success([
                'error' => false,
                'num_success' => $report['total_rows'],
                'total_rows' => $report['total_rows'],
            ]);
        }

        $file = $this->generateErrorReport($report, $data);
        return wp_send_json_success([
            'error' => true,
            'num_error' => $report['total_errors'],
            'num_success' => $report['total_rows'] - $report['total_errors'],
            'total_rows' => $report['total_rows'],
            'error_report_file' => $file,
        ]);
    }

    private function generateErrorReport( array $report, array $data ) {
        $date         = date( "ymd", time() );
        $randomString = bin2hex( random_bytes( 20 ) );
        $fileDir      = 'export' . DIRECTORY_SEPARATOR . 'exported' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $randomString;
        $fileName     = 'Errori_Lavorazione_Scarti.txt';
        wp_mkdir_p( plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir );
        $file = plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir . DIRECTORY_SEPARATOR . $fileName;

        $fileContent = [];

        foreach ($report['row_errors'] as $key => $error) {
            if(!empty($data['numero'][$key])){
                $fileContent[] = implode('|', [$data['numero'][$key], DateTime::createFromFormat('m/d/Y', $data['data'][$key])->format('d/m/Y'), $data['dipartimento'][$key], $error]);
            }
        }
        $fileContent = implode("\n", $fileContent);
        file_put_contents( $file, $fileContent );

        return plugins_url( $fileDir . DIRECTORY_SEPARATOR . $fileName, __FILE__ );
    }

    /**
     * @return array
     */
    private function processData( array $data ) {
        global $wpdb;
        global $uploadYear;
        $mysqli = fatturaDBConnect();

        $mysqli -> select_db(UPLOAD_DB_NAME . "_" . $uploadYear);
        $report = [
            'row_errors' => [],
            'total_rows' => count($data['stato lavorazione'])
        ];
        $query = <<<SQL
SELECT f.id FROM fattura AS f 
JOIN fattura_dati_body AS fb ON f.id=fb.fattura_id 
WHERE fb.numero_fattura = %s 
    AND fb.data_fattura = %s
    AND f.dipartimento= %s;
SQL;
        $statusArray = [
            'lavorata' => 1,
            'archiviata' => 2,
        ];
        foreach ($data['stato lavorazione'] as $key => $status) {
            if (!in_array(strtolower($status), ['lavorata', 'archiviata'])) {
                $report['row_errors'][$key] = "Stato lavorazione $status non ammesso";
                continue;
            }
            $statusNumber = $statusArray[strtolower($status)];

            try {
                $results = $mysqli->query(
                    $wpdb->prepare(
                        $query,
                        [
                            $data['numero'][$key],
                            DateTime::createFromFormat('m/d/Y', $data['data'][$key])->format('Y-m-d'),
                            strtoupper($data['dipartimento'][$key])
                        ]
                    )
                );
                if (!$results || !$results->num_rows) {
                    $report['row_errors'][$key] = 'Fattura non trovata nel database';
                    continue;
                }

                $ids = array_map(function ($row) {
                    return (int) $row['id'];
                }, $results->fetch_all(MYSQLI_ASSOC));

                
                //$mysqli->query($wpdb->prepare("UPDATE fattura SET lavorazione_scarto = %d WHERE id IN (%s)" , [$statusNumber, implode(',', $ids)]));

                $mysqli->query("UPDATE fattura SET lavorazione_scarto = $statusNumber WHERE id IN (" . implode(',', $ids) . ")");

            } catch (Exception $exception) {
                $report['row_errors'][$key] = 'Error when process: ' . $exception->getMessage();
                continue;
            }
        }

        $report['total_errors'] = count($report['row_errors']);
        return $report;
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function getData() {
        $rows = $this->spreadsheet->getActiveSheet()->getRowIterator();

        // firstRow - label
        $labels = [];
        $firstRow = true;
        $data = [];
        foreach ($rows as $rowNum => $row) {
            foreach ($row->getCellIterator() as $col => $cell) {
                if ($firstRow) {
                    $labels[$col] = strtolower($cell->getValue());
                    $data[$labels[$col]] = [];
                } else {
                    $data[$labels[$col]][$rowNum] = $cell->getFormattedValue();
                }
            }
            if ($firstRow) {
                $firstRow = false;
            }
        }
        return $data;
    }

    private function initFile() {
        if (!isset($_FILES['allega-excel']['tmp_name'])) {
            throw new \Exception('does not find excel file');
        }

        $GLOBALS['uploadYear'] = $_REQUEST['upload_year'];
        
        $inputFileType = 'Xlsx';
        $inputFileName = $_FILES['allega-excel']['tmp_name'];
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $reader->setReadEmptyCells(false);
        $this->spreadsheet = $reader->load($inputFileName);

        if ($this->spreadsheet->getActiveSheet()->getHighestRow() < 2) {
            throw new \Exception('excel file does not have data');
        }
        if (Coordinate::columnIndexFromString($this->spreadsheet->getActiveSheet()->getHighestColumn()) != 4) {
            throw new \Exception('excel file does not have correct number of columns');
        }
    }
}

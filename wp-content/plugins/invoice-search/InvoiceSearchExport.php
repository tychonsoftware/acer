<?php

class InvoiceSearchExport {

	protected $fattura_dati_riepilogo = [];

	public $currentDay;
	public $hasChangeDay = false;
	public $dayData = [];
	public $currentMonth;
	public $hasChangeMonth = false;
	public $monthData = [];
	public $summaryData = [
        'Totale' => 0,
        'Imponibile' => 0,
        'Imposta' => 0,
        'Imponibile004' => 0,
        'Imposta004' => 0,
        'Imponibile006' => 0,
        'Imposta006' => 0,
        'ImponibileESC' => 0,
        'ImpostaESC' => 0,
        'ImponibileESE' => 0,
        'ImpostaESE' => 0,
        'ImponibileFCI' => 0,
        'ImpostaFCI' => 0,
    ];
	public $errorSummaryRows = [];

	public function exportData() {
		$date         = date( "ymd", time() );
		$randomString = bin2hex( random_bytes( 20 ) );
		$fileDir      = 'export' . DIRECTORY_SEPARATOR . 'exported' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $randomString;
		$fileName     = 'Registro_Sezionale_IVA_'.$this->getSezionaleNamePart().'_'.date( "dmY").'.txt';
		wp_mkdir_p( plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir );
		$file = plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir . DIRECTORY_SEPARATOR . $fileName;
		file_put_contents( $file, $this->getExportContent() );

		echo plugins_url( $fileDir . DIRECTORY_SEPARATOR . $fileName, __FILE__ );

		wp_die();
	}

	private function getExportContent() {
		$request = getInvoiceSearchRequest();
		$GLOBALS['uploadYear'] = $request['uploadYear'];
		$items = rf_get_fattura_export($this->isShowDaySummary());
		$itemsErrorSummary = rf_get_fattura_export_error_summary();
		$sezionaleNamePart = $this->getSezionaleNamePart();
        if ( ($items->num_rows < 1) && ($itemsErrorSummary->num_rows < 1) ) {
            throw new Exception( 'Does not have data to export' );
        }

		ob_start();
		include_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'template.phtml' );

		return ob_get_clean();
	}

	public function getEmi( array $row ) {
		$month = (int) date('n', strtotime($row['data_fattura']));
		return intdiv($month + 2, 3);

	}

	public function getRagSociale1( array $row ) {
		return $row['denominazione_cesCom'] ?? ($row['cognome_cesCom'] . ' ' . $row['nome_cesCom']);
	}

	public function getRagSociale2( array $row ) {
		return $row['idCodice_cesCom'] ?? $row['codiceFiscale_cesCom'];
	}

	public function getExtraChildRows( array $row ) {
		$id = $row['id'];
		$n = count($this->fattura_dati_riepilogo[$id]);
		$result = [];
		for ($i = 2; $i < $n; $i++) {
			$result[] = $this->getRowDataN($row, $i+1);
		}
		return $result;
	}

	public function getRowData1( array $row ) {
		return $this->getRowDataN($row, 1);
	}

	public function getRowData2( array $row ) {
		return $this->getRowDataN($row, 2);
	}

	protected function getRowDataN($row, $n) {
		$id = $row['id'];
		if (isset($this->fattura_dati_riepilogo[$id][$n - 1])) {
			$data = $this->fattura_dati_riepilogo[$id][$n - 1];
			return [
				'Imponibile' => floatval($data['imponibile_riep']),
				'Imposta' => floatval($data['imposta_riep']),
				'Alq' => $data['aliquota_riep'],
				'Vox' => $this->getVox($data),
			];
		}
		return [
			'Imponibile' => '',
			'Imposta' => '',
			'Alq' => '',
			'Vox' => ''
		];
	}

    public function processRowSummaryBefore( ?array $row ) {
	    $day = date('d/m/Y', strtotime($row['data_fattura']));
        $month = date('m/Y', strtotime($row['data_fattura']));

        if (isset($this->currentDay) && ($this->currentDay !== $day)) {
            $this->hasChangeDay = true;
            if ($this->currentMonth !== $month) {
                $this->hasChangeMonth = true;
            }
        } else {
            $this->hasChangeDay = false;
            $this->hasChangeMonth = false;
        }
    }

    public function processRowSummaryAfter( ?array $row ) {
	    $day = date('d/m/Y', strtotime($row['data_fattura']));
        $month = date('m/Y', strtotime($row['data_fattura']));

        if (!isset($this->currentDay) || $this->hasChangeDay) {
            $this->resetDayData($day);
        }
        if (!isset($this->currentMonth) || $this->hasChangeMonth) {
            $this->resetMonthData($month);
        }
        $this->currentDay = $day;
        $this->currentMonth = $month;
    }

	public function hasExtraChildRows( array $row ) {
		$id = $row['id'];
		return count($this->fattura_dati_riepilogo[$id]) > 2;
	}

	public function isShowDaySummary() {
	    return isset($_POST["show_day_summary"]) ? !!$_POST["show_day_summary"] : false;
    }

	private $cacheVox = [];
	private function getVox( $data ) {
		$tipoNaturaId = $data['tipo_natura_id'];
		if (is_null($tipoNaturaId) || $tipoNaturaId < 1) {
			if ($data['aliquota_riep'] == 22) {
				return 'I22';
			}
			if ($data['aliquota_riep'] == 10) {
				return 'I10';
			}
			return '';
		}
		if (!isset($this->cacheVox[$tipoNaturaId])) {
			$mysqli = fatturaDBConnect();

			$sql = <<<SQL
SELECT codi_xxml FROM fattura_tipo_natura WHERE id = $tipoNaturaId LIMIT 1
SQL;
			$result = $mysqli->query($sql)->fetch_assoc();
			if ($result) {
			    switch ($result['codi_xxml']) {
                    case 'N1':
                        $this->cacheVox[$tipoNaturaId] = 'ESC';
                        break;
                    case 'N2':
                        $this->cacheVox[$tipoNaturaId] = 'FCI';
                        break;
                    case 'N4':
                        $this->cacheVox[$tipoNaturaId] = 'ESE';
                        break;
                    default:
                        $this->cacheVox[$tipoNaturaId] = '';
                        break;
                }
			} else {
				$this->cacheVox[$tipoNaturaId] = '';
			}
		}
		return $this->cacheVox[$tipoNaturaId];
	}

	protected $cacheTipo = [];
	public function getTipo( array $row ) {
		$tipoId = $row['tipologia_documento_id'];
		if (( (int) $tipoId ) < 1) {
			return '';
		}
		if (isset($this->cacheTipo[ $tipoId ])) {
			return $this->cacheTipo[ $tipoId ];
		}
		$mysqli = fatturaDBConnect();

		$sql = <<<SQL
SELECT codi_xxml FROM fattura_tipologia_documento WHERE id = $tipoId LIMIT 1
SQL;
		$result = $mysqli->query($sql)->fetch_assoc();
		if ($result) {
			switch ($result['codi_xxml']) {
				case 'TD01':
					$this->cacheTipo[ $tipoId ] = 'F';
					break;
				case 'TD04':
					$this->cacheTipo[ $tipoId ] = 'C';
					break;
				case 'TD05':
					$this->cacheTipo[ $tipoId ] = 'D';
					break;
				default:
					$this->cacheTipo[ $tipoId ] = '';
			}
		} else {
			$this->cacheTipo[$tipoId] = '';
		}
		return $this->cacheTipo[ $tipoId ];
	}

	private function resetDayData( string $day ) {
		$this->dayData = [
			'day' => $day,
			'Totale' => 0,
			'Imponibile' => 0,
			'Imposta' => 0,
			'Imponibile004' => 0,
			'Imposta004' => 0,
			'Imponibile006' => 0,
			'Imposta006' => 0,
			'ImponibileESC' => 0,
			'ImpostaESC' => 0,
			'ImponibileESE' => 0,
			'ImpostaESE' => 0,
			'ImponibileFCI' => 0,
			'ImpostaFCI' => 0,
		];
	}

	private function resetMonthData( string $month ) {
		$this->monthData = [
			'month' => $month,
            'Totale' => 0,
            'Imponibile' => 0,
            'Imposta' => 0,
            'Imponibile004' => 0,
            'Imposta004' => 0,
            'Imponibile006' => 0,
            'Imposta006' => 0,
            'ImponibileESC' => 0,
            'ImpostaESC' => 0,
            'ImponibileESE' => 0,
            'ImpostaESE' => 0,
            'ImponibileFCI' => 0,
            'ImpostaFCI' => 0,
		];
	}

	public function getLavorazioneScartoText($row) {
	    $val = (int) $row['lavorazione_scarto'];
	    switch ($val) {
            case 0:
                return 'Da lavorare';
            case 1:
                return 'Lavorata';
            case 2:
                return 'Archiviata';
            default:
                return '';
        }
    }

    private function getSezionaleNamePart() {
	    $request = getInvoiceSearchRequest();
        $sezionale = $request['sezionale'] ?? '';

        if ($sezionale) {
            return $sezionale;
        }

        return 'AC';
    }

    /**
     * @param array $row
     */
    private function calculateSumary( array $row ): void {
        $mysqli = fatturaDBConnect();

        $id                                  = $row['id'];
        $sql                                 = <<<SQL
SELECT * FROM fattura_dati_riepilogo WHERE fattura_id = $id LIMIT 30
SQL;
        $result                              = $mysqli->query( $sql )->fetch_all( MYSQLI_ASSOC );
        $this->fattura_dati_riepilogo[ $id ] = $result;

        $this->dayData['Totale']     += floatval( $row['importo_totale'] );
        $this->monthData['Totale']   += floatval( $row['importo_totale'] );
		$this->summaryData['Totale'] += floatval( $row['importo_totale'] );
		$importo_bollo = empty($row['importo_bollo']) ? 0.0 : floatval($row['importo_bollo']);
		$this->dayData['Imponibile'] += $importo_bollo;
		$this->summaryData['ImponibileFCI'] += $importo_bollo;
		$this->monthData['ImponibileFCI'] += $importo_bollo;
		$this->dayData['ImponibileFCI'] += $importo_bollo;
		$this->summaryData['Imponibile'] += $importo_bollo;
		$this->monthData['Imponibile'] += $importo_bollo;
        for ( $i = 0; $i < count( $result ); $i ++ ) {
            $rowData                         = $this->getRowDataN( $row, $i + 1 );
            $this->dayData['Imponibile']     += floatval( $rowData['Imponibile'] );
            $this->monthData['Imponibile']   += floatval( $rowData['Imponibile'] );
            $this->summaryData['Imponibile'] += floatval( $rowData['Imponibile'] );
            $this->dayData['Imposta']        += floatval( $rowData['Imposta'] );
            $this->monthData['Imposta']      += floatval( $rowData['Imposta'] );
			$this->summaryData['Imposta']    += floatval( $rowData['Imposta'] );
            switch ( trim( $rowData['Vox'] ) ) {
				case 'I10':
                    $this->dayData['Imponibile004']     += floatval( $rowData['Imponibile'] );
                    $this->monthData['Imponibile004']   += floatval( $rowData['Imponibile'] );
                    $this->summaryData['Imponibile004'] += floatval( $rowData['Imponibile'] );
                    $this->dayData['Imposta004']        += floatval( $rowData['Imposta'] );
                    $this->monthData['Imposta004']      += floatval( $rowData['Imposta'] );
					$this->summaryData['Imposta004']    += floatval( $rowData['Imposta'] );
                    break;
				case 'I22':
					$this->dayData['Imponibile006']     += floatval( $rowData['Imponibile'] );
                    $this->monthData['Imponibile006']   += floatval( $rowData['Imponibile'] );
                    $this->summaryData['Imponibile006'] += floatval( $rowData['Imponibile'] );
                    $this->dayData['Imposta006']        += floatval( $rowData['Imposta'] );
                    $this->monthData['Imposta006']      += floatval( $rowData['Imposta'] );
					$this->summaryData['Imposta006']    += floatval( $rowData['Imposta'] );
                    break;
				case 'ESC':
					
					$this->dayData['ImponibileESC']     += floatval( $rowData['Imponibile'] );
                    $this->monthData['ImponibileESC']   += floatval( $rowData['Imponibile'] );
                    $this->summaryData['ImponibileESC'] += floatval( $rowData['Imponibile'] );
                    $this->dayData['ImpostaESC']        += floatval( $rowData['Imposta'] );
                    $this->monthData['ImpostaESC']      += floatval( $rowData['Imposta'] );
					$this->summaryData['ImpostaESC']    += floatval( $rowData['Imposta'] );
                    break;
				case 'ESE':
					$this->dayData['ImponibileESE']     += floatval( $rowData['Imponibile'] );
                    $this->monthData['ImponibileESE']   += floatval( $rowData['Imponibile'] );
                    $this->summaryData['ImponibileESE'] += floatval( $rowData['Imponibile'] );
                    $this->dayData['ImpostaESE']        += floatval( $rowData['Imposta'] );
                    $this->monthData['ImpostaESE']      += floatval( $rowData['Imposta'] );
					$this->summaryData['ImpostaESE']    += floatval( $rowData['Imposta'] );
                    break;
				case 'FCI':
					$this->dayData['ImponibileFCI']     += floatval( $rowData['Imponibile'] );
					$this->monthData['ImponibileFCI']   += floatval( $rowData['Imponibile'] );
					$this->summaryData['ImponibileFCI'] += floatval( $rowData['Imponibile'] );
                    $this->dayData['ImpostaFCI']        += floatval( $rowData['Imposta'] );
                    $this->monthData['ImpostaFCI']      += floatval( $rowData['Imposta'] );
                    $this->summaryData['ImpostaFCI']    += floatval( $rowData['Imposta'] );
                    break;
            }
        }
    }
}
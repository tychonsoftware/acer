<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Invoice Search
 * Description:       Search/Extract Data From Invoices
 * Version:           1.00
 * Text Domain:       invoice-search
 */

if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/SearchFunctions.php');	
define("RF_URL", plugin_dir_url(dirname(__FILE__) . '/InvoiceSearch.php'));

if (!class_exists('RicercaFattura')) {
    class RicercaFattura

    {
        var $version;
        var $minium_WP   = '3.1';
        
        function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->define_tables();
            $this->load_dependencies();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('InvoiceSearch', 'activate'));
            register_deactivation_hook(__FILE__, array('InvoiceSearch', 'deactivate'));
            register_uninstall_hook(__FILE__, array('InvoiceSearch', 'uninstall'));
            add_action('wp_enqueue_scripts', array(&$this, 'InvoiceSearch_Enqueue_Scripts'));
			add_action( 'wp_ajax_export_invoice_search', function () {
				include_once(dirname(__FILE__) . '/InvoiceSearchExport.php');
				(new InvoiceSearchExport())->exportData();
			});
            add_action( 'wp_ajax_export_excel_invoice_search', function () {
                include_once(dirname(__FILE__) . '/InvoiceSearchExportExcel.php');
                (new InvoiceSearchExportExcel())->exportData();
            });
            add_action( 'wp_ajax_export_csv_invoice_search', function () {
                include_once(dirname(__FILE__) . '/InvoiceSearchExportCSV.php');
                (new InvoiceSearchExportCSV())->exportData();
            });
            add_action( 'wp_ajax_invoice_search_lavorazione_scarto_select', function () {
                $this->lavorazioneScartoUpdate();
            });
            add_action( 'wp_ajax_invoice_search_allega_excel_upload', function () {
                include_once(dirname(__FILE__) . '/AllegaExcelUpdate.php');
                (new AllegaExcelUpdate)->import();
            });
            add_action( 'wp_ajax_invoice_search_data_loading', function () {
                if(isset( $_POST['year'])){
                    // $date = DateTime::createFromFormat("d/m/Y", $_POST['year']);
                    // $year =  $date->format("Y");
                    $GLOBALS['uploadYear'] =  $_POST['year'];
                    $success = 'success';
                    $stats = rf_GetStatuses();
                    if($stats == null){
                        $success = 'error';
                    }
                    $statuses = array();
                    foreach($stats as $stat) {
                        $status = new stdClass();
                        $status->stato = $stat['stato'];
                        $status->descrizione = $stat['descrizione'];
                        array_push($statuses,$status);
                    }

                    $sezionales = getDistinctValues( 'fattura', 'sezionale' );
                    $titolares = getDistinctValues( 'fattura', 'titolare' );
                    $cespites = getDistinctValues( 'fattura', 'cespite' );
                    $ambitocontrattuales = getDistinctValues( 'fattura', 'ambito_contrattuale' );
                    $voces = getDistinctValues( 'fattura_voci', 'voce' );
                    $codixmls = getDistinctValues( 'fattura_tipo_natura', 'codi_xxml', 'id' );

                    $tipos = array();
                    foreach($codixmls as $id => $naturaValue) {
                        $tipo = new stdClass();
                        $tipo->id = $id;
                        $tipo->naturaValue = $naturaValue;
                        array_push($tipos,$tipo);
                    }

                    $errorMessage = '';
                    if($success == 'error'){
                        $errorMessage  = 'Connection could not be established for Database ' .UPLOAD_DB_NAME.$_POST['year']; 
                    }
                    return wp_send_json_success([
                        'result' => $success,
                        'statuses' => $statuses,
                        'sezionales' => $sezionales,
                        'titolares' => $titolares,
                        'cespites' => $cespites,
                        'ambitocontrattuales' => $ambitocontrattuales,
                        'voces' => $voces,
                        'tipos' => $tipos,
                        'error' => $errorMessage
                    ]);
                }
			});
        }

        function load_dependencies()
        {
            if (is_admin()) {
                require_once(dirname(__FILE__) . '/admin/admin.php');
            }
        }

        function define_tables()
        {
            global $wpdb, $table_prefix;
            $wpdb->table_name_Fattura = "fattura";
            $wpdb->table_name_Fattura_Dati_Body = "fattura_dati_body";
            $wpdb->table_name_Fattura_Dati_Header = "fattura_dati_header";
            $wpdb->table_name_Fattura_State = "fattura_state";
        }

        static function init()
	    {

        }
        static function screen_option()
	    {
            
        }

        static function dizionari_set_option($status, $option, $value)
        {
        }

        static function InvoiceSearch_Enqueue_Scripts($hook_suffix)
		{

            $path = plugins_url('', __FILE__);
            wp_enqueue_script('jquery');

            wp_register_script( 'Select2', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js');
            wp_enqueue_script('Select2');

            wp_enqueue_script('datepicker', $path . '/js/datepicker.min.js', array('jquery'));
            // wp_enqueue_script('jquery-ui-datepicker', '', array('jquery'));
            wp_enqueue_script('sweetalert1', $path . '/js/sweetalert.min.js');
            wp_enqueue_script('invoicesearch', $path . '/js/invoicesearch.js', array('jquery'));
            wp_localize_script( 'invoicesearch', 'invoicesearch', array(
                // URL to wp-admin/admin-ajax.php to process the request
                'ajaxurl' => admin_url( 'admin-ajax.php' ),
                'postData' => $_POST
            ));

            wp_register_style( 'Select2', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' );
            wp_enqueue_style('Select2');

            wp_register_style('InvoiceSearch', $path . '/css/style.css');
            wp_enqueue_style('InvoiceSearch');
        }
        
        static function activate()
        {
            global $wpdb;
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);
        }

        
        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
        }
        static function uninstall()
        {
        }

        private function lavorazioneScartoUpdate() {
            if ( ! is_admin() ) {
                echo 'You are not allowed to do this';
                wp_die();
            }

            $data = $_POST['data'];
            $id = (int) $data['id'];
            $value = (int) $data['value'];
            $GLOBALS['uploadYear'] = (int) $data['year'];
            try {
                $mysqli = fatturaDBConnect();
                $mysqli->query("UPDATE fattura SET lavorazione_scarto = $value WHERE id = $id");
            } catch (Throwable $e) {
                error_log($e->getMessage());
                echo $e->getMessage();
            }

            wp_die();
        }
    }
    global $RF_OnLine;
    $RF_OnLine = new RicercaFattura();
}

if (isset($_GET['invoice-search-setup'])) {
    $mysqli = fatturaDBConnect();
    $mysqli->query('ALTER TABLE fattura ADD COLUMN IF NOT EXISTS lavorazione_scarto TINYINT(1) DEFAULT 0');
}
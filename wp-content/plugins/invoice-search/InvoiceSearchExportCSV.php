<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class InvoiceSearchExportCSV {

    public function exportData() {
        $date         = date( "ymd", time() );
        $randomString = bin2hex( random_bytes( 20 ) );
        $fileDir      = 'export' . DIRECTORY_SEPARATOR . 'exported' . DIRECTORY_SEPARATOR . 'csv' . DIRECTORY_SEPARATOR . $date . DIRECTORY_SEPARATOR . $randomString;
        $fileName     = 'Esiti_SDI_'. date( "dmY").'.csv';
        wp_mkdir_p( plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir );
        $file = plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . $fileDir . DIRECTORY_SEPARATOR . $fileName;
        $this->getExportContent($file);

        echo plugins_url( $fileDir . DIRECTORY_SEPARATOR . $fileName, __FILE__ );

        wp_die();


    }

    private function getExportContent($file) {
        $request = getInvoiceSearchRequest();
		$GLOBALS['uploadYear'] = $request['uploadYear'];
        $items = rf_get_fattura_export_csv();

        $fp = fopen($file, 'w');

        $header = ['Numero_Fattura', 'Codice_Utente', 'Esito_SDI', 'Data_Esito', 'Errore','Nome File'];

        fwrite($fp, implode('|', $header) . "\n");

        if ($items) {
            while ($row = $items->fetch_assoc()) {
                fwrite($fp, implode('|', [
                        $row['numero_fattura'],
                        $row['rif_ammin_ced_prest'],
                        $row['stato'],
                        $row['data_esito_csa'] ? date("d-m-Y", strtotime($row['data_esito_csa'])) : '',
                        in_array($row['stato_code'], ['33', '34']) ? $row['messaggio_esito_csa'] : '',
                        $row['nome_file_zip'],
                    ]) . "\n");
            }
        }

        fclose($fp);
    }
}
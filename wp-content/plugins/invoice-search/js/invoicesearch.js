function overlayPreventAction() {
	var $ = jQuery;
	$('body').addClass('__overlayPreventAction')
}

function closeOverlayPreventAction() {
	var $ = jQuery;
	$('body').removeClass('__overlayPreventAction')
}

function isDate(s) {
    if(isNaN(s) && !isNaN(Date.parse(s)))
        return true;
    else return false;
}
jQuery(document).ready(function ($) {
	var date_to = $("#DataFatturaA").val();

	if(date_to == null || date_to == ''){
		$("#ricerca").attr('disabled','disabled');
		$("#ricerca").attr('title','Per la ricerca \u00E8 obbligatorio valorizzare i campi Data Fattura Da e Data Fattura A con date dello stesso anno');
		$("#ricerca").css({"cursor": "not-allowed", " pointer-events": "none !important"});
	}
		
	$('#DataFatturaDa').datepicker({
		dateFormat: 'dd/mm/yy'
	});
	$('#DataFatturaA').datepicker({ dateFormat: 'dd/mm/yy' });

	$('#DataEsitoSdiDal').datepicker({ dateFormat: 'dd/mm/yy' });

	$('td > .select2-wrapper > .select2').each(function (i, ele) {
		$(ele).parent().css("maxWidth", $(ele).parent().width() + "px")
		$(ele).parent().parent().css("width", $(ele).parent().parent().width() + "px")
	});

	$('.select2').select2();

	function validateTXTExport() {
		return $('#DataFatturaDa').val()
			&& $('#DataFatturaA').val();
	}

	var isExport = false;
	$('#export-invoice-search-button').click(function () {
		if (!validateTXTExport()) {
			var error = 'Per la generazione del registro è necessario che siano compilati almeno i campi di ricerca Data Fattura Da e Data Fattura A';
			$('.invoice-search-popup .text .content').html(error);
			$('.invoice-search-popup .overlay').show();
			return;
		}

		if (isExport) {
			$('#export-invoice-search .overlay.txt').show();
			return;
		}

		$('#export-invoice-search .overlay.txt .text').text('Elaborazione del tracciato in corso');
		$('#export-invoice-search .overlay.txt').show();

		var data = {
			action: 'export_invoice_search',
			data: invoicesearch.postData,
			show_day_summary: document.getElementById("riepilogo_per_giorno").checked ? 1 : ''
		};
		$.post(invoicesearch.ajaxurl, data, function(response) {
			var html = '<a href="'+response+'" download>Clicca qui per scaricare il tracciato</a><br><button class="close">CHIUDI</button>';
			$('#export-invoice-search .overlay.txt .text').html(html);
			isExport = true;
		}).fail(function(response) {
			$('#export-invoice-search .overlay.txt').hide();
			var error = 'Per i criteri selezionati non sono state trovate fatture inviate al provider';
			$('.invoice-search-popup .text .content').html(error);
			$('.invoice-search-popup .overlay').show();
			return;
		});
	});

	$('#riepilogo_per_giorno').change(function () {
		isExport = false;
	});

	var isExportExcel = false;

	function validateExcelExport() {
		return $('#DataFatturaDa').val()
			&& $('#DataFatturaA').val()
			&& $('#dipartimento').val();
	}

	$('#export-excel-invoice-search-button').click(function () {
		if (!validateExcelExport()) {
			var error = 'Per la generazione del report è necessario che siano compilati almeno i campi di ricerca Data Fattura Da, Data Fattura A e Dipartimento';
			$('.invoice-search-popup .text .content').html(error);
			$('.invoice-search-popup .overlay').show();
			return;
		}

		$('#export-invoice-search .overlay.excel').show();

		if (isExportExcel) {
			return;
		}

		var data = {
			action: 'export_excel_invoice_search',
			data: invoicesearch.postData
		};
		$.post(invoicesearch.ajaxurl, data, function(response) {
			var html = '<a href="'+response+'" download>Clicca qui per scaricare il tracciato</a><br><button class="close">CHIUDI</button>';
			$('#export-invoice-search .overlay.excel .text').html(html);
			isExportExcel = true;
		});
	});

	function validateCsvExport() {
		return $('#DataEsitoSdiDal').val();
	}

	var isExportCSV = false;
	$('#export-csv-invoice-search-button').click(function () {
		if (!validateCsvExport()) {
			var error = 'Per la generazione del report è necessario che siano compilato almeno il campo di ricerca Data Esito Sdi Dal';
			$('.invoice-search-popup .text .content').html(error);
			$('.invoice-search-popup .overlay').show();
			return;
		}

		$('#export-invoice-search .overlay.csv').show();

		if (isExportCSV) {
			return;
		}

		var data = {
			action: 'export_csv_invoice_search',
			data: invoicesearch.postData
		};
		$.post(invoicesearch.ajaxurl, data, function(response) {
			var html = '<a href="'+response+'" download>Clicca qui per scaricare il tracciato</a><br><button class="close">CHIUDI</button>';
			$('#export-invoice-search .overlay.csv .text').html(html);
			isExportCSV = true;
		}).fail(function(response) {
			isExportCSV = true;
			var html = '<p>Errore riuscito o Nessuna fattura inviata al fornitore trovata per i criteri selezionati</p><button class="close">CHIUDI</button>';
			$('#export-invoice-search .overlay.csv .text').html(html);
		});
	});

	$('#export-invoice-search .overlay').on('click', '.close', function () {
		$('#export-invoice-search .overlay').hide();
	});

	$('.stato-error-link').click(function () {
		var error = $(this).find('.content').html();
		$('.invoice-search-popup .text .content').html(error);
		$('.invoice-search-popup .overlay').show();
		return false;
	});
	$('.invoice-search-popup .overlay').on('click', '.close', function () {
		$('.invoice-search-popup .overlay').hide();
	});

	$('.lavorazione_scarto_select').change(function () {
		var pattern =/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/([0-9]{4})$/;
		var toYear = '';
		if(pattern.test(date_to)){
			var dateParts = date_to.split("/");
			toYear = dateParts[2];
		}
		var id = $(this).data('row-id'),
			value = $(this).val();
		var data = {
			action: 'invoice_search_lavorazione_scarto_select',
			data: {
				id: id,
				value: value,
				year: toYear
			}
		};
		$.post(invoicesearch.ajaxurl, data, function(response) {
			if (response) {
				console.log(response);
			}
		});
	});

	$('.open-close-button').click(function () {
		$('.extend-search').toggleClass('hide-search')
		$(this).toggleClass('close');
	});

	// Lavorazione Scarti
	$('.allega-excel-input').change(function () {
		var val = $(this).val().toLowerCase(),
			regex = new RegExp("(.*?)\.(xlsx)$");

		if (!(regex.test(val))) {
			$(this).val('');
			alert('Allegare file excel');
		}
	});
	$('#lavorazione-scarti-button').click(function () {
		function excelPopup() {
			swal1({
				content: $('.lavorazione-scarti-form-wrap .lavorazione-scarti-form').clone(true)[0],
				buttons: {
					cancel: {
						text: "Annulla",
						value: null,
						visible: true,
						className: "",
						closeModal: true,
					},
					confirm: {
						text: "Avvia",
						value: true,
						visible: true,
						className: "",
						closeModal: false
					}
				},
				"className": "swal-lavorazione-scarti",
			}).then((agree) => {
				if (!agree) return null;

				var form = $('.swal-modal form')[0];
				if (!form.checkValidity()) {
					alert('Allegare file excel');
					swal1.close();
					excelPopup();
					return null;
				}

				$('.swal-modal').prepend($('<div class="swal-text">In corso il processo di aggiornamento dello stato lavorazione degli scarti</div>'));
				overlayPreventAction();
				$.ajax({
					url: invoicesearch.ajaxurl,
					type: 'POST',
					contentType: false,
					processData: false,
					data: new FormData(form),
					success: function (response) {
						if (!response.success) {
							return swal1("Errore", response.data.message, "error", {
								button: "Chiudi",
								"className": "swal-lavorazione-scarti",
							});
						}
						var content = '<p>Fatture Totali: '+response.data.total_rows+'</p>';
						content += '<p>Fatture aggiornate correttamente: '+response.data.num_success+'</p>';

						if (response.data.error) {
							content += '<p>Fatture in errore: '+response.data.num_error+'  <a download target="_blank" href="'+response.data.error_report_file+'">Errori_Lavorazione_Scarti.txt</a></p>';
						}
						return swal1({
							content: $('<div style="text-align: left;">' + content + '</div>')[0],
							button: "Chiudi",
							"className": "swal-lavorazione-scarti",
						});
					},
					error: function () {
						return swal1("Errore", "Impossibile elaborare", "error", {
							button: "Chiudi",
							"className": "swal-lavorazione-scarti",
						});
					},
					complete: function () {
						closeOverlayPreventAction();
					}
				});

			});
		}
		excelPopup();

	});

	$("#ricerca").click(function(){
		
		var date_to = $("#DataFatturaA").val();
		var date_from = $("#DataFatturaDa").val();
		
		if(date_to =='' || date_from=='')
		{
			alert('Per la ricerca è obbligatorio valorizzare i campi “Data Fattura Da” e “Data Fattura A”.');
			return false;
		}

		if(date_to !='' || date_from !='')
		{
			var splitdatefrom = date_from.split('/');
			var splitdateto = date_to.split('/');
			
			if(splitdatefrom[2] != splitdateto[2]){
				alert('Controllare i campi “Data Fattura Da” e “Data Fattura A”: non è possible effettuare ricerche su più anniPer la ricerca è obbligatorio valorizzare i campi “Data Fattura Da” e “Data Fattura A”.');
				return false;
			}else{
				$('#filtro-invoice').append('<input type="hidden" name="uploadYear" value='+splitdatefrom[2]+'>');
			}
			
		}
		
	});

	$("#DataFatturaDa, #DataFatturaA").on('change keydown paste input', function(){
		$("#dbcerror").hide();
		delay(function(){
			loadData();
		  }, 1000 );
	  });
	  //loadData();

	  function loadData(){
		var date_to = $("#DataFatturaA").val();
				var date_from = $("#DataFatturaDa").val();
				
				var pattern =/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/([0-9]{4})$/;
				var toYear = '';
				var fromYear = '';
				if(pattern.test(date_to)){
					var dateParts = date_to.split("/");
					toYear = dateParts[2];
				}
				if(pattern.test(date_from)){
					var dateParts = date_from.split("/");
					fromYear = dateParts[2];
				}
				if(toYear != '' && (toYear == fromYear)){
					var data = {
						action: 'invoice_search_data_loading',
						year:toYear
					};
	
					jQuery('body').addClass('loader-overlay');
					jQuery('body').css('position','relative');
					jQuery('.loader-gif').css('display','block');

					$.post(invoicesearch.ajaxurl, data, function(response) {
						jQuery('body').removeClass('loader-overlay');
						jQuery('.loader-gif').css('display','none');
						if(response.data.result == 'success'){
							$('#ricerca').removeAttr('disabled');
							$("#ricerca").css({"cursor": "pointer", " pointer-events": "auto !important"});
							$("#dbcerror").hide();
							$('#stato').empty();
							$.each(response.data.statuses, function(i, stato) {
								$('#stato').append($('<option>').text(stato.descrizione).attr('value', stato.stato));
							});
	
							$('#sezionale').empty();
							$('#sezionale').append($('<option>').text("-Seleziona Sezionale-").attr('value', ''));
							$.each(response.data.sezionales, function(i, sezionale) {
								$('#sezionale').append($('<option>').text(sezionale).attr('value', sezionale));
							});
	
							$('#titolare').empty();
							$.each(response.data.titolares, function(i, titolare) {
								$('#titolare').append($('<option>').text(titolare).attr('value', titolare));
							});
	
							$('#cespite').empty();
							$.each(response.data.cespites, function(i, cespite) {
								$('#cespite').append($('<option>').text(cespite).attr('value', cespite));
							});
	
							$('#ambito_contrattuale').empty();
							$.each(response.data.ambitocontrattuales, function(i, ambito_contrattuale) {
								$('#ambito_contrattuale').append($('<option>').text(ambito_contrattuale).attr('value', ambito_contrattuale));
							});
	
							$('#voce').empty();
							$.each(response.data.voces, function(i, voce) {
								$('#voce').append($('<option>').text(voce).attr('value', voce));
							});
	
							$('#natura').empty();
							$.each(response.data.tipos, function(i, tipo) {
								$('#natura').append($('<option>').text(tipo.naturaValue).attr('value', tipo.id));
							});
						}else {
							$("#dbcerror").show();
							$("#dbcerror").text(response.data.error);
							$('#sezionale').empty();
							$('#sezionale').append($('<option>').text("-Seleziona Sezionale-").attr('value', ''));
							$('#stato').empty();
							$('#titolare').empty();
							$('#cespite').empty();
							$('#ambito_contrattuale').empty();
							$('#voce').empty();
							$('#natura').empty();
						}
						
					});
			}else {
					$("#ricerca").attr('disabled', 'disabled');
					$("#ricerca").attr('title','Per la ricerca \u00E8 obbligatorio valorizzare i campi Data Fattura Da e Data Fattura A con date dello stesso anno');
					$("#ricerca").css({ "cursor": "not-allowed", " pointer-events": "none !important" });
					$('#sezionale').empty();
					$('#sezionale').append($('<option>').text("-Seleziona Sezionale-").attr('value', ''));
					$('#stato').empty();
					$('#titolare').empty();
					$('#cespite').empty();
					$('#ambito_contrattuale').empty();
					$('#voce').empty();
					$('#natura').empty();
			}
	}
});

var delay = (function(){
	var timer = 0;
	return function(callback, ms){
	clearTimeout (timer);
	timer = setTimeout(callback, ms);
   };
  })();
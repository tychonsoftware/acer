<?= sprintf(<<<'TEXT'
                                              Tot. documenti alla data: %14.14s
                                              Tot. voci alla data     :            %14.14s
                                              Tot. imposte alla data  :                       %14.14s

***********************************************************************************************************************
 Totali del giorno %10.10s
 Vox Descrizione                      Imponibile      Imposta
 I10 I.V.A. 10%%                       %10.10s   %10.10s
 I22 I.V.A. 22%%                       %10.10s   %10.10s
 ESC Escluso art.15 c.1 DPR 633/72    %10.10s   %10.10s
 ESE Esente art.10  c.1 DPR 633/72 %13.13s   %10.10s
 FCI Fuori campo IVA                  %10.10s   %10.10s
                                    ------------ ------------
                                    %12.12s %12.12s -------->  %12.12s
***********************************************************************************************************************
TEXT
	, number_format($this->dayData['Totale'], 2, ',', '.'), number_format($this->dayData['Imponibile'], 2, ',', '.'), number_format($this->dayData['Imposta'], 2, ',', '.'),
	$this->dayData['day'],
	number_format($this->dayData['Imponibile004'], 2, ',', '.'), number_format($this->dayData['Imposta004'], 2, ',', '.'),
	number_format($this->dayData['Imponibile006'], 2, ',', '.'), number_format($this->dayData['Imposta006'], 2, ',', '.'),
	number_format($this->dayData['ImponibileESC'], 2, ',', '.'), number_format($this->dayData['ImpostaESC'], 2, ',', '.'),
	number_format($this->dayData['ImponibileESE'], 2, ',', '.'), number_format($this->dayData['ImpostaESE'], 2, ',', '.'),
	number_format($this->dayData['ImponibileFCI'], 2, ',', '.'), number_format($this->dayData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->dayData['Imponibile004'] + $this->dayData['Imponibile006'] + $this->dayData['ImponibileESC'] + $this->dayData['ImponibileESE'] + $this->dayData['ImponibileFCI'], 2, ',', '.'), number_format($this->dayData['Imposta004'] + $this->dayData['Imposta006'] + $this->dayData['ImpostaESC'] + $this->dayData['ImpostaESE'] + $this->dayData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->dayData['Imponibile004'] + $this->dayData['Imponibile006'] + $this->dayData['ImponibileESC'] + $this->dayData['ImponibileESE'] + $this->dayData['ImponibileFCI'] + $this->dayData['Imposta004'] + $this->dayData['Imposta006'] + $this->dayData['ImpostaESC'] + $this->dayData['ImpostaESE'] + $this->dayData['ImpostaFCI'], 2, ',', '.')
) ?>



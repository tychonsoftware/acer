<?= sprintf(<<<'TEXT'
***********************************************************************************************************************
                                              Importo totale documenti: %14.14s
                                              Importo totale voci     :            %14.14s
                                              Importo totale imposte  :                       %14.14s

***********************************************************************************************************************
 Totali del registro
 Vox Descrizione                      Imponibile      Imposta
 I10 I.V.A. 10%%                       %10.10s   %10.10s
 I22 I.V.A. 22%%                       %10.10s   %10.10s
 ESC Escluso art.15 c.1 DPR 633/72    %10.10s   %10.10s
 ESE Esente art.10  c.1 DPR 633/72 %13.13s   %10.10s
 FCI Fuori campo IVA                  %10.10s   %10.10s
                                    ------------ ------------
                                    %12.12s %12.12s -------->  %12.12s
TEXT
	, number_format($this->summaryData['Totale'], 2, ',', '.'), number_format($this->summaryData['Imponibile'], 2, ',', '.'), number_format($this->summaryData['Imposta'], 2, ',', '.'),
	number_format($this->summaryData['Imponibile004'], 2, ',', '.'), number_format($this->summaryData['Imposta004'], 2, ',', '.'),
	number_format($this->summaryData['Imponibile006'], 2, ',', '.'), number_format($this->summaryData['Imposta006'], 2, ',', '.'),
	number_format($this->summaryData['ImponibileESC'], 2, ',', '.'), number_format($this->summaryData['ImpostaESC'], 2, ',', '.'),
	number_format($this->summaryData['ImponibileESE'], 2, ',', '.'), number_format($this->summaryData['ImpostaESE'], 2, ',', '.'),
	number_format($this->summaryData['ImponibileFCI'], 2, ',', '.'), number_format($this->summaryData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->summaryData['Imponibile004'] + $this->summaryData['Imponibile006'] + $this->summaryData['ImponibileESC'] + $this->summaryData['ImponibileESE'] + $this->summaryData['ImponibileFCI'], 2, ',', '.'), number_format($this->summaryData['Imposta004'] + $this->summaryData['Imposta006'] + $this->summaryData['ImpostaESC'] + $this->summaryData['ImpostaESE'] + $this->summaryData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->summaryData['Imponibile004'] + $this->summaryData['Imponibile006'] + $this->summaryData['ImponibileESC'] + $this->summaryData['ImponibileESE'] + $this->summaryData['ImponibileFCI'] + $this->summaryData['Imposta004'] + $this->summaryData['Imposta006'] + $this->summaryData['ImpostaESC'] + $this->summaryData['ImpostaESE'] + $this->summaryData['ImpostaFCI'], 2, ',', '.')
) ?>

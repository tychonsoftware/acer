<?= sprintf(<<<'TEXT'
                                              Tot. documenti del mese : %14.14s
                                              Tot. voci del mese      :            %14.14s
                                              Tot. imposte alla data  :                       %14.14s

***********************************************************************************************************************
 Totali del mese %10.10s
 Vox Descrizione                      Imponibile      Imposta
 I10 I.V.A. 10%%                       %10.10s   %10.10s
 I22 I.V.A. 22%%                       %10.10s   %10.10s
 ESC Escluso art.15 c.1 DPR 633/72    %10.10s   %10.10s
 ESE Esente art.10  c.1 DPR 633/72 %13.13s   %10.10s
 FCI Fuori campo IVA                  %10.10s   %10.10s
                                    ------------ ------------
                                    %12.12s %12.12s -------->  %12.12s
TEXT
	, number_format($this->monthData['Totale'], 2, ',', '.'), number_format($this->monthData['Imponibile'], 2, ',', '.'), number_format($this->monthData['Imposta'], 2, ',', '.'),
	$this->monthData['month'],
	number_format($this->monthData['Imponibile004'], 2, ',', '.'), number_format($this->monthData['Imposta004'], 2, ',', '.'),
	number_format($this->monthData['Imponibile006'], 2, ',', '.'), number_format($this->monthData['Imposta006'], 2, ',', '.'),
	number_format($this->monthData['ImponibileESC'], 2, ',', '.'), number_format($this->monthData['ImpostaESC'], 2, ',', '.'),
	number_format($this->monthData['ImponibileESE'], 2, ',', '.'), number_format($this->monthData['ImpostaESE'], 2, ',', '.'),
	number_format($this->monthData['ImponibileFCI'], 2, ',', '.'), number_format($this->monthData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->monthData['Imponibile004'] + $this->monthData['Imponibile006'] + $this->monthData['ImponibileESC'] + $this->monthData['ImponibileESE'] + $this->monthData['ImponibileFCI'], 2, ',', '.'), number_format($this->monthData['Imposta004'] + $this->monthData['Imposta006'] + $this->monthData['ImpostaESC'] + $this->monthData['ImpostaESE'] + $this->monthData['ImpostaFCI'], 2, ',', '.'),
	number_format($this->monthData['Imponibile004'] + $this->monthData['Imponibile006'] + $this->monthData['ImponibileESC'] + $this->monthData['ImponibileESE'] + $this->monthData['ImponibileFCI'] + $this->monthData['Imposta004'] + $this->monthData['Imposta006'] + $this->monthData['ImpostaESC'] + $this->monthData['ImpostaESE'] + $this->monthData['ImpostaFCI'], 2, ',', '.')
) ?>



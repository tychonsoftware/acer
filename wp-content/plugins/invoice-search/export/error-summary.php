<?php
/** @var mysqli_result $itemsErrorSummary */
?>


***********************************************************************************************************************
 A.C.E.R. Campania - Progetto Mosaico - Elaborazione del <?= date('d/m/Y', time()); ?> -
***********************************************************************************************************************

ELENCO TITOLI SCARTATI DALLO SDI.

    N. RIGA  PROGR.      NUMERO            DATA       CODICE           IMPORTO    ESITO                     STATUS
<?php $totalImporto = 0; $iRow = 0; ?>
<?php while($row = $itemsErrorSummary->fetch_assoc()): ?>
<?php $totalImporto += $row['importo_totale']; $iRow++; ?>

<?= sprintf(
'    %5.5s    %-7.7s  %-2.2s/%-12.12s   %8.8s   %10.10s        %9.9s    %-25.25s %-11.11s',
        $iRow,
        $row['progressivo_acer'],
        date('y', strtotime($row['data_fattura'])),
        $row['numero_fattura'],
        date('d/m/y', strtotime($row['data_fattura'])),
        $row['rif_ammin_ced_prest'],
        number_format($row['importo_totale'], 2, ',', '.'),
        rf_GetStatusLabel($row['stato']),
        $this->getLavorazioneScartoText($row)) ?>
<?php endwhile; ?>

                                                                --------------
                                                                <?= sprintf('%14.14s', number_format($totalImporto, 2, ',', '.')) ?>


***********************************************************************************************************************
 A.C.E.R. Campania - Progetto Mosaico - Elaborazione del <?= date('d/m/Y', time()); ?> -
***********************************************************************************************************************

<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {
    die( 'You are not allowed to call this page directly.' );
}

if ( isset( $_REQUEST['action'] ) ) {

} else {
    if ( isset( $_REQUEST['uploadYear'] ) ) {
        $GLOBALS['uploadYear'] = $_REQUEST['uploadYear'];
    }
    if ( isset( $_REQUEST['annullaricerca'] ) ) {
        unset( $_REQUEST['DataInizio'] );
        unset( $_REQUEST['DataFine'] );
        unset( $_REQUEST['numerofattura'] );
        unset( $_REQUEST['cliente'] );
        unset( $_REQUEST['codicsfiscale'] );
        unset( $_REQUEST['destinatario'] );
        unset( $_REQUEST['Dipartimento'] );
        unset( $_REQUEST['sezionale'] );
        unset( $_REQUEST['Stato'] );
        unset( $_REQUEST['codice-riferimento'] );
        unset( $_REQUEST['titolare'] );
        unset( $_REQUEST['cespite'] );
        unset( $_REQUEST['ambito_contrattuale'] );
        unset( $_REQUEST['voce'] );
        unset( $_REQUEST['natura'] );
        unset( $_REQUEST['Pag'] );
        unset( $_REQUEST['DataEsitoSdiDal'] );
        VisualizzaInvoiceRicerca( false );
    } else if ( isset( $_REQUEST['ricerca'] ) ) {

        VisualizzaInvoiceRicerca( true );
    } else {
        VisualizzaInvoiceRicerca( false );
    }

}
function VisualizzaInvoiceRicerca( $filter = false ) {
    $per_pages = 10;
    if ( $filter === true ) {
        if ( ! isset( $_REQUEST['Pag'] ) ) {
            $st = 0;
            $pp = $per_pages;
        } else {
            $st = ( $_REQUEST['Pag'] - 1 ) * $per_pages;
            $pp = $per_pages;
        }

        if ( $_REQUEST ) {
            $_REQUEST = array_map( 'sanitize', $_REQUEST );
        }

        list($items, $itemsCountQuery) = rf_get_fattura( $st, $pp );
        if($itemsCountQuery != null)
            $itemscounts = intval(current($itemsCountQuery->fetch_row()));
        $statuses = rf_GetStatuses();
        $sezionales = getDistinctValues( 'fattura', 'sezionale' );
        $titolares = getDistinctValues( 'fattura', 'titolare' );
        $cespites = getDistinctValues( 'fattura', 'cespite' );
        $ambitocontrattuales = getDistinctValues( 'fattura', 'ambito_contrattuale' );
        $voces = getDistinctValues( 'fattura_voci', 'voce' );
        $codixmls = getDistinctValues( 'fattura_tipo_natura', 'codi_xxml', 'id' );
    } else {
        $items = null;
        
    }
    

    ?>
    <?php include 'search/form.phtml' ?>
    <?php if ( isset( $items->num_rows ) && $items->num_rows > 0 ): ?>
        <div id="export-invoice-search" class="alignfull">
            <div class="">
                <label for="riepilogo_per_giorno" id="riepilogo_per_giorno_label">
                    <input type="checkbox" id="riepilogo_per_giorno" name="riepilogo_per_giorno" checked>
                    <span>Riepilogo per giorno</span>
                </label>
            </div>
            <button id="export-invoice-search-button">Registro IVA</button>
            <div class="overlay txt">
                <div class="text">Elaborazione del tracciato in corso</div>
            </div>

            <button id="export-excel-invoice-search-button">Tavola Accertamenti</button>
            <div class="overlay excel">
                <div class="text">Elaborazione del file excel in corso, si prega di attendere</div>
            </div>

            <button id="export-csv-invoice-search-button">Esiti SDI</button>
            <div class="overlay csv">
                <div class="text">Elaborazione del file csv in corso, si prega di attendere</div>
            </div>

            <button id="lavorazione-scarti-button" data-micromodal-trigger="lavorazione-scarti-popup">Lavorazione Scarti</button>
            <div style="display: none;" class="lavorazione-scarti-form-wrap">
                <form action="" enctype="multipart/form-data" class="lavorazione-scarti-form">
                    <input type="hidden" name="action" value="invoice_search_allega_excel_upload">
                    <label>Anno</label>
                    <select class="form-control" name="upload_year" id="UploadYear" style="width:75%; margin-bottom:10px;">
                    <option value="2020" selected>2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    </select>
                    <label>Allega Excel</label>
                    <input type="file" name="allega-excel" required accept="*.xlsx" class="allega-excel-input"/>
                </form>
            </div>
        </div>
    <?php else: ?>
        <div class="alignfull">
            <button id="lavorazione-scarti-button" data-micromodal-trigger="lavorazione-scarti-popup">Lavorazione Scarti</button>
            <div style="display: none;" class="lavorazione-scarti-form-wrap">
                <form action="" enctype="multipart/form-data" class="lavorazione-scarti-form">
                    <input type="hidden" name="action" value="invoice_search_allega_excel_upload">
                    <label>Anno</label>
                    <select class="form-control" name="upload_year" id="UploadYear" style="width:75%; margin-bottom:10px;">
                    <option value="2020" selected>2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    </select>
                    <label>Allega Excel</label>
                    <input type="file" name="allega-excel" required accept="*.xlsx" class="allega-excel-input"/>
                </form>
            </div>
        </div>
    <?php endif; ?>
    <div style="margin-top:20px;" class="alignfull">
        <table class="wp-list-table widefat striped posts" style="width:100%">
            <thead>
            <tr>
                <th scope="col" style="width: 10%">Codice Utente</th>
                <th scope="col" style="width: 5%">Num. Fattura</th>
                <th scope="col" style="width: 10%">Data Fattura</th>
                <th scope="col" style="width: 10%">Dipartimento</th>
                <th scope="col" style="width: 20%">Cliente</th>
                <th scope="col" style="width: 5%">Importo Totale</th>
                <!-- <th scope="col" style="width: 15%">Stato</th> -->
                <th scope="col" style="width: 15%">Stato</th>
                <th scope="col" style="width: 5%">File</th>
                <th scope="col" style="width: 20%">Lavorazione Scarto</th>
            </tr>
            </thead>
            <tbody id="the-list">
            <?php
            if ( isset( $items->num_rows ) && $items->num_rows > 0 ) {
                while ( $row = $items->fetch_assoc() ) {
                    ?>
                    <tr class="iedit author-self level-0 type-post status-publish format-standard hentry category-annunci">
                        <td align="center">
                            <?php
                                echo $row["rif_ammin_ced_prest"];
                            // $codice_destinatario = $row["codice_destinatario"];
                            // $pec_destinatario    = $row["pec_destinatario"];
                            // echo $codice_destinatario;
                            // if ( ! isset( $pec_destinatario ) || trim( $pec_destinatario ) === '' ) {
                            // } else {
                            //     if ( ! isset( $codice_destinatario ) || trim( $codice_destinatario ) === '' ) {
                            //         echo $pec_destinatario;
                            //     } else {
                            //         echo "<br>" . $pec_destinatario;
                            //     }
                            // }
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $row["numero_fattura"]; ?>
                        </td>
                        <td align="center">
                            <?php $oDate = new DateTime( $row["data_fattura"] );
                            echo $oDate->format( "d/m/Y" );
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $row["dipartimento"]; ?>
                        </td>
                        <td align="center">
                            <?php
                            if ( ! isset( $row["denominazione_cesCom"] ) ||
                                 trim( $row["denominazione_cesCom"] ) === '' ) {
                                $data = '';
                                if ( ! isset( $row["cognome_cesCom"] ) ||
                                     trim( $row["cognome_cesCom"] ) === '' ) {
                                } else {
                                    $data = $row["cognome_cesCom"];
                                }
                                if ( ! isset( $row["nome_cesCom"] ) ||
                                     trim( $row["nome_cesCom"] ) === '' ) {
                                } else {
                                    $data = $data . ' ' . $row["nome_cesCom"];
                                }
                                echo $data;
                            } else {
                                echo $row["denominazione_cesCom"];
                            }
                            if ( ! isset( $row["codiceFiscale_cesCom"] )
                                 || trim( $row["codiceFiscale_cesCom"] ) === '' ) {
                                if ( ! isset( $row["idCodice_cesCom"] ) ||
                                     trim( $row["idCodice_cesCom"] ) === '' ) {
                                } else {
                                    echo "<br>" . $row["idCodice_cesCom"];
                                }
                            } else {
                                echo "<br>" . $row["codiceFiscale_cesCom"];
                            }
                            ?>
                        </td>
                        <td align="center">
                            <?php echo $row["importo_totale"]; ?>
                        </td>
                        <td align="center">
                            <?php
                            $stato       = (int) $row["stato"];
                            $stato_error = false;

                            $stato     = $row["stato"];
                            $stato_arr = rf_GetStatusLabel( $stato );
                            if ( $stato == 33 ||  $stato == 34) {
                                $stato_error = trim( $row['messaggio_esito_csa'] );
                            } else if ( $stato == 40 ) {
                                $stato_error = trim( $row['errore'] );
                            }

                            if ( $stato_error !== false ) {
                                echo '<div style="font-size: smaller">';
                            }
                            if ( $stato_arr ) {
                                echo $stato_arr;
                            }

                            if ( $stato_error !== false ) {
                                $stato_error = htmlentities($stato_error);
                                echo <<<HTML
<br><a href="#" class="stato-error-link">Errore <span style="display: none" class="content">$stato_error</span></a></div>
HTML;
                            }
                            ?>
                        </td>
                        <td align="center">
                            <?php if ( ! empty( $row["nome_file_xml_csa"] ) ) {
                            $xml_file_path = $row["csa_path"] . DIRECTORY_SEPARATOR . $row["nome_file_xml_csa"]; ?>
                            <a href="<?php echo RF_URL ?>DowloadDocument.php?action=<?php echo $xml_file_path ?>"
                               target="_blank">

                                <div class="crop">
                                    <img src="<?php echo RF_URL . 'img/xml_icona.png' ?>"
                                         title="<?php echo $row["nome_file_xml_csa"] ?>" height="30" width="30"
                                         allegato/>
                                </div>
                                <?php
                                } ?>

                            </a>

                        </td>
                        <td align="center">
                            <?php
                            $stato             = (int) $row["stato"];
                            $lavorazioneScarto = array(
                                0 => 'Da lavorare',
                                1 => 'Lavorata',
                                2 => 'Archiviata',
                            );
                            if ( $stato === 33 ||  $stato == 34):
                                ?>
                                <select name="lavorazione_scarto" class="lavorazione_scarto_select"
                                        data-row-id="<?= $row['id'] ?>">
                                    <?php foreach ( $lavorazioneScarto as $key => $value ): ?>
                                        <option value="<?= $key ?>" <?php if ( $key === ( (int) $row['lavorazione_scarto'] ) )
                                            echo ' selected ' ?> ><?= $value ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                        </td>

                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
        <?php
        if ( isset( $itemscounts ) && $itemscounts > $per_pages ) {
            $query = [];
            foreach ( $_REQUEST as $k => $v ) {
                if ( $k != "Pag" and $k != "vf" ) {
                    if (is_array($v)) {
                        if (count($v)) {
                            $query[] = implode('&amp;', array_map(function ($i) use ($k) {
                                return  "$k%5B%5D=$i";
                            }, $v));
                        }
                    } else {
                        $query[] = $k . '=' . trim($v);
                    }
                }
            }
            $Para = implode('&amp;', $query);
            if ( $Para == '' ) {
                $Para = "?Pag=";
            } else {
                $Para = "?" . $Para . "&amp;Pag=";
            }
            $Npag = (int) ( $itemscounts / $per_pages );
            if ( $itemscounts % $per_pages > 0 ) {
                $Npag ++;
            }
            ?>
            <div class="tablenav" style="float:right;" id="risultati">
                <div class="tablenav-pages">
                    <p>Pagine
                        <?php
                        if ( isset( $_REQUEST['Pag'] ) and $_REQUEST['Pag'] > 1 ) {
                            $Pagcur = $_REQUEST['Pag'];
                            $PagPre = $Pagcur - 1;
                            ?>
                            &nbsp;<a href="<?php echo $Para ?>1" class="page-numbers numero-pagina"
                                     title="Vai alla prima pagina">&laquo;</a>
                            &nbsp;<a href="<?php echo $Para . $PagPre ?>" class="page-numbers numero-pagina"
                                     title="Vai alla pagina precedente">&lsaquo;</a>
                            <?php
                        } else {
                            $Pagcur = 1;
                            ?>
                            &nbsp;<span class="page-numbers current"
                                        title="Sei gi&agrave; nella prima pagina">&laquo;</span>
                            &nbsp;<span class="page-numbers current"
                                        title="Sei gi&agrave; nella prima pagina">&lsaquo;</span>
                            <?php
                        }
                        ?>
                        &nbsp;<span class="page-numbers current"><?php echo $Pagcur . '/' . $Npag ?></span>
                        <?php
                        $PagSuc = $Pagcur + 1;
                        if ( $PagSuc <= $Npag ) {
                            ?>
                            &nbsp;<a href="<?php echo $Para . $PagSuc ?>" class="page-numbers numero-pagina"
                                     title="Vai alla pagina successiva">&rsaquo;</a>&nbsp;<a
                                    href="<?php echo $Para . $Npag ?>" class="page-numbers numero-pagina"
                                    title="Vai all\'ultima pagina">&raquo;</a>
                            <?php
                        } else {
                            ?>
                            &nbsp;<span class="page-numbers current"
                                        title="Se nell\'ultima pagina non puoi andare oltre">&rsaquo;</span>&nbsp;<span
                                    class="page-numbers current" title="Se nell\'ultima pagina non puoi andare oltre">&raquo;</span>'
                            <?php
                        }
                        ?>
                    </p>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}

?>
<div class="invoice-search-popup">
    <div class="overlay">
        <div class="text">
            <div class="content">Nessun contenuto</div>
            <button class="close">CHIUDI</button>
        </div>
    </div>
</div>
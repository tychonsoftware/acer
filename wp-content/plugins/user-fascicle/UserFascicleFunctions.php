<?php
/**
 * Functions necessary to the plugin
 * @package    User FSascicle
 */

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

################################################################################
// Funzioni 
################################################################################
function uf_CreaTabella($Tabella){
	global $wpdb;
		switch ($Tabella){
			case $wpdb->table_name_Fascicle:
				$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->table_name_Fascicle." (
				  `Id` int(11) NOT NULL auto_increment,
				  `Name` varchar(255) NOT NULL,
				  `Path` varchar(255) NOT NULL,
				  `Opening_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `User_id` bigint(20) unsigned NOT NULL,
				  PRIMARY KEY  (`Id`),
				  FOREIGN KEY (User_id) REFERENCES " . $wpdb->table_name_Users . "(`id`));";
				break;
			case $wpdb->table_name_Document:
				$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->table_name_Document." (
				  `Id` int(11) NOT NULL auto_increment,
				  `Name` varchar(255) NOT NULL,
				  `Path` varchar(255) NOT NULL,
				  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `Subject` varchar(255) NOT NULL,
				  `Fascicle_id` int(11) NOT NULL,
				  PRIMARY KEY  (`Id`),
				  FOREIGN KEY (Fascicle_id) REFERENCES " . $wpdb->table_name_Fascicle . "(`id`));";
				break;
		}
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}

function uf_insert_fascicle($FolderName,$FolderPath,$UserId){
	global $wpdb;
	$Sql="SELECT user_id FROM $wpdb->table_name_Fascicle WHERE User_id=%d;";
	$Res=$wpdb->get_results($wpdb->prepare($Sql,$UserId));
	if(count($Res)==0){
		if ( false === $wpdb->insert($wpdb->table_name_Fascicle,
			array('Name' => $FolderName,
			   'Path'  => $FolderPath,
			   'User_id' => $UserId),
			array('%s','%s','%d')))
				return FALSE;
		else
				return TRUE;	
	}else {
		if ( $wpdb->update($wpdb->table_name_Fascicle,
								array('Name' => $FolderName,
								'Path' => $FolderPath),
								array( 'User_id'   => $UserId),
								array( '%s','%s'),
								array( '%d')))
				return TRUE;
			else
				return FALSE;
	}
}

function uf_get_fascicle_path($User_id){
	global $wpdb;
	return $wpdb->get_results("SELECT Path
							   FROM $wpdb->table_name_Fascicle WHERE 
							   User_id=$User_id");
}

function uf_get_protocol_field_number($form_id){
	global $wpdb;
	return $wpdb->get_var( "SELECT id FROM {$wpdb->prefix}nf3_fields WHERE `label` = 'protocolnumber' AND `parent_id` = {$form_id}" );
}

function uf_get_fascicle_id($User_id){
	global $wpdb;
	return $wpdb->get_results("SELECT Id
							   FROM $wpdb->table_name_Fascicle WHERE 
							   User_id=$User_id");
}

function uf_insert_document($FileName,$FilePath,$Subject,$Fascicle_Id, $Protocol_Number=null){
	global $wpdb;
	$Sql="SELECT Fascicle_id FROM $wpdb->table_name_Document WHERE Fascicle_id=%d AND Name=%s;";
	$Res=$wpdb->get_results($wpdb->prepare($Sql,$Fascicle_Id,$FileName));
	if(count($Res)==0){
		if ( false === $wpdb->insert($wpdb->table_name_Document,
			array('Name' => $FileName,
			   'Path'  => $FilePath,
			   'Subject' => $Subject,
			   'Fascicle_id' => $Fascicle_Id,
			   'Protocol_Number' => $Protocol_Number),
			array('%s','%s','%s','%d','%s')))
				return FALSE;
		else
				return TRUE;	
	}
	else {
		if ( $wpdb->update($wpdb->table_name_Document,
								array('Name' => $FileName,
								'Path' => $FilePath,
								'Subject' => $Subject,
								'Protocol_Number' => $Protocol_Number,),
								array( 'Fascicle_id'   => $Fascicle_Id),
								array( '%s','%s','%s','%s'),
								array( '%d')))
				return TRUE;
			else
				return FALSE;
	}
}

function uf_get_all_documents($fascicle_id,$Conteggio=false,$DaRiga=0,$ARiga=20){
		global $wpdb;
		if ($DaRiga==0 AND $ARiga==0)
			$Limite="";
		else
			$Limite=" Limit ".$DaRiga.",".$ARiga;

		if ($Conteggio){
			return $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->table_name_Document WHERE Fascicle_id = " . $fascicle_id . ";");
		}else{
			return $wpdb->get_results("SELECT * FROM $wpdb->table_name_Document WHERE Fascicle_id = $fascicle_id $Limite;");	
		}
	}	

function uf_get_documents_by_name($fascicle_document_name){
		global $wpdb;
		return $wpdb->get_results("SELECT Path FROM $wpdb->table_name_Document WHERE Name = '" . $fascicle_document_name . "';");	
	}
	
function uf_VisualizzaData($dataDB) {
		$dataDB=substr($dataDB,0,10);
		$rsl = explode ('-',$dataDB);
		$rsl = array_reverse($rsl);
		return implode($rsl,'/');
	}

function uf_ExtensionType($fileName) {
		$NomeFile=explode(".", $fileName);
		$ext=strtolower($NomeFile[count($NomeFile)-1]);
		if (!ap_isExtensioType($ext)){
			return "pdf";
		}
	  return $ext;
	}

	
function uf_path_url($File){
		$PathUploads = wp_upload_dir(); 
		$allegato=$PathUploads['baseurl']."/".strstr($File, "AllegatiAttiAlboPretorio");
		return str_replace("\\","/",$allegato);
		
	}

	
function uf_existFieldInTable($Tabella, $Campo){
		global $wpdb;
		$ris=$wpdb->get_row("SHOW COLUMNS FROM $Tabella LIKE '$Campo'", ARRAY_A);
		if(count($ris)>0 ) 
			return true;
		else
			return false;	
	}


function uf_AggiungiCampoTabella($Tabella, $Campo, $Parametri){
		global $wpdb;
		if ( false === $wpdb->query("ALTER TABLE $Tabella ADD $Campo $Parametri")){
			return new WP_Error('db_insert_error', 'Non sono riuscito a creare il campo '.$Campo.' Nella Tabella '.$Tabella.' Errore '.$wpdb->last_error, $wpdb->last_error);
		} else{
			return true;
		}
}
function uf_delete_user_fascicle_document($Fascicle_id){
		global $wpdb;
		$Sql="DELETE FROM $wpdb->table_name_Document WHERE	Fascicle_id=%d";
		$Sql=$wpdb->prepare( $Sql,$Fascicle_id);
		$Res=$wpdb->query($Sql);
		if($Res!==FALSE And $Res==0){
			return TRUE;
		}else{
			return FALSE;
		}
}

function uf_delete_user_fascicle($IdFascicle){
	global $wpdb;
	$Sql="DELETE FROM $wpdb->table_name_Fascicle WHERE	Id=%d";
	$Sql=$wpdb->prepare( $Sql,$IdFascicle);
	$Res=$wpdb->query($Sql);
	if($Res!==FALSE And $Res==0){
		return TRUE;
	}else{
		return FALSE;
	}
}

function remove_fascicle_user_data($user_id ) {
	$fascicle = uf_get_fascicle_id($user_id);
	if(sizeof($fascicle) > 0){
		$fascicle_id = $fascicle[0]->Id;
		uf_delete_user_fascicle_document($fascicle_id);
		uf_delete_user_fascicle($fascicle_id);
	}
}

add_action( 'delete_user', 'remove_fascicle_user_data', 10 );


//remove_action('admin_notices', 'update_nag');


// function redirect_dashboard () {

// 	if (strpos($_SERVER['REQUEST_URI'] , 'wp-admin/index.php?') !== false) {
// 		wp_redirect( get_option( 'siteurl' ) . '/');
// 	}

// }
//add_action('admin_menu', 'redirect_dashboard');
//add_filter( 'login_redirect', function() { return site_url('/'); } );

// function custom_toolbar_link($wp_admin_bar) {
// 	$href = get_option( 'home' ) . '?action=visualizzfascicolo';
// 	if (strpos($_SERVER['REQUEST_URI'] , 'wp-admin') !== false) {
// 		$href = get_option( 'siteurl' ) . '/?action=visualizzfascicolo';
// 	}
// 	$args = array(
// 			'id' => 'visualizzafascicolo',
// 			'title' => 'Visualizza Fascicolo', 
// 			'href' => $href, 
// 			'meta' => array(
// 					'class' => 'ab-top-secondary ab-top-menu', 
// 					'title' => 'Visualizza Fascicolo'
// 					)
// 	);
// 	$wp_admin_bar->add_node($args);
// }
// add_action('admin_bar_menu', 'custom_toolbar_link', 999);

?>
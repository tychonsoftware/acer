<?php
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

if (!class_exists('WP_List_Table')) {
 require_once(ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

class AdminTableVisualizza extends WP_List_Table
{
    
    public $fascicolo="Visualizza Fascicolo";
    function __construct() {
        parent::__construct(array('singular'=>'Fascicolo','plural'=>'Fascicole'));
    }
    function prepare_items()
    {
        global $wpdb;
        $columns  = $this->get_columns();
        $hidden   = $this->get_columns_hidden();
        $sortable = $this->get_columns_sortable();
        $this->_column_headers = array($columns,$hidden,$sortable);
        $user = get_current_user_id();
	    // $screen = get_current_screen();
	    // $screen_option = $screen->get_option('per_page', 'option');
        // $per_page = get_user_meta($user, $screen_option, true);
        // if ( empty ( $per_page) || $per_page < 1 ) {
        //     $per_page = $screen->get_option( 'per_page', 'default' );
        // }
        // if (!is_numeric($per_page))
        //     $per_page = 10;
        $per_page = 2;
        if (!isset($_REQUEST['paged'])) 
            $paged = 0;
          else $paged = max(0,(intval($_REQUEST['paged'])-1)*$per_page);
    
        if (isset($_REQUEST['orderby'])and in_array($_REQUEST['orderby'],array_keys($sortable)))
            $orderby = $_REQUEST['orderby']; 
        else
            $orderby ="Anno DESC, Numero DESC , Data DESC";
    
        if (isset($_REQUEST['order']) and in_array($_REQUEST['order'],array('asc','desc')))
            $order = $_REQUEST['order']; 
        else $order = '';
    
               
        $fascicle_id = uf_get_fascicle_id($user)[0]->Id;
        $total_items = uf_get_all_documents($fascicle_id,true);

        
        $this->items = uf_get_all_documents($fascicle_id);
    //     $this->set_pagination_args(array(
    //     'total_items' => $total_items,
    //     'per_page'    => $per_page,
    //     'total_pages' => ceil($total_items/$per_page)
    //   ));
    }

    function get_columns()
    {
        $columns = array(
		    'NomeDocumento' => 'Nome Documento',
		    'DataDocumento' => 'Data Documento',
		    'Oggetto'       => 'Oggetto',
            'Document'      => 'Documento');
            
        return $columns;
    }

    function get_columns_hidden() {
        return array();  
    }

    function get_columns_sortable() {
        return array();  
    }

    function column_default($item,$column_name) { 
        return $item->$column_name; 
      }

    function column_Oggetto($item) { 
        $Oggetto=stripslashes($item->Subject);
        if ( strlen( $Oggetto ) > 120 ) {
              $Oggetto = substr( $Oggetto, 0, 120 ) . " ...";
          }
      return $Oggetto; 
    }

    function column_NomeDocumento($item) { 
        $nome=stripslashes($item->Name);
        if ( strlen( $nome ) > 120 ) {
              $nome = substr( $nome, 0, 120 ) . " ...";
          }
      return $nome; 
    }

    function column_DataDocumento($item) { 
        return uf_VisualizzaData($item->Date); 
    }

    function column_Document($item) { 
        $Estensione=uf_ExtensionType($item->Path);
        $TipidiFiles["pdf"] = array("Descrizione" => "File Pdf", "Icona" => UF_URL . "img/Pdf.png", "Verifica" => "");
        $html = '<div style="float: left;display: inline;width: 40px;height: 40px;padding-top:5px;padding-left:5px;">';
        $html .= '<a class="vfpdf" href="' . UF_URL . 'DowloadDocument.php?action='. $item->Path.'"> <img src="'.$TipidiFiles[strtolower($Estensione)]['Icona'].'" alt="'.$TipidiFiles[strtolower($Estensione)]['Descrizione'].'" height="30" width="30"allegato/></a></div><div style="clear:both;"></div> ';
        return $html;
    }

}
if(isset($_REQUEST['downloadaction'])){
    $file_path = $_REQUEST['downloadaction'];
    header('Content-Description: File Transfer');
    header('Content-Type: application/force-download');
    header("Content-Disposition: attachment; filename=\"" . basename($file_path) . "\";");
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_path));
    ob_clean();
    flush();
    readfile($file_path); //showing the path to the server where the file is to be download
}else {
    //Lista_Fascicolo();
}

unset($_REQUEST['downloadaction']);

function Lista_Fascicolo1(){

    echo' <div class="wrap">
	<div class="heading">';

    $tablenew = new AdminTableVisualizza();
    $tablenew->fascicolo;
    $tablenew->prepare_items();
    $page = filter_input(INPUT_GET,'page' ,FILTER_SANITIZE_STRIPPED);
    $paged = filter_input(INPUT_GET,'paged',FILTER_SANITIZE_NUMBER_INT);
    $Titolo="Visualizza Fascicolo";
    echo '<h3>'.$Titolo.'</h3>
    </div>
    <div class="wrap">
        <form id="fascicolo-table" method="GET">
            <input type="hidden" name="page" value="'.$_REQUEST['page'].'" />
		  	<input type="hidden" name="paged" value="'.$paged.'"/>';
	$tablenew->display(); 
	echo '</form>
    </div>
    </div>';
}

function Lista_Fascicolo_FrontEnd(){
    $Contenuto='';
    $Contenuto.=' <div class="wrap">
	<div class="heading">';

    $tablenew = new AdminTableVisualizza();
    $tablenew->fascicolo;
    $tablenew->prepare_items();
    $page = filter_input(INPUT_GET,'page' ,FILTER_SANITIZE_STRIPPED);
    $paged = filter_input(INPUT_GET,'paged',FILTER_SANITIZE_NUMBER_INT);
    $Titolo="Visualizza Fascicolo";
    $Contenuto.= '<h3>'.$Titolo.'</h3>
    </div>
    <div class="wrap">';
    $Contenuto.= $tablenew->display(); 
	$Contenuto.= '</form>
    </div>
    </div>';
    return $Contenuto;
}

?>
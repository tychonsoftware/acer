<?php
/**
 * @wordpress-plugin
 * Plugin Name:       User Fascicle
 * Description:       For each new user a new object Fascicole is created
 * Version:           1.00
 * Text Domain:       user-fascicle
 */

if (preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) {
	die('You are not allowed to call this page directly.');
}

include_once(dirname(__FILE__) . '/UserFascicleFunctions.php');	
define("UF_URL", plugin_dir_url(dirname(__FILE__) . '/UserFascicle.php'));

if (!class_exists('UserFascicle')) {
    class UserFascicle
    {
		var $version;
        var $minium_WP   = '3.1';
        	
		function __construct()
		{
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            
            if( !class_exists( 'AlboPretorio' ) ) {
                deactivate_plugins( plugin_basename( __FILE__ ) );
                wp_die( __( 'Please install and Activate Albo Pretorio On line.', 
                    'albopretorio-addon-slug' ), 'Plugin dependency check', array( 'back_link' => true ) );
            }
            $plugins = get_plugins("/" . plugin_basename(dirname(__FILE__)));
            $plugin_nome = basename((__FILE__));
            $this->version = $plugins[$plugin_nome]['Version'];
            $this->define_tables();
            $this->plugin_name = plugin_basename(__FILE__);
            register_activation_hook(__FILE__, array('UserFascicle', 'activate'));
            register_deactivation_hook(__FILE__, array('UserFascicle', 'deactivate'));
            register_uninstall_hook(__FILE__, array('UserFascicle', 'uninstall'));

            add_action('admin_enqueue_scripts', array('UserFascicle', 'User_Fascicle_Enqueue_Scripts'));
            add_action('init', array('UserFascicle', 'init'));
            add_action( 'user_register', array('UserFascicle', 'create_fascicle_object'),10,1);
            add_filter('set-screen-option', array('UserFascicle', 'fascicle_set_option'), 10, 3);
        }

        function define_tables()
        {
            global $wpdb, $table_prefix;
            $wpdb->table_name_Fascicle = $table_prefix . "userfascicle_fascicle";
            $wpdb->table_name_Document = $table_prefix . "userfascicle_Document";
            $wpdb->table_name_Users = $table_prefix . "users";
        }
        static function init()
	    {

        }

        static function add_menu()
	    {
            $fascicolo_page = add_submenu_page('index.php', 'Visualizza Fascicolo', 'Visualizza Fascicolo', 'gest_atti_albo', 'visualizz_fascicolo', array('UserFascicle', 'show_menu'));
            add_action("load-$fascicolo_page", array('UserFascicle', 'screen_option'));
        }

        static function screen_option()
	    {
            if (!isset($_GET['action'])) {
                $args = array(
                    'label'   => 'Fascicle per pagina',
                    'default' => 25,
                    'option'  => 'fascicle_per_page'
                );
                add_screen_option('per_page', $args);
            }
        }
        static function fascicle_set_option($status, $option, $value)
        {
            if ('fascicle_per_page' == $option)
                return $value;
        }
    

        static function show_menu()
	    {
            global $UF_OnLine;
            include_once(dirname(__FILE__) . '/VisualizzaFascicolo.php');
        
        }
        
        static function activate()
        {
            global $wpdb;
            if (!function_exists('get_plugins'))
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');

            if (get_option('opt_AP_Versione')  == '' || !get_option('opt_AP_Versione')) {
                add_option('opt_AP_Versione', '0');
            }
            $PData = get_plugin_data(__FILE__);
            $PVer = $PData['Version'];
            update_option('opt_AP_Versione', $PVer);
         
            uf_CreaTabella($wpdb->table_name_Fascicle);
            uf_CreaTabella($wpdb->table_name_Document);

            if (!uf_existFieldInTable($wpdb->table_name_Document, "Protocol_Number")) {
                uf_AggiungiCampoTabella($wpdb->table_name_Document, "Protocol_Number", " CHAR(100) DEFAULT NULL");
            }

        }

        
        static function deactivate()
        {
            if (!current_user_can('activate_plugins'))
                return;
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");
            flush_rewrite_rules();
        }
        static function uninstall()
        {
            global $wpdb;
            $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->table_name_Document);
            $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->table_name_Fascicle);
            
        }

        static function create_fascicle_object( $user_id ) {
            $user = new WP_User($user_id);
            $user_email = stripslashes($user->user_email);
            $ROOT_PATH = FASCICLE_ROOT_PATH;
           
            if (!file_exists($ROOT_PATH)) {
                mkdir($ROOT_PATH, 0755, true);
            }
            $dirs = scandir($ROOT_PATH, 1);
            $next_folder_name = "1";
            if(count($dirs) == 0){
                mkdir($ROOT_PATH . DIRECTORY_SEPARATOR . $next_folder_name, 0755, TRUE);
            }else {
                $last_folder_name = reset($dirs);
                $last_folder = $ROOT_PATH  . DIRECTORY_SEPARATOR . $last_folder_name;
                $num_files = count(scandir($last_folder))-2;
                if($num_files >= 1000){
                    $next_folder_name = (((int)$last_folder_name) + 1);
                    mkdir($ROOT_PATH . DIRECTORY_SEPARATOR . $next_folder_name, 0755, true);
                }
            }

            mkdir($ROOT_PATH . DIRECTORY_SEPARATOR . $next_folder_name . DIRECTORY_SEPARATOR . $user_email , 0755, true);
    
            uf_insert_fascicle($user_email,$ROOT_PATH . DIRECTORY_SEPARATOR . $next_folder_name . DIRECTORY_SEPARATOR . $user_email,$user_id);
        }

        static function User_Fascicle_Enqueue_Scripts($hook_suffix)
		{
            $path = plugins_url('', __FILE__);
            wp_register_style('UserFascicle', $path . '/css/style.css');
            wp_enqueue_style('UserFascicle');
            //wp_register_style('ufbootstrap', $path . '/css//bootstrap-italia.min.css');
            //wp_enqueue_style('ufbootstrap');
        }
       
       function store_pdf_fascicle_folder_delete($attachments, $form_name, $sub_id=null, $form_id=null) {
            if ( WP_DEBUG === true ) {
                error_log("Inside store_pdf_fascicle_folder ");
                error_log("Sub Id ", $sub_id);
                error_log("Form Id ", $form_id);
            }

            $protocol_field_number = uf_get_protocol_field_number($form_id);
            $protocol_number = get_post_meta( $sub_id,'_field_' . $protocol_field_number, true );
            if ( WP_DEBUG === true ) {
                error_log("protocol_field_number " . $protocol_field_number);
                error_log("protocol_number ". $protocol_number);
            }
            $pdf_file_path = '';
            foreach ($attachments as $attachment) {
                if(strpos($attachment,'.pdf') != false){
                    $pdf_file_path = $attachment;
                }
            }
            if(!empty($pdf_file_path)){
                $pdf_file_name =  basename($pdf_file_path);
                $user = wp_get_current_user();
                $user_name = $user->display_name;
                $user_first_name = trim($user_name);
                $user_last_name = (strpos($user_name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $user_name);
                $user_first_name = trim( preg_replace('#'.$user_last_name.'#', '', $user_name ) );
				
				if(isset($user)){
					$fascicle_path_array = uf_get_fascicle_path($user->ID);
					error_log("USER FASCICLE: LOGGATO");
				} else {
					$fascicle_path_array = uf_get_fascicle_path(0);
					error_log("USER FASCICLE: NON LOGGATO");
				}
                if(sizeof($fascicle_path_array) > 0){
                    $fascicle_path = $fascicle_path_array[0]->Path;
                    if (!file_exists($fascicle_path)) {
                        mkdir($fascicle_path, 0755, true);
                    }
                    $fascicle_file_path = $fascicle_path . DIRECTORY_SEPARATOR . $pdf_file_name;
                    if (!file_exists($fascicle_file_path)){
                        if (!copy($pdf_file_path, $fascicle_file_path)) {
                            error_log($pdf_file_path . " not copied to " . $fascicle_path);
                        }else {
                            $fascicle_id = uf_get_fascicle_id($user->ID)[0]->Id;
                            $subject= $form_name . ' - ' . $user_last_name . ' - '. $user_first_name . ' - ' . $user->user_email;
                            uf_insert_document($pdf_file_name,$fascicle_path . DIRECTORY_SEPARATOR . $pdf_file_name,$subject,$fascicle_id, $protocol_number);
                        }
                    }
                }else {
                    error_log("No Fascicle object defined for user " . $user->display_name);
                }
            }
          
        }

        function store_pdf_fascicle_folder($pdf_file_path, $form_name, $sub_id=null, $form_id=null) {
            if ( WP_DEBUG === true ) {
                error_log("Inside store_pdf_fascicle_folder ");
                error_log("Sub Id ". $sub_id);
                error_log("Form Id ". $form_id);
                error_log("Form Name ". $form_name);
                error_log("pdf_file_path ". print_r($pdf_file_path,true));
            }

            $protocol_field_number = uf_get_protocol_field_number($form_id);
            $protocol_number = get_post_meta( $sub_id,'_field_' . $protocol_field_number, true );
            if ( WP_DEBUG === true ) {
                error_log("protocol_field_number " . $protocol_field_number);
                error_log("protocol_number ". $protocol_number);
            }
            if(!empty($pdf_file_path)){
                $pdf_file_name =  basename($pdf_file_path);
                $user = wp_get_current_user();
                $user_name = $user->display_name;
                $user_first_name = trim($user_name);
                $user_last_name = (strpos($user_name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $user_name);
                $user_first_name = trim( preg_replace('#'.$user_last_name.'#', '', $user_name ) );

                if(isset($user)){
					$fascicle_path_array = uf_get_fascicle_path($user->ID);
					error_log("USER FASCICLE: LOGGATO");
				} else {
					$fascicle_path_array = uf_get_fascicle_path(0);
					error_log("USER FASCICLE: NON LOGGATO");
				}
				
                if(sizeof($fascicle_path_array) > 0){
                    $fascicle_path = $fascicle_path_array[0]->Path;
                    if (!file_exists($fascicle_path)) {
                        mkdir($fascicle_path, 0755, true);
                    }
                    $fascicle_file_path = $fascicle_path . DIRECTORY_SEPARATOR . $pdf_file_name;
                    if (!file_exists($fascicle_file_path)){
                        if (!copy($pdf_file_path, $fascicle_file_path)) {
                            error_log($pdf_file_path . " not copied to " . $fascicle_path);
                        }else {
                            $fascicle_id = uf_get_fascicle_id($user->ID)[0]->Id;
                            $subject= $form_name . ' - ' . $user_last_name . ' - '. $user_first_name . ' - ' . $user->user_email;
                            uf_insert_document($pdf_file_name,$fascicle_path . DIRECTORY_SEPARATOR . $pdf_file_name,$subject,$fascicle_id, $protocol_number);
                        }
                    }
                }else {
                    error_log("No Fascicle object defined for user " . $user->display_name);
                }
            }
          
        }
    }
    global $UF_OnLine;
    $UF_OnLine = new UserFascicle();
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <?php wp_head(); ?>
   </head>
   <body <?php body_class(); ?>>
      <div id="wrapper" class="hfeed">

         <header id="header" class="" role="banner">
<?php /*
            <section class="branding-up">
               <div class="container">
                  <div class="row">
                     <div class="col">
                        <!-- <img alt="" src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>"> -->
                        <img alt="" src="<?php header_image(); ?>" width="200">
                     </div>
                     <div class="col text-right">
                        <?php wp_nav_menu(array( 'theme_location' => 'menu-language', 'container' => 'ul', 'menu_class' => 'nav float-right' )); ?>
                     </div>
                  </div>
               </div>
            </section> */ ?>

            <section class="branding">
               <div class="container">
                  <div class="row">
                     <div class="col-12 col-lg-6">

                        <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        if ( has_custom_logo() ) {
                           echo '<a href="' . get_home_url() . '"><img class="custom-logo" src="'. esc_url( $logo[0] ) .'"></a>';
                        } else {
                           echo '<a href="' . get_home_url() . '"><img class="custom-logo" src="'. get_template_directory_uri() . '/img/Logo_Sito.png' .'"></a>';
                        } ?>

                     	<?php //wppa_the_custom_logo(); ?>
                     </div>
                     <div class="col-lg-2"></div>
                     <div class="col-lg-4">
                        <div class="container">
                           <div class="row">
                               <div class="col search-form">
                                   <?php get_search_form(); ?>
                               </div>
                               <!-- <div class="w-100 d-none d-lg-block"></div> -->
                               <div class="menu-social">
                                   <!-- <p>Social</p> -->
                                   <?php wp_nav_menu( array( 'theme_location' => 'menu-social', 'container' => 'ul', 'menu_class' => 'nav')); ?>
                               </div>
                           </div>
                        </div>

                     </div>
					 <div class="col">

                        <div id="site-description" style="padding-left:100px"><?php bloginfo( 'description' ); ?></div>
                     </div>
                  </div>
               </div>
            </section>

            <nav class="menu-main" role="navigation">
               <div class="container">
                  <div class="row justify-content-center">
                     <label for="show-menu-main" class="show-menu-main">Menu</label>
                     <input type="checkbox" id="show-menu-main" role="button">
                     <?php wp_nav_menu(array( 'theme_location' => 'menu-main', 'container' => 'ul', 'menu_class' => 'nav' )); ?>
                  </div>
                  <!-- <div class="row">
                     <?php wp_nav_menu(array( 'theme_location' => 'menu-secondary', 'container' => 'ul', 'menu_class' => 'nav' )); ?>
                  </div> -->
               </div>

                <!-- This's location for widget Slider -->
                <?php
                    // global $wp;
                    // $currentUrl = home_url(add_query_arg(array($_GET), $wp->request));
                    // $arrExplodeUrl = explode_url($currentUrl);
                    // $path = $arrExplodeUrl['path'];
                ?>
                <!-- Just display slider at homepage -->
                <?php if ( is_active_sidebar( 'header-slider-widget-area' ) && is_front_page() ) : ?>
                    <div class="header-slider-widget-area">
                            <?php dynamic_sidebar( 'header-slider-widget-area' ); ?>
                    </div>
                <?php endif; ?>
            </nav>
         </header>
         <div id="container">

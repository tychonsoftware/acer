<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<?php if ( !is_search() ) get_template_part( 'entry-footer' ); ?>
		<?php if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
		<div class="at-list" style="width: 100%"><div class="at-no-img"></div><div class="at-info">
		<?php
			if ( is_singular() )
			{
				echo '<h1 class="entry-title" style="line-height: 0.8 !important;">';
			} else
			{
				echo '<h4 class="entry-title" style="line-height: 0.8 !important;">';
			}
		?>
		<strong>
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</strong>
		<?php if ( is_singular() ) { echo '</h1>'; } else { echo '</h4>'; } ?>

		<?php
		$expiration_date = get_post_meta( get_the_ID(), '_atexpirationdate', true );
		$date_now = null;
		if(!empty($expiration_date)){
		  $date = strtotime($expiration_date);
		  $formatteddate = date('d/m/Y',$date);
		}
		?>
 		<span class="atdateOne"><?php echo get_the_date('d/m/Y'); ?></span>
        <span class="atdateOne" style="margin-left:25%"><?php echo $formatteddate; ?></span>
		<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
		<?php edit_post_link(); ?>

		<?php if ( is_singular() ) { wppa_breadcrumb(); } ?>
		</div></div>
	</header>

</article>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:700' rel='stylesheet' type='text/css'>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        jQuery("document").ready(function($){
            resetMenuPosition();
            window.addEventListener("resize", function() {
                resetMenuPosition();
            });
            document.addEventListener('visibilitychange', function(ev) {
                resetMenuPosition();
            });
            var childrens = $('.top-section .panel-layout').children();
            $.each(childrens, function( k, v ) {
                if(k<(childrens.length-1)) {
                    v.remove();
                }
            });
            $('.top-section').show();
            setInterval(autoslide,2000);
        });
        function resetMenuPosition() {
            var menutp = $(window).width()<1025 ? ($('#menu-menu-principale').height()>50 ? '-1.2em' : '-2.9em') : ($('#menu-menu-principale').height()>50 ? '-1.2em' : '0px');
            $('#menu-menu-principale').css('margin-top', menutp);
        }
        function autoslide() {
            $(".top-section .sow-carousel-next").click();
            var childrens = $('.top-section .panel-layout').children();
            $.each(childrens, function( k, v ) {
                var t = $(v).find('.sow-carousel-item').length-1;
                var c = $(v).find('.sow-carousel-item.slick-current.slick-active').attr('data-slick-index');
                if(c==t) {
                    $(v).find('.sow-carousel-wrapper').children().slick("slickGoTo",0);
                }
            });
        }
    </script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">

    <header id="header" class="" role="banner">
        <?php /*
            <section class="branding-up">
               <div class="container">
                  <div class="row">
                     <div class="col">
                        <!-- <img alt="" src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>"> -->
                        <img alt="" src="<?php header_image(); ?>" width="200">
                     </div>
                     <div class="col text-right">
                        <?php wp_nav_menu(array( 'theme_location' => 'menu-language', 'container' => 'ul', 'menu_class' => 'nav float-right' )); ?>
                     </div>
                  </div>
               </div>
            </section> */ ?>

        <section class="branding">
            <div class="container">
                <div class="row branding-row">
                    <div class="col-12 col-lg-6">

                        <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                        if ( has_custom_logo() ) {
                            echo '<a href="' . get_home_url() . '"><img class="custom-logo" src="'. esc_url( $logo[0] ) .'"></a>';
                        } else {
                            echo '<a href="' . get_home_url() . '"><img class="custom-logo" src="'. get_template_directory_uri() . '/img/Logo_Sito.png' .'"></a>';
                        } ?>

                        <?php //wppa_the_custom_logo(); ?>
                    </div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <div class="container">
                            <div class="row col-10" style="float: right;">
                                <div class="col-7 search-form">
                                    <?php get_search_form(); ?>
                                </div>
                                <!-- <div class="w-100 d-none d-lg-block"></div> -->
                                <div class="col-5 menu-social">
                                    <img class="user-img" src="<?php bloginfo('template_url'); ?>/img/user-bianco.png" />
                                    <!-- <p>Social</p> -->
                                    <?php wp_nav_menu( array( 'theme_location' => 'menu-social', 'container' => 'ul', 'menu_class' => 'nav')); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col">

                        <div id="site-description" style="padding-left:100px"><?php bloginfo( 'description' ); ?></div>
                    </div>
                </div>
            </div>
        </section>

        <nav class="menu-main" role="navigation">
            <div class="container">
                <div class="row justify-content-center">
                    <label for="show-menu-main" class="show-menu-main">Menu</label>
                    <input type="checkbox" id="show-menu-main" role="button">
                    <?php
                    $menus = wp_get_nav_menus();
                    $menu_size = sizeof($menus);
                    foreach ( $menus as $menu_maybe ) {
                        $menu_items = wp_get_nav_menu_items( $menu_maybe->term_id, array( 'update_post_term_cache' => false ) );
                        if ( $menu_items ) {
                            break;
                        }
                    }
                    ?>
                    <?php wp_nav_menu(array( 'theme_location' => 'menu-main', 'container' => 'ul', 'menu_class' => 'nav' . ($menu_size > 7 ? ' add-mt' : '') )); ?>
                </div>
                <!-- <div class="row">
                     <?php wp_nav_menu(array( 'theme_location' => 'menu-secondary', 'container' => 'ul', 'menu_class' => 'nav' )); ?>
                  </div> -->
            </div>

            <!-- This's location for widget Slider -->
            <?php
            // global $wp;
            // $currentUrl = home_url(add_query_arg(array($_GET), $wp->request));
            // $arrExplodeUrl = explode_url($currentUrl);
            // $path = $arrExplodeUrl['path'];
            ?>
            <!-- Just display slider at homepage -->
            <?php if ( is_active_sidebar( 'header-slider-widget-area' ) && is_front_page() ) : ?>
                <div class="header-slider-widget-area">
                    <?php dynamic_sidebar( 'header-slider-widget-area' ); ?>
                    <section class="entry-content top-section" style="width: 40%;position: absolute;top: 130px;left: 14%;border-top: 7px solid #0084ff;display: none;">
                        <?php the_content(); ?>
                    </section>
                </div>
            <?php endif; ?>
        </nav>
    </header>
    <div id="container">

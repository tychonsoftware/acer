<?php get_header(); ?>
<section id="content" role="main" class="col-12">
   <div class="col-12">
      <div class="row f-calibri">
          <?php $post_type = get_post_type( get_the_ID()); ?>
          <?php if($post_type === 'amm-trasparente') {?>
      <div class="col-md-10 w-78">
		   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		   <?php get_template_part( 'entry' ); ?>
		   <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
		   <?php endwhile; endif; ?>
	   </div>
      <div class="col-md-3 w-21">
         <?php get_sidebar(); ?>
      </div>
          <?php } else {?>
              <div class="col-md-12">
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                      <?php get_template_part( 'entry' ); ?>
                      <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
                  <?php endwhile; endif; ?>
              </div>
          <?php }?>
      </div>
   </div>

   <!-- <footer class="footer">
      <?php get_template_part( 'nav', 'below-single' ); ?>
   </footer> -->
   <style>
      @media (min-width: 768px) {
         .w-78 {
            width: 78.446%;
            max-width: 78.446%;
         }
         .w-21 {
            width: 21.554%;
            max-width: 21.554%;
         }
      }
      .entry-content div h3 {
         font-size: 18px;
      }
      .entry-title a {
         font-size: 22px !important;
      }
      .f-calibri > * {
         font-family: Calibri !important;
      }
   </style>
</section>
<?php get_footer(); ?>

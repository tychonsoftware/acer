<?php get_header(); ?>
<section id="content" role="main">
   <div class="container">
      <div class="row">
	  
	  <div class="col-sm-3">
   		<?php if ( is_active_sidebar( 'page-widget-area' ) ) : ?>
   		<div class="container-fluid widget-area">
   		   <ul class="xoxo">
   		      <?php dynamic_sidebar( 'page-widget-area' ); ?>
   		   </ul>
   		</div>
   		<?php endif; ?>
	  </div>

      <div class="col-md-9">
	  <div class="searchPad">
            <?php if ( have_posts() ) {
				$srchPosts  = array(); ?>
            <?php while ( have_posts() ) { the_post();
			$postID = get_the_ID();
			$postSlug = get_post_field( 'post_name', $postID ); ?>
			<?php $srchPosts[$postSlug] = '<hr><article id="post-'. $postID .' '. $postSlug .'">
			<header><h4 class="entry-title">
			<a href="'. get_permalink($postID) .'" title="'. get_the_title($postID) .'" rel="bookmark">'. get_the_title($postID) .'</a>
			</h4></header>
			<section class="entry-summary">'. get_the_excerpt($postID) .'</section>
			</article>'; ?>
            <?php } ?>
            <?php } ?>
			<?php global $wpdb;
			$table_name = $wpdb->prefix.'albopretorio_atti';
			$srchKey = $_REQUEST['s'];
			$Selezione.=' WHERE Oggetto like "%'.$srchKey.'%"';
			$Selezione.=' OR Riferimento like "%'.$srchKey.'%"';
			$Selezione.=' OR Informazioni like "%'.$srchKey.'%"';
			$srchActs = $wpdb->get_results("SELECT * FROM $table_name $Selezione;");
			foreach($srchActs as $srchAct) {
				$postSlug = createSlug($srchAct->Riferimento);
				$srchPosts[$postSlug] = '<hr><article id="post-'. $srchAct->IdAtto .' '. $postSlug .'">
			<header><h4 class="entry-title">
			<a href="'. get_site_url() .'?action=visatto&id='. $srchAct->IdAtto .'" title="'. $srchAct->Riferimento .'" rel="bookmark">
			'. $srchAct->Riferimento .'</a>
			</h4></header>
			<section class="entry-summary">'. $srchAct->Oggetto .'</section>
			</article>';
			}
			 ?>
			
			<?php if (empty($srchPosts)) { ?>
            <article id="post-0" class="post no-results not-found">
               <header class="header">
                  <h2 class="entry-title"><?php _e( 'Nessun risultato', 'wppa' ); ?></h2>
               </header>
               <section class="entry-content">
                  <p><?php _e( 'La ricerca non ha prodotto risultati per i termini utilizzati.', 'wppa' ); ?></p>
                  <?php get_search_form(); ?>
               </section>
            </article>
            <?php } else { ?>
			<header class="header">
               <h1 class="entry-title"><?php printf( __( 'Risultato della ricerca per: %s', 'wppa' ), get_search_query() ); ?></h1>
            </header>
			<?php ksort($srchPosts);
				foreach($srchPosts as $key => $value) {
					echo $value;
				}
			} ?>

   		</div>
		</div>
	</div>
	</div>
</section>

<?php get_footer(); ?>
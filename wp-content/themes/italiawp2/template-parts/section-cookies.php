<?php
/*
 * ### SEZIONE COOKIES & PRIVACY ###
 *
 */
?>


<?php
  if (get_option('dettagli-num-articoli')) {
    $num_articoli = get_option('dettagli-num-articoli');
  } else {
    $num_articoli = 3;
  }
  $args = array(
    'posts_per_page' => ($num_articoli + 1),
    'orderby' => 'date',
    'order' => 'DESC'
  );

  $i = 0;
  $the_query = new WP_Query($args);
  if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();

  endwhile; endif;
  wp_reset_postdata();

?>

<?php if (!get_theme_mod('disactive_section_cookies')) : ?>

  <?php
  $page_id_cookie_banner = get_option('dettagli-id-cookie');
  if (!$page_id_cookie_banner) $page_id_cookie_banner = get_option('dettagli-id-privacy');
  ?>

  <div class="cookiebar hide u-background-80" aria-hidden="true">
    <p class="text-white">
      <?php echo __('This site uses technical, analytics and third-party cookies', 'italiawp2'); ?>.
      <?php echo __('By continuing to browse, you accept the use of cookies', 'italiawp2'); ?>.<br/>
      <button data-accept="cookiebar" class="btn btn-info mr-2 btn-verde">
        <?php echo __('I accept', 'italiawp2'); ?>
      </button>
      <a href="<?php echo get_permalink($page_id_cookie_banner); ?>"
         class="btn btn-outline-info btn-trasp"><?php echo __('Cookie policy', 'italiawp2'); ?></a>
    </p>
  </div>

<?php endif; ?>

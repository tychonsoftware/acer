import { Injectable } from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';
import {ResidenceType} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class ResidenceTypeService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<ResidenceType[]> => this.apiService.get('/residences/types');

  getByUUID = (uuid: string): Observable<ResidenceType> => this.apiService.get(`/residences/types/${uuid}`);

  create = (residenceType: ResidenceType): Observable<ResidenceType> => this.apiService.post('/residences/types', residenceType);

  update = (residenceType: ResidenceType): Observable<ResidenceType> => this.apiService.put('/residences/types', residenceType);

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/residences/types/${uuid}`);
}

import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {Residence} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class ResidenceService {
  private _residence = new BehaviorSubject<Residence>({} as Residence);
  private dataStore: { residence: Residence } = {residence: {} as Residence};
  readonly residence$ = this._residence.asObservable();

  constructor(private apiService: ApiService) {
  }

  get(): void {
    this.apiService.get('/residences').subscribe(
      data => {
        console.log('RESIDENCE RETURNED FROM GET');
        console.log(data);
        this.dataStore.residence = data;
        this._residence.next(Object.assign({}, this.dataStore).residence);
      },
      error => console.log('Could not load Residence.')
    );
  }

  getByUUID = (uuid: string): Observable<Residence> => this.apiService.get(`/residences/${uuid}`);

  create(residence: Residence): void {
    this.apiService.post('/residences', residence)
      .subscribe(
        data => {
          console.log('RESIDENCE RETURNED FROM POST');
          console.log(data);
          this.dataStore.residence = data;
          this._residence.next(Object.assign({}, this.dataStore).residence);
        },
        error => console.log('Could not create Residence.')
      );
  }

  update(residence: Residence): void {
    this.apiService.put('/residences', residence)
      .subscribe(
        data => {
          console.log('RESIDENCE RETURNED FROM PUT');
          console.log(data);
          this.dataStore.residence = data;
          this._residence.next(Object.assign({}, this.dataStore).residence);
        },
        error => console.log('Could not create Residence.')
      );
  }

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/residences/${uuid}`);
}

import {Injectable} from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {File} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService extends ObservableApiService<File> {

  constructor(apiService: ApiService) {
    super('/files', apiService);
  }

  downloadFile = (path: string): Observable<any> => this.apiService.get(`${this.apiPath}/download`, new HttpParams().append('path', path), 'text');

  placeHolding = (uuid: string, html: string): Observable<any> => this.apiService.get(`${this.apiPath}/placeHolding`,
    new HttpParams().append('guestUuid', uuid).append('html', html), 'text')
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Asl} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class AslService {

  constructor(private apiService: ApiService) {
  }
  
  update = (asl: any, registryIn: boolean): Observable<Asl> => this.apiService.put(`/asls/${registryIn}`, asl);

  getByRegistryIn = (registryIn: boolean): Observable<Asl[]> => this.apiService.get(`/asls/byRegistryIn/${registryIn}`);

  getAll = (): Observable<Asl[]> => this.apiService.get('/asls');

  getAslFromCity = (cityId: number): Observable<Asl[]> => this.apiService.get(`/asls/byCity/${cityId}`);

  checkRequiredInvoiceFields = (asl: any): Observable<boolean> => this.apiService.put(`/asls/checkRequiredFields`, asl);

}

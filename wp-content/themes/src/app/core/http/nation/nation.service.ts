import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Nation} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class NationService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<Nation[]> => this.apiService.get('/nations');
}

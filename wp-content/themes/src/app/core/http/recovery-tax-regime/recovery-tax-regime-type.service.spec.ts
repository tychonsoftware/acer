import { TestBed } from '@angular/core/testing';

import { RecoveryTaxRegimeTypeService } from './recovery-tax-regime-type.service';

describe('RecoveryTaxRegimeTypeService', () => {
  let service: RecoveryTaxRegimeTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecoveryTaxRegimeTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

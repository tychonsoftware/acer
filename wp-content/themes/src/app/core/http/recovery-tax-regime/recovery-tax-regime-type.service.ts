import { Injectable } from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';
import {RecoveryTaxRegimeType} from '@app/core/models';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecoveryTaxRegimeTypeService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<RecoveryTaxRegimeType[]> => this.apiService.get('/recoveryTaxRegimeTypes');

  getByOrganization = (): Observable<RecoveryTaxRegimeType[]> => this.apiService.get('/recoveryTaxRegimeTypes/byOrganization', new HttpParams());

}

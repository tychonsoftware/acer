import { TestBed } from '@angular/core/testing';

import { RecoveryTaxRegimeService } from './recovery-tax-regime.service';

describe('RecoveryTaxRegimeService', () => {
  let service: RecoveryTaxRegimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecoveryTaxRegimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

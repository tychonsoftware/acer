import {Injectable} from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {RecoveryTaxRegime} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class RecoveryTaxRegimeService extends ObservableApiService<RecoveryTaxRegime> {

  constructor(apiService: ApiService) {
    super('/recoveryTaxRegimes', apiService);
  }
}

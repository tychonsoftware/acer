import { TestBed } from '@angular/core/testing';

import { ParentAnaService } from './parent-ana.service';

describe('ParentAnaService', () => {
  let service: ParentAnaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParentAnaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

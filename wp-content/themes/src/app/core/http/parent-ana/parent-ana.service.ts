import {Injectable} from '@angular/core';
import {ParentAna} from '@app/core/models';
import {Observable} from 'rxjs';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class ParentAnaService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<ParentAna[]> => this.apiService.get('/parents/ana');

  getByUUID = (uuid: string): Observable<ParentAna> => this.apiService.get(`/parents/ana/${uuid}`);

  create = (guestTransition: ParentAna): Observable<ParentAna> => this.apiService.post('/parents/ana', guestTransition);

  update = (guestTransition: ParentAna): Observable<ParentAna> => this.apiService.put('/parents/ana', guestTransition);

  delete = (id: number): Observable<any> => this.apiService.delete(`/parents/ana/${id}`);
}

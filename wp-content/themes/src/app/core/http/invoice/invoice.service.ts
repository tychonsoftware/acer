import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {Invoice} from '@app/core/models';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService extends ObservableApiService<Invoice> {

  constructor(apiService: ApiService) {
    super('/invoice', apiService);
  }

  getAll = (): void => super.getAll();

  /**
   * Get all invoices  by period.
   * @param period Period of format mm/yyyy (e.g. 05/2021).
   */
  getByPeriod = (period: string): Observable<Invoice[]> => this.apiService.get(`${this.apiPath}/byPeriod`, 
		new HttpParams().append('period', period));
  
  generate = (period: string, recoveryTaxRegimeTypeId?: number): Observable<Invoice[]> => this.apiService.get(`${this.apiPath}/generate/${recoveryTaxRegimeTypeId}`, 
		new HttpParams().append('period', period));
  
  consolidate = (invoices: Invoice[], recoveryTaxRegimeTypeId?: number): Observable<Invoice[]> => this.apiService
		.put(`${this.apiPath}/consolidate`, invoices, new HttpParams().append('recoveryTaxRegimeType_id', String(recoveryTaxRegimeTypeId)));
  
  generatePdf = (invoices: Invoice[]): Observable<Invoice[]> => this.apiService.put(`${this.apiPath}/generatePdf`, invoices);

  generateXml = (invoices: Invoice[]): Observable<Invoice[]> => this.apiService.put(`${this.apiPath}/generateXml`, invoices);
		
  getByPeriodAndTaxRegime = (period: string, recoveryTaxRegimeTypeId?: number): Observable<Invoice[]> =>
    this.apiService.get(`${this.apiPath}/byPeriodAndRecovery`,
      new HttpParams().append('period', period).append('recoveryRegimeTaxId', String(recoveryTaxRegimeTypeId)))

  getAllByTaxRegime = (taxRegimeTypeId: number): Observable<Invoice[]> =>
    this.apiService.get(`${this.apiPath}/findAll/byTaxRegime/` + taxRegimeTypeId)
}

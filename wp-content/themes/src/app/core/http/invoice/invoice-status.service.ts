import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {AssistanceProjectStatus, InvoiceStatus} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';
import {AttendanceReason} from '@app/core/models/attendance/attendance-reason.model';

@Injectable({
  providedIn: 'root'
})
export class InvoiceStatusService extends ObservableApiService<InvoiceStatus>{

  constructor(apiService: ApiService) {
    super('/invoiceStatus', apiService);
  }
}

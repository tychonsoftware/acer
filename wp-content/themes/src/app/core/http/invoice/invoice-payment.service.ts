import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {AssistanceProjectStatus, InvoicePayment, InvoiceStatus} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';
import {AttendanceReason} from '@app/core/models/attendance/attendance-reason.model';

@Injectable({
  providedIn: 'root'
})
export class InvoicePaymentService extends ObservableApiService<InvoicePayment>{

  constructor(apiService: ApiService) {
    super('/invoicePayment', apiService);
  }
}

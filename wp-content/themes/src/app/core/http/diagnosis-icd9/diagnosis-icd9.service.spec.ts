import { TestBed } from '@angular/core/testing';

import { DiagnosisIcd9Service } from './diagnosis-icd9.service';

describe('DiagnosisIcd9Service', () => {
  let service: DiagnosisIcd9Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiagnosisIcd9Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

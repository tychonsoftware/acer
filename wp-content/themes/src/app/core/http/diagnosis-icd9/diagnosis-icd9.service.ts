import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {DiagnosisICD9} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class DiagnosisIcd9Service extends ObservableApiService<DiagnosisICD9>{

  constructor(apiService: ApiService) {
    super('/diagnosisICD9', apiService);
  }
}

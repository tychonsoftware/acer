import {Injectable} from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Dismiss} from '@app/core/models/guests/dismiss.model';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DismissService extends ObservableApiService<Dismiss> {

  constructor(apiService: ApiService) {
    super('/dismisses', apiService);
  }
  
  update(dismiss?: Dismiss): Observable<Dismiss> {
    return this.apiService.put('/dismisses', dismiss);
  }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {District} from '@app/core/models/shared/district.model';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<District[]> => this.apiService.get('/districts');

  getDistrictFromCity = (cityId: number): Observable<District[]> => this.apiService.get(`/districts/byCity/${cityId}`);
}

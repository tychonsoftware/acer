import {Injectable} from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {ApiService} from '@app/core/http/api.service';
import {PerformanceType} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class PerformanceTypeService extends ObservableApiService<PerformanceType> {

  constructor(apiService: ApiService) {
    super('/performanceTypes', apiService);
  }
}

import { TestBed } from '@angular/core/testing';

import { PerformanceChargeService } from './performance-charge.service';

describe('PerformanceChargeService', () => {
  let service: PerformanceChargeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerformanceChargeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

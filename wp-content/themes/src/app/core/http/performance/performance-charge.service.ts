import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {PerformanceCharge} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class PerformanceChargeService extends ObservableApiService<PerformanceCharge> {

  constructor(apiService: ApiService) {
    super('/performanceCharges', apiService);
  }
}

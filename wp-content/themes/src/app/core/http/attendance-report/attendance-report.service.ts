import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { AttendanceReport } from '@app/core/models/attendance-report/attendance-report.model';

@Injectable({
  providedIn: 'root'
})
export class AttendanceReportService extends ObservableApiService<AttendanceReport> {

  constructor(apiService: ApiService) {
    super('/attendances', apiService);
  }

  /**
   * Get all attendances by guest uuid.
   * @param uuid Guest UUID.
   */
  getAllAttendancesReports = (recoveryRegimeTaxTypeId: string, startAssistanceProject: string, endAssistanceProject: string):
    Observable<AttendanceReport[]> => this.getAllAsObservable(
    `reports`, new HttpParams().append('recoveryRegimeTaxTypeId', recoveryRegimeTaxTypeId).append('startAssistanceProject', startAssistanceProject).append('endAssistanceProject', endAssistanceProject))
}
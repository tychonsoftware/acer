import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '@app/core/http/api.service';
import {NatureInvoiceType} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class NatureInvoiceTypeService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<NatureInvoiceType[]> => this.apiService.get('/natureInvoiceTypes');

  getByUUID = (uuid: string): Observable<NatureInvoiceType> => this.apiService.get(`/natureInvoiceTypes/${uuid}`);

  create = (natureInvoiceType: NatureInvoiceType): Observable<NatureInvoiceType> => this.apiService.post('/natureInvoiceTypes', natureInvoiceType);

  update = (natureInvoiceType: NatureInvoiceType): Observable<NatureInvoiceType> => this.apiService.put('/natureInvoiceTypes', natureInvoiceType);

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/natureInvoiceTypes/${uuid}`);
}

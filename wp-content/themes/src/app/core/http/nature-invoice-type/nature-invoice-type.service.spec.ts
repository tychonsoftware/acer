import { TestBed } from '@angular/core/testing';

import { NatureInvoiceTypeService } from './nature-invoice-type.service';

describe('NatureInvoiceTypeService', () => {
  let service: NatureInvoiceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NatureInvoiceTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

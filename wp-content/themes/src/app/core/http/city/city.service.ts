import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {City} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<City[]> => this.apiService.get('/cities');

  getByArea = (areaId: number): Observable<City[]> => this.apiService.get(`/cities/byArea/${areaId}`);

  getByProvince = (provinceId: number): Observable<City[]> => this.apiService.get(`/cities/byProvince/${provinceId}`);

  getByRegion = (regionId: number): Observable<City[]> => this.apiService.get(`/cities/byRegion/${regionId}`);
}

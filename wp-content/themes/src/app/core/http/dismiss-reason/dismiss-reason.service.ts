import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {DismissReason} from '@app/core/models/guests/dismiss-reason.model';
import {ObservableApiService} from '@app/core/http/observable-api.service';

@Injectable({
  providedIn: 'root'
})
export class DismissReasonService extends ObservableApiService<DismissReason> {

  constructor(apiService: ApiService) {
	super('/dismissReasons', apiService);
  }

}

import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {FileH} from '@app/core/models';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileHService extends ObservableApiService<FileH> {

  constructor(apiService: ApiService) {
    super('/fileH', apiService);
  }

  getByPeriod = (buildingId:any, period: string): Observable<FileH> => this.apiService.get(`${this.apiPath}/generate`, new HttpParams().append('period', period).append('buildingId', buildingId));

  getHistoryByPeriod = (period: string): void => super.getAll('byPeriod', new HttpParams().append('period', period));

  getAll = (): void => super.getAll();

  getByYear = (year: string): void => super.getAll('byYear', new HttpParams().append('year', year));
  
  getByYearAndBuilding = (year: string, buildingId: any): void => super.getAll('byYearAndBuilding', new HttpParams().append('year', year).append('buildingId',buildingId));

  updateStatus = (fileHId: number, statusId: number): Observable<FileH> => this.apiService.put(`${this.apiPath}/updateStatus/${fileHId}`, {}, new HttpParams().append('statusId', String(statusId)));
}

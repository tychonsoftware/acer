import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '@app/core/http/api.service';
import {ValidationObject, ValidationResponse} from '@app/core/models/shared/validator.model';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor(private apiService: ApiService) {
  }

  checkCF = (data: ValidationObject): Observable<ValidationResponse> => this.apiService.post('/validator/checkCF', data);
}

import { Injectable } from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';
import {StructureType} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class StructureTypeService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<StructureType[]> => this.apiService.get('/structure/types');
}

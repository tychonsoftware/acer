import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {Organization} from '@app/core/models/shared/organization.model';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  public resourceUrl = environment.api_url + '/datasource';

  constructor(protected http: HttpClient) {
  }

  getByName(name: string): Observable<HttpResponse<Organization>> {
    let headers = new HttpHeaders();
    headers = headers.set('Database-Type', environment.tenant_name_commonDB);
    return this.http
      .get<Organization>(`${this.resourceUrl}/${name}`, {observe: 'response', headers});
  }
}

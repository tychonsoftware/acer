import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Observable} from 'rxjs';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrganizationBuildingsService extends ObservableApiService<OrganizationBuilding> {

  constructor(apiService: ApiService) {
    super('/organizationBuildings', apiService);
  }

  findAll(db?: string): Observable<OrganizationBuilding[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('isDataDbRequest', 'true');
    if (db) {
      httpParams = httpParams.set('isSpecialDataDb', db);
    }
    return this.getAllAsObservable(undefined, httpParams);
  }

  findRSABuildings(db?: string): Observable<OrganizationBuilding[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('isDataDbRequest', 'true');
    if (db) {
      httpParams = httpParams.set('isSpecialDataDb', db);
    }
    return this.getAllAsObservable('/rsa', httpParams);
  }

}

import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {AssistanceProjectStatus} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class AssistanceProjectStatusService extends ObservableApiService<AssistanceProjectStatus>{

  constructor(apiService: ApiService) {
    super('/assistanceProjectStatuses', apiService);
  }
}

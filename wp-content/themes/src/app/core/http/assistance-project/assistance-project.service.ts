import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {AssistanceProject} from '@app/core/models/assistance-projects/assistance-project.model';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseByPeriod} from '@app/core/models/assistance-projects/response-by-period.model';
import { AssistanceProjectFiltered } from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class AssistanceProjectService extends ObservableApiService<AssistanceProject> {

  constructor(apiService: ApiService) {
    super('/assistanceProjects', apiService);
  }

  /**
   * Get all assistance projects by period.
   * @param period Period of format mm/yyyy (e.g. 05/2021).
   */
  getByPeriod = (period: string): Observable<ResponseByPeriod> => this.apiService.get(`${this.apiPath}/byPeriod`, new HttpParams().append('period', period));

  getByPeriodAndBuildingNotValidate = (buildingId: any, period: string): Observable<ResponseByPeriod> => this.apiService.get(`${this.apiPath}/byPeriodAndBuildingNotValidate`, new HttpParams().append('period', period).append('buildingId', buildingId));

  putCalculateCharges = (assistanceSProjects: AssistanceProject[]): Observable<any> => this.apiService.put(
    `/assistanceProjects/calculateCharges`,
    assistanceSProjects
  )

  calculateTotalCharge = (period: string): Observable<any> => this.apiService.get(
    `/assistanceProjects/totalCharge/`,
    new HttpParams().append('period', period)
  )

  /**
   * Get all assistance projects by guest uuid.
   * @param uuid Guest UUID.
   */
  getByGuestUUID = (uuid: string): Observable<AssistanceProject[]> => this.getAllAsObservable(`byGuest/${uuid}`);

  getAll = (): void => super.getAll();
  // TODO: use only for mock server
  // update = (ap: AssistanceProject): Observable<AssistanceProject> => this.apiService.put(`/assistanceProjects/${ap.id}`, ap);
  // updateAndGetObservable = (ap: AssistanceProject): Observable<AssistanceProject> => this.apiService.put(`/assistanceProjects/${ap.id}`, ap);

  getAssistanceProjectsFilter = (assistanceProjectFiltered: AssistanceProjectFiltered): Observable<AssistanceProject[]> => this.apiService.put(
    `/assistanceProjects/filtered/`,assistanceProjectFiltered)
  
}

import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Disability} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class DisabilityService extends ObservableApiService<Disability>{

  constructor(apiService: ApiService) {
    super('/disabilities', apiService);
  }
}

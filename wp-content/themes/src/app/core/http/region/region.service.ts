import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AslRegion} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<AslRegion[]> => this.apiService.get('/regions');
}

import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {DocumentTemplateType} from '@app/core/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentTemplateTypeService extends ObservableApiService<DocumentTemplateType> {

  constructor(apiService: ApiService) {
    super('/documentTemplateTypes', apiService);
  }
  /**
   * Get all type template document template except Head Letter
   */
  getAllExceptLetterHead = (): Observable<DocumentTemplateType[]> => this.apiService.get(`${this.apiPath}`);

}

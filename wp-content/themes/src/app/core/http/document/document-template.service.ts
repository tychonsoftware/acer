import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {DocumentTemplate} from '@app/core/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentTemplateService extends ObservableApiService<DocumentTemplate> {

  constructor(apiService: ApiService) {
    super('/documentTemplates', apiService);
  }
  /**
   * Get all document template except Head Letter.
   */
  getAllDocumentExceptLetterHead = (): Observable<DocumentTemplate[]> => this.apiService.get(`${this.apiPath}`);

}

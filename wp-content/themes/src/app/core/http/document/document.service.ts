import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {Document} from '@app/core/models/document/document.model';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {GuestTransition} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class DocumentService extends ObservableApiService<Document> {

  constructor(apiService: ApiService) {
    super('/documents', apiService);
  }
  getAll = (): void => super.getAll();
  
  getDocumentsByGuestStatus = (statusId: number): Observable<Document[]> => this.apiService.get(`${this.apiPath}/byGuestStatus/${statusId}`);

  generate = (documentTemplateId: number, htmlContent: string): Observable<any> => this.apiService.get(`${this.apiPath}/generate`,
    new HttpParams().append('documentTemplateId', documentTemplateId.toString()).append('htmlContent', htmlContent), 'text')

  update = (document: any): Observable<Document> => this.apiService.put('/documents', document);

  create = (document: any): Observable<GuestTransition> => this.apiService.post('/documents', document);

  deleteDocument = (id: number): Observable<any> => this.apiService.delete(`/documents/${id}`);
}

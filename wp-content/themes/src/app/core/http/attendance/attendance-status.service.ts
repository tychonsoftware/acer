import { Injectable } from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {AssistanceProjectStatus} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';
import {AttendanceStatus} from '@app/core/models/attendance/attendance-status.model';

@Injectable({
  providedIn: 'root'
})
export class AttendanceStatusService extends ObservableApiService<AttendanceStatus>{

  constructor(apiService: ApiService) {
    super('/attendanceStatuses', apiService);
  }
}

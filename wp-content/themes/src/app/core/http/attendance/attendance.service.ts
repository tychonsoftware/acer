import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Attendance} from '@app/core/models/attendance/attendance.model';
import {AttendanceCalendar} from '@app/core/models/attendance/attendance-calendar.model';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService extends ObservableApiService<Attendance> {

  constructor(apiService: ApiService) {
    super('/attendances', apiService);
  }

  /**
   * Get all guest attendances by period.
   * @param period Period of format mm/yyyy (e.g. 07/2021).
   * @param uuid Guest UUID.
   */
  getByPeriod = (period: string, uuid: string): Observable<AttendanceCalendar> => this.apiService
    .get(`${this.apiPath}/calendar/byPeriod`, new HttpParams().append('period', period).append('guestUuid', uuid))

  /**
   * Get all guest closer attendances .
   * @param uuid Guest UUID.
   */
  getGuestCloserAttendances = (uuid: string): Observable<Attendance> => this.apiService
    .get(`${this.apiPath}/closer`, new HttpParams().append('guestUuid', uuid))

  /**
   * Get all attendances by guest uuid.
   * @param uuid Guest UUID.
   */
  getGuestAttendances = (period: string, uuid: string): Observable<Attendance[]> => this.getAllAsObservable(
    `byPeriod`, new HttpParams().append('guestUuid', uuid).append('period', period))

  /**
   * Get all attendances by guest uuid.
   * @param uuid Guest UUID.
   */
  getGuestAllPeriodAttendances = (uuid: string): Observable<Attendance[]> => this.getAllAsObservable(
    `byPeriod`, new HttpParams().append('guestUuid', uuid))

  /**
   * Get all attendances by guest uuid.
   * @param period Period of format mm/yyyy (e.g. 07/2021).
   * @param uuid Guest UUID.
   */
  getGuestAttendancesByPeriod = (period: string, uuid: string): Observable<Attendance[]> => this.getAllAsObservable(
    `byPeriod`, new HttpParams().append('guestUuid', uuid).append('period', period))

  deleteById = (id: number): Observable<any> => this.apiService.delete(`${this.apiPath}/${id}`);
}

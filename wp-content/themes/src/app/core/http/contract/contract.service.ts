import {Injectable} from '@angular/core';
import {Contract} from '@app/core/models';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContractService extends ObservableApiService<Contract> {

  constructor(apiService: ApiService) {
    super('/contracts', apiService);
  }

  // TODO: use only for mock server
  // update = (contract: Contract): Observable<Contract> => this.apiService.put(`/contracts/${contract.id}`, contract);
}

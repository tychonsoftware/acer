import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Area} from '@app/core/models/shared/area.model';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  constructor(private apiService: ApiService) {
  }
  
  update = (area: any, registryIn: boolean): Observable<Area> => this.apiService.put(`/areas/${registryIn}`, area);

  getByRegistryIn = (registryIn: boolean): Observable<Area[]> => this.apiService.get(`/areas/byRegistryIn/${registryIn}`);

  getAll = (): Observable<Area[]> => this.apiService.get('/areas');

  getAreaFromCity = (cityId: number): Observable<Area[]> => this.apiService.get(`/areas/byCity/${cityId}`);
}

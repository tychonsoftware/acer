import {Injectable} from '@angular/core';
import {GuestAna} from '@app/core/models/guests/guest-ana.model';
import {Observable} from 'rxjs';
import {ApiService} from '@app/core/http/api.service';

@Injectable({
  providedIn: 'root'
})
export class GuestAnaService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<GuestAna[]> => this.apiService.get('/guests/ana');

  getByUUID = (uuid: string): Observable<GuestAna> => this.apiService.get(`/guests/ana/${uuid}`);

  create = (guestTransition: GuestAna): Observable<GuestAna> => this.apiService.post('/guests/ana', guestTransition);

  update = (guestTransition: GuestAna): Observable<GuestAna> => this.apiService.put('/guests/ana', guestTransition);

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/guests/ana/${uuid}`);
}

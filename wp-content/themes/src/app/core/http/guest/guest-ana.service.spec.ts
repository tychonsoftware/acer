import {TestBed} from '@angular/core/testing';

import {GuestAnaService} from './guest-ana.service';

describe('GuestAnaService', () => {
  let service: GuestAnaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuestAnaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

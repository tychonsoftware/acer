import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {GuestMinimalTransition, GuestTransition} from '@app/core/models';
import {ApiService} from '@app/core/http/api.service';
import {HttpParams} from '@angular/common/http';
import {environment} from '@env/environment';
import {catchError, shareReplay} from 'rxjs/operators';
import {Dismiss} from '@app/core/models/guests/dismiss.model';

@Injectable({
  providedIn: 'root'
})
export class GuestService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<GuestTransition[]> => this.apiService.get('/guests');

  getByStatus = (statusId: number, onlyAna: boolean): Observable<GuestTransition[]> => this.apiService.get(`/guests/byStatus/${statusId}?onlyAna=` + onlyAna);

  getByFilter(statusId: number, onlyAna: boolean, buildingId: number | undefined, recoveryTaxRegimeId: number | undefined): Observable<GuestTransition[]> {
    const params: HttpParams = new HttpParams().append('onlyAna', onlyAna + '')
      .append('buildingId', buildingId ? buildingId.toString() : '')
      .append('recoveryTaxRegimeId', recoveryTaxRegimeId ? recoveryTaxRegimeId.toString() : '');
    console.log(params);
    return this.apiService.get(`/guests/byStatus/${statusId}/filter`, params);
  }

  getByUUID = (uuid: string): Observable<GuestTransition> => this.apiService.get(`/guests/${uuid}`);

  getMinimal = (): Observable<GuestMinimalTransition[]> => this.apiService.get('/guests/minimal');

  getMinimalByUUID = (uuid: string): Observable<GuestMinimalTransition> => this.apiService.get(`/guests/minimal/${uuid}`);

  getByFiscalCodeFromArchive = (fiscalCode: string): Observable<GuestTransition[]> => this.apiService.get(`/guests/byFiscalCodeFromArchive?fiscalCode=` + fiscalCode);

  create(guestTransition: GuestTransition): Observable<GuestTransition> {
    return this.apiService.post('/guests', guestTransition,);
  }

  update(guestTransition: GuestTransition): Observable<GuestTransition> {
    return this.apiService.put('/guests', guestTransition);
  }

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/guests/${uuid}`);

  deleteFromDB = (uuid: string): Observable<any> => this.apiService.delete(`/guests/deleteFromDB/${uuid}`);

  recover = (uuid: string): Observable<any> => this.apiService.get(`/guests/recover/${uuid}`);

  dismiss = (dismiss: Dismiss): Observable<any> => this.apiService.post(`/guests/dismiss`, dismiss);

  getDeleted = (deleted: boolean): Observable<GuestTransition[]> => this.apiService.get(`/guests/byDeleted/${deleted}`);
  // TODO: use only for mock server
  // update = (guestTransition: GuestTransition): Observable<GuestTransition> => this.apiService.put(`/guests/${guestTransition.uuid}`, guestTransition);

  exportGuests = (): Observable<any> => this.apiService.download(`/guests/export`);
}

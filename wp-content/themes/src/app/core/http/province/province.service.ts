import {Injectable} from '@angular/core';
import {ApiService} from '@app/core/http/api.service';
import {Observable} from 'rxjs';
import {Province} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<Province[]> => this.apiService.get('/provinces');
}

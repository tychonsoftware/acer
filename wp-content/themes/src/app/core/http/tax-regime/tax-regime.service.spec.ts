import { TestBed } from '@angular/core/testing';

import { TaxRegimeService } from './tax-regime.service';

describe('TaxRegimeService', () => {
  let service: TaxRegimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaxRegimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

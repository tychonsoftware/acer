import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '@app/core/http/api.service';
import {TaxRegime} from '@app/core/models';

@Injectable({
  providedIn: 'root'
})
export class TaxRegimeService {

  constructor(private apiService: ApiService) {
  }

  getAll = (): Observable<TaxRegime[]> => this.apiService.get('/taxRegimes');

  getByUUID = (uuid: string): Observable<TaxRegime> => this.apiService.get(`/taxRegimes/${uuid}`);

  create = (guestTransition: TaxRegime): Observable<TaxRegime> => this.apiService.post('/taxRegimes', guestTransition);

  update = (guestTransition: TaxRegime): Observable<TaxRegime> => this.apiService.put('/taxRegimes', guestTransition);

  delete = (uuid: string): Observable<any> => this.apiService.delete(`/taxRegimes/${uuid}`);
}

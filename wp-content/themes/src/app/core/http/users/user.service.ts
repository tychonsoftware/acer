import {Injectable} from '@angular/core';
import {ObservableApiService} from '@app/core/http/observable-api.service';
import {ApiService} from '@app/core/http/api.service';
import {User} from '@app/core/models/users/user';
import {CookieService} from 'ngx-cookie-service';
import {Constants} from '@app/core/constants/Constants';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ObservableApiService<User> {

  private static user?: User;
  private static isLoaded = false;

  constructor(apiService: ApiService,
              protected cookieService: CookieService) {
    super('/users', apiService);
  }

  init(): void {
    if (!UserService.isLoaded) {
      const username = this.cookieService.get('username') ? this.cookieService.get('username') : Constants.DEFAULT_ADMIN;
      this.getAsObservable(`${username}/${Constants.PORTAL_ID}`).subscribe(res => {
        UserService.isLoaded = true;
        UserService.user = res;
        let functionalities = '';
        if (UserService.user.functionalities) {
          for (const functionality of UserService.user.functionalities) {
            functionalities += functionality.description + ' ';
          }
          console.log('Received user functionalities -> ' + JSON.stringify(UserService.user.username) + '\n' + functionalities);
        }
      });
    }
  }

  getUserSecurity(): User | undefined {
    return UserService.user;
  }

  getUserSecurityObservable(): Observable<User> | undefined {
    const username = this.cookieService.get('username') ? this.cookieService.get('username') : Constants.DEFAULT_ADMIN;
    return this.getAsObservable(`${username}/${Constants.PORTAL_ID}`);
  }

  getIsLoaded(): boolean {
    return UserService.isLoaded;
  }

}

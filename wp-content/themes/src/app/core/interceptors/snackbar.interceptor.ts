import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {environment} from '@env/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class SnackbarInterceptor implements HttpInterceptor {

  constructor(private snackBar: MatSnackBar, private readonly injector: Injector, private dialogService: DialogService, private cookieService: CookieService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return next.handle(request).pipe(
      tap(e => {
        if (request.url.indexOf('/checkCF') === -1 && (request.method === 'POST' || request.method === 'PUT')) {
          if (e instanceof HttpResponse && e.status === 200) {
            this.snackBar.open(
              this.injector.get(TranslateService).instant('SNACKBAR.SUCCESS'),
              this.injector.get(TranslateService).instant('SNACKBAR.ACTION'),
              {duration: 2000, panelClass: 'successSnack', verticalPosition: 'top'});
          }
        }
      }),
      catchError(error => {
        if (error.status === 401) {
          setTimeout(() => {
            this.cookieService.set('/oidc/logout', 'true');
            const snackBarObs = this.snackBar.open((error && error.title) ? error.title : 'Sessione di lavoro scadute, si raccomanda di effettuare nuovamente la login.',
              'per accedere',
              {duration: 0, panelClass: 'errorSnack', verticalPosition: 'top'});
            snackBarObs.onAction().subscribe(() => window.location.href = environment.main_page_url);
          }, 500);
          return throwError(error);
        }
        if (error && error.error) {
          let exception;
          if (typeof error.error === 'string') {
            exception = JSON.parse(error.error);
          } else {
            exception = error.error;
          }
          if (exception && exception.status && (exception.status === 500 || exception.status === 404) && (exception.title || exception.detail)) {
            const data = {
              title: exception.title,
              message: exception.detail || exception.title,
              cancelText: 'Annulla',
              confirmText: '',
              hideSave: true
            };
            // Open the warning dialog
            this.dialogService.openWarningDialog(data);
            return throwError(error);
          }
        }
        this.snackBar.open(
          (error && error.title) ? error.title : this.injector.get(TranslateService).instant('SNACKBAR.ERROR'),
          // this.injector.get(TranslateService).instant('SNACKBAR.ERROR'),
          this.injector.get(TranslateService).instant('SNACKBAR.ACTION'),
          {duration: 2000, panelClass: 'errorSnack', verticalPosition: 'top'});
        return throwError(error);
      })
    );
  }
}

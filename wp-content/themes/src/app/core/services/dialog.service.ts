import {ApplicationRef, ComponentFactoryResolver, Injectable, Injector, Type} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {FormDialogComponent, IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {ConfirmDialogComponent, IConfirmDialogConfig} from '@app/shared/components/confirm-dialog/confirm-dialog.component';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {DialogComponent, IDialogConfig} from '@app/shared/components/dialog/dialog.component';
import {IWarningDialogConfig, WarningDialogComponent} from '@app/shared/components/warning-dialog/warning-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    private dialog: MatDialog) {
  }

  public openDialog(data: IDialogConfig, config?: MatDialogConfig): MatDialogRef<DialogComponent> {
    return this.dialog.open(DialogComponent, this._buildModalDialogConfig(data, config));
  }

  public openConfirmDialog(data: IConfirmDialogConfig, config?: MatDialogConfig): MatDialogRef<ConfirmDialogComponent> {
    return this.dialog.open(ConfirmDialogComponent, this._buildModalDialogConfig(data, config));
  }


  public openWarningDialog(data: IWarningDialogConfig, config?: MatDialogConfig): MatDialogRef<WarningDialogComponent> {
    return this.dialog.open(WarningDialogComponent, this._buildModalDialogConfig(data, config));
  }


  public openFormDialog<T>(componentBody: Type<FormContentDirective>,
                           data: Partial<IFormDialogConfig>,
                           config?: MatDialogConfig): MatDialogRef<FormDialogComponent> {
    const dialogRef = this.dialog.open(FormDialogComponent, this._buildModalDialogConfig(data, config));
    dialogRef.componentInstance.childComponentType = componentBody;
    return dialogRef;
  }

  public confirmed(dialogRef: any): Observable<any> | undefined {
    return dialogRef.afterClosed().pipe(take(1), map(res => res));
  }

  private _buildModalDialogConfig(data: any, config?: MatDialogConfig): MatDialogConfig {
    if (!config) {
      config = {
        disableClose: true,
        autoFocus: false,
      };
    }
    config.data = data;
    return config;
  }
}

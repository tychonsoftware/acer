import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackBarComponent} from '@app/shared/components/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackbar: MatSnackBar) { }

  showSnackBar(title:string,message: string, messageType: 'error' | 'success') {
    this.snackbar.openFromComponent(SnackBarComponent, {
      data: {
        title: title,
        message: message,
        messageType: messageType
      },
      duration: 2000,
      panelClass: messageType
    });
  }
}

import { FileH } from './file-h.model';

export interface FileHPeriods {
  period: string;
  fileHs: FileH[];
}

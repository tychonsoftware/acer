import {File} from '@app/core/models/file/file.model';
import {FileHStatus} from '@app/core/models/file/file-h-status.model.ts';

export enum ConsolidationStatus {
  TO_BE_CONSOLIDATED = 1,
  CONSOLIDATED
}

export interface FileH {
  id: number;
  dateInsert: Date;
  anaFile: File;
  sanFile: File;
  status: FileHStatus;
  numberPatients: number;
  numberProjects: number;
  numberRowsAna: number;
  numberRowsSan: number;
  period: string;
  totUserCharge: number;
  totAslCharge: number;
  totBirthplaceCharge: number;
  totNumberPresence: number;
}

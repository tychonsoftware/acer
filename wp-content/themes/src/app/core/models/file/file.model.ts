import {DocumentsType} from '@app/core/models/file/documents-type.model';
import {DocumentsConfiguration} from '@app/core/models/file/documents-configuration.model';

export interface File {
  id:	number;
  uuid?: string;
  application: number;
  dateInsert:	Date;
  documentType: DocumentsType;
  dynamicPath:	string;
  obsolete:	boolean;
  obsoleteDate:	Date;
  staticPath: DocumentsConfiguration;
}

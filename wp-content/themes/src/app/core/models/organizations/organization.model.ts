import { RecoveryTaxRegimeFlag } from '../tax-regime/recovery-tax-regime-flag.model';
import { City } from '../shared/city.model';

export interface OrganizationRole {
  id?: number;
  name: string;
}

export interface Organization {
  id?: number;
  organizationName: string;

  // DB info
  dbUrl: string;
  dbUser: string;
  dbPassword: string;
  dbDriver: string;
  dbLiquibaseChangelog: string;

  // DB Ana info
  dbAnaUrl: string;
  dbAnaUser: string;
  dbAnaPassword: string;
  dbAnaDriver: string;
  dbAnaLiquibaseChangelog: string;
  piva:	string;
  phone:	string;
  email: string;
  roles?: OrganizationRole[];

  dateInsert?: Date;
  dateLastChange?: Date;
  recoveryTaxRegimeFlags: RecoveryTaxRegimeFlag[];
  fiscalCode: string;
  postalCode: string;
  address: string;
  reaOffice: string;
  reaNumber: string;
  liquidationStatus: boolean;
  city: City;
}

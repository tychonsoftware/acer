export interface Exemption {
  id?: number;
  uuid?: string;
  tag: string;
  expirationDate: Date;
  dateInsert?: Date;
  eliminato?: boolean;
  lastDateModify?: Date;
  lastUserModify?: Date;
  userInsertion?: string;
}

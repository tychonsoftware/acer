import {HistoricResidence} from '@app/core/models';
import {RecoveryTaxRegime} from '@app/core/models/guests/recovery-tax-regime.model';

export interface GuestDropDown {
  uuid?: string;

  // Residenza Storica, Domicilio e Domicilio Sanitario
  historicResidence: HistoricResidence;
  firstName: string;
  lastName: string;
  fiscalCode: string;
  birthDate: Date;
  birthPlace: string;
  acceptanceDate: Date;
  recoveryTaxRegime: RecoveryTaxRegime;
  fullName?: string;
}

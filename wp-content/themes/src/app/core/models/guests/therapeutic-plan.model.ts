export interface TherapeuticPlan {
  id?: number;
  uuid?: string;
  tag: string;
  expirationDate: Date;
  dateInsert?: Date;
  eliminato?: boolean;
  lastDateModify?: Date;
  lastUserModify?: string;
  userInsertion?: string;
}

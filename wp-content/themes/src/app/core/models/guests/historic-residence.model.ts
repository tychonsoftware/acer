import {City} from '@app/core/models/shared/city.model';
import {Asl} from '@app/core/models/shared/asl.model';
import {AslRegion} from '@app/core/models/shared/asl-region.model';
import {Area} from '@app/core/models/shared/area.model';
import {District} from '@app/core/models/shared/district.model';

export interface HistoricResidence {
  id?: number;
  uuid?: string;
  city: City;
  asl: Asl;
  region: AslRegion;
  area: Area;
  district: District;
}

export interface RecoveryTaxRegimeType {
  id?: number;
  uuid?: string;
  description: string;
}

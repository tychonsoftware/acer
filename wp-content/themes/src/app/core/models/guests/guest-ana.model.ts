export interface GuestAna {
  id?: number;
  uuid?: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  birthPlace?: string;
  fiscalCode?: string;
  landingPhone?: string;
  mobilePhone?: string;
  sex?: boolean;
}

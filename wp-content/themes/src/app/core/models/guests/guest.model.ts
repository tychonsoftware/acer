import {Address, Exemption, GuestStatus, HistoricResidence, ParentAna, TherapeuticPlan} from '@app/core/models';

export interface Guest {
  id: number;
  uuid?: string;

  // Residenza Storica, Domicilio e Domicilio Sanitario
  historicResidence?: HistoricResidence;
  medicalDomicile?: boolean;
  residence?: boolean;
  address?: Address;
  addressDomicile?: Address;

  // Familiari
  parents?: ParentAna[];

  // Esenzioni e Piano Terapeutico
  exemptions?: Exemption[];
  therapeuticPlans?: TherapeuticPlan[];

  // Altre Info
  numberIdentifier?: string;
  disability?: boolean;
  doctor?: string;
  doctorMail?: string;
  expirationCardDate?: Date;
  allergies?: string;
  independent?: boolean;
  privacy?: boolean;

  // Foto
  picName?: string;
  picContent?: string;

  // Note
  note?: string;

  status?: GuestStatus;
  acceptanceDate?: Date;
  dateInsert?: Date;
  deleted?: boolean;
  exitDate?: Date;
  lastDateModify?: Date;
  lastUserModify?: string;
  userInsertion?: string;
}

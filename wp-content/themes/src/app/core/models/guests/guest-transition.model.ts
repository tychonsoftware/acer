import {Address} from '@app/core/models/shared/address.model';
import {ParentAna} from '@app/core/models/guests/parent-ana.model';
import {GuestStatus} from '@app/core/models/guests/guest-status.model';
import {HistoricResidence} from '@app/core/models/guests/historic-residence.model';
import {Exemption} from '@app/core/models/guests/exemption.model';
import {TherapeuticPlan} from '@app/core/models/guests/therapeutic-plan.model';
import {RecoveryTaxRegime} from '@app/core/models/';
import {File} from '@app/core/models/file/file.model';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';
import {HospitalizationSession} from '@app/core/models/guests/hospitalization-session.model';

export interface GuestTransition {
  uuid?: string;

  // Anagrafica
  firstName: string;
  lastName: string;
  fullName: string;
  reversedFullName: string;
  birthDate?: Date;
  birthPlace?: string;
  fiscalCode?: string;
  landingPhone?: string;
  mobilePhone?: string;
  sex?: boolean;

  // Altre Info
  numberIdentifier?: string;
  disability?: boolean;
  doctor?: string;
  doctorMail?: string;
  expirationCardDate?: Date;
  allergies?: string;
  independent?: boolean;
  privacy?: boolean;

  // Foto
  picName?: string;
  picContent?: string;

  // Note
  note?: string;

  profilePicture?: File;

  // Residenza Storica, Domicilio e Domicilio Sanitario
  historicResidence?: HistoricResidence;
  medicalDomicile?: boolean;
  residence?: boolean;
  address?: Address;
  addressDomicile?: Address;

  // Familiari
  parents?: ParentAna[];

  // Esenzioni e Piano Terapeutico
  exemptions?: Exemption[];
  therapeuticPlans?: TherapeuticPlan[];

  // Recovery tax regime
  recoveryTaxRegime?: RecoveryTaxRegime;
  organizationBuilding?: OrganizationBuilding;

  status?: GuestStatus;
  acceptanceDate?: Date;
  dateInsert?: Date;
  deleted?: boolean;
  exitDate?: Date;
  lastDateModify?: Date;
  lastUserModify?: string;
  userInsertion?: string;
  hospitalizationSession?: HospitalizationSession;
}

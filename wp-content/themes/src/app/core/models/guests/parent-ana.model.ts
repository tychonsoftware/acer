import {Province} from "@app/core/models";

export interface ParentAna {
  id: number;
  uuid: string;
  address: string;
  dateInsert: Date;
  email: string;
  firstName: string;
  lastName: string;
  kinship: string;
  landingPhone: string;
  mobilePhone: string;
  note: string;
  isResponsible: boolean;
  province: Province;
  cap: string;
  birthPlace?: string;
  fiscalCode: string;
}

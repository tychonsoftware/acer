export interface DismissReason {
  id?: number;
  description?: string;
}

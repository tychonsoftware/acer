import {Dismiss} from '@app/core/models/guests/dismiss.model';

export interface HospitalizationSession {
  id?: number;
  sessionName: string;
  startDate: Date;
  endDate: Date;
  guestId: number;
  attendances?: number[];
  dismiss?: Dismiss;
}

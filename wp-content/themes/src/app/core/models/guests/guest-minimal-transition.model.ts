import {HistoricResidence} from '@app/core/models';
import {RecoveryTaxRegime} from '@app/core/models/guests/recovery-tax-regime.model';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';

export interface GuestMinimalTransition {
  uuid?: string;

  // Residenza Storica, Domicilio e Domicilio Sanitario
  historicResidence: HistoricResidence;
  firstName: string;
  lastName: string;
  fiscalCode: string;
  birthDate: Date;
  birthPlace: string;
  acceptanceDate: Date;
  recoveryTaxRegime: RecoveryTaxRegime;
  organizationBuilding: OrganizationBuilding;
  fullName: string;
}

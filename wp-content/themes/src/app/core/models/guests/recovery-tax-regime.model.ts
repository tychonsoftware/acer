import {City} from '@app/core/models/shared/city.model';
import {Nation} from '@app/core/models/shared/nation.model';
import {Province} from '@app/core/models/shared/province.model';
import {Contract} from '@app/core/models/configurations/contract.model';
import {RecoveryTaxRegimeType} from '@app/core/models/guests/recovery-tax-regime-type.model';


export interface RecoveryTaxRegime {
  id?: number;
  uuid?: string;
  acceptanceDate: Date;
  address: string;
  cap: string;
  city: City;
  contract: Contract;
  firstName: string;
  fiscalCode: string;
  lastName: string;
  nation: Nation;
  pec: string;
  province: Province;
  recipientCode: string;
  recoveryTaxRegimeType: RecoveryTaxRegimeType;
}

export interface GuestStatus {
  id?: number;
  uuid?: string;
  description?: string;
}

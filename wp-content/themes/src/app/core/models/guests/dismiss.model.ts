import {DismissReason} from '@app/core/models/guests/dismiss-reason.model';

export interface Dismiss {
  id: number;
  date: Date;
  time: string;
  reason: DismissReason;
  note: string;
  drugManagement: string;
  articleManagement: string;
  absentOnLastDay: boolean;
  guestUuid: string;
}

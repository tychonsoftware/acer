import {DocumentTemplateType, File, GuestMinimalTransition} from '@app/core/models';
import {GuestDropDown} from '@app/core/models/guests/guest-dropdown.model';

export interface Document {
  id: number;
  guestMinimal: GuestDropDown;
  date: Date;
  document: File;
  documentContent: string;
  documentName: string;
  description: string;
  documentTemplateType: DocumentTemplateType;
}

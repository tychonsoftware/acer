import {DocumentTemplate, GuestMinimalTransition} from '@app/core/models';

export interface DocumentGenerate {
  id: number;
  guestMinimal: GuestMinimalTransition;
  document: DocumentTemplate;
}

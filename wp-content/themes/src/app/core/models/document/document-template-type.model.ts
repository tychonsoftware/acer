export interface DocumentTemplateType {
  id: number;
  code: string;
  description: string;
  uuid?: string;
}

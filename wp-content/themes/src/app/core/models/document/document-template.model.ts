import {DocumentTemplateType} from '@app/core/models';

export interface DocumentTemplate {
  id: number;
  name: string;
  description: string;
  documentType: DocumentTemplateType;
  dateLastModify: Date;
  headerContent: string;
  bodyContent: string;
  footerContent: string;
  showHeader: boolean;
  showFooter: boolean;
}

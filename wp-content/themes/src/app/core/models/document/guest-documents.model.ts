import {Document} from '@app/core/models';

export interface GuestDocuments {
  firstName: string;
  lastName: string;
  documents: Document[];
}

export interface InvoiceType {
  id:	number;
  code:	string;
  description: string;
}

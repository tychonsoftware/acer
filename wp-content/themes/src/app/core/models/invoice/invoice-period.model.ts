import {Invoice} from '@app/core/models';

export interface InvoicePeriods {
  period: string;
  invoices: Invoice[];
}

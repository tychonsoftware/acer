import {File, InvoicePayment, Asl, City} from '@app/core/models';
import {Area} from '@app/core/models/shared/area.model';
import {InvoiceStatus} from '@app/core/models/invoice/invoice-status.model';
import {InvoiceType} from '@app/core/models/invoice/invoice-payment.model';

export enum InvoiceState {
  EMESSA = 1,
  PAGATA = 2,
  SOSPESA = 3,
  STORNATA = 4,
  BOZZA = 5
}
export interface Invoice {
  id: number;
  dateInsert:	Date;
  description: string;
  destinatary: string;
  pdfFile: File;
  xmlFile: File;
  invoiceType: InvoiceType;
  numInvoice: number;
  payment: InvoicePayment;
  period: string;
  invoiceStatus: InvoiceStatus;
  total: number;
  paymentDate:	Date;
  checked: boolean;
  isRecipientAnagraficaChanged: boolean;
  aslDest: Asl;
  areaDest: Area;
  cityDest: City;
}

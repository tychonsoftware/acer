import {RecoveryTaxRegimeType} from '@app/core/models/guests/recovery-tax-regime-type.model';
import {StructureType} from '@app/core/models';
import {ResidenceType} from '@app/core/models/configurations/residence-type.model';

export interface OrganizationBuilding {
  id?: number;
  name?: string;
  uuid?: string;
  recoveryTaxRegimeTypes?: RecoveryTaxRegimeType[];
  healthcareCompanyCode?: string;
  structureCode?: string;
  districtCode?: string;
  structureType?: StructureType;
  residenceType?: ResidenceType;
}


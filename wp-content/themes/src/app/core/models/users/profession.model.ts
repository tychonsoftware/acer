export interface Profession {
  id?: number;
  code?: string;
  description?: string;
}

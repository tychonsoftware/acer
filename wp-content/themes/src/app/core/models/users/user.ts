import {Profession} from '@app/core/models/users/profession.model';
import {OrganizationRole} from '@app/core/models';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';

export interface User {
  id?: number;
  username?: string;
  password?: string;
  name?: string;
  surname?: string;
  mobile?: string;
  email?: string;
  fiscalCode?: string;
  status?: UserStatus;
  types?: UserType[];
  buildings?: OrganizationBuilding[];
  defaultBuilding?: OrganizationBuilding;
  functionalities?: UserFunctionalities[];
  profession: Profession;
  typesString: string;
}

export interface UserStatus {
  id?: number;
  status?: string;
  description?: string;
}

export interface UserType {
  id?: number;
  description?: string;
  type?: string;
  functionalities?: UserFunctionalities[];
  roles?: OrganizationRole[];
}

export interface UserFunctionalities {
  id?: number;
  description?: string;
  functionality?: string;
  portal?: Portal;
}

export interface Portal {
  id?: number;
  description?: string;
}

import { AssistanceProjectStatus } from '..';
import { GuestMinimalTransition } from '../guests/guest-minimal-transition.model';

export interface AttendanceReport {
  id?: number;
  numberAttendance?: number;
  uuid?: string;
  guest: GuestMinimalTransition;
  assistanceProjectStatusDTO?: AssistanceProjectStatus;
}
import {RecoveryTaxRegimeType} from '@app/core/models/tax-regime/recovery-tax-regime-type.model';

export interface RecoveryTaxRegimeFlag {
  id?: number;
  description: string;
  recoveryTaxRegimeType: RecoveryTaxRegimeType;
}

import {City} from '@app/core/models/shared/city.model';
import {Nation} from '@app/core/models/shared/nation.model';
import {Province} from '@app/core/models/shared/province.model';
import {Contract} from '@app/core/models/configurations/contract.model';
import {RecoveryTaxRegimeType} from '@app/core/models/tax-regime/recovery-tax-regime-type.model';

export interface RecoveryTaxRegime {
  id?: number;
  uuid?: string;
  firstName: string;
  lastName: string;
  fiscalCode: string;
  acceptanceDate: Date;
  address: string;
  cap: string;
  city: City;
  nation: Nation;
  province: Province;
  contract: Contract;
  pec: string;
  recipientCode: string;
  recoveryTaxRegimeType: RecoveryTaxRegimeType;
}

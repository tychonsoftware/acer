export interface DocumentsConfiguration {
  id: number;
  uuid?: string;
  path: string;
  application: number;
  dateInsert: Date;
  obsolete: boolean;
  obsoleteDate: Date;
}

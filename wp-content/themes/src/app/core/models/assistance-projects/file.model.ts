import {DocumentsType} from '@app/core/models/assistance-projects/documents-type.model';
import {DocumentsConfiguration} from '@app/core/models/assistance-projects/documents-configuration.model';

export interface File {
  id: number;
  uuid?: string;
  application: number;
  dateInsert: Date;
  dynamicPath: string;
  obsolete: boolean;
  obsoleteDate: Date;
  documentType: DocumentsType;
  staticPath: DocumentsConfiguration;
}

export interface Disability {
  id: number;
  uuid?: string;
  code: string;
  description: string;
  codeDescription: string;
}

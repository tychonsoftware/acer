import {GuestMinimalTransition} from '@app/core/models/guests/guest-minimal-transition.model.ts';
import {Disability} from '@app/core/models/assistance-projects/disability.model';
import {DiagnosisICD9} from '@app/core/models/assistance-projects/diagnosis-ICD9.model';
import {PerformanceCharge} from '@app/core/models/assistance-projects/performance-charge.model';
import {PerformanceType} from '@app/core/models/assistance-projects/performance-type.model';
import {AssistanceProjectStatus} from '@app/core/models/assistance-projects/assistance-project-status.model';
import {File} from '@app/core/models/file/file.model';
import {FileH} from '@app/core/models/file/file-h.model';
import {Invoice} from '@app/core/models/invoice/invoice.model';

export interface AssistanceProject {
  id?: number;

  dateInsert: Date;
  obsolete: boolean;
  guestMinimal: GuestMinimalTransition;

  // Prescription
  prescriptionDate: Date;
  file: File;
  fileName: string;
  fileContent: string;
  filesH: Set<FileH>;
  invoices: Set<Invoice>;

  status: AssistanceProjectStatus;

  // Performances
  performanceType: PerformanceType;
  performanceCharge: PerformanceCharge;

  // Treatment
  treatment: string;
  treatmentStrDate: Date;
  treatmentEndDate: Date;
  treatmentDaysNumber: number;

  // Fee quotes
  dailyFee: number;
  aslPercentFee: number;
  cityPercentFee: number;
  userPercentFee: number;
  application: number;

  aslCharge: number;
  birthPlaceCharge: number;
  userCharge: number;
  numberOfPresence?: number;
  uuid: string;
  period: string;

  // Authorization
  authNumber: number;
  authDate: Date;

  // Other info
  prescriberCode: string;
  disability: Disability;
  diagnosisICD9: DiagnosisICD9;
}

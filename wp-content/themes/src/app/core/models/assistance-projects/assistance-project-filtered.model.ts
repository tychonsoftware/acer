import {AssistanceProjectStatus} from '@app/core/models';
import {RecoveryTaxRegimeType} from '@app/core/models/tax-regime/recovery-tax-regime-type.model'

export interface AssistanceProjectFiltered {
  authNumber?: number;
  status?: AssistanceProjectStatus;
  treatments?: string;
  startDateTreatments?: string;
  endDateTreatments?: string;
  firstName?: string;
  lastName?: string;
  recoveryTaxRegimeTypeDTO?: RecoveryTaxRegimeType;
}

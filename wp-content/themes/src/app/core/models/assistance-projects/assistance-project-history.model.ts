import {AssistanceProject, AssistanceProjectStatus} from '@app/core/models';

export interface AssistanceProjectHistory {
  dataAutorizzazione: Date;
  treatmentStrDate: Date;
  status: AssistanceProjectStatus;
  authNumber: number;
  guestName: string;
  assistanceProjects: AssistanceProject[];
}

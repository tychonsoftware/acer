export interface AssistanceProjectStatus {
  id: number;
  uuid?: string;
  description: string;
}

import {AssistanceProject, AssistanceProjectCharge} from '@app/core/models';

export interface ResponseByPeriod {
  assistanceProjectCharge: AssistanceProjectCharge;
  assistanceProjects: AssistanceProject[];
}

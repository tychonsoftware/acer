import {AssistanceProject} from '@app/core/models';

export interface GuestAssistanceProjects {
  firstName: string;
  lastName: string;
  fiscalCode: string;
  birthDate: Date;
  assistanceProjects: AssistanceProject[];
}

export interface PerformanceType {
  id: number;
  uuid?: string;
  code: string;
  description: string
  charge?: number;
  descriptionCharge: string;
}

export interface PerformanceCharge {
  id: number;
  uuid?: string;
  code: string;
  description: string;
}

export interface AssistanceProjectCharge {
  id:	number;
  totNumberPresence:	number;
  totAslCharge:	number;
  totBirthplaceCharge:	number;
  totNumberGuest:	number;
  totUserCharge:	number;
}

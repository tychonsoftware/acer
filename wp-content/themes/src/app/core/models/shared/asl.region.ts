export interface AslRegion {
  id?: number;
  uuid?: string;
  code: string;
  description: string;
}

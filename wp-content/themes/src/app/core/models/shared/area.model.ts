import {City} from '@app/core/models/shared/city.model';

export interface Area {
  id?: number;

  //General Data
  areaCode: string;
  businessName: string;
  supplierCode: string;
  cuuIpa: string;
  piva: string;
  fiscalCode: string;
  address: string;
  city: City;
  province: string;
  cap: string;
  
  //Invoice data
  fiscalNature: string;
  payCode: string;
  payType: string;
  bank: string;
  iban: string;
  invoiceExpirationDays: number;
  measureUnit: string;
  administrationReference: string;
  cig: string;
  causal: string;

  //Invoice Configuration flags
  groupingByCityRSA: boolean;
  billTaxRSA: boolean;
  isPerformanceTypeDescriptionRSA: boolean;
  groupByDistrictRSA: boolean;
  isProvidedTreatmentsSummaryRSA: boolean;

  headCity: City;
  registryIn: boolean;  
}

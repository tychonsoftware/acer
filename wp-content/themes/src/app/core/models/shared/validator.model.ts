export interface ValidationObject {
  cf: string;
  name: string;
  surname: string;
  sex: boolean;
  dataNascita: string;
  codLuogoNascita: string;
  userType: string;
  guestUuid?: string;
}

export interface ValidationResponse {
  status: string;
  message: string;
  type: string;
  archivedUuid: string;
}

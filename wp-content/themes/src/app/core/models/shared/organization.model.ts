import {AslRegion} from '@app/core/models/shared/asl.region';

export interface Organization {
  id?: number;
  uuid?: string;
  organizationName?: string;
  dbUrl: string;
  dbUser: string;
  dbPassword: string;
  dbDriver: string;
  dbLiquibaseChangelog: string;
  dbAnaUrl: string;
  dbAnaUser: string;
  dbAnaPassword: string;
  dbAnaLiquibaseChangelog: string;
  dbAnaDriver: string;
  piva: string;
  email: string;
  region: AslRegion;
}

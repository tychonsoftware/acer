import {Nation} from '@app/core/models/shared/nation.model';
import {City} from '@app/core/models/shared/city.model';

export interface Address {
  id?: number;
  uuid?: string;
  address: string;
  cap: string;
  nation: Nation;
  city: City;
}

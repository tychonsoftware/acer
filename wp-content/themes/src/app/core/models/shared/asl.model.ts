import {City} from '@app/core/models/shared/city.model';

export interface Asl {
  id?: number;
  uuid?: string;

  //General Data
  description: string;
  supplierCode: string;
  cuuIpa: string;
  piva: string;
  fiscalCode: string;
  address: string;
  city: City;
  province: string;
  cap: string;

  //Invoice data
  fiscalNature: string;
  payCode: string;
  payType: string;
  bank: string;
  iban: string;
  invoiceExpirationDays: number;
  measureUnit: string;
  administrationReference: string;
  cig: string;
  causal: string;

  //Invoice file data
  regionCode: string;
  aslCode: string;
  tel: string;
  fax: string;
  email: string;
  webSite: string;
  destCode: string;

  //Invoice Configuration flags
  reducedBillInvoiceRSA: boolean;
  billTaxRSA: boolean;
  isPerformanceTypeDescriptionRSA: boolean;
  groupByDistrictRSA: boolean;
  isProvidedTreatmentsSummaryRSA: boolean;

  obsolete: boolean;
  registryIn: boolean;  
}

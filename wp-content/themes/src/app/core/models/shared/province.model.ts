export interface Province {
  id?: number;
  uuid?: string;
  code: string;
  description: string;
}

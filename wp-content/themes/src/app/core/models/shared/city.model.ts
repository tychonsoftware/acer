import {Province} from '@app/core/models/shared/province.model';
import {AslRegion} from '@app/core/models/shared/asl.region';

export interface City {
  id: number;
  uuid?: string;
  cap: string;
  cfis: string;
  code: string;
  description: string;
  obsolete: boolean;
  aslRegion?: AslRegion;
  province?: Province;
  isNation?: boolean;
}

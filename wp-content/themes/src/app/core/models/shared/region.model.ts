export interface Region {
  id: number;
  uuid?: string;
  code: string;
  description: string;
}

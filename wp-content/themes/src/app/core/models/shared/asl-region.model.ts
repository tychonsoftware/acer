export interface AslRegion {
  id: number;
  code: string;
  description: string;
}

export interface Nation {
  id?: number;
  uuid?: number;
  cfis: string;
  code: string;
  description: string;
}

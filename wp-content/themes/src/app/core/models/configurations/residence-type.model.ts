export interface ResidenceType {
  id?: number;
  uuid?: string;
  description: string;
  code: string;
}

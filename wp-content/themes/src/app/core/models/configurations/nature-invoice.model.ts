export interface NatureInvoiceType {
  id?: number;
  uuid?: string;
  description: string;
  xml_code: string;
}

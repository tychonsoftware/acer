import {Address} from '@app/core/models/shared/address.model';
import {Province} from '@app/core/models/shared/province.model';
import {TaxRegime} from '@app/core/models/configurations/tax-regime.model';

export interface Residence {
  id?: number;
  uuid?: number;
  residenceName: string;
  piva: string;
  taxRegim: TaxRegime;
  billingAddress: Address;
  province: Province;
  predefinedServiceDrug: number;
  automaticGenerationSdi: boolean;
  automaticMonthlyGenerationSdi: boolean;
  email: string;
  faxNumber: string;
  phoneNumber: string;
  web: string;
}

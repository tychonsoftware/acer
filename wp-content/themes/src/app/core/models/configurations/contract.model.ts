import {NatureInvoiceType} from '@app/core/models/configurations/nature-invoice.model';

export interface Contract {
  id?: number;
  uuid?: string;
  dateInsert: Date;
  dailyPrice: number;
  delete: boolean;
  description: string;
  iva: number;
  lastDateModify: Date;
  monthlyPrice: number;
  natureInvoiceType?: NatureInvoiceType;
  privateRoom: boolean;
}

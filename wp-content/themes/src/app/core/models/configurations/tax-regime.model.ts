export interface TaxRegime {
  id?: number;
  uuid?: string;
  description: string;
}

export interface StructureType {
  id?: number;
  uuid?: string;
  description: string;
}

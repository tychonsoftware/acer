import {Guest} from '@app/core/models';
import {AttendanceReason} from '@app/core/models/attendance/attendance-reason.model';
import {AttendanceStatus} from '@app/core/models/attendance/attendance-status.model';

export interface Attendance {
  id?: number;
  uuid: string;
  actualExitDate: Date;
  actualReturnDate: Date;
  plannedExitDate: Date;
  plannedReturnDate: Date;
  reason: AttendanceReason;
  status: AttendanceStatus;
  guest: Guest;
  note: string;
  enableExit: boolean;
  common?: boolean;
}

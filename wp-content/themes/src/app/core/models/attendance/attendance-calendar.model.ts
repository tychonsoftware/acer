import {Attendance} from '@app/core/models/attendance/attendance.model';

export interface AttendanceCalendar {
  actualAttendanceDTO: Attendance[];
  plannedAttendanceDTO: Attendance[];
}

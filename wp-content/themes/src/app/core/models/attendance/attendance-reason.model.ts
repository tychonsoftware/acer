export interface AttendanceReason {
  id?: number;
  uuid: string;
  description: string;
}

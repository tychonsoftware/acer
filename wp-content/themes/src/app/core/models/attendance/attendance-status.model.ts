export interface AttendanceStatus {
  id?: number;
  uuid: string;
  description: string;
}

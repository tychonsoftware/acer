import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {registerLocaleData} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgxPopper} from 'angular-popper';
import {AlertModule} from 'ngx-bootstrap/alert';
import {OAuthModule} from 'angular-oauth2-oidc';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from '@app/core/core.module';
import {SharedModule} from '@app/shared/shared.module';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import localeIt from '@angular/common/locales/it';
import {UserService} from '@app/core/http/users/user.service';

registerLocaleData(localeIt);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPopper,
    AlertModule.forRoot(),
    OAuthModule.forRoot(),
    SharedModule.forRoot(),
    CoreModule,
    MatSnackBarModule,
  ],
  bootstrap: [AppComponent],
  providers: [UserService,
    {
      provide: APP_INITIALIZER,
      useFactory: (userService: UserService) => () => userService.init(),
      deps: [UserService],
      multi: true
    }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}

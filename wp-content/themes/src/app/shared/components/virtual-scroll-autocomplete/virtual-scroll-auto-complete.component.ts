import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {isObservable, Observable} from 'rxjs';

interface IFilters {
  [key: string]: (value: any) => boolean;
}

@Component({
  selector: 'app-virtual-scroll-autocomplete',
  templateUrl: './virtual-scroll-auto-complete.component.html',
  styleUrls: ['./virtual-scroll-auto-complete.component.scss']
})
export class VirtualScrollAutoCompleteComponent implements OnInit {
  // Outputs
  @Output() selectedOption: EventEmitter<any> = new EventEmitter();

  // Inputs
  @Input() label = '';
  @Input() isFormSubmitted = false;
  @Input() control: FormControl = new FormControl();
  @Input() optionKey: string | undefined;
  @Input() optionsView: string[] | undefined;
  @Input() filterKey = '';
  @Input() formFieldClass: string | undefined;
  @Input() required = false;
  @Input() isAllUpperCase = false;
  @Input() isEmptyOption = false;
  @Input() labelOutside = false;

  _options: any[] = [];
  @Input() set options(options: Observable<any[]> | any[] | undefined) {
    if (options) {
      if (isObservable(options)) {
        options.subscribe(d => {
          this._options = d;
          this.filteredOptions = d;
        });
      } else {
        this._options = options;
        this.filteredOptions = options;
      }
    }
  }

  filteredOptions: any[] | undefined;
  height = '200px';

  ngOnInit(): void {
  }

  trackByFn(index: number, item: any): number {
    return index;
  }

  displayFn(option: any): string {
    const selectedOption = option && this.optionKey && option[this.optionKey] ? option[this.optionKey] : option;
    return (typeof selectedOption === 'string') ? selectedOption.toUpperCase() : selectedOption;
  }

  onInputChange(value: any): void {
    this.filteredOptions = this._filter(value);

    // Recompute how big the viewport should be.
    if (this.filteredOptions.length > 0) {
      this.height = (this.filteredOptions.length < 4) ? ((this.filteredOptions?.length * 50) + 50) + 'px' : '200px';
    } else {
      this.filteredOptions = this._options;
      this.height = '200px';
    }
  }

  onSelection(event: any): void {
    this.control.setValue(event.option.value);
    this.selectedOption.emit(event.option.value);
  }

  onTab(): void {
    this.filteredOptions?.forEach((option) => {
      if (option && this.optionKey && option[this.optionKey] === this.control?.value) {
        this.control.setValue(option);
        this.selectedOption.emit(option);
      }
    });
  }

  private _filter(value: any): any[] {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();
      if (this.filterKey === 'fullName' || this.filterKey === 'codeDescription' || this.filterKey === 'description') {
        return this._options.filter(o => o[this.filterKey].toLowerCase().indexOf(filterValue) > -1);
      }
      return this._options.filter(o => o[this.filterKey].toLowerCase().startsWith(filterValue));
    }
    return [];
  }

  /**
   * Filters an array of objects using custom predicates.
   *
   * @param array the array to filter
   * @param filters an object with the filter criteria
   */
  private filterArray(array: any[], filters: any): any[] {
    const filterKeys = Object.keys(filters);
    return array.filter(item => {
      // validates all filter criteria
      return filterKeys.every(key => {
        // ignores non-function predicates
        if (typeof filters[key] !== 'function') {
          return true;
        }
        return filters[key](item[key]);
      });
    });
  }
}

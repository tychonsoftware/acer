import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Constants} from '@app/core/constants/Constants';
import {openBlobFromFile} from '@app/shared/helpers/utility';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  @Input() isMultiple = false;
  @Input() accept: string | undefined;
  @Input() browseFileLabel: string | undefined;
  @Input() borderColor: string | undefined;
  @Input() textColor: string | undefined;
  @Output() fileUploadedChange: EventEmitter<File[]> = new EventEmitter();
  @Output() validationChange: EventEmitter<boolean> = new EventEmitter();
  @Input() isDisabled = false;
  @Input() files: any[] = [];
  @Input() newUI = false;
  fileTypeError: any = null;

  ngOnInit(): void {
  }

  /**
   * on file drop handler
   */
  onFileDropped(event: any): void {
    this.prepareFilesList(event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(event: any): void {
    this.prepareFilesList(event.target.files);
  }

  /**
   * Return the formatted file name.
   * @param file The file to show
   */
  getFileName(file: File): string {
    const fileExtension = Constants.FILE_EXTENSION_REGEX.exec(file.name);
    return file?.name.length > 25 ? `${file.name.slice(0, 20)}...${fileExtension ? fileExtension[1] : ''}` : file.name;
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number): void {
    this.files.splice(index, 1);
    this.fileUploadedChange.emit(this.files);
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number): void {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 5;
          }
        }, 50);
      }
    }, 50);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>): void {
    this.fileTypeError = null;
    const types = this.accept?.split(',');
    for (const item of files) {
      if (!types?.includes(item.type) || !this.isValidImageExtension(item)) {
        this.fileTypeError = !types?.includes(item.type) ? item.type : this.fileTypeError;
        this.validationChange.next(true);
        this.files = [];
        return;
      }else {
        this.validationChange.next(false);
      }
      item.progress = 0;
      this.files.pop();
      this.files.push(item);
    }
    this.uploadFilesSimulator(0);
    this.fileUploadedChange.emit(this.files);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes: number, decimals?: number): string {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals && decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  isValidImageExtension(item: any): boolean {
    const fileExt = item.name.split('.').pop().toLowerCase();
    const image = this.accept?.split('/')[0];
    if (!this.accept?.includes(image + '/' + fileExt)){
      this.fileTypeError = image + '/' + fileExt;
      return false;
    }
    this.fileTypeError = undefined;
    return true;
  }

  openFile(file: any) {
	if (file) {
		openBlobFromFile(file);
	}
  }
}

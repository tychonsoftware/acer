import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  HostListener,
  Inject,
  OnDestroy,
  Type,
  ViewChild
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {InsertionDirective} from '@app/shared/directives/insertion.directive';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {Guest, GuestMinimalTransition} from '@app/core/models';

export interface IFormDialogConfig {
  editFormData?: any;
  title: string;
  confirmText: string;
  cancelText: string;
  patientUUID?: string;
  guest: GuestMinimalTransition;
  isGuestExit: boolean;
  disableSave: boolean;
  isExemptionTable: boolean;
}

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.scss']
})
export class FormDialogComponent implements AfterViewInit, OnDestroy {
  componentRef: ComponentRef<any> | undefined;

  @ViewChild(InsertionDirective)
  insertionPoint!: InsertionDirective;

  childComponentType: Type<FormContentDirective> | undefined;
  formId = 'dialogForm';

  constructor(@Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
              private componentFactoryResolver: ComponentFactoryResolver,
              private cd: ChangeDetectorRef,
              private mdDialogRef: MatDialogRef<FormDialogComponent>) {
  }

  ngAfterViewInit(): void {
    if (this.childComponentType) {
      this._loadChildComponent(this.childComponentType);
      this.cd.detectChanges();
    }
  }

  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  public cancel(): void {
    this._close(false);
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this._close(false);
  }

  private _close(value: any): void {
    this.mdDialogRef.close(value);
  }

  private _loadChildComponent(componentType: Type<FormContentDirective>): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);

    const viewContainerRef = this.insertionPoint.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    // Set the dialog component parameters
    this.componentRef.instance.formId = this.formId;
    this.componentRef.instance.validSubmit.subscribe((data: any) => this._close(data));
  }
}

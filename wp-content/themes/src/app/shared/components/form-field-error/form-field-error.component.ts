import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-form-field-error',
  template: `
    <ng-container *ngIf="isFieldValid()">
      <div id="error" class="font-weight-bold">{{getErrorMessage()}}</div>
    </ng-container>
  `,
  styles: [`
    #error {
      font-size: 0.8rem;
    }
  `]
})
export class FormFieldErrorComponent {

  @Input()
  formInput: AbstractControl | null | undefined;

  @Input()
  isFormSubmitted = false;

  @Input()
  min: number | undefined;

  @Input()
  max: number | undefined;

  @Input()
  patternName: string | undefined;

  constructor(private translateService: TranslateService) {
  }

  /**
   * Helper function to check if form field is valid.
   */
  isFieldValid(): boolean {
    if (this.formInput) {
      return (this.formInput.invalid && (this.formInput.touched || this.formInput.dirty)) ||
        (this.formInput.untouched && this.isFormSubmitted);
    }
    return false;
  }

  /**
   * Show the error message for the specified form field.
   */
  getErrorMessage(): string {
    if (this.formInput) {
      // required error
      if (this.formInput.hasError('required')) {
        return this.translateService.instant('formErrors.required');
      }
      // number error
      else if (this.formInput.hasError('number')) {
        return this.translateService.instant('formErrors.number');
      }
      // decimal error
      else if (this.formInput.hasError('decimal')) {
        return this.translateService.instant('formErrors.decimal');
      }
      // min error
      else if (this.formInput.hasError('min') || this.formInput.hasError('minlength')) {
        const key = 'formErrors.' + (this.formInput.hasError('min') ? 'min' : 'minlength');
        return this.translateService.instant(key, {min: this.min});
      }
      // max error
      else if (this.formInput.hasError('max') || this.formInput.hasError('maxlength')) {
        const key = 'formErrors.' + (this.formInput.hasError('max') ? 'max' : 'maxlength');
        return this.translateService.instant(key, {max: this.max});
      }
      // pattern error
      else if (this.formInput.hasError('pattern')) {
        const key = 'formErrors.' + (this.patternName?.includes('password') ? 'passwordPattern' : 'pattern');
        return this.translateService.instant(key, {pattern: this.patternName});
      }
    }
    return '';
  }
}

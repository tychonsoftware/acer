import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  HostListener,
  Inject,
  OnDestroy,
  Type,
  ViewChild
} from '@angular/core';
import {InsertionDirective} from '@app/shared/directives/insertion.directive';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormGroup} from '@angular/forms';


export interface IDialogConfig {
  title: string;
  confirmText: string;
  cancelText: string;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements AfterViewInit, OnDestroy {

  componentRef: ComponentRef<any> | undefined;

  @ViewChild(InsertionDirective)
  insertionPoint!: InsertionDirective;

  childComponentType: Type<any> | undefined;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IDialogConfig,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef,
    private mdDialogRef: MatDialogRef<DialogComponent>) {
  }

  ngAfterViewInit(): void {
    if (this.childComponentType) {
      this.loadChildComponent(this.childComponentType);
      this.cd.detectChanges();
    }
  }

  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  public cancel(): void {
    this.close(false);
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.close(false);
  }

  public confirm(): void {
    this.close(true);
  }

  private close(value: any): void {
    this.mdDialogRef.close(value);
  }

  loadChildComponent(componentType: Type<any>): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);

    const viewContainerRef = this.insertionPoint.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
  }
}

import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Guest, GuestMinimalTransition, GuestTransition} from '@app/core/models';
@Component({
  selector: 'app-guest-menu',
  templateUrl: './guest-menu.component.html',
  styleUrls: ['./guest-menu.component.scss'],
})
export class GuestMenuComponent implements AfterViewInit, OnInit {

  @Input() guest: any;

  @Input() isEdited = false;

  @Input() isDismiss = false;

  @Output() selectedMenuChange = new EventEmitter<number>();

  @Output() modifica = new EventEmitter<any>();

  @Output() dismiss = new EventEmitter<any>();

  selectedMenu = 1;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  setSelectedMenu(value: number): void {
    this.selectedMenuChange.emit(value);
    this.selectedMenu = value;
  }

  // Restituisce la data calcolata, in base all'anno di nascita preso in input
  getAge(birthDate: Date): number {
    const timeDiff = Math.abs(Date.now() - new Date(birthDate).getTime());
    return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
  }

  changeDismiss(): void {
    this.dismiss.emit();
  }

  changeModifica(): void {
    this.modifica.emit();
  }
}

import {Component, HostListener, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export interface IConfirmDialogConfig {
  title: string;
  message: string;
  confirmText: string;
  cancelText: string;
}

@Component({
  selector: 'app-confirm-dialog',
  template: `
    <div>
      <h1 mat-dialog-title [innerHTML]="title | titlecase"></h1>
    </div>
    <div mat-dialog-content>
      <p [innerHTML]="message"></p>
    </div>
    <div mat-dialog-actions class="float-lg-right">
      <button mat-raised-button color="warn" (click)="cancel()" [innerHTML]="cancelText"></button>
      <button mat-raised-button (click)="confirm()" [innerHTML]="confirmText"></button>
    </div>
  `
})
export class ConfirmDialogComponent {
  title = '';
  message = '';
  confirmText = 'Conferma';
  cancelText = 'Cancella';

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: IConfirmDialogConfig,
    private mdDialogRef: MatDialogRef<ConfirmDialogComponent>) {
    if (data) {
      this.title = data.title || this.title;
      this.message = data.message || this.message;
      this.confirmText = data.confirmText || this.confirmText;
      this.cancelText = data.cancelText || this.cancelText;
    }
  }

  public confirm(): void {
    this.close(true);
  }

  public cancel(): void {
    this.close(false);
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.close(false);
  }

  private close(value: any): void {
    this.mdDialogRef.close(value);
  }
}

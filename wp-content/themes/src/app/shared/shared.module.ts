import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule, formatDate} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LayoutModule} from '@angular/cdk/layout';
import {MaterialLibs} from '@app/shared/material-libs';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FormFieldErrorComponent} from '@app/shared/components/form-field-error/form-field-error.component';
import {ConfirmDialogComponent} from '@app/shared/components/confirm-dialog/confirm-dialog.component';
import {WarningDialogComponent} from '@app/shared/components/warning-dialog/warning-dialog.component';
import {FormDialogComponent} from '@app/shared/components/form-dialog/form-dialog.component';
import {InsertionDirective} from '@app/shared/directives/insertion.directive';
import {DialogComponent} from '@app/shared/components/dialog/dialog.component';
import {GenericTableComponent} from './components/generic-table/generic-table.component';
import {VirtualScrollAutoCompleteComponent} from './components/virtual-scroll-autocomplete/virtual-scroll-auto-complete.component';
import {FileUploadComponent} from './components/file-upload/file-upload.component';
import {GuestMenuComponent} from './components/guest-menu/guest-menu.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {AppDateAdapter} from '@app/shared/helpers/format-datepicker';
import {NGX_MAT_DATE_FORMATS} from '@angular-material-components/datetime-picker';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import {DndDirective} from './directives/dnd.directive';
import {DialogService} from '@app/core/services/dialog.service';
import { Moment } from 'moment';
import {SnackBarService} from '@app/core/services/snack-bar.service';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/app/', '.json');
}

class CustomDateAdapter extends MomentDateAdapter {

  getDayOfWeekNames(style: 'long' | 'short' | 'narrow'): string[] {
    return ['dom', 'lun', 'mar', 'mer', 'gio', 'ven', 'sab'];
  }
  format(date: Moment, displayFormat: any): string {
    return formatDate(new Date(date.toDate()), 'dd/MMM/yyyy', 'it-IT');
  }
}

export const MATERIAL_DATEPICKER_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MMM/YY',
    monthYearLabel: 'DD/MMM/YYYY',
    dateA11yLabel: 'DD/MMM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

export const MATERIAL_DATETIMEPICKER_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MMM/YYYY HH:mm',
    monthYearLabel: 'DD/MMM/YYYY',
    dateA11yLabel: 'DD/MMM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    WarningDialogComponent,
    FormDialogComponent,
    InsertionDirective,
    DialogComponent,
    FormFieldErrorComponent,
    GenericTableComponent,
    GuestMenuComponent,
    VirtualScrollAutoCompleteComponent,
    FileUploadComponent,
    SnackBarComponent,

    // Directives
    InsertionDirective,
    DndDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    ...MaterialLibs,
    MDBBootstrapModule.forRoot(),

    // Per la traduzione eventuale in altre lingue
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    })
  ],
    exports: [
        // Modules
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
        ...MaterialLibs,
    FormFieldErrorComponent,
    GenericTableComponent,
    GuestMenuComponent,
    VirtualScrollAutoCompleteComponent,
    FileUploadComponent
  ],

  providers: [
    SnackBarService,
    DialogService,
    {provide: MAT_DATE_LOCALE, useValue: 'it-IT'},
    {provide: LOCALE_ID, useValue: 'it-IT'},
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: NGX_MAT_DATE_FORMATS, useValue: MATERIAL_DATETIMEPICKER_FORMATS},
    {provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}},
    {provide: DateAdapter, useClass: CustomDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MATERIAL_DATEPICKER_FORMATS}
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule
    };
  }
}

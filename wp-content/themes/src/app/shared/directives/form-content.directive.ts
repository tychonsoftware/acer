import {Directive, EventEmitter, Injectable, Input, Output, ViewChild} from '@angular/core';
import {FormGroup, NgForm} from '@angular/forms';
import {Nation} from '@app/core/models';

@Directive()
@Injectable()
export abstract class FormContentDirective {
  @ViewChild('f') ngForm: NgForm | undefined;

  protected _formId = '';

  @Input()
  public set formId(formId: string) {
    this._formId = formId;
  }

  public get formId(): string {
    return this._formId;
  }

  @Input()
  editFormData: any;

  // Output event on valid form submit
  @Output()
  protected validSubmit = new EventEmitter<any>();

  protected form?: FormGroup;

  public isEdit = false;

  public formSubmitAttempt = false;

  /**
   * Verify if the required input field have been provided for this component.
   * @param input The input field to verify.
   */
  checkRequiredFields(input: any): void {
    if (input === null) {
      throw new Error('Attribute "form" is required');
    }
  }

  /**
   * Compare two object on their ID.
   * @param c1 First component.
   * @param c2 Second component.
   */
  compare(c1: { id: number }, c2: { id: number }): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

  /**
   * On form submit
   */
  onSubmit(withRowValue?: boolean): void {
    this.formSubmitAttempt = true;
    if (this.form?.valid) {
      const formValue = (withRowValue ? this.form.getRawValue() : this.form.value);
      let dataToEmit: any;
      const currentDate = new Date();

      if (this.editFormData) {
        dataToEmit = {
          ...this.editFormData,
          ...formValue,
          lastDateModify: currentDate,
        };
      } else {
        dataToEmit = {
          ...formValue,
          dateInsert: currentDate,
        };
      }

      this.validSubmit.emit(dataToEmit);
    }
  }
  onFormSubmitWithoutDate(withRowValue?: boolean): void {
    this.formSubmitAttempt = true;
    if (this.form?.valid) {
      const formValue = (withRowValue ? this.form.getRawValue() : this.form.value);
      let dataToEmit: any;
      if (this.editFormData) {
        dataToEmit = {
          ...this.editFormData,
          ...formValue,
        };
      } else {
        dataToEmit = {
          ...formValue,
        };
      }
      this.validSubmit.emit(dataToEmit);
    }
  }
  patchFormValue(data: any): void {
    this.form?.patchValue(data);
  }

  /**
   * Update the user form with the provided data.
   * @private
   */
  updateForm(data?: any): void {
    this.form?.setValidators([]);
    this.form?.patchValue(data);
  }

  disableForm(): void {
    this.form?.disable();
  }

  clearSubmitted(): void {
    this.formSubmitAttempt = false;
    const value = this.form?.value;
    this.ngForm?.resetForm();
    this.form?.setValue(value);
  }

  markAsSubmitted(): void {
    this.formSubmitAttempt = true;
  }
}

export function compareById(c1: { id: number }, c2: { id: number }): boolean {
  return c1 && c2 && c1.id === c2.id;
}

export function compareByUUID(c1: { uuid: string }, c2: { uuid: string }): boolean {
  return c1 && c2 && c1.uuid.toLowerCase().startsWith(c2.uuid.toLowerCase());
}

export function compareGuests(c1: any, c2: any): boolean {
  if (typeof c2 === 'string'){
    return c1 && c2 && c1.uuid.toLowerCase().startsWith(c2.toLowerCase());
  }
  return compareByUUID(c1, c2);
}

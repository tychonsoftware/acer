import {formatDate} from '@angular/common';

export enum OrderType {asc, desc}

export function mapObjectDateKeyToDate(obj: any): any {
  if (obj) {
    Object.keys(obj).forEach(key => {
      if (key.match(/Date/i)) {
        if (obj[key] && (typeof obj[key] === 'number')) {
          obj[key] = new Date(obj[key]);
        }
      } else if ((typeof obj[key] === 'object') && !key.match(/uuid/i)) {
        mapObjectDateKeyToDate(obj[key]);
      }
    });
  }

  return obj;
}

export function sortObjectByDate(obj: any, dateKey: string, order?: OrderType): any {
  if (order && (order === OrderType.desc)) {
    return obj.sort((a: any, b: any) => getDateTime(b[dateKey]) - getDateTime(a[dateKey]));
  } else {
    return obj.sort((a: any, b: any) => getDateTime(a[dateKey]) - getDateTime(b[dateKey]));
  }
}

export function sortObjectByPeriod(obj: any, dateKey: string, order?: OrderType): any {
  if (order && (order === OrderType.desc)) {
    return obj.sort((a: any, b: any) => getPeriodDateTime(b[dateKey]) - getPeriodDateTime(a[dateKey]));
  } else {
    return obj.sort((a: any, b: any) => getPeriodDateTime(a[dateKey]) - getPeriodDateTime(b[dateKey]));
  }
}

export function getPeriodDateTime(period?: string): number {
  return period != null ? getDateFromPeriod(period).getTime() : 0;
}

export function getDateTime(date?: Date): number {
  return date != null ? date.getTime() : 0;
}

export function getPeriod(month: string, year: string): string {
  return `${(month.length === 1 ? `0${month}` : month)}/${year}`;
}

export function getLastMonthPeriod(): string {
  const d = new Date();
  d.setMonth(d.getMonth() - 1);
  return formatDate(d, 'MM/yyyy', 'it-IT');
}

export function getCurrentMonthPeriod(): string {
  const d = new Date();
  d.setMonth(d.getMonth());
  return formatDate(d, 'MM/yyyy', 'it-IT');
}

export function getFileNameFromPath(fullPath: string): string {
  return fullPath.replace(/^.*[\\\/]/, '');
}

export function getMonthNameFromDate(date: Date): string {
  return new Date(date).toLocaleDateString('default', {month: 'long'});
}

export function getDateFromPeriod(period: string): Date {
  return new Date(`01/${period}`.replace(/(\d{2})\/(\d{2})\/(\d{4})/, '$2/$1/$3'));
}

export function base64ToDownloadLink(dataInBase64: string, fileName: string): HTMLAnchorElement {
  const linkSource = dataInBase64;
  const downloadLink = document.createElement('a');
  downloadLink.href = linkSource;
  downloadLink.download = fileName;
  return downloadLink;
}

export function sortObject(obj: any, dateKey: string, order?: OrderType): any {
  if (order && (order === OrderType.desc)) {
    return obj.sort((a: any, b: any) => b[dateKey] - a[dateKey]);
  } else {
    return obj.sort((a: any, b: any) => a[dateKey] - b[dateKey]);
  }
}

export function getTransformDate(date: any): string {
  return formatDate(date, 'dd/MM/YYYY', 'it-IT');
}

export function getNumberOfDaysFromDates (startDate: any, endDate: any): number {
	return Math.floor((endDate - startDate) / 1000 / 60 / 60 / 24) + 1;
}

export function getStartDateFromEndAndDays (numDays: number, endDate: any): Date {
	var dateRet = new Date(endDate + (1000 * 60 * 60 * 24));
	dateRet.setDate(dateRet.getDate() - numDays - 1);
	return dateRet;
}

export function getEndDateFromStartAndDays (numDays: number, startDate: any): Date {
	var dateRet = new Date(startDate + (1000 * 60 * 60 * 24));
	dateRet.setDate(dateRet.getDate() + numDays - 1);
	return dateRet;
}

export function openBlobFromBase64 (base64File: string): void {
	fetch(base64File)
		.then(res => res.blob())
		.then(blob => {
			var url = window.URL.createObjectURL(blob);
			window.open(url);
		});
}

export function openBlobFromFile (file: any): void {
	var url = window.URL.createObjectURL(file);
	window.open(url);
}


import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentsRoutingModule } from './documents-routing.module';
import { DocumentsComponent } from './documents.component';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {DocumentsTableComponent} from '@app/modules/documents/documents-table/documents-table.component';
import {DocumentFormComponent} from '@app/modules/documents/document-form/document-form.component';
import {DocumentUploadFormComponent} from '@app/modules/documents/document-upload-form/document-upload-form.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/documents/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    DocumentsComponent,
    DocumentsTableComponent,
    DocumentUploadFormComponent,
    DocumentFormComponent
  ],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DocumentsModule { }

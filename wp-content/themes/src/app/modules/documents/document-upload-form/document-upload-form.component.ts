import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {DocumentTemplateTypeService, FileService, GuestService} from '@app/core/http';
import {Observable} from 'rxjs';
import {City, Document, DocumentTemplateType, GuestMinimalTransition} from '@app/core/models';
import {compareById} from '@app/shared/helpers/comparators';
import {GuestDropDown} from '@app/core/models/guests/guest-dropdown.model';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-document-upload-form',
  templateUrl: './document-upload-form.component.html',
  styleUrls: ['./document-upload-form.component.scss']
})
export class DocumentUploadFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    guestMinimal: [null, Validators.required],
    date: [null, Validators.required],
    documentTemplateType: [null, Validators.required],
    description: [null],
    documentName: [null, Validators.required],
    documentContent: [null, Validators.required]
  });
  guests: GuestDropDown[] = [];
  documentTemplateTypes$: Observable<DocumentTemplateType[]> | undefined;
  compareById = compareById;
  dataInBase64 = '';
  files: File[] = [];
  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private documentTemplateTypeService: DocumentTemplateTypeService,
    private guestService: GuestService,
    private fileService: FileService
  ) {
    // Calling super to init the extended class
    super();
    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      console.log('Document Edit form data ', data.editFormData);
      this.isEdit = true;
      if (data.editFormData.document && data.editFormData.document.staticPath && data.editFormData.document.dynamicPath){
        const filePath = `${data.editFormData.document?.staticPath.path}/${data.editFormData.document.dynamicPath}`;
        this.fileService.downloadFile(filePath).subscribe(result => {
          this.dataInBase64 = result;
          this.files.push(new File([result], data.editFormData.documentName));
        },
          () => this.updateForm(data.editFormData),
          () => this.updateForm(data.editFormData));
      }else {
        this.updateForm(data.editFormData);
      }
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
    this.guestService.getMinimal().pipe(
      tap(res => {}),
      map(res => res.map(guest => ({...guest, fullName: guest.firstName + ' ' + guest.lastName})))
    ).subscribe(res => {
      this.guests = res;
      console.log(res);
    });
    this.documentTemplateTypes$ = this.documentTemplateTypeService.getAllExceptLetterHead();
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * Update the document form data
   * @param data Data to update the form.
   */
  updateForm(data: Document): void {
    this.form?.patchValue({
      ...data,
      // Dates
      date: data.date != null ? new Date(data.date) : null,
      documentContent: this.dataInBase64
    });
    if (data.guestMinimal) {
      const documentGuest = {
        ...data.guestMinimal,
        fullName: data.guestMinimal.firstName + ' ' + data.guestMinimal.lastName
      };
      this.form?.patchValue({
        guestMinimal: documentGuest,
      });
    }
  }

  getFileUploadBorder(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'border-danger' : 'border-dark';
  }

  getFileUploadTextColor(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'text-danger' : 'text-dark';
  }

  /**
   * On guest selection change
   * @param files Uploaded file/s.
   */
  onFileUploadedChange(files: any[]): void {
    if (files && files[0]) {
      const file = files[0];
      this.form.get('documentName')?.setValue(file.name);

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = ( () => {
        this.form.get('documentContent')?.setValue(reader.result);
      } );
    }
  }

  guestFormControl(): FormControl {
    return this.form.get('guestMinimal') as FormControl;
  }
}

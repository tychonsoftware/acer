import {Component, Input} from '@angular/core';
import {Document} from '@app/core/models';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {GenericTableComponent, TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {GuestDocuments} from '@app/core/models/document/guest-documents.model';
import {MatTableDataSource} from '@angular/material/table';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {DocumentUploadFormComponent} from '@app/modules/documents/document-upload-form/document-upload-form.component';
import {DialogService} from '@app/core/services/dialog.service';
import {MatDialogConfig} from '@angular/material/dialog';
import {DocumentService, FileService} from '@app/core/http';
import {base64ToDownloadLink, getFileNameFromPath} from '@app/shared/helpers/utility';
import {formatDate} from '@angular/common';
import {Functionalities} from '@app/core/constants/Functionalities';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-documents-table',
  templateUrl: './documents-table.component.html',
  styleUrls: ['./documents-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DocumentsTableComponent extends GenericTableComponent<GuestDocuments> {
  public documentTableColumns: TableColumn[] = [
    {
      name: 'Data Documento',
      dataKey: 'date',
      position: 'center',
      isDate: true,
      isSortable: true
    },
    {
      name: 'Descrizione',
      dataKey: 'description',
      position: 'center',
      isDate: false,
      isSortable: true
    },
    {
      name: 'Nome Documento',
      dataKey: 'documentName',
      position: 'center',
      isDate: false,
      isSortable: true
    },
    {
      name: 'Tipologia',
      dataKey: 'documentTemplateType.description',
      position: 'center',
      isDate: false,
      isSortable: true
    }
  ];

  isEditable = false;

  @Input() set tableData(data: Observable<Document[]> | Document[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  dischargedStatus = 2;
  expandedElement?: Document;
  dataForSearch?: GuestDocuments[];

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '35%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    private documentService: DocumentService,
    private fileService: FileService,
    private userSecurityService: UserService
  ) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
    this.sort.subscribe(data => {
      this.tableDataSource.data.sort((a, b) => {
        if (data.direction === 'desc') {
          return a[data.active] < b[data.active] ? 1 : -1;
        } else {
          return a[data.active] > b[data.active] ? 1 : -1;
        }
      });
    });
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.DOCUMENT)) {
      this.isEditable = true;
    }
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: Document[]): void {
    data.forEach(document => {
      this.toUpperCaseFields(document);
    });
    const getGuestDocuments = this._getGuestDocuments(data);
    this.dataForSearch = JSON.parse(JSON.stringify(getGuestDocuments));
    this.tableDataSource = new MatTableDataSource(getGuestDocuments);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }

  private _getGuestDocuments(data: Document[]): GuestDocuments[] {
    this.sortDocumentsByGuest(data);
    const guestDocumentsMap = data.reduce((obj: any, item) => {
      const uuid = item.guestMinimal?.uuid;
      if (uuid) {
        obj[uuid] ? obj[uuid].documents.push(item) : obj[uuid] = {
          firstName: item.guestMinimal?.firstName,
          lastName: item.guestMinimal?.lastName,
          fiscalCode: item.guestMinimal?.fiscalCode,
          birthDate: item.guestMinimal?.birthDate,
          documents: [item]
        };
      }
      return obj;
    }, {});

    return Object.values(guestDocumentsMap);
  }

  /**
   * Open the Contract form dialog.
   * @param contract Data of the Contract to edit.
   */
  openFormDialog(document?: Document): void {
    if (document && document.id) {
      // Getting the contract data from the API
      this._openDocumentFormDialog(document);
    } else {
      this._openDocumentFormDialog();
    }
  }

  /**
   * Open the Contract create/edit dialog.
   * @param editedContract The Contract data only for the edit case.
   * @private
   */
  private _openDocumentFormDialog(editedDocument?: Document): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedDocument,
      title: this.translateService.instant(`documents.formDialog.${editedDocument?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('documents.uploadFormDialog.cancelText'),
      confirmText: this.translateService.instant('documents.uploadFormDialog.confirmText')
    };

    // Open the dialog with the DocumentUploadFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(DocumentUploadFormComponent, data, this.dialogConfig);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(uploadDoc => this._uploadDocument(uploadDoc as Document));
  }

  private _uploadDocument(document?: Document): void {
    console.log('Update document', document);
    if (document) {
      const documentGuest = {
        uuid: document.guestMinimal.uuid,
        historicResidence: document.guestMinimal.historicResidence,
        firstName: document.guestMinimal.firstName,
        lastName: document.guestMinimal.lastName,
        fiscalCode: document.guestMinimal.fiscalCode,
        birthDate: document.guestMinimal.birthDate,
        birthPlace: document.guestMinimal.birthPlace,
        acceptanceDate: document.guestMinimal.acceptanceDate,
        recoveryTaxRegime: document.guestMinimal.recoveryTaxRegime,
      };
      const uploadedDocument = {
        id: document.id,
        guestMinimal: documentGuest,
        date: document.date,
        documentContent: document.documentContent,
        documentName: document.documentName,
        description: document.description,
        documentTemplateType: document.documentTemplateType
      };
      delete uploadedDocument.documentTemplateType.uuid;
      if (uploadedDocument.id) { // edit case
        this.documentService.update(uploadedDocument).subscribe(res => {
          this.documentService.observableData$.subscribe(data => {
            this.tableDataSource.data.forEach(guest => {
              guest.documents.forEach((doc: Document) => {
                if (doc.id === res.id) {
                  doc.guestMinimal = res.guestMinimal;
                  doc.date = res.date;
                  doc.documentContent = res.documentContent;
                  doc.documentName = res.documentName;
                  doc.description = res.description;
                  doc.document = res.document;
                  doc.documentTemplateType = res.documentTemplateType;
                }
              });
            });
          });

        });
      } else {
        this.documentService.createAndGetObservable(uploadedDocument);
      }
    }
  }

  /**
   * Open the delete Document dialog.
   * @param document The Document to delete.
   */
  onDeleteClick(document: Document): void {
    const data = {
      title: this.translateService.instant('documents.deleteDialog.title'),
      message: this.translateService.instant('documents.deleteDialog.message', {value: document.documentName}),
      cancelText: this.translateService.instant('documents.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('documents.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteDocument(document));
  }

  /**
   * Delete Document
   * @param document The document to delete.
   * @private
   */
  private _deleteDocument(document: Document): void {
    if (document.id) {
      this.documentService.deleteDocument(document.id).subscribe(_ => {
        this.documentService.getDocumentsByGuestStatus(this.dischargedStatus).subscribe(res => {
          this.setTableDataSource(res);
        });
      });
    }
  }

  onDownloadClick(document: Document): void {
    const path = `${document?.document.staticPath?.path}/${document?.document?.dynamicPath}`;
    const fileName = getFileNameFromPath(document?.document?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  onSort(s: any): void {
    this.tableDataSource.data.forEach(guest => {
      guest.documents.sort((a: any, b: any) => {
        if (s.direction === 'desc') {
          return a[s.active] < b[s.active] ? 1 : -1;
        } else {
          return a[s.active] > b[s.active] ? 1 : -1;
        }
      });
    });
  }

  /**
   * Apply the filter on the expanded data.
   * @param event Filter write event
   * @param invoicePeriod data for setting filted data
   */
  applyFilterToExpandedData(event: Event, guestDocuments: GuestDocuments): void {
    const filterValue = (event.target as HTMLInputElement).value;
    const filter = filterValue.trim().toLowerCase();
    this.dataForSearch?.forEach(data => {

      if (data.firstName === guestDocuments.firstName && data.lastName === guestDocuments.lastName) {
        guestDocuments.documents = [];
        data.documents.forEach(document => {
          const documentObj: any = {};
          documentObj.documentName = document.documentName;
          documentObj.date = formatDate(document.date, 'dd/MM/yyyy', 'it-IT'),
            documentObj.description = document.description;
          const documentStr = JSON.stringify(documentObj).toLowerCase();
          if (filterValue === '' || documentStr.indexOf(filter) !== -1) {
            guestDocuments.documents.push(document);
          }
        });
      }
    });
  }

  /**
   * Apply the filter on the table data.
   * @param event Filter write event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    this.tableDataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      console.log(dataStr);
      return dataStr.indexOf(filter) !== -1;
    };
  }

  toUpperCaseFields(document: Document): void {
    document.description = document.description?.toUpperCase();
    document.documentName = document.documentName?.toUpperCase();
    if (document.documentTemplateType) {
      document.documentTemplateType.description = document.documentTemplateType?.description?.toUpperCase();
    }
  }

  // Ordina gli elementi nella tabella, facendo un ordinamento per cognome e nome
  sortDocumentsByGuest(data: Document[]): void {

    // Effettuo ordinamento per cognome e nome
    data.sort((a, b) => {
      const user1 = (a.guestMinimal.lastName + a.guestMinimal.firstName).trim().toLowerCase();
      const user2 = (b.guestMinimal.lastName + b.guestMinimal.firstName).trim().toLowerCase();
      return user1 > user2 ? 1 : -1;
    });
  }

  findDischarged(event: MatSlideToggleChange): void {
    (event.checked) ? this.dischargedStatus = 3 : this.dischargedStatus = 2;
    const dischargedData = this.documentService.getDocumentsByGuestStatus(this.dischargedStatus);
    dischargedData.subscribe(data => {
      this.setTableDataSource(data);
    });
  }

}

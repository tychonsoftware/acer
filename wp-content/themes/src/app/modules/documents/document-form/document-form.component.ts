import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {DocumentTemplate, GuestMinimalTransition} from '@app/core/models';
import {GuestService} from '@app/core/http';
import {DocumentTemplateService} from '@app/core/http/document/document-template.service';
import {GuestDropDown} from '@app/core/models/guests/guest-dropdown.model';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    guestMinimal: [null, Validators.required],
    document: [null, Validators.required]
  });
  guests: GuestDropDown[] = [];
  documentTemplates$: Observable<DocumentTemplate[]> | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private guestService: GuestService,
    private documentTemplateService: DocumentTemplateService
  ) {
    // Calling super to init the extended class
    super();

    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      this.isEdit = true;
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
    this.guestService.getMinimal().pipe(
      tap(res => {}),
      map(res => res.map(guest => ({...guest, fullName: guest.firstName + ' ' + guest.lastName})))
    ).subscribe(res => {
      this.guests = res;
      console.log(res);
    });
    this.documentTemplates$ = this.documentTemplateService.getAllDocumentExceptLetterHead();
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * Update the document form data
   * @param data Data to update the form.
   */
  updateForm(data: Document): void {

    // Patching data for recovery tax regime form
    this.form?.patchValue({
      ...data
    });
  }

  guestFormControl(): FormControl {
    return this.form.get('guestMinimal') as FormControl;
  }
}

import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {MatDialogConfig} from '@angular/material/dialog';
import {DocumentFormComponent} from '@app/modules/documents/document-form/document-form.component';
import {DocumentUploadFormComponent} from '@app/modules/documents/document-upload-form/document-upload-form.component';
import {Document} from '@app/core/models';
import {DocumentService, FileService} from '@app/core/http';
import {tap} from 'rxjs/operators';
import {DocumentGenerate} from '@app/core/models/document/document.generate.model';
import {base64ToDownloadLink} from '@app/shared/helpers/utility';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

  public documentsTableColumns: TableColumn[] = [
    {
      name: 'Nome Ospite',
      dataKey: 'firstName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Cognome Ospite',
      dataKey: 'lastName',
      position: 'center',
      isSortable: true
    },
	{
      name: 'Data di Nascita',
      dataKey: 'birthDate',
      position: 'center',
      isSortable: true,
	  isDate: true
    },
	{
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true
    }
  ];

  documents$: Observable<Document[]> | undefined;

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '35%'
  };

  constructor(
    private translateService: TranslateService,
    private documentService: DocumentService,
    private fileService: FileService,
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this._getDocuments();
  }

  private _getDocuments(): void{
    this.documents$ = this.documentService.getDocumentsByGuestStatus(2).
    pipe(
      tap(res => {
        console.log('GET All Documents');
        console.log(res);
      }));
  }
  onSort(s: any): void {
    console.log(s);
  }

  /**
   * Open the Document form dialog.
   * @param document Data of the Document to edit.
   */
  openFormDialog(document?: DocumentGenerate): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: document,
      title: this.translateService.instant(`documents.formDialog.${document?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('documents.formDialog.cancelText'),
      confirmText: this.translateService.instant('documents.documentsForm.generateButton')
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(DocumentFormComponent, data, this.dialogConfig);

    // On Generate confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(doc => this._generateDocument(doc as DocumentGenerate));

  }

  /**
   * Open the Document Upload form dialog.
   * @param document Data of the Document to upload.
   */
  openUploadFormDialog(doc?: Document): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: doc,
      title: this.translateService.instant(`documents.uploadFormDialog.uploadTitle`),
      cancelText: this.translateService.instant('documents.uploadFormDialog.cancelText'),
      confirmText: this.translateService.instant('documents.uploadFormDialog.confirmText')
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(DocumentUploadFormComponent, data, this.dialogConfig);

    // On upload
    this.dialogService.confirmed(dialogRef)?.subscribe(uploadDoc => this._uploadDocument(uploadDoc as Document));
  }
  private _generateDocument(doc?: DocumentGenerate): void {
    console.log('Generate Document', doc);
    if (doc?.guestMinimal.uuid != null){
      this.fileService.placeHolding(doc.guestMinimal.uuid, doc.document.bodyContent).subscribe(html => {
        console.log('Generate document for template id ' , doc.document.id);
        console.log('Html for document generation ' , html);
        this.documentService.generate(doc.document.id, html).subscribe(result => {
          console.log('Generated Document');
          console.log(result);
          const nomeFile = doc.guestMinimal.lastName + '_documento_' + doc.document.documentType.description + '.pdf';
          base64ToDownloadLink(result, nomeFile).click();
        });
      });
    }
  }

  private _uploadDocument(document?: Document): void {
    console.log('Create document', document);
    if (document){
      delete document.guestMinimal.fullName;
      // const documentGuest = {
      //   uuid: document.guestMinimal.uuid,
      //   historicResidence: document.guestMinimal.historicResidence,
      //   firstName: document.guestMinimal.firstName,
      //   lastName: document.guestMinimal.lastName,
      //   fiscalCode: document.guestMinimal.fiscalCode,
      //   birthDate: document.guestMinimal.birthDate,
      //   birthPlace: document.guestMinimal.birthPlace,
      //   acceptanceDate: document.guestMinimal.acceptanceDate,
      //   recoveryTaxRegime: document.guestMinimal.recoveryTaxRegime,
      // };
      // const uploadedDocument =  {
      //   guestMinimal: documentGuest,
      //   date: document.date,
      //   documentContent: document.documentContent,
      //   documentName: document.documentName,
      //   description: document.description,
      //   documentTemplateType: document.documentTemplateType
      // };
      this.documentService.createAndGetObservable(document).subscribe(_ =>
        this._getDocuments()
      );
    }
  }
}

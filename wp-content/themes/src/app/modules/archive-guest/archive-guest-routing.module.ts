import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StepperComponent} from '@app/modules/acceptance/stepper/stepper.component';
import {ArchiveGuestComponent} from '@app/modules/archive-guest/archive-guest.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: ArchiveGuestComponent},
  {path: 'stepper', component: StepperComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchiveGuestRoutingModule {
}

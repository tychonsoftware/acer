import {NgModule} from '@angular/core';
import {SharedModule} from '@app/shared/shared.module';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {DD_MM_YYYY_DATE_FORMAT} from '@app/shared/helpers/format-datepicker';
import {ArchiveGuestComponent} from '@app/modules/archive-guest/archive-guest.component';
import {ArchiveGuestRoutingModule} from '@app/modules/archive-guest/archive-guest-routing.module';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/assistance-projects/', suffix: '.json'},
    {prefix: './assets/i18n/acceptance/', suffix: '.json'},
    {prefix: './assets/i18n/guest/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    ArchiveGuestComponent
  ],
  imports: [
    ArchiveGuestRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it'
    })
  ],
  providers: [
    // Per il datapicker
    {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_DATE_FORMAT},
    {provide: MAT_DATE_LOCALE, useValue: 'it-IT'},
  ]
})
export class ArchiveGuestModule {
}

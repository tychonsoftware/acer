import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {GuestTransition} from '@app/core/models';
import {GuestService} from '@app/core/http/guest/guest.service';
import {DatePipe, formatDate} from '@angular/common';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';

@Component({
  selector: 'app-guest',
  templateUrl: './archive-guest.component.html',
  styleUrls: ['./archive-guest.component.scss'],
})
export class ArchiveGuestComponent implements AfterViewInit, OnInit {

  // array dei valori da popolare
  patientsHospitalized: GuestTransition[] = [];
  dataSource: MatTableDataSource<GuestTransition> = new MatTableDataSource<GuestTransition>(this.patientsHospitalized);
  cardClass = 'img-label';
  cardAbsentClass = 'abs-img-label';
  @ViewChild(MatSort) sort: MatSort | undefined;
  // controllare lo stato del paziente assente
  esportazioneStato: boolean;
  isAccessible = false;

  datePipe = new DatePipe('it-IT');
  years: string[] = [];
  yearSelected: string | undefined;
  filterValue: string | undefined;

  constructor(
    private dialog: MatDialog,
    private route: Router,
    private guestService: GuestService,
    private userSecurityService: UserService
  ) {
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
    this.esportazioneStato = false;
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_DETAIL
      || functionality.functionality === Functionalities.GUEST_DETAIL_READ)) {
      this.isAccessible = true;
    }
  }

  ngOnInit(): void {
    this.guestService.getByStatus(3, false).subscribe(absent => {
      for (let i = 0; i <= absent.length; i++) {
        if (absent[i] !== undefined) {
          this.patientsHospitalized.push(absent[i]);
        }
      }
      this.esportazioneStato = true;
      this.patientsHospitalized.sort((a, b) => {
        const exitDateA = a.exitDate;
        const exitDateB = b.exitDate;
        if (!exitDateA && !exitDateB) {
          return 0;
        } else if (!exitDateA) {
          return 1;
        } else if (!exitDateB) {
          return -1;
        } else {
          return exitDateA > exitDateB ? -1 : 1;
        }
      });
      this.dataSource.data = this.patientsHospitalized;
      this.dataSource._updateChangeSubscription();
    });
    this._findInitDate();
    this._filterData();
  }

  // Dopo che il componente è stato inizializzato
  ngAfterViewInit(): void {
    // Definisco sort se esiste
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  }

  // Applica il filtro di ricerca
  applyFilter(event: Event): void {
    this.filterValue = (event.target as HTMLInputElement).value;
    this._filterData();
  }

  // Quando si fa clic su una riga, i dettagli si aprono con i dati dell'guest
  openGuestDetails(element: GuestTransition): void {
    if (element) {
      this.guestService.getByUUID(element.uuid ? element.uuid : '').subscribe(
        result => {
          element = result;
          this.route.navigate(['/archive-guest/stepper'], {state: {data: {element}}});
        });
    } else {
      this.route.navigate(['/archive-guest/stepper'], {state: {data: {element}}});
    }
  }

  // Restituisce la data calcolata, in base all'anno di nascita preso in input
  getAge(element: GuestTransition): number {
    if (element.birthDate !== undefined) {
      const timeDiff = Math.abs(Date.now() - new Date(element.birthDate as Date).getTime());
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
    return 0;
  }

  // Restituisce la data calcolata, in base all'anno di nascita preso in input
  getDate(element: Date | undefined): string {
    const transform = this.datePipe.transform(element, 'dd/MM/YYYY');
    return transform ? transform : '';
  }

  esporta(): void {
    this.guestService.exportGuests().subscribe((response: any) => {
      const blob = new Blob([response], {type: 'application/vnd.ms-excel'});
      const nomeFile = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'it-IT') + '_ospiti.xls';
      const a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = nomeFile;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    });
  }

  onChangeYear(): void {
    this._filterData();
  }

  private _findInitDate(): void {
    const today = new Date();
    const years = [];
    this.years = [];
    const currentYear = today.getFullYear();
    for (let year = currentYear; year > (currentYear - 10) ; year--) {
      years.push(year.toString());
    }
    this.years = years;
    this.yearSelected = today.getFullYear().toString();
  }

  private _filterData(): void {
    this.dataSource.filter = '1';
    this.dataSource.filterPredicate = (data: any) => {
      let isExist = true;
      let yearMatched = false;
      if (this.filterValue && this.filterValue !== ''){
        if (data.fullName?.toLowerCase().includes(this.filterValue?.trim().toLowerCase()) ||
          data.fiscalCode?.toLowerCase().includes(this.filterValue?.trim().toLowerCase())) {
          isExist = true;
        }else {
          isExist = false;
        }
      }
      if (this.yearSelected && this.yearSelected !== ''){
        if (data.exitDate){
          const exitDate = new Date(data.exitDate as Date);
          if (this.yearSelected === exitDate.getFullYear().toString()) {
            yearMatched = true;
          }
        }
      }
      return isExist && yearMatched;
    };
  }
}

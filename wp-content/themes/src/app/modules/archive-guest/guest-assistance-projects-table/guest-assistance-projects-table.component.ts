import {Component, Input, OnInit} from '@angular/core';
import {AssistanceProject, AssistanceProjectStatus} from '@app/core/models';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {GuestAssistanceProjects} from '@app/core/models/assistance-projects/guest-assistance-projects.model';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {AssistanceProjectStatusService} from '@app/core/http';
import {FormGroup, FormControl} from '@angular/forms';
import {OrderType, sortObject} from '@app/shared/helpers/utility';
import {UserService} from "@app/core/http/users/user.service";
import {environment} from "@env/environment";
import {Functionalities} from "@app/core/constants/Functionalities";

@Component({
  selector: 'app-guest-assistance-projects-table',
  templateUrl: './guest-assistance-projects-table.component.html',
  styleUrls: ['./guest-assistance-projects-table.component.scss'],
})
export class GuestAssistanceProjectsTableComponent extends GenericTableComponent<GuestAssistanceProjects> implements OnInit {

  @Input() set tableData(data: Observable<AssistanceProject[]> | AssistanceProject[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  expandedElement?: AssistanceProject;
  selectedDADate: Date | undefined;
  selectedADate: Date | undefined;
  selectedStatus: AssistanceProjectStatus | undefined;
  isEditable = false;

  assistanceProjectStatues$: Observable<AssistanceProjectStatus[]> | undefined;
  statusFormGroup = new FormGroup({
    status: new FormControl()
  });

  constructor(private assistanceProjectStatusService: AssistanceProjectStatusService,
              private userSecurityService: UserService) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_ASSISTANCE_PROJECT)) {
      this.isEditable = true;
    }
  }

  ngOnInit(): void {
    this.assistanceProjectStatues$ = this.assistanceProjectStatusService?.getAllAsObservable();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: AssistanceProject[]): void {
    data = sortObject(data, 'authDate', OrderType.desc);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }
  }

  applyDADateFilter(daDate: MatDatepickerInputEvent<any, any>): void {
    this.selectedDADate = daDate.value;
    this._filterData();
  }

  deleteDADateFilter(): void {
    this.selectedDADate = undefined;
    this._filterData();
  }

  deleteADateFilter(): void {
    this.selectedADate = undefined;
    this._filterData();
  }

  applyADateFilter(aDate: MatDatepickerInputEvent<any, any>): void {
    this.selectedADate = aDate.value;
    this._filterData();
  }

  onStatusSelection(): void {
    this.selectedStatus = this.statusFormGroup?.get('status')?.value;
    this._filterData();
  }

  private _filterData(): void {

    this.tableDataSource.filteredData = this.tableDataSource.data;

    if (this.selectedDADate || this.selectedADate) {
      this.tableDataSource.filteredData = this.tableDataSource.filteredData.filter(data => {
        if (this.selectedDADate && this.selectedADate) {
          return data.treatmentStrDate >= this.selectedDADate && data.treatmentStrDate <= this.selectedADate;
        } else if (this.selectedDADate) {
          return data.treatmentStrDate >= this.selectedDADate;
        } else if (this.selectedADate) {
          return data.treatmentStrDate <= this.selectedADate;
        }
        return data;
      });
    }

    if (this.selectedStatus) {
      this.tableDataSource.filteredData = this.tableDataSource.filteredData.filter(data => {
        return data.status?.description == this.statusFormGroup?.get('status')?.value?.description;
      });
    }

  }

  private _getGuestAssistanceProjects(data: AssistanceProject[]): GuestAssistanceProjects[] {
    const guestAssistanceProjectsMap = data.reduce((obj: any, item) => {
      const uuid = item.guestMinimal?.uuid;
      if (uuid) {
        obj[uuid] ? obj[uuid].assistanceProjects.push(item) : obj[uuid] = {
          firstName: item.guestMinimal?.firstName,
          lastName: item.guestMinimal?.lastName,
          assistanceProjects: [item]
        };
      }
      return obj;
    }, {});

    return Object.values(guestAssistanceProjectsMap);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configurations',
  template: `<router-outlet></router-outlet>`
})
export class ConfigurationsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

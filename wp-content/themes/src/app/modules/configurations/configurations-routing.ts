import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContractsComponent} from '@app/modules/configurations/contracts/contracts.component';
import {GeneralSettingsComponent} from '@app/modules/configurations/general-settings/general-settings.component';
import {AslRegistryComponent} from '@app/modules/configurations/asl-registry/asl-registry.component';
import {AreaRegistryComponent} from '@app/modules/configurations/area-registry/area-registry.component';

const routes: Routes = [
  {path: 'contracts', component: ContractsComponent},
  {path: 'general-settings', component: GeneralSettingsComponent},
  {path: 'asl-registry', component: AslRegistryComponent},
  {path: 'area-registry', component: AreaRegistryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule {
}

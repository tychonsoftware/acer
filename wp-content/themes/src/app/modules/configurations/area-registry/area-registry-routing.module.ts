import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AreaRegistryComponent} from '@app/modules/configurations/area-registry/area-registry.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AreaRegistryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreaRegistryRoutingModule {
}

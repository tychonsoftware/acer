import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Area} from '@app/core/models/shared/area.model';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';

@Component({
  selector: 'app-registry-base-area-table',
  templateUrl: './registry-base-area-table.component.html',
  styleUrls: ['./registry-base-area-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class RegistryBaseAreaTableComponent extends GenericTableComponent<Area> {

  @Input() set tableData(data: Observable<Area[]> | Area[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
      if (this.matPaginator) {
        this.tableDataSource.paginator = this.matPaginator;
      }
    }
  }

  @Output() ngRowSelected: EventEmitter<Area> = new EventEmitter();

  constructor() {
    super();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: Area[]): void {
	this.sortAreaByAreaCode(data);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }
  
  sortAreaByAreaCode(data: Area[]): void {
    // Effettuo ordinamento per codice area
    data.sort((a, b) => {
      const areaCode1 = a.areaCode;
      const areaCode2 = b.areaCode;
      return areaCode1 > areaCode2 ? 1 : -1;
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }

  rowSelected(element: Area): void {
    this.ngRowSelected.emit(element);
  }
}

import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, Validators} from '@angular/forms';
import {
  City,
} from '@app/core/models';
import {Area} from '@app/core/models/shared/area.model';
import {CityService} from '@app/core/http';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';

@Component({
  selector: 'app-area-registry-form',
  templateUrl: './area-registry-form.component.html',
  styleUrls: ['./area-registry-form.component.scss']
})
export class AreaRegistryFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],

    //General Data
    businessName: [null, Validators.required],
	headCity: [null, Validators.required],
    supplierCode: [null, Validators.required],
    cuuIpa: [null, Validators.required],
    piva: [null, Validators.required],
    fiscalCode: [null, Validators.required],
    address: [null, Validators.required],
    city: [null, Validators.required],
	province:[null, Validators.required],
    cap: [null, Validators.required],

	//Invoice data
    fiscalNature: [null, Validators.required],
    payCode: [null, Validators.required],
    payType: [null, Validators.required],
    bank: [null, Validators.required],
    iban: [null, Validators.required],
    invoiceExpirationDays: [null, [Validators.required, Validators.min(1)]],
    measureUnit: [null, Validators.required],
    administrationReference: [null, Validators.required],
    cig: [null],
    causal: [null, Validators.required],
	
    groupingByCityRSA: [],
    billTaxRSA: [],
    isPerformanceTypeDescriptionRSA: [],
 	groupByDistrictRSA: [],
	isProvidedTreatmentsSummaryRSA: [],
	registryIn: [],
	areaCode: []
  });

  selectedAreaCities: City[] = [];
  selectedAreaHeadCities: City[] = [];
  selectedArea: Area | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
	private cityService: CityService,
	private userSecurityService: UserService
  ) {
    // Calling super to init the extended class
    super();
	
	setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
	
    if (data.editFormData) {
		this.updateForm(data.editFormData);
		console.log(data.editFormData);
	}
	
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (!user?.functionalities?.find(functionality => functionality.functionality === Functionalities.AREA_REGISTRY)) {
      this.form.disable();
    }
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * Update the area form data
   * @param data Data to update the form.
   */
  updateForm(data: Area): void {
	
	this.selectedArea = data;
    if (data.cap)
    	this.form.get('cap')?.disable();
	if (data.businessName)
		this.form.get('businessName')?.disable();
	if (data.address)
		this.form.get('address')?.disable();
	if (data.city) {
		this.selectedAreaCities.push(data.city);
		this.form.get('city')?.disable();
		this.form.get('province')?.disable();
	} else if (data.id){
		this.cityService.getByArea(data.id).subscribe(res => {
			this.selectedAreaCities = res;
		});
	}
	
	if (data.headCity) {
		this.selectedAreaHeadCities.push(data.headCity);
		this.form.get('headCity')?.disable();
	} else if (data.id){
		this.cityService.getByArea(data.id).subscribe(res => {
			this.selectedAreaHeadCities = res;
		});
	}

    this.form?.patchValue({
      ...data,
	  province: data.city?.province?.code,
	  measureUnit: data.measureUnit ? data.measureUnit : 'QT\u00C0'
    });
  }

  keyPressAlphanumeric($event: KeyboardEvent) {
    const inp = String.fromCharCode($event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      $event.preventDefault();
      return false;
    }
  }

  onChangeCity() {
	var city = this.form.get('city')?.value;
	if (city) {
		this.form.get('province')?.setValue(city?.province?.code);
	}
  }

}


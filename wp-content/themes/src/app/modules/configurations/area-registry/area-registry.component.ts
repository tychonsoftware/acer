import {Component, OnInit} from '@angular/core';
import {AreaService} from '@app/core/http/area/area.service';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {MatDialogConfig} from '@angular/material/dialog';
import {Area} from '@app/core/models/shared/area.model';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AreaRegistryFormComponent} from '@app/modules/configurations/area-registry/area-registry-form/area-registry-form.component';
import {Observable} from 'rxjs';
import { RegistryBaseAreaFormComponent } from './registry-base-area-form/registry-base-area-form.component';

@Component({
  selector: 'app-area-registry',
  templateUrl: './area-registry.component.html',
  styleUrls: ['./area-registry.component.scss']
})
export class AreaRegistryComponent implements OnInit {
  // TODO: generare mediante translate service
  public areasTableColumns: TableColumn[] = [
    {
      name: 'Codice AMBITO',
      dataKey: 'areaCode',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Ragione Sociale',
      dataKey: 'businessName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Partita IVA',
      dataKey: 'piva',
      position: 'center',
      isSortable: true,
    },
    {
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true
    },
    {
      name: ' ',
      dataKey: 'expand',
      position: 'center',
      isSortable: false
    }
  ];

  areas$: Area[] | undefined;
  areaSelected: any;

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    public areaService: AreaService) {
  }

  ngOnInit(): void {
    this._getNonObsoleteRegisteredArea();
  }

  onSort(s: any): void {
    console.log(s);
  }

  /**
   * Open the Area form dialog.
   * @param area Data of the Area to edit.
   */
  openFormDialog(area?: Area): void {
    if (area && area.id) {
      // Getting the contract data from the API
      this._openAreaRegistryFormDialog(area);
    } else {
      this._openAreaRegistryFormDialog();
    }
  }

  /**
   * Open the Registry base Area form dialog.
   * @param area Data of the Area to edit.
   */
  openBaseAreaFormDialog(area?: Area): void {
    if (area && area.id) {
      // Getting the contract data from the API
      this._openRegistryBaseAreaFormDialog(area);
    } else {
      this._openRegistryBaseAreaFormDialog();
    }
  }

  /**
   * Open the delete Area dialog.
   * @param area The Area to delete.
   */
  onDeleteClick(area: Area): void {
    const data = {
      title: this.translateService.instant('areaRegistry.deleteDialog.title'),
      message: this.translateService.instant('areaRegistry.deleteDialog.message', {value: area.areaCode}),
      cancelText: this.translateService.instant('areaRegistry.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('areaRegistry.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteArea(area));
  }

  private _openAreaRegistryFormDialog(editedArea?: Area): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedArea,
      cancelText: this.translateService.instant('areaRegistry.formDialog.cancelText'),
      confirmText: this.translateService.instant('areaRegistry.formDialog.confirmText'),
    };

    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view-btns';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AreaRegistryFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => this._saveArea(ap as Area));
  }

  private _openRegistryBaseAreaFormDialog(editedArea?: Area): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedArea,
    };
    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(RegistryBaseAreaFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => {
      if (ap) {
        this.areaSelected = ap;
        this._openAreaRegistryFormDialog(ap);
      }
    });
  }

  /**
   * Save the Area.
   * @param area The Area to save
   * @private
   */
  private _saveArea(area?: Area): void {
    if (area) {
	  delete (area as any)?.dateInsert;
      let result: Observable<Area>;
      if (area.id) { // edit case
        result = this.areaService.update(area, true);
		result.subscribe(_ => this._getNonObsoleteRegisteredArea());
      } 
    }
  }

  /**
   * Delete Area
   * @param area The Area to delete.
   * @private
   */
  private _deleteArea(area: Area): void {
	let result: Observable<Area>;
    if (area.id) {
      result = this.areaService.update(area, false);
	  result.subscribe(_ => {
		this._getNonObsoleteRegisteredArea()} );
    }
  }

  private _getNonObsoleteRegisteredArea(): void {
    this.areaService.getByRegistryIn(true).subscribe(res => {
		console.log('GET by RegistriIn true');
        console.log(res);
		this.sortAreaByAreaCode(res);
		this.areas$ = res;
	});

  }

  sortAreaByAreaCode(data: Area[]): void {
    // Effettuo ordinamento per codice area
    data.sort((a, b) => {
      const areaCode1 = a.areaCode;
      const areaCode2 = b.areaCode;
      return areaCode1 > areaCode2 ? 1 : -1;
    });
  }

}

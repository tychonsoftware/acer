import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AreaRegistryComponent} from './area-registry.component';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DATE_FORMATS} from '@angular/material/core';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {AreaRegistryRoutingModule} from '@app/modules/configurations/area-registry/area-registry-routing.module';
import {AreaRegistryFormComponent} from './area-registry-form/area-registry-form.component';
import {AreaRegistryTableComponent} from './area-registry-table/area-registry-table.component';
import { AreaRegistryDetailsComponent } from './area-registry-table/area-registry-details/area-registry-details.component';
import {DD_MM_YYYY_DATE_FORMAT} from '@app/shared/helpers/format-datepicker';
import { RegistryBaseAreaFormComponent } from './registry-base-area-form/registry-base-area-form.component';
import { RegistryBaseAreaTableComponent } from './registry-base-area-table/registry-base-area-table.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
  ]);
}

@NgModule({
  declarations: [
    AreaRegistryComponent,
    AreaRegistryFormComponent,
    AreaRegistryTableComponent,
    AreaRegistryDetailsComponent,
    RegistryBaseAreaFormComponent,
    RegistryBaseAreaTableComponent
  ],
  imports: [
    AreaRegistryRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
  ],
  providers: [
    // Per il datapicker
  {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_DATE_FORMAT},
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AreaRegistryModule {
}

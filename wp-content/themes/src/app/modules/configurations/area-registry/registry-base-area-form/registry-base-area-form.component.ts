import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AreaService} from '@app/core/http/area/area.service';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {Area} from '@app/core/models/shared/area.model';

@Component({
  selector: 'app-registry-base-area-form',
  templateUrl: './registry-base-area-form.component.html',
  styleUrls: ['./registry-base-area-form.component.scss']
})
export class RegistryBaseAreaFormComponent extends FormContentDirective implements OnInit, OnChanges {

  public areasTableColumns: TableColumn[] = [
    {
      name: 'Codice Ambito',
      dataKey: 'areaCode',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Ragione Sociale',
      dataKey: 'businessName',
      position: 'center',
      isSortable: true
    }
  ];

  areas$: Area[] | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    private areaService: AreaService,
    public dialogRef: MatDialogRef<RegistryBaseAreaFormComponent>
  ) {
    // Calling super to init the extended class
    super();
  }

  ngOnInit(): void {
    this.areaService.getByRegistryIn(false).subscribe(res => {
		this.areas$ = res;
	})
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
  }

  onRowSelected(area: Area): void {
    this.dialogRef.close(area);
  }
}


import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Area} from '@app/core/models/shared/area.model';
import {RowActionIcons} from '@app/shared/components/generic-table/generic-table.component';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';

@Component({
  selector: 'app-area-registry-details',
  templateUrl: './area-registry-details.component.html',
  styleUrls: ['./area-registry-details.component.scss']
})
export class AreaRegistryDetailsComponent implements OnInit {

  // Input
  @Input() area: Area | undefined;
  @Input() rowActionIcons: RowActionIcons | undefined;

  // Output
  @Output() ngEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngDelete: EventEmitter<any> = new EventEmitter<any>();

  groupingByCityRSA: boolean | undefined;
  billTaxRSA: boolean | undefined;
  isPerformanceTypeDescriptionRSA: boolean | undefined;
  groupByDistrictRSA: boolean | undefined;
  isProvidedTreatmentsSummaryRSA: boolean | undefined;

  isEditable = false;

  constructor(userSecurityService: UserService) {
    setTimeout(() => {
      const user = userSecurityService.getUserSecurity();
      if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.AREA_REGISTRY)) {
        this.isEditable = true;
      }
    }, environment.TIME_TO_TIMEOUT);
  }

  ngOnInit(): void {
	this.groupingByCityRSA = this.area?.groupingByCityRSA;
	this.billTaxRSA = this.area?.billTaxRSA;
	this.isPerformanceTypeDescriptionRSA = this.area?.isPerformanceTypeDescriptionRSA;
	this.groupByDistrictRSA = this.area?.groupByDistrictRSA;
	this.isProvidedTreatmentsSummaryRSA = this.area?.isProvidedTreatmentsSummaryRSA;
  }

  emitEditAction(row: any): void {
    this.ngEdit.emit(row);
  }

  emitDeleteAction(row: any): void {
    this.ngDelete.emit(row);
  }

}

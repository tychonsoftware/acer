import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SanitaryFlowsFormComponent } from './sanitary-flows-form.component';

describe('SanitaryFlowsFormComponent', () => {
  let component: SanitaryFlowsFormComponent;
  let fixture: ComponentFixture<SanitaryFlowsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SanitaryFlowsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SanitaryFlowsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

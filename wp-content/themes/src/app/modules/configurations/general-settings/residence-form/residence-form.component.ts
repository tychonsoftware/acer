import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Constants} from '@app/core/constants/Constants';
import {Observable} from 'rxjs';
import {City, Nation, Province, Residence, TaxRegime} from '@app/core/models';
import {TranslateService} from '@ngx-translate/core';
import {CityService, NationService, ProvinceService, TaxRegimeService} from '@app/core/http';
import {ResidenceService} from '@app/core/http/residence/residence.service';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-residence-form',
  templateUrl: './residence-form.component.html',
  styleUrls: ['./residence-form.component.scss']
})
export class ResidenceFormComponent implements OnInit {

  // Form
  form = this.fb.group({
    id: [],
    uuid: [],
    residenceName: ['', Validators.required],
    piva: ['', [
      Validators.required,
      Validators.minLength(11),
      Validators.maxLength(11)]
    ],
    taxRegim: ['', Validators.required],
    email: ['', [
      Validators.required, Validators.minLength(5),
      Validators.maxLength(254),
      Validators.pattern(Constants.EMAIL_REGEX)
    ]],
    phoneNumber: ['', [Validators.pattern(Constants.PHONE_REGEX)]],
    faxNumber: ['', [Validators.pattern(Constants.PHONE_REGEX)]],
    web: [''],
    billingAddress: this.fb.group({
      id: [],
      uuid: [],
      address: [''],
      cap: [],
      nation: [null],
      city: [null]
    }),
    province: [null],
    automaticGenerationSdi: [],
    automaticMonthlyGenerationSdi: [],
    predefinedServiceDrug: [],
  });

  // Selects options
  predefinedServiceDrugs$: Observable<any[]> | undefined; // TODO: implement this when provided by Marco
  taxRegimes$: Observable<TaxRegime[]> | undefined;
  provinces$: Observable<Province[]> | undefined;
  nations$: Observable<Nation[]> | undefined;
  cities$: Observable<City[]> | undefined;
  formSubmitAttempt = false;

  residence$: Observable<Residence> | undefined;

  constructor(
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private taxRegimeService: TaxRegimeService,
    private provinceService: ProvinceService,
    private nationService: NationService,
    private cityService: CityService,
    private residenceService: ResidenceService
  ) {
  }

  ngOnInit(): void {
    // Getting the billing property asynchronously from API
    this.taxRegimes$ = this.taxRegimeService.getAll();
    this.provinces$ = this.provinceService.getAll();
    this.nations$ = this.nationService.getAll();
    this.cities$ = this.cityService.getAll();
    // Updating form with provided data if any
    this.residence$ = this.residenceService.residence$.pipe(
      map(residence => residence === null ? {} as Residence : residence),
      tap(residence => this.form.patchValue(residence))
    );
    this.residenceService.get();
  }

  /**
   * Compare two object on their ID.
   * @param c1 First component.
   * @param c2 Second component.
   */
  compare(c1: { id: number }, c2: { id: number }): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

  /**
   * On form submit
   */
  onSubmit(): void {
    this.formSubmitAttempt = true;
    if (this.form.valid) {
      const residence: Residence = this.form.value;
      this._saveResidence(residence);
    }
  }

  /**
   * On form reset
   */
  onReset(): void {
    this.formSubmitAttempt = false;
    this.form.reset();
  }

  /**
   * Save the Residence data.
   * @param residence Data to save.
   * @private
   */
  private _saveResidence(residence?: Residence): void {
    if (residence) {
      console.log('RESIDENCE TO SAVE');
      console.log(residence);

      if (residence.uuid) { // edit case
        this.residenceService.update(residence);
      } else {
        this.residenceService.create(residence);
      }
    }
  }
}

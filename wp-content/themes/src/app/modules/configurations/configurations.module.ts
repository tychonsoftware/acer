import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

import {ConfigurationsComponent} from './configurations.component';
import {ContractsComponent} from './contracts/contracts.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ConfigurationsRoutingModule} from '@app/modules/configurations/configurations-routing';
import {SharedModule} from '@app/shared/shared.module';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {ContractFormComponent} from './contracts/contract-form/contract-form.component';
import {GeneralSettingsComponent} from './general-settings/general-settings.component';
import {ResidenceFormComponent} from './general-settings/residence-form/residence-form.component';
import {SanitaryFlowsFormComponent} from './general-settings/sanitary-flows-form/sanitary-flows-form.component';
import {AslRegistryComponent} from './asl-registry/asl-registry.component';
import {RegistryBaseAslTableComponent} from './asl-registry/registry-base-asl-table/registry-base-asl-table.component';
import {RegistryBaseAslFormComponent} from './asl-registry/registry-base-asl-form/registry-base-asl-form.component';
import {AslRegistryTableComponent} from './asl-registry/asl-registry-table/asl-registry-table.component';
import {AslRegistryDetailsComponent} from './asl-registry/asl-registry-table/asl-registry-details/asl-registry-details.component';
import {AslRegistryFormComponent} from './asl-registry/asl-registry-form/asl-registry-form.component';
import {AreaRegistryComponent} from './area-registry/area-registry.component';
import {RegistryBaseAreaTableComponent} from './area-registry/registry-base-area-table/registry-base-area-table.component';
import {RegistryBaseAreaFormComponent} from './area-registry/registry-base-area-form/registry-base-area-form.component';
import {AreaRegistryTableComponent} from './area-registry/area-registry-table/area-registry-table.component';
import {AreaRegistryDetailsComponent} from './area-registry/area-registry-table/area-registry-details/area-registry-details.component';
import {AreaRegistryFormComponent} from './area-registry/area-registry-form/area-registry-form.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/configurations/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    ConfigurationsComponent,
    ContractsComponent,
    ContractFormComponent,
    GeneralSettingsComponent,
    ResidenceFormComponent,
    SanitaryFlowsFormComponent,
	AslRegistryComponent,
	RegistryBaseAslTableComponent,
	RegistryBaseAslFormComponent,
	AslRegistryTableComponent,
	AslRegistryDetailsComponent,
	AslRegistryFormComponent,
	AreaRegistryComponent,
	RegistryBaseAreaTableComponent,
	RegistryBaseAreaFormComponent,
	AreaRegistryTableComponent,
	AreaRegistryDetailsComponent,
	AreaRegistryFormComponent
  ],
  imports: [
    ConfigurationsRoutingModule,
    SharedModule,
    ReactiveFormsModule,

    // Per la traduzione eventuale in altre lingue
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    })
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConfigurationsModule {
}

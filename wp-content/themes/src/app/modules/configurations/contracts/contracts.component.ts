import {Component, OnInit} from '@angular/core';
import {Contract} from '@app/core/models/configurations/contract.model';
import {ContractService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {DialogService} from '@app/core/services/dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {ContractFormComponent} from '@app/modules/configurations/contracts/contract-form/contract-form.component';
import {MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {

  // TODO: generare mediante translate service
  public contractsTableColumns: TableColumn[] = [
    {
      name: 'Descrizione',
      dataKey: 'description',
      position: 'center',
      isSortable: false
    },
    {
      name: 'Importo Giornalierlo',
      dataKey: 'dailyPrice',
      position: 'center',
      isSortable: true,
      isCurrency: true,
      isBold: true
    },
    {
      name: 'Importo Mensile',
      dataKey: 'monthlyPrice',
      position: 'center',
      isSortable: true,
      isCurrency: true,
      isBold: true
    },
    {
      name: 'Stanza Privata',
      dataKey: 'privateRoom',
      position: 'center',
      isSortable: true,
      isBold: true
    },
    {
      name: 'Natura Economica',
      dataKey: 'natureInvoiceType.description',
      position: 'center',
      isSortable: true,
      isBold: true
    },
    {
      name: 'IVA (%)',
      dataKey: 'iva',
      position: 'center',
      isSortable: true,
      isBold: true,
      isPercentage: true
    },
  ];

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true,
    width: '55%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    public contractService: ContractService
  ) {
  }

  ngOnInit(): void {
    this.contractService.getAll();
  }

  /**
   * Open the Contract form dialog.
   * @param contract Data of the Contract to edit.
   */
  openFormDialog(contract?: Contract): void {
    if (contract && contract.id) {
      // Getting the contract data from the API
      this._openContractFormDialog(contract);
    } else {
      this._openContractFormDialog();
    }
  }

  /**
   * Open the delete Contract dialog.
   * @param contract The Contract to delete.
   */
  onDeleteClick(contract: Contract): void {
    const data = {
      title: this.translateService.instant('contract.deleteDialog.title'),
      message: this.translateService.instant('contract.deleteDialog.message', {value: contract.description}),
      cancelText: this.translateService.instant('contract.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('contract.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteContract(contract));
  }

  /**
   * Open the Contract create/edit dialog.
   * @param editedContract The Contract data only for the edit case.
   * @private
   */
  private _openContractFormDialog(editedContract?: Contract): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedContract,
      title: this.translateService.instant(`contract.formDialog.${editedContract?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('contract.formDialog.cancelText'),
      confirmText: this.translateService.instant('contract.formDialog.confirmText')
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(ContractFormComponent, data, this.dialogConfig);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(c => this._saveContract(c as Contract));
  }

  /**
   * Save the Contract.
   * @param contract The Contract to save
   * @private
   */
  private _saveContract(contract?: Contract): void {
    if (contract) {
      if (contract.id) { // edit case
        this.contractService.update(contract);
      } else {
        this.contractService.create(contract);
      }
    }
  }

  /**
   * Delete Contract
   * @param contract The Contract to delete.
   */
  private _deleteContract(contract: Contract): void {
    if (contract.uuid) {
      this.contractService.deleteByUUID(contract.uuid);
    }
  }
}

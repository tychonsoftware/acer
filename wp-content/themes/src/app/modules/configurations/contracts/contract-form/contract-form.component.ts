import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {FormBuilder, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {NatureInvoiceType} from '@app/core/models';
import {ContractService, NatureInvoiceTypeService} from '@app/core/http';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-contract-form',
  templateUrl: './contract-form.component.html',
  styleUrls: ['./contract-form.component.scss']
})
export class ContractFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    description: ['', Validators.required],
    dailyPrice: ['', Validators.required],
    monthlyPrice: ['', Validators.required],
    iva: ['', Validators.required],
    natureInvoiceType: [null, Validators.required],
    privateRoom: [false]
  });

  // Selects options
  natureInvoiceTypes$: Observable<NatureInvoiceType[]> | undefined;

  constructor(// Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private contractService: ContractService,
    private natureInvoiceTypeService: NatureInvoiceTypeService) {
    // Calling super to init the extended class
    super();

    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      this.isEdit = true;
      this.updateForm(data.editFormData);
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
    this.natureInvoiceTypes$ = this.natureInvoiceTypeService.getAll();
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }
}

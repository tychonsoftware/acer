import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Asl} from '@app/core/models';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';

@Component({
  selector: 'app-registry-base-asl-table',
  templateUrl: './registry-base-asl-table.component.html',
  styleUrls: ['./registry-base-asl-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class RegistryBaseAslTableComponent extends GenericTableComponent<Asl> {

  @Input() set tableData(data: Observable<Asl[]> | Asl[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
      if (this.matPaginator) {
        this.tableDataSource.paginator = this.matPaginator;
      }
    }
  }

  @Output() ngRowSelected: EventEmitter<Asl> = new EventEmitter();

  constructor() {
    super();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: Asl[]): void {
	this.sortAslByAslCode(data);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }
  
  sortAslByAslCode(data: Asl[]): void {
    // Effettuo ordinamento per codice asl
    data.sort((a, b) => {
      const aslCode1 = (parseInt) (a.aslCode);
      const aslCode2 = (parseInt) (b.aslCode);
      return aslCode1 > aslCode2 ? 1 : -1;
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }

  rowSelected(element: Asl): void {
    this.ngRowSelected.emit(element);
  }
}

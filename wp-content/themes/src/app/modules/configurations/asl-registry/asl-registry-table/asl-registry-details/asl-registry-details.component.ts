import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Asl} from '@app/core/models';
import {RowActionIcons} from '@app/shared/components/generic-table/generic-table.component';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';

@Component({
  selector: 'app-asl-registry-details',
  templateUrl: './asl-registry-details.component.html',
  styleUrls: ['./asl-registry-details.component.scss']
})
export class AslRegistryDetailsComponent implements OnInit {

  // Input
  @Input() asl: Asl | undefined;
  @Input() rowActionIcons: RowActionIcons | undefined;

  // Output
  @Output() ngEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngDelete: EventEmitter<any> = new EventEmitter<any>();

  reducedBillInvoiceRSA: boolean | undefined;
  groupByDistrictRSA: boolean | undefined;
  billTaxRSA: boolean | undefined;
  isPerformanceTypeDescriptionRSA: boolean | undefined;
  isProvidedTreatmentsSummaryRSA: boolean | undefined;
  
  isEditable = false;

  constructor(userSecurityService: UserService) {
    setTimeout(() => {
      const user = userSecurityService.getUserSecurity();
      if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASL_REGISTRY)) {
        this.isEditable = true;
      }
    }, environment.TIME_TO_TIMEOUT);
  }

  ngOnInit(): void {
	this.reducedBillInvoiceRSA = this.asl?.reducedBillInvoiceRSA;
	this.groupByDistrictRSA = this.asl?.groupByDistrictRSA;
	this.billTaxRSA = this.asl?.billTaxRSA;
	this.isPerformanceTypeDescriptionRSA = this.asl?.isPerformanceTypeDescriptionRSA;
	this.isProvidedTreatmentsSummaryRSA = this.asl?.isProvidedTreatmentsSummaryRSA;
  }

  emitEditAction(row: any): void {
    this.ngEdit.emit(row);
  }

  emitDeleteAction(row: any): void {
    this.ngDelete.emit(row);
  }

}

import {Component, Input} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {Asl} from '@app/core/models/shared/asl.model';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';

@Component({
  selector: 'app-asl-registry-table',
  templateUrl: './asl-registry-table.component.html',
  styleUrls: ['./asl-registry-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AslRegistryTableComponent extends GenericTableComponent<Asl> {

  @Input() set tableData(data: Observable<Asl[]> | Asl[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  expandedElement?: Asl;
  isEditable = false;

  constructor(userSecurityService: UserService) {
    super();
    setTimeout(() => this.checkSecurity(userSecurityService), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(userSecurityService: UserService): void {
    const user = userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASL_REGISTRY)) {
      this.isEditable = true;
    }
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: Asl[]): void {
    this.sortAslByAslCode(data);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }

  // Ordina gli elementi nella tabella, facendo un ordinamento per codice asl
  sortAslByAslCode(data: Asl[]): void {

    // Effettuo ordinamento per cognome e nome
    data.sort((a, b) => {
      const aslCode1 = (parseInt) (a.aslCode);
      const aslCode2 = (parseInt) (b.aslCode);
      return aslCode1 > aslCode2 ? 1 : -1;
    });
  }

  // Serve per comparare le stringhe, nella select
  compare(c1: { id: number }, c2: { id: number }): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

}

import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AslRegistryComponent} from './asl-registry.component';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DATE_FORMATS} from '@angular/material/core';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {AslRegistryRoutingModule} from '@app/modules/configurations/asl-registry/asl-registry-routing.module';
import {AslRegistryFormComponent} from './asl-registry-form/asl-registry-form.component';
import {AslRegistryTableComponent} from './asl-registry-table/asl-registry-table.component';
import { AslRegistryDetailsComponent } from './asl-registry-table/asl-registry-details/asl-registry-details.component';
import {DD_MM_YYYY_DATE_FORMAT} from '@app/shared/helpers/format-datepicker';
import { RegistryBaseAslFormComponent } from './registry-base-asl-form/registry-base-asl-form.component';
import { RegistryBaseAslTableComponent } from './registry-base-asl-table/registry-base-asl-table.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
  ]);
}

@NgModule({
  declarations: [
    AslRegistryComponent,
    AslRegistryFormComponent,
    AslRegistryTableComponent,
    AslRegistryDetailsComponent,
    RegistryBaseAslFormComponent,
    RegistryBaseAslTableComponent
  ],
  imports: [
    AslRegistryRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
  ],
  providers: [
    // Per il datapicker
  {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_DATE_FORMAT},
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AslRegistryModule {
}

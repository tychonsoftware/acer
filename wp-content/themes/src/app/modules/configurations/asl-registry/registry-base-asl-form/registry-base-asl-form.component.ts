import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';

import {AslService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {Asl} from '@app/core/models';

@Component({
  selector: 'app-registry-base-asl-form',
  templateUrl: './registry-base-asl-form.component.html',
  styleUrls: ['./registry-base-asl-form.component.scss']
})
export class RegistryBaseAslFormComponent extends FormContentDirective implements OnInit, OnChanges {

  public aslsTableColumns: TableColumn[] = [
    {
      name: 'Codice ASL',
      dataKey: 'aslCode',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Ragione Sociale',
      dataKey: 'description',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Partita IVA',
      dataKey: 'piva',
      position: 'center',
      isSortable: true,
    },
    {
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true
    }
  ];

  asls$: Asl[] | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    private aslService: AslService,
    public dialogRef: MatDialogRef<RegistryBaseAslFormComponent>
  ) {
    // Calling super to init the extended class
    super();
  }

  ngOnInit(): void {
    this.aslService.getByRegistryIn(false).subscribe(res => {
		this.asls$ = res;
	})
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
  }

  onRowSelected(asl: Asl): void {
    this.dialogRef.close(asl);
  }
}


import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AslRegistryComponent} from '@app/modules/configurations/asl-registry/asl-registry.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AslRegistryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AslRegistryRoutingModule {
}

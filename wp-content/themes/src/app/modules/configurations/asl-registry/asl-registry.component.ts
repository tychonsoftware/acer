import {Component, OnInit} from '@angular/core';
import {AslService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {MatDialogConfig} from '@angular/material/dialog';
import {Asl} from '@app/core/models/shared/asl.model';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AslRegistryFormComponent} from '@app/modules/configurations/asl-registry/asl-registry-form/asl-registry-form.component';
import {Observable} from 'rxjs';
import { RegistryBaseAslFormComponent } from './registry-base-asl-form/registry-base-asl-form.component';

@Component({
  selector: 'app-asl-registry',
  templateUrl: './asl-registry.component.html',
  styleUrls: ['./asl-registry.component.scss']
})
export class AslRegistryComponent implements OnInit {
  // TODO: generare mediante translate service
  public aslsTableColumns: TableColumn[] = [
    {
      name: 'Codice ASL',
      dataKey: 'aslCode',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Ragione Sociale',
      dataKey: 'description',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Partita IVA',
      dataKey: 'piva',
      position: 'center',
      isSortable: true,
    },
    {
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true
    },
    {
      name: ' ',
      dataKey: 'expand',
      position: 'center',
      isSortable: false
    }
  ];

  asls$: Asl[] | undefined;
  aslSelected: any;

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    public aslService: AslService) {
  }

  ngOnInit(): void {
    this._getNonObsoleteRegisteredAsl();
  }

  onSort(s: any): void {
    console.log(s);
  }

  /**
   * Open the Asl form dialog.
   * @param asl Data of the Asl to edit.
   */
  openFormDialog(asl?: Asl): void {
    if (asl && asl.id) {
      // Getting the contract data from the API
      this._openAslRegistryFormDialog(asl);
    } else {
      this._openAslRegistryFormDialog();
    }
  }

  /**
   * Open the Registry base Asl form dialog.
   * @param asl Data of the Asl to edit.
   */
  openBaseAslFormDialog(asl?: Asl): void {
    if (asl && asl.id) {
      // Getting the contract data from the API
      this._openRegistryBaseAslFormDialog(asl);
    } else {
      this._openRegistryBaseAslFormDialog();
    }
  }

  /**
   * Open the delete Asl dialog.
   * @param asl The Asl to delete.
   */
  onDeleteClick(asl: Asl): void {
    const data = {
      title: this.translateService.instant('aslRegistry.deleteDialog.title'),
      message: this.translateService.instant('aslRegistry.deleteDialog.message', {value: asl.aslCode}),
      cancelText: this.translateService.instant('aslRegistry.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('aslRegistry.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteAsl(asl));
  }

  private _openAslRegistryFormDialog(editedAsl?: Asl): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedAsl,
      cancelText: this.translateService.instant('aslRegistry.formDialog.cancelText'),
      confirmText: this.translateService.instant('aslRegistry.formDialog.confirmText'),
    };

    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view-btns';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AslRegistryFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => this._saveAsl(ap as Asl));
  }

  private _openRegistryBaseAslFormDialog(editedAsl?: Asl): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedAsl,
    };
    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(RegistryBaseAslFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => {
      if (ap) {
        this.aslSelected = ap;
        this._openAslRegistryFormDialog(ap);
      }
    });
  }

  /**
   * Save the Asl.
   * @param asl The Asl to save
   * @private
   */
  private _saveAsl(asl?: Asl): void {
    if (asl) {
	  delete (asl as any)?.dateInsert;
      let result: Observable<Asl>;
      if (asl.id) { // edit case
        result = this.aslService.update(asl, true);
		result.subscribe(_ => this._getNonObsoleteRegisteredAsl());
      } 
    }
  }

  /**
   * Delete Asl
   * @param asl The Asl to delete.
   * @private
   */
  private _deleteAsl(asl: Asl): void {
	let result: Observable<Asl>;
    if (asl.id) {
      result = this.aslService.update(asl, false);
	  result.subscribe(_ => {
		this._getNonObsoleteRegisteredAsl()} );
    }
  }

  private _getNonObsoleteRegisteredAsl(): void {
    this.aslService.getByRegistryIn(true).subscribe(res => {
		console.log('GET by RegistriIn true');
        console.log(res);
		this.sortAslByAslCode(res);
		this.asls$ = res;
	});

  }

  sortAslByAslCode(data: Asl[]): void {
    // Effettuo ordinamento per codice asl
    data.sort((a, b) => {
      const aslCode1 = (parseInt) (a.aslCode);
      const aslCode2 = (parseInt) (b.aslCode);
      return aslCode1 > aslCode2 ? 1 : -1;
    });
  }

}

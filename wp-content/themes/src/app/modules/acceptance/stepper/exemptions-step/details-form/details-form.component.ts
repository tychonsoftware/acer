import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-details-form',
  templateUrl: './details-form.component.html',
  styleUrls: ['./details-form.component.scss']
})
export class DetailsFormComponent extends FormContentDirective implements OnInit, OnChanges {

  disabledExpireDate = false;

  form = this.fb.group({
    id: [],
    uuid: [],
    tag: ['', Validators.required],
    expirationDate: []
  });

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder
  ) {
    // Calling super to init the extended class
    super();

    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      this.isEdit = true;
      this.updateForm(data.editFormData);
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * Quando si clicca la checkbox disabilito il required dal campo data di scadenza e lo disabilito
   */
  checkBoxChangeValue(): void {
    this.disabledExpireDate = !this.disabledExpireDate;
    if (this.disabledExpireDate) {
      this.form.get('expirationDate')?.setValue(null);
    }
    this.form.get('expirationDate')?.setValidators((this.disabledExpireDate ? null : Validators.required));
  }
}

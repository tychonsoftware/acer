import {Component, OnInit} from '@angular/core';
import {MatDialogConfig} from '@angular/material/dialog';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {Exemption, TherapeuticPlan} from '@app/core/models';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {DetailsFormComponent} from '@app/modules/acceptance/stepper/exemptions-step/details-form/details-form.component';
import {Functionalities} from "@app/core/constants/Functionalities";

enum DataType {
  EXEMPTIONS,
  THERAPEUTIC_PLAN
}

@Component({
  selector: 'app-exemptions-step',
  templateUrl: './exemptions-step.component.html',
  styleUrls: ['./exemptions-step.component.scss']
})
export class ExemptionsStepComponent implements OnInit {
  // TODO: generare mediante translate service
  // Table Columns
  public tableColumns: TableColumn[] = [
    {
      name: 'Etichetta',
      dataKey: 'tag',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Scadenza',
      dataKey: 'expirationDate',
      position: 'center',
      isSortable: true,
      isDate: true
    },
    {
      name: 'Data Inserimento',
      dataKey: 'dateInsert',
      position: 'center',
      isSortable: true,
      isDate: true
    }
  ];

  // Data
  exemptionsData: Exemption[] = [];
  therapeuticPlanData: TherapeuticPlan[] = [];
  disabled = false;

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true,
    width: '30%'
  };

  public DataTypeEnum = DataType;

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
  ) {
  }

  ngOnInit(): void {
  }

  getExemptionsData(): Exemption[] {
    return this.exemptionsData;
  }

  getTherapeuticPlanDataData(): TherapeuticPlan[] {
    return this.therapeuticPlanData;
  }

  setData(exemptions?: Exemption[], therapeuticPlans?: TherapeuticPlan[]): void {
    if (exemptions) {
      this.exemptionsData = exemptions;
    }

    if (therapeuticPlans) {
      this.therapeuticPlanData = therapeuticPlans;
    }
  }

  openFormDialog(dataType: DataType, element?: Exemption): void {
    if (element) {
      this._openFormDialog(dataType, element);
    } else {
      this._openFormDialog(dataType);
    }
  }

  onDeleteClick(dataType: DataType, elementToRemove: Exemption | TherapeuticPlan): void {
    let tableData: (Exemption | TherapeuticPlan)[];

    if (dataType === DataType.EXEMPTIONS) {
      tableData = this.exemptionsData;
    } else {
      tableData = this.therapeuticPlanData;
    }

    const data = {
      title: this.translateService.instant('deleteDialog.title'),
      message: this.translateService.instant('deleteDialog.message', {value: `${elementToRemove.tag}`}),
      cancelText: this.translateService.instant('deleteDialog.cancelText'),
      confirmText: this.translateService.instant('deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => {
      if (confirmed) {
        // Se true prendo il valore selezionato lo rimuovo e riassegno il nuovo vettore di valori da visualizzare
        // Modifico l'elemento

        tableData.splice(tableData.indexOf(elementToRemove), 1);
        tableData = [...tableData];
        this._updateData(dataType, tableData);
      }
    });
  }

  private _openFormDialog(dataType: DataType, editedElement?: Exemption | TherapeuticPlan): void {
    let tableData: (Exemption | TherapeuticPlan)[];
    let formDialogType = 'exemptions.';

    if (dataType === DataType.EXEMPTIONS) {
      tableData = this.exemptionsData;
      formDialogType += 'exemptionFormDialog';
    } else {
      tableData = this.therapeuticPlanData;
      formDialogType += 'therapeuticPlanFormDialog';
    }

    const data: Partial<IFormDialogConfig> = {
      editFormData: editedElement,
      title: this.translateService.instant(`${formDialogType}.${editedElement ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant(`${formDialogType}.cancelText`),
      confirmText: this.translateService.instant(`${formDialogType}.confirmText`),
      isExemptionTable: dataType === DataType.EXEMPTIONS
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(DetailsFormComponent, data, this.dialogConfig);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(newElement => {
      if (newElement) {

        console.log(newElement);

        if (editedElement) {
          // Modifico l'elemento
          tableData.splice(tableData.indexOf(editedElement), 1, newElement);
          tableData = [...tableData];
        } else {
          newElement.dateInsert = new Date();
          tableData = [...tableData, newElement];
        }

        this._updateData(dataType, tableData);
      }
    });
  }

  private _updateData(dataType: DataType, data: any[]): void {
    if (dataType === DataType.EXEMPTIONS) {
      this.exemptionsData = data;
    } else {
      this.therapeuticPlanData = data;
    }
  }

  public setDisabled(val: boolean): void {
    this.disabled = val;
  }
}

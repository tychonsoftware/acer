import {Component, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Dismiss} from '@app/core/models/guests/dismiss.model';
import {DismissReason} from '@app/core/models/guests/dismiss-reason.model';
import {DismissReasonService} from '@app/core/http/dismiss-reason/dismiss-reason.service';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';
import {DrugManagement, ArticleManagement} from '@app/core/models/guests/dismiss-drug-article-management.model';

@Component({
  selector: 'app-dismiss-step',
  templateUrl: './dismiss-step.component.html',
  styleUrls: ['./dismiss-step.component.scss']
})
export class DismissStepComponent extends FormContentDirective implements OnInit{
  
  // Form
  form = this.fb.group({
    id: [],
    date: [null, Validators.required],
    time: [null, Validators.required],
    reason: [null, Validators.required],
    note: [null],
    drugManagement: [null, Validators.required],
    articleManagement: [null, Validators.required],
    absentOnLastDay: [null],
	guestUuid: [null]
  });
  
  selectedDrug: string | undefined;
  selectedArticle: string | undefined;
  dismissReasons: DismissReason[] | undefined;
  drugManagements: DrugManagement[] | undefined;
  articleManagements: ArticleManagement[] | undefined;
  drugManageDelete = {id: 1, description: 'Elimina farmaci'};
  drugManageMove = {id: 2, description: 'Sposta farmaci nel magazzino generale'};
  articleManageDelete = {id: 1, description: 'Elimina ausili'};
  articleManageMove = {id: 2, description: 'Sposta ausili nel magazzino generale'};
  reasonDismiss: DismissReason | undefined;
  dismissedGuestUUID: string | undefined;
  dismissData: Dismiss | undefined;

  constructor(
    private fb: FormBuilder,
	private dismissReasonService: DismissReasonService,
    private userSecurityService: UserService
  ) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (!user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_MANAGEMENT)) {
      this.form.disable();
    }
  }

  ngOnInit(): void {
	if (this.form.get('reason')?.value?.id === 1)
		this.form.get('note')?.disable();
	this.dismissReasons = [];
	this.dismissReasonService.getAllAsObservable()?.subscribe(reasons => {
		reasons.forEach(reason => this.dismissReasons?.push(reason));
	});
	this.drugManagements = [this.drugManageDelete, this.drugManageMove];
	this.articleManagements = [this.articleManageDelete, this.articleManageMove];
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  updateForm(data?: Dismiss): void {
    this.form?.patchValue({
      ...data,
      // Dates
      date: data?.date != null ? new Date(data.date) : null,
    });
  }

  reasonFormControl(): FormControl {
    return this.form.get('reason') as FormControl;
  }
  
  drugManagementFormControl(): FormControl {
    return this.form.get('drugManagement') as FormControl;
  }

  articleManagementFormControl(): FormControl {
    return this.form.get('articleManagement') as FormControl;
  }
  
  onReasonSelection(): void {
	let selectedDismissReason = this.form.get('reason')?.value;
    if (selectedDismissReason.id === 2 || selectedDismissReason.id === 3) {
		this.form.get('note')?.enable();
	} else {
		this.form.get('note')?.setValue(null);
		this.form.get('note')?.disable();
	}
  }

  getDismissData(): Dismiss | undefined {
    return {
      ...this.form.value,
	  drugManagement: this.form?.get('drugManagement')?.value?.description,
	  articleManagement: this.form?.get('articleManagement')?.value?.description
    };
  }

  setDismissData(data?: Dismiss): void {
    if (data) {
      this.form?.setValue(data);
	  this.form.get('date')?.setValue(new Date(data.date));
	  const drugManage = data.drugManagement.includes('Elimina') ? this.drugManageDelete : this.drugManageMove;
	  this.form.get('drugManagement')?.setValue(drugManage);
	  const articleManage = data.articleManagement.includes('Elimina') ? this.articleManageDelete : this.articleManageMove;
	  this.form.get('articleManagement')?.setValue(articleManage);
    }
  }

}

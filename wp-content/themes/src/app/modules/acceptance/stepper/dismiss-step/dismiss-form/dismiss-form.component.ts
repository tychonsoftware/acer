import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, Validators, FormControl} from '@angular/forms';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';
import {Dismiss} from '@app/core/models/guests/dismiss.model';
import {DismissReason} from '@app/core/models/guests/dismiss-reason.model';
import {DismissReasonService} from '@app/core/http/dismiss-reason/dismiss-reason.service';

@Component({
  selector: 'app-dismiss-form',
  templateUrl: './dismiss-form.component.html',
  styleUrls: ['./dismiss-form.component.scss']
})
export class DismissFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    date: [null, Validators.required],
    time: [null, Validators.required],
    reason: [null, Validators.required],
    note: [null],
    drugManagement: [null, Validators.required],
    articleManagement: [null, Validators.required],
    absentOnLastDay: [null]
  });
  
  dismissReasons: DismissReason[] | undefined;
  drugManagements$ = ['Elimina farmaci', 'Sposta farmaci nel magazzino generale'];
  articleManagements$ = ['Elimina ausili', 'Sposta ausili nel magazzino generale'];
  reasonDismiss: DismissReason | undefined;
  dismissedGuestUUID: string | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
	private dismissReasonService: DismissReasonService,
    private userSecurityService: UserService
  ) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (!user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_MANAGEMENT)) {
      this.form.disable();
    }
  }

  ngOnInit(): void {
	this.form.get('note')?.disable();
	this.dismissReasons = [];
	this.dismissReasonService.getAllAsObservable()?.subscribe(reasons => {
		reasons.forEach(reason => this.dismissReasons?.push(reason));
	});
	this.dismissedGuestUUID = this.data.guest?.uuid;
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  updateForm(data: Dismiss): void {
    this.form?.patchValue({
      ...data,
      // Dates
      date: data.date != null ? new Date(data.date) : null,
    });
  }

  reasonFormControl(): FormControl {
    return this.form.get('reason') as FormControl;
  }
  
  drugManagementFormControl(): FormControl {
    return this.form.get('drugManagement') as FormControl;
  }

  articleManagementFormControl(): FormControl {
    return this.form.get('articleManagement') as FormControl;
  }
  
  onReasonSelection(): void {
	let selectedDismissReason = this.form.get('reason')?.value;
    if (selectedDismissReason.id === 2 || selectedDismissReason.id === 3) {
		this.form.get('note')?.enable();
	} else {
		this.form.get('note')?.setValue(null);
		this.form.get('note')?.disable();
	}
  }

}

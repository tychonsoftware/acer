import {Component, Input, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Constants} from '@app/core/constants/Constants';
import {City, GuestTransition} from '@app/core/models';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatTableDataSource} from '@angular/material/table';
import {GuestService} from '@app/core/http';
import {DialogService} from '@app/core/services/dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-registry-step',
  templateUrl: './registry-step.component.html',
  styleUrls: ['./registry-step.component.scss']
})
export class RegistryStepComponent extends FormContentDirective implements OnInit{
  // Selects options
  @Input()
  cities: City[] = [];
  fileTypes = ['image/png', 'image/jpg', 'image/jpeg'];
  fileNotSupported = false;
  isFromArchive = false;
  isHospitalized = false;
  buttonDisabled = true;
  CFMessage: string | undefined;
  CFUuidArchive: string | undefined;
  sameArchivedCF = false;

  // Form del primo step (Anagrafica)
  form = this.fb.group({
    uuid: [null],
    // Anagrafica
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    birthDate: [null, Validators.required],
    birthPlace: ['', Validators.required],
    fiscalCode: [null, [Validators.required, Validators.pattern(Constants.FISCAL_CODE_REGEX)]],
    landingPhone: [],
    mobilePhone: [],
    sex: [],

    // Altre Info
    numberIdentifier: [],
    disability: [],
    doctor: [],
    doctorMail: [null, [Validators.minLength(5), Validators.maxLength(254), Validators.pattern(Constants.EMAIL_REGEX)]],
    expirationCardDate: [],
    independent: [],
    privacy: [],

    // Foto
    picName: [],
    picContent: [],

    // Note
    note: [],

    // Dates
    acceptanceDate: [],
    dateInsert: [],
    exitDate: [],
    lastDateModify: [],
    userInsertion: [],
    profilePicture: [null]
  });
  files: File[] = [];
  lastPicContent: string | ArrayBuffer | null | undefined;


  // caricamente delle immagini
  url = './assets/img/user.jpeg';
  constructor(
    // Utility services
    private fb: FormBuilder,
    private guestService: GuestService,
	private translateService: TranslateService,
	private dialogService: DialogService
  ) {
    super();
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    console.log(this.form.get('picContent')?.value);
    console.log(this.lastPicContent);
    this.lastPicContent = this.form.get('picContent')?.value;

	if (history.state.data) {
        const patientData = history.state.data.element;
        if (patientData) {
          if (patientData.status !== undefined && (patientData.status.id === 3)) {
            this.isFromArchive = true;
          }
          if (patientData.status !== undefined && (patientData.status.id === 2 || patientData.status.id === 4)) {
            this.isHospitalized = true;
          }
        }
     }
  }

  birthPlaceFormControl(): FormControl {
    return this.form.get('birthPlace') as FormControl;
  }

  picContentFormControl(): FormControl {
    return this.form.get('picContent') as FormControl;
  }

  updateForm(data: GuestTransition): void {

    this.form?.patchValue({
      ...data,
      birthDate: data.birthDate ? new Date(data.birthDate) : null,
      birthPlace: this._getBirthPlace(data.birthPlace),
      expirationCardDate: data.expirationCardDate ? new Date(data.expirationCardDate) : null,
      picContent: data.picContent,
      picName: data.picName,
      profilePicture: data.profilePicture
    });
  }

  private _getBirthPlace(birthPlace?: string): City | undefined {
    return birthPlace ? this.cities.find(c => c.description.toLowerCase().startsWith(birthPlace.toLowerCase())) : undefined;
  }

  isProfilePicturePresent(): boolean{
    return this.lastPicContent != null;
  }

  getFileUploadBorder(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'border-danger' : 'border-dark';
  }

  getFileUploadTextColor(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'text-danger' : 'text-dark';
  }
  /**
   * On guest selection change
   * @param files Uploaded file/s.
   */
  onFileUploadedChange(files: any[]): void {
    if (files && files.length === 0) {
      this.lastPicContent = null;
      this.form.get('picName')?.setValue(null);
      this.form.get('picContent')?.setValue(null);
      this.form.get('profilePicture')?.setValue(null);
    }
    if (files && files[0]) {
      if (files.length > 1) {
        files.shift();
      }
      const file = files[0];
      this.form.get('picName')?.setValue(file.name);
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = ( () => {
        this.form.get('picContent')?.setValue(reader.result);
        this.lastPicContent = reader.result;
      } );
    }
  }

  onValidationChange(value: any): void {
    console.log(value);
    this.fileNotSupported = value;
    if (value){
      this.lastPicContent = undefined;
    }
  }

  checkCFFromArchive() {
	const fiscalCode = this.form.get('fiscalCode')?.value;
	if (fiscalCode && fiscalCode.length === 16) {
		console.log('Checking fiscal code presence from archive');
		this.guestService.getByFiscalCodeFromArchive(fiscalCode).subscribe (guests => {
			if (guests.length > 0) {
				console.log('Same discharged fiscal code detected');
				const guest = guests[0];
				let pipe = new DatePipe('it-IT'); // Use your own locale
				const formattedDate = pipe.transform(guest.birthDate, 'shortDate');
				let guestMessage = "Il codice fiscale \u00E8 gi\u00E0 presente in archivio e appartiene all' ospite: " +
					guest.firstName + " " + guest.lastName + " nato/a a " +  guest.birthPlace + " il " + formattedDate + ". ";
				let requestMessage = "Vuoi recuperare i dati anagrafici dall' archivio?";
				this.CFMessage = guestMessage + requestMessage;
				this.CFUuidArchive = guest.uuid;
				this.buttonDisabled = false;
				this.sameArchivedCF = true;
			}
			else {
				this.buttonDisabled = true;
				this.sameArchivedCF = false;
			}
		});
	} else {
		this.buttonDisabled = true;
		this.sameArchivedCF = false;
	}
  }

  manageConfirmCopyDialog(): void {
	const message = this.CFMessage;
    const archivedUuid = this.CFUuidArchive;

	if (message) {
		const data = {
	      title: this.translateService.instant('copyGuestDialog.title'),
	      message: this.translateService.instant(message),
	      cancelText: this.translateService.instant('copyGuestDialog.cancelText'),
	      confirmText: this.translateService.instant('copyGuestDialog.confirmText')
	    };

	    // Open the delete dialog
	    const dialogRef = this.dialogService.openConfirmDialog(data);

		this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => {
			if (confirmed && archivedUuid) {
				this.guestService.getByUUID(archivedUuid).subscribe(result => {
					const guest = result;
					this.form?.get('firstName')?.setValue(guest.firstName);
					this.form?.get('lastName')?.setValue(guest.lastName);
					this.form?.get('sex')?.setValue(guest.sex);
					if (guest.birthDate)
						this.form?.get('birthDate')?.setValue(new Date(guest.birthDate));
					this.form?.get('mobilePhone')?.setValue(guest.mobilePhone);
					this.form?.get('landingPhone')?.setValue(guest.landingPhone);
					this.form?.get('numberIdentifier')?.setValue(guest.numberIdentifier);
					this.form?.get('disability')?.setValue(guest.disability);
					this.form?.get('doctor')?.setValue(guest.doctor);
					this.form?.get('doctorMail')?.setValue(guest.doctorMail);
					this.form?.get('expirationCardDate')?.setValue(guest.expirationCardDate);
					this.form?.get('independent')?.setValue(guest.independent);
					this.form?.get('privacy')?.setValue(guest.privacy);
					this.form?.get('picContent')?.setValue(guest.picContent);
					this.form?.get('picName')?.setValue(guest.picName);
				    this.form?.get('note')?.setValue(guest.note);
					this.form?.get('birthPlace')?.setValue(this._getBirthPlace(guest.birthPlace));
				});
			}
			this.sameArchivedCF = false;
		});
  	 }
  }
}

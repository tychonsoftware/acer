import {Component, Inject, Input, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {City, ParentAna, Province} from '@app/core/models';
import {Observable} from "rxjs";
import {CityService, ProvinceService} from '@app/core/http';
import {Functionalities} from "@app/core/constants/Functionalities";
import {UserService} from "@app/core/http/users/user.service";
import {environment} from "@env/environment";
import {Constants} from '@app/core/constants/Constants';
import { filter } from 'rxjs/operators';



@Component({
  selector: 'app-family-member-form',
  templateUrl: './family-member-form.component.html',
  styleUrls: ['./family-member-form.component.scss']
})
export class FamilyMemberFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    uuid: [],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    landingPhone: [''],
    mobilePhone: [''],
    fiscalCode: [null, [Validators.pattern(Constants.FISCAL_CODE_REGEX)]], kinship: ['', Validators.required],
    email: [null, [Validators.minLength(5), Validators.maxLength(254), Validators.pattern(Constants.EMAIL_REGEX)]],
    address: [''],
    province: [null],
    cap: [''],
    birthPlace: [null],
    note: [''],
    isResponsible: ['']
  });
  // API Results
  provinces$: Province[] = [];
  cities$: City[] = [];

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private provinceService: ProvinceService,
    public cityService: CityService
  ) {
    // Calling super to init the extended class
    super();

    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      this.isEdit = true;
      this.updateForm(data.editFormData);
    }
  }

  ngOnInit(): void {
    this.provinceService.getAll().subscribe( result => {
      this.provinces$ = result;
    });
    this.cityService.getAll().subscribe( result => {
      this.cities$ = result;
    });
    this.checkRequiredFields(this.form);
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  onSubmit(): void {
    this.formSubmitAttempt = true;
    if (this.form?.valid) {
      let dataToEmit: ParentAna;
      const currentDate = new Date();
	  
	  const city = this.form?.get('birthPlace')?.value;

      if (this.editFormData) {
        dataToEmit = {
          ...this.editFormData,
          ...this.form.value,
		  birthPlace: city?.description ? city?.description : city
        };
      } else {
        dataToEmit = {
          ...this.form.value,
          dateInsert: currentDate,
		  birthPlace: city?.description ? city?.description : city
        };
      }

      console.log(JSON.stringify(dataToEmit));
      this.validSubmit.emit(dataToEmit);
    }
  }

  formControl(formControlKey: string): FormControl {
    return this.form?.get(formControlKey) as FormControl;
  }

  birthPlaceFormControl(): FormControl {
    return this.form.get('birthPlace') as FormControl;
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {City, ParentAna} from '@app/core/models';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {MatDialogConfig} from '@angular/material/dialog';
import {FamilyMemberFormComponent} from '@app/modules/acceptance/stepper/family-step/family-member-form/family-member-form.component';
import {Functionalities} from "@app/core/constants/Functionalities";

@Component({
  selector: 'app-family-step',
  templateUrl: './family-step.component.html',
  styleUrls: ['./family-step.component.scss']
})
export class FamilyStepComponent implements OnInit {
  // TODO: generare mediante translate service
  // Table Columns
  familyTableColumns: TableColumn[] = [
    {
      name: 'Nome',
      dataKey: 'firstName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Cognome',
      dataKey: 'lastName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Parentela',
      dataKey: 'kinship',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Recapito Telefonico',
      dataKey: 'landingPhone',
      position: 'center',
      isSortable: true
    },
	{
      name: 'Cellulare',
      dataKey: 'mobilePhone',
      position: 'center',
      isSortable: true
    }
  ];

  // Data
  familyData: ParentAna[] = [];
  disabled = false;
  filterSearch = '';

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true,
    width: '30%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
  ) {
  }

  ngOnInit(): void {
  }

  getFamilyData(): ParentAna[] {
    return this.familyData;
  }

  setFamilyData(data?: ParentAna[]): void {
    if (data) {
      this.familyData = data;
    }
  }

  openFamilyMemberDialog(parentAna?: ParentAna): void {
    if (parentAna && parentAna.firstName) {
      this._openFamilyMemberFormDialog(parentAna);
    } else {
      this._openFamilyMemberFormDialog();
    }
  }

  // Dialog che si apre quando si cerca di eliminare un elemento dalla lista
  onDeleteFamilyMember(editedParentAna: ParentAna): void {
    const data = {
      title: this.translateService.instant('deleteDialog.title'),
      message: this.translateService.instant('deleteDialog.message', {value: `${editedParentAna.firstName} ${editedParentAna.lastName}`}),
      cancelText: this.translateService.instant('deleteDialog.cancelText'),
      confirmText: this.translateService.instant('deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => {
      if (confirmed) {
        // Se true prendo il valore selezionato lo rimuovo e riassegno il nuovo vettore di valori da visualizzare
        // Modifico l'elemento
		this.familyData.splice(this.familyData.indexOf(editedParentAna), 1);
        this.familyData = [...this.familyData];
      }
    });
  }

  // Visualizza i dettagli del familiare del paziente quando si clicca la riga della tabella
  // onDetailsFamilyMember(element: ParentAna): void {
  //   // Apro la modal di aliminazione di un elemento
  //   this.dialog.open(DetailsFamilyComponent, {
  //     data: {element},
  //     width: '30%',
  //   });
  // }

  private _openFamilyMemberFormDialog(editedParentAna?: ParentAna): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedParentAna,
      title: this.translateService.instant(`family.formDialog.${editedParentAna ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('family.formDialog.cancelText'),
      confirmText: this.translateService.instant('family.formDialog.confirmText')
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(FamilyMemberFormComponent, data, this.dialogConfig);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(newParentAna => {
      if (newParentAna) {
        if (editedParentAna) {
          // Modifico l'elemento
          this.familyData.splice(this.familyData.indexOf(editedParentAna), 1, newParentAna);
          this.familyData = [...this.familyData];
        } else {
          newParentAna.dateInsert = new Date();
          this.familyData = [...this.familyData, newParentAna];
        }
      }
    });
  }
  public setDisabled(val: boolean): void {
    this.disabled = val;
  }
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterSearch = filterValue;
  }
}

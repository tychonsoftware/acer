import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {City, Contract, Nation, Province, RecoveryTaxRegime, RecoveryTaxRegimeType} from '@app/core/models';
import {
  CityService,
  ContractService,
  NationService,
  ProvinceService,
  RecoveryTaxRegimeTypeService
} from '@app/core/http';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {Constants} from '@app/core/constants/Constants';
import {MatButtonToggleGroup} from '@angular/material/button-toggle';
import {UserService} from '@app/core/http/users/user.service';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';

@Component({
  selector: 'app-recovery-tax-regime-step',
  templateUrl: './recovery-tax-regime-step.component.html',
  styleUrls: ['./recovery-tax-regime-step.component.scss']
})
export class RecoveryTaxRegimeStepComponent extends FormContentDirective implements OnInit {
  // Selects options
  @Input()
  cities: City[] = [];
  @Input()
  building?: OrganizationBuilding;
  @Input()
  recoveryTaxRegimeTypes?: RecoveryTaxRegimeType[];
  @Input()
  isEditable = true;

  @ViewChild('group') group: MatButtonToggleGroup | undefined;


  // Form
  form = this.fb.group({
    id: [],
    firstName: [''],
    lastName: [''],
    fiscalCode: ['', [Validators.pattern(Constants.FISCAL_CODE_REGEX)]],
    recipientCode: [],
    address: [''],
    cap: [''],
    city: [null],
    province: [null],
    nation: [null],
    pec: [null],
    contract: [null],
    recoveryTaxRegimeType: [null, Validators.required],
    acceptanceDate: ['', Validators.required]
  });

  // isPrivateTaxRegime = false;

  // Selects options
  provinces$: Observable<Province[]> | undefined;
  nations$: Observable<Nation[]> | undefined;
  contracts$: Observable<Contract[]> | undefined;
  buildings: OrganizationBuilding[] = [];

  constructor(
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private provinceService: ProvinceService,
    private nationService: NationService,
    private cityService: CityService,
    private recoveryTaxRegimeTypeService: RecoveryTaxRegimeTypeService,
    private contractService: ContractService,
    private userService: UserService
  ) {
    super();
  }

  ngOnInit(): void {
    // Getting the billing property asynchronously from API
    this.provinces$ = this.provinceService.getAll();
    this.nations$ = this.nationService.getAll();
    // This are get from ObservableApiService
    this.contracts$ = this.contractService.getAllAsObservable();
    const userSecurity = this.userService.getUserSecurity();
    if (!this.building) {
      this.building = userSecurity?.defaultBuilding ? userSecurity.defaultBuilding : {};
    }
    this.buildings = userSecurity?.buildings ? userSecurity.buildings : [];
    if (!this.recoveryTaxRegimeTypes) {
      this.recoveryTaxRegimeTypes = this.building?.recoveryTaxRegimeTypes ? this.building.recoveryTaxRegimeTypes : [];
    }

	if (this.form?.get('city')?.value) {
		var selectedCity = this.form?.get('city')?.value;
		this.form?.get('province')?.setValue(selectedCity.province);
	}
  }

  public disableBuilding(): void {
    this.isEditable = false;
  }

  public enableBuilding(): void {
    this.isEditable = true;
  }

  isPrivateTaxRegime(): boolean {
    const selectedTaxRegimeType = this.form.get('recoveryTaxRegimeType')?.value;
    return selectedTaxRegimeType && selectedTaxRegimeType.toLowerCase().startsWith('privato');
  }

  onTaxRegimeTypeChange(event: any): void {
    if (!this.isPrivateTaxRegime()) {
      this.form.get('contract')?.patchValue(null);
    }
  }

  formControl(formControlKey: string): FormControl {
    return this.form.get(formControlKey) as FormControl;
  }

  getAcceptanceDate(): Date | undefined {
    return this.form?.get('acceptanceDate')?.value ? new Date(this.form?.get('acceptanceDate')?.value) : undefined;
  }

  getRecoveryTaxRegimeData(): RecoveryTaxRegime {
    return {
      ...this.form.value,
      recoveryTaxRegimeType: this._getRecoveryTaxRegimeType(this.form.get('recoveryTaxRegimeType')?.value)
    };
  }

  updateForm(data?: RecoveryTaxRegime): void {
    this.form?.setValidators([]);
    this.form?.patchValue({
      ...data,
      recoveryTaxRegimeType: data?.recoveryTaxRegimeType?.description,
      acceptanceDate: data?.acceptanceDate ? new Date(data?.acceptanceDate) : null
    });
  }

  clearSubmitted(): void {
    this.formSubmitAttempt = false;
    const value = this.form?.value;
    this.ngForm?.resetForm();
    this.form?.patchValue(value);
    this.form?.get('recoveryTaxRegimeType')?.setValue(value.recoveryTaxRegimeType);
  }

  private _getRecoveryTaxRegimeType(description: string): RecoveryTaxRegimeType | undefined {
    if (description && this.recoveryTaxRegimeTypes) {
      description = description.toLowerCase();
      return this.recoveryTaxRegimeTypes.find(r => r.description.toLowerCase().startsWith(description));
    }
    return undefined;
  }

  setRecoveryTaxRegimeTypes(): void {
    if (this.building) {
      this.recoveryTaxRegimeTypes = this.building.recoveryTaxRegimeTypes ? this.building.recoveryTaxRegimeTypes : [];
    }
  }

  onProvinceSelection(selectedProvince: Province): void {
    this.form.get('city')?.reset();
    this.cities = [];
	if (selectedProvince.id) {
		this.cityService.getByProvince(selectedProvince.id).subscribe(res => {
			console.log('Cities filtered by selected province');
			console.log(res);
			this.cities = res;
		});
	}
  }
}

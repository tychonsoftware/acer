import {AfterViewInit, Component, Input, Output, EventEmitter , OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {AbstractControl, FormBuilder, FormControl, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Asl, City, Nation, Region, Province} from '@app/core/models';
import {NationService, RegionService, AslService} from '@app/core/http';
import {AreaService} from '@app/core/http/area/area.service';
import {DistrictService} from '@app/core/http/district/district.service';
import {Observable} from 'rxjs';
import {OrganizationService} from '@app/core/http/organization/organization.service';
import {CookieService} from 'ngx-cookie-service';
import {AslRegion} from '@app/core/models/shared/asl.region';
import {Area} from '@app/core/models/shared/area.model';
import {District} from '@app/core/models/shared/district.model';
import {Constants} from '@app/core/constants/Constants';


@Component({
  selector: 'app-residence-step',
  templateUrl: './residence-step.component.html',
  styleUrls: ['./residence-step.component.scss']
})
export class ResidenceStepComponent extends FormContentDirective implements OnInit, AfterViewInit, OnChanges {
  // Selects options
  @Input()
  cities: City[] = [];
  // Form
  form = this.fb.group({
    // Residenza Storica
    historicResidence: this.fb.group({
      uuid: [null],
      id: [],
      asl: [],
      // cap: [],
      city: [],
      region: [],
      address: [],
      area: [],
      district: []
      // nation: [], // fixme: aggunto questo eliminare
    }),
    // Addresses
    address: this.fb.group({
      uuid: [null],
      id: [],
      address: [null, Validators.required],
      cap: [null, Validators.required],
      nation: [null, Validators.required],
      province: [],
      region: [],
      city: [null, Validators.required]
    }),
    addressDomicile: this.fb.group({
      uuid: [null],
      id: [],
      address: [],
      cap: [],
      nation: [],
      province: [],
      region: [],
      city: [],
      copiedInfo: []
    }),
    medicalDomicile: [],
    residence: []
  });

  // API results
  nations$: Observable<Nation[]> | undefined;
  regions$: Observable<AslRegion[]> | undefined;
  asls$: Observable<Asl[]> | undefined;
  areas$: Observable<Area[]> | undefined;
  districts$: Observable<District[]> | undefined;

  // Selects options
  citiesByAslRegionId: { [key: number]: City[] } = {};
  aslsByCityId: { [key: number]: Asl[] } = {};
  regionsByCityId: { [key: number]: AslRegion[] } = {};
  provincesByCityId: { [key: number]: Province[] } = {};
  areasByCityId: { [key: number]: String[] } = {};
  districtsByCityId: { [key: number]: String[] } = {};

  filteredCityOptions: City[] = [];
  regionFromSelectedCity: AslRegion[] = [];
  provinceFromSelectedCity: Province[] = [];
  areaFromSelectedCity: String[] = [];
  districtFromSelectedCity: String[] = [];

  isCopy = false;
  isCampania = false;
  copiedCity: City | undefined;
  selectedRegion: AslRegion | undefined;

  constructor(
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private nationService: NationService,
    private regionService: RegionService,
    private organizationService: OrganizationService,
    private cookieService: CookieService,
    private aslService: AslService,
    private areaService: AreaService,
    private districtService: DistrictService
  ) {
    super();
  }

  private _setCities(cities: City[]): void {
    cities.forEach(c => {

      if (c.aslRegion?.id) {
        if (!this.citiesByAslRegionId[c.aslRegion.id]) {
          this.citiesByAslRegionId[c.aslRegion.id] = [];
        }
        this.citiesByAslRegionId[c.aslRegion.id].push(c);
      }

      if (c?.id && c?.aslRegion) {
        if (!this.regionsByCityId[c.id]) {
          this.regionsByCityId[c.id] = [];
        }
        this.regionsByCityId[c.id].push(c.aslRegion);
      }

      if (c?.id && c?.province) {
        if (!this.provincesByCityId[c.id]) {
          this.provincesByCityId[c.id] = [];
        }
        this.provincesByCityId[c.id].push(c.province);
      }
    });
  }

  ngOnInit(): void {
    // Getting the billing property asynchronously from API
    this.nations$ = this.nationService.getAll();
    this.regions$ = this.regionService.getAll();
    this.nationService.getAll().subscribe(
      res => {
        this.form.get('address')?.get('nation')?.setValue(res.find(n => n.description === 'ITALIA'));
        this.form.get('addressDomicile')?.get('nation')?.setValue(res.find(n => n.description === 'ITALIA'));
      }
    );
    this.organizationService.getByName(this.cookieService.get('organization') ? this.cookieService.get('organization') : Constants.DEFAULT_ORGANIZATION)
      .subscribe(res => {
        if (res && res.body && res.body.region && res.body.region.description === 'CAMPANIA') {
          this.isCampania = true;
          const region = this.form.get('historicResidence')?.get('region')?.value?.description;
          if (region && region !== 'CAMPANIA') {
            this.form.get('historicResidence')?.get('area')?.disable();
            this.form.get('historicResidence')?.get('district')?.disable();
          }
        }
      });

    // Disable forms' fields
    this.form.get('address')?.get('region')?.disable();
    this.form.get('address')?.get('province')?.disable();
    this.form.get('addressDomicile')?.get('region')?.disable();
    this.form.get('addressDomicile')?.get('province')?.disable();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cities = this.cities.filter((v: any, i: number, a: any) => a.findIndex((t: any) => (t.description === v.description)) === i);
    this._setCities(this.cities);
    this.filteredCityOptions = this.citiesByAslRegionId[this.form.get('historicResidence')?.get('region')?.value?.id];
  }

  ngAfterViewInit(): void {
    //
  }

  regionFormControl(): FormControl {
    return this.form.get('historicResidence')?.get('region') as FormControl;
  }

  cityFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('city') as FormControl;
  }

  addressDomicileCityFormControl(): FormControl {
    return this.form.get('addressDomicile')?.get('city') as FormControl;
  }

  aslFormControl(): FormControl {
    return this.form.get('historicResidence')?.get('asl') as FormControl;
  }

  cityRegionFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('region') as FormControl;
  }

  provinceFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('province') as FormControl;
  }

  areaFormControl(): FormControl {
    return this.form.get('historicResidence')?.get('area') as FormControl;
  }

  districtFormControl(): FormControl {
    return this.form.get('historicResidence')?.get('district') as FormControl;
  }

  nationFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('nation') as FormControl;
  }

  toggleFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('copiedInfo') as FormControl;
  }

  onRegionSelection(selectedAslRegion: Region): void {
	this.selectedRegion = selectedAslRegion;
    this.filteredCityOptions = this.citiesByAslRegionId[selectedAslRegion.id];
    this.form.get('historicResidence')?.get('city')?.reset();
    this.form.get('historicResidence')?.get('asl')?.reset();
    this.form.get('historicResidence')?.get('area')?.reset();
    this.form.get('historicResidence')?.get('district')?.reset();
    if (selectedAslRegion?.description != 'CAMPANIA') {
      this.form.get('historicResidence')?.get('area')?.disable();
      this.form.get('historicResidence')?.get('district')?.disable();
    } else {
      this.form.get('historicResidence')?.get('area')?.enable();
      this.form.get('historicResidence')?.get('district')?.enable();
    }
  }

  onCitySelection(selectedCity: City): void {
    this.form.get('historicResidence')?.get('asl')?.reset();
    this.form.get('historicResidence')?.get('area')?.reset();
    this.form.get('historicResidence')?.get('district')?.reset();
    this.asls$ = this.aslService.getAslFromCity(selectedCity.id);
    this.asls$.subscribe(res => {
      if (res.length === 1) {
        this.form.get('historicResidence')?.get('asl')?.setValue(res[0]);
      }
    });

    this.areas$ = this.areaService.getAreaFromCity(selectedCity.id);
    this.areas$.subscribe(res => {
      if (res.length === 1) {
        this.form.get('historicResidence')?.get('area')?.setValue(res[0]);
      }
    });

    this.districts$ = this.districtService.getDistrictFromCity(selectedCity.id);
    this.districts$.subscribe(res => {
      if (res.length === 1) {
        this.form.get('historicResidence')?.get('district')?.setValue(res[0]);
      }
    });
  }

  onCitySelectionChangeRegionProvince(selectedCity: City, section: string): void {
    this.regionFromSelectedCity = this.regionsByCityId[selectedCity.id];
    if (this.regionFromSelectedCity) {
      this.form.get(section)?.get('region')?.setValue(this.regionFromSelectedCity[0]);
    } else {
      this.form.get(section)?.get('region')?.setValue(null);
    }

    this.provinceFromSelectedCity = this.provincesByCityId[selectedCity.id];
    if (this.provinceFromSelectedCity) {
      this.form.get(section)?.get('province')?.setValue(this.provinceFromSelectedCity[0]);
    } else {
      this.form.get(section)?.get('province')?.setValue(null);
    }
  }

  disableNonItalianFields(selectedNation: Nation, section: string): void {
    if (selectedNation.description !== 'ITALIA') {
      this.form.get(section)?.get('region')?.setValue(null);
      this.form.get(section)?.get('province')?.setValue(null);
      this.form.get(section)?.get('city')?.setValue(null);
      this.form.get(section)?.get('city')?.disable();
      this.form.get(section)?.get('province')?.disable();
      this.form.get(section)?.get('region')?.disable();
    } else {
      this.form.get(section)?.get('city')?.enable();
      this.form.get(section)?.get('province')?.enable();
      this.form.get(section)?.get('region')?.enable();
    }
  }

  onCopy(): void {
    this.isCopy = !this.isCopy;
    if (this.isCopy) {
      this.form?.get('addressDomicile')?.get('nation')?.enable();
      this.form?.get('addressDomicile')?.get('city')?.enable();
      this.form?.get('addressDomicile')?.get('region')?.enable();
      this.form?.get('addressDomicile')?.get('province')?.enable();
      this.form?.get('addressDomicile')?.get('address')?.enable();
      this.form?.get('addressDomicile')?.get('cap')?.enable();
      if (this.isCampania) {
        this.form.get('addressDomicile')?.get('address')?.setValue(this.form.get('address')?.get('address')?.value);
        this.form.get('addressDomicile')?.get('cap')?.setValue(this.form.get('address')?.get('cap')?.value);
        this.form.get('addressDomicile')?.get('nation')?.setValue(this.form.get('address')?.get('nation')?.value);
        this.form.get('addressDomicile')?.get('province')?.setValue(this.form.get('address')?.get('province')?.value);
        this.form.get('addressDomicile')?.get('region')?.setValue(this.form.get('address')?.get('region')?.value);
        this.form.get('addressDomicile')?.get('city')?.setValue(this.form.get('address')?.get('city')?.value);
      } else {
        this.form.get('addressDomicile')?.get('address')?.setValue(this.form.get('historicResidence')?.get('address')?.value);
        this.form.get('addressDomicile')?.get('city')?.setValue(this.form.get('historicResidence')?.get('city')?.value);
        this.form.get('addressDomicile')?.get('region')?.setValue(this.form.get('historicResidence')?.get('region')?.value);
        this.form.get('addressDomicile')?.get('region')?.disable();
        if (this.filteredCityOptions) {
          this.copiedCity = this.filteredCityOptions.find(city => city.description === this.form.get('addressDomicile')?.get('city')?.value.description);
        }
        if (this.copiedCity) {
          this.provinceFromSelectedCity = this.provincesByCityId[this.copiedCity.id];
        }
        this.form.get('addressDomicile')?.get('province')?.setValue(this.provinceFromSelectedCity[0]);
        this.form.get('addressDomicile')?.get('province')?.disable();
        this.nationService.getAll().subscribe(
          res => {
            this.form.get('addressDomicile')?.get('nation')?.setValue(res.find(n => n.description === 'ITALIA'));
          }
        );
      }
      this.disableNonItalianFields(this.form?.get('addressDomicile')?.get('nation')?.value, 'address');
    } else {
      this.form?.get('addressDomicile')?.get('nation')?.disable();
      this.form?.get('addressDomicile')?.get('city')?.disable();
      this.form?.get('addressDomicile')?.get('region')?.disable();
      this.form?.get('addressDomicile')?.get('province')?.disable();
      this.form?.get('addressDomicile')?.get('address')?.disable();
      this.form?.get('addressDomicile')?.get('cap')?.disable();
    }
  }
  clearSubmitted(): void {
    this.formSubmitAttempt = false;
    const value = this.form?.value;
    this.ngForm?.resetForm();
    this.form?.patchValue(value);
  }

}

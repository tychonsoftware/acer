import {AfterViewInit, Component, EventEmitter, Input, OnChanges, Output, ViewChild} from '@angular/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MatCalendar } from '@angular/material/datepicker';
import { AttendanceCalendar } from '@app/core/models/attendance/attendance-calendar.model';
import {UserService} from "@app/core/http/users/user.service";
import {Functionalities} from "@app/core/constants/Functionalities";

export const MATERIAL_DATEPICKER_FORMATS = {
  parse: {
    dateInput: 'DD/MMM/YYYY',
  },
  display: {
    dateInput: 'DD/MMM/YYYY',
    monthYearLabel: 'DD/MM/YYYY',
    dateA11yLabel: 'DD/MMM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-mat-calendar-inline',
  templateUrl: './mat-calendar-inline.component.html',
  styleUrls: ['./mat-calendar-inline.component.scss'],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: MATERIAL_DATEPICKER_FORMATS}
  ],
})
export class MatCalendarInlineComponent implements OnChanges, AfterViewInit {

  @Input() calendarObject: AttendanceCalendar | undefined;
  @ViewChild(MatCalendar) calendar: MatCalendar<Date> | undefined;
  @Output() previous: EventEmitter<any> = new EventEmitter<any>();
  @Output() next: EventEmitter<any> = new EventEmitter<any>();
  @Output() dateSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngAfterViewInit(): void {
    (document.getElementsByClassName('mat-calendar-previous-button')[0] as HTMLButtonElement).addEventListener('click', e=> {this.previous.emit(this.calendar?.monthView.activeDate)});
    (document.getElementsByClassName('mat-calendar-next-button')[0] as HTMLButtonElement).addEventListener('click', e=> {this.next.emit(this.calendar?.monthView.activeDate)});
  }

  isSelected = (event: any) => {
    const dateObj = event._d;
    let selected = '';
    const date = dateObj.getFullYear() + '-' + ('00' + (dateObj.getMonth() + 1)).slice(-2) + '-' + ('00' + dateObj.getDate()).slice(-2);
    const dateTimeStamp = new Date(date).getTime();
    const currentDate = new Date();
    if (dateObj.getDate() > currentDate.getDate() && dateObj.getMonth() >= currentDate.getMonth() &&  dateObj.getFullYear() >= currentDate.getFullYear()) {
      selected += 'futureDay';
    }
    if (this.calendarObject) {
      if (this.calendarObject.actualAttendanceDTO) {
        const actualAttendance = this.calendarObject.actualAttendanceDTO;
        actualAttendance.forEach(attendance => {
          let actualExitDateObj = null;
          if (attendance.actualExitDate != null){
            actualExitDateObj = new Date(attendance.actualExitDate);
            let actualReturnDateObj = null;
            if (attendance.actualReturnDate != null){
              actualReturnDateObj =  new Date(attendance.actualReturnDate);
            }else {
              actualReturnDateObj = new Date();
            }
            let actualExitDate = actualExitDateObj.getFullYear() + '-' + ('00' + (actualExitDateObj.getMonth() + 1)).slice(-2);
            actualExitDate +=  '-' +  ('00' + actualExitDateObj.getDate()).slice(-2);
            let actualReturnDate = actualReturnDateObj.getFullYear() + '-';
            actualReturnDate +=  ('00' + (actualReturnDateObj.getMonth() + 1)).slice(-2) + '-' + ('00' + actualReturnDateObj.getDate()).slice(-2);
            const actualExitDateTimeStamp = new Date(actualExitDate).getTime();
            const actualReturnDateTimeStamp = new Date(actualReturnDate).getTime();
            if (dateTimeStamp === actualExitDateTimeStamp) {
              selected = 'selected red-bar start';
              return;
            } else if (dateTimeStamp > actualExitDateTimeStamp && dateTimeStamp <= actualReturnDateTimeStamp) {
              selected = 'selected red-bar' + (dateTimeStamp === actualReturnDateTimeStamp ? ' end' : '');
              return;
            }
          }
        });
      }
      if (this.calendarObject.plannedAttendanceDTO) {
        const plannedAttendance = this.calendarObject.plannedAttendanceDTO;
        plannedAttendance.forEach(attendance => {
          let plannedExitDateObj = null;
          if (attendance.plannedExitDate != null){
            plannedExitDateObj = new Date(attendance.plannedExitDate);
            let plannedReturnDateObj = null;
            if (attendance.plannedReturnDate != null){
              plannedReturnDateObj =  new Date(attendance.plannedReturnDate);
            }else {
              plannedReturnDateObj =  new Date();
            }
            let plannedExitDate = plannedExitDateObj.getFullYear() + '-' + ('00' + (plannedExitDateObj.getMonth() + 1)).slice(-2);
            plannedExitDate +=  '-' +  ('00' + plannedExitDateObj.getDate()).slice(-2);
            let plannedReturnDate = plannedReturnDateObj.getFullYear() + '-';
            plannedReturnDate +=  ('00' + (plannedReturnDateObj.getMonth() + 1)).slice(-2) + '-' + ('00' + plannedReturnDateObj.getDate()).slice(-2);
            const plannedExitDateTimeStamp = new Date(plannedExitDate).getTime();
            const plannedReturnDateTimeStamp = new Date(plannedReturnDate).getTime();
            if (dateTimeStamp === plannedExitDateTimeStamp) {
              selected = 'selected yellow-bar start';
              return;
            } else if (dateTimeStamp > plannedExitDateTimeStamp && dateTimeStamp <= plannedReturnDateTimeStamp) {
              selected = 'selected yellow-bar' + (dateTimeStamp === plannedReturnDateTimeStamp ? ' end' : '');
              return;
            }
          }
        });
      }
    }
    return selected;
  }

  ngOnChanges(changes: any): void {
    this.calendar?.updateTodaysDate();
  }

  select(event: any, calendar: any): void {
    this.dateSelected.emit(event);
  }
}

import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {TranslateService} from '@ngx-translate/core';
// Services
import {AssistanceProjectService, CityService, FileService, GuestService, RecoveryTaxRegimeTypeService} from '@app/core/http';

import {
  AssistanceProject,
  City,
  GuestMinimalTransition,
  GuestStatus,
  GuestTransition, Nation, Province, Region,
  ValidationObject
} from '@app/core/models';
import {RecoveryTaxRegimeStepComponent} from '@app/modules/acceptance/stepper/recovery-tax-regime-step/recovery-tax-regime-step.component';
import {ResidenceStepComponent} from '@app/modules/acceptance/stepper/residence-step/residence-step.component';
import {RegistryStepComponent} from '@app/modules/acceptance/stepper/registry-step/registry-step.component';
import {formatDate} from '@angular/common';
import {ValidatorService} from '@app/core/http/validator/validator.service';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {FamilyStepComponent} from '@app/modules/acceptance/stepper/family-step/family-step.component';
import {ExemptionsStepComponent} from '@app/modules/acceptance/stepper/exemptions-step/exemptions-step.component';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {Observable, of} from 'rxjs';
import {MatDialogConfig} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AssistanceProjectFormComponent} from '@app/modules/assistance-projects/assistance-project-form/assistance-project-form.component';
import {DialogService} from '@app/core/services/dialog.service';
import {SnackBarService} from '@app/core/services/snack-bar.service';
import {tap} from 'rxjs/operators';
import {AttendanceCalendar} from '@app/core/models/attendance/attendance-calendar.model';
import {AttendanceService} from '@app/core/http/attendance/attendance.service';
import {Attendance} from '@app/core/models/attendance/attendance.model';
import {AttendanceFormComponent} from '@app/modules/guest/guest-attendance-form/attendance-form.component';
import {UserService} from '@app/core/http/users/user.service';
import {getFileNameFromPath, OrderType, sortObject} from '@app/shared/helpers/utility';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';
import {NationService} from '@app/core/http';

import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';
import {HospitalizationSession} from '@app/core/models/guests/hospitalization-session.model';
import {Dismiss} from '@app/core/models/guests/dismiss.model';
import {DismissFormComponent} from '@app/modules/acceptance/stepper/dismiss-step/dismiss-form/dismiss-form.component';
import {DismissStepComponent} from '@app/modules/acceptance/stepper/dismiss-step/dismiss-step.component';
import {DismissService} from '@app/core/http/dismiss/dismiss.service';

@Component({
  selector: 'app-stepper-acceptance',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, AfterViewInit, AfterViewChecked {
  // Steps
  @ViewChild(RegistryStepComponent) registryStepComponent: RegistryStepComponent | undefined;
  @ViewChild(ResidenceStepComponent) residenceStepComponent: ResidenceStepComponent | undefined;
  @ViewChild(RecoveryTaxRegimeStepComponent) recoveryTaxRegimeStepComponent: RecoveryTaxRegimeStepComponent | undefined;
  @ViewChild(FamilyStepComponent) familyStepComponent: FamilyStepComponent | undefined;
  @ViewChild(ExemptionsStepComponent) exemptionsStepComponent: ExemptionsStepComponent | undefined;
  @ViewChild(DismissStepComponent) dismissStepComponent: DismissStepComponent | undefined;

  isEditable = false;
  isEditableAttendance = false;
  isEditableAssistance = false;

  // Selects options
  cities: City[] = [];
  editable = false;

  // Steps forms
  get registryForm(): FormGroup | null {
    return this.registryStepComponent ? this.registryStepComponent.form : null;
  }

  get residenceForm(): FormGroup | null {
    return this.residenceStepComponent ? this.residenceStepComponent.form : null;
  }

  get recoveryTaxRegimeForm(): FormGroup | null {
    return this.recoveryTaxRegimeStepComponent ? this.recoveryTaxRegimeStepComponent.form : null;
  }

  // Serve oer definire se lo stepper deve essere lineare o meno
  isFromArchive = false;
  isEdit = false;
  fiscalCodeValidationMessage = '';
  registryStepHasError = false;
  recoveryTaxRegimeStepHasError = false;
  residenceHasError = false;
  // familyHasError = false;
  isHospitalized = false;
  isDismissed = false;
  patient: any;
  selectedMenu = 1;
  calendarElement: AttendanceCalendar | undefined;
  guestCondition = 'RIENTRATO';
  savedUuid: string | undefined;
  nearestAttendance: Attendance | undefined;
  isGuestExit = true;
  error = false;

  public assistanceProjectsTableColumns: TableColumn[] = [];

  public closerProgrammingTableColumns: TableColumn[] = [
    {
      name: 'Uscita Programmata',
      dataKey: 'plannedExitDate',
      position: 'center',
      isDate: true,
      isSortable: false,
      method: {
        methodName: 'nonSpecificatoUscita',
        methodParam: 'Uscita Programmata'
      }
    },
    {
      name: 'Rientro Programmato',
      dataKey: 'plannedReturnDate',
      position: 'center',
      isDate: true,
      isSortable: true,
      method: {
        methodName: 'nonSpecificatoRientro',
        methodParam: 'Rientro Programmato'
      }
    },
    {
      name: 'Uscita Effettiva',
      dataKey: 'actualExitDate',
      position: 'center',
      isDate: true,
      isSortable: true,
      method: {
        methodName: 'nonSpecificatoUscita',
        methodParam: 'Uscita Effettiva'
      }
    },
    {
      name: 'Rientro Effettiva',
      dataKey: 'actualReturnDate',
      position: 'center',
      isDate: true,
      isSortable: true,
      method: {
        methodName: 'nonSpecificatoRientro',
        methodParam: 'Rientro Effettiva'
      }
    },
    {
      name: 'Motivo Uscita',
      dataKey: 'reason.description',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Note',
      dataKey: 'note',
      position: 'center',
      isSortable: true
    }
  ];

  public allProgrammingColumns: TableColumn[] = [
    {
      name: 'Uscita',
      dataKey: 'actualExitDate',
      position: 'center',
      isDate: true,
      isSortable: false,
      method: {
        methodName: 'nonSpecificatoUscita',
        methodParam: 'Non specificato'
      }
    },
    {
      name: 'Rientro',
      dataKey: 'actualReturnDate',
      position: 'center',
      isDate: true,
      isSortable: false,
      method: {
        methodName: 'nonSpecificatoRientro',
        methodParam: 'Non specificato'
      }
    },
    {
      name: 'Motivo',
      dataKey: 'reason.description',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Status',
      dataKey: 'status.description',
      position: 'center',
      isSortable: true
    }
  ];
  assistanceProjects$: Observable<AssistanceProject[]> | undefined;
  closerProgramming$: Observable<Attendance[]> | undefined;
  showUscito = false;
  allProgramming$: Observable<Attendance[]> | undefined;
  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%',
    panelClass: 'ap-full-view-btns'
  };
  selectedDate: any = null;
  header: any;
  currentDate = new Date();
  guestMinimal: GuestMinimalTransition | undefined;
  currentSession: HospitalizationSession | undefined;

  constructor(
    // Utility services
    private route: Router,
    private cookieService: CookieService,
    private translateService: TranslateService,
    // Api services
    private guestService: GuestService,
    private cityService: CityService,
    private validatorService: ValidatorService,
    private recoveryTaxRegimeTypeService: RecoveryTaxRegimeTypeService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private dialogService: DialogService,
    public assistanceProjectsService: AssistanceProjectService,
    public attendanceService: AttendanceService,
    private elem: ElementRef,
    private renderer: Renderer2,
    private userSecurityService: UserService,
    private fileService: FileService,
    private nationService: NationService,
    private dismissService: DismissService,
    private snackBarService: SnackBarService
  ) {
  }

  // Inizializzazione dello stepper otionArrray
  ngOnInit(): void {
    this.cityService.getAll().subscribe(cities => {
      this.cities = cities;
      if (history.state.data) {
        const patientData = history.state.data.element;
        if (patientData) {
          if (patientData.status !== undefined && (patientData.status.id === 3)) {
            this.isFromArchive = true;
          }
          if (patientData.status !== undefined && (patientData.status.id === 2 || patientData.status.id === 4)) {
            this.isHospitalized = true;
            if (patientData.status.id === 2) {
              this.guestCondition = 'USCITO';
            }
          }
          console.log('Data from GET/EDIT');
          console.log(patientData);
          this._patchValues(patientData);
          this._getGuestAssistanceProjects();
          if (this.isEdit && this.isHospitalized) {
            this.registryForm?.disable();
            this.residenceForm?.disable();
            this.recoveryTaxRegimeForm?.disable();
            this.recoveryTaxRegimeStepComponent?.disableBuilding();
            this.familyStepComponent?.setDisabled(true);
            this.exemptionsStepComponent?.setDisabled(true);
          }

        }
        if (patientData && patientData.hospitalizationSession) {
          this.currentSession = patientData.hospitalizationSession;
        }
        if (patientData && patientData.uuid) {
          if (patientData?.status?.id === 3) {
            this.isDismissed = true;
          }
          this.guestService.getMinimalByUUID(patientData.uuid).subscribe(result => {
            this.guestMinimal = result;
          });
        }
      }
	  this.checkSecurity();
    });
    this.selectedDate = new Date();
    // this.header = new Date().toLocaleString('it-IT', {month: 'long', year: 'numeric'});
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (this.isHospitalized && user?.functionalities?.find(element => element.functionality === Functionalities.GUEST_DETAIL)
      || (!this.isHospitalized && user?.functionalities?.find(element => element.functionality === Functionalities.NEW_GUEST))) {
      this.isEditable = true;
    }
    if (this.isHospitalized && user?.functionalities?.find(element => element.functionality === Functionalities.GUEST_ATTENDANCE)) {
      this.isEditableAttendance = true;
    }
    if (this.isHospitalized && user?.functionalities?.find(element => element.functionality === Functionalities.ASSISTANCE_PROJECT)) {
      this.isEditableAssistance = true;
    }
  }

// Dopo che il componente è stato inizializzato
  ngAfterViewInit(): void {
    // Assegno il valore passato come parametro dalla schermata di accettazione, che può essere null in caso stiamo ricoverando un paziente
    // non presente in lista di attesa, e sarà popolata se stiamo passando un paziente che era in lista di attesa.
    if (history.state.data) {
      const patientData = history.state.data.element;
      if (patientData) {
        console.log('Data from GET/EDIT');
        console.log(patientData);
        this.patient = patientData;
        this._patchValues(patientData);
      }
    }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  // onFileSelected(): void {
  //   const inputNode: any = document.querySelector('#file');
  //
  //   if (typeof (FileReader) !== 'undefined') {
  //     const reader = create FileReader();
  //
  //     reader.onload = (e: any) => {
  //       this.registryForm.get('picUrl')?.patchValue(e.target.result);
  //     };
  //
  //     console.log(this.registryForm);
  //     reader.readAsArrayBuffer(inputNode.files[0]);
  //   }
  // }


  // Step click
  onStepChange(event: StepperSelectionEvent): void {
    event.previouslySelectedStep.interacted = false;
    if (event.selectedIndex === 4) {
      this._setDefaulRecoveryTaxRegimeFields();
    }
    // saving data
    /*    if (!this.isHospitalized) {
          this._saveData();
        }*/
  }

  // Patient save.
  onSave(): void {
    this.registryStepHasError = false;
    this.recoveryTaxRegimeStepHasError = false;
    this.residenceHasError = false;
    // this.familyHasError = false;

    // Clearing the form submission errors
    this.registryStepComponent?.clearSubmitted();
    this.recoveryTaxRegimeStepComponent?.clearSubmitted();
    this.residenceStepComponent?.clearSubmitted();


    if (this.editable) {
      if (this.isHospitalized) {
        this.registryStepComponent?.markAsSubmitted();
        this.registryStepComponent?.form.markAllAsTouched();
        this.recoveryTaxRegimeStepComponent?.markAsSubmitted();
        this.recoveryTaxRegimeStepComponent?.form.markAllAsTouched();
        this.residenceStepComponent?.markAsSubmitted();
        this.residenceStepComponent?.form?.markAllAsTouched();

        // Setting errors message on steps if any
        this._setStepsErrorMessage();
        if (this.registryForm?.valid) {
          // this._checkParentsResponsible();
          this._checkFiscalCode(false);

        }
      } else {
        // saving data
        // this._setStepsErrorMessage();
        this._saveData(false, true);
      }
    } else if (!this.isHospitalized) {
      // saving data
      // this._setStepsErrorMessage();
      this.snackBarService.showSnackBar('SUCCESSO', 'Salvataggio avvenuto con successo', 'success');
      this._saveData(false, true);
    }
  }

  onHospitalize(): void {
    this.registryStepComponent?.markAsSubmitted();
    this.registryStepComponent?.form.markAllAsTouched();
    this.recoveryTaxRegimeStepComponent?.markAsSubmitted();
    this.recoveryTaxRegimeStepComponent?.form.markAllAsTouched();
    this.residenceStepComponent?.markAsSubmitted();
    this.residenceStepComponent?.form?.markAllAsTouched();

    // this._checkParentsResponsible();
    // Setting errors message on steps if any
    this._setStepsErrorMessage();
    if (this.registryForm?.valid) {
      console.log(this.registryForm.value);
      this._checkFiscalCode(true);
    } else {
      const data = {
        title: 'Verifica Errori',
        message: 'Alcuni campi sono incompleti o assenti.',
        cancelText: 'Annulla',
        confirmText: '',
        hideSave: true
      };
      //this.dialogService.openWarningDialog(data);
      this.snackBarService.showSnackBar('Verifica Errori', 'Alcuni campi sono assenti o incorretti', 'error');
    }
  }

  /* private _checkParentsResponsible(): void {
     if (this.familyStepComponent?.familyData) {
       let isValid = false;
       const parents = this.familyStepComponent?.familyData as ParentAna[];
       parents.forEach((parent) => {
         if (parent.isResponsible) {
           isValid = true;
         }
       });
       this.familyHasError = !isValid;
       if (this.familyHasError) {
         const data = {
           title: 'Familiare Coobligato errore',
           message: 'Obbligatorio almeno un Familiare Coobbligato!',
           cancelText: 'Annulla',
           confirmText: '',
           hideSave: true
         };
         this.dialogService.openWarningDialog(data);
       }
     } else {
       this.familyHasError = true;
     }
   }*/

  private _checkFiscalCode(onHospitalize: boolean): void {
    // if (!this.familyHasError) {
    const patientData = history.state.data.element;
    console.log(patientData);
    const validationDTO: ValidationObject = {
      cf: this.registryForm?.get('fiscalCode')?.value,
      name: this.registryForm?.get('firstName')?.value,
      surname: this.registryForm?.get('lastName')?.value,
      sex: this.registryForm?.get('sex')?.value,
      dataNascita: formatDate(this.registryForm?.get('birthDate')?.value, 'dd/MM/yyyy', 'it-IT'),
      codLuogoNascita: (this.registryForm?.get('birthPlace')?.value as City).code,
      userType: 'guest',
      guestUuid: patientData ? patientData?.uuid : this.savedUuid,
    };

    console.log('Validating CF : ', JSON.stringify(validationDTO));

    this.validatorService.checkCF(validationDTO).subscribe(res => {
      console.log('CF Validation response ', JSON.stringify(res));
      this.fiscalCodeValidationMessage = res.message;
      if (res.status === 'OK') {
        this.registryStepHasError = false;

        // Only if also the recovery tax regime form is valid we save and do redirect.
        if (this.recoveryTaxRegimeForm?.valid) {
          this._saveData(true);
        }
      } else if (res.status === 'SAME_ACCEPTED_GUEST_CF') {
        this.registryStepHasError = true;
        this.registryForm?.get('fiscalCode')?.setErrors({pattern: true});
        this.openErrorSameCFDialog(res.message);
      } else if (res.status === 'SAME_DISCHARGED_GUEST_CF' && onHospitalize) {
        this.registryStepHasError = false;
        console.log('SAME DISCHARGED GUEST FISCAL CODE DETECTED');
        if (this.recoveryTaxRegimeForm?.valid && this.residenceForm?.valid) {
          this._updateArchivedGuest(true, res.archivedUuid, validationDTO.guestUuid);
        }
      } else if (res.status === 'SAME_DISCHARGED_GUEST_CF' && !onHospitalize) {
        this.registryStepHasError = false;
        console.log('SAME DISCHARGED GUEST FISCAL CODE DETECTED - ERROR');
        this.openErrorSameCFDialog('Il codice fiscale non \u00E8 valido perch\u00E8 gi\u00E0 presente in archivio.');
      } else {
        this.registryStepHasError = true;
        this.registryForm?.get('fiscalCode')?.setErrors({pattern: true});
      }
    });
    // }
  }

  private openErrorSameCFDialog(message: string): void {
    const data = {
      title: this.translateService.instant('sameCFErrorDialog.title'),
      message: this.translateService.instant(message),
      cancelText: this.translateService.instant('sameCFErrorDialog.cancelText'),
      confirmText: '',
      hideSave: true
    };
    this.dialogService.openWarningDialog(data);
  }

  /**
   * Save the GuestStatus data and redirect to the acceptance page if doRedirect is true.
   * @param doRedirect True to redirect to the acceptance page after saving and validating data.
   * @private
   */
  private _updateArchivedGuest(doRedirect = false, archivedUuid: string, onHoldUuid: string | undefined): void {
    if (this.registryForm?.get('firstName')?.invalid || this.registryForm?.get('lastName')?.invalid
      || this.registryForm?.get('birthDate')?.invalid || this.registryForm?.get('birthPlace')?.invalid) {
      this.registryStepHasError = true;
      this.registryStepComponent?.markAsSubmitted();
      this.registryForm?.get('firstName')?.markAsTouched();
      this.registryForm?.get('lastName')?.markAsTouched();
      this.registryForm?.get('birthDate')?.markAsTouched();
      this.registryForm?.get('birthPlace')?.markAsTouched();
      // this.registryForm?.get('firstName')?.setErrors({required: true});
      // this.registryForm?.get('lastName')?.setErrors({required: true});

      return;
    }

    let guestStatus: GuestStatus = {id: 2, description: 'Ammesso'};

    this.guestService.getByUUID(archivedUuid).subscribe(result => {
      const guest = result;
      guest.acceptanceDate = new Date();
      guest.status = guestStatus;
      guest.lastDateModify = new Date();
      guest.lastUserModify = this.cookieService.get('username');
      guest.exitDate = undefined;

      guest.mobilePhone = this.registryForm?.get('mobilePhone')?.value;
      guest.landingPhone = this.registryForm?.get('landingPhone')?.value;
      guest.numberIdentifier = this.registryForm?.get('numberIdentifier')?.value;
      guest.disability = this.registryForm?.get('disability')?.value;
      guest.doctor = this.registryForm?.get('doctor')?.value;
      guest.doctorMail = this.registryForm?.get('doctorMail')?.value;
      guest.expirationCardDate = this.registryForm?.get('expirationCardDate')?.value;
      guest.independent = this.registryForm?.get('independent')?.value;
      guest.privacy = this.registryForm?.get('privacy')?.value;
      guest.picContent = this.registryForm?.get('picContent')?.value;
      guest.picName = this.registryForm?.get('picName')?.value;
      guest.note = this.registryForm?.get('note')?.value;

      // Residenza information
      guest.historicResidence = this.residenceForm?.get('historicResidence')?.value;
      guest.address = this.residenceForm?.get('address')?.value;
      guest.addressDomicile = this.residenceForm?.get('addressDomicile')?.value;

      // Familiari information
      guest.parents = [];
      guest.parents = this.familyStepComponent?.getFamilyData();

      // Exemptions information
      guest.exemptions = [];
      guest.exemptions = this.exemptionsStepComponent?.getExemptionsData();

      // Therapeutic Plan information
      guest.therapeuticPlans = [];
      guest.therapeuticPlans = this.exemptionsStepComponent?.getTherapeuticPlanDataData();

      guest.recoveryTaxRegime = this.recoveryTaxRegimeStepComponent?.getRecoveryTaxRegimeData();

      guest.acceptanceDate = this.recoveryTaxRegimeStepComponent?.getAcceptanceDate();

      console.log('BEFORE SAVE');
      console.log(guest);
      this.guestService.update(guest).subscribe(saved => {
        console.log('Result from PUT');
        console.log(saved);

        this._onGuestCallback(saved, doRedirect);

        if (!onHoldUuid) {
          onHoldUuid = this.savedUuid;
        }

        if (onHoldUuid) {
          this.guestService.deleteFromDB(onHoldUuid).subscribe(_ => {
            console.log('DELETED FROM DB GUEST WITH UUID: ' + onHoldUuid);
          });
        }

      });
    });
  }

  /**
   * Save the GuestStatus data and redirect to the acceptance page if doRedirect is true.
   * @param doRedirect True to redirect to the guest page after saving and validating data.
   * @param doRedirectToAcceptance True to redirect to the acceptance page after saving and validating data.
   * @private
   */
  private _saveData(doRedirect = false, doRedirectToAcceptance = false): void {
    if (this.registryForm?.get('firstName')?.invalid || this.registryForm?.get('lastName')?.invalid
      || this.registryForm?.get('birthDate')?.invalid || this.registryForm?.get('birthPlace')?.invalid) {
      this.registryStepHasError = true;
      this.registryStepComponent?.markAsSubmitted();
      this.registryForm?.get('firstName')?.markAsTouched();
      this.registryForm?.get('lastName')?.markAsTouched();
      this.registryForm?.get('birthDate')?.markAsTouched();
      this.registryForm?.get('birthPlace')?.markAsTouched();
      // this.registryForm?.get('firstName')?.setErrors({required: true});
      // this.registryForm?.get('lastName')?.setErrors({required: true});

      return;
    }

    // if form are not dirty or untouched we do not save
    // if ((!this.registryForm?.dirty && !this.residenceForm?.dirty && !this.recoveryTaxRegimeForm?.dirty) ||
    //   (this.registryForm?.untouched && this.residenceForm?.untouched && this.recoveryTaxRegimeForm?.untouched)) {
    //   return;
    // }

    // If doRedirect GuestStatus => 'Ammesso', 'in Attesa' otherwise
    let guestStatus: GuestStatus = doRedirect ? {id: 2, description: 'Ammesso'} : {
      id: 1,
      description: 'in Attesa',
      uuid: 'uuid-status-01'
    };
    if (this.isFromArchive) {
      guestStatus = {
        id: 3,
        description: 'Dimesso'
      };
    }

    // todo: domandare a Marco perché non c'è
    // recupero la provincia di residenza
    // let province = this.provinces.find(obj => this.residenceForm.value.provinceResidence.id === obj.id);
    if (this.dismissStepComponent?.getDismissData() && this.dismissStepComponent?.getDismissData()?.id) {
      this.dismissService.update(this.dismissStepComponent.getDismissData()).subscribe(saved => {
        this.dismissStepComponent?.setDismissData(saved);
        if (this.currentSession && this.currentSession.dismiss) {
          this.currentSession.dismiss = saved;
        }
      });
    }

    console.log(this.recoveryTaxRegimeForm?.value);
    if (this.residenceForm?.get('address')?.get('city') && this.residenceForm?.get('address')?.get('city')?.value === '') {
      this.residenceForm?.get('address')?.get('city')?.setValue(undefined);
    }
    if (this.residenceForm?.get('addressDomicile')?.get('city') && this.residenceForm?.get('addressDomicile')?.get('city')?.value === '') {
      this.residenceForm?.get('addressDomicile')?.get('city')?.setValue(undefined);
    }

    if (this.recoveryTaxRegimeForm?.get('city')?.value === '') {
      this.recoveryTaxRegimeForm?.get('city')?.setValue(undefined);
    }
    if (this.recoveryTaxRegimeForm?.get('province')?.value === '') {
      this.recoveryTaxRegimeForm?.get('province')?.setValue(undefined);
    }

    // creazione dell'guest
    const guest: GuestTransition = {
      ...this.registryForm?.value,
      ...this.residenceForm?.value,
      organizationBuilding: this.recoveryTaxRegimeStepComponent?.building,
      birthPlace: (this.registryForm?.get('birthPlace')?.value as City)?.description,

      // non so come gestire
      deleted: false,
      lastUserModify: this.cookieService.get('username'),

      recoveryTaxRegime: {
        ...this.recoveryTaxRegimeStepComponent?.getRecoveryTaxRegimeData()
      },

      // Family members, exemptions and therapeutic plans
      parents: this.familyStepComponent?.getFamilyData(),
      exemptions: this.exemptionsStepComponent?.getExemptionsData(),
      therapeuticPlans: this.exemptionsStepComponent?.getTherapeuticPlanDataData(),
      hospitalizationSession: this.currentSession,
      status: guestStatus,

      lastDateModify: new Date(),
      acceptanceDate: this.recoveryTaxRegimeStepComponent?.getAcceptanceDate()
    };
    console.log('BEFORE SAVE');
    console.log(guest);
    // todo: decommentare il redirect
    if (this.isEdit) {
      this.guestService.update(guest).subscribe(result => {
        console.log('Result from PUT');
        console.log(result);
        if (this.currentSession && this.currentSession.dismiss && result.hospitalizationSession) {
          result.hospitalizationSession.dismiss = this.currentSession.dismiss;
        }
        this.snackBarService.showSnackBar('SUCCESSO!', 'Salvataggio avvenuto con successo', 'success');
        this.savedUuid = result.uuid;
        this._onGuestCallback(result, doRedirect, doRedirectToAcceptance);
      });
    } else {
      // Adding dates
      guest.acceptanceDate = new Date();
      guest.dateInsert = new Date();
      guest.userInsertion = this.cookieService.get('username');

      this.guestService.create(guest).subscribe(result => {
        console.log('Result from POST');
        console.log(result);
        this.snackBarService.showSnackBar('SUCCESSO!', 'Salvataggio avvenuto con successo', 'success');
        this.savedUuid = result.uuid;
        this._onGuestCallback(result, doRedirect, doRedirectToAcceptance);
      });
    }
  }

  private _onGuestCallback(result: GuestTransition, doRedirect: boolean, doRedirectToAcceptance = false): void {
    if (result) {
      if (result.profilePicture && result.profilePicture.staticPath !== undefined) {
        const filePath = `${result.profilePicture?.staticPath.path}/${result.profilePicture?.dynamicPath}`;
        this.fileService.downloadFile(filePath).subscribe(pictureData => {
          this.patient = result;
          if (result.profilePicture) {
            this.patient.picName = getFileNameFromPath(result.profilePicture.dynamicPath);
          }
          this.patient.picContent = pictureData;
        });
      }
      this._patchValues(result);
      if (doRedirect) {
        if (!this.isHospitalized) {
          // In caso di risultato positivo faccio un redirect alla pagina di accettazione
          this.route.navigate(['/guest']);
        }
      } else if (doRedirectToAcceptance) {
        this.route.navigate(['/acceptance']);
      }
    }
  }

  private _patchValues(patientData: GuestTransition): void {
    if (patientData) {
      // Set isEdit to true so thant next we call updateGuest
      this.isEdit = true;

      // Updating data for registry form
      if (patientData.profilePicture && patientData.profilePicture.dynamicPath !== undefined) {
        patientData.picName = getFileNameFromPath(patientData.profilePicture.dynamicPath);
      }
      this.registryStepComponent?.updateForm(patientData);
      const birthPlace = this._getBirthPlace(patientData.birthPlace);
      this.registryForm?.get('birthPlace')?.setValue(birthPlace !== undefined ? birthPlace : '');

      // Updating data for residence form
      this.residenceStepComponent?.updateForm(patientData);

      // Updating data for recovery tax regime form
      this.recoveryTaxRegimeStepComponent?.updateForm(patientData.recoveryTaxRegime);

      // Family member data
      this.familyStepComponent?.setFamilyData(patientData.parents);

      // Exemptions and therapeutic plans data
      this.exemptionsStepComponent?.setData(patientData.exemptions, patientData.therapeuticPlans);

      // Updating data for residence form
      if (this.isFromArchive) {
        this.dismissStepComponent?.setDismissData(patientData.hospitalizationSession?.dismiss);
      }
    }
    this._setDefaulRecoveryTaxRegimeFields();
  }

  private _setDefaulRecoveryTaxRegimeFields(): void {
    // GET INFO FROM REGISTRY FORM
    if (this.registryForm?.get('firstName')?.value && !this.recoveryTaxRegimeForm?.get('firstName')?.value) {
      this.recoveryTaxRegimeForm?.get('firstName')?.setValue(this.registryForm?.get('firstName')?.value);
    }
    if (this.registryForm?.get('lastName')?.value && !this.recoveryTaxRegimeForm?.get('lastName')?.value) {
      this.recoveryTaxRegimeForm?.get('lastName')?.setValue(this.registryForm?.get('lastName')?.value);
    }
    if (this.registryForm?.get('fiscalCode')?.value && !this.recoveryTaxRegimeForm?.get('fiscalCode')?.value) {
      this.recoveryTaxRegimeForm?.get('fiscalCode')?.setValue(this.registryForm?.get('fiscalCode')?.value);
    }
    // GET INFO FROM RESIDENCE FROM
    if (this.residenceForm?.get('address')?.get('city')?.value && !this.recoveryTaxRegimeForm?.get('city')?.value) {
      const city = this.residenceForm?.get('address')?.get('city')?.value;
      this.recoveryTaxRegimeForm?.get('city')?.setValue(city);
      if (!this.recoveryTaxRegimeForm?.get('province')?.value) {
        this.recoveryTaxRegimeForm?.get('province')?.setValue(city?.province);
      }
    }
    if (this.residenceForm?.get('address')?.get('address')?.value && !this.recoveryTaxRegimeForm?.get('address')?.value) {
      this.recoveryTaxRegimeForm?.get('address')?.setValue(this.residenceForm?.get('address')?.get('address')?.value);
    }
    if (this.residenceForm?.get('address')?.get('cap')?.value && !this.recoveryTaxRegimeForm?.get('cap')?.value) {
      this.recoveryTaxRegimeForm?.get('cap')?.setValue(this.residenceForm?.get('address')?.get('cap')?.value);
    }
    this.nationService.getAll().subscribe(nations => {
      nations.forEach(nat => {
        if (nat.description === 'ITALIA') {
          this.recoveryTaxRegimeForm?.get('nation')?.setValue(nat);
        }
      });
    });
  }

  private _setStepsErrorMessage(): void {
    if (this.recoveryTaxRegimeForm) {
      this.recoveryTaxRegimeStepHasError = this.recoveryTaxRegimeForm.invalid;
    }
    if (this.registryForm) {
      this.registryStepHasError = this.registryForm.invalid;
    }
    if (this.residenceForm) {
      this.residenceHasError = this.residenceForm.invalid;
    }
    if (this.residenceHasError || this.registryStepHasError || this.recoveryTaxRegimeStepHasError) {
      this.snackBarService.showSnackBar('Verifica Errori', 'Alcuni campi sono assenti o incorretti', 'error');
    }
  }

  private _getBirthPlace(birthPlace?: string): City | undefined {
    return birthPlace ? this.cities.find(c => c.description.toLowerCase().startsWith(birthPlace.toLowerCase())) : undefined;
  }

  selectedMenuChange(value: number): void {
    this.selectedMenu = value;
    if (this.selectedMenu === 2) {
      this._getGuestAssistanceProjects();
    } else if (this.selectedMenu === 3) {
      this.currentDate = new Date();
      this.header = new Date().toLocaleString('it-IT', {month: 'long', year: 'numeric'});
      this._getGuestCalendarAttendances(this.selectedDate);
      this._getGuestCloserAttendances();
      this._getGuestAttendances(this.selectedDate);
    }
  }

  /**
   * Open the Assistance Project form dialog.
   * @param assistanceProject Data of the Assistance Project to edit.
   */
  openFormDialog(assistanceProject?: AssistanceProject): void {
    if (assistanceProject && assistanceProject.id) {
      // Getting the contract data from the API
      this._openAssistanceProjectFormDialog(assistanceProject);
    } else {
      this._openAssistanceProjectFormDialog();
    }
  }

  /**
   * Open the delete Assistance Project dialog.
   * @param assistanceProject The Assistance Project to delete.
   */
  onDeleteClick(assistanceProject: AssistanceProject): void {
    const data = {
      title: this.translateService.instant('assistanceProjects.deleteDialog.title'),
      message: this.translateService.instant('assistanceProjects.deleteDialog.message', {value: assistanceProject.authNumber}),
      cancelText: this.translateService.instant('assistanceProjects.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteContract(assistanceProject));
  }

  /**
   * Delete Assistance Project
   * @param assistanceProject The Assistance Project to delete.
   * @private
   */
  private _deleteContract(assistanceProject: AssistanceProject): void {
    if (assistanceProject.id) {
      this.assistanceProjectsService.delete(assistanceProject.id);
      this._getGuestAssistanceProjects();
    }
  }

  private _openAssistanceProjectFormDialog(assistanceProject?: AssistanceProject): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: assistanceProject,
      title: this.translateService.instant(`assistanceProjects.formDialog.${assistanceProject?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('assistanceProjects.formDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.formDialog.confirmText'),
      patientUUID: this.patient.uuid
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AssistanceProjectFormComponent, data, {
      disableClose: true,
      autoFocus: false,
      width: '75%',
      panelClass: 'ap-full-view-btns'
    });

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => this._saveAssistanceProject(ap as AssistanceProject));
  }

  private _getGuestAssistanceProjects(): void {
    this.assistanceProjects$ = this.assistanceProjectsService.getByGuestUUID(this.patient.uuid)
      .pipe(tap(res => {
        console.log('GET assistanceProjects for guest');
        console.log(res);
      }));
  }

  /**
   * Save the Assistance Project.
   * @param assistanceProject The Assistance Project to save
   * @private
   */
  private _saveAssistanceProject(assistanceProject?: AssistanceProject): void {
    if (assistanceProject) {
      delete (assistanceProject as any)?.aslPercentEuro;
      delete (assistanceProject as any)?.cityPercentEuro;
      delete (assistanceProject as any)?.userPercentEuro;
      let result: Observable<AssistanceProject>;
      if (assistanceProject.id) { // edit case
        result = this.assistanceProjectsService.updateAndGetObservable(assistanceProject);
      } else {
        result = this.assistanceProjectsService.createAndGetObservable(assistanceProject);
      }

      result.subscribe(_ => this._getGuestAssistanceProjects());
    }
  }

  onSelect(event: any): void {
    console.log(event._d.getTime());
    this.selectedDate = event;
  }

  clickPrevious(): void {
    (document.getElementsByClassName('mat-calendar-previous-button')[0] as HTMLButtonElement).click();
  }

  clickNext(): void {
    (document.getElementsByClassName('mat-calendar-next-button')[0] as HTMLButtonElement).click();
  }

  onPrevious(val: any): void {
    this.currentDate = new Date(val._d.getFullYear(),
      (this.currentDate.getMonth() === val._d.getMonth() ? val._d.getMonth() - 1 : val._d.getMonth()), val._d.getDate());
    this.header = this.currentDate.toLocaleString('it-IT', {month: 'long', year: 'numeric'});
    this._getGuestCalendarAttendances(this.currentDate);
    this._getGuestAttendances(this.currentDate);
  }

  onNext(val: any): void {
    this.currentDate = new Date(val._d.getFullYear(),
      (this.currentDate.getMonth() === val._d.getMonth() ? val._d.getMonth() + 1 : val._d.getMonth()), val._d.getDate());
    this.header = this.currentDate.toLocaleString('it-IT', {month: 'long', year: 'numeric'});
    this._getGuestCalendarAttendances(this.currentDate);
    this._getGuestAttendances(this.currentDate);
  }

  private _getGuestCalendarAttendances(selectedDate: Date): void {
    this.attendanceService.getByPeriod(formatDate(selectedDate, 'MM/yyyy', 'it-IT'), this.patient.uuid).subscribe(res => {
      console.log('Calendar object : ', res);
      this.calendarElement = res;
    });
  }

  private _getGuestCloserAttendances(): void {
    const attendances: Attendance[] = [];
    this.nearestAttendance = undefined;
    this.attendanceService.getGuestCloserAttendances(this.patient.uuid)
      .subscribe(res => {
          console.log('GET Closer attendances for guest');
          console.log(res);
          if (res != null) {
            this.nearestAttendance = res;
            attendances.push(res);
          }
          this.showUscito = attendances.length !== 0;
          this.closerProgramming$ = of(attendances);
        }, () => {
          this.showUscito = attendances.length !== 0;
          this.closerProgramming$ = of(attendances);
        }
      );
  }

  private _getGuestAttendances(selectedDate: any): void {
    this.attendanceService.getGuestAllPeriodAttendances(this.patient.uuid).subscribe
    (res => {
      console.log('GET attendances for guest');
      console.log(res);
      let data = res;
      setTimeout(() => {
        this.setCommonProgramming(data);
        data = sortObject(Object.values(data), 'plannedExitDate', OrderType.desc);
        this.allProgramming$ = of(data);
      }, 200);

    }, () => {
      this.allProgramming$ = of();
    });
  }

  setCommonProgramming(allprogramming: any): void {
    allprogramming.forEach((data: any) => {
      if (this.nearestAttendance?.id === data.id) {
        data.common = true;
      }
    });
  }

  openGuestExitPopup(attendance?: Attendance): void {
    this.isGuestExit = true;
    if (attendance && attendance.id) {
      // Getting the contract data from the API
      this._openAttendanceFormDialog(attendance);
    } else {
      this._openAttendanceFormDialog();
    }
  }

  /**
   * Open the Attendance form dialog.
   * @param attendance
   */
  openAttendanceFormDialog(attendance?: Attendance): void {
    this.isGuestExit = false;
    if (attendance && attendance.id) {
      // Getting the contract data from the API
      this._openAttendanceFormDialog(attendance);
    } else {
      this._openAttendanceFormDialog();
    }
  }

  private _openAttendanceFormDialog(attendance?: Attendance): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: attendance,
      title: this.translateService.instant(`attendance.formDialog.${attendance?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('attendance.formDialog.cancelText'),
      confirmText: this.translateService.instant('attendance.formDialog.confirmText'),
      guest: this.guestMinimal,
      isGuestExit: this.isGuestExit,
    };
    this.dialogConfig.width = '600px';
    this.dialogConfig.panelClass = '';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AttendanceFormComponent, data, this.dialogConfig);
    this.dialogService.confirmed(dialogRef)?.subscribe(at => this._saveAttendance(at as Attendance, data));
  }

  /**
   * Save the Attendance.
   * @param attendance The Attendance to save
   * @param data data
   * @private
   */
  private _saveAttendance(attendance?: Attendance, data?: any): void {

    if (attendance) {
      let result: Observable<Attendance>;
      let newAttendance = false;
      if (data && data.editFormData && data.editFormData.fromCalendar) {
        attendance.plannedExitDate = attendance.actualExitDate;
        attendance.plannedReturnDate = attendance.actualReturnDate;
        this.selectedDate = this.currentDate;
      }
      if (attendance.id) {
        result = this.attendanceService.updateAndGetObservable(attendance);
      } else {
        newAttendance = true;
        result = this.attendanceService.createAndGetObservable(attendance);
      }
      result.subscribe(_ => {
        let updateGuest = false;
        if (attendance.status != null && attendance.status.id === 2) {
          if (!data || !data.editFormData || !data.editFormData.fromCalendar) {
            this.patient.status = {id: 4, description: 'ASSENTE_TEMPORANEAMENTE'};
          }
          updateGuest = true;
        } else if (!newAttendance && attendance.status != null && attendance.status.id === 3) {
          this.patient.status = {id: 2, description: 'AMMESSO'};
          updateGuest = true;
        }
        if (updateGuest) {
          this.guestService.update(this.patient).subscribe(() => {
            this._getGuestAttendances(this.selectedDate);
            this._getGuestCloserAttendances();
            this._getGuestCalendarAttendances(this.selectedDate);
            if (this.patient.status != null && this.patient.status.id === 2) {
              this.guestCondition = 'USCITO';
            } else {
              this.guestCondition = 'RIENTRATO';
            }
          });
        } else {
          this._getGuestAttendances(this.selectedDate);
          this._getGuestCloserAttendances();
          this._getGuestCalendarAttendances(this.selectedDate);
        }
      });
    }
  }

  /**
   * Open the delete Attendance dialog.
   * @param attendance The Attendance to delete.
   */
  onAttendanceDeleteClick(attendance: Attendance): void {
    const data = {
      title: this.translateService.instant('attendance.deleteDialog.title'),
      message: this.translateService.instant('attendance.deleteDialog.message'),
      cancelText: this.translateService.instant('attendance.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('attendance.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteAttendance(attendance));
  }

  /**
   * Delete Attendance
   * @param attendance The Attendance to delete.
   * @private
   */
  private _deleteAttendance(attendance: Attendance): void {
    if (attendance.id) {
      let updateGuest = false;
      if (attendance.guest != null && attendance.status != null && attendance.status.id === 2) {
        updateGuest = true;
        this.patient.status = {id: 2, description: 'AMMESSO'};
        this.guestCondition = 'USCITO';
      }
      this.attendanceService.deleteById(attendance.id).subscribe(() => {
        if (updateGuest) {
          this.guestService.update(this.patient).subscribe(() => {
            this._getGuestAttendances(this.selectedDate);
            this._getGuestCloserAttendances();
            this._getGuestCalendarAttendances(this.selectedDate);
          });
        } else {
          this._getGuestAttendances(this.selectedDate);
          this._getGuestCloserAttendances();
          this._getGuestCalendarAttendances(this.selectedDate);
        }
      });
    }
  }

  onDateSelected(date: any): void {
    let attendance: any;
    attendance = {
      // plannedExitDate: date.toDate().getTime(),
      // plannedReturnDate: date.toDate().getTime(),
      actualExitDate: date.toDate().getTime(),
      fromCalendar: true
    };
    this._openAttendanceFormDialog(attendance);
  }

  changeModifica(): void {
    this.editable = true;
    this.registryForm?.enable();
    this.residenceForm?.enable();
    this.recoveryTaxRegimeForm?.enable();
    this.familyStepComponent?.setDisabled(false);
    this.exemptionsStepComponent?.setDisabled(false);
    this.recoveryTaxRegimeStepComponent?.enableBuilding();
  }

  dismiss(dismissed: GuestTransition): void {
    this._openDismissFormDialog(dismissed);
  }

  private _openDismissFormDialog(dismissed: GuestTransition, dismiss?: Dismiss): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: dismiss,
      title: this.translateService.instant(`dismiss.formDialog.dismissTitle`),
      cancelText: this.translateService.instant('dismiss.formDialog.cancelText'),
      confirmText: this.translateService.instant('dismiss.formDialog.dismissText'),
      guest: this.guestMinimal,
      isGuestExit: this.isGuestExit,
    };
    this.dialogConfig.width = '800px';
    this.dialogConfig.panelClass = '';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(DismissFormComponent, data, this.dialogConfig);
    this.dialogService.confirmed(dialogRef)?.subscribe(dism => {
      dism.guestUuid = dismissed.uuid;
      this._dismissGuest(dism as Dismiss, data);
    });
  }

  private _dismissGuest(dismiss?: Dismiss, data?: any) {
    if (dismiss) {
      this.guestService.dismiss(dismiss).subscribe(
        () => this.route.navigate(['guest'])
      );
    }
  }

}

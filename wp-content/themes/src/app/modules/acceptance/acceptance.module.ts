import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AcceptanceComponent} from '@app/modules/acceptance/acceptance.component';
import {SharedModule} from '@app/shared/shared.module';
import {DetailsDialogComponent} from '@app/modules/acceptance/details-dialog/details-dialog.component';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {StepperComponent} from '@app/modules/acceptance/stepper/stepper.component';
import {RecoveryTaxRegimeStepComponent} from './stepper/recovery-tax-regime-step/recovery-tax-regime-step.component';
import {ResidenceStepComponent} from './stepper/residence-step/residence-step.component';
import {RegistryStepComponent} from './stepper/registry-step/registry-step.component';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {FamilyStepComponent} from './stepper/family-step/family-step.component';
import {FamilyMemberFormComponent} from './stepper/family-step/family-member-form/family-member-form.component';
import {ExemptionsStepComponent} from './stepper/exemptions-step/exemptions-step.component';
import {DetailsFormComponent} from './stepper/exemptions-step/details-form/details-form.component';
import {AcceptanceRoutingModule} from '@app/modules/acceptance/acceptance-routing.module';
import {GuestAssistanceProjectsTableComponent} from '../guest/guest-assistance-projects-table/guest-assistance-projects-table.component';
import {GuestAssistanceProjectDetailsComponent} from '@app/modules/guest/guest-assistance-projects-table/guest-assistance-project-details/guest-assistance-project-details.component';
import {AttendanceFormComponent} from '@app/modules/guest/guest-attendance-form/attendance-form.component';
import {DismissFormComponent} from '@app/modules/acceptance/stepper/dismiss-step/dismiss-form/dismiss-form.component';
import {IConfig, NgxMaskModule} from 'ngx-mask';
import {MatCalendarInlineComponent} from './stepper/mat-calendar-inline/mat-calendar-inline.component';
import {MatSortModule} from '@angular/material/sort';
import {DismissStepComponent} from '@app/modules/acceptance/stepper/dismiss-step/dismiss-step.component'

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/acceptance/', suffix: '.json'},
    {prefix: './assets/i18n/assistance-projects/', suffix: '.json'},
    {prefix: './assets/i18n/guest/', suffix: '.json'}
  ]);
}
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AcceptanceComponent,
    DetailsDialogComponent,
    StepperComponent,
    RecoveryTaxRegimeStepComponent,
    ResidenceStepComponent,
    RegistryStepComponent,
    FamilyStepComponent,
    FamilyMemberFormComponent,
    ExemptionsStepComponent,
    DismissStepComponent,
    DetailsFormComponent,
    GuestAssistanceProjectsTableComponent,
    GuestAssistanceProjectDetailsComponent,
    AttendanceFormComponent,
	DismissFormComponent,
    MatCalendarInlineComponent
  ],
  imports: [
    AcceptanceRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
    NgxMaskModule.forRoot(maskConfig),
    MatSortModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AcceptanceModule {
}

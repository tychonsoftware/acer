import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AcceptanceComponent} from '@app/modules/acceptance/acceptance.component';
import {StepperComponent} from '@app/modules/acceptance/stepper/stepper.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AcceptanceComponent},
  {path: 'stepper', component: StepperComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcceptanceRoutingModule {
}

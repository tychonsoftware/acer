import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {DetailsDialogComponent} from '@app/modules/acceptance/details-dialog/details-dialog.component';
import {DialogService} from '@app/core/services/dialog.service';
import {TranslateService} from '@ngx-translate/core';
import {GuestTransition} from '@app/core/models';
import {GuestService} from '@app/core/http/guest/guest.service';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-acceptance',
  templateUrl: './acceptance.component.html',
  styleUrls: ['./acceptance.component.scss'],
})
export class AcceptanceComponent implements AfterViewInit, OnInit {

  // array dei valori da popolare
  patientsAcceptances: GuestTransition[] = [];
  patientsAcceptancesDeleted: GuestTransition[] = [];

  // Campi da visualizzare in tabella, tradotti nel file di traduzione.
  groupedColumns: string[] = ['groupedHeader'];
  displayedColumns: string[] = ['firstName', 'lastName', 'birthDate', 'landingPhone', 'actions'];
  dataSource: MatTableDataSource<GuestTransition> = new MatTableDataSource<GuestTransition>(this.patientsAcceptances);
  paginationSizes: number[] = [5, 10, 20];
  defaultPageSize = this.paginationSizes[1];
  isDeleted = false;

  @ViewChild(MatPaginator, {static: false}) paginator?: MatPaginator;
  @ViewChild(MatSort) sort: MatSort | undefined;

  constructor(
    private dialog: MatDialog,
    private route: Router,
    private guestService: GuestService,
    private dialogService: DialogService,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.findGuestsByStatus(1);
  }

  // Dopo che il componente è stato inizializzato
  ngAfterViewInit(): void {
    // Definisce il paginator se esiste
    if (this.paginator) {
      this.dataSource.paginator = this.paginator;
    }

    // Definisco sort se esiste
    if (this.sort) {
      this.dataSource.sort = this.sort;
      console.log('Sort should works!');
    }
  }

  // Applica il filtro di ricerca
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // Quando si preme ricovera su un paziente
  onHospitalize(element: GuestTransition | null): void {
    if (element) {
      this.guestService.getByUUID(element.uuid ? element.uuid : '').subscribe(
        result => {
          element = result;
          this.route.navigate(['/acceptance/stepper'], {state: {data: {element}}});
        });
    } else {
      this.route.navigate(['/acceptance/stepper'], {state: {data: {element}}});
    }
  }


  // Quando si clicca una riga si apre un pop up con i dati dell'utente
  openGuestDetailsDialog(element: GuestTransition): void {
    // Apro la modal di aliminazione di un elemento
    this.dialog.open(DetailsDialogComponent, {
      data: {element},
      width: '30%',
      // data: { name: element.name, surname: element.surname }
    });
  }

  // Dialog che si apre quando si cerca di eliminare un elemento dalla lista
  openGuestDeleteDialog(guestTransition: GuestTransition): void {
    const data = {
      title: this.translateService.instant('deleteDialog.title'),
      message: this.translateService.instant('deleteDialog.message', {value: `${guestTransition.firstName} ${guestTransition.lastName}`}),
      cancelText: this.translateService.instant('deleteDialog.cancelText'),
      confirmText: this.translateService.instant('deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => {
      if (confirmed && guestTransition.uuid) {
        this.guestService.delete(guestTransition.uuid).subscribe(_ => {

          // Se true prendo il valore selezionato lo rimuovo e riassegno il nuovo vettore di valori da visualizzare
          this.dataSource.data.splice(this.dataSource.data.indexOf(guestTransition), 1);

          // aggiungo all'array degli eliminati. forse fare una seconda tabella che compare quando viene premuto il tasto per gli eliminati
          this.patientsAcceptancesDeleted.push(guestTransition);

          // Fa l'aggiornamento del dataSource
          this.dataSource._updateChangeSubscription();
        });
      }
    });
  }

  // Quando aggiungo un nuovo paziente il lista
  addInList(): void {
    // Apro la modal di aliminazione di un elemento
    const dialog = this.dialog.open(DetailsDialogComponent, {
      data: {element: null},
      width: '30%',
      // data: { name: element.name, surname: element.surname }
    });

    // Alla chiusura in base a quello che mi restituisce, cancello o meno l'elemento
    dialog.afterClosed().subscribe((result) => {
      // Alla chiusura controllo se abbia restituito true/false
      if (result) {
        // Il codice fiscale e il luogo di nascita sono obbligatori domandare come fare, magari aggiungere la compilazione in questa form
        const patiet: GuestTransition = {
          firstName: result.value.firstName,
          lastName: result.value.lastName,
          fullName: result.value.firstName + ' ' + result.value.lastName,
          reversedFullName: result.value.lastName + ' ' + result.value.firstName,
          birthDate: result.value.birthDate,
          landingPhone: result.value.landingPhone,
          mobilePhone: result.value.mobilePhone,
          note: result.value.note,
          status: {
            id: 1,
            uuid: '01',
            description: 'in Attesa'
          }
        };

        // Chiamata alla rest per il salvataggio dell'utente in lista di attesa
        this.guestService.create(patiet).subscribe(guest => {
          // Se presente il risultato allora devo aggiungere il valore retituito nella lista di attesa
          this.dataSource.data.push(guest);
          this.dataSource.data.sort((guestFirst, guestSecond) => {
            const compare = guestFirst.lastName?.trim().localeCompare(guestSecond.lastName?.trim());
            return compare === 0 ? guestFirst.firstName?.trim().localeCompare(guestSecond.firstName?.trim()) : compare;
          });

          // Fa l'aggiornamento del dataSource della tabella
          this.dataSource._updateChangeSubscription();
        });
      }
    });
  }

  // Restituisce la data calcolata, in base all'anno di nascita preso in input
  getAge(birthDate: Date): number | string {
    if (birthDate) {
      const timeDiff = Math.abs(Date.now() - new Date(birthDate).getTime());
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    } else {
      return '';
    }
  }

  findDeleted(event: MatSlideToggleChange): void {
    this.isDeleted = event.checked;
    if (event.checked) {
      this.findGuestsByDeleted(true);
    } else {
      this.findGuestsByStatus(1);
    }
  }

  findGuestsByStatus(statusId: number): void {
    // all'avvio della sezione diaccettazione devo fare una chiamata alla rest per caricare i dati degli ospiti in lista di attesa
    this.guestService.getByStatus(statusId, true).subscribe(result => {
      result.sort((guestFirst, guestSecond) => {
        const compare = guestFirst.lastName?.trim().localeCompare(guestSecond.lastName?.trim());
        return compare === 0 ? guestFirst.firstName?.trim().localeCompare(guestSecond.firstName?.trim()) : compare;
      });
      this.patientsAcceptances = result;
      this.dataSource.data = this.patientsAcceptances;
      this.dataSource._updateChangeSubscription();
      if (this.paginator) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  findGuestsByDeleted(isDeleted: boolean): void {
    this.guestService.getDeleted(isDeleted).subscribe(result => {
      result.sort((guestFirst, guestSecond) => {
        const compare = guestFirst.lastName?.trim().localeCompare(guestSecond.lastName?.trim());
        return compare === 0 ? guestFirst.firstName?.trim().localeCompare(guestSecond.firstName?.trim()) : compare;
      });
      this.patientsAcceptances = result;
      this.dataSource.data = this.patientsAcceptances;
      this.dataSource._updateChangeSubscription();
      if (this.paginator) {
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  onRecoverGuest(guestTransition: GuestTransition): void {
    if (guestTransition.uuid) {
      this.guestService.recover(guestTransition.uuid).subscribe(() => this.findGuestsByDeleted(true));
    }
  }
}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GuestTransition} from '@app/core/models';

@Component({
  selector: 'app-details-dialog',
  templateUrl: './details-dialog.component.html',
  styleUrls: ['./details-dialog.component.scss']
})
export class DetailsDialogComponent implements OnInit {

  patientAcceptance: GuestTransition | undefined;
  title = 'Nuovo Paziente';

  patientAcceptanceForm: FormGroup;
  enabledButton = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<GuestTransition>
  ) {

    this.patientAcceptanceForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthDate: [''],
      landingPhone: [''],
      mobilePhone: [''],
      note: ['']
    });
  }

  ngOnInit(): void {
    // Se quando apro questa modal gli ho passato un valore da visualizzare allora disabilito il bottone di salvataggio
    if (this.data && this.data.element !== null) {
      this.enabledButton = false;
      this.patientAcceptance = this.data.element;
      this.title = 'Dettagli Paziente';
    }
  }

  onSave(): void {
    if (this.patientAcceptanceForm.valid) {
      this.dialogRef.close(this.patientAcceptanceForm);
    }
  }
}

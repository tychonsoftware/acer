import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GuestComponent} from '@app/modules/guest/guest.component';
import {StepperComponent} from '@app/modules/acceptance/stepper/stepper.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: GuestComponent},
  {path: 'stepper', component: StepperComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestRoutingModule {
}

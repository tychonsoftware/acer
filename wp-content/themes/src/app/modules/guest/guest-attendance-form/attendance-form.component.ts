import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Attendance} from '@app/core/models/attendance/attendance.model';
import {AttendanceStatusService} from '@app/core/http/attendance/attendance-status.service';
import {AttendanceReasonService} from '@app/core/http/attendance/attendance-reason.service';
import {Observable} from 'rxjs';
import {AttendanceStatus} from '@app/core/models/attendance/attendance-status.model';
import {AttendanceReason} from '@app/core/models/attendance/attendance-reason.model';
import {compareById} from '@app/shared/helpers/comparators';
import {GuestService} from '@app/core/http';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';

@Component({
  selector: 'app-attendance-form',
  templateUrl: './attendance-form.component.html',
  styleUrls: ['./attendance-form.component.scss']
})
export class AttendanceFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    actualExitDate: [null, this.actualDateValidator()],
    plannedExitDate: [null, Validators.required],
    actualReturnDate: [null, this.actualDateValidator()],
    plannedReturnDate: [null, Validators.required],
    reason: [null, Validators.required],
    note: [null, Validators.required],
    status: [null],
    guest: [null]
  });
  attendanceStatues: AttendanceStatus[] = [];
  attendanceReasons$: Observable<AttendanceReason[]> | undefined;
  compareById = compareById;
  selectedReason: AttendanceReason | undefined;
  guestStatusId: number | undefined;
  dataFromCalendar = false;
  isEditable = false;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    private attendanceStatusService: AttendanceStatusService,
    private attendanceReasonService: AttendanceReasonService,
    private guestService: GuestService,
    private userSecurityService: UserService
  ) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
    this.form.setValidators(this.comparisonValidator());
    this.guestStatusId = -1;
    if (data.editFormData && !data.editFormData.fromCalendar) {
      this.isEdit = true;
      this.updateForm(data.editFormData);
      this.selectedReason = data.editFormData.attendanceReason;
      if (data.isGuestExit) {
        if (data.editFormData.plannedExitDate != null) {
          this.form.get('plannedExitDate')?.disable();
        } else {
          this.form.get('plannedExitDate')?.enable();
        }
        if (data.editFormData.guest != null && data.editFormData.guest.status != null && data.editFormData.guest.status.id !== 2) {
          this.form.get('actualExitDate')?.disable();
        } else {
          this.form.get('actualExitDate')?.enable();
        }
        if (data.editFormData.plannedReturnDate != null) {
          this.form.get('plannedReturnDate')?.disable();
        } else {
          this.form.get('plannedReturnDate')?.enable();
        }
      } else {
        this.checkDates();
      }
    } else {
      if (data.editFormData && data.editFormData.fromCalendar) {
        this.dataFromCalendar = true;
        this.updateForm(data.editFormData);
      }
      this.attendanceStatusService.getAllAsObservable().subscribe(res => {
        this.attendanceStatues = res;
        this.form.get('status')?.patchValue(this.attendanceStatues.find(s => s.id === 1));
      });
      if (data.guest != null) {
        this.form.get('guest')?.patchValue(data.guest);
      }
    }
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (!user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_ATTENDANCE)) {
      this.form.disable();
    }
  }

  comparisonValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const actualExitDate = control.get('actualExitDate');
      const actualReturnDate = control.get('actualReturnDate');
      const plannedExitDate = control.get('plannedExitDate');
      const plannedReturnDate = control.get('plannedReturnDate');
      if (this.dataFromCalendar || (plannedExitDate?.disabled && plannedReturnDate?.disabled)) {
        if (actualExitDate?.value == null) {
          actualExitDate?.setErrors({required: true});
          return {required: true};
        }
        if (actualExitDate?.value != null && actualReturnDate?.value != null) {
          plannedExitDate?.setErrors(null);
          plannedReturnDate?.setErrors(null);
          return {};
        }
      }
      if (plannedExitDate?.value == null) {
        plannedExitDate?.setErrors({required: true});
        return {required: true};
      } else if (plannedReturnDate?.value != null && !plannedReturnDate.disabled) {
        const plannedExitDateTimeStamp = new Date(plannedExitDate?.value).getTime();
        const plannedReturnDateTimeStamp = new Date(plannedReturnDate?.value).getTime();
        if (plannedReturnDateTimeStamp < plannedExitDateTimeStamp) {
          plannedExitDate.setErrors({scheduleEalierInvalid: true});
          return {scheduleEalierInvalid: true};
        } else if (!this.dataFromCalendar && (plannedReturnDateTimeStamp - plannedExitDateTimeStamp) < 0) {
          plannedExitDate.setErrors({oneDayLongInvalid: true});
          return {oneDayLongInvalid: true};
        }
      } else if (plannedExitDate.disabled && plannedReturnDate?.disabled && actualReturnDate?.value != null) {
        const actualExitDateTimeStamp = new Date(actualExitDate?.value).getTime();
        const actualReturnDateTimeStamp = new Date(actualReturnDate?.value).getTime();
        if (actualReturnDateTimeStamp < actualExitDateTimeStamp) {
          actualExitDate?.setErrors({scheduleEalierInvalid: true});
          return {scheduleEalierInvalid: true};
        } else if ((actualReturnDateTimeStamp - actualExitDateTimeStamp) < 0) {
          plannedExitDate.setErrors({oneDayLongInvalid: true});
          return {oneDayLongInvalid: true};
        }
      }
      if (!this.dataFromCalendar) {
        const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
        if (this.data && this.data.editFormData && this.data.editFormData.plannedExitDate != null && this.data.editFormData.plannedReturnDate != null) {
          const plannedExitDateTimeStamp = new Date(plannedExitDate?.value).getTime();
          const plannedReturnDateTimeStamp = new Date(plannedReturnDate?.value).getTime();
          if (Math.floor(plannedExitDateTimeStamp / 86400000) < Math.floor(yesterday.getTime() / 86400000) &&
            Math.floor(plannedReturnDateTimeStamp / 86400000) < Math.floor(yesterday.getTime() / 86400000)) {
            if (actualExitDate?.value == null) {
              actualExitDate?.setErrors({required: true});
              return {required: true};
            }
            if (actualReturnDate?.value == null) {
              actualReturnDate?.setErrors({required: true});
              return {required: true};
            }
          }
          if (this.data && !this.data.isGuestExit) {
            if (actualExitDate?.value != null && actualReturnDate?.value == null) {
              actualReturnDate?.setErrors({required: true});
              return {required: true};
            }
          }
        }
      }
      plannedExitDate?.setErrors(null);
      return {};
    };
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
    this.attendanceReasons$ = this.attendanceReasonService.getAllAsObservable();
    this.attendanceStatusService.getAllAsObservable().subscribe(res => {
      this.attendanceStatues = res;
    });
    if (!this.dataFromCalendar && this.data.guest.uuid != null) {
      this.guestService.getByUUID(this.data.guest.uuid).subscribe(res => {
        this.guestStatusId = res.status != null ? res.status.id : -1;
        if (this.guestStatusId != null && this.guestStatusId === 2 && this.form.get('plannedExitDate')?.disabled) {
          this.form.get('actualReturnDate')?.disable();
        } else {
          this.form.get('actualReturnDate')?.enable();
        }
        if (this.guestStatusId != null && this.guestStatusId !== 2) {
          this.form.get('actualExitDate')?.disable();
        } else {
          this.form.get('actualExitDate')?.enable();
        }
        if (!this.data.isGuestExit) {
          this.checkDates();
        }
      });
    }
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  checkDates(): void {
    const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
    if (this.data && this.data.editFormData && this.data.editFormData.plannedExitDate != null && this.data.editFormData.plannedReturnDate != null) {
      const plannedExitDate = new Date(this.data.editFormData.plannedExitDate);
      const plannedReturnDate = new Date(this.data.editFormData.plannedReturnDate);
      if (Math.floor(plannedExitDate.getTime() / 86400000) < Math.floor(yesterday.getTime() / 86400000) &&
        Math.floor(plannedReturnDate.getTime() / 86400000) < Math.floor(yesterday.getTime() / 86400000)) {
        this.form.get('actualExitDate')?.enable();
        this.form.get('actualReturnDate')?.enable();
      }
    }
  }

  updateForm(data: Attendance): void {
    this.form?.patchValue({
      ...data,
      // Dates
      actualExitDate: data.actualExitDate != null ? new Date(data.actualExitDate) : null,
      plannedExitDate: data.plannedExitDate != null ? new Date(data.plannedExitDate) : null,
      actualReturnDate: data.actualReturnDate != null ? new Date(data.actualReturnDate) : null,
      plannedReturnDate: data.plannedReturnDate != null ? new Date(data.plannedReturnDate) : null
    });
  }

  onFormSubmitWithoutDate(withRowValue?: boolean): void {
    console.log('onFormSubmitWithoutDate', this.form);
    if (this.data.isGuestExit) {
      const formValue = (withRowValue ? this.form.getRawValue() : this.form.value);
      if (formValue.guest != null) {
        if (this.guestStatusId === 2) {
          this.form.get('status')?.patchValue(this.attendanceStatues.find(s => s.id === 2));
        } else if (this.guestStatusId === 4) {
          this.form.get('status')?.patchValue(this.attendanceStatues.find(s => s.id === 3));
        }
      }
    }
    if (this.dataFromCalendar) {
      this.form.get('status')?.patchValue(this.attendanceStatues.find(s => s.id === 3));
    } else if (this.data && !this.data.isGuestExit) {
      if (this.form.get('actualExitDate')?.value != null) {
        this.form.get('status')?.patchValue(this.attendanceStatues.find(s => s.id === 3));
      }
    }
    super.onFormSubmitWithoutDate(withRowValue);
  }

  actualDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const today = new Date();
      if (!(control && control.value)) {
        // if there's no control or no value, that's ok
        return null;
      }
      return control.value > today ? {invalidDate: ''} : null;
    };
  }
}

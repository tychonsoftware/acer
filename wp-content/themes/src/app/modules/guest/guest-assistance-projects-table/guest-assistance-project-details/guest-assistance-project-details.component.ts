import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssistanceProject} from '@app/core/models';
import {RowActionIcons} from '@app/shared/components/generic-table/generic-table.component';
import {FileService} from '@app/core/http';
import {base64ToDownloadLink, getFileNameFromPath} from '@app/shared/helpers/utility';

@Component({
  selector: 'app-guest-assistance-project-details',
  templateUrl: './guest-assistance-project-details.component.html',
  styleUrls: ['./guest-assistance-project-details.component.scss']
})
export class GuestAssistanceProjectDetailsComponent implements OnInit {

  // Input
  @Input() assistanceProject: AssistanceProject | undefined;
  @Input() rowActionIcons: RowActionIcons | undefined;

  @Input() isEditable = true;

  // Output
  @Output() ngDownloadUVI: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngDelete: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fileService: FileService) {
  }

  ngOnInit(): void {
  }
  
  emitDownloadAction(row: any): void {
    this.ngDownloadUVI.emit(row);
  }

  emitEditAction(row: any): void {
    this.ngEdit.emit(row);
  }

  emitDeleteAction(row: any): void {
    this.ngDelete.emit(row);
  }

  onDownloadClick(assistanceProject: any): void {
    const path = `${assistanceProject?.file.staticPath?.path}/${assistanceProject?.file?.dynamicPath}`;
    // const fileName = assistanceProject?.fileName ? assistanceProject.fileName : getFileNameFromPath(assistanceProject?.file?.dynamicPath);
    const fileName = getFileNameFromPath(assistanceProject?.file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

}

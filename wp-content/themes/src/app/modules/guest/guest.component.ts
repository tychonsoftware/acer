import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {GuestTransition, RecoveryTaxRegime, RecoveryTaxRegimeType} from '@app/core/models';
import {GuestService} from '@app/core/http/guest/guest.service';
import {OrganizationBuildingsService} from '@app/core/http/organization/organization-buildings.service';
import {formatDate} from '@angular/common';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Functionalities} from '@app/core/constants/Functionalities';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';
import {RecoveryTaxRegimeTypeService} from '@app/core/http';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss'],
})
export class GuestComponent implements AfterViewInit, OnInit {

  // array dei valori da popolare
  patientsHospitalized: GuestTransition[] = [];
  dataSource: MatTableDataSource<GuestTransition> = new MatTableDataSource<GuestTransition>(this.patientsHospitalized);
  cardClass = 'img-label';
  cardAbsentClass = 'abs-img-label';
  @ViewChild(MatSort) sort: MatSort | undefined;
  // controllare lo stato del paziente assente
  esportazioneStato: boolean;
  isAccessible = false;

  buildings: (OrganizationBuilding | undefined)[] = [];
  buildingSelected: OrganizationBuilding | undefined;

  recoveryTaxRegimeTypes: (RecoveryTaxRegimeType | undefined)[] = [];
  recoveryTaxRegimeTypeSelected: RecoveryTaxRegimeType | undefined;

  constructor(
    private dialog: MatDialog,
    private route: Router,
    private guestService: GuestService,
    private userSecurityService: UserService,
    private organizationBuildingsService: OrganizationBuildingsService,
    private recoveryTaxRegimeTypeService: RecoveryTaxRegimeTypeService
  ) {
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
    this.esportazioneStato = false;
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    console.log(user);
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_DETAIL
      || functionality.functionality === Functionalities.GUEST_DETAIL_READ)) {
      this.isAccessible = true;
    }
  }

  ngOnInit(): void {
    this._findBuildings();
    this._findRecoveryTaxRegimeTypes();
    this._setGuests();
  }

  // Dopo che il componente è stato inizializzato
  ngAfterViewInit(): void {
    // Definisco sort se esiste
    if (this.sort) {
      this.dataSource.sort = this.sort;
    }
  }

  // Applica il filtro di ricerca
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    this.dataSource.filteredData = JSON.parse(JSON.stringify(this.dataSource.data));
    this.dataSource.filteredData = this.dataSource.filteredData.filter((record: any) => {
      let isExist = false;
      Object.keys(record).forEach((r) => {
        if (r !== 'picContent' && r !== 'birthPlace' && record[r] != null) {
          const strRecord = record[r]?.toString()?.trim()?.toLowerCase();
          if (strRecord?.includes(filterValue.trim().toLowerCase())) {
            isExist = true;
          }
        }
      });
      if (isExist) {
        return record;
      }
    });
  }

  // Quando si fa clic su una riga, i dettagli si aprono con i dati dell'guest
  openGuestDetails(element: GuestTransition): void {
    if (element) {
      this.guestService.getByUUID(element.uuid ? element.uuid : '').subscribe(
        result => {
          element = result;
          this.route.navigate(['/guest/stepper'], {state: {data: {element}}});
        });
    } else {
      this.route.navigate(['/guest/stepper'], {state: {data: {element}}});
    }
  }

  // Restituisce la data calcolata, in base all'anno di nascita preso in input
  getAge(element: GuestTransition): number {
    if (element.birthDate !== undefined) {
      const timeDiff = Math.abs(Date.now() - new Date(element.birthDate as Date).getTime());
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
    return 0;
  }


  esporta(): void {
    this.guestService.exportGuests().subscribe((response: any) => {
      const blob = new Blob([response], {type: 'application/vnd.ms-excel'});
      const nomeFile = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'it-IT') + '_ospiti.xls';
      const a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = nomeFile;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    });
  }

  private _findBuildings(): void {
    this.organizationBuildingsService.findAll().subscribe(res => {
      this.buildings = [];
      this.buildings.push(undefined);
      if (res && res.length > 0) {
        this.buildings = this.buildings.concat(res);
        this.buildingSelected = undefined;
      }
    });
  }

  private _findRecoveryTaxRegimeTypes(): void {
    if (this.buildingSelected && this.buildingSelected.recoveryTaxRegimeTypes != null) {
      this.recoveryTaxRegimeTypes = [];
      this.recoveryTaxRegimeTypes.push(undefined);
      this.recoveryTaxRegimeTypes = this.recoveryTaxRegimeTypes.concat(this.buildingSelected.recoveryTaxRegimeTypes);
    } else {
      this.recoveryTaxRegimeTypeService.getAll().subscribe(res => {
        this.recoveryTaxRegimeTypes = [];
        this.recoveryTaxRegimeTypes.push(undefined);
        if (res && res.length > 0) {
          this.recoveryTaxRegimeTypes = this.recoveryTaxRegimeTypes.concat(res);
          this.recoveryTaxRegimeTypeSelected = undefined;
        }
      });
    }


  }


  onChange(): void {
    this._findRecoveryTaxRegimeTypes();
    this._setGuests();
  }

  private _setGuests(): void {
    this.patientsHospitalized = [];
    this.guestService.getByFilter(2, true, this.buildingSelected?.id, this.recoveryTaxRegimeTypeSelected?.id).subscribe(result => {
      // mostra il pulsante di esportazione se l'guest è presente con lo stato 4
      this.guestService.getByFilter(4, true, this.buildingSelected?.id, this.recoveryTaxRegimeTypeSelected?.id).subscribe(absent => {
        for (let i = 0; i <= result.length; i++) {
          if (result[i] !== undefined) {
            this.patientsHospitalized.push(result[i]);
          }
        }
        if (absent.length > 0) {
          for (let i = 0; i <= absent.length; i++) {
            if (absent[i] !== undefined) {
              this.patientsHospitalized.push(absent[i]);
            }
          }
          this.esportazioneStato = true;
        } else {
          this.esportazioneStato = false;
        }
        this.patientsHospitalized.sort((a, b) => {
          const user1 = (a.lastName + a.firstName).trim().toLowerCase();
          const user2 = (b.lastName + b.firstName).trim().toLowerCase();
          return user1 > user2 ? 1 : -1;
        });
        this.dataSource.data = this.patientsHospitalized;
        this.dataSource._updateChangeSubscription();
      });
    });
  }

}

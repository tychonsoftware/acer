import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';

import {GuestService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {Observable} from 'rxjs';
import {GuestTransition} from '@app/core/models';

@Component({
  selector: 'app-assistance-guest-project-form',
  templateUrl: './assistance-guest-project-form.component.html',
  styleUrls: ['./assistance-guest-project-form.component.scss']
})
export class AssistanceGuestProjectFormComponent extends FormContentDirective implements OnInit, OnChanges {

  public assistanceGuestProjectsTableColumns: TableColumn[] = [
    {
      name: ' ',
      dataKey: 'image',
      position: 'center',
      isSortable: false
    },
    {
      name: 'Nome',
      dataKey: 'firstName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Cognome',
      dataKey: 'lastName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true,
    },
    {
      name: 'Comune',
      dataKey: 'birthPlace',
      position: 'center',
      isSortable: true
    },
    {
      name: 'ETÀ',
      dataKey: 'birthDate',
      position: 'center',
      isSortable: true
    }
  ];

  guests$: Observable<GuestTransition[]> | undefined;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    private guestService: GuestService,
    public dialogRef: MatDialogRef<AssistanceGuestProjectFormComponent>
  ) {
    // Calling super to init the extended class
    super();
  }

  ngOnInit(): void {
    this.guests$ = this.guestService.getByStatus(2, false);
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
  }

  onRowSelected(guest: GuestTransition): void {
    this.dialogRef.close(guest);
  }
}


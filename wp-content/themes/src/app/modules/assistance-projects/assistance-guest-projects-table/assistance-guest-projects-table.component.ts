import {Component, EventEmitter, Input, Output} from '@angular/core';
import {GuestTransition} from '@app/core/models';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-assistance-guest-projects-table',
  templateUrl: './assistance-guest-projects-table.component.html',
  styleUrls: ['./assistance-guest-projects-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AssistanceGuestProjectsTableComponent extends GenericTableComponent<GuestTransition> {

  @Input() set tableData(data: Observable<GuestTransition[]> | GuestTransition[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
      if (this.matPaginator) {
        this.tableDataSource.paginator = this.matPaginator;
      }
    }
  }

  @Output() ngRowSelected: EventEmitter<GuestTransition> = new EventEmitter();

  constructor() {
    super();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: GuestTransition[]): void {
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }

  getAge(birthDate: Date): number {
    const timeDiff = Math.abs(Date.now() - new Date(birthDate).getTime());
    return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    if (this.tableDataSource.paginator) {
      this.tableDataSource.paginator.firstPage();
    }
  }

  rowSelected(element: GuestTransition): void {
    this.ngRowSelected.emit(element);
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssistanceProject} from '@app/core/models';
import {RowActionIcons} from '@app/shared/components/generic-table/generic-table.component';
import {getLastMonthPeriod} from '@app/shared/helpers/utility';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';

@Component({
  selector: 'app-assistance-project-details',
  templateUrl: './assistance-project-details.component.html',
  styleUrls: ['./assistance-project-details.component.scss']
})
export class AssistanceProjectDetailsComponent implements OnInit {

  // Input
  @Input() assistanceProject: AssistanceProject | undefined;
  @Input() rowActionIcons: RowActionIcons | undefined;
  @Input() period: string | undefined;

  // Output
  @Output() ngDownloadUVI: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngDelete: EventEmitter<any> = new EventEmitter<any>();
  anaFile: string | undefined;

  isEditable = false;

  constructor(userSecurityService: UserService) {
    setTimeout(() => {
      const user = userSecurityService.getUserSecurity();
      if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT)) {
        this.isEditable = true;
      }
    }, environment.TIME_TO_TIMEOUT);
  }

  ngOnInit(): void {
	this.assistanceProject?.filesH.forEach(fileH => {
		if (this.period == null && fileH.status?.description == 'Consolidato' && fileH.period == getLastMonthPeriod()) {
			this.anaFile = this.getFileNameFromPathWithoutExtension(fileH.anaFile?.dynamicPath);
		}
		if (this.period != null && fileH.status?.description == 'Consolidato' && fileH.period == this.period) {
			this.anaFile = this.getFileNameFromPathWithoutExtension(fileH.anaFile?.dynamicPath);
		}
	});
  }

  emitDownloadAction(row: any): void {
    this.ngDownloadUVI.emit(row);
  }

  emitEditAction(row: any): void {
    this.ngEdit.emit(row);
  }

  emitDeleteAction(row: any): void {
    this.ngDelete.emit(row);
  }

  getFileNameFromPathWithoutExtension(fullPath: string): string {
  	const fileName = fullPath.replace(/^.*[\\\/]/, '');
	  return fileName.substring(0, fileName.indexOf('_') + 4);
  }

}

import {Component, Input} from '@angular/core';
import {AssistanceProject} from '@app/core/models';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {GuestAssistanceProjects} from '@app/core/models/assistance-projects/guest-assistance-projects.model';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';
import {OrderType, sortObjectByDate} from '@app/shared/helpers/utility';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';
import {AssistanceProjectService} from '@app/core/http';
import {getPeriod} from '@app/shared/helpers/utility';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-assistance-projects-table',
  templateUrl: './assistance-projects-table.component.html',
  styleUrls: ['./assistance-projects-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AssistanceProjectsTableComponent extends GenericTableComponent<GuestAssistanceProjects> {

  months: Month[] = [
    {id: 1, name: 'Gennaio', numberOfDay: 31},
    {id: 2, name: 'Febbraio', numberOfDay: 29},
    {id: 3, name: 'Marzo', numberOfDay: 31},
    {id: 4, name: 'Aprile', numberOfDay: 30},
    {id: 5, name: 'Maggio', numberOfDay: 31},
    {id: 6, name: 'Giugno', numberOfDay: 30},
    {id: 7, name: 'Luglio', numberOfDay: 31},
    {id: 8, name: 'Agosto', numberOfDay: 31},
    {id: 9, name: 'Settembre', numberOfDay: 30},
    {id: 10, name: 'Ottobre', numberOfDay: 31},
    {id: 11, name: 'Novembre', numberOfDay: 30},
    {id: 12, name: 'Dicembre', numberOfDay: 31},
  ];
  monthsToView: Month[] = [];
  monthSelected: Month | undefined;

  years: string[] = [];
  yearSelected: string | undefined;
  period: string | undefined;

  @Input() set tableData(data: Observable<AssistanceProject[]> | AssistanceProject[] | undefined) {
    this.findInitDate();
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  expandedElement?: AssistanceProject;
  isEditable = false;

  constructor(userSecurityService: UserService,
              private assistanceService: AssistanceProjectService) {
    super();
    setTimeout(() => this.checkSecurity(userSecurityService), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(userSecurityService: UserService): void {
    const user = userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT)) {
      this.isEditable = true;
    }
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: AssistanceProject[]): void {
    const guestAssistanceProjects = this._getGuestAssistanceProjects(data);
    this.tableDataSource = new MatTableDataSource(guestAssistanceProjects);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }

  }

  // Ordina gli elementi nella tabella, facendo un ordinamento per cognome e nome
  sortAssistanceProjectsByGuest(data: AssistanceProject[]): void {

    // Effettuo ordinamento per cognome e nome
    data.sort((a, b) => {
      const user1 = (a.guestMinimal.lastName + a.guestMinimal.firstName).trim().toLowerCase();
      const user2 = (b.guestMinimal.lastName + b.guestMinimal.firstName).trim().toLowerCase();
      return user1 > user2 ? 1 : -1;
    });
  }

  private _getGuestAssistanceProjects(data: AssistanceProject[]): GuestAssistanceProjects[] {
    this.sortAssistanceProjectsByGuest(data);
    const guestAssistanceProjectsMap = data.reduce((obj: any, item) => {
      const uuid = item.guestMinimal?.uuid;
      if (uuid) {
        obj[uuid] ? obj[uuid].assistanceProjects.push(item) : obj[uuid] = {
          firstName: item.guestMinimal?.firstName,
          lastName: item.guestMinimal?.lastName,
          fiscalCode: item.guestMinimal?.fiscalCode,
          birthDate: item.guestMinimal?.birthDate,
          assistanceProjects: [item]
        };
      }
      return obj;
    }, {});
    return Object.values(guestAssistanceProjectsMap);
  }

  sortByStartTreatmentDate(assistanceProjects: AssistanceProject[]): AssistanceProject[] {
    return sortObjectByDate(assistanceProjects, 'treatmentStrDate', OrderType.desc);
  }

  // Chiamata quando, viene cambiato l'anno o il mese
  changePeriod(): void {
    this.monthsToView = [];
    this.findMonthToView();
    const emptyList: AssistanceProject[] = [];
    if (this.monthSelected && this.yearSelected) {
      // Creo il campo period che assegneò agli oggetti recuperati mediante la chiamta abyPeriod
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      this.period = query;
      // Faccio la chiamata per popolare la tabella con i proggetti assistenziali
      this.assistanceService.getByPeriod(query).subscribe(element => {
        if (element.assistanceProjects?.length > 0) {
          this.setTableDataSource(element.assistanceProjects);
        } else {
          this.setTableDataSource(emptyList);
        }
      });

    }
  }

  private findInitDate(): void {
    const today = new Date();
    for (let year = 2021; year <= today.getFullYear(); year++) {
      this.years.push(year.toString());
    }
    this.yearSelected = today.getFullYear().toString();
    this.monthSelected = today.getMonth() > 0 ? this.months.find(m => m.id === (today.getMonth() + 1)) : this.months.find(m => m.id === 1);
    this.findMonthToView();
  }

  private findMonthToView(): void {
    if (new Date().getFullYear().toString() === this.yearSelected) {
      this.months.forEach(m => {
        if (m.id <= new Date().getMonth() + 1) {
          console.log(m);
          this.monthsToView.push(m);
        }
      });
    } else {
      this.monthsToView = this.months;
    }
  }

  // Serve per comparare le stringhe, nella select
  compare(c1: { id: number }, c2: { id: number }): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

}

import {Component, OnInit} from '@angular/core';
import {AssistanceProjectService, FileService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from '@app/core/services/dialog.service';
import {MatDialogConfig} from '@angular/material/dialog';
import {AssistanceProject} from '@app/core/models/assistance-projects/assistance-project.model';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AssistanceProjectFormComponent} from '@app/modules/assistance-projects/assistance-project-form/assistance-project-form.component';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {base64ToDownloadLink, getFileNameFromPath, getCurrentMonthPeriod, mapObjectDateKeyToDate} from '@app/shared/helpers/utility';
import { AssistanceGuestProjectFormComponent } from './assistance-guest-project-form/assistance-guest-project-form.component';

@Component({
  selector: 'app-assistance-projects',
  templateUrl: './assistance-projects.component.html',
  styleUrls: ['./assistance-projects.component.scss']
})
export class AssistanceProjectsComponent implements OnInit {
  // TODO: generare mediante translate service
  public assistanceProjectsTableColumns: TableColumn[] = [
    {
      name: 'Nome Ospite',
      dataKey: 'firstName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Cognome Ospite',
      dataKey: 'lastName',
      position: 'center',
      isSortable: true
    },
    {
      name: 'Data di Nascita',
      dataKey: 'birthDate',
      position: 'center',
      isSortable: true,
      isDate: true
    },
    {
      name: 'Codice fiscale',
      dataKey: 'fiscalCode',
      position: 'center',
      isSortable: true
    },
    {
      name: ' ',
      dataKey: 'expand',
      position: 'center',
      isSortable: false
    }
  ];

  assistanceProjects$: Observable<AssistanceProject[]> | undefined;
  guest: any;

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    private fileService: FileService,
    public assistanceProjectsService: AssistanceProjectService) {
  }

  ngOnInit(): void {
    this._getAssistanceProjectsByPeriod();
  }

  onSort(s: any): void {
    console.log(s);
  }

  /**
   * Open the Assistance Project form dialog.
   * @param assistanceProject Data of the Assistance Project to edit.
   */
  openFormDialog(assistanceProject?: AssistanceProject): void {
    if (assistanceProject && assistanceProject.id) {
      // Getting the contract data from the API
      this._openAssistanceProjectFormDialog(assistanceProject);
    } else {
      this._openAssistanceProjectFormDialog();
    }
  }

  /**
   * Open the Assistance Ospite Project form dialog.
   * @param assistanceProject Data of the Assistance Project to edit.
   */
  openGuestFormDialog(assistanceProject?: AssistanceProject): void {
    if (assistanceProject && assistanceProject.id) {
      // Getting the contract data from the API
      this._openAssistanceGuestProjectFormDialog(assistanceProject);
    } else {
      this._openAssistanceGuestProjectFormDialog();
    }
  }

  /**
   * Open the delete Assistance Project dialog.
   * @param assistanceProject The Assistance Project to delete.
   */
  onDeleteClick(assistanceProject: AssistanceProject): void {
    const data = {
      title: this.translateService.instant('assistanceProjects.deleteDialog.title'),
      message: this.translateService.instant('assistanceProjects.deleteDialog.message', {value: assistanceProject.authNumber}),
      cancelText: this.translateService.instant('assistanceProjects.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteContract(assistanceProject));
  }

  onDownloadClick(assistanceProject: AssistanceProject): void {
    const path = `${assistanceProject?.file.staticPath?.path}/${assistanceProject?.file?.dynamicPath}`;
    // const fileName = assistanceProject?.fileName ? assistanceProject.fileName : getFileNameFromPath(assistanceProject?.file?.dynamicPath);
    const fileName = getFileNameFromPath(assistanceProject?.file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  private _openAssistanceProjectFormDialog(editedAssistanceProject?: AssistanceProject): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedAssistanceProject,
      title: this.translateService.instant(`assistanceProjects.formDialog.${editedAssistanceProject?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('assistanceProjects.formDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.formDialog.confirmText'),
      patientUUID: this.guest?.uuid
    };

    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view-btns';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AssistanceProjectFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => this._saveAssistanceProject(ap as AssistanceProject));
  }

  private _openAssistanceGuestProjectFormDialog(editedAssistanceProject?: AssistanceProject): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedAssistanceProject,
      title: this.translateService.instant(`assistanceProjects.formDialog.${editedAssistanceProject?.id ? 'editTitle' : 'addTitle'}`),

    };
    const config = {... this.dialogConfig};
    config.panelClass = 'ap-full-view';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AssistanceGuestProjectFormComponent, data, config);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => {
      if (ap) {
        this.guest = ap;
        this._openAssistanceProjectFormDialog();
      }
    });
  }

  /**
   * Save the Assistance Project.
   * @param assistanceProject The Assistance Project to save
   * @private
   */
  private _saveAssistanceProject(assistanceProject?: AssistanceProject): void {
    if (assistanceProject) {
      delete (assistanceProject as any)?.aslPercentEuro;
      delete (assistanceProject as any)?.cityPercentEuro;
      delete (assistanceProject as any)?.userPercentEuro;
      let result: Observable<AssistanceProject>;
      if (assistanceProject.id) { // edit case
        result = this.assistanceProjectsService.updateAndGetObservable(assistanceProject);
      } else {
        result = this.assistanceProjectsService.createAndGetObservable(assistanceProject);
      }

      result.subscribe(_ => this._getAssistanceProjectsByPeriod());
    }
  }

  /**
   * Delete Assistance Project
   * @param assistanceProject The Assistance Project to delete.
   * @private
   */
  private _deleteContract(assistanceProject: AssistanceProject): void {
    if (assistanceProject.id) {
      this.assistanceProjectsService.delete(assistanceProject.id);
      this._getAssistanceProjectsByPeriod();
    }
  }

  private _getAssistanceProjectsByPeriod(): void {
    this.assistanceProjects$ = this.assistanceProjectsService.getByPeriod(getCurrentMonthPeriod())
      .pipe(
        tap(res => {
          console.log('GET FROM byPeriod');
          console.log(res);
        }),
        map(res => res.assistanceProjects.map(a => mapObjectDateKeyToDate(a))));
  }
}

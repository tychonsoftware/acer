import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AssistanceProjectsComponent} from './assistance-projects.component';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DATE_FORMATS} from '@angular/material/core';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {AssistanceProjectsRoutingModule} from '@app/modules/assistance-projects/assistance-projects-routing.module';
import {AssistanceProjectFormComponent} from './assistance-project-form/assistance-project-form.component';
import {AssistanceProjectsTableComponent} from './assistance-projects-table/assistance-projects-table.component';
import { AssistanceProjectDetailsComponent } from './assistance-projects-table/assistance-project-details/assistance-project-details.component';
import {DD_MM_YYYY_DATE_FORMAT} from '@app/shared/helpers/format-datepicker';
import { AssistanceProjectsHistoryComponent } from './assistance-projects-history/assistance-projects-history.component';
import {AssistanceProjectsHistoryTableComponent} from '@app/modules/assistance-projects/assistance-projects-history/assistance-projects-history-table/assistance-projects-history-table.component';
import { AssistanceGuestProjectFormComponent } from './assistance-guest-project-form/assistance-guest-project-form.component';
import { AssistanceGuestProjectsTableComponent } from './assistance-guest-projects-table/assistance-guest-projects-table.component';
import { AssistanceProjectHistoryDetailsComponent } from './assistance-projects-history/assistance-projects-history-table/assistance-project-history-details/assistance-project-history-details.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/assistance-projects/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    AssistanceProjectsComponent,
    AssistanceProjectFormComponent,
    AssistanceProjectsTableComponent,
    AssistanceProjectDetailsComponent,
    AssistanceProjectHistoryDetailsComponent,
    AssistanceProjectsHistoryComponent,
    AssistanceProjectsHistoryTableComponent,
    AssistanceGuestProjectFormComponent,
    AssistanceGuestProjectsTableComponent
  ],
  imports: [
    AssistanceProjectsRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
  ],
  providers: [
    // Per il datapicker
  {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_DATE_FORMAT},
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AssistanceProjectsModule {
}

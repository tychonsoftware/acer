import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {getNumberOfDaysFromDates, getStartDateFromEndAndDays, getEndDateFromStartAndDays} from '@app/shared/helpers/utility.ts';
import {
  AssistanceProjectStatusService,
  DiagnosisIcd9Service,
  DisabilityService, FileService,
  GuestService,
  PerformanceChargeService,
  PerformanceTypeService,
} from '@app/core/http';
import {Observable} from 'rxjs';
import {
  Asl,
  AssistanceProject,
  AssistanceProjectStatus,
  City,
  DiagnosisICD9,
  Disability,
  GuestMinimalTransition,
  PerformanceCharge,
  PerformanceType,
  Region,
} from '@app/core/models';

import {compareById, compareGuests} from '@app/shared/helpers/comparators';
import {openBlobFromBase64, base64ToDownloadLink} from '@app/shared/helpers/utility';
import {Functionalities} from '@app/core/constants/Functionalities';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {Area} from '@app/core/models/shared/area.model';
import {District} from '@app/core/models/shared/district.model';

@Component({
  selector: 'app-assistance-project-form',
  templateUrl: './assistance-project-form.component.html',
  styleUrls: ['./assistance-project-form.component.scss']
})
export class AssistanceProjectFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    obsolete: [false],
    guestMinimal: this.fb.group({
      uuid: ['', Validators.required],
      firstName: [{value: '', disabled: true}, Validators.required],
      lastName: [{value: '', disabled: true}, Validators.required],
      fiscalCode: [{value: '', disabled: true}, Validators.required],
      birthDate: [{value: '', disabled: true}, Validators.required],
      birthPlace: [{value: '', disabled: true}, Validators.required],
      fullName: ['', Validators.required],
      historicResidence: this.fb.group({
        city: [{value: '', disabled: true}, Validators.required],
        asl: [{value: null, disabled: true}, Validators.required],
        region: [{value: null, disabled: true}, Validators.required],
        area: [{value: null, disabled: true}, Validators.required],
        district: [{value: null, disabled: true}, Validators.required],
      }),
    }),

    // Prescription
    prescriptionDate: [null, Validators.required],
    fileName: [null, Validators.required],
    fileContent: [null, Validators.required],
    status: [null, Validators.required],

    // Performances
    performanceType: [null, Validators.required],
    performanceCharge: [null, Validators.required],

    // Treatment
    treatment: [null],
    treatmentStrDate: [null, [Validators.required, this.strDateValidator()]],
    treatmentEndDate: [null, [Validators.required, this.endDateValidator()]],
    treatmentDaysNumber: [null],

    // Fees
    dailyFee: [null, Validators.required],
    aslPercentFee: [null, [Validators.required, Validators.min(0), Validators.max(100), this.percentFeeChargeValidator()]],
    cityPercentFee: [null, [Validators.min(0), Validators.max(100), this.percentFeeChargeValidator()]],
    userPercentFee: [null, [Validators.min(0), Validators.max(100), this.percentFeeChargeValidator()]],
    aslCharge: [null, [Validators.required, Validators.min(0), this.percentFeeChargeValidator()]],
    birthPlaceCharge: [null, [Validators.min(0), this.percentFeeChargeValidator()]],
    userCharge: [null, [Validators.min(0), this.percentFeeChargeValidator()]],
    // Authorization
    authNumber: [null, [Validators.max(99999999)]],
    authDate: [null, Validators.required],

    // Other info
    prescriberCode: [null, [Validators.required, Validators.max(99999999)]],
    disability: [],
    diagnosisICD9: [],
    filesH: [],
    invoices: []
  });

  guests$: Observable<GuestMinimalTransition[]> | undefined;
  performanceTypes$: Observable<PerformanceType[]> | undefined;
  performanceCharges$: Observable<PerformanceCharge[]> | undefined;
  assistanceProjectStatues$: Observable<AssistanceProjectStatus[]> | undefined;
  disabilities$: Observable<Disability[]> | undefined;
  diagnosisICD9$: Observable<DiagnosisICD9[]> | undefined;

  selectedGuest: GuestMinimalTransition | undefined;
  selectedGuestRegions: Region[] = [];
  selectedGuestCities: City[] = [];
  selectedGuestAsls: Asl[] = [];
  selectedGuestAreas: Area[] = [];
  selectedGuestDistricts: District[] = [];
  selectedGuestUuid = '';
  dataInBase64 = '';
  compareById = compareById;
  compareGuests = compareGuests;
  isGuest = false;

  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    // Api services
    private guestService: GuestService,
    private performanceTypeService: PerformanceTypeService,
    private performanceChargeService: PerformanceChargeService,
    private assistanceProjectStatusService: AssistanceProjectStatusService,
    private disabilityService: DisabilityService,
    private diagnosisIcd9Service: DiagnosisIcd9Service,
    public fileService: FileService,
    private userSecurityService: UserService
  ) {
    // Calling super to init the extended class
    super();

    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);

    // If data is provided remove password and confirmPassword FormControl, the MustMatch validator and then patch values
    if (data.editFormData) {
      this.isEdit = true;
      this.form.get('guestMinimal')?.disable();
      this._getSelectedGuestInfo(data.editFormData.guestMinimal);
      this.updateForm(data.editFormData);
      console.log(data.editFormData);
      this.checkValidatedFilesH(data.editFormData);
      if (data.editFormData.file && data.editFormData.file.staticPath && data.editFormData.file.dynamicPath) {
        const filePath = `${data.editFormData.file?.staticPath.path}/${data.editFormData.file.dynamicPath}`;
        this.fileService.downloadFile(filePath).subscribe(result => {
          this.dataInBase64 = result;
          this.updateForm(data.editFormData);
        });
      } else {
        this.updateForm(data.editFormData);
      }
    }

    if (data.patientUUID && data.patientUUID.length > 0) {
      this.selectedGuestUuid = data.patientUUID;
      this.isGuest = true;
      this.form.get('guestMinimal')?.disable();
      if (!this.isEdit) {
        this.guestService.getMinimalByUUID(data.patientUUID).subscribe(result => {
          console.log('Result from getMinimalByUUID ', result);
          this._getSelectedGuestInfo(result);
          this.form.get('guestMinimal')?.patchValue({
            ...result,
            birthDate: result?.birthDate ? new Date(result?.birthDate) : null
          });
        });
      }
    }
    this.form.get('dailyFee')?.disable();
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (!user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT)) {
      this.form.disable();
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);

    this.guests$ = this.guestService.getMinimal();
    this.performanceTypes$ = this.performanceTypeService.getAllAsObservable();
    this.performanceCharges$ = this.performanceChargeService.getAllAsObservable();
    this.assistanceProjectStatues$ = this.assistanceProjectStatusService.getAllAsObservable();
    this.disabilities$ = this.disabilityService.getAllAsObservable();
    this.diagnosisICD9$ = this.diagnosisIcd9Service.getAllAsObservable();

  }

  getFileUploadBorder(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'border-danger' : 'border-dark';
  }

  getFileUploadTextColor(): string {
    const formInput = this.form.get('fileName');
    return (this.formSubmitAttempt && formInput && formInput?.invalid) ? 'text-danger' : 'text-dark';
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  /**
   * On guest selection change
   * @param event Selected guest event
   */
  onGuestSelectionChange(guest: GuestMinimalTransition): void {
    this._getSelectedGuestInfo(guest);
    this.form.get('guestMinimal')?.patchValue({
      ...this.selectedGuest,
      birthDate: this.selectedGuest?.birthDate ? new Date(this.selectedGuest?.birthDate) : null
    });
    this.form.get('guestMinimal')?.disable();
  }

  /**
   * On guest selection change
   * @param files Uploaded file/s.
   */
  onFileUploadedChange(files: any[]): void {
    if (files && files[0]) {
      const file = files[0];
      this.form.get('fileName')?.setValue(file.name);

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (() => {
        this.form.get('fileContent')?.setValue(reader.result);
      });
    }
  }

  openFile(): void {
  if (this.form.get('fileContent')?.value) {
    openBlobFromBase64(this.form.get('fileContent')?.value);
  }
  }

  /**
   * Return the 'guestMinimal' form control
   */
  guestMinimalControl(): FormControl {
    return this.form.get('guestMinimal') as FormControl;
  }

  performanceTypeFormControl(): FormControl {
    return this.form.get('performanceType') as FormControl;
  }

  guestUuidFormControl(formGroup: AbstractControl | null): FormControl {
    return formGroup?.get('fullName') as FormControl;
  }

  performanceChargeFormControl(): FormControl {
    return this.form.get('performanceCharge') as FormControl;
  }

  treatmentTypeFormControl(): FormControl {
    return this.form.get('treatment') as FormControl;
  }

  disabilityFormControl(): FormControl {
    return this.form.get('disability') as FormControl;
  }

  /**
   * Return the 'diagnosisICD9' form control
   */
  diagnosisICD9FormControl(): FormControl {
    return this.form.get('diagnosisICD9') as FormControl;
  }

  /**
   * Update the assistance project form data
   * @param data Data to update the form.
   */
  updateForm(data: AssistanceProject): void {

    // Patching data for recovery tax regime form
    this.form?.patchValue({
      ...data,
      guestMinimal: {
        ...data.guestMinimal,
        birthDate: data.guestMinimal?.birthDate ? new Date(data.guestMinimal?.birthDate) : null
      },

      // Dates
      prescriptionDate: new Date(data.prescriptionDate),
      treatmentStrDate: new Date(data.treatmentStrDate),
      treatmentEndDate: new Date(data.treatmentEndDate),
      aslCharge: this.calculateEuro(data.aslPercentFee, data.dailyFee),
      birthPlaceCharge: this.calculateEuro(data.cityPercentFee, data.dailyFee),
      userCharge: this.calculateEuro(data.userPercentFee, data.dailyFee),
      // treatmentDaysNumber: getNumberOfDaysFromDates(data.treatmentStrDate, data.treatmentEndDate),
      authDate: new Date(data.authDate),
      fileContent: this.dataInBase64,
      filesH: data.filesH,
      invoices: data.invoices
    });
  }

  /**
   * Check if the assistance project is related to validated files h
   * @param data Project to check
   */
  checkValidatedFilesH(data: AssistanceProject): void {
    const filesH = data?.filesH;
    filesH?.forEach(file => {
      if (file.status.description === 'Consolidato') {
        if (!this.isEdit) {
          this.form.get('aslPercentFee')?.disable();
          this.form.get('cityPercentFee')?.disable();
          this.form.get('userPercentFee')?.disable();
        }
        return;
      }
    });
    if (this.isEdit) {
      this.changeQuote('aslPercent', 'Fee');
      this.changeQuote('cityPercent', 'Fee');
      this.changeQuote('userPercent', 'Fee');
      return;
    }
  }

  private _getSelectedGuestInfo(guestMinimal: GuestMinimalTransition): void {
    this.selectedGuest = guestMinimal;
    this.selectedGuestRegions = [this.selectedGuest.historicResidence.region];
    this.selectedGuestCities = [this.selectedGuest.historicResidence.city];
    this.selectedGuestAsls = [this.selectedGuest.historicResidence.asl];
    this.selectedGuestAreas = [this.selectedGuest.historicResidence.area];
    this.selectedGuestDistricts = [this.selectedGuest.historicResidence.district];
  }

  strDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const endTreatmentDate = this.form?.get('treatmentEndDate')?.value;
      if (!(control && control.value)) {
        // if there's no control or no value, that's ok
        return null;
      }

      // return null if there's no errors
      else if (endTreatmentDate) {
        if (control.value > endTreatmentDate) {
          return {invalidDate: ''};
        } else {
          this.form?.get('treatmentEndDate')?.setErrors(null);
          return null;
        }
      } else {
        return null;
      }
    };
  }

  endDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const strTreatmentDate = this.form?.get('treatmentStrDate')?.value;
      if (!(control && control.value)) {
        // if there's no control or no value, that's ok
        return null;
      }

      // return null if there's no errors
      else if (strTreatmentDate) {
        if (control.value < strTreatmentDate) {
          return {invalidDate: ''};
        } else {
          this.form?.get('treatmentStrDate')?.setErrors(null);
          return null;
        }
      } else {
        return null;
      }
    };
  }

  percentFeeChargeValidator(): ValidatorFn {
    // tslint:disable-next-line:indent
	return (control: AbstractControl): { [key: string]: any } | null => {
  // percent fees
      const userPercentFee = this.form?.get('userPercentFee')?.value == null ? 0 : this.form?.get('userPercentFee')?.value;
      const cityPercentFee = this.form?.get('cityPercentFee')?.value == null ? 0 : this.form?.get('cityPercentFee')?.value;
      const aslPercentFee = this.form?.get('aslPercentFee')?.value == null ? 0 : this.form?.get('aslPercentFee')?.value;

    // charges
      const birthPlaceCharge = this.form?.get('birthPlaceCharge')?.value == null ? 0 : this.form?.get('birthPlaceCharge')?.value;
      const userCharge = this.form?.get('userCharge')?.value == null ? 0 : this.form?.get('userCharge')?.value;
      const aslCharge = this.form?.get('aslCharge')?.value == null ? 0 : this.form?.get('aslCharge')?.value;
      const dailyFee = this.form?.get('dailyFee')?.value;

      if (dailyFee == null) {
      return null;
    } else {
        const sumFees = aslPercentFee + userPercentFee + cityPercentFee;
        const sumCharges = birthPlaceCharge + userCharge + aslCharge;
        const infDailyFee = dailyFee - 0.2;
        const supDailyFee = dailyFee + 0.2;
        if ((sumFees == 100) || (infDailyFee < sumCharges && sumCharges < supDailyFee)) {
            this.form?.get('aslPercentFee')?.setErrors(null);
            this.form?.get('userPercentFee')?.setErrors(null);
            this.form?.get('cityPercentFee')?.setErrors(null);
            this.form?.get('birthPlaceCharge')?.setErrors(null);
            this.form?.get('userCharge')?.setErrors(null);
            this.form?.get('aslCharge')?.setErrors(null);
            return null;
        } else {
          return {invalidFee: ''};
        }
      }
    };
  }

  changeQuote(value: string, changeFrom: string): void {
    let fee;
    let euro;
    switch (value) {
      case 'aslPercent': {
        fee = this.form?.get('aslPercentFee');
        euro = this.form?.get('aslCharge');
        break;
      }
      case 'cityPercent': {
        fee = this.form?.get('cityPercentFee');
        euro = this.form?.get('birthPlaceCharge');
        break;
      }
      case 'userPercent': {
        fee = this.form?.get('userPercentFee');
        euro = this.form?.get('userCharge');
        break;
      }
      default: {
        break;
      }
    }
    if (changeFrom !== 'Euro' && fee?.value !== null) {
      euro?.disable();
      const euroValue = this.calculateEuro(fee?.value);
      euro?.setValue(euroValue);
    } else if (changeFrom !== 'Fee' && euro?.value !== null) {
      fee?.disable();
      const perValue = this.calculatePercentage(euro?.value);
      fee?.setValue(perValue);
    } else {
      if (changeFrom === 'Fee' && fee?.value === null) {
        euro?.setValue(null);
      }
      if (changeFrom === 'Euro' && euro?.value == null) {
        fee?.setValue(null);
      }
      fee?.enable();
      euro?.enable();
    }
  }

  calculatePercentage(euro: number): number {
    return Math.round(euro * 10000 / this.form.get('dailyFee')?.value) / 100;
  }

  calculateEuro(percentage: number, total?: number): number {
    if (total) {
      return Math.round(total * percentage / 1) / 100;
    }
    return Math.round(this.form.get('dailyFee')?.value * percentage / 1) / 100;
  }

  onPerformanceTypeSelection(performanceType: PerformanceType): void {
    if (performanceType) {
      this.form.get('dailyFee')?.setValue(performanceType.charge);
      this.changeQuote('aslPercent', 'Fee');
      this.changeQuote('cityPercent', 'Fee');
      this.changeQuote('userPercent', 'Fee');
    }
  }

  changeTreatmentNumberOfDays(): void {
    const startDate = this.form?.get('treatmentStrDate')?.value;
    const endDate = this.form?.get('treatmentEndDate')?.value;
    let numDays = this.form?.get('treatmentDaysNumber')?.value;

    if (numDays) {
      if (startDate) {
        if (!this.isEdit) {
          numDays--;
        }
        const dateEnd = getEndDateFromStartAndDays(numDays, startDate);
        this.form?.get('treatmentEndDate')?.setValue(dateEnd);
      }
    } else if (endDate) {
      const dateStart = getStartDateFromEndAndDays(numDays, endDate);
      this.form?.get('treatmentStrDate')?.setValue(dateStart);
    }

  }

  changeTreatmentStrDate(): void {
    const startDate = this.form?.get('treatmentStrDate')?.value;
    const endDate = this.form?.get('treatmentEndDate')?.value;
    const numDays = this.form?.get('treatmentDaysNumber')?.value;

    if (startDate) {
      if (numDays) {
        const dateEnd = getEndDateFromStartAndDays(numDays - 1, startDate);
        this.form?.get('treatmentEndDate')?.setValue(dateEnd);
      } else if (endDate) {
        const days = getNumberOfDaysFromDates(startDate, endDate);
        this.form?.get('treatmentDaysNumber')?.setValue(days);
      }
    }
  }

  changeTreatmentEndDate(): void {
    const startDate = this.form?.get('treatmentStrDate')?.value;
    const endDate = this.form?.get('treatmentEndDate')?.value;
    const numDays = this.form?.get('treatmentDaysNumber')?.value;

    if (endDate) {
      if (startDate) {
        const days = getNumberOfDaysFromDates(startDate, endDate);
        this.form?.get('treatmentDaysNumber')?.setValue(days);
      } else if (numDays) {
        const dateStart = getStartDateFromEndAndDays(numDays, endDate);
        this.form?.get('treatmentStrDate')?.setValue(dateStart);
      }
    }
  }

  keyPressAlphanumeric($event: KeyboardEvent) {
    const inp = String.fromCharCode($event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      $event.preventDefault();
      return false;
    }
  }

}


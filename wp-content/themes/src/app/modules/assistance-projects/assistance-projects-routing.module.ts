import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssistanceProjectsComponent} from '@app/modules/assistance-projects/assistance-projects.component';
import {AssistanceProjectsHistoryComponent} from '@app/modules/assistance-projects/assistance-projects-history/assistance-projects-history.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AssistanceProjectsComponent},
  {path: 'history', pathMatch: 'full', component: AssistanceProjectsHistoryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssistanceProjectsRoutingModule {
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssistanceProject} from '@app/core/models';
import {RowActionIcons} from '@app/shared/components/generic-table/generic-table.component';

@Component({
  selector: 'app-assistance-project-history-details',
  templateUrl: './assistance-project-history-details.component.html',
  styleUrls: ['./assistance-project-history-details.component.scss']
})
export class AssistanceProjectHistoryDetailsComponent implements OnInit {

  // Input
  @Input() assistanceProject: AssistanceProject | undefined;
  @Input() rowActionIcons: RowActionIcons | undefined;
  @Input() period: string | undefined;

  // Output
  @Output() ngDownloadUVI: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() ngDelete: EventEmitter<any> = new EventEmitter<any>();
  validated = false;
  anaFile: string | undefined;

  constructor() {
  }

  ngOnInit(): void {
  }

  emitDownloadAction(row: any): void {
    this.ngDownloadUVI.emit(row);
  }

  emitEditAction(row: any): void {
    this.ngEdit.emit(row);
  }

  emitDeleteAction(row: any): void {
    this.ngDelete.emit(row);
  }
}

import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';
import {AssistanceProject, AssistanceProjectStatus} from '@app/core/models';
import {MatTableDataSource} from '@angular/material/table';
import {getMonthNameFromDate, OrderType, sortObject} from '@app/shared/helpers/utility';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {MatDialogConfig} from '@angular/material/dialog';
import {AssistanceProjectStatusService} from '@app/core/http';
import {FormControl, FormGroup} from '@angular/forms';
import {AssistanceProjectHistory} from '@app/core/models/assistance-projects/assistance-project-history.model';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-assistance-projects-history-table',
  templateUrl: './assistance-projects-history-table.component.html',
  styleUrls: ['./assistance-projects-history-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AssistanceProjectsHistoryTableComponent extends GenericTableComponent<AssistanceProjectHistory> implements OnInit {
  @ViewChild(MatSort, {static: false}) matSort?: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator?: MatPaginator;
  GetMonthNameFromDate = getMonthNameFromDate;
  period: string | undefined;

  statusFormGroup = new FormGroup({
    status: new FormControl()
  });
  inputAuthNumber: string | undefined;
  selectectedStatus: AssistanceProjectStatus | undefined;
  assistanceProjectStatues$: Observable<AssistanceProjectStatus[]> | undefined;
  treatMentStartDate: Date | undefined;
  treatMentEndDate: Date | undefined;

  @Input() set tableData(data: Observable<AssistanceProject[]> | AssistanceProject[] | undefined) {
    const date = new Date();
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }
  expandedElement?: AssistanceProject;

  itemPerPage = 5;
  currentPage = 0;
  dataForSearch?: AssistanceProject[];
  mainData: AssistanceProject[] = [];
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };

  constructor(private assistanceProjectStatusService: AssistanceProjectStatusService) {
    super();
  }

  ngOnInit(): void {
    if (this.paginator) {
      this.tableDataSource.paginator = this.paginator;
    }
    this.displayedColumns = ['Ospite','Data Autorizzazione', 'Stato', 'Nr', ' '];
    this.assistanceProjectStatues$ = this.assistanceProjectStatusService.getAllAsObservable();
  }
  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: AssistanceProject[]): void {
    this.mainData = sortObject(Object.values(data), 'authDate', OrderType.desc);
    const assitanceProjectHistory = this._getAssistanceProjectHistory(this.mainData);
    this.tableDataSource = new MatTableDataSource(assitanceProjectHistory);
    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.paginator) {
      this.tableDataSource.paginator = this.paginator;
    }
    this._updateData();
  }

  private _getAssistanceProjectHistory(data: AssistanceProject[]): AssistanceProjectHistory[] {

    const assitanceProjectsHistory: { dataAutorizzazione: Date; treatmentStrDate: Date; status: AssistanceProjectStatus; authNumber: number;
      guestName: string; assistanceProjects: AssistanceProject[]; }[] = [];
    const assitanceProjectsHistoryMap = data.reduce((obj: any, item) => {
      assitanceProjectsHistory.push({
        dataAutorizzazione: item.authDate,
        treatmentStrDate: item.treatmentStrDate,
        status: item.status,
        authNumber: item.authNumber,
        guestName: item.guestMinimal?.lastName + ' ' + item.guestMinimal?.firstName,
        assistanceProjects: [item]
      });
      return assitanceProjectsHistory;
    }, {});

    return Object.values(assitanceProjectsHistoryMap);
  }

  applyAuthNumberFilter(event: Event): void {
    this.inputAuthNumber = (event.target as HTMLInputElement).value;
    this._updateData();
  }
  private _updateData(): void{
    this.tableDataSource.filter = '1';
    this.tableDataSource.filterPredicate = (data: any, filter) => {
      let authMatch = true;
      let statusMatch = true;
      let dateMatch = true;
      if (this.inputAuthNumber && this.inputAuthNumber !== ''){
        authMatch =  data.authNumber.toString() === this.inputAuthNumber;
      }
      if (this.selectectedStatus && this.selectectedStatus.id > 0){
        statusMatch =  data.status.id === this.selectectedStatus.id;
      }

      if (data.treatmentStrDate != null){
        const startDate = new Date(data.treatmentStrDate);
        startDate.setHours(0, 0, 0, 0);
        if (this.treatMentStartDate && this.treatMentEndDate){
          dateMatch = startDate.getTime() >= this.treatMentStartDate.getTime() && startDate.getTime() <= this.treatMentEndDate.getTime();
        }else if (this.treatMentStartDate){
          dateMatch = startDate.getTime() >= this.treatMentStartDate.getTime();
        }else if (this.treatMentEndDate){
          dateMatch = startDate.getTime() <= this.treatMentEndDate.getTime();
        }
      }
      return authMatch && statusMatch && dateMatch;
    };
    // this.tableDataSource.filteredData = this.tableDataSource.data;
    // this.tableDataSource.filteredData = this.tableDataSource.filteredData.filter(data => {
    //   let authMatch = true;
    //   let statusMatch = true;
    //   let dateMatch = true;
    //   if (this.inputAuthNumber && this.inputAuthNumber !== ''){
    //     authMatch =  data.authNumber.toString() === this.inputAuthNumber;
    //   }
    //   if (this.selectectedStatus && this.selectectedStatus.id > 0){
    //     statusMatch =  data.status.id === this.selectectedStatus.id;
    //   }
    //   if (data.treatmentStrDate != null){
    //     const startDate = new Date(data.treatmentStrDate);
    //     startDate.setHours(0, 0, 0, 0);
    //     if (this.treatMentStartDate && this.treatMentEndDate){
    //       dateMatch = startDate.getTime() >= this.treatMentStartDate.getTime() && startDate.getTime() <= this.treatMentEndDate.getTime();
    //     }else if (this.treatMentStartDate){
    //       dateMatch = startDate.getTime() >= this.treatMentStartDate.getTime();
    //     }else if (this.treatMentEndDate){
    //       dateMatch = startDate.getTime() <= this.treatMentEndDate.getTime();
    //     }
    //   }
    //   return authMatch && statusMatch && dateMatch;
    // });
  }
  onStatusSelection(): void {
    this.selectectedStatus = this.statusFormGroup.get('status')?.value;
    this._updateData();
  }

  dateChange(event: any, key: string): void {
    const value = event?.value;
    if (key === 'treatmentStartDate') {
      if (value){
        this.treatMentStartDate = value.toDate();
      }else {
        this.treatMentStartDate = undefined;
      }
    }else if (key === 'treatmentEndDate') {
      if (value) {
        this.treatMentEndDate = value.toDate();
      } else {
        this.treatMentEndDate = undefined;
      }
    }
    this._updateData();
  }
}

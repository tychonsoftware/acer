import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {AssistanceProject} from '@app/core/models';
import {AssistanceProjectService, FileService} from '@app/core/http';
import {TableColumn} from '@app/shared/components/generic-table/generic-table.component';
import {base64ToDownloadLink, getFileNameFromPath} from '@app/shared/helpers/utility';
import {DialogService} from '@app/core/services/dialog.service';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {AssistanceProjectFormComponent} from '@app/modules/assistance-projects/assistance-project-form/assistance-project-form.component';
import {TranslateService} from '@ngx-translate/core';
import {MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-assistance-projects-history',
  templateUrl: './assistance-projects-history.component.html',
  styleUrls: ['./assistance-projects-history.component.scss']
})

export class AssistanceProjectsHistoryComponent implements OnInit {


  assistanceProjects$: Observable<AssistanceProject[]> | undefined;

  public assistanceProjectsHistoryTableColumns: TableColumn[] = [
    {
      name: 'Ospite',
      dataKey: 'guestName',
      position: 'center',
      isDate: false,
      isSortable: true
    },
	{
      name: 'Data Autorizzazione',
      dataKey: 'dataAutorizzazione',
      position: 'center',
      isDate: true,
      isSortable: true,
      columnWidth: '20%'
    },
    {
      name: 'Stato',
      dataKey: 'status.description',
      position: 'center',
      isDate: false,
      isSortable: true
    },
    {
      name: 'Nr',
      dataKey: 'authNumber',
      position: 'center',
      isDate: false,
      isSortable: true
    },
    {
      name: ' ',
      dataKey: 'expand',
      position: 'center',
      isSortable: false
    }
  ];

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };
  constructor( private translateService: TranslateService,
               private assistanceProjectsService: AssistanceProjectService,
               private fileService: FileService,
               private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this._getAssistanceProjects();
  }

  private _getAssistanceProjects(): void {
    this.assistanceProjects$ = this.assistanceProjectsService.getAllAsObservable();
  }

  onDownloadClick(assistanceProject: AssistanceProject): void {
    const path = `${assistanceProject?.file.staticPath?.path}/${assistanceProject?.file?.dynamicPath}`;
    const fileName = getFileNameFromPath(assistanceProject?.file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  /**
   * Open the Assistance Project form dialog.
   * @param assistanceProject Data of the Assistance Project to edit.
   */
  openFormDialog(assistanceProject?: AssistanceProject): void {
    if (assistanceProject && assistanceProject.id) {
      // Getting the contract data from the API
      this._openAssistanceProjectFormDialog(assistanceProject);
    } else {
      this._openAssistanceProjectFormDialog();
    }
  }

  /**
   * Open the delete Assistance Project dialog.
   * @param assistanceProject The Assistance Project to delete.
   */
  onDeleteClick(assistanceProject: AssistanceProject): void {
    const data = {
      title: this.translateService.instant('assistanceProjects.deleteDialog.title'),
      message: this.translateService.instant('assistanceProjects.deleteDialog.message', {value: assistanceProject.authNumber}),
      cancelText: this.translateService.instant('assistanceProjects.deleteDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.deleteDialog.confirmText')
    };

    // Open the delete dialog
    const dialogRef = this.dialogService.openConfirmDialog(data);

    // On delete confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(confirmed => confirmed && this._deleteContract(assistanceProject));
  }

  private _openAssistanceProjectFormDialog(editedAssistanceProject?: AssistanceProject): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: editedAssistanceProject,
      title: this.translateService.instant(`assistanceProjects.formDialog.${editedAssistanceProject?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('assistanceProjects.formDialog.cancelText'),
      confirmText: this.translateService.instant('assistanceProjects.formDialog.confirmText')
    };

    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(AssistanceProjectFormComponent, data, this.dialogConfig);

    // On save confirmation
    this.dialogService.confirmed(dialogRef)?.subscribe(ap => this._saveAssistanceProject(ap as AssistanceProject));
  }

  /**
   * Save the Assistance Project.
   * @param assistanceProject The Assistance Project to save
   * @private
   */
  private _saveAssistanceProject(assistanceProject?: AssistanceProject): void {
    if (assistanceProject) {
      let result: Observable<AssistanceProject>;
      if (assistanceProject.id) { // edit case
        result = this.assistanceProjectsService.updateAndGetObservable(assistanceProject);
      } else {
        result = this.assistanceProjectsService.createAndGetObservable(assistanceProject);
      }

      result.subscribe(_ => {
        this.assistanceProjects$ = undefined;
        this._getAssistanceProjects();
      });

    }
  }

  /**
   * Delete Assistance Project
   * @param assistanceProject The Assistance Project to delete.
   * @private
   */
  private _deleteContract(assistanceProject: AssistanceProject): void {
    if (assistanceProject.id) {
      this.assistanceProjectsService.delete(assistanceProject.id);
      this.assistanceProjects$ = undefined;
      this._getAssistanceProjects();
    }
  }
}

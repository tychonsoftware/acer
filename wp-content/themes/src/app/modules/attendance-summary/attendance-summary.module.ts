import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AttendanceSummaryComponent} from '@app/modules/attendance-summary/attendance-summary.component';
import {HttpClient} from '@angular/common/http';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {SharedModule} from '@app/shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MAT_DATE_FORMATS} from '@angular/material/core';
import {DD_MM_YYYY_DATE_FORMAT} from '@app/shared/helpers/format-datepicker';
import { AttendanceSummaryTableComponent } from './attendance-summary-table/attendance-summary-table.component';
import { DatePipe } from '@angular/common';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AttendanceSummaryComponent},
];

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/attendance-summary/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    AttendanceSummaryComponent,
    AttendanceSummaryTableComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    }),
  ],
  exports: [RouterModule],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: DD_MM_YYYY_DATE_FORMAT},
    DatePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AttendanceSummaryModule {
}

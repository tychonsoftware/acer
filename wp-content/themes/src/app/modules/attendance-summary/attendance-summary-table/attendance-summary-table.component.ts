import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {compareById} from '@app/shared/helpers/comparators';
import {MatTableDataSource} from '@angular/material/table';
import {AssistanceProjectFiltered} from '@app/core/models';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {OrderType, sortObject} from '@app/shared/helpers/utility';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';
import { GenericTableComponent, TableColumn } from '@app/shared/components/generic-table/generic-table.component';
import { isObservable, Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { AttendanceReport } from '@app/core/models/attendance-report/attendance-report.model';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-attendance-summary-table',
  templateUrl: './attendance-summary-table.component.html',
  styleUrls: ['./attendance-summary-table.component.scss']
})
export class AttendanceSummaryTableComponent extends GenericTableComponent<AttendanceReport> implements OnInit {

  compareById = compareById;
  paginationSizes: number[] = [5, 10, 20];
  defaultPageSize = this.paginationSizes[1];
  isEditable = false;
  filterStartDate: Date | undefined;
  filterEndDate: Date | undefined;
  assistanceProjectFilter: AssistanceProjectFiltered | undefined;

  @Input() set tableData(data: Observable<AttendanceReport[]> | AttendanceReport[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  @Input() title = '';
  @Output() filter: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatSort, {static: false}) sortFamily?: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginatorFamily?: MatPaginator;

  constructor(private userSecurityService: UserService, public datepipe: DatePipe) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
    this.assistanceProjectFilter = {
      startDateTreatments: new Date().toISOString(),
      endDateTreatments: new Date().toISOString()
    };
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_MANAGEMENT)) {
      this.isEditable = true;
    }
  }

  ngOnInit(): void {
    const columnNames = this.tableColumns.map((tableColumn: TableColumn) => tableColumn.name);
    if (this.rowActionIcons) {
      this.displayedColumns = [...columnNames, 'actions'];
    } else {
      this.displayedColumns = columnNames;
    }
    // Calcolo la data del mese precedente
    this.findInitDate();
    this.emitFilterChange();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: AttendanceReport[]): void {
    data.forEach(element => {
      if(element.numberAttendance==null || element.numberAttendance==undefined) {
        element.numberAttendance = 0;
      }
    });
    data = sortObject(data, 'authDate', OrderType.desc);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }
  }

  dateChange(event: any, key: string): void {
    const value = event?.value;
    if (key === 'treatmentStartDate') {
      if (value){
        this.filterStartDate = value.toDate();
      }else {
        this.filterStartDate = undefined;
      }
    }else if (key === 'treatmentEndDate') {
      if (value) {
        this.filterEndDate = value.toDate();
      } else {
        this.filterEndDate = undefined;
      }
    }
    this.emitFilterChange();
  }

  /**
   * Apply the filter on the table data.
   * @param event Filter write event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    this.tableDataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
  }

  private findInitDate(): void {
    const today = new Date();
    this.filterStartDate = new Date(today.getFullYear(), today.getMonth(), 1);
    this.filterEndDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
  }

  emitFilterChange(): void {
    const startDate = this.datepipe.transform(this.filterStartDate, 'dd-MM-yyyy');
    const endDate = this.datepipe.transform(this.filterEndDate, 'dd-MM-yyyy');
    if (startDate && endDate) {
      const query =  this.assistanceProjectFilter = {
        startDateTreatments: startDate,
        endDateTreatments: endDate
      };
      this.filter.emit(query);
    }
  }
}

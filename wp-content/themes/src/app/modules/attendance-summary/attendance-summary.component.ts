import { StepperSelectionEvent } from '@angular/cdk/stepper';
import {Component, OnInit} from '@angular/core';
import {AssistanceProjectService, AttendanceService, RecoveryTaxRegimeTypeService} from '@app/core/http';
import { AttendanceReportService } from '@app/core/http/attendance-report/attendance-report.service';
import { UserService } from '@app/core/http/users/user.service';
import {AssistanceProjectFiltered, RecoveryTaxRegimeType} from '@app/core/models';
import { AttendanceReport } from '@app/core/models/attendance-report/attendance-report.model';
import { TableColumn } from '@app/shared/components/generic-table/generic-table.component';

@Component({
  selector: 'app-attendance-summary',
  templateUrl: './attendance-summary.component.html',
  styleUrls: ['./attendance-summary.component.scss']
})
export class AttendanceSummaryComponent implements OnInit {

  recoveryTaxRegimeTypes: RecoveryTaxRegimeType[] = [];
  buildings: any;
  stepSelectedIndex = 0;
  data: AttendanceReport[] | undefined;
  assistanceProjectFilter: AssistanceProjectFiltered | undefined;
  selectedStrDate: Date;
  selectedEndDate: Date;

  public columns: TableColumn[] = [
    {
      name: 'attendanceSummary.table.guest',
      dataKey: 'guest.fullName',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.fiscalCode',
      dataKey: 'guest.fiscalCode',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.dateOfBirth',
      dataKey: 'guest.birthDate',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.asld',
      dataKey: 'guest.historicResidence.asl.description',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.regimeOfHospitalization',
      dataKey: 'guest.recoveryTaxRegime.recoveryTaxRegimeType.description',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.treatmentStartDate',
      dataKey: 'guest.acceptanceDate',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.dateOfDischarge',
      dataKey: 'guest.hospitalizationSession.dismiss.date',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.statusOfCommitment',
      dataKey: 'assistanceProjectStatusDTO.description',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.daysOfPresence',
      dataKey: 'numberAttendance',
      position: 'center',
    },
  ];

  public privatoColumns: TableColumn[] = [
    {
      name: 'attendanceSummary.table.guest',
      dataKey: 'guest.fullName',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.fiscalCode',
      dataKey: 'guest.fiscalCode',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.dateOfBirth',
      dataKey: 'guest.birthDate',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.regimeOfHospitalization',
      dataKey: 'guest.recoveryTaxRegime.recoveryTaxRegimeType.description',
      position: 'center',
    },
    {
      name: 'attendanceSummary.table.InpatientDate',
      dataKey: 'guest.acceptanceDate',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.dateOfDischarge',
      dataKey: 'guest.hospitalizationSession.dismiss.date',
      position: 'center',
      isDate: true,
    },
    {
      name: 'attendanceSummary.table.daysOfPresence',
      dataKey: 'numberAttendance',
      position: 'center',
    },
  ];

  constructor(private userService: UserService, private attendanceReportService: AttendanceReportService, private assistanceProjectService: AssistanceProjectService,
              recoveryTaxRegimeTypeService: RecoveryTaxRegimeTypeService) {
    const currentDate = new Date();
    this.selectedStrDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    this.selectedEndDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
  }

  ngOnInit(): void {
    this.buildings = this.userService?.getUserSecurity()?.buildings ?
    this.userService?.getUserSecurity()?.buildings : this.userService?.getUserSecurity()?.defaultBuilding;
    this.recoveryTaxRegimeTypes = [];
    this.buildings?.forEach((building: any) => {
      building.recoveryTaxRegimeTypes?.forEach((recoveryTaxRegimeType: any) => {
        if (!this.recoveryTaxRegimeTypes?.find(element => element.description === recoveryTaxRegimeType.description)) {
          this.recoveryTaxRegimeTypes.push(recoveryTaxRegimeType);
        }
      });
    });
  }

  loadData(): void {
    if (this.recoveryTaxRegimeTypes && this.recoveryTaxRegimeTypes[this.stepSelectedIndex] &&
      this.assistanceProjectFilter?.startDateTreatments && this.assistanceProjectFilter?.endDateTreatments) {
      this.attendanceReportService.getAllAttendancesReports(
        this.recoveryTaxRegimeTypes[this.stepSelectedIndex].id + '', this.assistanceProjectFilter?.startDateTreatments,
        this.assistanceProjectFilter?.endDateTreatments).subscribe(data => {
        this.data = [...data];
      });
    }
   /*  const filter: AssistanceProjectFiltered = {
        startDateTreatments: this.selectedStrDate?.toUTCString(),
        endDateTreatments: this.selectedEndDate?.toUTCString(),
      recoveryTaxRegimeTypeDTO: this.recoveryTaxRegimeTypes[this.stepSelectedIndex]
      };
    this.assistanceProjectService.getAssistanceProjectsFilter(filter).subscribe(data => {
        console.log(data);
      }); */
    }

   // Step click
   onStepChange(event: StepperSelectionEvent): void {
    event.previouslySelectedStep.interacted = false;
    this.stepSelectedIndex = event.selectedIndex;
    this.loadData();
  }

  onFilterChange(filter: AssistanceProjectFiltered): void {
    console.log(filter);
    this.assistanceProjectFilter = filter;
    this.loadData();
  }

}

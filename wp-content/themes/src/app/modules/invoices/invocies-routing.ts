import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InvoicesGenerateComponent} from '@app/modules/invoices/invoices-generate/invoices-generate.component';
import {InvoicesHistoryComponent} from '@app/modules/invoices/invoices-history/invoices-history.component';

const routes: Routes = [
  {path: 'gen', component: InvoicesGenerateComponent},
  {path: 'history', component: InvoicesHistoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule {
}

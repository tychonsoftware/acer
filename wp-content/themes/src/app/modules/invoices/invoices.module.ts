import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {SharedModule} from '@app/shared/shared.module';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {InvoicesComponent} from '@app/modules/invoices/invoices.component';
import {InvoicesRoutingModule} from '@app/modules/invoices/invocies-routing';
import {InvoicesGenerateComponent} from '@app/modules/invoices/invoices-generate/invoices-generate.component';
import {InvoicesHistoryComponent} from '@app/modules/invoices/invoices-history/invoices-history.component';
import {InvoicesHistoryTableComponent} from '@app/modules/invoices/invoices-history/invoices-history-table/invoices-history-table.component';
import {InvoiceFormComponent} from '@app/modules/invoices/invoices-history/invoice-form/invoice-form.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/invoices/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    InvoicesHistoryComponent,
    InvoicesGenerateComponent,
    InvoiceFormComponent,
    InvoicesHistoryTableComponent,
    InvoicesComponent
  ],
  imports: [
    InvoicesRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it'
    }),
  ],
  providers: [
    // Per il datapicker
    {provide: MAT_DATE_LOCALE, useValue: 'it-IT'},
  ]
})
export class InvoicesModule {
}

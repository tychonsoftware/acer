import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {compareById} from '@app/shared/helpers/comparators';
import {MatTableDataSource} from '@angular/material/table';
import {Invoice, File} from '@app/core/models';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {getPeriod, OrderType, sortObject, getDateFromPeriod} from '@app/shared/helpers/utility';
import {InvoiceService, FileService} from '@app/core/http';
import {DialogService} from '@app/core/services/dialog.service';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';
import {base64ToDownloadLink, getFileNameFromPath} from '@app/shared/helpers/utility';
import { InvoicePeriods } from '@app/core/models/invoice/invoice-period.model';
import { GenericTableComponent } from '@app/shared/components/generic-table/generic-table.component';
import { isObservable, Observable } from 'rxjs';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-invoices-history-table',
  templateUrl: './invoices-history-table.component.html',
  styleUrls: ['./invoices-history-table.component.scss']
})
export class InvoicesHistoryTableComponent extends GenericTableComponent<Invoice> implements OnInit {
  years: string[] = [];
  yearSelected: string | undefined;

  months: Month[] = [
    {id: 1, name: 'Gennaio', numberOfDay: 31},
    {id: 2, name: 'Febbraio', numberOfDay: 29},
    {id: 3, name: 'Marzo', numberOfDay: 31},
    {id: 4, name: 'Aprile', numberOfDay: 30},
    {id: 5, name: 'Maggio', numberOfDay: 31},
    {id: 6, name: 'Giugno', numberOfDay: 30},
    {id: 7, name: 'Luglio', numberOfDay: 31},
    {id: 8, name: 'Agosto', numberOfDay: 31},
    {id: 9, name: 'Settembre', numberOfDay: 30},
    {id: 10, name: 'Ottobre', numberOfDay: 31},
    {id: 11, name: 'Novembre', numberOfDay: 30},
    {id: 12, name: 'Dicembre', numberOfDay: 31},
  ];
  monthsToView: Month[] = [];
  monthSelected: Month | undefined;
  lastMonth: Month | undefined;
  lastYear: string | undefined;
  disableGenerate = false;
  compareById = compareById;
  displayedColumns: string[] = ['checkConsilidate', 'numInvoice', 'dateInsert', 'documentType', 'destinatary', 'description', 'status', 'documents', 'total', 'azioni'];
  paginationSizes: number[] = [5, 10, 20];
  defaultPageSize = this.paginationSizes[1];
  isEditable = false;

  @Input() set tableData(data: Observable<Invoice[]> | Invoice[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  @Input() title = '';
  @Output() period: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatSort, {static: false}) sortFamily?: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginatorFamily?: MatPaginator;

  constructor(
    private invoiceService: InvoiceService,
    private dialogService: DialogService,
    private userSecurityService: UserService,
	private fileService: FileService) {
    super();
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_MANAGEMENT)) {
      this.isEditable = true;
    }
  }

  ngOnInit(): void {
    // Calcolo la data del mese precedente
    this.findInitDate();
    this.emitPeriodChange();
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
   setTableDataSource(data: Invoice[]): void {
    data = sortObject(data, 'authDate', OrderType.desc);
    this.tableDataSource = new MatTableDataSource(data);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }
  }

  openInvoiceFormDialog(invoice: Invoice): void {
    if (invoice && invoice.id) {
      this.ngEdit.emit(invoice);
    }
  }

  changePeriod(): void {
    this.monthsToView = [];
    this.findMonthToView();
    if (this.monthSelected && this.yearSelected) {
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      this.disableButton(query);
    }
    this.emitPeriodChange();
  }

  toggleAllDraftInvoices(checked: boolean): void {
	const invoices = this.tableDataSource.data;
	if (checked) {
		invoices.forEach(invoice => {
				invoice.checked = true;
		});
	} else {
		invoices.forEach(invoice => {
			invoice.checked = false;
		});
	}
  }

  toggleInvoice(invoice: Invoice, checked: boolean): void {
	  invoice.checked = checked;
  }

  onDownloadClick(file: File): void {
    const path = `${file.staticPath?.path}/${file?.dynamicPath}`;
    const fileName = getFileNameFromPath(file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  onAllPdfDownloadClick(): void {
    this.tableDataSource.data?.forEach((element: any) => {
      if(element.pdfFile) {
        console.log(element);
        this.onDownloadClick(element.pdfFile);
      }
    });
  }

  onAllXmlDownloadClick(): void {
    this.tableDataSource.data?.forEach((element: any) => {
      if(element.xmlFile) {
        console.log(element);
        this.onDownloadClick(element.xmlFile);
      }
    });
  }

  /**
   * Apply the filter on the table data.
   * @param event Filter write event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
    this.tableDataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
  }

  /**
   * Disable generate button if the specified period is after last month
   */
  disableButton(query: string): void {
    // Calcolo la data del mese precedente
    const date = new Date();
    this.lastMonth = this.months.find(m => m.id === date.getMonth());
    this.lastYear = date.getFullYear().toString();
    const lastMonthPeriod = getPeriod(`${this.lastMonth?.id}`, this.lastYear);
    const lastMonthPeriodDate = getDateFromPeriod(lastMonthPeriod);
    const queryDate = getDateFromPeriod(query);

    if (queryDate > lastMonthPeriodDate) {
      this.disableGenerate = true;
    } else {
      this.disableGenerate = false;
    }
  }

  private findInitDate(): void {
    const today = new Date();
    for (let year = 2021; year < today.getFullYear() || (today.getFullYear() === year && today.getMonth() !== 0); year++) {
      this.years.push(year.toString());
    }
    this.yearSelected = today.getMonth() > 0 ? today.getFullYear().toString() : (today.getFullYear() - 1).toString();
    this.monthSelected = today.getMonth() > 0 ? this.months.find(m => m.id === today.getMonth()) : this.months.find(m => m.id === 12);
    this.findMonthToView();
  }

  private findMonthToView(): void {
    if (new Date().getFullYear().toString() === this.yearSelected) {
      this.months.forEach(m => {
        if (m.id < new Date().getMonth() + 1) {
          console.log(m);
          this.monthsToView.push(m);
        }
      });
    } else {
      this.monthsToView = this.months;
    }
  }

  isRowSelected(): boolean {
    let selected = false;
    this.tableDataSource.data?.forEach((element: any) => {
      if(element.checked) {
        selected = true;
      }
    });
    return selected;
  }

  emitPeriodChange() {
    if(this.monthSelected && this.yearSelected) {
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      this.period.emit(query);
    }
  }
}
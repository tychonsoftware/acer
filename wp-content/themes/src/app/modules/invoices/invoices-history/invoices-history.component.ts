import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {File, Invoice} from '@app/core/models';
import {FileService, InvoiceService} from '@app/core/http';
import {map} from 'rxjs/operators';
import {base64ToDownloadLink, getFileNameFromPath, mapObjectDateKeyToDate} from '@app/shared/helpers/utility';
import { IFormDialogConfig } from '@app/shared/components/form-dialog/form-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { DialogService } from '@app/core/services/dialog.service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { UserService } from '@app/core/http/users/user.service';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

@Component({
  selector: 'app-invoices-history',
  templateUrl: './invoices-history.component.html',
  styleUrls: ['./invoices-history.component.scss']
})
export class InvoicesHistoryComponent implements OnInit {

  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: false,
    width: '75%'
  };

  buildings: any;
  recoveryTaxRegimeTypes: any[] = [];
  stepSelectedIndex = 0;
  period: string | undefined;

  observableData$: Observable<Invoice[]> | undefined;
  constructor(public invoiceService: InvoiceService,
              private fileService: FileService,
              private translateService: TranslateService,
              private dialogService: DialogService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.buildings = this.userService?.getUserSecurity()?.buildings ?
      this.userService?.getUserSecurity()?.buildings : this.userService?.getUserSecurity()?.defaultBuilding;
    this.recoveryTaxRegimeTypes = [];
    this.buildings?.forEach((building: any) => {
      building.recoveryTaxRegimeTypes?.forEach((recoveryTaxRegimeType: any) => {
        if (!this.recoveryTaxRegimeTypes?.find(element => element.description === recoveryTaxRegimeType.description)) {
          this.recoveryTaxRegimeTypes.push(recoveryTaxRegimeType);
        }
      });
      this.loadData();
    });
  }

  // Step click
  onStepChange(event: StepperSelectionEvent): void {
    event.previouslySelectedStep.interacted = false;
    this.stepSelectedIndex = event.selectedIndex;
    this.loadData();
  }

  loadData(): void {
    if (this.period && this.recoveryTaxRegimeTypes && this.recoveryTaxRegimeTypes[this.stepSelectedIndex]) {
      this.invoiceService.getByPeriodAndTaxRegime(this.period, this.recoveryTaxRegimeTypes[this.stepSelectedIndex].id).subscribe((result: any) => {
        console.log('Result from getInvoicesByPeriod');
        console.log(result);
        map((res: Invoice[]) => res.map((a: Invoice) => mapObjectDateKeyToDate(a)));
        this.observableData$ = of([...result]);
      });
    }
  }

  onDownloadClick(file: File): void {
    const path = `${file.staticPath?.path}/${file?.dynamicPath}`;
    const fileName = getFileNameFromPath(file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  openInvoiceFormDialog(invoice: Invoice): void {
    const data: Partial<IFormDialogConfig> = {
      editFormData: invoice,
      title: this.translateService.instant(`fatturazioneHistory.formDialog.${invoice?.id ? 'editTitle' : 'addTitle'}`),
      cancelText: this.translateService.instant('fatturazioneHistory.formDialog.cancelText'),
      confirmText: this.translateService.instant('fatturazioneHistory.formDialog.confirmText'),
      disableSave: invoice.invoiceStatus != null && invoice.invoiceStatus.description === 'PAGATA'
    };
    this.dialogConfig.width = '600px';
    // Open the dialog with the UserFormComponent injected as its body
    const dialogRef = this.dialogService.openFormDialog(InvoiceFormComponent, data, this.dialogConfig);
    this.dialogService.confirmed(dialogRef)?.subscribe(inv => {
      delete inv.file;
      this._saveInvoice(inv as Invoice);
    });
  }

  /**
   * Save the Invoice.
   * @param invoice The Invoice to save
   * @private
   */
  private _saveInvoice(invoice?: Invoice): void {
    if (invoice) {
      if (invoice.id) {
        this.invoiceService.updateAndGetObservable(invoice).subscribe(() => {
          this.loadData();
        });
      }
    }
  }

  onPeroidChange(period: string) {
    console.log(period);
    this.period = period
    this.loadData();
  }
}

import {Component, Inject, OnChanges, OnInit} from '@angular/core';
import {FormContentDirective} from '@app/shared/directives/form-content.directive';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';
import {FormBuilder, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import {compareById} from '@app/shared/helpers/comparators';
import {InvoiceStatusService} from '@app/core/http';
import {Invoice, InvoicePayment, InvoiceState, InvoiceStatus} from '@app/core/models';
import {InvoicePaymentService} from '@app/core/http/invoice/invoice-payment.service';

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.scss']
})
export class InvoiceFormComponent extends FormContentDirective implements OnInit, OnChanges {

  // Form
  form = this.fb.group({
    id: [],
    invoiceStatus: [null],
    payment: [null],
    paymentDate: [null],
    dateInsert: [null],
    description: [null],
    destinatary: [null],
    file: [null],
    numInvoice: [null],
    period: [null],
    total: [null],
    invoiceType: [null]
  });
  invoiceStatuses$: Observable<InvoiceStatus[]> | undefined;
  invoicePayments$: Observable<InvoicePayment[]> | undefined;
  compareById = compareById;
  selectedStatus: InvoiceStatus | undefined;
  selectedPayment: InvoicePayment | undefined;
  paymentDateInvalid = false;
  isPaid = false;
  constructor(
    // Data injected form the parent component
    @Inject(MAT_DIALOG_DATA) public data: IFormDialogConfig,
    // Utility services
    private fb: FormBuilder,
    private translateService: TranslateService,
    private invoiceStatusService: InvoiceStatusService,
    private invoicePaymentService: InvoicePaymentService
  ) {
    super();
    if (data.editFormData) {
      this.isEdit = true;
      console.log('Invoice Form Data : ', data.editFormData);
      this.updateForm(data.editFormData);
    }
  }

  ngOnInit(): void {
    this.checkRequiredFields(this.form);
    const invoiceStatuses: InvoiceStatus[] = [];
    this.invoiceStatusService.getAllAsObservable().subscribe(data=> {
      console.log(data);
      data.forEach(element => {
        if(element.description === 'PAGATA' || element.description === 'SOSPESA' || element.description === 'STORNATA') {
          invoiceStatuses.push(element);
        }
      });
      this.invoiceStatuses$ = of(invoiceStatuses);
    });
    //this.invoiceStatuses$ = this.invoiceStatusService.getAllAsObservable();
    this.invoicePayments$ = this.invoicePaymentService.getAllAsObservable();
    this.checkPaymentDateValidator();
  }

  /**
   * OnChanges component lifecycle callback
   */
  ngOnChanges(changes: any): void {
    this.checkRequiredFields(this.form);
  }

  updateForm(data: Invoice): void {
    this.form?.patchValue({
      ...data,
      paymentDate: data?.paymentDate ? new Date(data?.paymentDate) : null
    });
  }

  onFormSubmitWithoutDate(withRowValue?: boolean): void {
    this.formSubmitAttempt = true;
    this.checkPaymentDateValidator();
    if (this.form?.valid) {
      super.onFormSubmitWithoutDate(withRowValue);
    }
  }

  statusChange(value: string): void {
    this.checkPaymentDateValidator();
  }

  checkPaymentDateValidator(): void {
    if (this.form.get('invoiceStatus')?.value.id === InvoiceState.PAGATA) {
      this.paymentDateInvalid = true;
      this.form.get('paymentDate')?.setValidators([Validators.required]);
      this.form.get('paymentDate')?.updateValueAndValidity();
    } else {
      this.paymentDateInvalid = false;
      this.form.get('paymentDate')?.setValidators([]);
      this.form.get('paymentDate')?.updateValueAndValidity();
    }
  }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {compareById} from '@app/shared/helpers/comparators';
import {MatTableDataSource} from '@angular/material/table';
import {Invoice, File} from '@app/core/models';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {getPeriod, OrderType, sortObject, getDateFromPeriod} from '@app/shared/helpers/utility';
import {InvoiceService, FileService} from '@app/core/http';
import {DialogService} from '@app/core/services/dialog.service';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {environment} from '@env/environment';
import {base64ToDownloadLink, getFileNameFromPath} from '@app/shared/helpers/utility';
import { RecoveryTaxRegimeType } from '@app/core/models/guests/recovery-tax-regime-type.model';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-invoices-generate',
  templateUrl: './invoices-generate.component.html',
  styleUrls: ['./invoices-generate.component.scss']
})
export class InvoicesGenerateComponent implements OnInit {
  years: string[] = [];
  yearSelected: string | undefined;
  recoveryTaxRegimeTypes: RecoveryTaxRegimeType[] = [];

  buildings: any;
  stepSelectedIndex = 0;
  period: string | undefined;

  months: Month[] = [
    {id: 1, name: 'Gennaio', numberOfDay: 31},
    {id: 2, name: 'Febbraio', numberOfDay: 29},
    {id: 3, name: 'Marzo', numberOfDay: 31},
    {id: 4, name: 'Aprile', numberOfDay: 30},
    {id: 5, name: 'Maggio', numberOfDay: 31},
    {id: 6, name: 'Giugno', numberOfDay: 30},
    {id: 7, name: 'Luglio', numberOfDay: 31},
    {id: 8, name: 'Agosto', numberOfDay: 31},
    {id: 9, name: 'Settembre', numberOfDay: 30},
    {id: 10, name: 'Ottobre', numberOfDay: 31},
    {id: 11, name: 'Novembre', numberOfDay: 30},
    {id: 12, name: 'Dicembre', numberOfDay: 31},
  ];
  monthsToView: Month[] = [];
  monthSelected: Month | undefined;
  lastMonth: Month | undefined;
  lastYear: string | undefined;
  disableGenerate = false;
  compareById = compareById;
  displayedColumns: string[] = ['checkConsilidate', 'numInvoice', 'dateInsert', 'documentType', 'destinatary', 'description', 'status', 'documents', 'total'];
  dataSource: MatTableDataSource<Invoice> = new MatTableDataSource<Invoice>([]);
  paginationSizes: number[] = [5, 10, 20];
  defaultPageSize = this.paginationSizes[1];
  isEditable = false;

  @ViewChild(MatSort, {static: false}) sortFamily?: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginatorFamily?: MatPaginator;

  constructor(
    private invoiceService: InvoiceService,
    private dialogService: DialogService,
    private userSecurityService: UserService,
	private fileService: FileService) {
    setTimeout(() => this.checkSecurity(), environment.TIME_TO_TIMEOUT);
  }

  private checkSecurity(): void {
    const user = this.userSecurityService.getUserSecurity();
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_MANAGEMENT)) {
      this.isEditable = true;
    }
  }

  ngOnInit(): void {
    // Calcolo la data del mese precedente
    this.findInitDate();

    const buildings: any = this.userSecurityService?.getUserSecurity()?.buildings ? this.userSecurityService?.getUserSecurity()?.buildings : this.userSecurityService?.getUserSecurity()?.defaultBuilding;
    this.recoveryTaxRegimeTypes = [];
    buildings?.forEach((building: any) => {
      building.recoveryTaxRegimeTypes?.forEach((recoveryTaxRegimeType: any) => {
        if(!this.recoveryTaxRegimeTypes?.find(element => element.description === recoveryTaxRegimeType.description)) {
          this.recoveryTaxRegimeTypes.push(recoveryTaxRegimeType);
        }
      });
    });
    this.loadData();
  }

  // Step click
  onStepChange(event: StepperSelectionEvent): void {
    event.previouslySelectedStep.interacted = false;
    this.stepSelectedIndex = event.selectedIndex;
    this.loadData();
  }

  loadData(): void {
    if (this.monthSelected && this.yearSelected && this.recoveryTaxRegimeTypes && this.recoveryTaxRegimeTypes[this.stepSelectedIndex]) {
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      // Faccio la chiamata per popolare la tabella con i proggetti assistenziali
      this.invoiceService.getByPeriodAndTaxRegime(query, this.recoveryTaxRegimeTypes[this.stepSelectedIndex].id).subscribe(element => {
        // Assegno la risposta alla tabella, assegno il periodo della qury ai dati recuperati
        this.dataSource.data = element;
        this.dataSource.data = sortObject(Object.values(element), 'numInvoice', OrderType.desc);
        this.dataSource.data.map(e => e.period = query);
        this.dataSource._updateChangeSubscription();
        if (this.paginatorFamily) {
          this.dataSource.paginator = this.paginatorFamily;
        }
      });
    }
  }

  changePeriod(): void {
    this.monthsToView = [];
    this.findMonthToView();
    if (this.monthSelected && this.yearSelected) {
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      this.disableButton(query);
      this.loadData();
    }
  }

  generateInvoices(): void {
    if (this.monthSelected && this.yearSelected) {

      // costruisco il period MM/YYYY da passare come input alla chiamata REST
      const query = getPeriod(`${this.monthSelected.id}`, this.yearSelected);
      this.invoiceService.generate(query, this.recoveryTaxRegimeTypes[this.stepSelectedIndex].id).subscribe(result => {
        console.log('Generated Invoices');
        console.log(result);
        this.changePeriod();
        if (result && result.length > 0) {
        } else {
          const data = {
            title: '',
            message: 'Nessun FileH convalidato trovato per il calcolo delle fatture',
            cancelText: 'Annulla',
            confirmText: '',
            hideSave: true
          };
          // Open the warning dialog
          this.dialogService.openWarningDialog(data);
        }
      });
    }
  }

  consolidateInvoices(): void {
	let invoices = this.dataSource.data;
	invoices = invoices.filter(invoice => invoice.checked);
	if (invoices === null || invoices.length < 1) {
		const data = {
            title: '',
            message: 'Selezionare almeno una fattura',
            cancelText: 'Annulla',
            confirmText: '',
            hideSave: true
          };
          // Open the warning dialog
          this.dialogService.openWarningDialog(data);
	} else {
		this.invoiceService.consolidate(invoices, undefined).subscribe (result => {
			console.log('Consolidated Invoices');
        	console.log(result);
		    this.changePeriod();
			result.forEach(invoice => {
				invoice.checked = false;
			});
			//if organization provides invoices pdf
				this.invoiceService.generatePdf(result).subscribe(pdfInvoices => {
					console.log('Invoices involved in pdf generation');
	        		console.log(pdfInvoices);
					//if organization provides invoices xml
					this.invoiceService.generateXml(pdfInvoices).subscribe(xmlInvoices => {
						console.log('Invoices involved in xml generation');
		        		console.log(xmlInvoices);
						this.changePeriod();
					});
				});
		});
	}
  }

  toggleAllDraftInvoices(checked: boolean): void {
	const invoices = this.dataSource.data;
	if (checked) {
		invoices.forEach(invoice => {
			if (invoice.invoiceStatus && invoice.invoiceStatus.description === 'BOZZA' && this.checkConsolidate(invoice)) {
				invoice.checked = true;
			} else
				invoice.checked = false;
		});
	} else {
		invoices.forEach(invoice => {
			invoice.checked = false;
		});
	}
  }

  toggleInvoice(invoice: Invoice, checked: boolean): void {
	invoice.checked = checked;
  }

  onDownloadClick(file: File): void {
    const path = `${file.staticPath?.path}/${file?.dynamicPath}`;
    const fileName = getFileNameFromPath(file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  /**
   * Apply the filter on the table data.
   * @param event Filter write event
   */
  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr = JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) !== -1;
    };
  }

  /**
   * Disable generate button if the specified period is after last month
   */
  disableButton(query: string): void {
    // Calcolo la data del mese precedente
    const date = new Date();
    this.lastMonth = this.months.find(m => m.id === date.getMonth());
    this.lastYear = date.getFullYear().toString();
    const lastMonthPeriod = getPeriod(`${this.lastMonth?.id}`, this.lastYear);
    const lastMonthPeriodDate = getDateFromPeriod(lastMonthPeriod);
    const queryDate = getDateFromPeriod(query);

    if (queryDate > lastMonthPeriodDate) {
      this.disableGenerate = true;
    } else {
      this.disableGenerate = false;
    }
  }

  private findInitDate(): void {
    const today = new Date();
    for (let year = 2021; year < today.getFullYear() || (today.getFullYear() === year && today.getMonth() !== 0); year++) {
      this.years.push(year.toString());
    }
    this.yearSelected = today.getMonth() > 0 ? today.getFullYear().toString() : (today.getFullYear() - 1).toString();
    this.monthSelected = today.getMonth() > 0 ? this.months.find(m => m.id === today.getMonth()) : this.months.find(m => m.id === 12);
    this.findMonthToView();
  }

  private findMonthToView(): void {
    if (new Date().getFullYear().toString() === this.yearSelected) {
      this.months.forEach(m => {
        if (m.id < new Date().getMonth() + 1) {
          console.log(m);
          this.monthsToView.push(m);
        }
      });
    } else {
      this.monthsToView = this.months;
    }
  }

  selectBackgroundColor(invoice: Invoice): string {
	var backColor = 'white';
	
	if (invoice.aslDest && !invoice.aslDest.registryIn) {
		backColor = 'red';
	} else if (invoice.areaDest && !invoice.areaDest.registryIn) {
		backColor = 'red';
	} else if (invoice.isRecipientAnagraficaChanged) {
		backColor = 'yellow';
	}
	return backColor;
  }
  
  checkConsolidate(invoice: Invoice): boolean {
	var checked = true;
	
	if (invoice.aslDest && !invoice.aslDest.registryIn) {
		checked = false;
	} else if (invoice.areaDest && !invoice.areaDest.registryIn) {
		checked = false;
	} else if (invoice.isRecipientAnagraficaChanged) {
		checked = false;
	}

	return checked;
  }
}

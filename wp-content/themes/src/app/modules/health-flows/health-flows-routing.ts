import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HealthFlowsGenerateComponent} from '@app/modules/health-flows/health-flows-generate/health-flows-generate.component';
import {HealthFlowsHistoryComponent} from '@app/modules/health-flows/health-flows-history/health-flows-history.component';

const routes: Routes = [
  {path: 'generate', component: HealthFlowsGenerateComponent},
  {path: 'history', component: HealthFlowsHistoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthFlowsRoutingModule {
}

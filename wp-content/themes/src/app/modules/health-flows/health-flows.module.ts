import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {SharedModule} from '@app/shared/shared.module';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {PatientCellComponent} from './health-flows-generate/patient-cell/patient-cell.component';
import {OtherInformationCellComponent} from './health-flows-generate/other-information-cell/other-information-cell.component';
import {TotalsCellComponent} from './health-flows-generate/totals-cell/totals-cell.component';
import {PresentsCellComponent} from './health-flows-generate/presents-cell/presents-cell.component';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {HealthFlowsHistoryComponent} from './health-flows-history/health-flows-history.component';
import {HealthFlowsGenerateComponent} from './health-flows-generate/health-flows-generate.component';
import {HealthFlowsRoutingModule} from '@app/modules/health-flows/health-flows-routing';
import {HealthFlowsHistoryTableComponent} from './health-flows-history/health-flows-history-table/health-flows-history-table.component';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import {HealthFlowsComponent} from '@app/modules/health-flows/health-flows.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/health-flows/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    PatientCellComponent,
    OtherInformationCellComponent,
    TotalsCellComponent,
    PresentsCellComponent,
    HealthFlowsHistoryComponent,
    HealthFlowsGenerateComponent,
    HealthFlowsHistoryTableComponent,
    HealthFlowsComponent
  ],
  imports: [
    HealthFlowsRoutingModule,
    SharedModule,

    // Traduzioni per i nomi colonne sulle tabelle
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it'
    }),
  ],
  providers: [
    // Per il datapicker
    {provide: MAT_DATE_LOCALE, useValue: 'it-IT'},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}}
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HealthFlowsModule {
}

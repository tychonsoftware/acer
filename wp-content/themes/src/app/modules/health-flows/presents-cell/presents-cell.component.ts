import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssistanceProject} from '@app/core/models';

export interface ChangePresent {
  idProject: number;
  numberOfPresence: number;
}

@Component({
  selector: 'app-presents-cell',
  templateUrl: './presents-cell.component.html',
  styleUrls: ['./presents-cell.component.scss']
})
export class PresentsCellComponent implements OnInit {

  @Input() element!: AssistanceProject;
  @Input() month!: string;
  // tslint:disable-next-line:no-output-native
  @Output() change: EventEmitter<ChangePresent> = new EventEmitter<ChangePresent>();

  constructor() {
  }

  ngOnInit(): void {
    if (this.element.numberOfPresence === null || this.element.numberOfPresence === undefined) {
      this.element.numberOfPresence = 0;
    }
  }

  onChange(presences: number): void {
    if (presences <= 0 || presences === undefined) {
      return;
    } else {
      if (this.element?.id) {
        const tmp: ChangePresent = {
          idProject: this.element?.id,
          numberOfPresence: presences
        };
        this.change.emit(tmp);
      }
    }
  }

}

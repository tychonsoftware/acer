import {Component, Input, OnInit} from '@angular/core';
import {AssistanceProject} from '@app/core/models';

@Component({
  selector: 'app-patient-cell',
  templateUrl: './patient-cell.component.html',
  styleUrls: ['./patient-cell.component.scss']
})
export class PatientCellComponent implements OnInit {

  @Input() element!: AssistanceProject;

  constructor() {
  }

  ngOnInit(): void {
  }

}

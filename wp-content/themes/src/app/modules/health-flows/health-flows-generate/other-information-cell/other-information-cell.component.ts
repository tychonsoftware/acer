import {Component, Input, OnInit} from '@angular/core';
import {AssistanceProject} from '@app/core/models';

@Component({
  selector: 'app-other-information-cell',
  templateUrl: './other-information-cell.component.html',
  styleUrls: ['./other-information-cell.component.scss']
})
export class OtherInformationCellComponent implements OnInit {

  @Input() element!: AssistanceProject;

  constructor() { }

  ngOnInit(): void {
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {AssistanceProject} from '@app/core/models';

@Component({
  selector: 'app-presents-cell',
  templateUrl: './presents-cell.component.html',
  styleUrls: ['./presents-cell.component.scss']
})
export class PresentsCellComponent implements OnInit {

  @Input() element!: AssistanceProject;
  @Input() month!: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {AssistanceProject} from '@app/core/models';

@Component({
  selector: 'app-totals-cell',
  templateUrl: './totals-cell.component.html',
  styleUrls: ['./totals-cell.component.scss']
})
export class TotalsCellComponent implements OnInit {

  @Input() element!: AssistanceProject;

  constructor() { }

  ngOnInit(): void { }

}

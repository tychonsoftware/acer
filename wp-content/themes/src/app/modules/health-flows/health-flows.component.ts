import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {AssistanceProject} from '@app/core/models';
import {AssistanceProjectCharge} from '@app/core/models/assistance-projects/assistance-project-charge.model';
import {AssistanceProjectService} from '@app/core/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FileService} from '@app/core/http/file/file.service';

interface Month {
  id: number;
  name: string;
  numberOfDay: number;
}

@Component({
  selector: 'app-health-flows',
  templateUrl: './health-flows.component.html',
  styleUrls: ['./health-flows.component.scss']
})
export class HealthFlowsComponent implements OnInit {

  months: Month[] = [
    {id: 1, name: 'Gennaio', numberOfDay: 31},
    {id: 2, name: 'Febbraio', numberOfDay: 29},
    {id: 3, name: 'Marzo', numberOfDay: 31},
    {id: 4, name: 'Aprile', numberOfDay: 30},
    {id: 5, name: 'Maggio', numberOfDay: 31},
    {id: 6, name: 'Giugno', numberOfDay: 30},
    {id: 7, name: 'Luglio', numberOfDay: 31},
    {id: 8, name: 'Agosto', numberOfDay: 31},
    {id: 9, name: 'Settembre', numberOfDay: 30},
    {id: 10, name: 'Ottobre', numberOfDay: 31},
    {id: 11, name: 'Novembre', numberOfDay: 30},
    {id: 12, name: 'Dicembre', numberOfDay: 31},
  ];
  monthSelected: Month | undefined;

  years: string[] = ['2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
  yearSelected: string | undefined;

  displayedColumns: string[] = ['patient', 'other-information', 'presents', 'totals'];
  dataSource: MatTableDataSource<AssistanceProject> = new MatTableDataSource<AssistanceProject>([]);
  @ViewChild(MatSort, {static: false}) sortFamily?: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginatorFamily?: MatPaginator;

  regimeOptions: string[] = ['RSA'];
  regimeSelected = '';

  totalCharge!: AssistanceProjectCharge;

  // Variabile per abilitare, bottone generatore File H, e disabilitare bottone calcola Totale
  enableButton = false;

  constructor(
    private assistanceService: AssistanceProjectService,
    private fileService: FileService,
    private snakbar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {

    // Calcolo la data del mese precedente
    const date = new Date();
    this.monthSelected = this.months.find(m => m.id === date.getMonth());
    this.yearSelected = date.getFullYear().toString();

    if (this.monthSelected && this.yearSelected) {

      // Creo il campo period che assegneò agli oggetti recuperati mediante la chiamta abyPeriod
      const query = this.monthSelected.id < 10 ? ('0' + this.monthSelected.id + '/' + this.yearSelected) : (this.monthSelected.id + '/' + this.yearSelected);

      // Faccio la chiamata per popolare la tabella con i proggetti assistenziali
      this.assistanceService.getByPeriod(query).subscribe( element => {

        // Assegno la risposta alla tabella, assegno il periodo della qury ai dati recuperati
        this.totalCharge = element.assistanceProjectCharge;
        this.dataSource.data = element.assistanceProjects;
        this.dataSource.data.map(e => e.period = query);

        // tslint:disable-next-line:max-line-length
        if ( (this.totalCharge.totBirthplaceCharge + this.totalCharge.totUserCharge + this.totalCharge.totAslCharge) !== 0 || this.totalCharge.totNumberPresence !== 0) {
          this.enableButton = true;
        }

        // Ordino i dati
        this.sortElementTable();
        this.dataSource._updateChangeSubscription();
      });
    }
  }

  // Calcola il totale, se le presenze sono state inserite per tutti i pazienti
  calculateTotal(): void {

    // Controllo prima che tutti i valori numberOfPresents siano diversi da 0 se lo sono faccio la chiamata alla rest
    let found = false;
    this.dataSource.data.forEach(e => {
      if (e.numberOfPresence === 0) {
        found = true;
        return;
      }
    });

    // Se sono stati trovati elementi con presenze uguali a 0 allora non devo fare nulla
    if (found || this.dataSource.data.length === 0) {
      this.snakbar.open('Compilare tutti i campi presenza', 'Chiudi', {duration: 2000, verticalPosition: 'top'});
      return;
    } else {

      console.log(this.dataSource.data);

      // Se tutti gli elmenti hanno numero di presenze diversa da 0 allora faccio la chiamata al backend
      this.assistanceService.putCalculateCharges(this.dataSource.data).subscribe(element => {

        // Controllo che i campi anno e mese possiedono dei valori
        if (this.monthSelected && this.yearSelected) {

          // costruisco il period MM/YYYY da passare come input alla chiamata REST
          // tslint:disable-next-line:max-line-length
          const query = this.monthSelected.id < 10 ? ('0' + this.monthSelected.id + '/' + this.yearSelected) : (this.monthSelected.id + '/' + this.yearSelected);

          // Devo popolare la tabella con la nuova risposta
          // this.totalCharge = element.assistanceProjectCharge;
          this.dataSource.data = element;

          // Faccio gli ordinamenti dei dati della tabella, e per ogni elemento gli aggiungo il period
          this.sortElementTable();
          this.dataSource.data.map(e => e.period = query);

          // Abilito il bottone del per generare il file H
          this.enableButton = true;

          // Abilito il bottone per il calcolo del FileH ed eseguo la chiamata per ricevere anche i totali del menu a sinistra
          this.assistanceService.calculateTotalCharge(query).subscribe( total => {

            // Se la chiamata ha avuto successo popolo la card sulla sinistra con i totali
            this.totalCharge = total;
          });
        }

      });
    }
  }

  // Chiamata quando, viene cambiato l'anno o il mese
  changePeriod(): void {

    if (this.monthSelected && this.yearSelected) {

      // Creo il campo period che assegneò agli oggetti recuperati mediante la chiamta abyPeriod
      const query = this.monthSelected.id < 10 ? ('0' + this.monthSelected.id + '/' + this.yearSelected) : (this.monthSelected.id + '/' + this.yearSelected);
      console.log(query);

      // Faccio la chiamata per popolare la tabella con i proggetti assistenziali
      this.assistanceService.getByPeriod(query).subscribe( element => {

        // Assegno la risposta alla tabella, assegno il periodo della qury ai dati recuperati
        this.totalCharge = element.assistanceProjectCharge;
        this.dataSource.data = element.assistanceProjects;
        this.dataSource.data.map(e => e.period = query);

        // Ordino i dati
        this.sortElementTable();
        this.dataSource._updateChangeSubscription();
      });
    }
  }

  // Genraione del file H quando si clicca il bottone genera file H
  generateFileH(): void {

    if (this.monthSelected && this.yearSelected) {

      // costruisco il period MM/YYYY da passare come input alla chiamata REST
      // tslint:disable-next-line:max-line-length
      const query = this.monthSelected.id < 10 ? ('0' + this.monthSelected.id + '/' + this.yearSelected) : (this.monthSelected.id + '/' + this.yearSelected);
      // this.fileService.getByPeriod(query).subscribe( result => {
      //   console.log(result);
      // });
    }
  }

  // Ogni volta che viene cambianto un giorno di presenza, aggiorno l'array
  onChangeNumberOfPresent(event: any): void {
    this.dataSource.data.map(e => {
      if (e.id === event.idProject) {
        e.numberOfPresence = event.numberOfPresence;
      }
    });
  }

  // Serve per comparare le stringhe, nella select
  compare(c1: { id: number }, c2: { id: number }): boolean {
    return c1 && c2 && c1.id === c2.id;
  }

  // Ordina gli elementi nella tabella, facendo un ordinamento prima per data di inizio trattamento e in seguito riordina per nome e cognome
  sortElementTable(): void {

    // Poi effettuo anche l'ordine per la data di inizio trattamento
    this.dataSource.data.sort((a, b) => {
      return a.treatmentStrDate > b.treatmentEndDate ? 1 : -1;
    });

    // Effettuo ordinamento per cognome e nome
    this.dataSource.data.sort((a, b) => {
      const user1 = (a.guestMinimal.lastName + a.guestMinimal.firstName).trim().toLowerCase();
      const user2 = (b.guestMinimal.lastName + b.guestMinimal.firstName).trim().toLowerCase();
      return user1 > user2 ? 1 : -1;
    });
  }

  // Per la visualizzazione dl paginator, viene mostrato solamente se l'array dei dati della tabella possiede più di 5 elementi
  displayPaginator(): boolean {
    if (this.dataSource.data) {
      return this.dataSource?.data.length > 5 || false;
    }
    return false;
  }

}

import {Component, OnInit} from '@angular/core';
import {FileHService, FileService} from '@app/core/http';
import {base64ToDownloadLink, getFileNameFromPath, mapObjectDateKeyToDate} from '@app/shared/helpers/utility';
import {ConsolidationStatus, File} from '@app/core/models';
import {FileHStatusChangeEvent} from '@app/modules/health-flows/health-flows-history/health-flows-history-table/health-flows-history-table.component';
import {Observable} from 'rxjs';
import {FileH} from '@app/core/models/file/file-h.model';
import {map} from 'rxjs/operators';
import {Functionalities} from '@app/core/constants/Functionalities';
import {UserService} from '@app/core/http/users/user.service';
import {environment} from '@env/environment';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';
import {OrganizationBuildingsService} from '@app/core/http/organization/organization-buildings.service';

@Component({
  selector: 'app-health-flows-history',
  templateUrl: './health-flows-history.component.html',
  styleUrls: ['./health-flows-history.component.scss']
})
export class HealthFlowsHistoryComponent implements OnInit {

  observableData$: Observable<FileH[]> | undefined;
  yearSelected: string | undefined;
  years: string[] = [];
  isEditable = false;
  buildings: OrganizationBuilding[] | undefined;
  buildingSelected: OrganizationBuilding | undefined;

  constructor(public fileHService: FileHService,
              private fileService: FileService,
              private userSecurityService: UserService,
              private organizationBuildingsService: OrganizationBuildingsService) {
    setTimeout(() => {
      const user = this.userSecurityService.getUserSecurity();
      if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_HISTORIC_MANAGEMENT)) {
        this.isEditable = true;
      }
    }, environment.TIME_TO_TIMEOUT);
  }

  ngOnInit(): void {
    this.findInitDate();

    if (this.yearSelected) {
      this.fileHService.getByYearAndBuilding(this.yearSelected, 1);
      this.fileHService.observableData$.subscribe((result: any) => {
        console.log('Result from getHistoryByYear');
        console.log(result);
        map((res: FileH[]) => res.map((a: FileH) => mapObjectDateKeyToDate(a)));
        this.observableData$ = result;
      });
    }

    this.organizationBuildingsService.findRSABuildings().subscribe(res => {
      if (res && res.length > 0) {
        this.buildings = res;
        this.buildingSelected = this.buildings?.find(building => building.id === 1);
      }
    });
    // this.fileHService.getAll();
    // this.fileHService.observableData$.subscribe((result: any) => {
    //   console.log('Result from getHistoryByPeriod');
    //   console.log(result);
    //   map((res: FileH[]) => res.map((a: FileH) => mapObjectDateKeyToDate(a)));
    //   this.observableData$ = result;
    // });
  }

  onDownloadClick(file: File): void {
    const path = `${file.staticPath?.path}/${file?.dynamicPath}`;
    const fileName = getFileNameFromPath(file?.dynamicPath);
    console.log(`Download of ${fileName} from path ${path}`);
    this.fileService.downloadFile(path).subscribe(result => base64ToDownloadLink(result, fileName).click());
  }

  onStatusChange(fileHStatusChangeEvent: FileHStatusChangeEvent): void {
    const fileStatusId = fileHStatusChangeEvent.status ? ConsolidationStatus.CONSOLIDATED : ConsolidationStatus.TO_BE_CONSOLIDATED;
    this.fileHService.updateStatus(fileHStatusChangeEvent.fileH.id, fileStatusId).subscribe(result => {
      console.log('Result from updateStatus');
      console.log(result);
    });
  }

  changePeriodOrStructure(): void {
    if (this.yearSelected && this.buildingSelected) {
      this.fileHService.getByYearAndBuilding(this.yearSelected, this.buildingSelected.id);
      this.fileHService.observableData$.subscribe((result: any) => {
        console.log('Result from getHistoryByYear');
        console.log(result);
        map((res: FileH[]) => res.map((a: FileH) => mapObjectDateKeyToDate(a)));
        this.observableData$ = result;
      });
    }
  }

  private findInitDate(): void {
    const today = new Date();
    for (let year = 2021; year < today.getFullYear() || (today.getFullYear() === year && today.getMonth() !== 0); year++) {
      this.years.push(year.toString());
    }
    this.yearSelected = today.getMonth() > 0 ? today.getFullYear().toString() : (today.getFullYear() - 1).toString();
  }
}

import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {GenericTableComponent} from '@app/shared/components/generic-table/generic-table.component';
import {ConsolidationStatus, FileH} from '@app/core/models';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {getDateFromPeriod, getMonthNameFromDate, OrderType, sortObjectByPeriod} from '@app/shared/helpers/utility';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {DatePipe} from '@angular/common';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {isObservable, Observable} from 'rxjs';
import {FileHPeriods} from '@app/core/models/file/file-h-period.model';

export interface FileHStatusChangeEvent {
  status: boolean;
  fileH: FileH;
}

@Component({
  selector: 'app-health-flows-history-table',
  templateUrl: './health-flows-history-table.component.html',
  styleUrls: ['./health-flows-history-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HealthFlowsHistoryTableComponent extends GenericTableComponent<FileHPeriods> implements OnInit {
  ConsolidationStatus = ConsolidationStatus;
  GetMonthNameFromDate = getMonthNameFromDate;

  public tableDataSource = new MatTableDataSource<FileHPeriods>([]);
  // public displayedColumns: string[] = ['dateInsert',  'fileHDetails', 'actions'];
  @ViewChild(MatSort, {static: false}) matSort?: MatSort;
  @ViewChild(MatPaginator, {static: false}) matPaginator?: MatPaginator;

  @Output() ngStatus: EventEmitter<FileHStatusChangeEvent> = new EventEmitter<FileHStatusChangeEvent>();
  @Input() isEditable = false;

  datePipe = new DatePipe('it-IT');

  expandedElement?: FileH;

  @Input() set tableData(data: Observable<FileH[]> | FileH[] | undefined) {
    if (data) {
      if (isObservable(data)) {
        data.subscribe(d => {
          if (d?.length > 0) {
            this.setTableDataSource(d);
          }
        });
      } else {
        this.setTableDataSource(data);
      }
    }
  }

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.displayedColumns = ['periodoDiRiferimento'];
  }

  getPeriod(fileH: FileH): string {
    const date = getDateFromPeriod(fileH.period);
    const startOfMonth = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth(), 1), 'dd/MM/YYYY');
    const endOfMonth = this.datePipe.transform(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'dd/MM/YYYY');

    return `${startOfMonth} - ${endOfMonth}`;
  }

  getMonthName(fileH: FileH): string {
    const date = getDateFromPeriod(fileH.period);
    const month = this.datePipe.transform(date, 'MMMM');
    return `${month}`;
  }

  emitStatusChangeAction(status: MatSlideToggleChange, fileH: FileH): void {
    this.ngStatus.emit({status: status.checked, fileH});
  }

  isStatusChecked(fileH: FileH): boolean {
    return fileH.status.id === ConsolidationStatus.CONSOLIDATED;
  }

  /**
   * Set table data table.
   * @param data Table data.
   * @private
   */
  setTableDataSource(data: FileH[]): void {
    const fileHs = this._getFileHs(data);
    this.tableDataSource = new MatTableDataSource(fileHs);

    if (this.matSort) {
      this.tableDataSource.sort = this.matSort;
    }
    if (this.matPaginator) {
      this.tableDataSource.paginator = this.matPaginator;
    }
  }

  private _getFileHs(data: FileH[]): FileHPeriods[] {
    const fileHPeriodsMap = data.reduce((obj: any, item) => {
      const period = item.period;
      if (period) {
        obj[period] ? obj[period].fileHs.push(item) : obj[period] = {
          period: item.period,
          fileHs: [item]
        };
      }
      return obj;
    }, {});
    return sortObjectByPeriod(Object.values(fileHPeriodsMap), 'period', OrderType.desc);
  }

  getTotale(fileHPeriods: FileHPeriods): string {
    let totaleAll = 0;
    fileHPeriods.fileHs.forEach(element => {
      totaleAll += (element.totAslCharge ? (+(element.totAslCharge + '').replace(',', '.')) : 0)
        + (element.totBirthplaceCharge ? (+(element.totBirthplaceCharge + '').replace(',', '.')) : 0)
        + (element.totUserCharge ? (+(element.totUserCharge + '').replace(',', '.')) : 0);
    });
    return totaleAll.toFixed(2).replace('.', ',');
  }

  getBindingTotal(fileHPeriods: FileHPeriods): string {
    let bindingTotal = 0;
    fileHPeriods.fileHs.forEach(element => {
      bindingTotal += (+(element.numberProjects + '').replace(',', '.'));
    });
    return bindingTotal.toString().replace('.', ',');
  }

  getFileNameFromPathWithoutExtension(fullPath: string): string {
    const fileName = fullPath.replace(/^.*[\\\/]/, '');
    return fileName.substring(0, fileName.indexOf('_') + 4);
  }

}

import {NgModule} from '@angular/core';
import {HomeLayoutComponent} from '@app/modules/home/layout/home-layout.component';
import {HomeRoutingModule} from '@app/modules/home/home-routing.module';
import {SharedModule} from '@app/shared/shared.module';
import {GuestModule} from '@app/modules/guest/guest.module';
import {FlexModule} from '@angular/flex-layout';
import {ToolbarComponent} from './layout/toolbar/toolbar.component';
import {MainNavComponent} from '@app/modules/home/layout/main-nav/main-nav.component';
import {MenuListItemComponent} from '@app/modules/home/layout/main-nav/menu-list-item/menu-list-item.component';
import {HealthFlowsModule} from '@app/modules/health-flows/health-flows.module';
import { AcceptanceModule } from '../acceptance/acceptance.module';
import { InvoicesModule } from '../invoices/invoices.module';
import { DocumentsModule } from '../documents/documents.module';

@NgModule({
  declarations: [
    HomeLayoutComponent,
    MainNavComponent,
    MenuListItemComponent,
    ToolbarComponent
  ],
  imports: [
    // BrowserAnimationsModule,
    HomeRoutingModule,
    HealthFlowsModule,
    SharedModule,
    FlexModule,
    GuestModule,
    AcceptanceModule,
    InvoicesModule,
    DocumentsModule
  ]
})
export class HomeModule {
}

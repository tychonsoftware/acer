import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeLayoutComponent} from '@app/modules/home/layout/home-layout.component';
import {DashboardComponent} from '@app/modules/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {
        path: 'acceptance',
        loadChildren: () => import('@app/modules/acceptance/acceptance.module').then(m => m.AcceptanceModule)
      },
      {
        path: 'referrals',
        loadChildren: () => import('@app/modules/assistance-projects/assistance-projects.module').then(m => m.AssistanceProjectsModule)
      },
      {
        path: 'healthflows',
        loadChildren: () => import('@app/modules/health-flows/health-flows.module').then(m => m.HealthFlowsModule)
      },
      {
        path: 'guest',
        loadChildren: () => import('@app/modules/guest/guest.module').then(m => m.GuestModule)
      },
      {
        path: 'archive-guest',
        loadChildren: () => import('@app/modules/archive-guest/archive-guest.module').then(m => m.ArchiveGuestModule)
      },
      {
        path: 'invoices',
        loadChildren: () => import('@app/modules/invoices/invoices.module').then(m => m.InvoicesModule)
      },
      {
        path: 'attendance-summary',
        loadChildren: () => import('@app/modules/attendance-summary/attendance-summary.module').then(m => m.AttendanceSummaryModule)
      },
      {
        path: 'documents',
        loadChildren: () => import('@app/modules/documents/documents.module').then(m => m.DocumentsModule)
      },
      {
        path: 'configurations',
        loadChildren: () => import('@app/modules/configurations/configurations.module').then(m => m.ConfigurationsModule)
      },
      {
        path: 'profilo',
        loadChildren: () => import('@app/modules/profilo/profilo.module').then(m => m.ProfiloModule)
      },
      {path: '', redirectTo: '/dashboard', pathMatch: 'full'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}

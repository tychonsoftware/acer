export interface NavItem {
  displayName: string;
  disabled?: boolean;
  matIconName?: string;
  svgIconName?: string;
  route: string;
  children?: NavItem[];
  openParent?: boolean;
}

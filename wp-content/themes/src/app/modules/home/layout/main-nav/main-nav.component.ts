/* tslint:disable:max-line-length */
import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {NavItem} from '@app/modules/home/layout/main-nav/nav-item';
import {NavService} from '@app/core/services/nav.service';
import {UserService} from '@app/core/http/users/user.service';
import {Functionalities} from '@app/core/constants/Functionalities';
import {User} from '@app/core/models';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements AfterViewInit, OnInit {
  @ViewChild('appDrawer') appDrawer?: ElementRef;

  after = false;

  navItems: NavItem[] = [];

  constructor(private breakpointObserver: BreakpointObserver,
              public navService: NavService,
              iconRegistry: MatIconRegistry,
              protected userSecurityService: UserService,
              sanitizer: DomSanitizer,
              public router: Router) {
    this.userSecurityService.getUserSecurityObservable()?.subscribe(user => this.checkSecurity(user));
    iconRegistry.addSvgIcon('dashboard', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/dashboard.svg'));
    iconRegistry.addSvgIcon('acceptance', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/acceptance.svg'));
    iconRegistry.addSvgIcon('file', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/file.svg'));
    iconRegistry.addSvgIcon('file_upload', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/file_upload.svg'));
    iconRegistry.addSvgIcon('nurity', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/nurity.svg'));
    iconRegistry.addSvgIcon('guest', sanitizer.bypassSecurityTrustResourceUrl('/assets/icon/guest.svg'));
  }

  private checkSecurity(user: User): void {
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.DASHBOARD)) {
      this.navItems.push(
        {
          displayName: 'Dashboard',
          svgIconName: 'dashboard',
          route: 'dashboard',
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.NEW_GUEST)) {
      this.navItems.push(
        {
          displayName: 'Accettazione',
          svgIconName: 'acceptance',
          route: 'acceptance'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_MANAGEMENT)) {
      this.navItems.push(
        {
          displayName: 'Gestione Ospiti',
          svgIconName: 'guest',
          route: 'guest'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.GUEST_ARCHIVE)) {
      this.navItems.push(
        {
          displayName: 'Archivio Ospiti',
          matIconName: 'library_books',
          route: 'archive-guest'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT
        || functionality.functionality === Functionalities.ASSISTANCE_PROJECT_READ)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT_HISTORIC)) {
      this.navItems.push(
        {
          displayName: 'Impegnative',
          matIconName: 'book',
          route: 'referrals',
          openParent: true,
          children: [
            {
              displayName: 'Storico Impegnative',
              route: 'referrals/history',
              matIconName: 'history',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ASSISTANCE_PROJECT
      || functionality.functionality === Functionalities.ASSISTANCE_PROJECT_READ)) {
      this.navItems.push(
        {
          displayName: 'Impegnative',
          matIconName: 'book',
          route: 'referrals'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.SANITARY_FLOW)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_MANAGEMENT || functionality.functionality === Functionalities.FILE_H_MANAGEMENT_READ)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_HISTORIC_MANAGEMENT || functionality.functionality === Functionalities.FILE_H_HISTORIC_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Flussi Sanitari',
          matIconName: 'sync',
          route: 'healthflows',
          children: [
            {
              displayName: 'Genera File H',
              route: 'healthflows/generate',
              matIconName: 'article',
            },
            {
              displayName: 'Storico File H',
              route: 'healthflows/history',
              matIconName: 'history',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.SANITARY_FLOW)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_MANAGEMENT || functionality.functionality === Functionalities.FILE_H_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Flussi Sanitari',
          matIconName: 'sync',
          route: 'healthflows',
          children: [
            {
              displayName: 'Storico File H',
              route: 'healthflows/history',
              matIconName: 'history',
            }
          ]
        }
      );
    }  else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.SANITARY_FLOW)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_MANAGEMENT || functionality.functionality === Functionalities.FILE_H_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Flussi Sanitari',
          matIconName: 'sync',
          route: 'healthflows',
          children: [
            {
              displayName: 'Storico File H',
              route: 'healthflows/history',
              matIconName: 'history',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.SANITARY_FLOW)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.FILE_H_HISTORIC_MANAGEMENT || functionality.functionality === Functionalities.FILE_H_HISTORIC_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Flussi Sanitari',
          matIconName: 'sync',
          route: 'healthflows',
          children: [
            {
              displayName: 'Genera File H',
              route: 'healthflows/generate',
              matIconName: 'article',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.SANITARY_FLOW)) {
      this.navItems.push(
        {
          displayName: 'Flussi Sanitari',
          matIconName: 'sync',
          route: 'healthflows'
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.ATTENDANCE_REPORT)) {
      this.navItems.push(
        {
          displayName: 'Riepilogo presenze',
          matIconName: 'swap_horiz',
          route: 'attendance-summary',
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_MANAGEMENT || functionality.functionality === Functionalities.INVOICE_MANAGEMENT_READ)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_HISTORIC_MANAGEMENT || functionality.functionality === Functionalities.INVOICE_H_HISTORIC_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Fatturazione',
          matIconName: 'payment',
          route: 'invoices',
          children: [
            {
              displayName: 'Genera Fattura',
              route: 'invoices/gen',
              matIconName: 'article',
            },
            {
              displayName: 'Gestione Fattura',
              route: 'invoices/history',
              matIconName: 'history',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_MANAGEMENT || functionality.functionality === Functionalities.INVOICE_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Fatturazione',
          matIconName: 'payment',
          route: 'invoices',
          children: [
            {
              displayName: 'Genera Fattura',
              route: 'invoices/gen',
              matIconName: 'article',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE)
      && user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE_HISTORIC_MANAGEMENT || functionality.functionality === Functionalities.INVOICE_H_HISTORIC_MANAGEMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Fatturazione',
          matIconName: 'payment',
          route: 'invoices',
          children: [
            {
              displayName: 'Gestione Fattura',
              route: 'invoices/history',
              matIconName: 'history',
            }
          ]
        }
      );
    } else if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.INVOICE)) {
      this.navItems.push(
        {
          displayName: 'Fatturazione',
          matIconName: 'payment',
          route: 'invoices'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.DOCUMENT
      || functionality.functionality === Functionalities.DOCUMENT_READ)) {
      this.navItems.push(
        {
          displayName: 'Documenti',
          matIconName: 'description',
          route: 'documents'
        }
      );
    }
    if (user?.functionalities?.find(functionality => functionality.functionality === Functionalities.CONFIGURATION)) {
      this.navItems.push(
        {
          displayName: 'Configurazione',
          matIconName: 'settings',
          route: 'configurations',
          children: [
            {
              displayName: 'Dati Generali',
              route: 'configurations/general-settings',
              matIconName: 'settings_applications',
            },
            {
              displayName: 'Contratti',
              route: 'configurations/contracts',
              matIconName: 'history_edu',
            },
			{
              displayName: 'Anagrafica Asl',
              route: 'configurations/asl-registry',
              svgIconName: 'guest',
            },
			{
              displayName: 'Anagrafica Ambito',
              route: 'configurations/area-registry',
              svgIconName: 'guest',
            },
          ]
        }
      );
    }
    this.navItems.push(
      {
        displayName: 'Profilo',
        matIconName: 'person',
        route: 'profilo'
      }
    );
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.navService.appDrawer = this.appDrawer;
  }
}

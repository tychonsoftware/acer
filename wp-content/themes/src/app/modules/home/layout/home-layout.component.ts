import {Component} from '@angular/core';

@Component({
  selector: 'app-home-layout',
  template: `
    <app-main-nav>
      <router-outlet></router-outlet>
    </app-main-nav>
  `
})
export class HomeLayoutComponent {
}

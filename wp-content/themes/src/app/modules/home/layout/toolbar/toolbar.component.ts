import {Component} from '@angular/core';
import {NavService} from '@app/core/services/nav.service';
import {environment} from '@env/environment';
import {Router} from '@angular/router';
import {UserService} from '@app/core/http/users/user.service';
import {CookieService} from 'ngx-cookie-service';
import {User} from '@app/core/models';
import {OrganizationBuilding} from '@app/core/models/users/organization-building.model';

@Component({
  selector: 'app-toolbar',
  templateUrl: 'toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  user: User | undefined;
  currentBuilding: OrganizationBuilding | undefined;

  constructor(public navService: NavService,
              public userSecurityService: UserService,
              private route: Router, private cookieService: CookieService) {
  }

  exit(): void {
    this.cookieService.set('/oidc/logout', 'true');
    window.location.href = environment.main_page_url;
  }

  goMainPage(): void {
    window.location.href = environment.main_page_url;
  }

  goProfilo(): void {
    this.route.navigate(['/profilo']);
  }

  public getUserInitials(): string {
    const names = this.getUserName().split(' ');
    let initials = names[0].substring(0, 1).toUpperCase();
    if (names.length > 1) {
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return  initials;
  }

  public getUserName(): string {
    let fullName = '';
    if (this.userSecurityService != null) {
      const user = this.userSecurityService.getUserSecurity();

      if (user) {
        this.user = user;

        if (user.defaultBuilding && (user.defaultBuilding.id || user.defaultBuilding.id === 0) &&
          !localStorage.getItem('buildingId')) {
          localStorage.setItem('buildingId', '' + user.defaultBuilding.id);
        } else if (!user.defaultBuilding || !user.defaultBuilding.id) {
          // console.log('Default building id not found in current user!');
        }

        if (user && user.name && user.name !== null) {
          fullName = user.name;
        }

        if (user && user.surname && user.surname !== null) {
          fullName = fullName + ' ' + user.surname;
        }
        if (fullName === '' && user && user.username) {
          fullName = user.username;
        }
        if (this.user && this.user.buildings) {
          this.user.buildings = this.user.buildings.sort((a: any, b: any) =>
            a?.name?.toLowerCase() > b?.name?.toLowerCase() ? 1 : -1);
          this.user.buildings.forEach((building: OrganizationBuilding, index: number) => {
            if (building.id === this.getCurrentBuildingId()) {
              this.currentBuilding = building;
              // this.user?.buildings?.splice(index, 1);
            }
          });
        }
      }
    }
    return fullName;
  }

  getCurrentBuildingId(): number {
    const item = localStorage.getItem('buildingId');
    if (item) {
      return Number.parseInt(item, 10);
    } else {
      return this.user?.defaultBuilding ? this.user.defaultBuilding.id ? this.user.defaultBuilding.id : 0 : 0;
    }
  }

  changeBuilding(id?: number): void {
    if (id || id === 0) {
      localStorage.setItem('buildingId', '' + id);
      window.location.reload();
    }
  }
}

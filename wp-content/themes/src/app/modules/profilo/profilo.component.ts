import {Component, OnInit} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogConfig } from '@angular/material/dialog';
import { UserService } from '@app/core/http/users/user.service';
import { DialogService } from '@app/core/services/dialog.service';
import { FormContentDirective } from '@app/shared/directives/form-content.directive';
import {ProfiloDialogComponent} from '@app/modules/profilo/profilo-dialog/profilo-dialog.component';
import {IFormDialogConfig} from '@app/shared/components/form-dialog/form-dialog.component';

@Component({
  selector: 'app-profilo',
  templateUrl: './profilo.component.html',
  styleUrls: ['./profilo.component.scss']
})
export class ProfiloComponent extends FormContentDirective implements OnInit {

  form = this.fb.group({
    uuid: [null],
    firstName: [null],
    lastName: [null],
    fiscalCode: [null],
    landingPhone: [null],
    nomeUtente: [null],
    email: [null],
    ruolo: [null],
    profession: [null]
  });

  // Dialog config parameter
  dialogConfig: MatDialogConfig = {
    disableClose: true,
    autoFocus: true,
    width: '80%'
  };
  showProfession = false;

  constructor(private fb: FormBuilder, private userSecurityService: UserService, private dialogService: DialogService) {
    super();
  }

  ngOnInit(): void {
    this.form.disable();
    const user = this.userSecurityService.getUserSecurity();
    if (user) {
      if (user.types && user.types.length > 0 && user.types[0].type && user.types[0].roles){
        user.types[0].roles?.forEach(role => {
          if (role.name === 'CLINICO_SANITARIO') {
            this.showProfession = true;
          }
        });
      }
      this.form?.patchValue({
        firstName: user.name,
        lastName: user.surname,
        fiscalCode: user.fiscalCode,
        landingPhone: user.mobile,
        nomeUtente: user.username,
        email: user.email,
        ruolo: user.types && user.types.length > 0 ? user.types[0].type : '',
        profession: user.profession?.description
      });
    }
    console.log(user);
  }
  _openRuoloDialog(): void {
    const user = this.userSecurityService.getUserSecurity();
    const data: Partial<IFormDialogConfig> = {
      editFormData: user?.types,
    };
    this.dialogService.openFormDialog(ProfiloDialogComponent, data, this.dialogConfig);
  }
}

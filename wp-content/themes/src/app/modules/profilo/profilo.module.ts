import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ProfiloRoutingModule} from '@app/modules/profilo/profilo-routing';
import {SharedModule} from '@app/shared/shared.module';
import {MultiTranslateHttpLoader} from '@app/core/services/multi-translate-loader';
import { ProfiloComponent } from '../profilo/profilo.component';
import { ProfiloDialogComponent } from './profilo-dialog/profilo-dialog.component';

export function createTranslateLoader(http: HttpClient): MultiTranslateHttpLoader {
  return new MultiTranslateHttpLoader(http, [
    {prefix: './assets/i18n/app/', suffix: '.json'},
    {prefix: './assets/i18n/configurations/', suffix: '.json'}
  ]);
}

@NgModule({
  declarations: [
    ProfiloComponent,
    ProfiloDialogComponent
  ],
  imports: [
    ProfiloRoutingModule,
    SharedModule,
    ReactiveFormsModule,

    // Per la traduzione eventuale in altre lingue
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      defaultLanguage: 'it',
      isolate: true
    })
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfiloModule {
}

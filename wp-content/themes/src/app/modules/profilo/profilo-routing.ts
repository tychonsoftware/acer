import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfiloComponent} from '@app/modules/profilo/profilo.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: ProfiloComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfiloRoutingModule {
}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GuestTransition, UserFunctionalities} from '@app/core/models';
import { FormContentDirective } from '@app/shared/directives/form-content.directive';

@Component({
  selector: 'app-profilo-dialog',
  templateUrl: './profilo-dialog.component.html',
  styleUrls: ['./profilo-dialog.component.scss']
})
export class ProfiloDialogComponent extends FormContentDirective implements OnInit {

  functionalitiesUserManagement: UserFunctionalities[] = [];
  functionalitiesClinic: UserFunctionalities[] = [];
  functionalitiesAdministration: UserFunctionalities[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.data && this.data.editFormData && this.data.editFormData.length > 0) {
      const functionalities = this.data.editFormData[0].functionalities;
      if (functionalities) {
        functionalities?.forEach((functionality: UserFunctionalities) => {
            if (functionality?.portal?.description === 'administration') {
              this.functionalitiesAdministration.push(functionality);
            } else if (functionality?.portal?.description === 'user-management') {
              this.functionalitiesUserManagement.push(functionality);
            } else if (functionality?.portal?.description === 'clinic') {
              this.functionalitiesClinic.push(functionality);
            }
          }
        );
      }
    }
  }
}

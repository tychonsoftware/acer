<div class="entry-summary multi-line-truncate">
<?php 
    $excerpt = get_the_excerpt();
    $data = explode(" Allegati ", $excerpt); 
    echo $data[0];
?>
<?php if( is_search() ) { ?><div class="entry-links"><?php wp_link_pages(); ?></div><?php } ?>
</div>
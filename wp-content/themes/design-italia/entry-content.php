<section class="entry-content">
	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
	<?php
    $post_type = get_post_type( get_the_ID());
    if($post_type === 'avcp'){
        add_filter( 'the_content', 'replace_content' );
    }

    function replace_content( $content ) {
        $filteredcontent =  $content;
        $dom = new \DOMDocument();
        $dom->loadHTML($filteredcontent,  LIBXML_HTML_NODEFDTD);
        $finder = new \DOMXPath($dom);
        $tagToRemove = $finder->query("//*[contains(@class, 'tabalbo')]");
        if($tagToRemove && count($tagToRemove) > 0){
            $tagToRemove[0]->parentNode->removeChild($tagToRemove[0]);
        }
        $filteredcontent = $dom->saveHTML();

        $filteredcontent = preg_replace('<body>', '', $filteredcontent);
        return preg_replace('<html>', '', $filteredcontent);

    }
    the_content();
    ?>
	<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>

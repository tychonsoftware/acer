<div style="max-width: 560px; padding: 20px; background: #ffffff; border-radius: 5px; margin: 40px auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="color: #444444; font-weight: normal;">
<div style="text-align: center; font-weight: 600; font-size: 26px; padding: 10px 0; border-bottom: solid 3px #eeeeee;">{site_name}</div>
<div style="clear: both;"> </div>
</div>
<div style="padding: 0 30px 30px 30px; border-bottom: 3px solid #eeeeee;">
<div style="padding: 30px 0; font-size: 24px; text-align: center; line-height: 40px;">Il tuo account è stato cancellato.<span style="display: block;">Tutti i tuoi dati personali e tutti i caricamenti sono stati rimossi in modo permanente.</span></div>
<div style="padding: 15px; background: #eee; border-radius: 3px; text-align: center;">Se il tuo account è stato cancellato per errore, per favore <a style="color: #3ba1da; text-decoration: none;" href="mailto:{admin_email}">contattaci</a>.</div>
</div>
</div>
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_class'     => 'primary-menu',
							)
						);
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>',
							)
						);
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>

			<div class="site-info">
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
				<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}
				?>
				<a href="<?php echo esc_url( __( 'https://www.innovatics.it//', 'twentysixteen' ) ); ?>" class="imprint">
					<?php printf( __( 'Powered by %s', 'twentysixteen' ), 'Innovatics with WL-FE'); ?>
				</a>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
        <div class="multi-logo-main-section" style="margin-top: -9rem;padding-bottom: 2rem;">
            <div class="multi-logo-section">
               <!-- <div class="unione-europea">
                    <img style="height: 125px;" src="/wordpress/wp-content/themes/twentysixteen/img/unine_europia.png">
                    <div><b>Unione Europea</b>Fondo Europea<br/>di Sviluppo Regionale</div>
                </div>
                <div style="text-align: center;">
                    <img style="max-height: 100px;" src="/wordpress/wp-content/themes/twentysixteen/img/republicca.png">
                    <div><b style="font-size: 19px;">REPUBBLICA ITALIANA</b></div>
                </div>
                <div style="text-align: center;">
                    <img src="/wordpress/wp-content/themes/twentysixteen/img/regione-umbria-logo.png">
                </div>
                <div style="text-align: center;">
                    <img src="/wordpress/wp-content/themes/twentysixteen/img/umbria.png">
                </div>-->
                <div>
                    <img src="<?php echo esc_url( get_template_directory_uri() . '/img/multi-logo.png' ); ?>">
                </div>
            </div>
        </div>
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
